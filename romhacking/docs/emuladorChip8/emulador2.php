<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>Tutorial de creaci�n de un Emulador sencillo de Chip-8 con VS 2008 y C# (Parte 2)</title>
</head>

<body>
<small>
<span style="font-family: Verdana;">
<a href="http://darknromhacking.com/">Volver a P�gina Principal</a>
</small>

<hr style="width: 100%; height: 2px;">

<table width="100%">
<tr>
<td>

<H3>
<span style="font-weight: bold;">Tutorial de creaci�n de un Emulador sencillo de Chip-8 con VS 2008 y C# (Parte 2)</span>
</H3>
<small>
Lenguaje: C#<br>
Para: VS 2008 con Sdl.Net 6.1<br>
Por Dark-N: <a href="mailto:hernaldog@gmail.com">hernaldog@gmail.com</a>
<br>

<span style="font-family: Verdana;">
<a href="http://darknromhacking.com/">http://darknromhacking.com</a>
<br>
Hilo del Foro: <a href="http://foro.romhackhispano.org/viewtopic.php?f=4&t=872">
http://foro.romhackhispano.org/viewtopic.php?f=4&t=872</a>
<br>
</span>
Actualizaci�n 13-08-2011: Se agreg� la variable rnd de tipo Random.
</small></td>
<td align="center">
</table>



<hr style="width: 100%; height: 2px;">

<span style="font-family: Verdana;"><small>
<a href="emulador1.php">Lecci�n Anterior</a> | <a href="emulador_menu.php">�ndice</a> | <a href="emulador3.php">Siguiente Lecci�n</a>

<H3>La idea</H3>

La idea en este cap�tulo es hacer el emulador m�s sencillo posible, lo realizaremos simplemente en modo <b>Consola</b>, solo utilizando caracteres ASCII para representar los gr�ficos (como "\", "/", "|" o "*") y no tendr� sonidos ni eventos de Teclado o Joystick.
<br>
En la imagen inferior se visualiza el juego <b>PONG</b> emulado en modo Consola, que es lo que haremos en este cap�tulo y en el siguiente:
<br><br>
<img height="40%" width="40%" src="imag/corriendo_modo_consola.PNG">

<H3>Crear Proyecto</H3>

Lo primero es crear un proyecto de tipo Consola:
<br><br>
<img src="imag/crear_proy_consola.PNG">


<H3>Using</H3>

Como se trabajar� en modo Consola, se deben agregar solo estos namespaces:
<br>
<pre style="font-size:12; background-color:#D8D8D8;">
using System;
using System.IO;
</pre>
NOTA: Si se est� trabajando con el <b>Framework 1.1</b> y no el <b>Framework 2.0</b> se debe incorporar adem�s el siguiente namespace para poder usar de la <b>bocina de la bios</b> (para simular el sonido) y realizar un <b>clearscren</b> de la ventada de comandos:
<br>
<pre style="font-size:12; background-color:#D8D8D8;">
using System.Runtime.InteropServices;  //para "clearscreen" y el "beep" de la Bios
</pre>

<H3>Variables Globales que representan la arquitectura de Chip-8</H3>

Antes de programar esta parte, se muy necesario que leas acerca de la arquitectura de Chip-8. Un buen sitio es en <a href="http://es.wikipedia.org/wiki/Chip-8">Wikipedia</a> o <a href="http://devernay.free.fr/hacks/chip8/C8TECH10.HTM">Cowgod's Chip-8</a>.
<br>
<pre style="font-size:12; background-color:#D8D8D8;">
//variables principales        
const int RES_X = 64;  //resolucion de pantalla 64x32
const int RES_Y = 32;        
const int DIR_INICIO = 0x200;
const int TAMANO_MEM = 0xFFF;
const int TAMANO_PILA = 16;
const int CANT_REGISTROS = 16;
int[,] arregloPantalla = new int[RES_X, RES_Y];

//arreglo que representa la memoria
int[] memoria = new int[TAMANO_MEM];

//Arreglo que representa los 16 Registros (V) 
int[] V = new int[CANT_REGISTROS];

//arreglo que representa la Pila
int[] pila = new int[TAMANO_PILA];

//variables que representan registros varios
int instruccion;  //representa una instruccion del Chip-8. Tiene largo 2 byte
int PC;
int I;
int SP;
int KK;

//variables para manejar los opcodes
int opcode1 = 0;
int opcode2 = 0;  //X
int opcode3 = 0;  //Y
int opcode4 = 0;
int NNN = 0;

//variables que representan los 2 timers: Delay Timer y Sound Timer
int delayTimer;
int soundTimer;
</pre>

Nota: Como no implementaremos el sonido en esta primera parte del tutorial, esta variable que representa la timer de sonido no se utilizar� en la l�gica principal.
<br><br>
Ojo con las notaciones, en el resto del c�digo utilizaremos mucho la nomenclatura <b>0xValor</b> indicando que Valor es un valor <b><a href="http://es.wikipedia.org/wiki/Sistema_hexadecimal">Hexadecimal</a></b> y no un <b>Decimal</b> o <b>Entero</b>. Esto quiere decir que <b>0x200</b> significa un n�mero 200 hex y no simplemente n�mero 200. Si se convierte 0x200 a decimal usando la calculadora de Windows nos da <b>0x200 = 512</b>. Es decir 200 hexadecimal equivale a 512 en numero entero o decimal
<br><br>
Ahora voy a explicar acerca del sistema de <b>gr�ficos</b> del Chip-8 traduciendo lo explicado en <a href="http://devernay.free.fr/hacks/chip8/C8TECH10.HTM">Cowgod's Chip-8</a>. La gr�fica original del Chip-8 es monocrom�tica (un solo color) con una resoluci�n de <b>64x32</b> pixels, aunque algunos int�rpretes como el <b>ETI 660</b> tiene gr�ficos como 64x48 o 64x64. El <b>Super Chip-48</b>, que es un interprete para la calculadora <a href="http://es.wikipedia.org/wiki/HP48">HP 48</a>, tiene el modo 128x64. Este modo es soportado por la mayor�a de de los int�rpretes sobre otras plataformas.

<br><br>
<TABLE BORDER=1 WIDTH=128 HEIGHT=64 CELLPADDING=0 CELLSPACING=0 ALIGN=left>
	<TR><TD>
		<TABLE BORDER=0 HEIGHT=60 WIDTH=100%> 
			<TR><TD VALIGN=top ALIGN=left>(0,0)</TD><TD VALIGN=top ALIGN=right>(63,0)</TD></TR>
			<TR><TD VALIGN=bottom ALIGN=left>(0,31)</TD><TD VALIGN=bottom ALIGN=right>(63,31)</TD></TR>
		</TABLE>
	</TD></TR>
</TABLE>

<br><br><br><br><br>
Chip-8 pinta los gr�ficos en pantalla a trav�s del uso de <b>sprites</b>. Un sprite es un grupo de bytes, en el cual, al representarse de modo binario, arma o conforma la gr�fica deseada. Los sprites del Chip-8 pueden ser de hasta 15 bytes, para un posible tama�o de sprite de 8x15.
<br>
Los programas o juegos utilizan grupos de sprites para representar caracteres o d�gitos (tambi�n llamados <b>fonts</b>) que van del 0 al 9 y de la A a la F. Estos sprites son de <b>5</b> bytes de largo o lo que es lo mismo 8x5 pixels. Los datos de estos caracteres se almacena el sector de la memoria que es fijo para esta tarea en el Chip-8 (desde 0x000 a 0x1FF). Abajo se muestra un ejemplo del car�cter "0" en formato byte y binario. En este �ltimo modo, se puede ver dibujado el d�gito "0". Estrictamente el "0" se puede dibujar con 4x5 bits ya que se ven solo ceros desde la mitad hacia la derecha:

<br><br>

<TABLE BORDER=1 CELLPADDING=3 CELLSPACING=0>
	<TR><TD>"0"</TD><TD>Binary</TD><TD>Hex</TD></TR>			
	<TR>
		<TD><TT>

			****<BR>
			*&nbsp;&nbsp;*<BR>
			*&nbsp;&nbsp;*<BR>
			*&nbsp;&nbsp;*<BR>
			****<BR>

		</TT></TD>
		<TD><TT>
			11110000<BR>
			10010000<BR>
			10010000<BR>
			10010000<BR>
			11110000<BR>

		</TT></TD>
		<TD><TT>
			0xF0<BR>
			0x90<BR>
			0x90<BR>
			0x90<BR>
			0xF0<BR>

		</TT></TD>
	</TR>
</TABLE>

<br>
<H3>Variables Globales que nos ayudan a representar otros puntos</H3>

Las siguientes variables las cree ya que para mi son la mejora forma de simular el manejo de las <b>fuentes</b> que en CHIP-8 abarcan del 0 al 9 y de la A a la F. Esto no quiere decir en absoluto que existan otras formas de manejar estas fuentes. Para los que tienen dudas de como se ven las fuentes en el juego, aqu� les pongo un ejemplo:
<br><br>
<img src="imag/ejemplo_font.PNG">
<br>

<pre style="font-size:12; background-color:#D8D8D8;">
//variables para el manejo de fuentes (80 bytes, ya que hay 5 bytes x caracter
//y son 16 caracteres o letras (5x16=80). Cada font es de 4x5 bits.
int[] arregloFuentes = {
	0xF0, 0x90, 0x90, 0x90, 0xF0,	// valores para 0
	0x60, 0xE0, 0x60, 0x60, 0xF0,	// valores para 1
	0x60, 0x90, 0x20, 0x40, 0xF0,	// valores para 2
	0xF0, 0x10, 0xF0, 0x10, 0xF0,	// valores para 3
	0x90, 0x90, 0xF0, 0x10, 0x10,	// valores para 4
	0xF0, 0x80, 0x60, 0x10, 0xE0,	// valores para 5
	0xF0, 0x80, 0xF0, 0x90, 0xF0,	// valores para 6
	0xF0, 0x10, 0x10, 0x10, 0x10,	// valores para 7
	0xF0, 0x90, 0xF0, 0x90, 0xF0,	// valores para 8
	0xF0, 0x90, 0xF0, 0x10, 0x10,	// valores para 9
	0x60, 0x90, 0xF0, 0x90, 0x90,	// valores para A
	0xE0, 0x90, 0xE0, 0x90, 0xE0,	// valores para B
	0x70, 0x80, 0x80, 0x80, 0x70,	// valores para C
	0xE0, 0x90, 0x90, 0x90, 0xE0,	// valores para D
	0xF0, 0x80, 0xF0, 0x80, 0xF0,	// valores para E
	0xF0, 0x90, 0xF0, 0x80, 0x80	// valores para F
};
</pre>

�Pero como equivalen esos bytes a ciertos d�gitos o letras?<br><br>

Para responder a eso hay que entender lo explicado arriba y que los fonts son de <b>8x5</b> y que los d�gitos se pueden dibujar con solo <b>4x5</b> pixels (5 filas de bits por 4 columnas de bits) y adem�s son de <b>1 bit por plano</b>, por lo tanto cada uno de los d�gitos (0x60, 0x90, etc) representa una fila, de esta forma si tomamos los d�gitos del <b>'2'</b> son: <b>0x60 0x90 0x20 0x40 0xF0</b>, entonces el 0x60 representa la primera fila de las 5 (4x5), el 0x90 la segunda de las 5, y as� sucesivamente. Si quieren hacerse una idea m�s gr�fica, les recomiendo bajar mi <a href="http://darknromhacking.com/archivos/snes_font_simulator_v11.zip">aplicaci�n</a> que desarroll� el 2006, que est� hecha para hacer o ver fuentes de la <a href="http://es.wikipedia.org/wiki/Snes">SNES</a>. Para usarla, se debe setear el modo Bit Plane de 1 bit y luego podemos escribir los bytes <b>60902040F0</b> y luego le damos a "VER" y mostrar�a algo as�:  
<br><br>
<img src="imag/ejemplo_2_snes_font_simulator.PNG">
<br><br>

Mas variables a utilizar son:
<pre style="font-size:12; background-color:#D8D8D8;">
private bool[] teclasPresionadas = { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false };

const int TECLA_1	= 0;
const int TECLA_2	= 1;
const int TECLA_3	= 2;
const int TECLA_4	= 3;
const int TECLA_Q	= 4;
const int TECLA_W	= 5;
const int TECLA_E	= 6;
const int TECLA_R	= 7;
const int TECLA_A	= 8;
const int TECLA_S	= 9;
const int TECLA_D	= 10;
const int TECLA_F	= 11;		
const int TECLA_Z	= 12;
const int TECLA_X	= 13;
const int TECLA_C	= 14;
const int TECLA_V	= 15;

// mapeamos las 16 teclas de Chip8
private byte[] MapeoTeclas = 
{	       
	0x01,0x02,0x03,0x0C,
	0x04,0x05,0x06,0x0D,
	0x07,0x08,0x09,0x0E,
	0x0A,0x00,0x0B,0x0F 
};
</pre>

Si les nace la duda, porqu� ese orden para el "Mapeo" de Teclas, seg�n <a href="http://devernay.free.fr/hacks/chip8/C8TECH10.HTM">Cowgod's Chip-8</a>, el teclado del Chip-8 tiene un key-pad de 16 teclas hexadecimales, repartidos de la forma:

<br><br>
<TABLE BORDER=1 CELLPADDING=3 CELLSPACING=0 ALIGN=left>
<TR><TD><TT>1</TT></TD><TD><TT>2</TT></TD><TD><TT>3</TT></TD><TD><TT>C</TT></TD></TR>
<TR><TD><TT>4</TT></TD><TD><TT>5</TT></TD><TD><TT>6</TT></TD><TD><TT>D</TT></TD></TR>
<TR><TD><TT>7</TT></TD><TD><TT>8</TT></TD><TD><TT>9</TT></TD><TD><TT>E</TT></TD></TR>
<TR><TD><TT>A</TT></TD><TD><TT>0</TT></TD><TD><TT>B</TT></TD><TD><TT>F</TT></TD></TR>
</TABLE>

<br><br><br><br><br><br><br>

Por �ltimo si se est� trabajando con el <b>Framework 1.1</b> y no el <b>Framework 2.0</b> se deben crear estas variables y declarar las funciones:
<br>
<pre style="font-size:12; background-color:#D8D8D8;">
[DllImport("Kernel32.dll")] //para beep
public static extern bool Beep(UInt32 frequency, UInt32 duration);
//En Framework 2.0 se puede usar directamente: Console.Beep(350, 50);

[DllImport("msvcrt.dll")] //Framwork 1.1: para cls (clear screen) en DOS
public static extern int system(string cmd);
//En Framework 2.0 se puede usar directamente: Console.Clear();
</pre>

La primera es para el uso del "Beep" utilizando la bocina de la BIOS. En el Framework 1.1 se usa la DLL del Kernel32 para lograr esto. Como en esta parte del tutorial lo utilizaremos el sonido, se dejar� comentado.
<br>
La segunda es para realizar un "Clear screen" en la ventana de comandos ya que este m�todo no est� presente en la librer�a. Una vez declarado, se puede usar de la forma <b>system("cls")</b> en cualquier parte del c�digo.
<br>
Ambas clases fueron mejoradas en el Framewrok 2.0 y en contemplan los m�todos <b>Console.Beep(int frecuencia sonido, int duraci�n)</b> y <b>Console.Clear()</b>.
<br><br>
<b>NOTA</b>: por alguna raz�n que no he logrado averiguar, el uso del system("cls"), me da mejor resultado a nivel visible que Console.Clear(), por lo que lo utilizar� a pesar de que estoy usando el Framework 2.0.
<br><br>

<br>
<b>Clase Random</b>
<br><br>
Algunas instrucciones del Chip-8 utilizan la funcionalidad de n�mero aleatorio. El interprete Chip-8 ten�a su propio mecanismo para esto, nosotros en C# tenemos la clase <b>Random</b>, por lo que crearemos una variable de este tipo que se utilizar� m�s adelante:
<br>
<pre style="font-size:12; background-color:#D8D8D8;">
//Variable de tipo Random (para generar n�meros aleatoios) utilizada por ciertas instrucciones
Random rnd = new Random();
</pre>

<hr style="width: 100%; height: 2px;">

C�digo fuente de esta parte (C# 2.0):
<br>
<pre style="font-size:12; background-color:#D8D8D8;">
using System;
using System.IO;
using System.Runtime.InteropServices;  //para "clearscreen" y el "beep" de la Bios

namespace Chip8_ConsoleMode
{
    class Emulador
    {

        #region variables emulador

        //variables principales        
        const int RES_X = 64;  //resolucion de pantalla 64x32
        const int RES_Y = 32;        
        const int DIR_INICIO = 0x200;
        const int TAMANO_MEM = 0xFFF;
        const int TAMANO_PILA = 16;
        const int CANT_REGISTROS = 16;
        int[,] arregloPantalla = new int[RES_X, RES_Y];

        //arreglo que representa la memoria
        int[] memoria = new int[TAMANO_MEM];

        //Arreglo que representa los 16 Registros (V) 
        int[] V = new int[CANT_REGISTROS];

        //arreglo que representa la Pila
        int[] pila = new int[TAMANO_PILA];

        //variables que representan registros varios
        int instruccion;  //representa una instruccion del Chip-8. Tiene largo 2 byte
        int PC;
        int I;
        int SP;
        int KK;

        //variables para manejar los opcodes
        int opcode1 = 0;
        int opcode2 = 0;  //X
        int opcode3 = 0;  //Y
        int opcode4 = 0;
        int NNN = 0;

        //variables que representan los 2 timers: Delay Timer y Sound Timer
        int delayTimer;
        int soundTimer;

	//variables para el manejo de fuentes (80 bytes, ya que hay 5 bytes x caracter
	//y son 16 caracteres o letras (5x16=80). Cada font es de 4x5 bits. 
	int[] arregloFuentes = {
			0xF0, 0x90, 0x90, 0x90, 0xF0,	// valores para 0
			0x60, 0xE0, 0x60, 0x60, 0xF0,	// valores para 1
			0x60, 0x90, 0x20, 0x40, 0xF0,	// valores para 2
			0xF0, 0x10, 0xF0, 0x10, 0xF0,	// valores para 3
			0x90, 0x90, 0xF0, 0x10, 0x10,	// valores para 4
			0xF0, 0x80, 0x60, 0x10, 0xE0,	// valores para 5
			0xF0, 0x80, 0xF0, 0x90, 0xF0,	// valores para 6
			0xF0, 0x10, 0x10, 0x10, 0x10,	// valores para 7
			0xF0, 0x90, 0xF0, 0x90, 0xF0,	// valores para 8
			0xF0, 0x90, 0xF0, 0x10, 0x10,	// valores para 9
			0x60, 0x90, 0xF0, 0x90, 0x90,	// valores para A
			0xE0, 0x90, 0xE0, 0x90, 0xE0,	// valores para B
			0x70, 0x80, 0x80, 0x80, 0x70,	// valores para C
			0xE0, 0x90, 0x90, 0x90, 0xE0, 	// valores para D
			0xF0, 0x80, 0xF0, 0x80, 0xF0,	// valores para E
			0xF0, 0x90, 0xF0, 0x80, 0x80	// valores para F
	};

        private bool[] teclasPresionadas = { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false };

        const int TECLA_1 = 0;
        const int TECLA_2 = 1;
        const int TECLA_3 = 2;
        const int TECLA_4 = 3;
        const int TECLA_Q = 4;
        const int TECLA_W = 5;
        const int TECLA_E = 6;
        const int TECLA_R = 7;
        const int TECLA_A = 8;
        const int TECLA_S = 9;
        const int TECLA_D = 10;
        const int TECLA_F = 11;
        const int TECLA_Z = 12;
        const int TECLA_X = 13;
        const int TECLA_C = 14;
        const int TECLA_V = 15;

        // mapeamos las 16 teclas de Chip8
        private byte[] MapeoTeclas = 
        {	       
            0x01,0x02,0x03,0x0C,
            0x04,0x05,0x06,0x0D,
            0x07,0x08,0x09,0x0E,
            0x0A,0x00,0x0B,0x0F 
        };

        //[DllImport("Kernel32.dll")] //para beep
        //public static extern bool Beep(UInt32 frequency, UInt32 duration);
        //En Framework 2.0 se puede usar directamente: Console.Beep(350, 50);

        [DllImport("msvcrt.dll")] //Framwork 1.1: para cls (clear screen) en DOS
        public static extern int system(string cmd);
        //En Framework 2.0 se puede usar directamente: Console.Clear();

        //Variable de tipo Random (para generar n�meros aleatoios) utilizada por ciertas instrucciones
        Random rnd = new Random();

        #endregion

        
        static void Main(string[] args)
        {
           // ....
        }
    }
}
</pre>

<a href="emulador1.php">Lecci�n Anterior</a> | <a href="emulador_menu.php">�ndice</a> | <a href="emulador3.php">Siguiente Lecci�n</a>

<?php
include '../../piecdisq.php';
?>
