<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>Tutorial de creaci�n de un Emulador sencillo de Chip-8 con VS 2008 y C#</title>
</head>

<body>
<small>
<span style="font-family: Verdana;">
<a href="http://darknromhacking.com/">Volver a P�gina Principal</a>
</small>

<hr style="width: 100%; height: 2px;">

<table width="100%">
<tr>
<td>

<H3>
<span style="font-weight: bold;">Tutorial de creaci�n de un Emulador sencillo de Chip-8 con VS 2008 y C#</span>
</H3>
<small>
Lenguaje: C# 1.0/2.0<br>
Para: VS 2008 con Sdl.Net 6.1<br>
Por Dark-N: <a href="mailto:hernaldog@gmail.com">hernaldog@gmail.com</a>
<br>

<span style="font-family: Verdana;">
<a href="http://darknromhacking.com/">http://darknromhacking.com</a>
<br>
Hilo del Foro: <a href="http://foro.romhackhispano.org/viewtopic.php?f=4&t=872">
http://foro.romhackhispano.org/viewtopic.php?f=4&t=872</a><br>
</span>
</small></td>
<td align="center">
</table>


<hr style="width: 100%; height: 2px;">

<span style="font-family: Verdana;"><small>

<H3>Introducci�n</H3>

Existen muchas p�ginas donde se muestran pistas de como escribir un emulador, pero al menos yo no he visto una que te ense�e paso a paso como hacerlo, mucho menos en espa�ol.
<br>Este tutorial tiene por objetivo ense�ar los conceptos y la codificaci�n de un emulador de <a href="http://es.wikipedia.org/wiki/Chip-8">Chip-8</a>, uno de los m�s sencillos de emular por sus pocos registros e instrucciones. Adem�s permite ejeuctar algunas <a href="http://es.wikipedia.org/wiki/ROM">ROMs</a> como el cl�sico <a href="http://es.wikipedia.org/wiki/Pong">PONG</a>.
<br><br>
<img src="imag/emuladorChip8.PNG">

<H3>�ndice</H3>

<li><a href="emulador1.php">Parte 1</a>: Operaciones b�sicas con Bits, Operadores Shift, Instrucciones de C# que nos ayudan a extraer bits de una direcci�n, Leer e Unir bits.</li>
<li><a href="emulador2.php">Parte 2</a>: Se arma esqueleto del programa. Se har� emulador en Modo Consola primero. Se explican las variables globales a utilizar. Hasta aqu� si alguien lo quiere hacer en Java, ser�a pr�cticamente copiar y pegar ya que el c�digo es muy parecido.</li>
<li><a href="emulador3.php">Parte 3</a>: Se explica como es el programa principal y el game-loop. Se muestra como es la carga de la ROM en la Memoria y como se leen las instrucciones y ejecutan en el denominado Ciclo Fetch (por ahora solo 2 instrucciones de las 36 que tiene el Chip-8).</li>
<li><a href="emulador4.php">Parte 4</a>: Explicaci�n de todas las instrucciones restantes.</li>
<li><a href="emulador5.php">Parte 5</a>: Se explican los pasos para realizar los cambios en el proyecto y m�todos para transformar el emulador de un formato Consola a formato Gr�fico usando SDL.Net y se agrega el uso de Teclado.</li>
<li><a href="emulador6.php">Parte 6</a>: Mejoras en el emulador como t�tulo de la ventana, c�lculo de FPS y agregar sonido.</li>
<li><a href="emulador7.php">Parte 7</a> <img src="../../imag/new.gif">: pasamos el emulador a una ventana con las opciones: Cargar ROM, Reset y Pause.
<li>Parte 8: Pr�ximamente: como hacer el m�dulo "Save State" y "Load State".

<H3>Errores Comunes <img src="../../imag/new.gif"></H3>

<li><a href="errores_soluciones.php">Soluci�n a Errores Comunes</a></li>

<H3>Links de Inter�s</H3>

<li><a href="http://es.wikipedia.org/wiki/Chip-8">Wikipedia</a> Chip-8 en Wikipedia.
<li><a href="http://devernay.free.fr/hacks/chip8/C8TECH10.HTM">Cowgod's Chip-8</a>: Referencia t�cnica del Chip-8 (en ingl�s).
<li><a href="http://www.retrowip.com/2009/03/01/emulador-de-sega-master-system-en-c-espanol/">Retrowip</a> Tutorial en espa�ol, en formato PDF muy  completo donde se explica como hacer emulador de Master System con C++.
<li><a href="http://www.cecs.csulb.edu/~hill/cecs497/nestreme/howto.html">cecs.csulb.edu</a> Tutorial donde se ense�a a un alto nivel como hacer un emulador usando <a href="http://es.wikipedia.org/wiki/C%2B%2B">C++</a> con <a href="http://es.wikipedia.org/wiki/Lenguaje_ensamblador">Assembly</a>.</li>

<li><a href="http://www.romhacking.net/forum/index.php?topic=8052.0">foro romhacking.net</a> Hilo del Foro de la conocida web donde se habla del tema.</li>
<li><a href="http://www.atarihq.com/danb/emulation.shtml">atarihq.com</a> Un poco de varios sistemas emulados con fuentes y documentaci�n.</li>

<?php
include '../../piecdisq.php';
?>