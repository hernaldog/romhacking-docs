<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>Tutorial de creaci�n de un Emulador sencillo de Chip-8 con VS 2008 y C# (Parte 5)</title>
</head>

<body>
<small>
<span style="font-family: Verdana;">
<a href="http://darknromhacking.com/">Volver a P�gina Principal</a>
</small>

<hr style="width: 100%; height: 2px;">

<table width="100%">
<tr>
<td>

<H3>
<span style="font-weight: bold;">Tutorial de creaci�n de un Emulador sencillo de Chip-8 con VS 2008 y C# (Parte 5)</span>
</H3>
<small>
Lenguaje: C#<br>
Para: VS 2008 con Sdl.Net 6.1<br>
Por Dark-N: <a href="mailto:hernaldog@gmail.com">hernaldog@gmail.com</a>
<br>

<span style="font-family: Verdana;">
<a href="http://darknromhacking.com/">http://darknromhacking.com</a>
<br>
Hilo del Foro: <a href="http://foro.romhackhispano.org/viewtopic.php?f=4&t=872">
http://foro.romhackhispano.org/viewtopic.php?f=4&t=872</a>
</small></td>
<td align="center">
</table>



<hr style="width: 100%; height: 2px;">

<span style="font-family: Verdana;"><small>
<a href="emulador4.php">Lecci�n Anterior</a> | <a href="emulador_menu.php">�ndice</a> | <a href="emulador6.php">Siguiente Lecci�n</a>

<H3>La idea</H3>

La idea de este cap�tulo es realizar los cambios necesario para "convertir" la aplicaci�n en modo consola a modo gr�fico utilizando para ello la librer�a SDL.Net v6.1. Los cambios son menores, por lo que no ser� una lecci�n larga.
<br>Nota: en este cap�tulo asumo que antes te has leido al menos hasta el <a href="../sdl_net/SDL_NET_menu.htm">cap�tulo 2</a> de mi gu�a de Desarrollo de VideoJuegos con SDL.Net. Si no lo haz hecho, no entender�s algunos conceptos como Sprite, Surface, Blit, etc.
<br><br>Nota: A partir de este cap�tulo incluir� algunas im�genes explicatorias con Windows 7 as� que no se espanten. 

<H3>Adaptando el Proyecto</H3>

Lo primero es renombrar la carpeta si la tienes con otro nombre, la dejaremos como "Chip8_vs2008":
<br><br>
Antes
<br>
<img src="imag/chip8_renombre1.png">
<br>
Luego queda:
<br>
<img src="imag/chip8_renombre2.png">
<br><br>
Tambi�n adentro se debe cambiar el nombre de la carpeta a solo "Chip8":
<br>
<img src="imag/chip8_renombre3.png">
<br>
Ahora queda:
<br>
<img src="imag/chip8_renombre4.png">
<br><br>
Por �ltimo abrimos el archivo <b>chip8.sln</b> con un editor de texto y se debe cambiar la secci�n que dice "Chip8_ConsoleMode" a solo "Chip8" (igual que la carpeta, con sus may�sculas y min�sculas):
<br><br>
<img src="imag/chip8_renombre5.png">
<br>
Con esto estar�amos ok con el renombrado y si abrimos la soluci�n "chip8.sln" debe abrirse sin problemas. Si no abre, es que te qued� algo mal configurado. 
<br>Para dejar todo listo para empezar a depurar, debemos hacer un <b>Rebuild</b> de la soluci�n y este no debe arrojar errores:
<br>
<img src="imag/chip8_renombre6.png">
<br>
Si no lo haces, es posible que al tratar de depurar te arroje el error:
<br>
<img src="imag/chip8_renombre7.png">
<br><br>
Lo segundo es cambiar el tipo de salida del proyecto de tipo <b>console application</b> a tipo <b>windows application</b>:
<br><br>
<img src="imag/output_win_app.PNG">

<br><br>
Lo tercero es opcional pero v�lido, y es cambiar de nombre el Proyecto, de "Chip8_ConsoleMode" a "Chip8_SDLMode". Nota: esto solo cambia el nombre que se ve en el Visual Studio pero no el nombre de la carpeta f�sica.
<br><br>
<img src="imag/renombre_proyecto_sdl.PNG">

<br><br>
Si ya tienes instalado el <a href="http://sourceforge.net/projects/cs-sdl/files/">SDL.NET 6.1 SDK</a>, lo tercero es agregar la <b>referencia</b> a la DLL de SDl.NET: 
<br><br>
<img src="imag/agregar_referencia.PNG">
<br><br>

Lo mismo con la referencia a <b>System.Drawing</b> que se debe agregar para manejar los colores.

<br><br>
Lo cuarto es agregar los <b>Using</b> al proyecto que son de 2 tipos:<br><br>

A. Los using de SDL.Net:
<br><br>
<li><b>SdlDotNet.Core</b>: permite el manejo de Eventos como el Tick o Salir.</li>
<li><b>SdlDotNet.Graphics</b>: permite usar Surfaces.</li>
<li><b>SdlDotNet.Graphics.Sprites</b>: para el manejo de Sprites y textos en pantalla</li>
<li><b>SdlDotNet.Input</b>: para el manejo del teclado</li>

<br>
B. Los using de .Net que utilizarmos para aspectos gr�ficos son:
<br><br>
<li><b>System.Drawing</b>: permite el manejo de colores.</li>

<br>
Por lo tanto, los using finales, sumados los anteriores son:
<br><br>
<table><tr><td bgcolor="#E6E6E6"><pre>
using System;
using System.IO; //Librer�a de .Net para manejar archivos
using System.Runtime.InteropServices;  //para el "beep" de la Bios
using System.Drawing;  //Librer�a de .Net para manejar Colores

using SdlDotNet.Core; //Eventos como Teclado, FPS, etc.
using SdlDotNet.Graphics; //para Surfaces
using SdlDotNet.Graphics.Sprites;  //para Sprites y textos en pantalla
using SdlDotNet.Input; //para manejo de teclado
</pre></td></tr></table>


<H3>Cambiando el Namespace</H3>

Esto es opcional, pero que aporta para ordenarnos, y es renombrar el antiguo namespace "Chip8_ConsoleMode" por "Chip8".

<table><tr><td bgcolor="#E6E6E6"><pre>
Antes:

namespace Chip8_ConsoleMode
{
...
}

Ahora:

namespace Chip8
{
...
}
</pre></td></tr></table>


<H3>Nuevas variables de clase</H3>

1. Crearemos 1 variable <b>instruccion</b> que represente a un Opcode cualquiera y ser� de uso general.
<br>
2. Crearemos 2 variables <b>PixelNegro</b> y <b>PixelBlanco</b> que representar�n los pixels, ser�n de tipo Surface y cada una representar� un pixel blanco y otro negro.
<br>
3. Creamos una variable llamada <b>tamanoPixel</b> que contiene el tama�o del Pixel que uno ve en pantalla en 5 unidades o pixels reales, de otra forma el juego se ver�a muy peque�o en pantalla. Este valor se utilizar� en el constructor. 
<br>
4. Creamos 2 variables para el manejo del reloj del CPU, una llamada <b>operPorSegundo</b> y otra <b>operPorFrame</b>.
<br>
5. Creamos otra variable para el manejo del reloj del CPU, una llamada <b>TimeUntilTimerUpdate</b>.

<table><tr><td bgcolor="#E6E6E6"><pre>
int	opcodes;
private Surface PixelNegro;
private Surface PixelBlanco;
const int tamanoPixel = 5; // tama�o de pixel, sirve para zoom de todo el emulador
public const int operPorSegundo = 600;
public const int operPorFrame = operPorSegundo / 60;
private int TimeUntilTimerUpdate;  //para la simulaci�n de timers
</pre></td></tr></table>

<H3>Cambiando el m�todo Run()</H3>

Se debe cambiar el m�todo Run por un nuevo ya que antes Run se utilizaba para simular el <b>game-loop</b> del juego que nos permit�a recorrer las instrucciones y pintar en la consola, ahora ese trabajo lo llevar� el evento <b>Tick</b>, por lo tanto, en nuestro m�todo Run solo le indicamos que inicie los Eventos en el juego que estar�n declarados en el Constructor.

<table><tr><td bgcolor="#E6E6E6"><pre>
Antes:

void Run()
{			
	ResetHardware();
	
	if (CargarJuego() == true)
	{
		while (true) //game-loop
		{                    
			EjecutaFech();
			ManejaTimers();
		}
	}
}

Ahora:

// Inicializamos los Eventos
public void Run()
{
	Events.Run();
}
</pre></td></tr></table>


<H3>Agregando Constructor</H3>

Debemos agregar un <b>constructor</b> a la clase Emulador ya que aqu� se inicializar�n algunas variables globales.

<table><tr><td bgcolor="#E6E6E6"><pre>
/// <summary>
/// Constructor de la clase
/// </summary>
public Emulador()
{ 

}
</pre></td></tr></table>

<H3>Manejar los errores: bloque Try-Catch</H3>

Mejoraremos de inmediato el Constructor con un simple manejador de Errores, para esto usamos las clausulas <b>Try</b> y <b>Catch</b> de C# y mostrando en pantalla un mensaje de error. Nota: Este tutorial no es un curso de C# por lo que no explicar� en detalle como manejar los errores en C# o cual es el mejor patr�n para manejarlos. Por lo tanto, los pasos para hacer esto es:<br><br>
<li>Asegurarnos de tener la referencia a la Dll: <b>System.Windows.Forms.dll</b></li>
<li>Agregar el using respectivo: <b>using System.Windows.Forms;</b>
<li>Agregar el c�digo try y el catch monstrando con la funci�n <b>MessageBox.Show("Texto")</b> el error en si con <b>Exception.Message</b> y su traza con <b>Exception.StackTrace</b>:

<table><tr><td bgcolor="#E6E6E6"><pre>
/// <summary>
/// Constructor de la clase
/// </summary>
public Emulador()
{
	try
	{
		...		
	}
	catch (Exception ex)
	{
	    MessageBox.Show("Error General: " + ex.Message + "-" + ex.StackTrace);
	    Events.QuitApplication();
	}
}
</pre></td></tr></table>


<H3>Creando una Aplicaci�n SDL.Net</H3>

Como se explic� en el <a href="../sdl_net/SDL_NET_menu.htm">cap�tulo 2</a> de mi gu�a de Desarrollo de VideoJuegos con SDL.Net, por lo tanto lo primero que debemos tener en el constructor es el manejo de la pantalla con la resoluci�n y el color de fondo del Surface principal del emulador gracias al objeto <b>Video</b>, y los Frames por Segundo que correr� la aplicaci�n gracias al objeto <b>SdlDotNet.Core.Events</b>:

<li>Seteo de resoluci�n del Surfase o plano de fondo donde dibujaremos los pixels.</li>
<li>Seteo del color de fondo de todo el Surfase, por defecto negro.</li>
<li>Seteo de Frames por Segundo por defecto a 60.</li>

<table><tr><td bgcolor="#E6E6E6"><pre>

/// <summary>
/// Constructor de la clase
/// </summary>
public Emulador()
{
	try
	{
		Video.SetVideoMode(RES_X * tamanoPixel, RES_Y * tamanoPixel);
		Video.Screen.Fill(Color.Black);
		Events.Fps = 60;
		Events.TargetFps = 60;	
	}
	catch (Exception ex)
	{
		MessageBox.Show("Error General: " + ex.Message + "-" + ex.StackTrace);
	}
}
</pre></td></tr></table>

<H3>Dibujando un Pixel</H3>

Lo principal que haremos en el emulador "gr�fico" es que reemplazaremos los c�digos <b>ASCII</b> que utilizabamos para dibujar en modo Consola, por el dibujado de <b>pixels</b>. Por lo tanto, debemos saber como simularemos un pixel utilizando SDL.Net.
<br>
Un Pixel es un "Surface", es decir una superficie de un color dado, al cual se le puede cargar un imagen encima. En nuestro caso, como el emulador es monocrom�tico (solo tiene 2 colores), usaremos un pixel negro que se usa para el fondo y otro blanco para mostrar el juego en s�. Con esto solo necesitamos crear 2 Surfaces, uno para cada pixel.
<br><br>La forma de crear una Surface de un color y de ciertas dimensiones es:<br>

<table><tr><td bgcolor="#E6E6E6"><pre>
Surface miSurface = new Video.CreateRgbSurface(ancho, alto);
</pre></td></tr></table>

<br>
Ejemplo: Creaci�n de un gran pixel de 10 x 10 pixels, de color blanco. Nota: este ejemplo no es parte de nuestro emulador, solo es un ejemplo.
<table><tr><td bgcolor="#E6E6E6"><pre>
private Surface PixelBla;
...
PixelBla = Video.CreateRgbSurface(10, 10);
PixelBla.Fill(Color.White);
...
Video.Screen.Blit(PixelBla, Point destino);
</pre></td></tr></table>

<br>
En el emulador el pixel que uno ve en pantalla no puede ser de 1 pixel ya que se ver�a muy peque�o, haciendo imposible jugar, por lo que la idea es pintar pixels grandes (de 5 x 5 por ejemplo) para que se vea mejor.

<br><br>
Por lo tanto en nuestro emulador seteamos en el constructor las 2 variables "PixelNegro" y "PixelBlanco" que definimos en la clase:
<br><br>
<table><tr><td bgcolor="#E6E6E6"><pre>
/// <summary>
/// Constructor de la clase
/// </summary>
public Emulador()
{
	...
	PixelNegro = Video.CreateRgbSurface(multipPixel, multipPixel);
	PixelBlanco = Video.CreateRgbSurface(multipPixel, multipPixel);
	PixelNegro.Fill(Color.Black);
	PixelBlanco.Fill(Color.White);
	...  
}
</pre></td></tr></table>


<H3>Reset Harware</H3>

En el modo consola se cargaba con el m�todo <b>ResetHardware</b> en el m�todo <b>Run</b> de la forma:
<table><tr><td bgcolor="#E6E6E6"><pre>
void Run()
{			
	ResetHardware();
	if (CargarJuego() == true)
	{
	...
</pre></td></tr></table>

Ahora se debe hacer lo mismo en el Constructor.


En el m�todo en s� cambian peque�as cosas, por ejemplo ahora para limpiar la pantalla se usa el m�todo <b>Video.Screen.Fill(Color.Black)</b> para pintar todo el Surface de un color (se pinta en memoria) y enviar el resfresco del Video con <b>Video.Screen.Update()</b> para decirle al Surface que actualice los cambios en pantalla y sean visible para nosotros.

<table><tr><td bgcolor="#E6E6E6"><pre>
/// <summary>
/// Constructor de la clase
/// </summary>
public Emulador()
{
	...
	ResetHardware();
}

void ResetHardware()
{
    // Reseteamos los Timers
    delayTimer = 0x0;
    soundTimer = 0x0;

    // Reseteamos variables
    instruccion = 0x0;
    PC = DIR_INICIO;
    SP = 0x0;
    I = 0x0;

    // Limpiamos la memoria
    for (int i = 0; i < TAMANO_MEM; i++)
        memoria[i] = 0x0;


    // Limpiamos registros
    for (int i = 0; i < CANT_REGISTROS; i++)
        V[i] = 0x0;


    // Limpiamos el stack
    for (int i = 0; i < TAMANO_PILA; i++)
        pila[i] = 0x0;

    // Cargamos las fuentes en la memoria por ej, los marcadores del juego PONG esos "[0-0]", "[0-2]"
    for (int i = 0; i < 80; i++)
        memoria[i] = arregloFuentes[i];	

    Video.Screen.Fill(Color.Black);
    Video.Screen.Update();
}
</pre></td></tr></table>

<H3>Carga de la Rom</H3>

En el modo consola se cargaba con el m�todo <b>CargaJuego</b> en el m�todo <b>Run</b> de la forma:
<table><tr><td bgcolor="#E6E6E6"><pre>
void Run()
{			
	ResetHardware();
	if (CargarJuego() == true)
	{
	...
</pre></td></tr></table>

Ahora se debe cargar la Rom en el Constructor y debemos cambiar la forma de manejar los errores de carga del archivo, la mejora forma que se me ocurri� es con <b>throw new Exception(mensaje)</b> que hace que el handler padre (que est� implementado en el constructor y que muestra un MessageBox) lo procese y no lo hacemos aqu� mismo, es algo avanzado para quien no ha trabajado con varios niveles de Try-Catch pero <a href="http://msdn.microsoft.com/es-es/library/0yd65esw%28v=vs.90%29.aspx">leyendo</a> un poco se entiende:

<table><tr><td bgcolor="#E6E6E6"><pre>
/// <summary>
/// Constructor de la clase
/// </summary>
public Emulador()
{
	...
	CargarROM("PONG");
}

void CargarJuego(string nombreRom)
{            
    FileStream rom;

    try
    {
        rom = new FileStream(@nombreRom, FileMode.Open);

        if (rom.Length == 0)
        {
            throw new Exception("Error en la Carga de la Rom: ROM da�ada o vac�a");
        }

        // Comenzamos a cargar la rom a la memoria a partir de la dir 0x200
        for (int i = 0; i < rom.Length; i++)
            memoria[DIR_INICIO + i] = (byte)rom.ReadByte();	

        rom.Close();                
    }
    catch (Exception ex)
    {
        throw new Exception("Error en la Carga de la Rom: " + ex.Message);
    }
}
</pre></td></tr></table>



<H3>Eventos</H3>

Existen 4 eventos que declararemos en el constructor:
<br><br>
<li> El evento <b>Tecla Presionada</b>: Ocurre cuando se presiona una tecla o se mantiene presionada.y no se suelta.</li>
<li> El evento <b>Tecla Liberada</b>: Ocurre cuando se libera o suelta una tecla que se manten�a presionada.</li>
<li> El evento <b>Salir</b> que se gatilla cuando cerramos la aplicaci�n pinchando la "X" de la ventana en la parte superior derecha.</li>
<li> El evento <b>Tick</b> que es gatillado constantemente por la misma aplicaci�n y que nos permitir� ejecutar lo que se conoce como <b>game-loop</b> y por ende, el pintado constante de los gr�ficos ya que en los juegos cada tick equivale a un <b>frame</b>. Recordemos que por defecto se ejecutan <b>60 ticks</b> o <b>frames</b> por cada <b>segundo</b> que pasa, o lo que es lo mismo, 1 frame cada 0,0166666 segundos, o tambi�n, 1 frame cada 16 milisegundos aproximadamente. El m�todo que har� la emulaci�n se llama <b>EmulaFrame()</b>.</li>
<br>
Por lo tanto, dentro del constructor tenemos:

<table><tr><td bgcolor="#E6E6E6"><pre>
public Emulador()
{ 
	...
	//declaraci�n de Eventos
	Events.KeyboardDown += new EventHandler<KeyboardEventArgs>(this.Tecla_Presionada);
	Events.KeyboardUp += new EventHandler<KeyboardEventArgs>(this.Tecla_Liberada);
	Events.Quit += new EventHandler<QuitEventArgs>(this.Events_Salir);
	Events.Tick += new EventHandler<TickEventArgs>(this.Events_Tick);
	...
}
...
private void Events_Salir(object sender, QuitEventArgs e)
{
	Events.QuitApplication();
}

private void Events_Tick(object sender, TickEventArgs e)
{
	EmulaFrame();
}
...
</pre></td></tr></table>

<br><b>1. Eventos de Teclado</b><br><br>

Los computadores que inicialmente usaban Chip 8 ten�an la siguiente configuraci�n de teclado basado en numeros y teclas Hexadecimales:
<br><br>
<TABLE BORDER=1 CELLPADDING=3 CELLSPACING=0 ALIGN=left>
			<TR><TD><TT>1</TT></TD><TD><TT>2</TT></TD><TD><TT>3</TT></TD><TD><TT>C</TT></TD></TR>

			<TR><TD><TT>4</TT></TD><TD><TT>5</TT></TD><TD><TT>6</TT></TD><TD><TT>D</TT></TD></TR>
			<TR><TD><TT>7</TT></TD><TD><TT>8</TT></TD><TD><TT>9</TT></TD><TD><TT>E</TT></TD></TR>
			<TR><TD><TT>A</TT></TD><TD><TT>0</TT></TD><TD><TT>B</TT></TD><TD><TT>F</TT></TD></TR>

		</TABLE>

<br><br><br><br><br><br><br>

Por lo tanto el c�digo del evento de Tecla Presionada y Tecla Liberada quedar�a:

<br><br>
<table><tr><td bgcolor="#E6E6E6"><pre>
private void Tecla_Presionada(object sender, KeyboardEventArgs e)
{	
	//Salimos del juego con tecla Escape
	if(e.Key == Key.Escape)
	{
		Events.QuitApplication();
	}
	
	if(e.Key == Key.One) {teclasPresionadas[TECLA_1] = true;}
	if(e.Key == Key.Two) {teclasPresionadas[TECLA_2] = true;}
	if(e.Key == Key.Three) {teclasPresionadas[TECLA_3] = true;}
	if(e.Key == Key.Four) {teclasPresionadas[TECLA_4] = true;}
	if(e.Key == Key.Q) {teclasPresionadas[TECLA_Q] = true;}
	if(e.Key == Key.W) {teclasPresionadas[TECLA_W] = true;}
	if(e.Key == Key.E) {teclasPresionadas[TECLA_E] = true;}
	if(e.Key == Key.R) {teclasPresionadas[TECLA_R] = true;}
	if(e.Key == Key.A) {teclasPresionadas[TECLA_A] = true;}
	if(e.Key == Key.S) {teclasPresionadas[TECLA_S] = true;}
	if(e.Key == Key.D) {teclasPresionadas[TECLA_D] = true;}
	if(e.Key == Key.F) {teclasPresionadas[TECLA_F] = true;}
	if(e.Key == Key.Z) {teclasPresionadas[TECLA_Z] = true;}
	if(e.Key == Key.X) {teclasPresionadas[TECLA_X] = true;}
	if(e.Key == Key.C) {teclasPresionadas[TECLA_C] = true;}
	if(e.Key == Key.V) {teclasPresionadas[TECLA_V] = true;}
}

private void Tecla_Liberada(object sender, KeyboardEventArgs e)
{			
	if(e.Key == Key.One) {teclasPresionadas[TECLA_1] = false;}
	if(e.Key == Key.Two) {teclasPresionadas[TECLA_2] = false;}
	if(e.Key == Key.Three) {teclasPresionadas[TECLA_3] = false;}
	if(e.Key == Key.Four) {teclasPresionadas[TECLA_4] = false;}
	if(e.Key == Key.Q) {teclasPresionadas[TECLA_Q] = false;}
	if(e.Key == Key.W) {teclasPresionadas[TECLA_W] = false;}
	if(e.Key == Key.E) {teclasPresionadas[TECLA_E] = false;}
	if(e.Key == Key.R) {teclasPresionadas[TECLA_R] = false;}
	if(e.Key == Key.A) {teclasPresionadas[TECLA_A] = false;}
	if(e.Key == Key.S) {teclasPresionadas[TECLA_S] = false;}
	if(e.Key == Key.D) {teclasPresionadas[TECLA_D] = false;}
	if(e.Key == Key.F) {teclasPresionadas[TECLA_F] = false;}
	if(e.Key == Key.Z) {teclasPresionadas[TECLA_Z] = false;}
	if(e.Key == Key.X) {teclasPresionadas[TECLA_X] = false;}
	if(e.Key == Key.C) {teclasPresionadas[TECLA_C] = false;}
	if(e.Key == Key.V) {teclasPresionadas[TECLA_V] = false;}
}
</pre></td></tr></table>


<br><b>2. Emulaci�n de Lectura de Cada Instrucci�n o Game-Loop</b><br><br>

Aqu� hacemos pocos cambios respecto al emulador en modo Consola ya que antes ten�amos:

<table><tr><td bgcolor="#E6E6E6"><pre>
void Run()
{
    ResetHardware();

    if (CargarJuego() == true)
    {
        while (true) //game-loop
        {
            EjecutaFech();            
        }
    }
}

void EjecutaFech()
{
	#region lectura de instrucciones
	
	// leemos cada una de las instrucciones desde la memoria. 
	// cada instruccion es de 2 bytes
	instruccion = memoria[PC] << 8 | memoria[PC + 1];
	...
</pre></td></tr></table>

<br>
Aqu� debemos hacer 4 cosas:<br><br>

1. creamos el m�todo <b>EmulaFrame()</b> que se encargar� de leer cada opcode de la memoria<br>
<table><tr><td bgcolor="#E6E6E6"><pre>
public void EmulaFrame() //representa un frame
{
    //por cada Tick o Frame, se ejecutan 600/60=10 instrucciones
    for (int i = 0; i < operPorFrame; i++)
    {
        EmulaOpcodes();
    }
}
</pre></td></tr></table>

<br>
2. creamos el m�todo <b>EmulaOpcodes()</b> que maneja los <b>Timers</b> de <b>Delay</b> y de <b>Tiempo</b> y que luego se encarga de llamar al m�todo que procesa los opcodes <b>EjecutaOpcodes()</b>:<br>

<table><tr><td bgcolor="#E6E6E6"><pre>
void EmulaOpcodes()
{
	if (TimeUntilTimerUpdate == 0)
	{
		if (delayTimer > 0)
			delayTimer--;
		
		if (soundTimer > 0) 
			soundTimer--;
		
		TimeUntilTimerUpdate = operPorFrame;
	}
	else
	{
		TimeUntilTimerUpdate--;
	}
	
	EjecutaOpcodes();
}
</pre></td></tr></table>

<br>
3. renombramos el nombre del m�todo <b>EjecutaFech()</b> por uno mas correcto <b>EjecutaOpcodes()</b><br>

<br>
4. cambiamos un poco el contenido del m�todo que limpia la pantalla <b>ClearScreen()</b>.

<br><br>Antes:
<table><tr><td bgcolor="#E6E6E6"><pre>
void ClearScreen()
{
    // No se limpia la pantalla directamente, sino el arreglo que la representa
    // asignandole un valor 0
	for (int p =0; p < RES_X; p++)
		for (int q = 0; q < RES_Y; q++)
			arregloPantalla[p,q] = 0;					
}
</pre></td></tr></table>
Ahora:
<table><tr><td bgcolor="#E6E6E6"><pre>
void ClearScreen()
{
	Video.Screen.Fill(Color.Black);
	Video.Screen.Update();
	
	for (int p =0; p < RES_X; p++)
		for (int q = 0; q < RES_Y; q++)
			arregloPantalla[p,q] = 0;
}
</pre></td></tr></table>

<br>
5. cambiamos el m�todo de realiza los gr�ficos <b>DrawSprite()</b>. Lo m�s importante aqu� es:
<br><br>
<li>el uso de <b>Video.Screen.Blit()</b> para el puntado de objetos sobre el Surface (los objetos se pintan en memoria).</li>
<li>usamos <b>Video.Screen.Update()</b> para decirle al Surface que actualice los cambios en pantalla.</li>
<li>No se utiliza el m�todo <b>ActualizarPantalla()</b> por lo que debemos eliminarlo. </li>
<li>Se deja un "if" inicial para la futura emulaci�n del <b>Super Chip</b> que explicado en forma simple, ser�a como un Chip-8 con mejoras permitiendo jugar m�s y mejores Roms.</li>

<br>
Antes ten�amos:
<table><tr><td bgcolor="#E6E6E6"><pre>
void DrawSprite()
{
	// En Chip8 un Sprite es de 8xN
	int largoSpriteX = 8;

	int dataLeida, tempx, tempy = 0;

	// Se resetea la detecci�n de colisi�n
	V[0xF] = 0x0;

	for (int lineaY = 0; lineaY < opcode4; lineaY++)
	{
		// Leemos un byte de memoria
		dataLeida = memoria[I + lineaY];

		for (int pixelX = 0; pixelX < largoSpriteX; pixelX++)
		{
			if ((dataLeida & (0x80 >> pixelX)) != 0)
			{
				// Chequeamos por alguna colisi�n (pixel sobrescrito)
				tempx = (V[opcode2] + pixelX) % 64;
				tempy = (V[opcode3] + lineaY) % 32;

				//% Resto o modulo de una division, ejemplo 32%64=32, 0%32=0, 1%32=1
				if (arregloPantalla[tempx, tempy] == 1)  
					V[0xF] = 1;

				// Dibujamos el el arreglo un 1, luego mas abajo pintamos graficamente el arreglo
				arregloPantalla[tempx, tempy] ^= 1;  //XOR 0^1=1, 1^0=1, 1^1 =0, 0^0=0
			} 
		}
	}			

	// Actualiza lo que se ve en pantalla con los valores del arreglo
	ActualizarPantalla();
}
</pre></td></tr></table>

Ahora es:
<table><tr><td bgcolor="#E6E6E6"><pre>
/// <summary>
/// Screen is 64x62 pixels
/// Todos los drawings son hechos en modos XOR. 
/// Cuando uno o mas pixels son borrados mientras un sprite es pintado, 
/// el registro VF se setea en 01, sino queda en 00.
/// </summary>
void DrawSprite()
{
    // Reseteamos el registro que detecta colisiones
    V[0xF] = 0x0;

    if ((instruccion & 0x000F) == 0) //opcode & 0x000F =opcode4
    {
        // Dibujamos un Sprite de SCHIP8 de tama�o 16x16
        // No implementado a�n
    }
    else
    {
        // Bibujamos un Sprite de CHIP8 de tama�o 8xN				
        for (int spriY = 0; spriY < opcode4; spriY++)
        {
            for (int spriX = 0; spriX < 8; spriX++)
            {
                int x = (memoria[I + spriY] & (0x80 >> spriX));
                if (x != 0)
                {
                    // checkeamos por alguna colision
                    int xx = (V[opcode2] + spriX);
                    int yy = (V[opcode3] + spriY);

                    if (arregloPantalla[xx % 64, yy % 32] == 1)
                    {
                        arregloPantalla[xx % 64, yy % 32] = 0;
                        Video.Screen.Blit(PixelNegro, new Point((xx % 64) * tamanoPixel, (yy % 32) * tamanoPixel));
                        V[0xF] = 1; //colision activado
                    }
                    else
                    {
                        arregloPantalla[xx % 64, yy % 32] = 1;
                        Video.Screen.Blit(PixelBlanco, new Point((xx % 64) * tamanoPixel, (yy % 32) * tamanoPixel));
                    }
                }
            }
        }
    }
    Video.Screen.Update();
}
</pre></td></tr></table>

<br>

Finalmente si hacemos un <b>Rebuild</b>, deber�a compilar ok y �podemos ver el resultado con SDL.Net!, adem�s si presionas la <b>tecla q</b> la paleta izquierda deber�a bajar.
<br><br>

<hr style="width: 100%; height: 2px;">

C�digo fuente completo de todo lo que llevamos hasta ahora (C# 2.0):
<br><br>
<table><tr><td bgcolor="#E6E6E6"><pre>
using System;
using System.IO; //Librer�a de .Net para manejar archivos
using System.Runtime.InteropServices;  //para el "beep" de la Bios
using System.Drawing;  //Librer�a de .Net para manejar Colores
using System.Windows.Forms; //Para enviar mensajes en ventanas de dialogos

using SdlDotNet.Core; //Eventos
using SdlDotNet.Graphics; //para Surfaces
using SdlDotNet.Graphics.Sprites;  //para Sprites y textos en pantalla
using SdlDotNet.Input; //para manejo de teclado


namespace Chip8
{
	class Emulador
	{     

		#region variables emulador

		//variables principales
        const int DIR_INICIO        = 0x200;
		const int TAMANO_MEM        = 0xFFF;
		const int TAMANO_PILA       = 16;
		const int CANT_REGISTROS    = 16;

        //arreglo que representa la memoria
        int[] memoria = new int[TAMANO_MEM];

        //Arreglo que representa los 16 Registros (V) 
        int[] V = new int[CANT_REGISTROS];

        //arreglo que representa la Pila
        int[] pila = new int[TAMANO_PILA];

        //variables que representan registros varios
        int instruccion;  //representa una instruccion del Chip-8. Tiene largo 2 byte
        int PC;
        int I;
        int SP;      
        int KK;
		
        //resolucion de pantalla 64x32 (mas alta que larga)
        const int RES_X	= 64;
		const int RES_Y	= 32;
        int[,] arregloPantalla = new int[RES_X, RES_Y];
		
        //variables para manejar los opcodes
		int opcode1 = 0;
		int opcode2 = 0;  //X
		int opcode3 = 0;  //Y
		int opcode4 = 0;
        int NNN = 0;
		
        //variables que representan los 2 timers: Delay Timer y Sound Timer
		int	delayTimer;
		int	soundTimer;

        //variables para el manejo de fuentes (80 bytes, ya que hay 5 bytes x caracter 
        //y son 16 caracteres o letras (5x16=80). Cada font es de 4x5 bits. 
		int[] arregloFuentes = {
		   0xF0, 0x90, 0x90, 0x90, 0xF0,	// valores para 0
		   0x60, 0xE0, 0x60, 0x60, 0xF0,	// valores para 1
		   0x60, 0x90, 0x20, 0x40, 0xF0,	// valores para 2
		   0xF0, 0x10, 0xF0, 0x10, 0xF0,	// valores para 3
		   0x90, 0x90, 0xF0, 0x10, 0x10,	// valores para 4
		   0xF0, 0x80, 0x60, 0x10, 0xE0,	// valores para 5
		   0xF0, 0x80, 0xF0, 0x90, 0xF0,	// valores para 6
		   0xF0, 0x10, 0x10, 0x10, 0x10,	// valores para 7
		   0xF0, 0x90, 0xF0, 0x90, 0xF0,	// valores para 8
		   0xF0, 0x90, 0xF0, 0x10, 0x10,	// valores para 9
		   0x60, 0x90, 0xF0, 0x90, 0x90,	// valores para A
		   0xE0, 0x90, 0xE0, 0x90, 0xE0,	// valores para B
		   0x70, 0x80, 0x80, 0x80, 0x70,	// valores para C
		   0xE0, 0x90, 0x90, 0x90, 0xE0, 	// valores para D
		   0xF0, 0x80, 0xF0, 0x80, 0xF0,	// valores para E
		   0xF0, 0x90, 0xF0, 0x80, 0x80		// valores para F
		};

        private bool[] teclasPresionadas = { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false };
		
        const int TECLA_1	= 0;
		const int TECLA_2	= 1;
		const int TECLA_3	= 2;
		const int TECLA_4	= 3;
		const int TECLA_Q	= 4;
		const int TECLA_W	= 5;
		const int TECLA_E	= 6;
		const int TECLA_R	= 7;
		const int TECLA_A	= 8;
		const int TECLA_S	= 9;
		const int TECLA_D	= 10;
		const int TECLA_F	= 11;		
		const int TECLA_Z	= 12;
		const int TECLA_X	= 13;
		const int TECLA_C	= 14;
		const int TECLA_V	= 15;
        
        // mapeamos las 16 teclas de Chip8
        private byte[] MapeoTeclas = 
        {	       
            0x01,0x02,0x03,0x0C,
			0x04,0x05,0x06,0x0D,
			0x07,0x08,0x09,0x0E,
			0x0A,0x00,0x0B,0x0F 
        };

        //[DllImport("Kernel32.dll")] //para beep
        //public static extern bool Beep(UInt32 frequency, UInt32 duration);
        //En Framework 2.0 se puede usar directamente: Console.Beep(350, 50);

        [DllImport("msvcrt.dll")] //Framwork 1.1: para cls (clear screen) en DOS
        public static extern int system(string cmd);
        //En Framework 2.0 se puede usar directamente: Console.Clear();

        //Variable de tipo Random (para generar n�meros aleatoios) utilizada por ciertas instrucciones
        Random rnd = new Random();
       
        private Surface PixelNegro;
        private Surface PixelBlanco;
        const int tamanoPixel = 5; // tama�o de pixel, sirve para zoom de todo el emulador
        public const int operPorSegundo = 600;
        public const int operPorFrame = operPorSegundo / 60;
        private int TimeUntilTimerUpdate;  //para la simulaci�n de timers

        #endregion
           
        		
        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public Emulador()
        {
            try
            {
                Video.SetVideoMode(RES_X * tamanoPixel, RES_Y * tamanoPixel);
                Video.Screen.Fill(Color.Black);
                Events.Fps = 60;
                Events.TargetFps = 60;

                PixelNegro = Video.CreateRgbSurface(tamanoPixel, tamanoPixel);
                PixelBlanco = Video.CreateRgbSurface(tamanoPixel, tamanoPixel);
                PixelNegro.Fill(Color.Black);
                PixelBlanco.Fill(Color.White);

                //declaraci�n de Eventos
                Events.KeyboardDown += new EventHandler<KeyboardEventArgs>(this.Tecla_Presionada);
                Events.KeyboardUp += new EventHandler<KeyboardEventArgs>(this.Tecla_Liberada);
                Events.Quit += new EventHandler<QuitEventArgs>(this.Events_Salir);
                Events.Tick += new EventHandler<TickEventArgs>(this.Events_Tick);

                ResetHardware();
                CargarJuego("PONG");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error General: " + ex.Message + "-" + ex.StackTrace);                
            }
        }

		static void Main(string[] args)
		{
            Emulador emulador = new Emulador();	
			emulador.Run();	
		}


        private void Events_Salir(object sender, QuitEventArgs e)
        {
            Events.QuitApplication();
        }

        private void Tecla_Presionada(object sender, KeyboardEventArgs e)
        {
            //Salimos del juego con tecla Escape
            if (e.Key == Key.Escape)
            {
                Events.QuitApplication();
            }

            if (e.Key == Key.One) { teclasPresionadas[TECLA_1] = true; }
            if (e.Key == Key.Two) { teclasPresionadas[TECLA_2] = true; }
            if (e.Key == Key.Three) { teclasPresionadas[TECLA_3] = true; }
            if (e.Key == Key.Four) { teclasPresionadas[TECLA_4] = true; }
            if (e.Key == Key.Q) { teclasPresionadas[TECLA_Q] = true; }
            if (e.Key == Key.W) { teclasPresionadas[TECLA_W] = true; }
            if (e.Key == Key.E) { teclasPresionadas[TECLA_E] = true; }
            if (e.Key == Key.R) { teclasPresionadas[TECLA_R] = true; }
            if (e.Key == Key.A) { teclasPresionadas[TECLA_A] = true; }
            if (e.Key == Key.S) { teclasPresionadas[TECLA_S] = true; }
            if (e.Key == Key.D) { teclasPresionadas[TECLA_D] = true; }
            if (e.Key == Key.F) { teclasPresionadas[TECLA_F] = true; }
            if (e.Key == Key.Z) { teclasPresionadas[TECLA_Z] = true; }
            if (e.Key == Key.X) { teclasPresionadas[TECLA_X] = true; }
            if (e.Key == Key.C) { teclasPresionadas[TECLA_C] = true; }
            if (e.Key == Key.V) { teclasPresionadas[TECLA_V] = true; }
        }

        private void Tecla_Liberada(object sender, KeyboardEventArgs e)
        {
            if (e.Key == Key.One) { teclasPresionadas[TECLA_1] = false; }
            if (e.Key == Key.Two) { teclasPresionadas[TECLA_2] = false; }
            if (e.Key == Key.Three) { teclasPresionadas[TECLA_3] = false; }
            if (e.Key == Key.Four) { teclasPresionadas[TECLA_4] = false; }
            if (e.Key == Key.Q) { teclasPresionadas[TECLA_Q] = false; }
            if (e.Key == Key.W) { teclasPresionadas[TECLA_W] = false; }
            if (e.Key == Key.E) { teclasPresionadas[TECLA_E] = false; }
            if (e.Key == Key.R) { teclasPresionadas[TECLA_R] = false; }
            if (e.Key == Key.A) { teclasPresionadas[TECLA_A] = false; }
            if (e.Key == Key.S) { teclasPresionadas[TECLA_S] = false; }
            if (e.Key == Key.D) { teclasPresionadas[TECLA_D] = false; }
            if (e.Key == Key.F) { teclasPresionadas[TECLA_F] = false; }
            if (e.Key == Key.Z) { teclasPresionadas[TECLA_Z] = false; }
            if (e.Key == Key.X) { teclasPresionadas[TECLA_X] = false; }
            if (e.Key == Key.C) { teclasPresionadas[TECLA_C] = false; }
            if (e.Key == Key.V) { teclasPresionadas[TECLA_V] = false; }
        }


        private void Events_Tick(object sender, TickEventArgs e)
        {
            EmulaFrame();
        }

        // Inicializamos los Eventos
        public void Run()
        {
            Events.Run();
        }

        void ResetHardware()
        {
            // Reseteamos los Timers
            delayTimer = 0x0;
            soundTimer = 0x0;

            // Reseteamos variables
            instruccion = 0x0;
            PC = DIR_INICIO;
            SP = 0x0;
            I = 0x0;

            // Limpiamos la memoria
            for (int i = 0; i < TAMANO_MEM; i++)
                memoria[i] = 0x0;


            // Limpiamos registros
            for (int i = 0; i < CANT_REGISTROS; i++)
                V[i] = 0x0;


            // Limpiamos el stack
            for (int i = 0; i < TAMANO_PILA; i++)
                pila[i] = 0x0;

            // Cargamos las fuentes en la memoria por ej, los marcadores del juego PONG esos "[0-0]", "[0-2]"
            for (int i = 0; i < 80; i++)
                memoria[i] = arregloFuentes[i];	

            Video.Screen.Fill(Color.Black);
            Video.Screen.Update();
        }


        void ManejaTimers()
        { 
            if (delayTimer > 0) 
                delayTimer--;
            if (soundTimer > 0)
                soundTimer--;
        }

        void CargarJuego(string nombreRom)
		{            
            FileStream rom;

            try
            {
                rom = new FileStream(@nombreRom, FileMode.Open);

                if (rom.Length == 0)
                {
                    throw new Exception("Error en la Carga de la Rom: ROM da�ada o vac�a");
                }

                // Comenzamos a cargar la rom a la memoria a partir de la dir 0x200
                for (int i = 0; i < rom.Length; i++)
                    memoria[DIR_INICIO + i] = (byte)rom.ReadByte();	

                rom.Close();                
            }
            catch (Exception ex)
            {
                throw new Exception("Error en la Carga de la Rom: " + ex.Message);
            }
		}

        public void EmulaFrame() //representa un frame
        {
            //por cada Tick o Frame, se ejecutan 600/60=10 instrucciones
            for (int i = 0; i < operPorFrame; i++)
            {
                EmulaOpcodes();
            }
        }

        void EmulaOpcodes()
        {
            if (TimeUntilTimerUpdate == 0)
            {
                if (delayTimer > 0)
                    delayTimer--;

                if (soundTimer > 0) 
                	soundTimer--;

                TimeUntilTimerUpdate = operPorFrame;
            }
            else
            {
                TimeUntilTimerUpdate--;
            }

            EjecutaOpcodes();
        }


        void EjecutaOpcodes()
		{
            #region lectura de instrucciones 

            // leemos cada una de las instrucciones desde la memoria. 
            // cada instruccion es de 2 bytes
            instruccion = memoria[PC] << 8 | memoria[PC + 1];

            // dejamos incrementado el Program Counter en 2, lista para leer 
            // la siguiente instruccion en el siguiente ciclo.
            PC += 2;

            #endregion 

            #region extracci�n de opcodes

            //obtengo el valor del registro KK, de largo 1 byte, el m�s chico de la instrucci�n
            KK = (instruccion & 0x00FF);

			// cada opcode es de 4 bit
            opcode1 = (instruccion & 0xF000) >> 12; //los 4 bits mayores de la instrucci�n
            opcode2 = (instruccion & 0x0F00) >> 8;  //X
            opcode3 = (instruccion & 0x00F0) >> 4;  //Y
            opcode4 = (instruccion & 0x000F) >> 0;  //Opcode N = los 4 bits menores de la instrucci�n

            //obtengo el valor del opcode NNN
            NNN = (instruccion & 0x0FFF);

            #endregion

            #region ejecuci�n de instrucciones

            // Ejecutamos las instrucciones a travez de los opcodes
			switch (opcode1)
			{
				// opcodes del tipo 0xxx
				case (0x0):
				{
                    switch (instruccion)
					{
						// opcode 00E0: Clear Screen.
						case (0x00E0):
						{
							ClearScreen();
							break;
						}
						// opcode 00EE: Return From Subroutine.
						case (0x00EE):
						{
							ReturnFromSub();
							break;
						}
					}
					break;
				}
				// opcodes del tipo 1xxx
				case (0x1):
				{
					// opcode 1NNN: Jump To Address NNN.
					JumpToAddr();
					break;
				}
                // opcodes del tipo 2xxx
				case (0x2):
				{
					// opcode 2NNN: Call Subroutine At Address NNN.
					CallSub();
					break;
				}
                // opcodes del tipo 3xxx
				case (0x3):
				{
					// opcode 4XKK: Skip Next Instruction If VX == KK
					SkipIfEql();
					break;
				}
                // opcodes del tipo 4xxx
				case (0x4):
				{
					// opcode 4XKK: Skip Next Instruction If VX != KK
					SkipIfNotEql();
					break;
				}
                // opcodes del tipo 5xxx
				case (0x5):
				{
					// opcode 5XY0: Skip Next Instruction If VX == VY
					SkipIfRegEql();
					break;
				}
                // opcodes del tipo 6xxx
				case (0x6):
				{
					// opcode 6XKK: Assign Number KK To Register X.
					AssignNumToReg();
					break;
				}
                // opcodes del tipo 7xxx
				case (0x7):
				{
					// opcode 7XKK: Add Number KK To Register X.
					AddNumToReg();
					break;
				}
                // opcodes del tipo 8xxx
				case (0x8):
				{
					//Tenemos varios tipos
					switch (opcode4)
					{
						// opcode 8XY0: Assign From Register To Register.
						case (0x0):
						{
							AssignRegToReg();
							break;
						}
						// opcode 8XY1: Bitwise OR Between Registers.
						case (0x1):
						{
							RegisterOR();
							break;
						}
						// opcode 8XY2: Bitwise AND Between Registers.
						case (0x2):
						{
							RegisterAND();
							break;
						}
						// opcode 8XY3: Bitwise XOR Between Registers.
						case (0x3):
						{
							RegisterXOR();
							break;
						}
						// opcode 8XY4: Add Register To Register.
						case (0x4):
						{
							AddRegToReg();
							break;
						}
						// opcode 8XY5: Sub Register From Register.
						case (0x5):
						{
							SubRegFromReg();
							break;
						}
						// opcode 8XY6: Shift Register Right Once.
						case (0x6):
						{
							ShiftRegRight();
							break;
						}
						// opcode 8XY7: Sub Register From Register (Reverse Order).
						case (0x7):
						{
							ReverseSubRegs();
							break;
						}
						// opcode 8XYE: Shift Register Left Once.
						case (0xE):
						{
							ShiftRegLeft();
							break;
						}
					}
					break;
				}
                // opcodes del tipo 9xxx
				case (0x9):
				{
					// opcode 9XY0: Skip Next Instruction If VX != VY
					SkipIfRegNotEql();
					break;
				}
				// opcodes del tipo AXXX
				case (0xA):
				{
					// opcode ANNN: Set Index Register To Address NNN.
					AssignIndexAddr();
					break;
				}
                // opcodes del tipo BXXX
				case (0xB):
				{
					// opcode BNNN: Jump To NNN + V0.
					JumpWithOffset();
					break;
				}
                // opcodes del tipo CXXX
				case (0xC):
				{
					// opcode CXKK: Assign Bitwise AND Of Random Number & KK To Register X.
					RandomANDnum();
					break;
				}
                // opcodes del tipo DXXX
				case (0xD):
				{
					// opcode DXYN: Draw Sprite To The Screen.
					DrawSprite();
					break;
				}
                // opcodes del tipo EXXX
				case (0xE):
				{
					// Tenemos 2 tipos seg�n EXKK
					switch (KK)
					{
						// opcode EX9E: Skip Next Instruction If Key In VX Is Pressed.
						case (0x9E):
						{
							SkipIfKeyDown();
							break;
						}
						// opcode EXA1: Skip Next Instruction If Key In VX Is NOT Pressed.
						case (0xA1):
						{
							SkipIfKeyUp();
							break;
						}
					}
					break;
				}
                // opcodes del tipo FXXX
				case (0xF):
				{
					// tenemos varios tipos de Opcodes
					switch (KK)
					{
						// opcode FX07: Assign Delay Timer To Register.
						case (0x07):
						{
							AssignFromDelay();
							break;
						}
						// opcode FX0A: Wait For Keypress And Store In Register.
						case (0x0A):
						{
							StoreKey();
							break;
						}
						// opcode FX15: Assign Register To Delay Timer.
						case (0x15):
						{
							AssignToDelay();
							break;
						}
						// opcode FX18: Assign Register To Sound Timer.
						case (0x18):
						{
							AssignToSound();
							break;
						}
						// opcode FX1E: Add Register To Index.
						case (0x1E):
						{
							AddRegToIndex();
							break;
						}
						// opcode FX29: Index Points At CHIP8 Font Char In Register.
						case (0x29):
						{
							IndexAtFontC8();
							break;
						}
						// opcode FX30: Index Points At SCHIP8 Font Char In Register.
						case (0x30):
						{
							IndexAtFontSC8();
							break;
						}
						// opcode FX33: Store BCD Representation Of Register In Memory.
						case (0x33):
						{
							StoreBCD();
							break;
						}
						// opcode FX55: Save Registers To Memory.
						case (0x55):
						{
							SaveRegisters();
							break;
						}
						// opcode FX65: Load Registers From Memory.
						case (0x65):
						{
							LoadRegisters();
							break;
						}
					}
					break;
				}
			}
            
            #endregion
        }

        #region implementaci�n de m�todos para cada instrucci�n

        void ClearScreen()
        {
            Video.Screen.Fill(Color.Black);
            Video.Screen.Update();

            for (int p = 0; p < RES_X; p++)
                for (int q = 0; q < RES_Y; q++)
                    arregloPantalla[p, q] = 0;
        }

		void ReturnFromSub()
		{
			// Se diminuye el SP en 1
			SP--;

			// Apuntamos el PC (que apunta a la sgte. instrucci�n a ejecutar) a la
            // posici�n salvada en la Pila
			PC = pila[SP];
		}

		void JumpToAddr()
		{
			// salta a la instrucci�n dada. No se salta directamente, sino que se le indica
            // al PC que vaya a dicha direccion luego de salir del actual ciclo.
            PC = NNN;
		}

		void CallSub()
		{
			// Salva la posicion actual del PC en la Pila, para volver a penas se ejecute la subrutina
			pila[SP] = PC;
			SP++;

			// Saltamos a la subrutina indicada en NNN
            PC = NNN;
		}

		void SkipIfEql()
		{
			// Recordar que Opcode2=X
			if (V[opcode2] == KK)
			{
				// Salta a la siguiente instruccion
				PC += 2;
			}
		}

		void SkipIfNotEql()
		{
			if (V[opcode2] != KK)
			{
                // Salta a la siguiente instruccion
				PC += 2;
			}
		}

		void SkipIfRegEql()
		{
			if (V[opcode2] == V[opcode3])
			{
                // Salta a la siguiente instruccion
				PC += 2;
			}
		}

		void AssignNumToReg()
		{
			V[opcode2] = KK;
		}

	
		/**
		entrega los 8 bits menores (char) de un numero de 16 bits (int)

		@param number el numero de 16 bit
		@return el numero de 8 bits
		**/
		private char getLower(int number)
		{
			return (char)(number&0xFF);
		}

		void AddNumToReg()
		{
			V[opcode2] += KK;
		}

		void AssignRegToReg()
		{
			//VX = VY
			V[opcode2] = V[opcode3];
		}

		void RegisterOR()
		{
			// OR binario es |, entonces hacemos VX = VX | VY o mas elegante VX |= VY
			V[opcode2] |= V[opcode3];
		}

		void RegisterAND()
		{
            // OR binario es &, entonces hacemos VX = VX & VY o mas elegante VX &= VY
			V[opcode2] &= V[opcode3];
		}

		void RegisterXOR()
		{
            // XOR es ^, entonces hacemos VX = VX ^ VY o mas elegante VX ^= VY
			V[opcode2] ^= V[opcode3];
		}

		void AddRegToReg()
		{
			//Con >> extraemos los 8 bits mayores de la suma, si el resultado supera
            //los 8 bits el >> 8 dara 1 si no 0
			V[0xF] = (V[opcode2] + V[opcode3]) >> 8;

			//VX = VX + VY
			V[opcode2] += V[opcode3];
		}

		void SubRegFromReg()
		{
			//seteamos F en 1 si VX > VY
			if (V[opcode2] >= V[opcode3])
				V[0xF] = 0x1;
			else
				V[0xF] = 0x0;

			//VX = VX - VY
			V[opcode2] -= V[opcode3];
		}

		void ShiftRegRight()
		{
            //VF = VX AND 1 (VF valdr� 1 o 0). Para este case es m�s optimo
			V[0xF] = V[opcode2] & 0x1;
            //Manera elegante de escribir un shift a la derecha para dividir por 2: V[opcode2] = V[opcode2] >> 1;
			V[opcode2] >>= 1;
		}

		void ReverseSubRegs()
		{
			if (V[opcode2] <= V[opcode3])
			{
				V[0xF] = 0x1;
			}
			else
			{
				V[0xF] = 0x0;
			}

            V[opcode2] = V[opcode3] - V[opcode2];
		}

		void ShiftRegLeft()
		{
            //VF = VX AND 10 hex
			V[0xF] = V[opcode2] & 0x10;
            //Manera elegante de escribir un shift a la izquierda para multiplicar por 2: V[opcode2] = V[opcode2] << 1;
			V[opcode2] <<= 1;
		}

		void SkipIfRegNotEql()
		{			
			if (V[opcode2] != V[opcode3])
			{
				//Aumentamos el PC en 2 para saltar a la siguiente instrucci�n
				PC += 2;
			}
		}

		void AssignIndexAddr()
		{
			// se setea el Registro de �ndice (I) a la direcci�n NNN.
            I = NNN;
		}

		void JumpWithOffset()
		{
            PC = NNN + V[0x0];
		}	

		void RandomANDnum()
		{
            //usamos el variable rnd seteada en la clase. Con el m�todo Next se le puede dar el m�nimo (0) y m�ximo (255)
            int numeroRnd = rnd.Next(0,255);
            V[opcode2] = numeroRnd & KK;
		}

        /// <summary>
        /// Screen is 64x62 pixels
        /// Todos los drawings son hechos en modos XOR. 
        /// Cuando uno o mas pixels son borrados mientras un sprite es pintado, 
        /// el registro VF se setea en 01, sino queda en 00.
        /// </summary>
        void DrawSprite()
        {
            // Reseteamos el registro que detecta colisiones
            V[0xF] = 0x0;

            if ((instruccion & 0x000F) == 0) //opcode & 0x000F =opcode4
            {
                // Dibujamos un Sprite de SCHIP8 de tama�o 16x16
                // No implementado a�n
            }
            else
            {
                // Bibujamos un Sprite de CHIP8 de tama�o 8xN				
                for (int spriY = 0; spriY < opcode4; spriY++)
                {
                    for (int spriX = 0; spriX < 8; spriX++)
                    {
                        int x = (memoria[I + spriY] & (0x80 >> spriX));
                        if (x != 0)
                        {
                            // checkeamos por alguna colision
                            int xx = (V[opcode2] + spriX);
                            int yy = (V[opcode3] + spriY);

                            if (arregloPantalla[xx % 64, yy % 32] == 1)
                            {
                                arregloPantalla[xx % 64, yy % 32] = 0;
                                Video.Screen.Blit(PixelNegro, new Point((xx % 64) * tamanoPixel, (yy % 32) * tamanoPixel));
                                V[0xF] = 1; //colision activado
                            }
                            else
                            {
                                arregloPantalla[xx % 64, yy % 32] = 1;
                                Video.Screen.Blit(PixelBlanco, new Point((xx % 64) * tamanoPixel, (yy % 32) * tamanoPixel));
                            }
                        }
                    }
                }
            }
            Video.Screen.Update();
        }

		void SkipIfKeyDown()
		{
            if (teclasPresionadas[MapeoTeclas[V[opcode2]]] == true)
				PC += 2;		
		}

		void SkipIfKeyUp()
		{
            if (teclasPresionadas[MapeoTeclas[V[opcode2]]] == false)
				PC += 2;
		}

		void AssignFromDelay()
		{
			V[opcode2] = delayTimer;
		}

		void StoreKey()
		{
            for (int i = 0; i < teclasPresionadas.Length; i++)
			{
                if (teclasPresionadas[i] == true)
				{
					V[opcode2] = i;
				}
			}
		}

		void AssignToDelay()
		{
			delayTimer = V[opcode2];
		}

		void AssignToSound()
		{
			soundTimer = V[opcode2];
		}

		void AddRegToIndex()
		{
			I += V[opcode2];
		}

		void IndexAtFontC8()
		{
			I = (V[opcode2] * 0x5);
		}

		void IndexAtFontSC8()
		{
			// Not Implemanted yer, SCHIP8 Function.
		}

		void StoreBCD()
		{
			int vx = (int) V[opcode2];
            memoria[I] = vx / 100;              //centenas
            memoria[I + 1] = (vx / 10) % 10;    //decenas
            memoria[I + 2] = vx % 10;           //unidades
		}

		void SaveRegisters()
		{
			for (int i = 0; i <= opcode2; i++)
			{
				memoria[I++] = V[i];
			}
            //I += 1;
		}

		void LoadRegisters()
		{
			for (int i = 0; i <= opcode2; i++)
			{
				V[i] = memoria[I++];
			}
            //I += 1;
		}

               
        #endregion

    }
}
</pre></td></tr></table>

<br>
Bueno, con esto es suficiente por hoy, ya tenemos un emulador graficamente m�s decente que en modo consola. Nos vemos en el siguiente cap�tulo.
<br><br>
Bajar <a href="chip8_vs2008_SDLNetMode.rar">aqu�</a> el proyecto .Net para Visual Studio 2008 que contiene los fuentes de este cap�tulo.
<br><br>
<img src="imag/chip8_tranf_final.png">

<br><br>
<a href="emulador4.php">Lecci�n Anterior</a> | <a href="emulador_menu.php">�ndice</a> | <a href="emulador6.php">Siguiente Lecci�n</a>

<?php
include '../../piecdisq.php';
?>
