<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>Tutorial de creaci�n de un Emulador sencillo de Chip-8 con VS 2008 y C# (Parte 7)</title>
</head>

<body>
<small>
<span style="font-family: Verdana;">
<a href="http://darknromhacking.com/">Volver a P�gina Principal</a>
</small>

<hr style="width: 100%; height: 2px;">

<table width="100%">
<tr>
<td>

<H3>
<span style="font-weight: bold;">Tutorial de creaci�n de un Emulador sencillo de Chip-8 con VS 2008 y C# (Parte 7)</span>
</H3>
<small>
Lenguaje: C# 2.0<br>
Para: VS 2008 con Sdl.Net 6.1<br>
Por Dark-N: <a href="mailto:hernaldog@gmail.com">hernaldog@gmail.com</a>
<br>

<span style="font-family: Verdana;">
<a href="http://darknromhacking.com/">http://darknromhacking.com</a>
<br>
Hilo del Foro: <a href="http://foro.romhackhispano.org/viewtopic.php?f=4&t=872">
http://foro.romhackhispano.org/viewtopic.php?f=4&t=872</a>
</small></td>
<td align="center">
</table>

<hr style="width: 100%; height: 2px;">

<span style="font-family: Verdana;"><small>
<a href="emulador6.php">Lecci�n Anterior</a> | <a href="emulador_menu.php">�ndice</a>

<H3>La idea</H3>

La idea de este cap�tulo es mostrar como dejar el emulador en una ventana con las opciones <b>Cargar Juego</b>, <b>Pausar</b> y <b>Salir</b>.
<br><b>Nota</b>: algunas im�genes se encogieron para mejorar la lectura de la p�gina. 
Haz clic sobre ellas para verlas en tama�o real.
<H3>Conceptos</H3>

La ventana del emulador tiene 3 componentes: <li>La principal es <b>System.Windows.Forms</b> que en su interior contiene 2 componentes: <b>System.Windows.Forms.MenuStrip</b> que permite el men� superior y que es 
parte del Framework .Net, y la componente gr�fica <b>SdlDotNet SurfaceControl</b> que nos permite dibujar en pantalla los juegos emulados tal como en cap�tulos anteriores, 
esta componente es parte de SDL.NET (est� contenida en bin\SdlDotNet.dll).

<br><br>
<a href="imag/chip8_cap70.png"><img src="imag/chip8_cap70.png" width="20%" height="20%"></a>

<H3>Agregar un Formulario</H3>

Debemos ir al proyecto y con el 2do. bot�n agregar un nuevo elemento tipo <b>Windows Form</b> y colocarle el nombre <b>Chip8.cs</b>:
<br><br>
<a href="imag/chip8_cap71.png"><img src="imag/chip8_cap71.png" width="20%" height="20%"></a>
<br><br>
<a href="imag/chip8_cap72.png"><img src="imag/chip8_cap72.png" width="20%" height="20%"></a> Notar que Visual Studio genera autom�ticamente el archivo Chip8.Designer.cs.

<br><br>
Se debe setear el ancho y alto del formulario en <b>650</b> de ancho por <b>382</b> de alto que es un tama�o aceptable.
<br><br>
<a href="imag/chip8_cap75.png"><img src="imag/chip8_cap75.png" width="20%" height="20%"></a> 

<H3>Agregar la componente para men� superior System.Windows.Forms.MenuStrip</H3>

Abrimos el formulario en modo dise�o y vamos al <b>ToolBox</b> (CONTROL + ALT + X) y all� vamos a la secci�n <b>Menus & Toolbars</b> y ubicamos la componente 
<a href="http://msdn.microsoft.com/en-us/library/system.windows.forms.menustrip%28v=vs.80%29.aspx">MenuStrip</a>, la tomamos y 
arrastramos arriba-izquierda del formulario:

<br><br>
<a href="imag/chip8_cap73.png"><img src="imag/chip8_cap73.png" width="20%" height="20%"></a>

<br><br>
Escribimos sobre este control las opciones "Archivo" y bajo esta "Cargar Juego", "Reset" y "Salir", luego arriba escribir "Pausa":

<br><br>
<a href="imag/chip8_cap74.png"><img src="imag/chip8_cap74.png" width="20%" height="20%"></a>

<H3>Agregar la componente gr�fica SdlDotNet SurfaceControl</H3>

Debemos agregar la componente <a href="http://cs-sdl.sourceforge.net/apidocs/html/class_sdl_dot_net_1_1_windows_1_1_surface_control.html#_details">SdlDotNet SurfaceControl</a> al formulario que agregamos en el paso anterior.
Haremos esto mediante c�digo, as� que hacemos doble clic al archivo "Chip8.Designer.cs" y una vez abierto abrimos la secci�n "Windows Form Designer generated code", con esto tenemos el siguiente c�digo:

<pre style="font-size:12; background-color:#D8D8D8;">
/// <summary>
/// Required designer variable.
/// </summary>
private System.ComponentModel.IContainer components = null;

...

#region Windows Form Designer generated code

/// <summary>
/// Required method for Designer support - do not modify
/// the contents of this method with the code editor.
/// </summary>
private void InitializeComponent()
{
    this.components = new System.ComponentModel.Container();
    this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
    this.Text = "Chip8";
}

#endregion
</pre>

Aqu� debemos incluir la nueva componente con la variable <b>SdlDotNet.Windows.SurfaceControl scDisplay</b> que representar� la parte gr�fica del emulador y 
lo ubicamos abajo del control de Men�, es decir en el punto 0,27 y con una resoluci�n de 640 x 320::

<pre style="font-size:12; background-color:#D8D8D8;">
/// <summary>
/// Required designer variable.
/// </summary>
private System.ComponentModel.IContainer components = null;

// Control gr�fico dentro de la ventana que permite mostrar el juego
private SdlDotNet.Windows.SurfaceControl scDisplay;

...

private void InitializeComponent()
{
    System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Chip8));
    this.scDisplay = new SdlDotNet.Windows.SurfaceControl();
    ((System.ComponentModel.ISupportInitialize)(this.scDisplay)).BeginInit();
	...
	// seteamos el control gr�fico scDisplay para el emulador
	this.scDisplay.AccessibleDescription = "SdlDotNet SurfaceControl";
	this.scDisplay.AccessibleName = "SurfaceControl";
	this.scDisplay.AccessibleRole = System.Windows.Forms.AccessibleRole.Graphic;
	this.scDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
	            | System.Windows.Forms.AnchorStyles.Left)
	            | System.Windows.Forms.AnchorStyles.Right)));
	this.scDisplay.Image = ((System.Drawing.Image)(resources.GetObject("scDisplay.Image")));
	this.scDisplay.InitialImage = null;
	this.scDisplay.Location = new System.Drawing.Point(0, 27);
	this.scDisplay.Name = "scDisplay";
	this.scDisplay.Size = new System.Drawing.Size(640, 320);
	this.scDisplay.TabIndex = 0;
	this.scDisplay.TabStop = false;
	
	// Agregamos el control gr�fico
	this.Controls.Add(this.scDisplay);
}
</pre>

Se debe agregar el recurso "scDisplay.Image" al proyecto, para esto copia esta peque�a imagen <a href="imag/scDisplay.Image.bmp"><img src="imag/scDisplay.Image.bmp"></a> y p�gala en el 
archivo <b>Chip8.resx</b> en la secci�n <b>Images</b>:
<br><br>
<a href="imag/chip8_cap76.png"><img src="imag/chip8_cap76.png" width="20%" height="20%"></a>
<br><br>
Luego cambia la opci�n de <b>Persistencia</b> de la imagen por <b>Embedded in .resx</b> de esta forma si queremos hacer un deploy del emulador, por ejemplo en un archivo .exe, esta imagen estar� 
embebida en este y no sea necesario tener la imagen en el mismo directorio:
<br><br>
<a href="imag/chip8_cap77.png"><img src="imag/chip8_cap77.png" width="20%" height="20%"></a>

<br><br>
Por �ltimo seteamos el color negro de fondo para el control scDisplay.
<br><br>
<a href="imag/chip8_cap78.png"><img src="imag/chip8_cap78.png" width="20%" height="20%"></a>

<br><br>
Para revisar como qued�, si vamos al dise�o del emulador se ver� la peque�a imagen que cargamos y el fondo negro:
<br><br>
<a href="imag/chip8_cap79.png"><img src="imag/chip8_cap79.png" width="20%" height="20%"></a>

<H3>Cambios en el c�digo</H3>

Debemos aplicar varios cambios al c�digo para que la ventana sea el "manejador" principal que usar� la clase "Emulador.cs" para casi todas las operaciones.
<br><br>

<li>Lo primero que haremos, ser� cambiar el c�digo que inicia la aplicaci�n para que parta en modo ventana y no el juego directamente, esto se puede comprobar d�ndole F5 y se abrir� el emulador del cap�tulo anterior y no 
la nueva ventana. Por lo que agregamos a la clase <b>Chip8.cs</b> estos 2 namespaces:
<pre style="font-size:12; background-color:#D8D8D8;">
using SdlDotNet.Graphics;
using SdlDotNet.Core;
</pre>

En la clase del formulario <b>Chip8.cs</b>, creamos una nueva instancia de la clase <b>Emulador</b>:
<pre style="font-size:12; background-color:#D8D8D8;">
namespace Chip8
{
    public partial class Chip8 : Form
    {
        Emulador Emu;
        public Chip8()
        {
            InitializeComponent();
            Emu = new Emulador();
        }
    }
}
</pre>
</li>

<li>En <b>Chip8.cs</b> creamos el m�todo <b>UpdateDisplay()</b> que es que va a actualizar el control Surface en el formulario con las gr�ficas que retorna la clase Emulador.cs:
<pre style="font-size:12; background-color:#D8D8D8;">
// Actualiza control gr�fico con lo que entrega el Surface de la clase Emulador.cs
private void UpdateDisplay()
{
	scDisplay.Blit(Emu.SurfaceOut);
	scDisplay.Update();
}
</pre>
</li>

<li>En la clase <b>Emulador.cs</b> comentar el m�todo <b>Event Tick</b> y sacar la inicializaci�n en el constructor: 
<pre style="font-size:12; background-color:#D8D8D8;">
//private void Events_Tick(object sender, TickEventArgs e)
//{
//    Video.WindowCaption = "Emulador Chip-8  FPS: " + Events.Fps;
//    EmulaFrame();
//}

...

public Emulador()
{
	...
	//Events.Tick += new EventHandler<TickEventArgs>(this.Events_Tick);
	...
}
</pre>
y este Evento lo manejaremos por el formulario, por lo que se debe inicializar con <b>SdlDotNet.Core.Events.Tick += ...</b> en la clase <b>Chip8.cs</b> y crear el m�todo que llama a <b>UpdateDisplay()</b>:
<pre style="font-size:12; background-color:#D8D8D8;">
// constructor
public Chip8()
{
	...
	SdlDotNet.Core.Events.Tick += new EventHandler<TickEventArgs>(Events_Tick);
}
...
void Events_Tick(object sender, TickEventArgs e)
{
	Emu.EmulaFrame();
	UpdateDisplay();
}
</pre></li>


<li>En la clase <b>Emulador.cs</b> debemos comentar el m�todo <b>Main</b> que inicia la aplicaci�n:
<pre style="font-size:12; background-color:#D8D8D8;">
//static void Main(string[] args)
//{
//	Emulador emulador = new Emulador();	
//	emulador.Run();	
//}
</pre>
Dejaremos que el emulador se inicie desde otra clase, es m�s elegante y es un punto b�sico de que se denomina <a href="http://es.wikipedia.org/wiki/Programaci%C3%B3n_orientada_a_objetos">Programaci�n orientada a objetos</a>. 
Por lo que creamos la clase <b>Programa.cs</b> y adentro escribimos el c�digo que "lanza" el emulador, es decir creamos una instancia de la clase con "new Chip8()":
<pre style="font-size:12; background-color:#D8D8D8;">
using System;
using System.Windows.Forms;

namespace Chip8
{
    class Programa
    {
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Chip8());
        }
    }
}
</pre>
</li>

<li>En la clase <b>Emulador</b> definir nuevas propiedades:
<pre style="font-size:12; background-color:#D8D8D8;">
/// <summary>
/// Salida de gr�ficos
/// </summary>
private Surface surfaceOut;
public Surface SurfaceOut
{
    get { return surfaceOut; }
}
</pre>

En el constructor y otras partes del c�digo cambiamos el uso de <b>Video.Screen</b> por nuestra nueva variable de salida <b>surfaceOut</b>, a la vez de incrementar la variable que guarda el largo/alto de un pixel <b>tamanoPixel</b> de 5 a 10:
<pre style="font-size:12; background-color:#D8D8D8;">

const int tamanoPixel = 10; // tama�o de pixel, sirve para zoom de todo el emulador

/// <summary>
/// Constructor de la clase
/// </summary>
public Emulador()
{
    try
    {
        //Video.SetVideoMode(RES_X * tamanoPixel, RES_Y * tamanoPixel);
        //Video.Screen.Fill(Color.Black);
        //Events.Fps = 60;

        surfaceOut = Video.CreateRgbSurface(RES_X * tamanoPixel, RES_Y * tamanoPixel);
</pre>

Cambiamos el m�todo <b>ResetHardware</b> ya que usamos el m�todo <b>ClearScreen()</b>:
<pre style="font-size:12; background-color:#D8D8D8;">
void ResetHardware()
{
	...
	//Video.Screen.Fill(Color.Black);
	//Video.Screen.Update();
	ClearScreen();
}
</pre>

Cambiamos el m�todo <b>ClearScreen</b> que se encarga de dejar la pantalla en negro, cambiando Video.Screen por surfaceOut:
<pre style="font-size:12; background-color:#D8D8D8;">
void ClearScreen()
{
	surfaceOut.Fill(Color.Black);
	surfaceOut.Update();
	...
</pre>

Cambiamos el m�todo <b>DrawSprite</b> que se encarga pintar los pixels, cambiando Video.Screen por surfaceOut:
<pre style="font-size:12; background-color:#D8D8D8;">
void DrawSprite()
{	
	...
	if (arregloPantalla[xx % 64, yy % 32] == 1)
	{
		arregloPantalla[xx % 64, yy % 32] = 0;
		surfaceOut.Blit(PixelNegro, new Point((xx % 64) * tamanoPixel, (yy % 32) * tamanoPixel));
		V[0xF] = 1; //colision activado
	}
	else
	{
		arregloPantalla[xx % 64, yy % 32] = 1;
		surfaceOut.Blit(PixelBlanco, new Point((xx % 64) * tamanoPixel, (yy % 32) * tamanoPixel));
	}
	...
	surfaceOut.Update();
	...
}
</pre></li>

<li>Agregar el evento de Click cuando se haga clic sobre <b>Cargar Juego</b>. En modo Dise�o, se debe seleccionar Cargar Juego y a la derecha, escoger "Eventos" 
(es un �cono con un rayo de color anaranjado) y en la secci�n "Click" hacer doble clic sobre el espacio en blanco:

<br><br>
<a href="imag/chip8_cap710.png"><img src="imag/chip8_cap710.png" width="20%" height="20%"></a>
<br><br>
Con esto se crea autom�ticamente una secci�n para poner nuestro c�digo:
<br><br>
<a href="imag/chip8_cap711.png"><img src="imag/chip8_cap711.png" width="20%" height="20%"></a>

<br><br>
Al hacer clic debemos activar los Eventos. Por ahora s�lo haremos esto, m�s adelante mostraremos una <b>Ventana de Di�logo</b> para escoger el juego.
<pre style="font-size:12; background-color:#D8D8D8;">

Emulador.cs

// constructor
public Emulador()
{
	try
	{
		...
		//ResetHardware();
		//CargarJuego("PONG");
		...

public void CargarJuego(string nombreRom)  //dejamos p�blico el m�todo
{
	..
}

Chip8.cs
// Evento clic en opci�n del men� "Cargar Juego"
private void cargarJuegoToolStripMenuItem1_Click(object sender, EventArgs e)
{
	Emu.ResetHardware();
	Emu.CargarJuego("PONG");
	SdlDotNet.Core.Events.Run();
}
</pre>
</li>

<li>En <b>Chip8.cs</b> agregamos el <b>Evento de Cerrar Aplicaci�n</b> (usar el �cono del rayo) y en este finalizamos los Eventos de SDL.Net, esto permite que al hacer clic 
sobre la X que cierre la aplicaci�n o sobre la opci�n "Salir" del men� superior, no queden pegados los sonidos u otros eventos.
<pre style="font-size:12; background-color:#D8D8D8;">
// Evento gatillado al cerrar la aplicaci�n con la "x" de la ventana
private void Chip8_FormClosed(object sender, FormClosedEventArgs e)
{
	CerrarAplicacion();
}

// M�todo que termina los eventos y cierra la aplicaci�n
void CerrarAplicacion()
{
	SdlDotNet.Core.Events.Close();
	SdlDotNet.Core.Events.QuitApplication();
	Application.Exit();
}

// Evento gatillado al presionar Salir
private void salirToolStripMenuItem_Click(object sender, EventArgs e)
{
	CerrarAplicacion();
}
</pre>
En <b>Emulador.cs</b> comentamos el m�todo que cierra la aplicaci�n que se controla desde el formulario, lo comentamos en el constructor y en el evento de Teclas Presionadas.
<pre style="font-size:12; background-color:#D8D8D8;">
public Emulador()
{
	try
	{
		...
		//Events.Quit += new EventHandler<QuitEventArgs>(this.Events_Salir);
		...
	}
	...
	//private void Events_Salir(object sender, QuitEventArgs e)
	//{
	//    Events.QuitApplication();
	//}
}
...
private void Tecla_Presionada(object sender, KeyboardEventArgs e)
{
	////Salimos del juego con tecla Escape
	//if (e.Key == Key.Escape)
	//{
	//    Events.QuitApplication();
	//}
	...
}
</pre>

En el editor de Visual Studio, si presionamos <b>F5</b> ya podemos ver el emulador y all� vamos a "Archivo" -> "Cargar Rom" se cargar� el emulador con el juego PONG:

<br><br>
<a href="imag/chip8_cap712.png"><img src="imag/chip8_cap712.png" width="20%" height="20%"></a>

<H3>FPS, Pause, Reset, Ventana Carga de ROM y manejo del Teclado</H3>

Para agregar los <b>Frames Por Segundo (FPS)</b> en la clase <b>Chip8.cs</b> se debe incluir en el m�todo <b>Events_Tick</b>, se debe crear la variable de clase <b>TargetFPS</b> y de 
pasada en el constructor agregar el t�tulo del formulario:
<pre style="font-size:12; background-color:#D8D8D8;">
private int TargetFPS;

// constructor
public Chip8()
{
	...
	this.Text = "Emulador Chip-8";
	SdlDotNet.Core.Events.Tick += new EventHandler<TickEventArgs>(Events_Tick);
	TargetFPS = 60;
	SdlDotNet.Core.Events.Fps = 60;
}
...
void Events_Tick(object sender, TickEventArgs e)
{
	this.Text = "Emulador Chip-8  FPS: " + SdlDotNet.Core.Events.Fps;
	...
}
</pre>

El <b>Pause</b> es muy importante en cualquier emulador. Lo primero es definir una variable de clase booleana que nos diga si el juego est� pausado o no, le llamaremos "estaPausado", luego en el Event_Tick se debe validar que no ejecute los 
frames si est� pausado, y finalmente generar el evento "click" de la opci�n "Pausa" con el �cono de eventos (rayo), con esto se genera el m�todo pausaToolStripMenuItem_Click y adentro setear 
la variable "estaPausado" que definimos:
<pre style="font-size:12; background-color:#D8D8D8;">
private bool estaPausado;
...

// constructor
public Chip8()
{
	estaPausado = false;
}
...
// Game-Loop
void Events_Tick(object sender, TickEventArgs e)
{
    this.Text = "Emulador Chip-8  FPS: " + SdlDotNet.Core.Events.Fps;
    if (estaPausado == false)
    {
        Emu.EmulaFrame();
        UpdateDisplay();
    }
}
...
// Evento gatillado al presionar Pausa
private void pausaToolStripMenuItem_Click(object sender, EventArgs e)
{
    if (estaPausado == false)
        estaPausado = true;
    else
        estaPausado = false;
}
</pre>

El <b>Reset</b> tambi�n es muy importante, as� que imagino que saben que vamos a hacer: generamos el evento "click" de la opci�n "Reset" con el �cono de eventos (rayo) 
y adentro llamamos al m�todo Reset de la clase Emulador.cs. No olvidar dejar p�blico este �ltimo m�todo.
<pre style="font-size:12; background-color:#D8D8D8;">
Emulador.cs
public void ResetHardware()  //lo dejamos p�blico
{
...
}

Chip8.cs
// Evento gatillado al presionar Reset
private void resetToolStripMenuItem_Click(object sender, EventArgs e)
{
	Reset();
}

// Resetamos el hardware (registros, memoria) y cargamos de nuevo el juego
void Reset()
{
    Emu.ResetHardware();
    Emu.CargarJuego(nombreROM);  // en el siguiente paso definimos nombreROM          
}
</pre>

Para hacer la <b>la ventana de di�logo de carga de ROM</b> se debe primero agregar el control <a href="http://msdn.microsoft.com/en-us/library/system.windows.forms.openfiledialog%28v=vs.80%29.aspx">System.Windows.Forms.OpenFileDialog</a>
 al formulario, para esto lo buscamos en el ToolBox (Control + Alt + X) y lo arrastramos al panel inferior de la ventana de dise�o.
<br><br>
<a href="imag/chip8_cap713.png"><img src="imag/chip8_cap713.png" width="20%" height="20%"></a>
<br>
<br>
Ahora modificamos el m�todo cargarJuegoToolStripMenuItem1_Click de la clase <b>Chip8.cs</b>:

<pre style="font-size:12; background-color:#D8D8D8;">
private string nombreROM = string.Empty;
...
// Evento clic en opci�n del men� "Cargar Juego"
private void cargarJuegoToolStripMenuItem1_Click(object sender, EventArgs e)
{
	openFileDialog1.FileName = string.Empty;

    if (openFileDialog1.ShowDialog() == DialogResult.OK)
    {
        nombreROM = openFileDialog1.FileName;
        Reset();
        SdlDotNet.Core.Events.Run();
    }
}
</pre>

Con esto podremos cargar cualquier juego y no s�lo la ROM de PONG:
<br><br>
<a href="imag/chip8_cap714.png"><img src="imag/chip8_cap714.png" width="20%" height="20%"></a>

<br><br>
Para el manejo del <b>Teclado</b>, si nos fijamos al correr el emulador, no funcionan las teclas, basta con presionar Q y la paleta izquierda en el juego PONG no baja, por lo tanto debemos hacer unos ajustes.
<br>
Lo primero es comentar en la clase <b>Emulador.cs</b> los eventos de Teclado. Con esto no tendr�amos ning�n manejo de evento en esta clase. Tambi�n comantamos los m�todos <b>Tecla_Presionada</b>, <b>Tecla_Liberada</b> 
las variables de cada tecla, y dejamos p�bica la variable <b>teclasPresionadas</b> para ser utilizada desde el formulario:
<pre style="font-size:12; background-color:#D8D8D8;">
//using SdlDotNet.Core; //Eventos
//using SdlDotNet.Input; //para manejo de teclado
...
public bool[] teclasPresionadas = { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false };
...
//constructor
public Emulador()
{
	...
	//Events.KeyboardDown += new EventHandler<KeyboardEventArgs>(this.Tecla_Presionada);
	//Events.KeyboardUp += new EventHandler<KeyboardEventArgs>(this.Tecla_Liberada);
	...
}
...
//private void Tecla_Presionada(object sender, KeyboardEventArgs e)
//{
//    ////Salimos del juego con tecla Escape
//    //if (e.Key == Key.Escape)
//    //{
//    //    Events.QuitApplication();
//    //}

//    if (e.Key == Key.One) { teclasPresionadas[TECLA_1] = true; }
//    if (e.Key == Key.Two) { teclasPresionadas[TECLA_2] = true; }
//    if (e.Key == Key.Three) { teclasPresionadas[TECLA_3] = true; }
//    if (e.Key == Key.Four) { teclasPresionadas[TECLA_4] = true; }
//    if (e.Key == Key.Q) { teclasPresionadas[TECLA_Q] = true; }
//    if (e.Key == Key.W) { teclasPresionadas[TECLA_W] = true; }
//    if (e.Key == Key.E) { teclasPresionadas[TECLA_E] = true; }
//    if (e.Key == Key.R) { teclasPresionadas[TECLA_R] = true; }
//    if (e.Key == Key.A) { teclasPresionadas[TECLA_A] = true; }
//    if (e.Key == Key.S) { teclasPresionadas[TECLA_S] = true; }
//    if (e.Key == Key.D) { teclasPresionadas[TECLA_D] = true; }
//    if (e.Key == Key.F) { teclasPresionadas[TECLA_F] = true; }
//    if (e.Key == Key.Z) { teclasPresionadas[TECLA_Z] = true; }
//    if (e.Key == Key.X) { teclasPresionadas[TECLA_X] = true; }
//    if (e.Key == Key.C) { teclasPresionadas[TECLA_C] = true; }
//    if (e.Key == Key.V) { teclasPresionadas[TECLA_V] = true; }
//}

//private void Tecla_Liberada(object sender, KeyboardEventArgs e)
//{
//    if (e.Key == Key.One) { teclasPresionadas[TECLA_1] = false; }
//    if (e.Key == Key.Two) { teclasPresionadas[TECLA_2] = false; }
//    if (e.Key == Key.Three) { teclasPresionadas[TECLA_3] = false; }
//    if (e.Key == Key.Four) { teclasPresionadas[TECLA_4] = false; }
//    if (e.Key == Key.Q) { teclasPresionadas[TECLA_Q] = false; }
//    if (e.Key == Key.W) { teclasPresionadas[TECLA_W] = false; }
//    if (e.Key == Key.E) { teclasPresionadas[TECLA_E] = false; }
//    if (e.Key == Key.R) { teclasPresionadas[TECLA_R] = false; }
//    if (e.Key == Key.A) { teclasPresionadas[TECLA_A] = false; }
//    if (e.Key == Key.S) { teclasPresionadas[TECLA_S] = false; }
//    if (e.Key == Key.D) { teclasPresionadas[TECLA_D] = false; }
//    if (e.Key == Key.F) { teclasPresionadas[TECLA_F] = false; }
//    if (e.Key == Key.Z) { teclasPresionadas[TECLA_Z] = false; }
//    if (e.Key == Key.X) { teclasPresionadas[TECLA_X] = false; }
//    if (e.Key == Key.C) { teclasPresionadas[TECLA_C] = false; }
//    if (e.Key == Key.V) { teclasPresionadas[TECLA_V] = false; }
//}

//const int TECLA_1	= 0;
//const int TECLA_2	= 1;
//const int TECLA_3	= 2;
//const int TECLA_4	= 3;
//const int TECLA_Q	= 4;
//const int TECLA_W	= 5;
//const int TECLA_E	= 6;
//const int TECLA_R	= 7;
//const int TECLA_A	= 8;
//const int TECLA_S	= 9;
//const int TECLA_D	= 10;
//const int TECLA_F	= 11;		
//const int TECLA_Z	= 12;
//const int TECLA_X	= 13;
//const int TECLA_C	= 14;
//const int TECLA_V	= 15;
</pre>

Lo segundo es modificar la clase <b>Chip8.cs</b> ya que ahora usamos el delegado <a href="http://msdn.microsoft.com/en-us/library/system.windows.forms.keyeventhandler%28v=vs.80%29.aspx">KeyEventHandler</a> del Framework .Net 
ya que el teclado lo manejamos desde el formulario y no la clase <a href="http://cs-sdl.sourceforge.net/apidocs/html/class_sdl_dot_net_1_1_input_1_1_keyboard_event_args.html">KeyboardEventArgs</a> de SDL.Net como lo hac�amos antes:

<pre style="font-size:12; background-color:#D8D8D8;">
// variables para cada tecla
const int TECLA_1 = 0;
const int TECLA_2 = 1;
const int TECLA_3 = 2;
const int TECLA_4 = 3;
const int TECLA_Q = 4;
const int TECLA_W = 5;
const int TECLA_E = 6;
const int TECLA_R = 7;
const int TECLA_A = 8;
const int TECLA_S = 9;
const int TECLA_D = 10;
const int TECLA_F = 11;
const int TECLA_Z = 12;
const int TECLA_X = 13;
const int TECLA_C = 14;
const int TECLA_V = 15;

// constructor
public Chip8()
{
	...
	this.KeyDown += new KeyEventHandler(this.Tecla_Presionada);
	this.KeyUp += new KeyEventHandler(this.Tecla_Liberada);
	...
}

private void Tecla_Presionada(object sender, KeyEventArgs e)
{
    if (e.KeyCode == Keys.NumPad1) { Emu.teclasPresionadas[TECLA_1] = true; }
    if (e.KeyCode == Keys.NumPad2) { Emu.teclasPresionadas[TECLA_2] = true; }
    if (e.KeyCode == Keys.NumPad3) { Emu.teclasPresionadas[TECLA_3] = true; }
    if (e.KeyCode == Keys.NumPad4) { Emu.teclasPresionadas[TECLA_4] = true; }
    if (e.KeyCode == Keys.Q) { Emu.teclasPresionadas[TECLA_Q] = true; }
    if (e.KeyCode == Keys.W) { Emu.teclasPresionadas[TECLA_W] = true; }
    if (e.KeyCode == Keys.E) { Emu.teclasPresionadas[TECLA_E] = true; }
    if (e.KeyCode == Keys.R) { Emu.teclasPresionadas[TECLA_R] = true; }
    if (e.KeyCode == Keys.A) { Emu.teclasPresionadas[TECLA_A] = true; }
    if (e.KeyCode == Keys.S) { Emu.teclasPresionadas[TECLA_S] = true; }
    if (e.KeyCode == Keys.D) { Emu.teclasPresionadas[TECLA_D] = true; }
    if (e.KeyCode == Keys.F) { Emu.teclasPresionadas[TECLA_F] = true; }
    if (e.KeyCode == Keys.Z) { Emu.teclasPresionadas[TECLA_Z] = true; }
    if (e.KeyCode == Keys.X) { Emu.teclasPresionadas[TECLA_X] = true; }
    if (e.KeyCode == Keys.C) { Emu.teclasPresionadas[TECLA_C] = true; }
    if (e.KeyCode == Keys.V) { Emu.teclasPresionadas[TECLA_V] = true; }
}

private void Tecla_Liberada(object sender, KeyEventArgs e)
{
    if (e.KeyCode == Keys.NumPad1) { Emu.teclasPresionadas[TECLA_1] = false; }
    if (e.KeyCode == Keys.NumPad2) { Emu.teclasPresionadas[TECLA_2] = false; }
    if (e.KeyCode == Keys.NumPad3) { Emu.teclasPresionadas[TECLA_3] = false; }
    if (e.KeyCode == Keys.NumPad4) { Emu.teclasPresionadas[TECLA_4] = false; }
    if (e.KeyCode == Keys.Q) { Emu.teclasPresionadas[TECLA_Q] = false; }
    if (e.KeyCode == Keys.W) { Emu.teclasPresionadas[TECLA_W] = false; }
    if (e.KeyCode == Keys.E) { Emu.teclasPresionadas[TECLA_E] = false; }
    if (e.KeyCode == Keys.R) { Emu.teclasPresionadas[TECLA_R] = false; }
    if (e.KeyCode == Keys.A) { Emu.teclasPresionadas[TECLA_A] = false; }
    if (e.KeyCode == Keys.S) { Emu.teclasPresionadas[TECLA_S] = false; }
    if (e.KeyCode == Keys.D) { Emu.teclasPresionadas[TECLA_D] = false; }
    if (e.KeyCode == Keys.F) { Emu.teclasPresionadas[TECLA_F] = false; }
    if (e.KeyCode == Keys.Z) { Emu.teclasPresionadas[TECLA_Z] = false; }
    if (e.KeyCode == Keys.X) { Emu.teclasPresionadas[TECLA_X] = false; }
    if (e.KeyCode == Keys.C) { Emu.teclasPresionadas[TECLA_C] = false; }
    if (e.KeyCode == Keys.V) { Emu.teclasPresionadas[TECLA_V] = false; }
}
</pre>

<hr>
El c�digo fuente de la lecci�n es el siguiente.
<br>
<b>Nota</b>: se sacaron las l�neas que indicamos que deben ser comentadas, esto para dejar el c�digo limpio y ayudar a la lectura de este.
<br><br>
Programa.cs
<pre style="font-size:12; background-color:#D8D8D8;">
using System;
using System.Windows.Forms;

namespace Chip8
{
    class Programa
    {
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Chip8());
        }
    }
}
</pre>

<br>
Emulador.cs
<pre style="font-size:12; background-color:#D8D8D8;">
using System;
using System.IO; //Librer�a de .Net para manejar archivos
using System.Runtime.InteropServices;  //para el "beep" de la Bios
using System.Drawing;  //Librer�a de .Net para manejar Colores
using System.Windows.Forms; //Para enviar mensajes en ventanas de dialogos
using SdlDotNet.Graphics; //para Surfaces
using SdlDotNet.Graphics.Sprites;  //para Sprites y textos en pantalla

namespace Chip8
{
	class Emulador
	{
		#region variables emulador

		//variables principales
        const int DIR_INICIO        = 0x200;
		const int TAMANO_MEM        = 0xFFF;
		const int TAMANO_PILA       = 16;
		const int CANT_REGISTROS    = 16;

        //arreglo que representa la memoria
        int[] memoria = new int[TAMANO_MEM];

        //Arreglo que representa los 16 Registros (V) 
        int[] V = new int[CANT_REGISTROS];

        //arreglo que representa la Pila
        int[] pila = new int[TAMANO_PILA];

        //variables que representan registros varios
        int instruccion;  //representa una instruccion del Chip-8. Tiene largo 2 byte
        int PC;
        int I;
        int SP;      
        int KK;
		
        //resolucion de pantalla 64x32 (mas alta que larga)
        const int RES_X	= 64;
		const int RES_Y	= 32;
        int[,] arregloPantalla = new int[RES_X, RES_Y];
		
        //variables para manejar los opcodes
		int opcode1 = 0;
		int opcode2 = 0;  //X
		int opcode3 = 0;  //Y
		int opcode4 = 0;
        int NNN = 0;
		
        //variables que representan los 2 timers: Delay Timer y Sound Timer
		int	delayTimer;
		int	soundTimer;

        //variable para manejo del sonido
        bool ejecutaSonido = true;

        //variables para el manejo de fuentes (80 bytes, ya que hay 5 bytes x caracter 
        //y son 16 caracteres o letras (5x16=80). Cada font es de 4x5 bits. 
		int[] arregloFuentes = {
		   0xF0, 0x90, 0x90, 0x90, 0xF0,	// valores para 0
		   0x60, 0xE0, 0x60, 0x60, 0xF0,	// valores para 1
		   0x60, 0x90, 0x20, 0x40, 0xF0,	// valores para 2
		   0xF0, 0x10, 0xF0, 0x10, 0xF0,	// valores para 3
		   0x90, 0x90, 0xF0, 0x10, 0x10,	// valores para 4
		   0xF0, 0x80, 0x60, 0x10, 0xE0,	// valores para 5
		   0xF0, 0x80, 0xF0, 0x90, 0xF0,	// valores para 6
		   0xF0, 0x10, 0x10, 0x10, 0x10,	// valores para 7
		   0xF0, 0x90, 0xF0, 0x90, 0xF0,	// valores para 8
		   0xF0, 0x90, 0xF0, 0x10, 0x10,	// valores para 9
		   0x60, 0x90, 0xF0, 0x90, 0x90,	// valores para A
		   0xE0, 0x90, 0xE0, 0x90, 0xE0,	// valores para B
		   0x70, 0x80, 0x80, 0x80, 0x70,	// valores para C
		   0xE0, 0x90, 0x90, 0x90, 0xE0, 	// valores para D
		   0xF0, 0x80, 0xF0, 0x80, 0xF0,	// valores para E
		   0xF0, 0x90, 0xF0, 0x80, 0x80		// valores para F
		};

        public bool[] teclasPresionadas = { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false };
		
        // mapeamos las 16 teclas de Chip8
        private byte[] MapeoTeclas = 
        {	       
            0x01,0x02,0x03,0x0C,
			0x04,0x05,0x06,0x0D,
			0x07,0x08,0x09,0x0E,
			0x0A,0x00,0x0B,0x0F 
        };

        [DllImport("Kernel32.dll")] //para beep
        public static extern bool Beep(UInt32 frequency, UInt32 duration);
        
        //Variable de tipo Random (para generar n�meros aleatoios) utilizada por ciertas instrucciones
        Random rnd = new Random();
       
        private Surface PixelNegro;
        private Surface PixelBlanco;
        const int tamanoPixel = 10; // tama�o de pixel, sirve para zoom de todo el emulador
        public const int operPorSegundo = 600;
        public const int operPorFrame = operPorSegundo / 60;
        private int TimeUntilTimerUpdate;  //para la simulaci�n de timers

        #endregion

        /// <summary>
        /// Salida de gr�ficos
        /// </summary>
        private Surface surfaceOut;
        public Surface SurfaceOut
        {
            get { return surfaceOut; }
        }
        		
        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public Emulador()
        {
            try
            {
                surfaceOut = Video.CreateRgbSurface(RES_X * tamanoPixel, RES_Y * tamanoPixel);
                PixelNegro = Video.CreateRgbSurface(tamanoPixel, tamanoPixel);
                PixelBlanco = Video.CreateRgbSurface(tamanoPixel, tamanoPixel);
                PixelNegro.Fill(Color.Black);
                PixelBlanco.Fill(Color.White);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error General: " + ex.Message + "-" + ex.StackTrace);                
            }
        }

        public void ResetHardware()
        {
            // Reseteamos los Timers
            delayTimer = 0x0;
            soundTimer = 0x0;

            // Reseteamos variables
            instruccion = 0x0;
            PC = DIR_INICIO;
            SP = 0x0;
            I = 0x0;

            // Limpiamos la memoria
            for (int i = 0; i < TAMANO_MEM; i++)
                memoria[i] = 0x0;


            // Limpiamos registros
            for (int i = 0; i < CANT_REGISTROS; i++)
                V[i] = 0x0;


            // Limpiamos el stack
            for (int i = 0; i < TAMANO_PILA; i++)
                pila[i] = 0x0;

            // Cargamos las fuentes en la memoria por ej, los marcadores del juego PONG esos "[0-0]", "[0-2]"
            for (int i = 0; i < 80; i++)
                memoria[i] = arregloFuentes[i];	

            ClearScreen();
        }

        // Carga la ROM (archivo) a la memoria (arreglo)
        public void CargarJuego(string nombreRom)
		{            
            FileStream rom;

            try
            {
                rom = new FileStream(@nombreRom, FileMode.Open);

                if (rom.Length == 0)
                {
                    throw new Exception("Error en la Carga de la Rom: ROM da�ada o vac�a");
                }

                // Comenzamos a cargar la rom a la memoria a partir de la dir 0x200
                for (int i = 0; i < rom.Length; i++)
                    memoria[DIR_INICIO + i] = (byte)rom.ReadByte();	

                rom.Close();                
            }
            catch (Exception ex)
            {
                throw new Exception("Error en la Carga de la Rom: " + ex.Message);
            }
		}

        // Emula la ejecuci�n de un Frame
        public void EmulaFrame() //representa un frame
        {
            if (soundTimer > 0 && ejecutaSonido == true)
            {
                Console.Beep(350, 50);
                ejecutaSonido = false;                
            }
            
            //por cada Tick o Frame, se ejecutan 600/60=10 instrucciones
            for (int i = 0; i < operPorFrame; i++)
            {
                EmulaOpcodes();
            }
        }

        // Emula la ejecuci�n de un Opcode
        void EmulaOpcodes()
        {
            if (TimeUntilTimerUpdate == 0)
            {
                if (delayTimer > 0)
                    delayTimer--;

                if (soundTimer > 0) 
                	soundTimer--;

                TimeUntilTimerUpdate = operPorFrame;
            }
            else
            {
                TimeUntilTimerUpdate--;
            }

            EjecutaOpcodes();
        }

        // Lee las instrucciones de la memoria y ejecuta los Opcodes
        void EjecutaOpcodes()
		{
            #region lectura de instrucciones 

            // leemos cada una de las instrucciones desde la memoria. 
            // cada instruccion es de 2 bytes
            instruccion = memoria[PC] << 8 | memoria[PC + 1];

            // dejamos incrementado el Program Counter en 2, lista para leer 
            // la siguiente instruccion en el siguiente ciclo.
            PC += 2;

            #endregion 

            #region extracci�n de opcodes

            //obtengo el valor del registro KK, de largo 1 byte, el m�s chico de la instrucci�n
            KK = (instruccion & 0x00FF);

			// cada opcode es de 4 bit
            opcode1 = (instruccion & 0xF000) >> 12; //los 4 bits mayores de la instrucci�n
            opcode2 = (instruccion & 0x0F00) >> 8;  //X
            opcode3 = (instruccion & 0x00F0) >> 4;  //Y
            opcode4 = (instruccion & 0x000F) >> 0;  //Opcode N = los 4 bits menores de la instrucci�n

            //obtengo el valor del opcode NNN
            NNN = (instruccion & 0x0FFF);

            #endregion

            #region ejecuci�n de instrucciones

            // Ejecutamos las instrucciones a travez de los opcodes
			switch (opcode1)
			{
				// opcodes del tipo 0xxx
				case (0x0):
				{
                    switch (instruccion)
					{
						// opcode 00E0: Clear Screen.
						case (0x00E0):
						{
							ClearScreen();
							break;
						}
						// opcode 00EE: Return From Subroutine.
						case (0x00EE):
						{
							ReturnFromSub();
							break;
						}
					}
					break;
				}
				// opcodes del tipo 1xxx
				case (0x1):
				{
					// opcode 1NNN: Jump To Address NNN.
					JumpToAddr();
					break;
				}
                // opcodes del tipo 2xxx
				case (0x2):
				{
					// opcode 2NNN: Call Subroutine At Address NNN.
					CallSub();
					break;
				}
                // opcodes del tipo 3xxx
				case (0x3):
				{
					// opcode 4XKK: Skip Next Instruction If VX == KK
					SkipIfEql();
					break;
				}
                // opcodes del tipo 4xxx
				case (0x4):
				{
					// opcode 4XKK: Skip Next Instruction If VX != KK
					SkipIfNotEql();
					break;
				}
                // opcodes del tipo 5xxx
				case (0x5):
				{
					// opcode 5XY0: Skip Next Instruction If VX == VY
					SkipIfRegEql();
					break;
				}
                // opcodes del tipo 6xxx
				case (0x6):
				{
					// opcode 6XKK: Assign Number KK To Register X.
					AssignNumToReg();
					break;
				}
                // opcodes del tipo 7xxx
				case (0x7):
				{
					// opcode 7XKK: Add Number KK To Register X.
					AddNumToReg();
					break;
				}
                // opcodes del tipo 8xxx
				case (0x8):
				{
					//Tenemos varios tipos
					switch (opcode4)
					{
						// opcode 8XY0: Assign From Register To Register.
						case (0x0):
						{
							AssignRegToReg();
							break;
						}
						// opcode 8XY1: Bitwise OR Between Registers.
						case (0x1):
						{
							RegisterOR();
							break;
						}
						// opcode 8XY2: Bitwise AND Between Registers.
						case (0x2):
						{
							RegisterAND();
							break;
						}
						// opcode 8XY3: Bitwise XOR Between Registers.
						case (0x3):
						{
							RegisterXOR();
							break;
						}
						// opcode 8XY4: Add Register To Register.
						case (0x4):
						{
							AddRegToReg();
							break;
						}
						// opcode 8XY5: Sub Register From Register.
						case (0x5):
						{
							SubRegFromReg();
							break;
						}
						// opcode 8XY6: Shift Register Right Once.
						case (0x6):
						{
							ShiftRegRight();
							break;
						}
						// opcode 8XY7: Sub Register From Register (Reverse Order).
						case (0x7):
						{
							ReverseSubRegs();
							break;
						}
						// opcode 8XYE: Shift Register Left Once.
						case (0xE):
						{
							ShiftRegLeft();
							break;
						}
					}
					break;
				}
                // opcodes del tipo 9xxx
				case (0x9):
				{
					// opcode 9XY0: Skip Next Instruction If VX != VY
					SkipIfRegNotEql();
					break;
				}
				// opcodes del tipo AXXX
				case (0xA):
				{
					// opcode ANNN: Set Index Register To Address NNN.
					AssignIndexAddr();
					break;
				}
                // opcodes del tipo BXXX
				case (0xB):
				{
					// opcode BNNN: Jump To NNN + V0.
					JumpWithOffset();
					break;
				}
                // opcodes del tipo CXXX
				case (0xC):
				{
					// opcode CXKK: Assign Bitwise AND Of Random Number & KK To Register X.
					RandomANDnum();
					break;
				}
                // opcodes del tipo DXXX
				case (0xD):
				{
					// opcode DXYN: Draw Sprite To The Screen.
					DrawSprite();
					break;
				}
                // opcodes del tipo EXXX
				case (0xE):
				{
					// Tenemos 2 tipos seg�n EXKK
					switch (KK)
					{
						// opcode EX9E: Skip Next Instruction If Key In VX Is Pressed.
						case (0x9E):
						{
							SkipIfKeyDown();
							break;
						}
						// opcode EXA1: Skip Next Instruction If Key In VX Is NOT Pressed.
						case (0xA1):
						{
							SkipIfKeyUp();
							break;
						}
					}
					break;
				}
                // opcodes del tipo FXXX
				case (0xF):
				{
					// tenemos varios tipos de Opcodes
					switch (KK)
					{
						// opcode FX07: Assign Delay Timer To Register.
						case (0x07):
						{
							AssignFromDelay();
							break;
						}
						// opcode FX0A: Wait For Keypress And Store In Register.
						case (0x0A):
						{
							StoreKey();
							break;
						}
						// opcode FX15: Assign Register To Delay Timer.
						case (0x15):
						{
							AssignToDelay();
							break;
						}
						// opcode FX18: Assign Register To Sound Timer.
						case (0x18):
						{
							AssignToSound();
							break;
						}
						// opcode FX1E: Add Register To Index.
						case (0x1E):
						{
							AddRegToIndex();
							break;
						}
						// opcode FX29: Index Points At CHIP8 Font Char In Register.
						case (0x29):
						{
							IndexAtFontC8();
							break;
						}
						// opcode FX30: Index Points At SCHIP8 Font Char In Register.
						case (0x30):
						{
							IndexAtFontSC8();
							break;
						}
						// opcode FX33: Store BCD Representation Of Register In Memory.
						case (0x33):
						{
							StoreBCD();
							break;
						}
						// opcode FX55: Save Registers To Memory.
						case (0x55):
						{
							SaveRegisters();
							break;
						}
						// opcode FX65: Load Registers From Memory.
						case (0x65):
						{
							LoadRegisters();
							break;
						}
					}
					break;
				}
			}
            
            #endregion
        }

        #region implementaci�n de m�todos para cada instrucci�n

        void ClearScreen()
        {
            //Video.Screen.Fill(Color.Black);
            //Video.Screen.Update();
            surfaceOut.Fill(Color.Black);
            surfaceOut.Update();

            for (int p = 0; p < RES_X; p++)
                for (int q = 0; q < RES_Y; q++)
                    arregloPantalla[p, q] = 0;
        }

		void ReturnFromSub()
		{
			// Se diminuye el SP en 1
			SP--;

			// Apuntamos el PC (que apunta a la sgte. instrucci�n a ejecutar) a la
            // posici�n salvada en la Pila
			PC = pila[SP];
		}

		void JumpToAddr()
		{
			// salta a la instrucci�n dada. No se salta directamente, sino que se le indica
            // al PC que vaya a dicha direccion luego de salir del actual ciclo.
            PC = NNN;
		}

		void CallSub()
		{
			// Salva la posicion actual del PC en la Pila, para volver a penas se ejecute la subrutina
			pila[SP] = PC;
			SP++;

			// Saltamos a la subrutina indicada en NNN
            PC = NNN;
		}

		void SkipIfEql()
		{
			// Recordar que Opcode2=X
			if (V[opcode2] == KK)
			{
				// Salta a la siguiente instruccion
				PC += 2;
			}
		}

		void SkipIfNotEql()
		{
			if (V[opcode2] != KK)
			{
                // Salta a la siguiente instruccion
				PC += 2;
			}
		}

		void SkipIfRegEql()
		{
			if (V[opcode2] == V[opcode3])
			{
                // Salta a la siguiente instruccion
				PC += 2;
			}
		}

		void AssignNumToReg()
		{
			V[opcode2] = KK;
		}

	
		/**
		entrega los 8 bits menores (char) de un numero de 16 bits (int)

		@param number el numero de 16 bit
		@return el numero de 8 bits
		**/
		private char getLower(int number)
		{
			return (char)(number&0xFF);
		}

		void AddNumToReg()
		{
			V[opcode2] += KK;
		}

		void AssignRegToReg()
		{
			//VX = VY
			V[opcode2] = V[opcode3];
		}

		void RegisterOR()
		{
			// OR binario es |, entonces hacemos VX = VX | VY o mas elegante VX |= VY
			V[opcode2] |= V[opcode3];
		}

		void RegisterAND()
		{
            // OR binario es &, entonces hacemos VX = VX & VY o mas elegante VX &= VY
			V[opcode2] &= V[opcode3];
		}

		void RegisterXOR()
		{
            // XOR es ^, entonces hacemos VX = VX ^ VY o mas elegante VX ^= VY
			V[opcode2] ^= V[opcode3];
		}

		void AddRegToReg()
		{
			//Con >> extraemos los 8 bits mayores de la suma, si el resultado supera
            //los 8 bits el >> 8 dara 1 si no 0
			V[0xF] = (V[opcode2] + V[opcode3]) >> 8;

			//VX = VX + VY
			V[opcode2] += V[opcode3];
		}

		void SubRegFromReg()
		{
			//seteamos F en 1 si VX > VY
			if (V[opcode2] >= V[opcode3])
				V[0xF] = 0x1;
			else
				V[0xF] = 0x0;

			//VX = VX - VY
			V[opcode2] -= V[opcode3];
		}

		void ShiftRegRight()
		{
            //VF = VX AND 1 (VF valdr� 1 o 0). Para este case es m�s optimo
			V[0xF] = V[opcode2] & 0x1;
            //Manera elegante de escribir un shift a la derecha para dividir por 2: V[opcode2] = V[opcode2] >> 1;
			V[opcode2] >>= 1;
		}

		void ReverseSubRegs()
		{
			if (V[opcode2] <= V[opcode3])
			{
				V[0xF] = 0x1;
			}
			else
			{
				V[0xF] = 0x0;
			}

            V[opcode2] = V[opcode3] - V[opcode2];
		}

		void ShiftRegLeft()
		{
            //VF = VX AND 10 hex
			V[0xF] = V[opcode2] & 0x10;
            //Manera elegante de escribir un shift a la izquierda para multiplicar por 2: V[opcode2] = V[opcode2] << 1;
			V[opcode2] <<= 1;
		}

		void SkipIfRegNotEql()
		{			
			if (V[opcode2] != V[opcode3])
			{
				//Aumentamos el PC en 2 para saltar a la siguiente instrucci�n
				PC += 2;
			}
		}

		void AssignIndexAddr()
		{
			// se setea el Registro de �ndice (I) a la direcci�n NNN.
            I = NNN;
		}

		void JumpWithOffset()
		{
            PC = NNN + V[0x0];
		}	

		void RandomANDnum()
		{
            //usamos el variable rnd seteada en la clase. Con el m�todo Next se le puede dar el m�nimo (0) y m�ximo (255)
            int numeroRnd = rnd.Next(0,255);
            V[opcode2] = numeroRnd & KK;
		}

        /// <summary>
        /// Screen is 64x62 pixels
        /// Todos los drawings son hechos en modos XOR. 
        /// Cuando uno o mas pixels son borrados mientras un sprite es pintado, 
        /// el registro VF se setea en 01, sino queda en 00.
        /// </summary>
        void DrawSprite()
        {
            // Reseteamos el registro que detecta colisiones
            V[0xF] = 0x0;

            if ((instruccion & 0x000F) == 0) //opcode & 0x000F =opcode4
            {
                // Dibujamos un Sprite de SCHIP8 de tama�o 16x16
                // No implementado a�n
            }
            else
            {
                // Bibujamos un Sprite de CHIP8 de tama�o 8xN				
                for (int spriY = 0; spriY < opcode4; spriY++)
                {
                    for (int spriX = 0; spriX < 8; spriX++)
                    {
                        int x = (memoria[I + spriY] & (0x80 >> spriX));
                        if (x != 0)
                        {
                            // checkeamos por alguna colision
                            int xx = (V[opcode2] + spriX);
                            int yy = (V[opcode3] + spriY);

                            if (arregloPantalla[xx % 64, yy % 32] == 1)
                            {
                                arregloPantalla[xx % 64, yy % 32] = 0;
                                surfaceOut.Blit(PixelNegro, new Point((xx % 64) * tamanoPixel, (yy % 32) * tamanoPixel));
                                V[0xF] = 1; //colision activado
                            }
                            else
                            {
                                arregloPantalla[xx % 64, yy % 32] = 1;
                                surfaceOut.Blit(PixelBlanco, new Point((xx % 64) * tamanoPixel, (yy % 32) * tamanoPixel));
                            }
                        }
                    }
                }
            }
            surfaceOut.Update();
        }

		void SkipIfKeyDown()
		{
            if (teclasPresionadas[MapeoTeclas[V[opcode2]]] == true)
				PC += 2;		
		}

		void SkipIfKeyUp()
		{
            if (teclasPresionadas[MapeoTeclas[V[opcode2]]] == false)
				PC += 2;
		}

		void AssignFromDelay()
		{
			V[opcode2] = delayTimer;
		}

		void StoreKey()
		{
            for (int i = 0; i < teclasPresionadas.Length; i++)
			{
                if (teclasPresionadas[i] == true)
				{
					V[opcode2] = i;
				}
			}
		}

		void AssignToDelay()
		{
			delayTimer = V[opcode2];
		}

		void AssignToSound()
		{
			soundTimer = V[opcode2];
            ejecutaSonido = true;
		}

		void AddRegToIndex()
		{
			I += V[opcode2];
		}

		void IndexAtFontC8()
		{
			I = (V[opcode2] * 0x5);
		}

		void IndexAtFontSC8()
		{
			// No implementado a�n, funci�n de Super Chip-8
		}

		void StoreBCD()
		{
            int vx = (int) V[opcode2];
            memoria[I] = vx / 100;              //centenas
            memoria[I + 1] = (vx / 10) % 10;    //decenas
            memoria[I + 2] = vx % 10;           //unidades
		}

		void SaveRegisters()
		{
			for (int i = 0; i <= opcode2; i++)
			{
				memoria[I++] = V[i];
			}            
		}

		void LoadRegisters()
		{
			for (int i = 0; i <= opcode2; i++)
			{
				V[i] = memoria[I++];
			}          
		}               
        #endregion
    }
}
</pre>

Chip8.cs
<pre style="font-size:12; background-color:#D8D8D8;">
using System;
using System.Windows.Forms;

using SdlDotNet.Graphics;
using SdlDotNet.Core;
using SdlDotNet.Input; //para manejo de teclado

namespace Chip8
{
    public partial class Chip8 : Form
    {
        Emulador Emu;
        private int TargetFPS;
        private bool estaPausado;
        private string nombreROM = string.Empty;

        // variables para cada tecla
        const int TECLA_1 = 0;
        const int TECLA_2 = 1;
        const int TECLA_3 = 2;
        const int TECLA_4 = 3;
        const int TECLA_Q = 4;
        const int TECLA_W = 5;
        const int TECLA_E = 6;
        const int TECLA_R = 7;
        const int TECLA_A = 8;
        const int TECLA_S = 9;
        const int TECLA_D = 10;
        const int TECLA_F = 11;
        const int TECLA_Z = 12;
        const int TECLA_X = 13;
        const int TECLA_C = 14;
        const int TECLA_V = 15;

        // constructor
        public Chip8()
        {
            InitializeComponent();
            
            Emu = new Emulador();
            this.Text = "Emulador Chip-8";
            this.KeyDown += new KeyEventHandler(this.Tecla_Presionada);
            this.KeyUp += new KeyEventHandler(this.Tecla_Liberada);
            SdlDotNet.Core.Events.Tick += new EventHandler<TickEventArgs>(Events_Tick);
            TargetFPS = 60;
            SdlDotNet.Core.Events.Fps = 60;
            estaPausado = false;
        }

        // Evento clic en opci�n del men� "Cargar Juego"
        private void cargarJuegoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            openFileDialog1.FileName = string.Empty;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                nombreROM = openFileDialog1.FileName;
                Reset();
                SdlDotNet.Core.Events.Run();
            }
        }

        // Resetamos el hardware (registros, memoria) y cargamos de nuevo el juego
        void Reset()
        {
            Emu.ResetHardware();
            Emu.CargarJuego(nombreROM);            
        }

        // Game-Loop
        void Events_Tick(object sender, TickEventArgs e)
        {
            this.Text = "Emulador Chip-8  FPS: " + SdlDotNet.Core.Events.Fps;
            if (estaPausado == false)
            {
                Emu.EmulaFrame();
                UpdateDisplay();
            }
        }

        // Actualiza control gr�fico con lo que entrega el Surface de la clase Emulador.cs
        private void UpdateDisplay()
        {
            scDisplay.Blit(Emu.SurfaceOut);
            scDisplay.Update();
        }

        // Evento gatillado al cerrar la aplicaci�n con la "x" de la ventana
        private void Chip8_FormClosed(object sender, FormClosedEventArgs e)
        {
            CerrarAplicacion();
        }

        // Evento gatillado al presionar Pausa
        private void pausaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (estaPausado == false)
                estaPausado = true;
            else
                estaPausado = false;
        }

        // Evento gatillado al presionar Reset
        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Reset();
        }

        // Evento gatillado al presionar Salir
        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CerrarAplicacion();
        }

        // M�todo que termina los eventos y cierra la aplicaci�n
        void CerrarAplicacion()
        {
            SdlDotNet.Core.Events.Close();
            SdlDotNet.Core.Events.QuitApplication();
            Application.Exit();
        }

        private void Tecla_Presionada(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.NumPad1) { Emu.teclasPresionadas[TECLA_1] = true; }
            if (e.KeyCode == Keys.NumPad2) { Emu.teclasPresionadas[TECLA_2] = true; }
            if (e.KeyCode == Keys.NumPad3) { Emu.teclasPresionadas[TECLA_3] = true; }
            if (e.KeyCode == Keys.NumPad4) { Emu.teclasPresionadas[TECLA_4] = true; }
            if (e.KeyCode == Keys.Q) { Emu.teclasPresionadas[TECLA_Q] = true; }
            if (e.KeyCode == Keys.W) { Emu.teclasPresionadas[TECLA_W] = true; }
            if (e.KeyCode == Keys.E) { Emu.teclasPresionadas[TECLA_E] = true; }
            if (e.KeyCode == Keys.R) { Emu.teclasPresionadas[TECLA_R] = true; }
            if (e.KeyCode == Keys.A) { Emu.teclasPresionadas[TECLA_A] = true; }
            if (e.KeyCode == Keys.S) { Emu.teclasPresionadas[TECLA_S] = true; }
            if (e.KeyCode == Keys.D) { Emu.teclasPresionadas[TECLA_D] = true; }
            if (e.KeyCode == Keys.F) { Emu.teclasPresionadas[TECLA_F] = true; }
            if (e.KeyCode == Keys.Z) { Emu.teclasPresionadas[TECLA_Z] = true; }
            if (e.KeyCode == Keys.X) { Emu.teclasPresionadas[TECLA_X] = true; }
            if (e.KeyCode == Keys.C) { Emu.teclasPresionadas[TECLA_C] = true; }
            if (e.KeyCode == Keys.V) { Emu.teclasPresionadas[TECLA_V] = true; }
        }

        private void Tecla_Liberada(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.NumPad1) { Emu.teclasPresionadas[TECLA_1] = false; }
            if (e.KeyCode == Keys.NumPad2) { Emu.teclasPresionadas[TECLA_2] = false; }
            if (e.KeyCode == Keys.NumPad3) { Emu.teclasPresionadas[TECLA_3] = false; }
            if (e.KeyCode == Keys.NumPad4) { Emu.teclasPresionadas[TECLA_4] = false; }
            if (e.KeyCode == Keys.Q) { Emu.teclasPresionadas[TECLA_Q] = false; }
            if (e.KeyCode == Keys.W) { Emu.teclasPresionadas[TECLA_W] = false; }
            if (e.KeyCode == Keys.E) { Emu.teclasPresionadas[TECLA_E] = false; }
            if (e.KeyCode == Keys.R) { Emu.teclasPresionadas[TECLA_R] = false; }
            if (e.KeyCode == Keys.A) { Emu.teclasPresionadas[TECLA_A] = false; }
            if (e.KeyCode == Keys.S) { Emu.teclasPresionadas[TECLA_S] = false; }
            if (e.KeyCode == Keys.D) { Emu.teclasPresionadas[TECLA_D] = false; }
            if (e.KeyCode == Keys.F) { Emu.teclasPresionadas[TECLA_F] = false; }
            if (e.KeyCode == Keys.Z) { Emu.teclasPresionadas[TECLA_Z] = false; }
            if (e.KeyCode == Keys.X) { Emu.teclasPresionadas[TECLA_X] = false; }
            if (e.KeyCode == Keys.C) { Emu.teclasPresionadas[TECLA_C] = false; }
            if (e.KeyCode == Keys.V) { Emu.teclasPresionadas[TECLA_V] = false; }
        }
    }
}
</pre>

<br>
Chip8.Designer.cs
<pre style="font-size:12; background-color:#D8D8D8;">
namespace Chip8
{
    partial class Chip8
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        // Control gr�fico dentro de la ventana que permite mostrar el juego
        private SdlDotNet.Windows.SurfaceControl scDisplay;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Chip8));
            this.scDisplay = new SdlDotNet.Windows.SurfaceControl();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.cargarJuegoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cargarJuegoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.resetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pausaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.scDisplay)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // scDisplay
            // 
            this.scDisplay.AccessibleDescription = "SdlDotNet SurfaceControl";
            this.scDisplay.AccessibleName = "SurfaceControl";
            this.scDisplay.AccessibleRole = System.Windows.Forms.AccessibleRole.Graphic;
            this.scDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.scDisplay.BackColor = System.Drawing.Color.Black;
            this.scDisplay.Image = ((System.Drawing.Image)(resources.GetObject("scDisplay.Image")));
            this.scDisplay.InitialImage = null;
            this.scDisplay.Location = new System.Drawing.Point(0, 27);
            this.scDisplay.Name = "scDisplay";
            this.scDisplay.Size = new System.Drawing.Size(640, 320);
            this.scDisplay.TabIndex = 0;
            this.scDisplay.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cargarJuegoToolStripMenuItem,
            this.pausaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(634, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // cargarJuegoToolStripMenuItem
            // 
            this.cargarJuegoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cargarJuegoToolStripMenuItem1,
            this.resetToolStripMenuItem,
            this.salirToolStripMenuItem});
            this.cargarJuegoToolStripMenuItem.Name = "cargarJuegoToolStripMenuItem";
            this.cargarJuegoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.cargarJuegoToolStripMenuItem.Text = "Archivo";
            // 
            // cargarJuegoToolStripMenuItem1
            // 
            this.cargarJuegoToolStripMenuItem1.Name = "cargarJuegoToolStripMenuItem1";
            this.cargarJuegoToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.cargarJuegoToolStripMenuItem1.Text = "Cargar Juego";
            this.cargarJuegoToolStripMenuItem1.Click += new System.EventHandler(this.cargarJuegoToolStripMenuItem1_Click);
            // 
            // resetToolStripMenuItem
            // 
            this.resetToolStripMenuItem.Name = "resetToolStripMenuItem";
            this.resetToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.resetToolStripMenuItem.Text = "Reset";
            this.resetToolStripMenuItem.Click += new System.EventHandler(this.resetToolStripMenuItem_Click);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // pausaToolStripMenuItem
            // 
            this.pausaToolStripMenuItem.Name = "pausaToolStripMenuItem";
            this.pausaToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.pausaToolStripMenuItem.Text = "Pausa";
            this.pausaToolStripMenuItem.Click += new System.EventHandler(this.pausaToolStripMenuItem_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Chip8
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 344);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.scDisplay);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Chip8";
            this.Text = "Chip8";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Chip8_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.scDisplay)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cargarJuegoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pausaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cargarJuegoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}
</pre>

Con esto tenemos un emulador con unas simples opciones que nos permiten cargar ROMs (por ahora solo de Chip-8, faltan algunos ajustes para que soporte ROMs de Super Chip-8), puede Resetear, Pausar, tiene sonido y 
funciona el teclado. Todo esto gracias a que nos basamos en el c�digo fuente de los cap�tulos anteriores.
<br><br>
Aqu� tenemos im�genes del juego <b>PONG2</b> Y <b>ANIMAL RACE</b> corriendo en nuestro emulador. Como se ven, a�n quedan detalles 
por arreglar en la emulaci�n, ya que la imagen se ve un poco "desplazada" a la derecha y parpadea mucho, haciendo que no se vean las paletas en el juego PONG2:
<br><br>
<a href="imag/chip8_cap715.png"><img src="imag/chip8_cap715.png" width="20%" height="20%"></a> <a href="imag/chip8_cap716.png"><img src="imag/chip8_cap716.png" width="20%" height="20%"></a>
<br><br>
Bajar <a href="chip8_vs2008_SDLNetMode_cap7.rar">aqu�</a> el proyecto .Net para Visual Studio 2008 que contiene los fuentes de este cap�tulo.

<br><br>
<a href="emulador6.php">Lecci�n Anterior</a> | <a href="emulador_menu.php">�ndice</a>

<?php
include '../../piecdisq.php';
?>
