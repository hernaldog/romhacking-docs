<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>Tutorial de creaci�n de un Emulador sencillo de Chip-8 con VS 2008 y C# (Parte 3)</title>
</head>

<body>
<small>
<span style="font-family: Verdana;">
<a href="http://darknromhacking.com/">Volver a P�gina Principal</a>
</small>

<hr style="width: 100%; height: 2px;">

<table width="100%">
<tr>
<td>

<H3>
<span style="font-weight: bold;">Tutorial de creaci�n de un Emulador sencillo de Chip-8 con VS 2008 y C# (Parte 3)</span>
</H3>
<small>
Lenguaje: C#<br>
Para: VS 2008 con Sdl.Net 6.1<br>
Por Dark-N: <a href="mailto:hernaldog@gmail.com">hernaldog@gmail.com</a>
<br>

<span style="font-family: Verdana;">
<a href="http://darknromhacking.com/">http://darknromhacking.com</a>
<br>
Hilo del Foro: <a href="http://foro.romhackhispano.org/viewtopic.php?f=4&t=872">
http://foro.romhackhispano.org/viewtopic.php?f=4&t=872</a>
</small></td>
<td align="center">
</table>



<hr style="width: 100%; height: 2px;">

<span style="font-family: Verdana;"><small>
<a href="emulador2.php">Lecci�n Anterior</a> | <a href="emulador_menu.php">�ndice</a> | <a href="emulador4.php">Siguiente Lecci�n</a>

<H3>La idea</H3>

La idea en este cap�tulo es hacer el core de emulador, es decir, la parte que se encarga de cargar la ROM, leer las instrucciones y luego ejecutarlas en el "ciclo fetch".
<br>Solo veremos las 2 primeras instrucciones en este cap�tulo. Tampoco se ver� el manejo de Timers.


<H3>Programa Principal y Ciclo de Ejecuci�n</H3>

Lo primero que hacemos es instanciar la clase y lamar al m�todo principal <b>Run()</b>.
<br>
<pre style="font-size:12; background-color:#D8D8D8;">
static void Main(string[] args)
{
	Emulador emulador = new Emulador();
	emulador.Run();
}
</pre>


Luego definimos el m�todo principal <b>Run</b>, donde a parte de realizar las tareas de <b>Reseteo de Hardware</b> y <b>Carga del Juego</b>, tambi�n se realiza la m�s importante tarea conocida como <b>Ciclo de Ejecuci�n</b> o <b>game-loop</b>, que es la particularidad que tienen los juegos, con respecto a otras aplicaciones, que significa que es un ciclo infinito que no se detiene o cambia a menos que haya alguna acci�n interactiva. �Se han fijado que en cualquier juego no se detiene a menos que uno salga a alg�n men� o apague la consola? Incluso hay un ciclo infinito cuando el juego est� en pausa, ya que muestra la misma imagen una y otra vez.

<br>
<pre style="font-size:12; background-color:#D8D8D8;">
void Run()
{			
	ResetHardware();

	if (CargarJuego() == true)   
	{
		while (true) //game-loop   
		{
			EjecutaFech();
			ManejaTimers();
		}
	}
}
</pre>

En Resumen, adentro del programa principal se tiene:<br><br>
<li><b>Reseteo de Hardware</b>: simulamos que lo que hace la m�quina, o el chip-8 en este caso, al partir.
<li><b>Carga del Rom o juego</b>
<li><b>Ciclo del Juego o Game-Loop</b>. Si solo el paso anterioe es ok, se realiza el 3er. paso que corresponde al ciclo del juego donde se tienen 2 etapas: 1. Ejecuci�n del ciclo <b>Fetch</b> donde internamente se leen y ejecutan las instrucciones. 2. Manejo de los <b>timers</b>.
<br><br>

Como se ve arriba, en C# se puede utilizar el <b>while(true)</b> para simular un ciclo infinito, donde no se detendr� a menos que se cierre la aplicaci�n o se ejecute la sentencia <b>break</b>.
<br>


<H3>Reseteo de Hardware</H3>

Todos los emuladores deben simular el inicio de la m�quina o chip que emulan, en nuestro caso debemos simular el reseteo del chip-8. Los pasos a realizar son:
<br><br>

<li>Reseteo de los timers
<li>Reseteo de Registros generales
<li>Limpiado del Registro V
<li>Limpiado de memoria
<li>Limpiado de la Pila
<li>Carga de Fuentes a Memoria

<br>
<pre style="font-size:12; background-color:#D8D8D8;">
void ResetHardware()
{
	// Reseteo de Timers.
	delayTimer = 0x0;
	soundTimer = 0x0;
	
	// Reseteo de Registros generales
	opcode = 0x0;
	PC = DIR_INICIO;
	SP = 0x0;
	I = 0x0;            
	
	// Limpiado del Registro V
	for (int regActual = 0; regActual < CANT_REGISTROS; regActual++)
	{
	    memoria[regActual] = 0x0;
	}
	
	// Limpiado de memoria
	for (int dir = 0; dir < TAMANO_MEM; dir++)
	{
	    memoria[dir] = 0x0;
	}
	
	// Limpiado de la Pila
	for (int item = 0; item < TAMANO_PILA; item++)
	{
	    pila[item] = 0x0;
	}
	
	// Carga de Fuentes a Memoria (eran 80 bytes, 5 byte por cada una de las 16 letras)
	for (int i = 0; i < 80; i++)
	{
	    memoria[i] = arregloFuentes[i];
	}
}
</pre>

<H3>Carga de la ROM (y como se carga en la Memoria RAM)</H3>

Como estamos simulando el verdadero comportamiento del Chip-8, debemos cargar la ROM ley�ndola byte a byte con el m�todo <b>FileStream.ReadByte()</b> para cargarla desde la direcci�n 0x200, que en nuestro c�digo ser�a memoria[DIR_INICIO]+0, memoria[DIR_INICIO]+1, memoria[DIR_INICIO]+2, etc. La forma m�s pr�ctica de abrir la ROM es con la clase <b>FileStream ([archivo a abrir],[modo Open])</b>:
<br>

<br>
<pre style="font-size:12; background-color:#D8D8D8;">
bool CargarJuego()
{
	string nombreRom = "PONG";
	FileStream rom;
	
	try
	{
		rom = new FileStream(@nombreRom, FileMode.Open);
		
		if (rom.Length == 0)
		{
			Console.Write("Error: ROM da�ada o vac�a");
			return false;
		}
		
		// Comenzamos a cargar la rom a la memoria a partir de la dir 0x200
		for (int i = 0; i < rom.Length; i++)
			memoria[DIR_INICIO + i] = (byte)rom.ReadByte();
		
		rom.Close();
		return true;
	}
	catch (Exception ex)
	{
		Console.Write("Error general al cargar la ROM. " + ex.Message);
		return false;
	}
}
</pre>

Es muy importante entender como es la carga en la Memoria del Chip-8 (cuando hablamos de Memoria nos referimos a la Memoria <b>RAM</b>, por si acaso ya que la otra memoria, el juego, es la Memoria <b>ROM</b> o memoria de solo lectura). 
<br>
Nosotros lo cargamos "byte a byte" leyendo de la ROM y copi�ndolo a la Memoria.
Ahora, la memoria del Chip-8 parte desde la direcci�n </b>0x000</b> (0000 0000 0000 binario) y termina en la posici�n <b>0xFFF</b> (4095 decimal o 1111 1111 1111 Binario).
<br><br>
Por otro lado, en C# el tipo de dato <b>byte</b> va del <b>0</b> al <b>255</b> o lo que es lo mismo del <b>00</b> al <b>FF</b>.
<br><br>
Vamos paso a paso, primero revisando los c�digos Hexadecimales de la ROM "PONG":
<br><br>
<img src="imag/pong_hex.PNG">
<br><br>
Por otro lado, la descripci�n inferior muestra como est� la Memoria <b>inicialmente</b> y como va quedando a medida que se carga con los bytes de la ROM "PONG":
<br>
<pre style="font-size:12; background-color:#D8D8D8;">
+---------------+= 0xFFF (4095) Fin RAM de Chip-8
|               |
|               |
| 0x200 a 0xFFF |
|  Programas de |
|    Chip-8 o   |
|    espacio    |
|  para datos   |
|               |
+---------------+= 0x200 (512) Inicio de la mayor�a de los programas de Chip-8
| 0x000 a 0x1FF |
| Reservado al  |
|  interprete   |
+---------------+= 0x000 (0) Inicio de la Memoria RAM de Chip-8
</pre>


Luego si cargamos el primer byte de la ROM, el <b>6A</b> que se ve en la imagen superior, por lo tanto tenemos:
<br>
<pre style="font-size:12; background-color:#D8D8D8;">
+-------+= 0xFFF (4095)
|       |
|       |
|       |
|       |
|       |
|       |
|       |
|       |
+--6 A -+= 0x200 (512)
|       |
|       |
|       |
+-------+= 0x000 (0)
</pre>


Luego si cargamos el 2do byte de la ROM, el <b>02</b> que se ve en la imagen superior:
<br>
<pre style="font-size:12; background-color:#D8D8D8;">

+-------+= 0xFFF (4095)
|       |
|       |
|       |
|       |
|       |
|       |
|       |
|  0 2  |  0x201   -> aqu� queda el 02
+--6 A -+= 0x200 (512)
|       |
|       |
|       |
+-------+= 0x000 (0)
</pre>

Luego si cargamos el 3er byte de la ROM, el <b>6B</b>:
<br>
<pre style="font-size:12; background-color:#D8D8D8;">

+-------+= 0xFFF (4095)
|       |
|       |
|       |
|       |
|       |
|       |
|  6 B  |  0x202 (514) -> aqu� queda el 6B
|  0 2  |  0x201
+--6 A -+= 0x200 (512)
|       |
|       |
|       |
+-------+= 0x000 (0)
</pre>

Para finalizar el ejemplo cargamos el 4er byte de la ROM, el <b>0C</b>:
<br>
<pre style="font-size:12; background-color:#D8D8D8;">

+-------+= 0xFFF (4095)
|       |
|       |
|       |
|       |
|       |
|  0 C  |  0x203    -> aqu� queda el 0C
|  6 B  |  0x202 (514)
|  0 2  |  0x201
+--6 A -+= 0x200 (512)
|       |
|       |
|       |
+-------+= 0x000 (0)
</pre>


<H3>Ejecuci�n del Ciclo Fetch</H3>

La implementaci�n original del Chip-8 tiene un total de 36 instrucciones, incluyendo funciones matem�ticas, gr�ficas y de control de flujo. 
El Super Chip-48 tiene 10 m�s, haciendo un total de 46.<br>
Cada instrucci�n es de 2 bytes y se almacena en la memoria con el byte m�s significante primero (por ejemplo, si tengo la instruccion ABC2, que es de 2 bytes, en la memoria se almacena primero el AB y luego el 02).<br><br>

El Ciclo Fetch se divide en 2 fases: 
<li>1. Lectura de cada instrucci�n desde la memoria
<li>2. Ejecuci�n de cada una de las Instrucciones
<br>

<h4>Lectura de las Instrucciones desde la  Memoria</h4>

En este paso vamos a leer las diferentes <b>instrucciones</b> que est�n en memoria ya que se cargaron previamente desde la ROM. Las instrucciones que leeremos son por ejemplo "Salta a una Subrutina" (el caso del opcode 0NNN, despu�s veremos que son los <b>Opcodes</b>) o "Suma el Registro VX con el registro KK" (el caso del opcode 7XKK). La instrucci�n se arma haciendo 2 lecturas a la Memoria, primero con el bytes m�s significativo y luego el menos significativo. Se utiliza el Program Counter para ir "moviendose" a trav�s de la memoria. 
<br>Estos son los pasos en un seudo c�digo:<br><br>
<li>1) leer el byes m�s significativo, es decir leer el valor de memoria[PC]
<li>2) leer el byes menos significativo, es decir leer el valor de memoria[PC+1]
<li>3) nuestra <b>instrucci�n</b>, que sabemos que es de 2 bytes, se arma de la forma: <b>A.</b> muevo 1) a la izquierda 8 pocisiones (1 byte) y luego <b>B.</b> se "une" lo anterior A. con 2), no me refiero a sumar, sino colocado uno al lado de otro.
<br>
<br>
Vamos por partes, como realizamos el paso <b>A</b>:<br>
Para mover ciertos bits de 1) a la izquierda se debe recordar el <a href="emulador1.php">Cap�tulo 1</a>, por lo que usamos un shift a la izquierda de 8 valores, de forma quedar�a:<br>
&nbsp;&nbsp;&nbsp; memoria[PC] << 8
<br>
Lo que internamente seg�n el cap�tulo 1 es lo mismo que memoria[PC] * 2^<sup>8</sup>.
<br>
<br>
Como se realiza el paso <b>B</b>:<br>
La unica forma de "unir" byes es con el comando OR (en C# es con "|"). De esta forma queda:
<br>
&nbsp;&nbsp;&nbsp; Instruccion = memoria[PC] << 8 | memoria[PC+1]

<br>
<br>
Finalmente siempre luego de asignar la Instruccion se debe <b>incrementar el Program Counter (PC)</b> para el siguiente ciclo, por lo tanto se deja PC + 2. No es PC + 1 como uno com�nmente pudiera pensar, ya que el PC salta de 2 en 2 ya que como se dijo en el paso anterior, la Instrucci�n lee de a 2 filas de la memoria (2 bytes).

<br>
<pre style="font-size:12; background-color:#D8D8D8;">
void EjecutaFech()
{
	// leemos cada una de las instrucciones desde la memoria. 
	// cada instruccion es de 2 bytes
	instruccion = memoria[PC] << 8 | memoria[PC + 1];
	
	// dejamos incrementado el Program Counter en 2, lista para leer 
	// la siguiente instruccion en el siguiente ciclo.
	PC += 2;
</pre>

<b>Ejemplo de lectura de Instrucci�n usando los 2 primeros ciclos de PONG</b>
<br><br>
Se sabe por la imagen de los Bytes que forman el juego PONG que los 4 primeros bytes son: 6A, 02, 6B y 0C.
<br>Ahora, ejecutemos el paso 1) para esto tomamos el primer btye 6A:<br><br>
000000001101010 bin = 6A = 106 dec en memoria[PC] y PC inicialmente es 512 = 0x200 <br>
<br>Luego ejecutemos el paso 2) para esto tomamos el 2do byte, 02:<br><br>
000000000000010 bin = 2   en memoria[PC+1] = en memoria[513]<br>
<br>Luego ejecutamos el paso A) aplicando shift a la izquierda del paso 1) en 8 pociciones:<br>
<br>
000000001101010 bin << 8  = 110101000000000<br>
<br>
Ahora ejecutamos el paso B) que "une" 1) con 2):<br><br>
instruccion = 000000001101010 << 8 | 000000000000010
<br>
instruccion = 110101000000010 = 27138 dec = <b>0x6A02</b>.
<br><br>Finalmente el resultado es la uni�n de los 2 bytes.
<br>
<br>
Ahora si ejecutamos el 2do ciclo, vamos m�s r�pido:
<br><br>

Tomanos el 3er byte, 6B, lo movemos 8 bits a la izquierda y luego lo unimos con el 4to byte, 0C, lo que nos da:
<br>instruccion = <b>0x6B0C</b>
<br>

<H4>Ejecuci�n de las Instrucciones</H4>

Esta etapa se divide en 2:
<br><br>
<li>1. "Divisi�n" de dicha instrucci�n en distintos bloques de bits llamados <b>Opcodes</b>
<li>2. Ejecuci�n de cada instrucci�n utilizando los Opcodes

<br><br><br>
<b>1. Divisi�n de la Instrucci�n en Opcodes</b>
<br><br>
A partir de la Instrucci�n, se debe obtener el valor del Registro KK y de los 4 Opcodes de 4 bits cada uno. La idea de los opcodes es ayudarnos a leer la instrucci�n, de esta forma se separa la instrucci�n en 4 bloques de 4 bits cada uno. Cada conjunto de bits representa un Opcode:
<br><br>
<li>Opcode1: represente la 4 bits mayores
<li>Opcode X: representa los 4 bits menores del byte mayor
<li>Opcode Y: representa los 4 bits mayores del byte menor
<li>Opcode N: representa los 4 bits menores
<li>Opcodes NNN: representan los 12 bits menores
<br>
<pre style="font-size:12; background-color:#D8D8D8;">

Instrucci�n (2 bytes): 6A02 = 0110 1010 0000 0010 binario
+----------+-----------+----------+-----------+
|          |           |          |           |
| Opcode1  | Opcode X  | Opcode Y | Opcode N  |
|  0110    |   1010    |  0000    |   0010    |
+----------+-----------+----------+-----------+
|          |         Opcode  NNN              |
|          |         10100000010              |
+----------+-----------+----------+-----------+
</pre>
Continuando con el c�digo fuente, esta parte ser�a as�:
<br>
<pre style="font-size:12; background-color:#D8D8D8;">
void EjecutaFech()
{
	...
	
	//obtengo el valor del registro KK, de largo 1 byte, el m�s chico de la instrucci�n
	KK = (instruccion & 0x00FF);
	
	// cada opcode es de 4 bit
	opcode1 = (instruccion & 0xF000) >> 12; //los 4 bits mayores de la instrucci�n
	opcode2 = (instruccion & 0x0F00) >> 8;  //X
	opcode3 = (instruccion & 0x00F0) >> 4;  //Y
	opcode4 = (instruccion & 0x000F) >> 0;  //Opcode N = los 4 bits menores de la instrucci�n

	//obtengo el valor del opcode NNN
	NNN = (instruccion & 0x0FFF);
</pre>
Si no entiendes lo anterior, en el <a href="emulador1.php">Cap�tulo 1</a> est� claramente explicado como extraer los bits para cada uno de los 4 opcodes utilizando el AND (&) y el shift a la derecha.

<br><br>
<b>2. Ejecuci�n de cada instrucci�n</b>
<br><br>
Esta es la parte m�s larga de realizar del emulador, ya que se debe simular cada una de las 35 instrucciones. Para ir en orden, iremos siguiendo las instrucciones seg�n aparecen en <a href="http://devernay.free.fr/hacks/chip8/C8TECH10.HTM">Cowgod's Chip-8</a> ya que est�n muy ordenadas cada una de las instrucciones. Si miras dicha tabla de instrucciones se ve que primero est� 0NNN, luego 00E0, luego 1NNN, 2NNN, 3XKK, y as� sucesivamente.

<br><br>
<b>Instrucciones del tipo 0XXX (Opcode1 = 0)</b>
<br><br>
Au� tenemos las 2 primeras instrucciones:
<br><br>

<li>1. 00E0 - CLS: Limpiar pantalla (clear screen)
<li>2. 00EE - RET: Retorno de una subrutina (Return From Subroutine)

<br><br>Nota: No se implementar� la instrucci�n 0XXX - SYS debido a que seg�n Cowgod's Chip-8, esta es ignorada por la mayor�a de los interpretes actuales.

<br>
<pre style="font-size:12; background-color:#D8D8D8;">
// Ejecutamos las instrucciones a trav�s de los opcodes
switch (opcode1)
{
	// opcodes del tipo 0xxx
	case (0x0):
	{
		switch (instruccion)
		{
			// opcode 00E0: Clear Screen.
			case (0x00E0):
			{
				ClearScreen();
				break;
			}
			// opcode 00EE: Return From Subroutine.
			case (0x00EE):
			{
				ReturnFromSub();
				break;
			}
		}
		break;
	}
...
}
</pre>

Notar que despu�s de cada "switch" tiene que existir un "break" que le indica al compilador .Net que salga del Case actual y contin�e su ejecuci�n.
<br>La implementaci�n de estos 2 m�todos <b>ClearScreen</b> y <b>ReturnFromSub</b> es la sgte:
<br><br>
<b>1. 00E0 - CLS: Limpiar pantalla (clear screen)</b>
<br>
<pre style="font-size:12; background-color:#D8D8D8;">
void ClearScreen()
{
	// No se limpia la pantalla directamente, sino el arreglo que la representa
	// asignandole un valor 0
	for (int p =0; p < RES_X; p++)
		for (int q = 0; q < RES_Y; q++)
			arregloPantalla[p,q] = 0;					
}
</pre>
<br>
<b>2. 00EE - RET: Retorno de una subrutina (Return From Subroutine)</b>
<br><br>
Aqu� la acciones son 2: 1ro se debe decrementar el puntero de la Pila (Stack Pointer o SP) en 1, luego se debe rescatar el valor que est� all� guardado y asignar a la direcci�n actual (PC) que se ejecutar�.
<br>Recordar que cada vez que se <b>lea</b> una direcci�n desde la Pila, primero se <b>decrementa el SP en 1</b> y luego lee de la Pila.
<br>Y para el caso que se quiera <b>guardar</b> un valor en la Pila, primero se guarda el valor en la Pila y luego se debe <b>aumenta el SP en 1</b>.
<br>
<pre style="font-size:12; background-color:#D8D8D8;">
void ReturnFromSub()
{
	// Se diminuye el SP en 1
	SP--;
	
	// Apuntamos el PC (que apunta a la sgte. instrucci�n a ejecutar) a la
	// posici�n salvada en la Pila
	PC = pila[SP];
}
</pre>

Bueno , esto es por ahora. Falta por explicar el resto de las instruciones, pero eso lo veremos en el siguiente cap�tulo. 
<br><br>

<hr style="width: 100%; height: 2px;"><br>

C�digo fuente de esta parte (C# 2.0):
<br>
<pre style="font-size:12; background-color:#D8D8D8;">
using System;
using System.IO;
using System.Runtime.InteropServices;  //para "clearscreen" y el "beep" de la Bios

namespace Chip8_ConsoleMode
{
    class Emulador
    {
        #region variables emulador

        //variables principales        
        const int RES_X = 64;  //resolucion de pantalla 64x32
        const int RES_Y = 32;        
        const int DIR_INICIO = 0x200;
        const int TAMANO_MEM = 0xFFF;
        const int TAMANO_PILA = 16;
        const int CANT_REGISTROS = 16;
        int[,] arregloPantalla = new int[RES_X, RES_Y];

        //arreglo que representa la memoria
        int[] memoria = new int[TAMANO_MEM];

        //Arreglo que representa los 16 Registros (V) 
        int[] V = new int[CANT_REGISTROS];

        //arreglo que representa la Pila
        int[] pila = new int[TAMANO_PILA];

        //variables que representan registros varios
        int instruccion;  //representa una instruccion del Chip-8. Tiene largo 2 byte
        int PC;
        int I;
        int SP;
        int KK;
        
        //variables para manejar los opcodes
        int opcode1 = 0;
        int opcode2 = 0;  //X
        int opcode3 = 0;  //Y
        int opcode4 = 0;
        int NNN = 0;

        //variables que representan los 2 timers: Delay Timer y Sound Timer
        int delayTimer;
        int soundTimer;

        //variables para el manejo de fuentes (80 bytes, ya que hay 5 bytes x caracter 
        //y son 16 caracteres o letras (5x16=80). Cada font es de 4x5 bits.
        int[] arregloFuentes = {
		   0xF0, 0x90, 0x90, 0x90, 0xF0,	// valores para 0
		   0x60, 0xE0, 0x60, 0x60, 0xF0,	// valores para 1
		   0x60, 0x90, 0x20, 0x40, 0xF0,	// valores para 2
		   0xF0, 0x10, 0xF0, 0x10, 0xF0,	// valores para 3
		   0x90, 0x90, 0xF0, 0x10, 0x10,	// valores para 4
		   0xF0, 0x80, 0x60, 0x10, 0xE0,	// valores para 5
		   0xF0, 0x80, 0xF0, 0x90, 0xF0,	// valores para 6
		   0xF0, 0x10, 0x10, 0x10, 0x10,	// valores para 7
		   0xF0, 0x90, 0xF0, 0x90, 0xF0,	// valores para 8
		   0xF0, 0x90, 0xF0, 0x10, 0x10,	// valores para 9
		   0x60, 0x90, 0xF0, 0x90, 0x90,	// valores para A
		   0xE0, 0x90, 0xE0, 0x90, 0xE0,	// valores para B
		   0x70, 0x80, 0x80, 0x80, 0x70,	// valores para C
		   0xE0, 0x90, 0x90, 0x90, 0xE0, 	// valores para D
		   0xF0, 0x80, 0xF0, 0x80, 0xF0,	// valores para E
		   0xF0, 0x90, 0xF0, 0x80, 0x80		// valores para F
		};

        private bool[] teclasPresionadas = { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false };

        const int TECLA_1 = 0;
        const int TECLA_2 = 1;
        const int TECLA_3 = 2;
        const int TECLA_4 = 3;
        const int TECLA_Q = 4;
        const int TECLA_W = 5;
        const int TECLA_E = 6;
        const int TECLA_R = 7;
        const int TECLA_A = 8;
        const int TECLA_S = 9;
        const int TECLA_D = 10;
        const int TECLA_F = 11;
        const int TECLA_Z = 12;
        const int TECLA_X = 13;
        const int TECLA_C = 14;
        const int TECLA_V = 15;

        // mapeamos las 16 teclas de Chip8
        private byte[] MapeoTeclas = 
        {	       
            0x01,0x02,0x03,0x0C,
            0x04,0x05,0x06,0x0D,
            0x07,0x08,0x09,0x0E,
            0x0A,0x00,0x0B,0x0F 
        };

        //[DllImport("Kernel32.dll")] //para beep
        //public static extern bool Beep(UInt32 frequency, UInt32 duration);
        //En Framework 2.0 se puede usar directamente: Console.Beep(350, 50);

        [DllImport("msvcrt.dll")] //Framwork 1.1: para cls (clear screen) en DOS
        public static extern int system(string cmd);
        //En Framework 2.0 se puede usar directamente: Console.Clear();

        //Variable de tipo Random (para generar n�meros aleatoios) utilizada por ciertas instrucciones
        Random rnd = new Random();

        #endregion


        static void Main(string[] args)
        {
            Emulador emulador = new Emulador();
            emulador.Run();
        }

        void Run()
        {
            ResetHardware();

            if (CargarJuego() == true)
            {
                while (true) //game-loop
                {
                    EjecutaFech();
                    //ManejaTimers(); //no implementado aun
                }
            }
        }


        void ResetHardware()
        {
            // Reseteo de Timers
            delayTimer = 0x0;
            soundTimer = 0x0;

            // Reseteo de Registros generales
            instruccion = 0x0;
            PC = DIR_INICIO;
            SP = 0x0;
            I = 0x0;            

            // Limpiado del Registro V
            for (int regActual = 0; regActual < CANT_REGISTROS; regActual++)
            {
                memoria[regActual] = 0x0;
            }

            // Limpiado de memoria
            for (int dir = 0; dir < TAMANO_MEM; dir++)
            {
                memoria[dir] = 0x0;
            }

            // Limpiado de la Pila
            for (int item = 0; item < TAMANO_PILA; item++)
            {
                pila[item] = 0x0;
            }

            // Carga de Fuentes a Memoria (eran 80 bytes, 5 byte por cada una de las 16 letras)
            for (int i = 0; i < 80; i++)
            {
                memoria[i] = arregloFuentes[i];
            }
        }

        bool CargarJuego()
        {
            string nombreRom = "PONG";
            FileStream rom;

            try
            {
                rom = new FileStream(@nombreRom, FileMode.Open);

                if (rom.Length == 0)
                {
                    Console.Write("Error: ROM da�ada o vac�a");
                    return false;
                }

                // Comenzamos a cargar la rom a la memoria a partir de la dir 0x200
                for (int i = 0; i < rom.Length; i++)
                    memoria[DIR_INICIO + i] = (byte)rom.ReadByte();

                rom.Close();
                return true;
            }
            catch (Exception ex)
            {
                Console.Write("Error general al cargar la ROM. " + ex.Message);
                return false;
            }
        }

        void EjecutaFech()
        {
            #region lectura de instrucciones

            // leemos cada una de las instrucciones desde la memoria. 
            // cada instruccion es de 2 bytes
            instruccion = memoria[PC] << 8 | memoria[PC + 1];

            // dejamos incrementado el Program Counter en 2, lista para leer 
            // la siguiente instruccion en el siguiente ciclo.
            PC += 2;

            #endregion

            #region extracci�n de opcodes

            //obtengo el valor del registro KK, de largo 1 byte, el m�s chico de la instrucci�n
            KK = (instruccion & 0x00FF);

            // cada opcode es de 4 bit
            opcode1 = (instruccion & 0xF000) >> 12; //los 4 bits mayores de la instrucci�n
            opcode2 = (instruccion & 0x0F00) >> 8;  //X
            opcode3 = (instruccion & 0x00F0) >> 4;  //Y
            opcode4 = (instruccion & 0x000F) >> 0;  //Opcode N = los 4 bits menores de la instrucci�n

            //obtengo el valor del opcode NNN
            NNN = (instruccion & 0x0FFF);

            #endregion

            #region ejecuci�n de instrucciones

            // Ejecutamos las instrucciones a travez de los opcodes
            switch (opcode1)
            {
                // opcodes del tipo 0xxx
                case (0x0):
                    {
                        switch (instruccion)
                        {
                            // opcode 00E0: Clear Screen.
                            case (0x00E0):
                                {
                                    ClearScreen();
                                    break;
                                }
                            // opcode 00EE: Return From Subroutine.
                            case (0x00EE):
                                {
                                    ReturnFromSub();
                                    break;
                                }
                        }
                        break;
                   }
                   
                //....
            }

            #endregion
        }

        #region implementaci�n de m�todos para cada instrucci�n

        void ClearScreen()
        {
            // No se limpia la pantalla directamente, sino el arreglo que la representa
            // asignandole un valor 0
            for (int p = 0; p < RES_X; p++)
                for (int q = 0; q < RES_Y; q++)
                    arregloPantalla[p, q] = 0;
        }

        void ReturnFromSub()
        {
            // Se diminuye el SP en 1
            SP--;

            // Apuntamos el PC (que apunta a la sgte. instrucci�n a ejecutar) a la
            // posici�n salvada en la Pila
            PC = pila[SP];
        }

        #endregion

    }
}
</pre>

<a href="emulador2.php">Lecci�n Anterior</a> | <a href="emulador_menu.php">�ndice</a> | <a href="emulador4.php">Siguiente Lecci�n</a>

<?php
include '../../piecdisq.php';
?>
