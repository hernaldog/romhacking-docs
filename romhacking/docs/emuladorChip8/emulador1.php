<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>Tutorial de creaci�n de un Emulador sencillo de Chip-8 con VS 2008 y C# (Parte 1)</title>
</head>

<body>
<small>
<span style="font-family: Verdana;">
<a href="http://darknromhacking.com/">Volver a P�gina Principal</a>
</small>

<hr style="width: 100%; height: 2px;">

<table width="100%">
<tr>
<td>

<H3>
<span style="font-weight: bold;">Tutorial de creaci�n de un Emulador sencillo de Chip-8 con VS 2008 y C# (Parte 1)</span>
</H3>
<small>
Lenguaje: C#<br>
Para: VS 2008 con Sdl.Net 6.1<br>
Por Dark-N: <a href="mailto:hernaldog@gmail.com">hernaldog@gmail.com</a>
<br>

<span style="font-family: Verdana;">
<a href="http://darknromhacking.com/">http://darknromhacking.com</a>
<br>
Hilo del Foro: <a href="http://foro.romhackhispano.org/viewtopic.php?f=4&t=872">
http://foro.romhackhispano.org/viewtopic.php?f=4&t=872</a><br>
</span>
(Agradecimientos a <a href="mailto:ivanzinhoa@gmail.com">Ivan Dario Ospina Acosta</a> por encontrar detalles en este cap�tulo que he corregido)
</small></td>
<td align="center">
</table>


<hr style="width: 100%; height: 2px;">

<span style="font-family: Verdana;"><small>
<a href="emulador_menu.php">�ndice</a> | <a href="emulador2.php">Siguiente Lecci�n</a>

<H3>La idea</H3>

En esta primera parte aprenderemos lo aspectos b�sicos para poder programar un emulador, como lo son, las operaciones b�sicas con bits, shifting, como extraer bits de un direcci�n de varios bytes y como unir bits.

<br>
<H4>Herramientas Utilizadas	</H4>
<li>Lenguaje de programaci�n <b>C#</b> 2.0 con <b>Visual Studio 2008</b>.
<li>Multimedia (gr�ficos, sonido, eventos de teclado) usar� <a href="http://cs-sdl.sourceforge.net/index.php/Main_Page">SDL.NET 6.1</a>.
<br>NOTA: este no es un tutorial de SDL.NET, si quieren saber m�s de esta librer�a recomiendo que visiten mi <a href="..\..\tutoriales\sdl_net\SDL_NET_menu.htm">tutorial</a> de desarrollo de juegos con SDL .NET que est� en este sitio.
<li>La <a href="roms/PONG">Rom</a> de PONG.

<br><br>

<H3>Operaciones b�sicas con Bits</H3>
<H4>Operaciones l�gicas</H4>

Para nuestro emulador usaremos est�s operaciones que se aplican sobre bits (hay m�s): <br><br>
<li><b>AND</b>: && y &: todos los elementos a comparar deben ser 1(true) para que el resultado sea 1(true). La regla general del AND es:<br>
-True AND True = True (1 AND 1 = 1)<br>
-True AND False = False (1 AND 0 = 0)<br>
-False AND True = False (0 AND 1 = 0)<br>
-False AND False = False (0 AND 0 = 0)<br>

<br><li><b>OR</b>: || y |: si un elemento es 1 (true), el resultado es 1 (true). La regla general del OR es:<br>
-True OR True = True (1 Xor 1 = 1)<br>
-True OR False = True (1 Xor 0 = 1)<br>
-False OR True = True (0 Xor 1 = 1)<br>
-False OR False = False (0 Xor 0 = 0)<br>


<br><li><b>XOR</b>: ^ : Es conocido como un "Or exclusivo", o para entender mas f�cil es como un "or" diferente. La regla es: si los 2 valores a evaluar son 1 (true) o 0 (false) el resultado es 0 (false), pero si solo uno es 1(true), el resultado es 1(true). La regla general del XOR es:<br>
-True XOR True = False (1 XOR 1 = 0)<br>
-True XOR False = True (1 XOR 0 = 1)<br>
-False XOR True = True (0 XOR 1 = 1)<br>
-False XOR False = False (0 XOR 0 = 0)<br>

<br>
Los operadores && y || se diferencia de & y | en que los primeros realizan la evaluaci�n "perezosa" y los segundos no. La evaluaci�n perezosa consiste en que si el resultado de evaluar el primer operando permite deducir el resultado de la operaci�n, entonces no se eval�a el segundo y se devuelve dicho resultado directamente, mientras que la evaluaci�n no perezosa consiste en evaluar siempre ambos operandos. 
<br>A modo de ejemplo, si el primer operando de una operaci�n && es falso se devuelve false directamente, sin evaluar el segundo; y si el primer operando de una || es cierto se devuelve true directamente, sin evaluar el otro. 


<br><br>Ejemplos generales:
<pre style="font-size:12; background-color:#D8D8D8;">
         0011 (3)
   OR(|) 0101 (5)
  -----------
         0111 (7)
 
         0011 (3)
  AND(&) 0101 (5)
  -----------
         0001 (1)
 
         0011 (3)
  Xor(^) 1010 (10)
  -----------
         1001 (9)
  	   
         0101 (5)
  Xor(^) 0001 (1)
  -----------
         0100 (4)
	   
         0000 (0)
  Xor(^) 0001 (1)
  -----------
         0001 (1)
</pre>
Ejemplos en C#:
<pre style="font-size:12; background-color:#D8D8D8;">
  int a = 0xAF;       //175 = 10101111
  int b = 0x05;       //  5 = 00000101

  int EjAND = a & b;  //   10101111 
                      // & 00000101 
                      //   --------
                      //   00000101  = 5

  int EjOR  = a | b;  //   10101111 
                      // | 00000101 
                      //   --------
                      //   10101111  = 175

  int EjXOR = a ^ b;  //   10101111 
                      // & 00000101 
                      //   --------
                      //   10101010  = 170
</pre>



<H4>Operadores Shift</H4>

Estos operadores sirven para multiplicar por 2 bits (&lt;&lt;) o dividir por 2 bits (&gt;&gt;), pero en nuestro emuladores los usaremos para "extraer" valores de bytes. Por ejemplo si tenemos 0x6A0B, podemos con los Operadores Shift obtener en una variable <i>opcodeA = 0x6A</i> y otra <i>opcodeB = 0x0B</i>.
<br><br>
<b>&nbsp;&nbsp;&lt;&lt; </b>: Shift a la izquierda, sirve para <b>multiplicar</b> por 2 el valor
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>F�rmula: x = y &lt;&lt; z </b> &nbsp;&nbsp;&nbsp;&nbsp; => es lo mismo que: <b>x = y * 2<sup>z</sup></b>
<br>
<br>
<b>&nbsp;&nbsp;&gt;&gt; </b>: Shift a la derecha, sirve para <b>dividir</b> por 2 el valor
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>F�rmula: x = y &gt;&gt; z </b> &nbsp;&nbsp;&nbsp;&nbsp; => es lo mismo que: <b>x = y / 2<sup>z</sup></b>

<br><br><b>&nbsp;&nbsp;&nbsp;&nbsp;Ejemplos Shift a la izquierda:</b><br>
<pre style="font-size:12; background-color:#D8D8D8;">
uint valor = 3;             // binario:    00000011
uint valor2 = valor << 1;   // Resultado = 00000110 = 6  (3 * 2^1)
uint valor3 = valor << 4;   // Resultado = 00110000 = 48 (3 * 2^4)
uint valor4 = 8 << 3;   // Resultado = 64 (8* 2^3)

//Overflow de bit, cuando al mover a la derecha o izquierda no hay espacio:
uint opcode = 240;   // 11110000
uint opcode = opcode << 1;  // se pueden dar 2 cosas: 
                            // si opcode solo almacena valores de 8 bits = 11100000 (se pierde un 1) = 224
                            // si opcode solo almacena valores mayores de 8 bits = 111100000 (se agrega un 1) = 480
</pre>

Fijarse que Shift a la izquierda "movemos" todos los bits a la izquierda.

<br><br><b>&nbsp;&nbsp;&nbsp;&nbsp;Ejemplos Shift a la derecha:</b><br>
<pre style="font-size:12; background-color:#D8D8D8;">
uint valor = 240; // 11110000
uint valor2 = valor >> 1;  // Resultado = 01111000 = 120 (240 / 2^1)
uint valor3 = valor >> 4;  // Resultado = 00001111 = 15  (240 / 2^4)

int opcode = 10;
opcode = opcode >> 0; // resultado opcode = 10;

//Overflow de bit, cuando al mover a la derecha no hay espacio, se pueden producir inconsistencias en los resultados:
uint opcode = 15;           // 00001111
uint opcode = opcode >> 1;  // 00001111 se corre a la derecha y se "pierde un 1" = 00000111 = 7
</pre>

Fijarse que con Shift a la derecha "movemos" todos los bits a la derecha.

<br><br><b>&nbsp;&nbsp;&nbsp;&nbsp;Ejemplo real</b></br><br>

Analicemos la primera instrucci�n de PONG. Dicha ROM carga en memoria estos datos:<br><br>
Memoria[512] = 106;<br>
Memoria[513] = 2;<br>

<br>Luego tenemos en el c�digo:
<pre style="font-size:12; background-color:#D8D8D8;">
  int PC = 0x200;  
  int instruccion = memoria[PC] << 8 | memoria[PC+1];
  int opcode = (instruccion & 0x0F00) >> 8;
</pre><
�que valor tendr� la variable <b>opcode</b> al final?
<br><br>
Sabemos que PC = 0x200 = 512. Entonces vamos por parte y escribamos <i>instruccion</i>:<br><br>

int instruccion = 106 << 8 | 2;

<br><br>
Ahora resolvamos 106 << 8 = 01101010 << 8 = 27136
<br><br>
Luego 27136 | 2 = 0110101000000000 | 00000010 = 0110 1010 0000 0010 = 27138 => convertimos a hex = 0x6A02<br>
Entonces instruccion = 0x6A02. 

<br><br>Ahora vamos con la parte que falta:<br><br>
int opcode = (0x6A02 & 0x0F00) >> 8;

<br><br>
0x6A02 & 0x00F0 = 0110101000000010 & 111100000000 = 101000000000 = 2560
<br><br>
2560 >> 8 = 101000000000 >> 8 = corremos 8 veces los bits a la derecha:<br><br>
101000000000 inicio<br>
010100000000 1 vez<br>
001010000000 2<br>
000101000000 3<br>
000010100000 4<br>
000001010000 5<br>
000000101000 6<br>
000000010100 7<br>
000000001010 8 = > 10
<br><br>
Finalmente queda:<br>
int opcode = 10<br><br>

<H4>Como extraer bits de una direcci�n</H4>
Una cosa muy comun en un emulador es que hagas c�culos con ciertas partes de un una direcci�n y no con la direcci�n completa. Por ejemplo, si la direcci�n es 0xAAB0 y queremos revisar que los bytes mayores sean mayores a cero, esto ser�a algo como <i>0xAA > 0</i>. Como dije arriba, para poder extraer opcodes de una una direcci�n (en chip-8 se opera con 12 bits) se debe usar Shift a la derecha o izquierda. Otra cosa a tener en cuenta es que en CHIP-8 las direcciones pueden ser de 16 bits aunque nunca se hacen operaciones con estos 16 bits, sin� que el int�rprete (registro I) a los m�s, usa los 12 bits menores. Los 4 bits superiores se usan para la carga de Fuentes.

<br><br>Casos posibles<br>
<pre style="font-size:12; background-color:#D8D8D8;">
Direcci�n (16 bits)  = 0000 0000 0000 0000
     Podemos sacar:
                       A    B    C    D   (A B C y D por separado, cada uno de 4 bits)
                       N    N    K    K   (N y K, cada uno de 8 bits)
                            Z    Z    Z   (Z de 12 bits)
</pre>
Esta es una forma de obtener cada uno de esas letras por separado usando C# es usar <b>&(AND)</b> y <b>&gt;&gt;</b> (Shift a la derecha):
<pre style="font-size:12; background-color:#D8D8D8;">
A = (Direcci�n & 0xF000) >> 12 = (Direcci�n AND 1111 0000 0000 0000) >> 12   -> movemos todo a la derecha 12 espacios
B = (Direcci�n & 0x0F00) >> 8  = (Direcci�n AND 0000 1111 0000 0000) >> 8    -> movemos todo a la derecha 8 espacios
C = (Direcci�n & 0x00F0) >> 4  = (Direcci�n AND 0000 0000 1111 0000) >> 4    -> movemos todo a la derecha 4 espacios
D = (Direcci�n & 0x000F)       = (Direcci�n AND 0000 0000 0000 FFFF)         -> no se mueve
N = (Direcci�n & 0xFF00) >> 8  = (Direcci�n AND 1111 1111 0000 0000) >> 8    -> movemos todo a la derecha 8 espacios
K = (Direcci�n & 0x00FF)       = (Direcci�n AND 0000 0000 1111 1111)         -> no se mueve
Z = (Direcci�n & 0x0FFF)       = (Direcci�n AND 0000 1111 1111 1111)         -> no se mueve
</pre>
Ahora calculemos cada letra tomando como base la direcci�n anterior <i>0x6A02 = 0110 1010 0000 0010</i>
<pre style="font-size:12; background-color:#D8D8D8;">
A = (0x6A02 & 0xF000) >> 12 = (0110 1010 0000 0010 & 1111 0000 0000 0000) >> 12 = 0110 = 6
B = (0x6A02 & 0x0F00) >> 8  = 1010 = 10
C = (0x6A02 & 0x00F0) >> 4  = 0000 = 0
D = (0x6A02 & 0x000F)       = 0010 = 2 
N = (0x6A02 & 0xFF00) >> 8  = (0110 1010 0000 0010 & 1111 1111 0000 0000) >> 8 = 0110 1010  = 106
K = (0x6A02 & 0x00FF)       = (0110 1010 0000 0010 & 0000 0000 1111 1111) =  0000 0010      = 2
Z = (0x6A02 & 0x0FFF)       = (0110 1010 0000 0010 & 0000 1111 1111 1111) =  1010 0000 0010 = 2562
</pre>
<b>Leer y unir BITS</b><br><br>

Para mostrar como "Leer" bits, imagina que tenemos un arreglo <i>miArreglo</i> con 2 elementos: <i>miArreglo[1] = 106</i> y <i>miArreglo[2] = 3</i><br>
Podemos crear una variable <i>X</i> que contenga el elemento de <i>miArreglo[1]</i>:

<pre style="font-size:12; background-color:#D8D8D8;">
  int x = miArreglo[1];  //x valdr�a 106

Para leer bits usaremos arreglos.
</pre>
Para mostrar como "Unir" se utiliza la instrucci�n <b>| (OR)</b>. Ejemplo:<br>
La variable <i>x</i> contendr� el contenido del <i>miArreglo[1]= 106</i> junto con <i>miArreglo[2] = 3</i>:
<pre style="font-size:12; background-color:#D8D8D8;">
  int x = miArreglo[1] | miArreglo[2];   //x valdr�a 107

  Internamente tenemos:
  x = 106 | 3 = 0110 1010 | 0000 0011 = 0110 1010 = 0110 1011 = 107

CADA vez que se unen bits, NO SE SUMAN, sin� que se "pegan" uno al lado de otro
</pre>

<a href="emulador_menu.php">�ndice</a> | <a href="emulador2.php">Siguiente Lecci�n</a>
<?php
include '../../piecdisq.php';
?>