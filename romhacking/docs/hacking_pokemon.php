<!DOCTYPE html>
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>La Web de Dark-N - Hacking de Pokemon Cristal, Oro, Plata, Amarillo, Rojo, Azul, Rubi, Zafiro, Esmeralda, Rojo Fuego, Verde Hoja y Soul Silver</title>
<META NAME="keywords" CONTENT="hacking, pokemon, red, blue, cristal, amarillo, yellow, edicion, items, masterball, legandario, pokemon, capturar, rubi, zafiro, esmeralda" />
<META NAME="description" CONTENT="Tutorial para hacking de pokemon Cristal, Plata, Oro, Amarillo, Rojo, Azul, Rubi, Zafiro, Esmeralda, Rojo Fuego, Verde Hoja y Soul Silver." />

<link rel="shortcut icon" href="../imag/favicon.ico" type="image/x-icon" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

<style type="text/css">
  .glyphicon {
    cursor: pointer;
  }
</style>

</head>
<body>
<div id="cuerpo" style="margin: 20px">
<small>
<span style="font-family: Verdana;">

<?php 
if (isset($_GET["origen"]))
	$linkVolver = $_GET["origen"] . ".php";
else
	$linkVolver = "index.php";
?>
<a href="<?php echo "../" . $linkVolver ?>">Volver</a>

<hr style="width: 100%; height: 1px;">
<center>
<h2>Hackear encuentros e &iacute;tems en Pokemon Cristal, Plata, Oro, Amarillo, Rojo, Azul, Rub&iacute;, Zafiro, Esmeralda, Rojo Fuego, Verde Hoja y Soul Silver</h2>


Por Dark-N: <?php include ('../mailsolo.php'); ?>
<br><a href="http://darknromhacking.com/">http://darknromhacking.com</a><br>
Versi&oacute;n 1.5, revisado el 03 de Marzo 2015.
<br><br>

<img width="90px" height="90px" src="img/pokemon_yellow_cover.jpg"> 
<img width="90px" height="90px" src="img/pokemon_red_cover.jpg"> 
<img width="90px" height="90px" src="img/pokemon_blue_cover.jpg">

<br>

<img width="90px" height="90px" src="img/pokemon_cristal_cover.jpg"> 
<img width="90px" height="90px" src="img/pokemon_silver_cover.jpg"> 
<img width="90px" height="90px" src="img/pokemon_oro_cover.jpg"> 

<br>

<img width="90px" height="90px" src="img/pokemon_rubi_cover.png"> 
<img width="90px" height="90px" src="img/pokemon_zafiro_cover.png"> 
<img width="90px" height="90px" src="img/pokemon_esmeralda_cover.jpg"> 
<img width="90px" height="90px" src="img/pokemon_rojo_fuego_cover.jpg"> 
<img width="90px" height="90px" src="img/pokemon_verde_hoja_cover.png">

<br>

<img width="90px" height="90px" src="img/pokemon_soul_silver_cover.jpg">

</center>

<hr style="width: 100%; height: 1px;">

<H3>NOTAS</H3> 

<div class="alert alert-warning" role="alert">
<li><font color="blue">Utiliza este hacking una vez hayas agotado <b>TODOS</b> tus recursos, o no puedas capturar a ese Pokemon legandario que tanto te costado, no lo uses siempre ya que la "magia" de Pokemon se esfuma y no te dar&aacute;n ganas de jugar otro Pokemon en un futuro.</font></li>
<li>Este hacking lo realic&eacute; para las versiones en <b>espa&ntilde;ol</b> de los juegos, no he probado con las versiones en ingl&eacute;s. Si alguien lo ha probado con las versiones en americanas(US) que me avise en los comentarios o por correo.</li>

<li>Este material es exclusivo ya que NO esta en la web, ni en ingl&eacute;s ni en espa&ntilde;ol. Con mis conocimientos de romhacking lo logr&eacute; descubrir.</li>
</div>

<H3>Requerimientos</H3>

<li>Tener acceso a la Calculadora de Windows en modo Programador (men&uacute; Ver).</li>
<br>
<li>Conocer la codificaci&oacute;n Hexadecimal:
<br>
&nbsp;&nbsp;&nbsp;AYUDA: 0 decimal=0 hexadecimal, 1 dec=1 hex,...,9 dec=9 hex, 10 dec=0A hex, 11 dec=0B hex, 15 dec=0F hex, 16 dec = 10 hex, etc...
<br>
&nbsp;&nbsp;&nbsp;<img width="200px" height="200px" src="img/hacking_poke_calc_1.png"> <img width="200px" height="200px" src="img/hacking_poke_calc_2.png">
</li>
<br>
<li>Tener la <b>ROM</b> del <a href="../pokemon/PokemonCristal.gbc">Pokemon Cristal</a>, <a href="../pokemon/PokemonPlata.gbc">Pokemon Plata</a>, 
 <a href="../pokemon/PokemonOro.gbc">Pokemon Oro</a>, <a href="../pokemon/PokemonAmarillo.gbc">Pokemon Amarillo</a>, <a href="../pokemon/PokemonRed.gb">Pokemon Rojo</a>,  
 <a href="../pokemon/PokemonBlue.gb">Pokemon Azul</a>, <a href="../pokemon/PokemonRubiEsp.rar">Pokemon Rub&iacute;</a>, 
 <a href="../pokemon/PokemonZafiroEsp.rar">Pokemon Zafiro</a>, <a href="../pokemon/PokemonEsmeraldaEsp.zip">Pokemon Esmeralda</a>, <a href="../pokemon/PokemonRojoFuegoEsp.zip">Pokemon Rojo Fuego</a> o <a href="../pokemon/PokemonVerdeHojaEsp.zip">Pokemon Verde Hoja</a>.</li> 
<br>
<li>Tener el excelente emulador de Game Boy Color <a href="../archivos/VisualBoyAdvance-1.8.0-beta3.zip">Visual Boy Advance 1.8.0 Beta3</a> o <a href="../archivos/NoGBA2.6a.rar">No$GBA 2.6a</a> (para Pokemon Esmeralda si no puedes corregir el problema de la pantalla blanca con el VisualBoyAdvance).</li>
<br>
<li>Bajar un <b>Editor Hexadecimal</b> como <a href="../archivos/translhextion16c.zip">Transhextion</a> o un editor que personalmente me gusta como <a href="http://www.editpadlite.com/">EditPad</a> (con Control + H buscas c&aacute;digos hex) y conocer b&aacute;sicamente como operan sus funciones: 
<br>
&nbsp;&nbsp;&nbsp;GOTO OFFSET, FIND TEXT, etc...</li>

<H3>1. Pelear contra cualquier pokemon salvaje (sirve para Pokemon Cristal / Plata / Oro / Rojo / Azul / Amarillo / Rub&iacute; / Zafiro / Esmeralda / Rojo Fuego / Verde Hoja / Soul Silver)</H3>

<li>Abre el editor hexadecimal que bajaste.</li>
<li>Luego abre el juego:
<br>
<br>

<div class="alert alert-warning" role="alert">

&nbsp;&nbsp;&nbsp;Para <b>Pokemon Cristal</b> abrir por ejemplo PokemonCrystal.GBC y luego ve a la direcci&oacute;n <b>2AE09</b> que corresponde a la Ruta 29 que es al comienzo del juego.
<br>
&nbsp;&nbsp;&nbsp;Para <b>Pokemon Plata</b> y <b>Pokemon Oro</b> ir a la direcci&oacute;n <b>2B3B6</b> que corresponde a la Ruta 29 que es al comienzo del juego.
<br>
&nbsp;&nbsp;&nbsp;Para <b>Pokemon Rojo</b> y <b>Pokemon Azul</b> ve a la direcci&oacute;n <b>D0E0</b> que corresponde a la Ruta 1 que es al principio del juego.
<br>
&nbsp;&nbsp;&nbsp;Para <b>Pokemon Amarillo</b> ve a la direcci&oacute;n <b>CD8C</b> que corresponde a la Ruta 1 que es al principio del juego.
<br>
&nbsp;&nbsp;&nbsp;Para <b>Pokemon Rub&iacute;</b> ve a la direcci&oacute;n <b>39FA7C</b> que corresponde a la Ruta 101 al comienzo del juego.
<br>
&nbsp;&nbsp;&nbsp;Para <b>Pokemon Zafiro</b> ve a la direcci&oacute;n <b>39F8C4</b> que corresponde a la Ruta 101 al comienzo del juego.
<br>
&nbsp;&nbsp;&nbsp;Para <b>Pokemon Esmeralda</b> ve a la direcci&oacute;n <b>553E40</b> que corresponde a la Ruta 101 al comienzo del juego.
<br>
&nbsp;&nbsp;&nbsp;Para <b>Pokemon Rojo Fuego</b> ve a la direcci&oacute;n <b>3C4550</b> que corresponde a la Ruta 1 al comienzo del juego.
<br>
&nbsp;&nbsp;&nbsp;Para <b>Pokemon Verde Hoja</b> ve a la direcci&oacute;n <b>3C438C</b> que corresponde a la Ruta 1 al comienzo del juego.
<br>
&nbsp;&nbsp;&nbsp;Para <b>Pokemon Soul Silver</b> ve a la direcci&oacute;n <b>28E5778</b> que corresponde a la Ruta 29 al comienzo del juego. NOTA: Esta direcci&oacute;n es para los pastos de <b>d&iacute;a</b> (no de noche), si es de noche en el mundo real, cambia tu hora en tu computador a las 10 AM por ejemplo y carga el juego de nuevo en el emulador para que cambie a d&iacute;a.
</div>


&nbsp;&nbsp;&nbsp;Con translhextion usa la funci&oacute;n <b>Jump to</b> con Control + G. Tambi&eacute;n con translhextion debes colocar una "x" antes para indicarle que es una direcci&oacute;n hex, la "h" al final puede o no ir, es decir la b&uacute;squeda x2AE09 y x2AE09h son lo mismo.
<br>
&nbsp;&nbsp;&nbsp;Por ejemplo, el Jump To para el Pokemon Cristal ser&iacute;a:
<br>
&nbsp;&nbsp;&nbsp;<img src="img/hacking_poke_cristal_0.png">
<br>
<br>
<li>En la l&iacute;nea correspondiente a esa direcci&oacute;n del editor hex ver&aacute;s algo como: 
<br><br>
&nbsp;&nbsp;&nbsp;<b>19 19 19 02 10 02 A1 03 10 03 A1 02 13 03 BB...</b>
</li>

<br>
<img src="img/hacking_poke_cristal_1.png">
<br><br>
Esto se lee del <b>02 10</b> en adelante y significa para <b>Pokemon Cristal/Plata/Oro</b>:
<br><br>
&nbsp;&nbsp;&nbsp;<b>0210</b>  --> nivel <b>2</b> del pokemon con c&oacute;digo 10, que es un <b>Pidgey</b> seg&uacute;n tabla de m&aacute;s abajo.
<br>
&nbsp;&nbsp;&nbsp;<b>02A1</b>  --> nivel <b>2</b> del pokemon con c&oacute;digo A1, que es un <b>Sentret</b> seg&uacute;n tabla de m&aacute;s abajo.
<br>
&nbsp;&nbsp;&nbsp;M&aacute;s adelante se lee:
<br>
&nbsp;&nbsp;&nbsp;<b>0213</b>  --> nivel <b>2</b> del pokemon con c&oacute;digo 13, que es un <b>Rattata</b> seg&uacute;n tabla de m&aacute;s abajo.
<br>
&nbsp;&nbsp;&nbsp;...
<br>
<br>
En el <b>Pokemon Rojo/Azul</b> los c&oacute;digos se ven as&iacute:
<br><br>
<img src="img/hacking_poke_red_1.png">

<br><br>
Esto se lee as&iacute; <b>03 24 03 A5 ...</b> y significa para <b>Pokemon Rojo/Azul</b>:
<br><br>
&nbsp;&nbsp;&nbsp;<b>0324</b>  --> nivel <b>3</b> del pokemon con c&oacute;digo 24, que es un <b>Pidgey</b> seg&uacute;n tabla de m&aacute;s abajo.
<br>
&nbsp;&nbsp;&nbsp;<b>03A5</b>  --> nivel <b>3</b> del pokemon con c&oacute;digo A5, que es un <b>Rattata</b> seg&uacute;n tabla de m&aacute;s abajo.
<br>
&nbsp;&nbsp;&nbsp;...

<br><br>

En el <b>Pokemon Rub&iacute;</b> y <b>Pokemon Zafiro</b> los c&oacute;digos se ven as&iacute:
<br>
<br>
<img src="img/hacking_poke_rubi_1.png">
<br><br>

Esto se lee as&iacute; <b>02 02 22 01 02 02 20 01 ... 03 03 1E 01</b> y significa para <b>Pokemon Rub&iacute;/Zafiro</b>:
<br><br>
&nbsp;&nbsp;&nbsp;<b>0202 2201</b>  --> 02 02 es el nivel <b>2</b> (aun no se porque hay dos veces 02) del pokemon con c&oacute;digo 2201, que es un <b>Wurmple</b> seg&uacute;n tabla de m&aacute;s abajo.
<br>
&nbsp;&nbsp;&nbsp;<b>0202 2001</b>  --> 02 02 es el nivel <b>2</b> (aun no se porque hay dos veces 02) del pokemon con c&oacute;digo 2001, que es un <b>Zigzagoon</b> seg&uacute;n tabla de m&aacute;s abajo.
<br>
&nbsp;&nbsp;&nbsp;<b>0303 1E01</b>  --> 03 03 es el nivel <b>3</b> (aun no se porque hay dos veces 03) del pokemon con c&oacute;digo 1E01, que es un <b>Poochyena</b> seg&uacute;n tabla de m&aacute;s abajo.
<br>
&nbsp;&nbsp;&nbsp;...

<br><br>
En el <b>Pokemon Soul Silver</b> de Nintendo DS, el hacking es para los primeros pastos de la ruta 29, tal como en la foto:

<br><br>
<img width="500px" height="250px" src="img/hacking_poke_soul_silver1.JPG">
<br><br>

El editor Translhextion se cae al cargar la ROM de Pokemon Soul Silver :(, as&iacute; que usar&eacute; el <a href="http://www.editpadlite.com/">EditPad</a>. Con Control + G se va a una direcci&oacute;n:

<br><br>
<img src="img/hacking_poke_soul_silver2.JPG">
<br><br>

Ver&aacute;s el sistema de c&oacute;digos del Pokemon Soul Silver, el cual es un poco distinto a los anteriores Pokemons: <b>00A1 0010 ... 0013</b> que significa 00A1=Sentret, 0010=Pidgey y 0013=Rattata. Esos <b>02 03 02 03 03...04 04</b> son los niveles de cada Pokemon de ese sector:

<br><br>
<img src="img/hacking_poke_soul_silver3.JPG">
<br><br>

<div class="alert alert-success" role="alert">
Mi olfato me dice que este sistema de c&oacute;digos de Pokemon en Soul Silver debe ser id&eacute;ntico en el Pokemon Perla y Pokemon Diamante...no he investigado, pero si es as&iacute; y encuentras las direcciones m&aacute;ndame un correo a <a href="mailto:hernaldog@gmail.com">hernaldog@gmail.com</a> y te dar&eacute; los cr&eacute;ditos respectivos claro.
</div>

Cambiamos esos c&oacute;digos <b>017F 017F ... 017F</b>, el 017F es el c&oacute;digo de <b>Groudon</b> (ver m&aacute;s abajo la lista completa con los c&oacute;digos de todos los Pokemons):

<br><br>
<img src="img/hacking_poke_soul_silver4.JPG">
<br><br>

Ups, tenemos en <b>Soul Silver</b> un Groudon salvaje:

<br><br>
<img width="500px" height="250px" src="img/hacking_poke_soul_silver5.JPG">

<br><br>
<div class="alert alert-info" role="alert">
En <b>Pokemon Cristal/Plata/Oro</b> los c&oacute;digos <b>19 19 19</b> indica que sigue la siguiente ruta o camino en el juego, por lo que dichos c&oacute;digos no debemos modificarlos.
<br>
<br>
En <b>Pokemon Rojo/Azul</b> el c&oacute;digo <b>19</b> es el separador de pueblo.
<br>
<br>
En <b>Pokemon Amarillo</b> el c&oacute;digo <b>00 19</b> es el separador de pueblo.
<br><br>
En <b>Pokemon Rub&iacute;/Zafiro</b> el c&oacute;digo <b>14 00 00 00</b> es el separador de pueblo.
<br><br>
En <b>Pokemon Rojo Fuego / Verde Hoja</b> el c&oacute;digo <b>15 00 00 00</b> es el separador de pueblo.
</div>


Ahora s&oacute;lo hay que cambiar el c&oacute;digo del pokemon que quieres que aparezca y si quieres tambi&eacute;n el nivel del pokemon.
<br>
Recuerda que son n&uacute;meros hexadecimales, entonces si est&aacute;s modificando el <b>Pokemon Cristal/Plata/Oro</b> y quieres que aparezca en vez de un <b>Pidgey</b> nivel 2 salga un <b>Charizard</b> nivel 80 debes reemplazar <b>0210</b> por <b>5006</b> ya que el nivel 02 se cambia por 50 (usa la calculadora de Windows para calcular que 80 decimal es 50 hexadecimal) lo que indica el nivel y 10 que es Pidgey por un 06 que es Charizard.

<br>
Este Pokemon salvaje se puede capturar con una PokeBall pero seg&uacute;n el nivel del Pokemon puede costarte m&aacute;s o menos.
<br>     
Si quieres reemplazas varias series de c&oacute;digos para que s&oacute;lo te aparezcan Charizard nivel 80 en la primera ruta, es decir cambiar esos c&oacute;digos que viste:

<br><br>
<b>0210 02A1 0310 03A1 0213 03BB 03BB 0210</b>
<br>
<br>
por
<br><br>
<b>5006 5006 5006 5006 5006 5006 5006 5006</b>

<br><br>
Puedes cambiar muchos m&aacute;s pares de c&oacute;digo si quieres como en la imagen de abajo. Cuando termines salva con Control + S:
<br><br>
<img src="img/hacking_poke_cristal_2.png">
<br><br>

En el <b>Pokemon Red</b> podemos poner los c&oacute;digos <b>03B403B4</b> etc... El B4 es Charizard y 03 el nivel 3, (recuerda que puedes poner el c&oacute;digo del Pokemon que quieras seg&uacute;n la lista de c&oacute;digos de m&aacute;s abajo)
<br><br>
<img src="img/hacking_poke_red_2.png">
<br><br>

Para <b>Pokemon Plata</b> y <b>Pokemon Oro</b> tenemos que la direcci&oacute;n era <b>2B3B9</b>:
<br><br>
<img src="img/hacking_poke_plata_1.png">
<br><br>
Hacemos que toda la primera ruta est&eacute; repleto de Charizard nivel 80. Recordemos que los c&oacute;digos <b>19 19 19</b> indica que sigue la siguiente ruta o camino en el juego, por lo que dichos c&oacute;digos no debemos modificarlos.
<br><br>
<img src="img/hacking_poke_plata_2.png">

<br><br>

En el <b>Pokemon Rub&iacute;/Zafiro</b> podemos poner los c&oacute;digos <b>02 02 95 01 ... 03 03 95 01</b> etc... es decir,  mantenemos los mismos niveles 02 02 (nivel 2) o 03 03 (nivel 3) pero cambiamos el c&oacute;digo del Pokemon por <b>9501</b> que es nada m&aacute;s ni nada menos que <b>Groudon</b>.
<br><br>
<img src="img/hacking_poke_rubi_2.png">
<br><br>

Volviendo al <b>Pokemon Cristal</b>, si entramos al juego y vamos a la primera ruta (si ya tienes una partida creada, usa siempre <b>"Continuar"</b> el juego y no <b>Load State</b> con F1 para asegurarte que en el juego veas el hacking):
<br><br>
<img width="250px" height="250px" src="img/hacking_poke_cristal_3.png">
<br><br>

Nuestro Cyndaquil est&aacute; con un nivel 5 reci&eacute;n:
<br><br>
<img width="250px" height="250px" src="img/hacking_poke_cristal_4.png">
<br><br>

Upps, &#161;sali&oacute; un Charizard nivel 80 en el primera ruta! Este hacking lo puedes combinar con el hacking que explico m&aacute;s abajo para capturarlo de una jeje.
<br><br>
<img width="250px" height="250px"  src="img/hacking_poke_cristal_5.png"> <img width="250px" height="250px" src="img/hacking_poke_cristal_6.png">
<br><br>
En el <b>Pokemon Red</b> igual puedes capturar a Charizard:
<br><br>
<img width="250px" height="250px"  src="img/hacking_poke_red_3.png">

<br><br>
En el <b>Pokemon Rub&iacute;</b> nos encontramos con un <b>Groudon</b> salvaje, yeah!:
<br><br>
<img width="300px" height="250px"  src="img/hacking_poke_rubi_3.png">


<H4>1.1 C&oacute;digos hexadecimales de los Pokemons</H4>

<div id="listaPokemon" class="panel panel-default collapse in">

  <div class="panel-heading">
  <div class="row">
    <div class="col-md-6"><h5 class="panel-title">Lista con los c&oacute;digos de cada Pokemon</h5></div>
    <div class="col-md-6"><span class="glyphicon glyphicon-chevron-up pull-right cursor-pointer"  data-toggle="collapse" data-target="#tablaPokemon"></span></div>
  </div>
  </div>

<div class="collapse in" id="tablaPokemon">
<div class="row">

<div class="col-md-3">

<h4>Pokemon Rojo / Azul / Amarillo</h4>

<table class="table table-striped">
<thead>
<tr>
    <th>C&oacute;digo</th>
    <th>Nombre</th>
</tr>
</thead>
<tbody>
<tr><td>01</td><td>Rhydon</td></tr>
<tr><td>02</td><td>Kangaskhan</td></tr>
<tr><td>03</td><td>Nidoran (M)</td></tr>
<tr><td>04</td><td>Clefairy</td></tr>
<tr><td>05</td><td>Spearow</td></tr>
<tr><td>06</td><td>Voltorb</td></tr>
<tr><td>07</td><td>Nidoking</td></tr>
<tr><td>08</td><td>Slowbro</td></tr>
<tr><td>09</td><td>Ivysaur</td></tr>
<tr><td>0A</td><td>Exeggutor</td></tr>
<tr><td>0B</td><td>Lickitung</td></tr>
<tr><td>0C</td><td>Exeggcute</td></tr>
<tr><td>0D</td><td>Grimer</td></tr>
<tr><td>0E</td><td>Gengar</td></tr>
<tr><td>0F</td><td>Nidoran (F)</td></tr>
<tr><td>10</td><td>Nidoqueen</td></tr>
<tr><td>11</td><td>Cubone</td></tr>
<tr><td>12</td><td>Rhyhorn</td></tr>
<tr><td>13</td><td>Lapras</td></tr>
<tr><td>14</td><td>Arcanine</td></tr>
<tr><td>15</td><td>Mew</td></tr>
<tr><td>16</td><td>Gyarados</td></tr>
<tr><td>17</td><td>Shellder</td></tr>
<tr><td>18</td><td>Tentacool</td></tr>
<tr><td>19</td><td>Gastly</td></tr>
<tr><td>1A</td><td>Scyther</td></tr>
<tr><td>1B</td><td>Staryu</td></tr>
<tr><td>1C</td><td>Blastoise</td></tr>
<tr><td>1D</td><td>Pinsir</td></tr>
<tr><td>1E</td><td>Tangela</td></tr>
<tr><td>1F</td><td>Missingno</td></tr>
<tr><td>20</td><td>Missingno</td></tr>
<tr><td>21</td><td>Growlithe</td></tr>
<tr><td>22</td><td>Onix</td></tr>
<tr><td>23</td><td>Fearow</td></tr>
<tr><td>24</td><td>Pidgey</td></tr>
<tr><td>25</td><td>Slowpoke</td></tr>
<tr><td>26</td><td>Kadabra</td></tr>
<tr><td>27</td><td>Graveler</td></tr>
<tr><td>28</td><td>Chansey</td></tr>
<tr><td>29</td><td>Machoke</td></tr>
<tr><td>2A</td><td>Mr Mime</td></tr>
<tr><td>2B</td><td>Hitmonlee</td></tr>
<tr><td>2C</td><td>Hitmonchan</td></tr>
<tr><td>2D</td><td>Arbok</td></tr>
<tr><td>2E</td><td>Parasect</td></tr>
<tr><td>2F</td><td>Psyduck</td></tr>
<tr><td>30</td><td>Drowzee</td></tr>
<tr><td>31</td><td>Golem</td></tr>
<tr><td>32</td><td>Missingno</td></tr>
<tr><td>33</td><td>Magmar</td></tr>
<tr><td>34</td><td>Mankey</td></tr>
<tr><td>35</td><td>Electabuzz</td></tr>
<tr><td>36</td><td>Magneton</td></tr>
<tr><td>37</td><td>Koffing</td></tr>
<tr><td>38</td><td>Missingno</td></tr>
<tr><td>39</td><td>Missingno</td></tr>
<tr><td>3A</td><td>Seel</td></tr>
<tr><td>3B</td><td>Diglett</td></tr>
<tr><td>3C</td><td>Tauros</td></tr>
<tr><td>3D</td><td>Missingno</td></tr>
<tr><td>3E</td><td>Missingno</td></tr>
<tr><td>3F</td><td>Missingno</td></tr>
<tr><td>40</td><td>Farfetch'd</td></tr>
<tr><td>41</td><td>Venonat</td></tr>
<tr><td>42</td><td>Dragonite</td></tr>
<tr><td>43</td><td>Missingno</td></tr>
<tr><td>44</td><td>Missingno</td></tr>
<tr><td>45</td><td>Missingno</td></tr>
<tr><td>46</td><td>Doduo</td></tr>
<tr><td>47</td><td>Poliwag</td></tr>
<tr><td>48</td><td>Jynx</td></tr>
<tr><td>49</td><td>Moltres</td></tr>
<tr><td>4A</td><td>Articuno</td></tr>
<tr><td>4B</td><td>Zapdos</td></tr>
<tr><td>4C</td><td>Ditto	</td></tr>
<tr><td>4D</td><td>Meowth</td></tr>
<tr><td>4E</td><td>Krabby</td></tr>
<tr><td>4F</td><td>Missingno</td></tr>
<tr><td>50</td><td>Missingno</td></tr>
<tr><td>51</td><td>Missingno</td></tr>
<tr><td>52</td><td>Vulpix</td></tr>
<tr><td>53</td><td>Ninetales</td></tr>
<tr><td>54</td><td>Pikachu</td></tr>
<tr><td>55</td><td>Raichu</td></tr>
<tr><td>56</td><td>Missingno</td></tr>
<tr><td>57</td><td>Missingno</td></tr>
<tr><td>58</td><td>Dratini</td></tr>
<tr><td>59</td><td>Dragonair</td></tr>
<tr><td>5A</td><td>Kabuto</td></tr>
<tr><td>5B</td><td>Kabutops</td></tr>
<tr><td>5C</td><td>Horsea</td></tr>
<tr><td>5D</td><td>Seadra</td></tr>
<tr><td>5E</td><td>Missingno</td></tr>
<tr><td>5F</td><td>Missingno</td></tr>
<tr><td>60</td><td>Sandshrew</td></tr>
<tr><td>61</td><td>Sandslash</td></tr>
<tr><td>62</td><td>Omanite</td></tr>
<tr><td>63</td><td>Omastar</td></tr>
<tr><td>64</td><td>Jigglypuff</td></tr>
<tr><td>65</td><td>Wigglytuff</td></tr>
<tr><td>66</td><td>Eevee</td></tr>
<tr><td>67</td><td>Flareon</td></tr>
<tr><td>68</td><td>Jolteon</td></tr>
<tr><td>69</td><td>Vaporeon</td></tr>
<tr><td>6A</td><td>Machop</td></tr>
<tr><td>6B</td><td>Zubat</td></tr>
<tr><td>6C</td><td>Ekans</td></tr>
<tr><td>6D</td><td>Paras</td></tr>
<tr><td>6E</td><td>Poliwhirl</td></tr>
<tr><td>6F</td><td>Poliwrath</td></tr>
<tr><td>70</td><td>Weedle</td></tr>
<tr><td>71</td><td>Kakuna</td></tr>
<tr><td>72</td><td>Beedrill</td></tr>
<tr><td>73</td><td>Missingno</td></tr>
<tr><td>74</td><td>Dodrio</td></tr>
<tr><td>75</td><td>Primeape</td></tr>
<tr><td>76</td><td>Dugtrio</td></tr>
<tr><td>77</td><td>Venomoth</td></tr>
<tr><td>78</td><td>Dewgong</td></tr>
<tr><td>79</td><td>Missingno</td></tr>
<tr><td>7A</td><td>Missingno</td></tr>
<tr><td>7B</td><td>Caterpie</td></tr>
<tr><td>7C</td><td>Metapod</td></tr>
<tr><td>7D</td><td>Butterfree</td></tr>
<tr><td>7E</td><td>Machamp</td></tr>
<tr><td>7F</td><td>Missingno</td></tr>
<tr><td>80</td><td>Golduck</td></tr>
<tr><td>81</td><td>Hypno</td></tr>
<tr><td>82</td><td>Golbat</td></tr>
<tr><td>83</td><td>Mewtwo</td></tr>
<tr><td>84</td><td>Snorlax</td></tr>
<tr><td>85</td><td>Magikarp</td></tr>
<tr><td>86</td><td>Missingno</td></tr>
<tr><td>87</td><td>Missingno</td></tr>
<tr><td>88</td><td>Muk</td></tr>
<tr><td>8A</td><td>Kingler</td></tr>
<tr><td>8B</td><td>Cloyster</td></tr>
<tr><td>8C</td><td>Missingno</td></tr>
<tr><td>8D</td><td>Electrode</td></tr>
<tr><td>8E</td><td>Clefable</td></tr>
<tr><td>8F</td><td>Weezing</td></tr>
<tr><td>90</td><td>Persian</td></tr>
<tr><td>91</td><td>Marowak</td></tr>
<tr><td>92</td><td>Missingno</td></tr>
<tr><td>93</td><td>Haunter</td></tr>
<tr><td>94</td><td>Abra</td></tr>
<tr><td>95</td><td>Alakazam</td></tr>
<tr><td>96</td><td>Pidgeotto</td></tr>
<tr><td>97</td><td>Pidgeot</td></tr>
<tr><td>98</td><td>Starmie</td></tr>
<tr><td>99</td><td>Bulbasaur</td></tr>
<tr><td>9A</td><td>Venusaur</td></tr>
<tr><td>9B</td><td>Tentacruel</td></tr>
<tr><td>9C</td><td>Missingno</td></tr>
<tr><td>9D</td><td>Goldeen</td></tr>
<tr><td>9E</td><td>Seaking</td></tr>
<tr><td>9F</td><td>Missingno</td></tr>
<tr><td>A0</td><td>Missingno</td></tr>
<tr><td>A1</td><td>Missingno</td></tr>
<tr><td>A2</td><td>Missingno</td></tr>
<tr><td>A3</td><td>Ponyta</td></tr>
<tr><td>A4</td><td>Rapidash</td></tr>
<tr><td>A5</td><td>Rattata</td></tr>
<tr><td>A6</td><td>Raticate</td></tr>
<tr><td>A7</td><td>Nidorino</td></tr>
<tr><td>A8</td><td>Nidorina</td></tr>
<tr><td>A9</td><td>Geodude</td></tr>
<tr><td>AA</td><td>Porygon</td></tr>
<tr><td>AB</td><td>Aerodactyl</td></tr>
<tr><td>AC</td><td>Missingno</td></tr>
<tr><td>AD</td><td>Magnemite</td></tr>
<tr><td>AE</td><td>Missingno</td></tr>
<tr><td>AF</td><td>Missingno</td></tr>
<tr><td>B0</td><td>Charmander</td></tr>
<tr><td>B1</td><td>Squirtle</td></tr>
<tr><td>B2</td><td>Charmeleon</td></tr>
<tr><td>B3</td><td>Wartortle</td></tr>
<tr><td>B4</td><td>Charizard</td></tr>
<tr><td>B5</td><td>Missingno</td></tr>
<tr><td>B6</td><td>Missingno</td></tr>
<tr><td>B7</td><td>Missingno</td></tr>
<tr><td>B8</td><td>Missingno</td></tr>
<tr><td>B9</td><td>Oddish</td></tr>
<tr><td>BA</td><td>Gloom</td></tr>
<tr><td>BB</td><td>Vileplume</td></tr>
<tr><td>BC</td><td>Bellsprout</td></tr>
<tr><td>BD</td><td>Weepinbell</td></tr>
<tr><td>BE</td><td>Victreebel</td></tr>
</tbody>
</table>

</div>


<div class="col-md-3">

<h4>Pokemon Cristal / Plata / Oro</h4>

<table class="table table-striped">
<thead>
<tr>
    <th>C&oacute;digo</th>
    <th>Nombre</th>
</tr>
</thead>
<tbody>
<tr><td>01</td><td>Bulbasaur</td></tr>
<tr><td>02</td><td>Ivysaur</td></tr>
<tr><td>03</td><td>Venusaur</td></tr>
<tr><td>04</td><td>Charmander</td></tr>
<tr><td>05</td><td>Charmeleon</td></tr>
<tr><td>06</td><td>Charizard</td></tr>
<tr><td>07</td><td>Squirtle</td></tr>
<tr><td>08</td><td>Wartortle</td></tr>
<tr><td>09</td><td>Blastoise</td></tr>
<tr><td>0A</td><td>Caterpie</td></tr>
<tr><td>0B</td><td>Metapod</td></tr>
<tr><td>0C</td><td>Butterfree</td></tr>
<tr><td>0D</td><td>Weedle</td></tr>
<tr><td>0E</td><td>Kakuna</td></tr>
<tr><td>0F</td><td>Beedrill</td></tr>
<tr><td>10</td><td>Pidgey</td></tr>
<tr><td>11</td><td>Pidgeotto</td></tr>
<tr><td>12</td><td>Pidgeot</td></tr>
<tr><td>13</td><td>Rattata</td></tr>
<tr><td>14</td><td>Raticate</td></tr>
<tr><td>15</td><td>Spearow</td></tr>
<tr><td>16</td><td>Fearow</td></tr>
<tr><td>17</td><td>Ekans</td></tr>
<tr><td>18</td><td>Arbok</td></tr>
<tr><td>19</td><td>Pikachu</td></tr>
<tr><td>1A</td><td>Raichu</td></tr>
<tr><td>1B</td><td>Sandshrew</td></tr>
<tr><td>1C</td><td>Sandslash</td></tr>
<tr><td>1D</td><td>Nidoran (hembra)</td></tr>
<tr><td>1E</td><td>Nidorina</td></tr>
<tr><td>1F</td><td>Nidoqueen</td></tr>
<tr><td>20</td><td>Nidoran (Macho)</td></tr>
<tr><td>21</td><td>Nidorino</td></tr>
<tr><td>22</td><td>Nidoking</td></tr>
<tr><td>23</td><td>Clefairy</td></tr>
<tr><td>24</td><td>Clefable</td></tr>
<tr><td>25</td><td>Vulpix</td></tr>
<tr><td>26</td><td>Ninetales</td></tr>
<tr><td>27</td><td>Jigglypuff</td></tr>
<tr><td>28</td><td>Wigglytuff</td></tr>
<tr><td>29</td><td>Zubat</td></tr>
<tr><td>2A</td><td>Golbat</td></tr>
<tr><td>2B</td><td>Oddish</td></tr>
<tr><td>2C</td><td>Gloom</td></tr>
<tr><td>2D</td><td>Vileplume</td></tr>
<tr><td>2E</td><td>Paras</td></tr>
<tr><td>2F</td><td>Parasect</td></tr>
<tr><td>30</td><td>Venonat</td></tr>
<tr><td>31</td><td>Venomoth</td></tr>
<tr><td>32</td><td>Diglett</td></tr>
<tr><td>33</td><td>Dugtrio</td></tr>
<tr><td>34</td><td>Meowth</td></tr>
<tr><td>35</td><td>Persian</td></tr>
<tr><td>36</td><td>Psyduck</td></tr>
<tr><td>37</td><td>Golduck</td></tr>
<tr><td>38</td><td>Mankey</td></tr>
<tr><td>39</td><td>Primeape</td></tr>
<tr><td>3A</td><td>Growlithe</td></tr>
<tr><td>3B</td><td>Arcanine</td></tr>
<tr><td>3C</td><td>Poliwag</td></tr>
<tr><td>3D</td><td>Poliwhirl</td></tr>
<tr><td>3E</td><td>Poliwrath</td></tr>
<tr><td>3F</td><td>Abra</td></tr>
<tr><td>40</td><td>Kadabra</td></tr>
<tr><td>41</td><td>Alakazam</td></tr>
<tr><td>42</td><td>Machop</td></tr>
<tr><td>43</td><td>Machoke</td></tr>
<tr><td>44</td><td>Machamp</td></tr>
<tr><td>45</td><td>Bellsprout</td></tr>
<tr><td>46</td><td>Weepinbell</td></tr>
<tr><td>47</td><td>Victreebel</td></tr>
<tr><td>48</td><td>Tentacool</td></tr>
<tr><td>49</td><td>Tentacruel</td></tr>
<tr><td>4A</td><td>Geodude</td></tr>
<tr><td>4B</td><td>Graveler</td></tr>
<tr><td>4C</td><td>Golem</td></tr>
<tr><td>4D</td><td>Ponyta</td></tr>
<tr><td>4E</td><td>Rapidash</td></tr>
<tr><td>4F</td><td>Slowpoke</td></tr>
<tr><td>50</td><td>Slowbro</td></tr>
<tr><td>51</td><td>Magnemite</td></tr>
<tr><td>52</td><td>Magnetron</td></tr>
<tr><td>53</td><td>Farfetch'd</td></tr>
<tr><td>54</td><td>Doduo</td></tr>
<tr><td>55</td><td>Dodrio</td></tr>
<tr><td>56</td><td>Seel</td></tr>
<tr><td>57</td><td>Dewgong</td></tr>
<tr><td>58</td><td>Grimer</td></tr>
<tr><td>59</td><td>Muk</td></tr>
<tr><td>5A</td><td>Shellder</td></tr>
<tr><td>5B</td><td>Cloyster</td></tr>
<tr><td>5C</td><td>Gastly</td></tr>
<tr><td>5D</td><td>Haunter</td></tr>
<tr><td>5E</td><td>Gengar</td></tr>
<tr><td>5F</td><td>Onix</td></tr>
<tr><td>60</td><td>Drowzee</td></tr>
<tr><td>61</td><td>Hypno</td></tr>
<tr><td>62</td><td>Krabby</td></tr>
<tr><td>63</td><td>Kingler</td></tr>
<tr><td>64</td><td>Voltorb</td></tr>
<tr><td>65</td><td>Electrode</td></tr>
<tr><td>66</td><td>Exeggcute</td></tr>
<tr><td>67</td><td>Exegutor</td></tr>
<tr><td>68</td><td>Cubone</td></tr>
<tr><td>69</td><td>Marowak</td></tr>
<tr><td>6A</td><td>Hitmonlee</td></tr>
<tr><td>6B</td><td>Hitmonchan</td></tr>
<tr><td>6C</td><td>Lickitung</td></tr>
<tr><td>6D</td><td>Koffing</td></tr>
<tr><td>6E</td><td>Weezing</td></tr>
<tr><td>6F</td><td>Rhyhorn</td></tr>
<tr><td>70</td><td>Rhydon</td></tr>
<tr><td>71</td><td>Chansey</td></tr>
<tr><td>72</td><td>Tangela</td></tr>
<tr><td>73</td><td>Kangaskhan</td></tr>
<tr><td>74</td><td>Horsea</td></tr>
<tr><td>75</td><td>Seadra</td></tr>
<tr><td>76</td><td>Goldeen</td></tr>
<tr><td>77</td><td>Seaking</td></tr>
<tr><td>78</td><td>Staryu</td></tr>
<tr><td>79</td><td>Starmie</td></tr>
<tr><td>7A</td><td>Mr. Mime</td></tr>
<tr><td>7B</td><td>Scyther</td></tr>
<tr><td>7C</td><td>Jynx</td></tr>
<tr><td>7D</td><td>Electabuzz</td></tr>
<tr><td>7E</td><td>Magmar</td></tr>
<tr><td>7F</td><td>Pinsir</td></tr>
<tr><td>80</td><td>Tauros</td></tr>
<tr><td>81</td><td>Magicarp</td></tr>
<tr><td>82</td><td>Gyarados</td></tr>
<tr><td>83</td><td>Lapras</td></tr>
<tr><td>84</td><td>Ditto</td></tr>
<tr><td>85</td><td>Eevee</td></tr>
<tr><td>86</td><td>Vaporeon</td></tr>
<tr><td>87</td><td>Jolteon</td></tr>
<tr><td>88</td><td>Flareon</td></tr>
<tr><td>89</td><td>Porygon</td></tr>
<tr><td>8A</td><td>Omanyte</td></tr>
<tr><td>8B</td><td>Omastar</td></tr>
<tr><td>8C</td><td>Kabuto</td></tr>
<tr><td>8D</td><td>Kabutops</td></tr>
<tr><td>8E</td><td>Aerodactyl</td></tr>
<tr><td>8F</td><td>Snorlax</td></tr>
<tr><td>90</td><td>Articuno</td></tr>
<tr><td>91</td><td>Zapdos</td></tr>
<tr><td>92</td><td>Moltres</td></tr>
<tr><td>93</td><td>Dratini</td></tr>
<tr><td>94</td><td>Dragonair</td></tr>
<tr><td>95</td><td>Dragonite</td></tr>
<tr><td>96</td><td>Mewtwo</td></tr>
<tr><td>97</td><td>Mew</td></tr>
<tr><td>98</td><td>Chikorita</td></tr>
<tr><td>99</td><td>Bayleef</td></tr>
<tr><td>9A</td><td>Meganium</td></tr>
<tr><td>9B</td><td>Cynaquil</td></tr>
<tr><td>9C</td><td>Guilava</td></tr>
<tr><td>9D</td><td>Typhlosion</td></tr>
<tr><td>9E</td><td>Totodile</td></tr>
<tr><td>9F</td><td>Croconaw</td></tr>
<tr><td>A0</td><td>Feraligatr</td></tr>
<tr><td>A1</td><td>Sentret</td></tr>
<tr><td>A2</td><td>Furret</td></tr>
<tr><td>A3</td><td>Hoothoot</td></tr>
<tr><td>A4</td><td>Noctowl</td></tr>
<tr><td>A5</td><td>Ledyba</td></tr>
<tr><td>A6</td><td>Ledian</td></tr>
<tr><td>A7</td><td>Spinarak</td></tr>
<tr><td>A8</td><td>Ariados</td></tr>
<tr><td>A9</td><td>Crobat</td></tr>
<tr><td>AA</td><td>Chinchou</td></tr>
<tr><td>AB</td><td>Lanturn</td></tr>
<tr><td>AC</td><td>Pichu</td></tr>
<tr><td>AD</td><td>Cleffa</td></tr>
<tr><td>AE</td><td>Igglybuff</td></tr>
<tr><td>AF</td><td>Togepi</td></tr>
<tr><td>B0</td><td>Togetic</td></tr>
<tr><td>B1</td><td>Natu</td></tr>
<tr><td>B2</td><td>Xatu</td></tr>
<tr><td>B3</td><td>Mareep</td></tr>
<tr><td>B4</td><td>Flaaffy</td></tr>
<tr><td>B5</td><td>Ampharos</td></tr>
<tr><td>B6</td><td>Bellossom</td></tr>
<tr><td>B7</td><td>Marill</td></tr>
<tr><td>B8</td><td>Azumarill</td></tr>
<tr><td>B9</td><td>Sudowoodo</td></tr>
<tr><td>BA</td><td>Politoed</td></tr>
<tr><td>BB</td><td>Hoppip</td></tr>
<tr><td>BC</td><td>Skiploom</td></tr>
<tr><td>BD</td><td>Jumpluff</td></tr>
<tr><td>BE</td><td>Aipom</td></tr>
<tr><td>BF</td><td>Sunkern</td></tr>
<tr><td>C0</td><td>Sunflora</td></tr>
<tr><td>C1</td><td>Yanma</td></tr>
<tr><td>C2</td><td>Wooper</td></tr>
<tr><td>C3</td><td>Quagsire</td></tr>
<tr><td>C4</td><td>Espeon</td></tr>
<tr><td>C5</td><td>Umbreon</td></tr>
<tr><td>C6</td><td>Murkrow</td></tr>
<tr><td>C7</td><td>Slowking</td></tr>
<tr><td>C8</td><td>Misdreavus</td></tr>
<tr><td>C9</td><td>Unown</td></tr>
<tr><td>CA</td><td>Wobbuffet</td></tr>
<tr><td>CB</td><td>Girafarig</td></tr>
<tr><td>CC</td><td>Pineco</td></tr>
<tr><td>CD</td><td>Forretress</td></tr>
<tr><td>CE</td><td>Dunsparce</td></tr>
<tr><td>CF</td><td>Gligar</td></tr>
<tr><td>D0</td><td>Steelix</td></tr>
<tr><td>D1</td><td>Snubbull</td></tr>
<tr><td>D2</td><td>Granbull</td></tr>
<tr><td>D3</td><td>Qwilfish</td></tr>
<tr><td>D4</td><td>Scizor</td></tr>
<tr><td>D5</td><td>Shuckle</td></tr>
<tr><td>D6</td><td>Heracross</td></tr>
<tr><td>D7</td><td>Sneasel</td></tr>
<tr><td>D8</td><td>Teddiursa</td></tr>
<tr><td>D9</td><td>Ursaring</td></tr>
<tr><td>DA</td><td>Slugma</td></tr>
<tr><td>DB</td><td>Magcargo</td></tr>
<tr><td>DC</td><td>Swinub</td></tr>
<tr><td>DD</td><td>Piloswine</td></tr>
<tr><td>DE</td><td>Corsola</td></tr>
<tr><td>DF</td><td>Remoraid</td></tr>
<tr><td>E0</td><td>Octillery</td></tr>
<tr><td>E1</td><td>Delibird</td></tr>
<tr><td>E2</td><td>Mantine</td></tr>
<tr><td>E3</td><td>Skarmory</td></tr>
<tr><td>E4</td><td>Houndour</td></tr>
<tr><td>E5</td><td>Houndoom</td></tr>
<tr><td>E6</td><td>Kingdra</td></tr>
<tr><td>E7</td><td>Phanpy</td></tr>
<tr><td>E8</td><td>Donphan</td></tr>
<tr><td>E9</td><td>Porygon2</td></tr>
<tr><td>EA</td><td>Stantler</td></tr>
<tr><td>EB</td><td>Smeargle</td></tr>
<tr><td>EC</td><td>Tyrogue</td></tr>
<tr><td>ED</td><td>Hitmontop</td></tr>
<tr><td>EE</td><td>Smoochum</td></tr>
<tr><td>EF</td><td>Elekid</td></tr>
<tr><td>F0</td><td>Magby</td></tr>
<tr><td>F1</td><td>Miltank</td></tr>
<tr><td>F2</td><td>Blissey</td></tr>
<tr><td>F3</td><td>Raikou</td></tr>
<tr><td>F4</td><td>Entei</td></tr>
<tr><td>F5</td><td>Suicune</td></tr>
<tr><td>F6</td><td>Larvitar</td></tr>
<tr><td>F7</td><td>Pupitar</td></tr>
<tr><td>F8</td><td>Tyranitar</td></tr>
<tr><td>F9</td><td>Lugia</td></tr>
<tr><td>FA</td><td>Ho-oh</td></tr>
<tr><td>FB</td><td>Celebi</td><tr>
</tbody>
</table>

</div>


<div class="col-md-3">

<h4>Pokemon Rub&iacute; / Zafiro / Esmeralda / Rojo Fuego / Verde Hoja</h4>

<table class="table table-striped">
<thead>
<tr>
    <th>C&oacute;digo</th>
    <th>Nombre</th>
</tr>
</thead>
<tbody>
<tr><td>0100</td><td>BULBASAUR</td></tr>
<tr><td>0200</td><td>IVYSAUR</td></tr>
<tr><td>0300</td><td>VENESAUR</td></tr>
<tr><td>0400</td><td>CHARMANDER</td></tr>
<tr><td>0500</td><td>CHARMELEON</td></tr>
<tr><td>0600</td><td>CHARIZARD</td></tr>
<tr><td>0700</td><td>SQUIRTLE</td></tr>
<tr><td>0800</td><td>WARTORTLE</td></tr>
<tr><td>0900</td><td>BLASTOISE</td></tr>
<tr><td>0A00</td><td>CATERPIE</td></tr>
<tr><td>0B00</td><td>METAPOD</td></tr>
<tr><td>0C00</td><td>BUTTERFREE</td></tr>
<tr><td>0D00</td><td>WEEDLE</td></tr>
<tr><td>0E00</td><td>KAKUNA</td></tr>
<tr><td>0F00</td><td>BEEDRILL</td></tr>
<tr><td>1000</td><td>PIDGEY</td></tr>
<tr><td>1100</td><td>PIDGEOTTO</td></tr>
<tr><td>1200</td><td>PIDGEOT</td></tr>
<tr><td>1300</td><td>RATTATA</td></tr>
<tr><td>1400</td><td>RATICATE</td></tr>
<tr><td>1500</td><td>SPEAROW</td></tr>
<tr><td>1600</td><td>FEAROW</td></tr>
<tr><td>1700</td><td>EKANS</td></tr>
<tr><td>1800</td><td>ARBOK</td></tr>
<tr><td>1900</td><td>PIKACHU</td></tr>
<tr><td>1A00</td><td>RAICHU</td></tr>
<tr><td>1B00</td><td>SANDSHREW</td></tr>
<tr><td>1C00</td><td>SANDSLASH</td></tr>
<tr><td>1D00</td><td>NIDORAN (Hembra)</td></tr>
<tr><td>1E00</td><td>NIDORINA</td></tr>
<tr><td>1F00</td><td>NIDOQUEEN</td></tr>
<tr><td>2000</td><td>NIDORAN (Macho)</td></tr>
<tr><td>2100</td><td>NIDORINO</td></tr>
<tr><td>2200</td><td>NIDOKING</td></tr>
<tr><td>2300</td><td>CLEFAIRY</td></tr>
<tr><td>2400</td><td>CLEFABLE</td></tr>
<tr><td>2500</td><td>VULPIX</td></tr>
<tr><td>2600</td><td>NINETALES</td></tr>
<tr><td>2700</td><td>JIGGLYPUFF</td></tr>
<tr><td>2800</td><td>WIGGLYTUFF</td></tr>
<tr><td>2900</td><td>ZUBAT</td></tr>
<tr><td>2A00</td><td>GOLBAT</td></tr>
<tr><td>2B00</td><td>ODDISH</td></tr>
<tr><td>2C00</td><td>GLOOM</td></tr>
<tr><td>2D00</td><td>VILEPLUME</td></tr>
<tr><td>2E00</td><td>PARAS</td></tr>
<tr><td>2F00</td><td>PARASECT</td></tr>
<tr><td>3000</td><td>VENONAT</td></tr>
<tr><td>3100</td><td>VENOMOTH</td></tr>
<tr><td>3200</td><td>DIGLETT</td></tr>
<tr><td>3300</td><td>DUGTRIO</td></tr>
<tr><td>3400</td><td>MEOWTH</td></tr>
<tr><td>3500</td><td>PERSIAN</td></tr>
<tr><td>3600</td><td>PSYDUCK</td></tr>
<tr><td>3700</td><td>GOLDUCK</td></tr>
<tr><td>3800</td><td>MANKEY</td></tr>
<tr><td>3900</td><td>PRIMEAPE</td></tr>
<tr><td>3A00</td><td>GROWLITHE</td></tr>
<tr><td>3B00</td><td>ARCANINE</td></tr>
<tr><td>3C00</td><td>POLIWAG</td></tr>
<tr><td>3D00</td><td>POLIWHIRL</td></tr>
<tr><td>3E00</td><td>POLIWRATH</td></tr>
<tr><td>3F00</td><td>ABRA</td></tr>
<tr><td>4000</td><td>KADABRA</td></tr>
<tr><td>4100</td><td>ALAKAZAM</td></tr>
<tr><td>4200</td><td>MACHOP</td></tr>
<tr><td>4300</td><td>MACHOKE</td></tr>
<tr><td>4400</td><td>MACHAMP</td></tr>
<tr><td>4500</td><td>BELLSPROUT</td></tr>
<tr><td>4600</td><td>WEEPINBELL</td></tr>
<tr><td>4700</td><td>VICTREEBELL</td></tr>
<tr><td>4800</td><td>TENTACOOL</td></tr>
<tr><td>4900</td><td>TENTACRUEL</td></tr>
<tr><td>4A00</td><td>GEODUDE</td></tr>
<tr><td>4B00</td><td>GRAVELER</td></tr>
<tr><td>4C00</td><td>GOLEM</td></tr>
<tr><td>4D00</td><td>PONYTA</td></tr>
<tr><td>4E00</td><td>RAPIDASH</td></tr>
<tr><td>4F00</td><td>SLOWPOKE</td></tr>
<tr><td>5000</td><td>SLOWBRO</td></tr>
<tr><td>5100</td><td>MAGNEMITE</td></tr>
<tr><td>5200</td><td>MAGNETON</td></tr>
<tr><td>5300</td><td>FARFETCH'D</td></tr>
<tr><td>5400</td><td>DODUO</td></tr>
<tr><td>5500</td><td>DODRIO</td></tr>
<tr><td>5600</td><td>SEEL</td></tr>
<tr><td>5700</td><td>DEWGONG</td></tr>
<tr><td>5800</td><td>GRIMER</td></tr>
<tr><td>5900</td><td>MUK</td></tr>
<tr><td>5A00</td><td>SHELLDER</td></tr>
<tr><td>5B00</td><td>CLOYSTER</td></tr>
<tr><td>5C00</td><td>GASTLY</td></tr>
<tr><td>5D00</td><td>HAUNTER</td></tr>
<tr><td>5E00</td><td>GENGAR</td></tr>
<tr><td>5F00</td><td>ONIX</td></tr>
<tr><td>6000</td><td>DROWZEE</td></tr>
<tr><td>6100</td><td>HYPNO</td></tr>
<tr><td>6200</td><td>KRABBY</td></tr>
<tr><td>6300</td><td>KINGLER</td></tr>
<tr><td>6400</td><td>VOLTORB</td></tr>
<tr><td>6500</td><td>ELECTRODE</td></tr>
<tr><td>6600</td><td>EXEGGCUTE</td></tr>
<tr><td>6700</td><td>EXEGGUTOR</td></tr>
<tr><td>6800</td><td>CUBONE</td></tr>
<tr><td>6900</td><td>MAROWAK</td></tr>
<tr><td>6A00</td><td>HITMONLEEE</td></tr>
<tr><td>6B00</td><td>HITMONCHAN</td></tr>
<tr><td>6C00</td><td>LICKITUNG</td></tr>
<tr><td>6D00</td><td>KOFFING</td></tr>
<tr><td>6E00</td><td>WEEZING</td></tr>
<tr><td>6F00</td><td>RHYHORN</td></tr>
<tr><td>7000</td><td>RHYDON</td></tr>
<tr><td>7100</td><td>CHANSEY</td></tr>
<tr><td>7200</td><td>TANGELA</td></tr>
<tr><td>7300</td><td>KANGASKHAN</td></tr>
<tr><td>7400</td><td>HORSEA</td></tr>
<tr><td>7500</td><td>SEADRA</td></tr>
<tr><td>7600</td><td>GOLDEEN</td></tr>
<tr><td>7700</td><td>SEAKING</td></tr>
<tr><td>7800</td><td>STARYU</td></tr>
<tr><td>7900</td><td>STARMIE</td></tr>
<tr><td>7A00</td><td>MR. MIME</td></tr>
<tr><td>7B00</td><td>SCYTHER</td></tr>
<tr><td>7C00</td><td>JYNX</td></tr>
<tr><td>7D00</td><td>ELECTABUZZ</td></tr>
<tr><td>7E00</td><td>MAGMAR</td></tr>
<tr><td>7F00</td><td>PINSIR</td></tr>
<tr><td>8000</td><td>TAUROS</td></tr>
<tr><td>8100</td><td>MAGIKARP</td></tr>
<tr><td>8200</td><td>GYARADOS</td></tr>
<tr><td>8300</td><td>LAPRAS</td></tr>
<tr><td>8400</td><td>DITTO</td></tr>
<tr><td>8500</td><td>EEVEE</td></tr>
<tr><td>8600</td><td>VAPOREON</td></tr>
<tr><td>8700</td><td>JOLTEON</td></tr>
<tr><td>8800</td><td>FLAREON</td></tr>
<tr><td>8900</td><td>PORYGON</td></tr>
<tr><td>8A00</td><td>OMANYTE</td></tr>
<tr><td>8B00</td><td>OMASTAR</td></tr>
<tr><td>8C00</td><td>KABUTO</td></tr>
<tr><td>8D00</td><td>KABUTOPS</td></tr>
<tr><td>8E00</td><td>AERODACTYL</td></tr>
<tr><td>8F00</td><td>SNORLAX</td></tr>
<tr><td>9000</td><td>ARTICUNO</td></tr>
<tr><td>9100</td><td>ZAPDOS</td></tr>
<tr><td>9200</td><td>MOLTRES</td></tr>
<tr><td>9300</td><td>DRATINI</td></tr>
<tr><td>9400</td><td>DRAGONAIR</td></tr>
<tr><td>9500</td><td>DRAGONITE</td></tr>
<tr><td>9600</td><td>MEWTWO</td></tr>
<tr><td>9700</td><td>MEW</td></tr>
<tr><td>9800</td><td>CHIKORITA</td></tr>
<tr><td>9900</td><td>BAYLEEF</td></tr>
<tr><td>9A00</td><td>MEGANIUM</td></tr>
<tr><td>9B00</td><td>CYNDAQUIL</td></tr>
<tr><td>9C00</td><td>QUILAVA</td></tr>
<tr><td>9D00</td><td>TYPLOSION</td></tr>
<tr><td>9E00</td><td>TOTODILE</td></tr>
<tr><td>9F00</td><td>CROCONAW</td></tr>
<tr><td>A000</td><td>FERALIGATR</td></tr>
<tr><td>A100</td><td>SENTRET</td></tr>
<tr><td>A200</td><td>FURRET</td></tr>
<tr><td>A300</td><td>HOOTHOOT</td></tr>
<tr><td>A400</td><td>NOCTOWL</td></tr>
<tr><td>A500</td><td>LEDYBA</td></tr>
<tr><td>A600</td><td>LEDIAN</td></tr>
<tr><td>A700</td><td>SPINARAK</td></tr>
<tr><td>A800</td><td>ARIADOS</td></tr>
<tr><td>A900</td><td>CROBAT</td></tr>
<tr><td>AA00</td><td>CHINCHOU</td></tr>
<tr><td>AB00</td><td>LANTURN</td></tr>
<tr><td>AC00</td><td>PICHU</td></tr>
<tr><td>AD00</td><td>CLEFFA</td></tr>
<tr><td>AE00</td><td>IGGLYBUFF</td></tr>
<tr><td>AF00</td><td>TOGEPI</td></tr>
<tr><td>B000</td><td>TOGETIC</td></tr>
<tr><td>B100</td><td>NATU</td></tr>
<tr><td>B200</td><td>XATU</td></tr>
<tr><td>B300</td><td>MAREEP</td></tr>
<tr><td>B400</td><td>FLAAFFY</td></tr>
<tr><td>B500</td><td>AMPHAROS</td></tr>
<tr><td>B600</td><td>BELLOSSOM</td></tr>
<tr><td>B700</td><td>MARRILL</td></tr>
<tr><td>B800</td><td>AZUMARILL</td></tr>
<tr><td>B900</td><td>SUDOWOODO</td></tr>
<tr><td>BA00</td><td>POLITOED</td></tr>
<tr><td>BB00</td><td>HOPPIP</td></tr>
<tr><td>BC00</td><td>SKI PLOOM</td></tr>
<tr><td>BD00</td><td>JUMPLUFF</td></tr>
<tr><td>BE00</td><td>AIPOM</td></tr>
<tr><td>BF00</td><td>SUNKERN</td></tr>
<tr><td>C000</td><td>SUNFLORA</td></tr>
<tr><td>C100</td><td>YANMA</td></tr>
<tr><td>C200</td><td>WOOPER</td></tr>
<tr><td>C300</td><td>QUAGSIRE</td></tr>
<tr><td>C400</td><td>ESPEON</td></tr>
<tr><td>C500</td><td>UMBREON</td></tr>
<tr><td>C600</td><td>MURKROW</td></tr>
<tr><td>C700</td><td>SLOWKING</td></tr>
<tr><td>C800</td><td>MISDREAVUS</td></tr>
<tr><td>C900</td><td>UNOWN</td></tr>
<tr><td>CA00</td><td>WOBBUFFET</td></tr>
<tr><td>CB00</td><td>GIRAFARIG</td></tr>
<tr><td>CC00</td><td>PINECO</td></tr>
<tr><td>CD00</td><td>FORRETRESS</td></tr>
<tr><td>CE00</td><td>DUNSPARCE</td></tr>
<tr><td>CF00</td><td>GLIGAR</td></tr>
<tr><td>D000</td><td>STEELIX</td></tr>
<tr><td>D100</td><td>SNUBBULL</td></tr>
<tr><td>D200</td><td>GRANBULL</td></tr>
<tr><td>D300</td><td>QWILFISH</td></tr>
<tr><td>D400</td><td>SCIZOR</td></tr>
<tr><td>D500</td><td>SHUCKLE</td></tr>
<tr><td>D600</td><td>HERACROSS</td></tr>
<tr><td>D700</td><td>SNEASEL</td></tr>
<tr><td>D800</td><td>TEDDIURSA</td></tr>
<tr><td>D900</td><td>URSARING</td></tr>
<tr><td>DA00</td><td>SLUGMA</td></tr>
<tr><td>DB00</td><td>MAGCARGO</td></tr>
<tr><td>DC00</td><td>SWINUB</td></tr>
<tr><td>DD00</td><td>PILOSWINE</td></tr>
<tr><td>DE00</td><td>CORSOLA</td></tr>
<tr><td>DF00</td><td>REMORAID</td></tr>
<tr><td>E000</td><td>OCTILLERY</td></tr>
<tr><td>E100</td><td>DELIBIRD</td></tr>
<tr><td>E200</td><td>MANTINE</td></tr>
<tr><td>E300</td><td>SKARMORY</td></tr>
<tr><td>E400</td><td>HOUNDOUR</td></tr>
<tr><td>E500</td><td>DOUNDOOM</td></tr>
<tr><td>E600</td><td>KINGDRA</td></tr>
<tr><td>E700</td><td>PHANPY</td></tr>
<tr><td>E800</td><td>DONPHAN</td></tr>
<tr><td>E900</td><td>PORYGON2</td></tr>
<tr><td>EA00</td><td>STANTLER</td></tr>
<tr><td>EB00</td><td>SMEARGLE</td></tr>
<tr><td>EC00</td><td>TYROGUE</td></tr>
<tr><td>ED00</td><td>HITMONTOP</td></tr>
<tr><td>EE00</td><td>SMOOCHUM</td></tr>
<tr><td>EF00</td><td>ELEKID</td></tr>
<tr><td>F000</td><td>MAGBY</td></tr>
<tr><td>F100</td><td>MILTANK</td></tr>
<tr><td>F200</td><td>BLISSEY</td></tr>
<tr><td>F300</td><td>RAIKOU</td></tr>
<tr><td>F400</td><td>ENTEI</td></tr>
<tr><td>F500</td><td>SUICINE</td></tr>
<tr><td>F600</td><td>LARVITAR</td></tr>
<tr><td>F700</td><td>PUPITAR</td></tr>
<tr><td>F800</td><td>TYRANITAR</td></tr>
<tr><td>F900</td><td>LUGIA</td></tr>
<tr><td>FA00</td><td>HO-OH</td></tr>
<tr><td>FB00</td><td>CELEBI</td></tr>
<tr><td>1501</td><td>TREECKO</td></tr>
<tr><td>1601</td><td>GROVYLE</td></tr>
<tr><td>1701</td><td>SCEPTILE</td></tr>
<tr><td>1801</td><td>TORCHIC</td></tr>
<tr><td>1901</td><td>COMBUSKEN</td></tr>
<tr><td>1A01</td><td>BLAZIKEN</td></tr>
<tr><td>1B01</td><td>MUDKIP</td></tr>
<tr><td>1C01</td><td>MARSHTOMP</td></tr>
<tr><td>1D01</td><td>SWAMPERT</td></tr>
<tr><td>1E01</td><td>POOCHYENA</td></tr>
<tr><td>1F01</td><td>MIGHTYENA</td></tr>
<tr><td>2001</td><td>ZIGZAGOON</td></tr>
<tr><td>2101</td><td>LINOONE</td></tr>
<tr><td>2201</td><td>WURMPLE</td></tr>
<tr><td>2301</td><td>SILCOON</td></tr>
<tr><td>2401</td><td>BEAUTIFLY</td></tr>
<tr><td>2501</td><td>CASCOON</td></tr>
<tr><td>2601</td><td>DUSTOX</td></tr>
<tr><td>2701</td><td>LOTAD</td></tr>
<tr><td>2801</td><td>LOMBRE</td></tr>
<tr><td>2901</td><td>LUDICOLO</td></tr>
<tr><td>2A01</td><td>SEEDOT</td></tr>
<tr><td>2B01</td><td>NUZLEAF</td></tr>
<tr><td>2C01</td><td>SHIFTRY</td></tr>
<tr><td>2D01</td><td>NINCADA</td></tr>
<tr><td>2E01</td><td>NINJASK</td></tr>
<tr><td>2F01</td><td>SHEDINJA</td></tr>
<tr><td>3001</td><td>TAILLOW</td></tr>
<tr><td>3101</td><td>SWELLOW</td></tr>
<tr><td>3201</td><td>SHROOMISH</td></tr>
<tr><td>3301</td><td>BRELOOM</td></tr>
<tr><td>3401</td><td>SPINDA</td></tr>
<tr><td>3501</td><td>WINGULL</td></tr>
<tr><td>3601</td><td>PELIPPER</td></tr>
<tr><td>3701</td><td>SURSKIT</td></tr>
<tr><td>3801</td><td>MASQUERAIN</td></tr>
<tr><td>3901</td><td>WAILMER</td></tr>
<tr><td>3A01</td><td>WAILORD</td></tr>
<tr><td>3B01</td><td>SKITTY</td></tr>
<tr><td>3C01</td><td>DELCATTY</td></tr>
<tr><td>3D01</td><td>KECLEON</td></tr>
<tr><td>3E01</td><td>BALTOY</td></tr>
<tr><td>3F01</td><td>CLAYDOL</td></tr>
<tr><td>4001</td><td>NOSEPASS</td></tr>
<tr><td>4101</td><td>TORKOAL</td></tr>
<tr><td>4201</td><td>SABLEYE</td></tr>
<tr><td>4301</td><td>BARBOACH</td></tr>
<tr><td>4401</td><td>WHISCASH</td></tr>
<tr><td>4501</td><td>LUVDISC</td></tr>
<tr><td>4601</td><td>CORPHISH</td></tr>
<tr><td>4701</td><td>CRAWDAUNT</td></tr>
<tr><td>4801</td><td>FEEBAS</td></tr>
<tr><td>4901</td><td>MILOTIC</td></tr>
<tr><td>4A01</td><td>CARVANHA</td></tr>
<tr><td>4B01</td><td>SHARPEDO</td></tr>
<tr><td>4C01</td><td>TRAPINCH</td></tr>
<tr><td>4D01</td><td>VIBRAVA</td></tr>
<tr><td>4E01</td><td>FLYGON</td></tr>
<tr><td>4F01</td><td>MAKUHITA</td></tr>
<tr><td>5001</td><td>HARIYAMA</td></tr>
<tr><td>5101</td><td>ELECTRIKE</td></tr>
<tr><td>5201</td><td>MANECTRIC</td></tr>
<tr><td>5301</td><td>NUMEL</td></tr>
<tr><td>5401</td><td>CAMERUPT</td></tr>
<tr><td>5501</td><td>SPHEAL</td></tr>
<tr><td>5601</td><td>SEALEO</td></tr>
<tr><td>5701</td><td>WALREIN</td></tr>
<tr><td>5801</td><td>CACNEA</td></tr>
<tr><td>5901</td><td>CACTURNE</td></tr>
<tr><td>5A01</td><td>SNORUNT</td></tr>
<tr><td>5B01</td><td>GLALIE</td></tr>
<tr><td>5C01</td><td>LUNATONE</td></tr>
<tr><td>5D01</td><td>SOLROCK</td></tr>
<tr><td>5E01</td><td>AZURILL</td></tr>
<tr><td>5F01</td><td>SPOINK</td></tr>
<tr><td>6001</td><td>GRUMPIG</td></tr>
<tr><td>6101</td><td>PLUSLE</td></tr>
<tr><td>6201</td><td>MINUN</td></tr>
<tr><td>6301</td><td>MAWILE</td></tr>
<tr><td>6401</td><td>MEDITITE</td></tr>
<tr><td>6501</td><td>MEDICHAM</td></tr>
<tr><td>6601</td><td>SWABLU</td></tr>
<tr><td>6701</td><td>ALTARIA</td></tr>
<tr><td>6801</td><td>WYNAUT</td></tr>
<tr><td>6901</td><td>DUSKULL</td></tr>
<tr><td>6A01</td><td>DUSCLOPS</td></tr>
<tr><td>6B01</td><td>ROSELIA</td></tr>
<tr><td>6C01</td><td>SLAKOTH</td></tr>
<tr><td>6D01</td><td>VIGOROTH</td></tr>
<tr><td>6E01</td><td>SLAKING</td></tr>
<tr><td>6F01</td><td>GULPIN</td></tr>
<tr><td>7001</td><td>SWALOT</td></tr>
<tr><td>7101</td><td>TROPIUS</td></tr>
<tr><td>7201</td><td>WHISMUR</td></tr>
<tr><td>7301</td><td>LOUDRED</td></tr>
<tr><td>7401</td><td>EXPLOUD</td></tr>
<tr><td>7501</td><td>CLAMPERL</td></tr>
<tr><td>7601</td><td>HUNTAIL</td></tr>
<tr><td>7701</td><td>GOREBYSS</td></tr>
<tr><td>7801</td><td>ABSOL</td></tr>
<tr><td>7901</td><td>SHUPPET</td></tr>
<tr><td>7A01</td><td>BANNETE</td></tr>
<tr><td>7B01</td><td>SEVIPER</td></tr>
<tr><td>7C01</td><td>ZANGOOSE</td></tr>
<tr><td>7D01</td><td>RELICANTH</td></tr>
<tr><td>7E01</td><td>ARON</td></tr>
<tr><td>7F01</td><td>LAIRON</td></tr>
<tr><td>8001</td><td>AGGRON</td></tr>
<tr><td>8101</td><td>CASTFORM</td></tr>
<tr><td>8201</td><td>VOLBEAT</td></tr>
<tr><td>8301</td><td>ILLUMISE</td></tr>
<tr><td>8401</td><td>LILEEP</td></tr>
<tr><td>8501</td><td>CRADILY</td></tr>
<tr><td>8601</td><td>ANORITH</td></tr>
<tr><td>8701</td><td>ARMALDO</td></tr>
<tr><td>8801</td><td>RALTS</td></tr>
<tr><td>8901</td><td>KIRLIA</td></tr>
<tr><td>8A01</td><td>GARDEVOIR</td></tr>
<tr><td>8B01</td><td>BAGON</td></tr>
<tr><td>8C01</td><td>SHELGON</td></tr>
<tr><td>8D01</td><td>SALAMENCE</td></tr>
<tr><td>8E01</td><td>BELDUM</td></tr>
<tr><td>8F01</td><td>METANG</td></tr>
<tr><td>9001</td><td>METAGROSS</td></tr>
<tr><td>9101</td><td>REGI ROCK</td></tr>
<tr><td>9201</td><td>REGICE</td></tr>
<tr><td>9301</td><td>REGI STEEL</td></tr>
<tr><td>9401</td><td>KYOGRE</td></tr>
<tr><td>9501</td><td>GROUDON</td></tr>
<tr><td>9601</td><td>RAYQUAZA</td></tr>
<tr><td>9701</td><td>LATIAS</td></tr>
<tr><td>9801</td><td>LATIOS</td></tr>
<tr><td>9901</td><td>JIRACHI</td></tr>
<tr><td>9A01</td><td>DEOXYS</td></tr>
<tr><td>9B01</td><td>CHIMECHO</td></tr>
</tbody>
</table>

</div>


<div class="col-md-3">

<h4>Pokemon Soul Silver/Diamante</h4>

<table class="table table-striped">
<thead>
<tr>
    <th>C&oacute;digo</th>
    <th>Nombre</th>
</tr>
</thead>
<tbody>
<tr><td>0001</td><td>Bulbasaur</td></tr>
<tr><td>0002</td><td>Ivysaur</td></tr>
<tr><td>0003</td><td>Venusaur</td></tr>
<tr><td>0004</td><td>Charmander</td></tr>
<tr><td>0005</td><td>Charmeleon</td></tr>
<tr><td>0006</td><td>Charizard</td></tr>
<tr><td>0007</td><td>Squirtle</td></tr>
<tr><td>0008</td><td>Wartortle</td></tr>
<tr><td>0009</td><td>Blastoise</td></tr>
<tr><td>000A</td><td>Caterpie</td></tr>
<tr><td>000B</td><td>Metapod</td></tr>
<tr><td>000C</td><td>Butterfree</td></tr>
<tr><td>000D</td><td>Weedle</td></tr>
<tr><td>000E</td><td>Kakuna</td></tr>
<tr><td>000F</td><td>Beedrill</td></tr>
<tr><td>0010</td><td>Pidgey</td></tr>
<tr><td>0011</td><td>Pidgeotto</td></tr>
<tr><td>0012</td><td>Pidgeot</td></tr>
<tr><td>0013</td><td>Rattata</td></tr>
<tr><td>0014</td><td>Raticate</td></tr>
<tr><td>0015</td><td>Spearow</td></tr>
<tr><td>0016</td><td>Fearow</td></tr>
<tr><td>0017</td><td>Ekans</td></tr>
<tr><td>0018</td><td>Arbok</td></tr>
<tr><td>0019</td><td>Pikachu</td></tr>
<tr><td>001A</td><td>Raichu</td></tr>
<tr><td>001B</td><td>Sandshrew</td></tr>
<tr><td>001C</td><td>Sandslash</td></tr>
<tr><td>001D</td><td>Nidoran-F</td></tr>
<tr><td>001E</td><td>Nidorina</td></tr>
<tr><td>001F</td><td>Nidoqueen</td></tr>
<tr><td>0020</td><td>Nidoran-M</td></tr>
<tr><td>0021</td><td>Nidorino</td></tr>
<tr><td>0022</td><td>Nidoking</td></tr>
<tr><td>0023</td><td>Clefairy</td></tr>
<tr><td>0024</td><td>Clefable</td></tr>
<tr><td>0025</td><td>Vulpix</td></tr>
<tr><td>0026</td><td>Ninetales</td></tr>
<tr><td>0027</td><td>Jigglypuff</td></tr>
<tr><td>0028</td><td>Wigglytuff</td></tr>
<tr><td>0029</td><td>Zubat</td></tr>
<tr><td>002A</td><td>Golbat</td></tr>
<tr><td>002B</td><td>Oddish</td></tr>
<tr><td>002C</td><td>Gloom</td></tr>
<tr><td>002D</td><td>Vileplume</td></tr>
<tr><td>002E</td><td>Paras</td></tr>
<tr><td>002F</td><td>Parasect</td></tr>
<tr><td>0030</td><td>Venonat</td></tr>
<tr><td>0031</td><td>Venomoth</td></tr>
<tr><td>0032</td><td>Diglett</td></tr>
<tr><td>0033</td><td>Dugtrio</td></tr>
<tr><td>0034</td><td>Meowth</td></tr>
<tr><td>0035</td><td>Persian</td></tr>
<tr><td>0036</td><td>Psyduck</td></tr>
<tr><td>0037</td><td>Golduck</td></tr>
<tr><td>0038</td><td>Mankey</td></tr>
<tr><td>0039</td><td>Primeape</td></tr>
<tr><td>003A</td><td>Growlithe</td></tr>
<tr><td>003B</td><td>Arcanine</td></tr>
<tr><td>003C</td><td>Poliwag</td></tr>
<tr><td>003D</td><td>Poliwhirl</td></tr>
<tr><td>003E</td><td>Poliwrath</td></tr>
<tr><td>003F</td><td>Abra</td></tr>
<tr><td>0040</td><td>Kadabra</td></tr>
<tr><td>0041</td><td>Alakazam</td></tr>
<tr><td>0042</td><td>Machop</td></tr>
<tr><td>0043</td><td>Machoke</td></tr>
<tr><td>0044</td><td>Machamp</td></tr>
<tr><td>0045</td><td>Bellsprout</td></tr>
<tr><td>0046</td><td>Weepinbell</td></tr>
<tr><td>0047</td><td>Victreebel</td></tr>
<tr><td>0048</td><td>Tentacool</td></tr>
<tr><td>0049</td><td>Tentacruel</td></tr>
<tr><td>004A</td><td>Geodude</td></tr>
<tr><td>004B</td><td>Graveler</td></tr>
<tr><td>004C</td><td>Golem</td></tr>
<tr><td>004D</td><td>Ponyta</td></tr>
<tr><td>004E</td><td>Rapidash</td></tr>
<tr><td>004F</td><td>Slowpoke</td></tr>
<tr><td>0050</td><td>Slowbro</td></tr>
<tr><td>0051</td><td>Magnemite</td></tr>
<tr><td>0052</td><td>Magneton</td></tr>
<tr><td>0053</td><td>Farfetch'd</td></tr>
<tr><td>0054</td><td>Doduo</td></tr>
<tr><td>0055</td><td>Dodrio</td></tr>
<tr><td>0056</td><td>Seel</td></tr>
<tr><td>0057</td><td>Dewgong</td></tr>
<tr><td>0058</td><td>Grimer</td></tr>
<tr><td>0059</td><td>Muk</td></tr>
<tr><td>005A</td><td>Shellder</td></tr>
<tr><td>005B</td><td>Cloyster</td></tr>
<tr><td>005C</td><td>Gastly</td></tr>
<tr><td>005D</td><td>Haunter</td></tr>
<tr><td>005E</td><td>Gengar</td></tr>
<tr><td>005F</td><td>Onix</td></tr>
<tr><td>0060</td><td>Drowzee</td></tr>
<tr><td>0061</td><td>Hypno</td></tr>
<tr><td>0062</td><td>Krabby</td></tr>
<tr><td>0063</td><td>Kingler</td></tr>
<tr><td>0064</td><td>Voltorb</td></tr>
<tr><td>0065</td><td>Electrode</td></tr>
<tr><td>0066</td><td>Exeggcute</td></tr>
<tr><td>0067</td><td>Exeggutor</td></tr>
<tr><td>0068</td><td>Cubone</td></tr>
<tr><td>0069</td><td>Marowak</td></tr>
<tr><td>006A</td><td>Hitmonlee</td></tr>
<tr><td>006B</td><td>Hitmonchan</td></tr>
<tr><td>006C</td><td>Lickitung</td></tr>
<tr><td>006D</td><td>Koffing</td></tr>
<tr><td>006E</td><td>Weezing</td></tr>
<tr><td>006F</td><td>Rhyhorn</td></tr>
<tr><td>0070</td><td>Rhydon</td></tr>
<tr><td>0071</td><td>Chansey</td></tr>
<tr><td>0072</td><td>Tangela</td></tr>
<tr><td>0073</td><td>Kangaskhan</td></tr>
<tr><td>0074</td><td>Horsea</td></tr>
<tr><td>0075</td><td>Seadra</td></tr>
<tr><td>0076</td><td>Goldeen</td></tr>
<tr><td>0077</td><td>Seaking</td></tr>
<tr><td>0078</td><td>Staryu</td></tr>
<tr><td>0079</td><td>Starmie</td></tr>
<tr><td>007A</td><td>Mr.Mime</td></tr>
<tr><td>007B</td><td>Scyther</td></tr>
<tr><td>007C</td><td>Jynx</td></tr>
<tr><td>007D</td><td>Electabuzz</td></tr>
<tr><td>007E</td><td>Magmar</td></tr>
<tr><td>007F</td><td>Pinsir</td></tr>
<tr><td>0080</td><td>Tauros</td></tr>
<tr><td>0081</td><td>Magikarp</td></tr>
<tr><td>0082</td><td>Gyarados</td></tr>
<tr><td>0083</td><td>Lapras</td></tr>
<tr><td>0084</td><td>Ditto</td></tr>
<tr><td>0085</td><td>Eevee</td></tr>
<tr><td>0086</td><td>Vaporeon</td></tr>
<tr><td>0087</td><td>Jolteon</td></tr>
<tr><td>0088</td><td>Flareon</td></tr>
<tr><td>0089</td><td>Porygon</td></tr>
<tr><td>008A</td><td>Omanyte</td></tr>
<tr><td>008B</td><td>Omastar</td></tr>
<tr><td>008C</td><td>Kabuto</td></tr>
<tr><td>008D</td><td>Kabutops</td></tr>
<tr><td>008E</td><td>Aerodactyl</td></tr>
<tr><td>008F</td><td>Snorlax</td></tr>
<tr><td>0090</td><td>Articuno</td></tr>
<tr><td>0091</td><td>Zapdos</td></tr>
<tr><td>0092</td><td>Moltres</td></tr>
<tr><td>0093</td><td>Dratini</td></tr>
<tr><td>0094</td><td>Dragonair</td></tr>
<tr><td>0095</td><td>Dragonite</td></tr>
<tr><td>0096</td><td>Mewtwo</td></tr>
<tr><td>0097</td><td>Mew</td></tr>
<tr><td>0098</td><td>Chikorita</td></tr>
<tr><td>0099</td><td>Bayleef</td></tr>
<tr><td>009A</td><td>Meganium</td></tr>
<tr><td>009B</td><td>Cyndaquil</td></tr>
<tr><td>009C</td><td>Quilava</td></tr>
<tr><td>009D</td><td>Typhlosion</td></tr>
<tr><td>009E</td><td>Totodile</td></tr>
<tr><td>009F</td><td>Croconaw</td></tr>
<tr><td>00A0</td><td>Feraligatr</td></tr>
<tr><td>00A1</td><td>Sentret</td></tr>
<tr><td>00A2</td><td>Furret</td></tr>
<tr><td>00A3</td><td>Hoothoot</td></tr>
<tr><td>00A4</td><td>Noctowl</td></tr>
<tr><td>00A5</td><td>Ledyba</td></tr>
<tr><td>00A6</td><td>Ledian</td></tr>
<tr><td>00A7</td><td>Spinarak</td></tr>
<tr><td>00A8</td><td>Ariados</td></tr>
<tr><td>00A9</td><td>Crobat</td></tr>
<tr><td>00AA</td><td>Chinchou</td></tr>
<tr><td>00AB</td><td>Lanturn</td></tr>
<tr><td>00AC</td><td>Pichu</td></tr>
<tr><td>00AD</td><td>Cleffa</td></tr>
<tr><td>00AE</td><td>Igglybuff</td></tr>
<tr><td>00AF</td><td>Togepi</td></tr>
<tr><td>00B0</td><td>Togetic</td></tr>
<tr><td>00B1</td><td>Natu</td></tr>
<tr><td>00B2</td><td>Xatu</td></tr>
<tr><td>00B3</td><td>Mareep</td></tr>
<tr><td>00B4</td><td>Flaaffy</td></tr>
<tr><td>00B5</td><td>Ampharos</td></tr>
<tr><td>00B6</td><td>Bellossom</td></tr>
<tr><td>00B7</td><td>Marill</td></tr>
<tr><td>00B8</td><td>Azumarill</td></tr>
<tr><td>00B9</td><td>Sudowoodo</td></tr>
<tr><td>00BA</td><td>Politoed</td></tr>
<tr><td>00BB</td><td>Hoppip</td></tr>
<tr><td>00BC</td><td>Skiploom</td></tr>
<tr><td>00BD</td><td>Jumpluff</td></tr>
<tr><td>00BE</td><td>Aipom</td></tr>
<tr><td>00BF</td><td>Sunkern</td></tr>
<tr><td>00C0</td><td>Sunflora</td></tr>
<tr><td>00C1</td><td>Yanma</td></tr>
<tr><td>00C2</td><td>Wooper</td></tr>
<tr><td>00C3</td><td>Quagsire</td></tr>
<tr><td>00C4</td><td>Espeon</td></tr>
<tr><td>00C5</td><td>Umbreon</td></tr>
<tr><td>00C6</td><td>Murkrow</td></tr>
<tr><td>00C7</td><td>Slowking</td></tr>
<tr><td>00C8</td><td>Misdreavus</td></tr>
<tr><td>00C9</td><td>Unown</td></tr>
<tr><td>00CA</td><td>Wobbuffet</td></tr>
<tr><td>00CB</td><td>Girafarig</td></tr>
<tr><td>00CC</td><td>Pineco</td></tr>
<tr><td>00CD</td><td>Forretress</td></tr>
<tr><td>00CE</td><td>Dunsparce</td></tr>
<tr><td>00CF</td><td>Gligar</td></tr>
<tr><td>00D0</td><td>Steelix</td></tr>
<tr><td>00D1</td><td>Snubbull</td></tr>
<tr><td>00D2</td><td>Granbull</td></tr>
<tr><td>00D3</td><td>Qwilfish</td></tr>
<tr><td>00D4</td><td>Scizor</td></tr>
<tr><td>00D5</td><td>Shuckle</td></tr>
<tr><td>00D6</td><td>Heracross</td></tr>
<tr><td>00D7</td><td>Sneasel</td></tr>
<tr><td>00D8</td><td>Teddiursa</td></tr>
<tr><td>00D9</td><td>Ursaring</td></tr>
<tr><td>00DA</td><td>Slugma</td></tr>
<tr><td>00DB</td><td>Magcargo</td></tr>
<tr><td>00DC</td><td>Swinub</td></tr>
<tr><td>00DD</td><td>Piloswine</td></tr>
<tr><td>00DE</td><td>Corsola</td></tr>
<tr><td>00DF</td><td>Remoraid</td></tr>
<tr><td>00E0</td><td>Octillery</td></tr>
<tr><td>00E1</td><td>Delibird</td></tr>
<tr><td>00E2</td><td>Mantine</td></tr>
<tr><td>00E3</td><td>Skarmory</td></tr>
<tr><td>00E4</td><td>Houndour</td></tr>
<tr><td>00E5</td><td>Houndoom</td></tr>
<tr><td>00E6</td><td>Kingdra</td></tr>
<tr><td>00E7</td><td>Phanpy</td></tr>
<tr><td>00E8</td><td>Donphan</td></tr>
<tr><td>00E9</td><td>Porygon 2</td></tr>
<tr><td>00EA</td><td>Stantler</td></tr>
<tr><td>00EB</td><td>Smeargle</td></tr>
<tr><td>00EC</td><td>Tyrogue</td></tr>
<tr><td>00ED</td><td>Hitmontop</td></tr>
<tr><td>00EE</td><td>Smoochum</td></tr>
<tr><td>00EF</td><td>Elekid</td></tr>
<tr><td>00F0</td><td>Magby</td></tr>
<tr><td>00F1</td><td>Miltank</td></tr>
<tr><td>00F2</td><td>Blissey</td></tr>
<tr><td>00F3</td><td>Raikou</td></tr>
<tr><td>00F4</td><td>Entei</td></tr>
<tr><td>00F5</td><td>Suicune</td></tr>
<tr><td>00F6</td><td>Larvitar</td></tr>
<tr><td>00F7</td><td>Pupitar</td></tr>
<tr><td>00F8</td><td>Tyranitar</td></tr>
<tr><td>00F9</td><td>Lugia</td></tr>
<tr><td>00FA</td><td>Ho-oh</td></tr>
<tr><td>00FB</td><td>Celebi</td></tr>
<tr><td>00FC</td><td>Treecko</td></tr>
<tr><td>00FD</td><td>Grovyle</td></tr>
<tr><td>00FE</td><td>Sceptile</td></tr>
<tr><td>00FF</td><td>Torchic</td></tr>
<tr><td>0100</td><td>Combusken</td></tr>
<tr><td>0101</td><td>Blaziken</td></tr>
<tr><td>0102</td><td>Mudkip</td></tr>
<tr><td>0103</td><td>Marshtomp</td></tr>
<tr><td>0104</td><td>Swampert</td></tr>
<tr><td>0105</td><td>Poochyena</td></tr>
<tr><td>0106</td><td>Mightyena</td></tr>
<tr><td>0107</td><td>Zigzagoon</td></tr>
<tr><td>0108</td><td>Linoone</td></tr>
<tr><td>0109</td><td>Wurmple</td></tr>
<tr><td>010A</td><td>Silcoon</td></tr>
<tr><td>010B</td><td>Beautifly</td></tr>
<tr><td>010C</td><td>Cascoon</td></tr>
<tr><td>010D</td><td>Dustox</td></tr>
<tr><td>010E</td><td>Lotad</td></tr>
<tr><td>010F</td><td>Lombre</td></tr>
<tr><td>0110</td><td>Ludicolo</td></tr>
<tr><td>0111</td><td>Seedot</td></tr>
<tr><td>0112</td><td>Nuzleaf</td></tr>
<tr><td>0113</td><td>Shiftry</td></tr>
<tr><td>0114</td><td>Taillow</td></tr>
<tr><td>0115</td><td>Swellow</td></tr>
<tr><td>0116</td><td>Wingull</td></tr>
<tr><td>0117</td><td>Pelipper</td></tr>
<tr><td>0118</td><td>Ralts</td></tr>
<tr><td>0119</td><td>Kirlia</td></tr>
<tr><td>011A</td><td>Gardevoir</td></tr>
<tr><td>011B</td><td>Surskit</td></tr>
<tr><td>011C</td><td>Masquerain</td></tr>
<tr><td>011D</td><td>Shroomish</td></tr>
<tr><td>011E</td><td>Breloom</td></tr>
<tr><td>011F</td><td>Slakoth</td></tr>
<tr><td>0120</td><td>Vigoroth</td></tr>
<tr><td>0121</td><td>Slaking</td></tr>
<tr><td>0122</td><td>Nincada</td></tr>
<tr><td>0123</td><td>Ninjask</td></tr>
<tr><td>0124</td><td>Shedinja</td></tr>
<tr><td>0125</td><td>Whismur</td></tr>
<tr><td>0126</td><td>Loudred</td></tr>
<tr><td>0127</td><td>Exploud</td></tr>
<tr><td>0128</td><td>Makuhita</td></tr>
<tr><td>0129</td><td>Hariyama</td></tr>
<tr><td>012A</td><td>Azurill</td></tr>
<tr><td>012B</td><td>Nosepass</td></tr>
<tr><td>012C</td><td>Skitty</td></tr>
<tr><td>012D</td><td>Delcatty</td></tr>
<tr><td>012E</td><td>Sableye</td></tr>
<tr><td>012F</td><td>Mawile</td></tr>
<tr><td>0130</td><td>Aron</td></tr>
<tr><td>0131</td><td>Lairon</td></tr>
<tr><td>0132</td><td>Aggron</td></tr>
<tr><td>0133</td><td>Meditite</td></tr>
<tr><td>0134</td><td>Medicham</td></tr>
<tr><td>0135</td><td>Electrike</td></tr>
<tr><td>0136</td><td>Manectric</td></tr>
<tr><td>0137</td><td>Plusle</td></tr>
<tr><td>0138</td><td>Minun</td></tr>
<tr><td>0139</td><td>Volbeat</td></tr>
<tr><td>013A</td><td>Illumise</td></tr>
<tr><td>013B</td><td>Roselia</td></tr>
<tr><td>013C</td><td>Gulpin</td></tr>
<tr><td>013D</td><td>Swalot</td></tr>
<tr><td>013E</td><td>Carvanha</td></tr>
<tr><td>013F</td><td>Sharpedo</td></tr>
<tr><td>0140</td><td>Wailmer</td></tr>
<tr><td>0141</td><td>Wailord</td></tr>
<tr><td>0142</td><td>Numel</td></tr>
<tr><td>0143</td><td>Camerupt</td></tr>
<tr><td>0144</td><td>Torkoal</td></tr>
<tr><td>0145</td><td>Spoink</td></tr>
<tr><td>0146</td><td>Grumpig</td></tr>
<tr><td>0147</td><td>Spinda</td></tr>
<tr><td>0148</td><td>Trapinch</td></tr>
<tr><td>0149</td><td>Vibrava</td></tr>
<tr><td>014A</td><td>Flygon</td></tr>
<tr><td>014B</td><td>Cacnea</td></tr>
<tr><td>014C</td><td>Cacturne</td></tr>
<tr><td>014D</td><td>Swablu</td></tr>
<tr><td>014E</td><td>Altaria</td></tr>
<tr><td>014F</td><td>Zangoose</td></tr>
<tr><td>0150</td><td>Seviper</td></tr>
<tr><td>0151</td><td>Lunatone</td></tr>
<tr><td>0152</td><td>Solrock</td></tr>
<tr><td>0153</td><td>Barboach</td></tr>
<tr><td>0154</td><td>Whiscash</td></tr>
<tr><td>0155</td><td>Corphish</td></tr>
<tr><td>0156</td><td>Crawdaunt</td></tr>
<tr><td>0157</td><td>Baltoy</td></tr>
<tr><td>0158</td><td>Claydol</td></tr>
<tr><td>0159</td><td>Lileep</td></tr>
<tr><td>015A</td><td>Cradily</td></tr>
<tr><td>015B</td><td>Anorith</td></tr>
<tr><td>015C</td><td>Armaldo</td></tr>
<tr><td>015D</td><td>Feebas</td></tr>
<tr><td>015E</td><td>Milotic</td></tr>
<tr><td>015F</td><td>Castform</td></tr>
<tr><td>0160</td><td>Kecleon</td></tr>
<tr><td>0161</td><td>Shuppet</td></tr>
<tr><td>0162</td><td>Banette</td></tr>
<tr><td>0163</td><td>Duskull</td></tr>
<tr><td>0164</td><td>Dusclops</td></tr>
<tr><td>0165</td><td>Tropius</td></tr>
<tr><td>0166</td><td>Chimecho</td></tr>
<tr><td>0167</td><td>Absol</td></tr>
<tr><td>0168</td><td>Wynaut</td></tr>
<tr><td>0169</td><td>Snorunt</td></tr>
<tr><td>016A</td><td>Glalie</td></tr>
<tr><td>016B</td><td>Spheal</td></tr>
<tr><td>016C</td><td>Sealeo</td></tr>
<tr><td>016D</td><td>Walrein</td></tr>
<tr><td>016E</td><td>Clamperl</td></tr>
<tr><td>016F</td><td>Huntail</td></tr>
<tr><td>0170</td><td>Gorebyss</td></tr>
<tr><td>0171</td><td>Relicanth</td></tr>
<tr><td>0172</td><td>Luvdisc</td></tr>
<tr><td>0173</td><td>Bagon</td></tr>
<tr><td>0174</td><td>Shellgon</td></tr>
<tr><td>0175</td><td>Salamence</td></tr>
<tr><td>0176</td><td>Beldum</td></tr>
<tr><td>0177</td><td>Metang</td></tr>
<tr><td>0178</td><td>Metagross</td></tr>
<tr><td>0179</td><td>Regirock</td></tr>
<tr><td>017A</td><td>Regice</td></tr>
<tr><td>017B</td><td>Registeel</td></tr>
<tr><td>017C</td><td>Latias</td></tr>
<tr><td>017D</td><td>Latios</td></tr>
<tr><td>017E</td><td>Kyogre</td></tr>
<tr><td>017F</td><td>Groudon</td></tr>
<tr><td>0180</td><td>Rayquaza</td></tr>
<tr><td>0181</td><td>Jirachi</td></tr>
<tr><td>0182</td><td>Deoxys</td></tr>
<tr><td>0183</td><td>Turtwig</td></tr>
<tr><td>0184</td><td>Grotle</td></tr>
<tr><td>0185</td><td>Torterra</td></tr>
<tr><td>0186</td><td>Chimchar</td></tr>
<tr><td>0187</td><td>Monferno</td></tr>
<tr><td>0188</td><td>Infernape</td></tr>
<tr><td>0189</td><td>Piplup</td></tr>
<tr><td>018A</td><td>Prinplup</td></tr>
<tr><td>018B</td><td>Empoleon</td></tr>
<tr><td>018C</td><td>Starly</td></tr>
<tr><td>018D</td><td>Staravia</td></tr>
<tr><td>018E</td><td>Staraptor</td></tr>
<tr><td>018F</td><td>Bidoof</td></tr>
<tr><td>0190</td><td>Bibarel</td></tr>
<tr><td>0191</td><td>Kricketot</td></tr>
<tr><td>0192</td><td>Kricketune</td></tr>
<tr><td>0193</td><td>Shinx</td></tr>
<tr><td>0194</td><td>Luxio</td></tr>
<tr><td>0195</td><td>Luxray</td></tr>
<tr><td>0196</td><td>Budew</td></tr>
<tr><td>0197</td><td>Roserade</td></tr>
<tr><td>0198</td><td>Cranidos</td></tr>
<tr><td>0199</td><td>Rampardos</td></tr>
<tr><td>019A</td><td>Shieldon</td></tr>
<tr><td>019B</td><td>Bastiodon</td></tr>
<tr><td>019C</td><td>Burmy</td></tr>
<tr><td>019D</td><td>Wormadam</td></tr>
<tr><td>019E</td><td>Mothim</td></tr>
<tr><td>019F</td><td>Combee</td></tr>
<tr><td>01A0</td><td>Vespiquen</td></tr>
<tr><td>01A1</td><td>Pachirisu</td></tr>
<tr><td>01A2</td><td>Buizel</td></tr>
<tr><td>01A3</td><td>Floatzel</td></tr>
<tr><td>01A4</td><td>Cherubi</td></tr>
<tr><td>01A5</td><td>Cherrim</td></tr>
<tr><td>01A6</td><td>Shellos</td></tr>
<tr><td>01A7</td><td>Gastrodon</td></tr>
<tr><td>01A8</td><td>Ambipom</td></tr>
<tr><td>01A9</td><td>Drifloon</td></tr>
<tr><td>01AA</td><td>Drifblim</td></tr>
<tr><td>01AB</td><td>Buneary</td></tr>
<tr><td>01AC</td><td>Lopunny</td></tr>
<tr><td>01AD</td><td>Mismagius</td></tr>
<tr><td>01AE</td><td>Honchkrow</td></tr>
<tr><td>01AF</td><td>Glameow</td></tr>
<tr><td>01B0</td><td>Purugly</td></tr>
<tr><td>01B1</td><td>Chingling</td></tr>
<tr><td>01B2</td><td>Stunky</td></tr>
<tr><td>01B3</td><td>Skuntank</td></tr>
<tr><td>01B4</td><td>Bronzor</td></tr>
<tr><td>01B5</td><td>Bronzong</td></tr>
<tr><td>01B6</td><td>Bonsly</td></tr>
<tr><td>01B7</td><td>Mime Jr.</td></tr>
<tr><td>01B8</td><td>Happiny</td></tr>
<tr><td>01B9</td><td>Chatot</td></tr>
<tr><td>01BA</td><td>Spiritomb</td></tr>
<tr><td>01BB</td><td>Gible</td></tr>
<tr><td>01BC</td><td>Gabite</td></tr>
<tr><td>01BD</td><td>Garchomp</td></tr>
<tr><td>01BE</td><td>Munchlax</td></tr>
<tr><td>01BF</td><td>Riolu</td></tr>
<tr><td>01C0</td><td>Lucario</td></tr>
<tr><td>01C1</td><td>Hippopotas</td></tr>
<tr><td>01C2</td><td>Hippowdon</td></tr>
<tr><td>01C3</td><td>Skorupi</td></tr>
<tr><td>01C4</td><td>Drapion</td></tr>
<tr><td>01C5</td><td>Croagunk</td></tr>
<tr><td>01C6</td><td>Toxicroak</td></tr>
<tr><td>01C7</td><td>Carnivine</td></tr>
<tr><td>01C8</td><td>Finneon</td></tr>
<tr><td>01C9</td><td>Lumineon</td></tr>
<tr><td>01CA</td><td>Mantyke</td></tr>
<tr><td>01CB</td><td>Snover</td></tr>
<tr><td>01CC</td><td>Abomasnow</td></tr>
<tr><td>01CD</td><td>Weavile</td></tr>
<tr><td>01CE</td><td>Magnezone</td></tr>
<tr><td>01CF</td><td>Lickilicky</td></tr>
<tr><td>01D0</td><td>Rhyperior</td></tr>
<tr><td>01D1</td><td>Tangrowth</td></tr>
<tr><td>01D2</td><td>Electivire</td></tr>
<tr><td>01D3</td><td>Magmortar</td></tr>
<tr><td>01D4</td><td>Togekiss</td></tr>
<tr><td>01D5</td><td>Yanmega</td></tr>
<tr><td>01D6</td><td>Leafeon</td></tr>
<tr><td>01D7</td><td>Glaceon</td></tr>
<tr><td>01D8</td><td>Gliscor</td></tr>
<tr><td>01D9</td><td>Mamoswine</td></tr>
<tr><td>01DA</td><td>Porygon-Z</td></tr>
<tr><td>01DB</td><td>Gallade</td></tr>
<tr><td>01DC</td><td>Probopass</td></tr>
<tr><td>01DD</td><td>Dusknoir</td></tr>
<tr><td>01DE</td><td>Froslass</td></tr>
<tr><td>01DF</td><td>Rotom</td></tr>
<tr><td>01E0</td><td>Uxie</td></tr>
<tr><td>01E1</td><td>Mesprit</td></tr>
<tr><td>01E2</td><td>Azelf</td></tr>
<tr><td>01E3</td><td>Dialga</td></tr>
<tr><td>01E4</td><td>Palkia</td></tr>
<tr><td>01E5</td><td>Heatran</td></tr>
<tr><td>01E6</td><td>Regigigas</td></tr>
<tr><td>01E7</td><td>Giratina</td></tr>
<tr><td>01E8</td><td>Cresselia</td></tr>
<tr><td>01E9</td><td>Phione</td></tr>
<tr><td>01EA</td><td>Manaphy</td></tr>
<tr><td>01EB</td><td>Darkrai</td></tr>
<tr><td>01EC</td><td>Shaymin</td></tr>
<tr><td>01ED</td><td>Arceus</td></tr>

</tbody>
</table>

</div>

</div>

</div>

</div>


<br>
<br>

<hr style="width: 100%; height: 1px;">

<H3>2. Editar &iacute;tems de venta en Poke Market (Pokemon Cristal / Plata / Oro / Rojo / Azul / Amarillo / Rub&iacute; / Zafiro / Esmeralda / Rojo Fuego / Verde Hoja)</H3>


<b>Nota:</b> Si estas haciendo un hacking partiendo un juego desde cero, recomiendo primero aplicar el hacking anterior, pero dejando solo enemigos f&aacute;ciles como Pidgey nivel 1 y no Charizard nivel 80, esto ser&iacute;a escribir el c&oacute;digo 0110, as&iacute; llegas f&aacute;cil a la primera tienda del primer pueblo.
<br><br>
Por ejemplo tenemos esta primera tienda del Pokemon Plata:
<br><br>
<img width="250px" height="250px" src="img/hacking_poke_plata_3.png">
<br><br>
Si entramos y vemos lo que nos ofrece el vendedor tenemos:
<br><br>
<img width="250px" height="250px" src="img/hacking_poke_plata_4.png">
<br><br>

Para hacer el romhacking usa un editor hexadecimal, como Transhextion:
<br>
<br>

<div class="alert alert-warning" role="alert">

<li>Para el <b>Pokemon Cristal</b> ve a la direcci&oacute;n: <b>1610E</b> que es la direcci&oacute;n del primer Poke Market de la Ciudad Cerezo.</li>
<li>Para el <b>Pokemon Plata/Oro</b> ve a la direcci&oacute;n <b>16372</b> que muestra los c&oacute;digos de la tienda de la primera ciudad, Ciudad Cerezo.</li>
<li>Para el <b>Pokemon Rojo/Azul</b> ve a la direcci&oacute;n <b>2443</b> que muestra los c&oacute;digos de la tienda de la primera ciudad, Ciudad Verde.</li>
<li>Para el <b>Pokemon Amarillo</b> ve a la direcci&oacute;n <b>233C</b> que muestra los c&oacute;digos de la tienda de la primera ciudad, Ciudad Verde.</li>
<li>Para el <b>Pokemon Rub&iacute;</b> ve a la direcci&oacute;n <b>153498</b> que muestra los c&oacute;digos de la tienda del primer pueblo, Pueblo Escaso.</li>
<li>Para el <b>Pokemon Zafiro</b> ve a la direcci&oacute;n <b>153428</b> que muestra los c&oacute;digos de la tienda del primer pueblo, Pueblo Escaso.</li>
<li>Para el <b>Pokemon Esmeralda</b> ve a la direcci&oacute;n <b>1FCAA4</b> que muestra los c&oacute;digos de la tienda del primer pueblo, Pueblo Escaso.</li>
<li>Para el <b>Pokemon Rojo Fuego</b> ve a la direcci&oacute;n <b>16A330</b> que muestra los c&oacute;digos de la tienda del primer pueblo, Ciudad Verde.</li>
<li>Para el <b>Pokemon Verde Hoja</b> ve a la direcci&oacute;n <b>16A30C</b> que muestra los c&oacute;digos de la tienda del primer pueblo, Ciudad Verde.</li>

</div>

<div class="alert alert-success" role="alert">
<li>Para el <b>Pokemon Soul Silver</b> no pude encontrar la direcci&oacute;n aunque investigu&eacute; por horas...:( Si la llegas a encontrar escr&iacute;beme a mi correo <a href="mailto:hernaldog@gmail.com">hernaldog@gmail.com</a> yo lo probar&eacute; y si funciona, ser&aacute;s el big boss!</li>
</div>

Usa Control + G para usar la funci&oacute;n Jump To. Por ejemplo para Pokemon Cristal usas:
<br><br>
<img src="img/hacking_poke_cristal_7.png">
<br><br>

Al presionar Enter veras unos c&oacute;digos as&iacute;:
<br><br>

<b>0512 090D 0CFF</b>

<br><br>

<img src="img/hacking_poke_cristal_8.png">

<br><br>

En <b>Pokemon Red</b> se ven los c&oacute;digos <b>040B0F0C</b>:

<br><br>

<img src="img/hacking_poke_red_4.png">

<br><br>

En <b>Pokemon Rub&iacute;</b> se ven los c&oacute;digos <b>0D00 0E00 1200 1100</b>:

<br><br>
<img src="img/hacking_poke_rubi_4.png">

<br><br>

Esto se interpreta viendo la lista completa de &iacute;tems de m&aacute;s abajo: 
<br>
<br>
<li>Para <b>Pokemon Cristal/Plata/Oro</b> el <b>05</b> es una <b>PokeBall</b>, <b>12</b> es una <b>Poci&oacute;n</b>, <b>09</b> es un <b>Ant&iacute;doto</b>, <b>0D</b> un <b>Antiparalizante (Paralyze Heal)</b>, <b>0C</b> es <b>Despertar</b>, <b>FF</b> indica <b>fin de art&iacute;culos</b>.</li>
<li>Para <b>Pokemon Rojo/Azul</b> tenemos el c&oacute;digo <b>040B0F0C</b> que se lee <b>04</b> es una <b>PokeBall</b>, <b>0B</b> es <b>Ant&iacute;doto</b>, <b>0F</b> es <b>Antiparalizante (Paralyze Heal)</b> y <b>0C</b> es <b>Antiquemar (Burn Heal)</b>.</li>
<li>Para <b>Pokemon Rub&iacute;</b>, <b>Pokemon Zafiro</b> y <b>Pokemon Esmeralda</b> tenemos el c&oacute;digo <b>0D00 0E00 1200 1100</b> que se lee <b>D000</b> es una <b>Poci&oacute;n</b>, <b>0E00</b> es <b>Ant&iacute;doto</b>, <b>1200</b> es <b>Antiparalizante (Paralyze Heal)</b> y <b>1100</b> es <b>Despertar (Awakening)</b>.</li>
<li>Para <b>Pokemon Rojo Fuego</b> y <b>Pokemon Verde Hoja</b> tenemos el c&oacute;digo <b>0400 0D00 0E00 1200</b> que se lee <b>0400</b> es una <b>Pokeball</b>, <b>0D00</b> es una <b>Poci&oacute;n</b>, <b>0E00</b> es <b>Ant&iacute;doto</b> y <b>1200</b> es <b>Antiparalizante (Paralyze Heal)</b>.</li>

<br>
En el <b>Pokemon Plata</b> ver&aacute;s estos c&oacute;digos en la direcci&oacute;n x16372:

<br><br>
<img src="img/hacking_poke_plata_5.png">
<br><br>

Ahora si quieres que aparezca otro &iacute;tem, como por ejemplo una <b>Master Ball</b>, solo debes cambiar el <b>05</b> por <b>01</b>, salvar y jugar.
<br><br>

En el <b>Pokemon R&iacute;, Zafiro y Esmeralda</b> se ven los c&oacute;digos <b>0D00 0E00 1200 1100</b>:
<br><br>

<img src="img/hacking_poke_zafiro_1.png">

<br><br>

Por ejemplo en el <b>Pokemon Plata</b> cambiamos un 12 (Poci&oacute;n) por 01 que es una MasterBall que tiene costo 0!
<br><br>
<img src="img/hacking_poke_plata_6.png">
<br><br>
Si vas al juego, cargas la partida y vas a la tienda, el vendedor ofrece ahora Masterball:

<br><br>
<img width="250px" height="250px" src="img/hacking_poke_plata_7.png">
<br><br>

Compramos 99, yupiii:

<br><br>
<img width="250px" height="250px" src="img/hacking_poke_plata_8.png">
<br><br>

Revisamos en nuestros objetos y verificamos que est&eacute;n all&iacute;:

<br><br>
<img width="250px" height="250px" src="img/hacking_poke_plata_9.png">
<br><br>

En el <b>Pokemon Red</b> tambi&eacute;n se puede:

<br><br>
<img width="250px" height="250px" src="img/hacking_poke_red_5.png">

<br><br>

En el <b>Pokemon Rub&iacute;</b> por fin pude hackearlo:
<br><br>
<img width="300px" height="250px" src="img/hacking_poke_rubi_5.png">


<H4>2.1 Lista completa de &Iacute;tems</H4>

<div id="listaItem" class="panel panel-default collapse in">

  <div class="panel-heading">
  <div class="row">
    <div class="col-md-6"><h5 class="panel-title">Lista con los &iacute;tems</h5></div>
    <div class="col-md-6"><span class="glyphicon glyphicon-chevron-up pull-right cursor-pointer"  data-toggle="collapse" data-target="#tablaItem"></span></div>
  </div>
  </div>

<div class="collapse in" id="tablaItem">

<div class="row">


<div class="col-md-4">

<h4>Pokemon Rojo / Azul / Amarillo</h4>

<table class="table table-striped">
<thead>
<tr>
    <th>C&oacute;digo</th>
    <th>Nombre</th>
</tr>
</thead>
<tbody>
<tr><td>01</td><td>Master Ball</td></tr>
<tr><td>02</td><td>Ultra Ball</td></tr>
<tr><td>03</td><td>Great Ball</td></tr>
<tr><td>04</td><td>PokeBall</td></tr>
<tr><td>05</td><td>Town Map</td></tr>
<tr><td>06</td><td>Bicycle</td></tr>
<tr><td>08</td><td>Safari Ball</td></tr>
<tr><td>09</td><td>Pokedex</td></tr>
<tr><td>0A</td><td>Moon Stone</td></tr>
<tr><td>0B</td><td>Antidote</td></tr>
<tr><td>0C</td><td>Burn Heal</td></tr>
<tr><td>0D</td><td>Ice Heal</td></tr>
<tr><td>0E</td><td>Awakening</td></tr>
<tr><td>0F</td><td>Paralyze Heal</td></tr>
<tr><td>10</td><td>Full Restore</td></tr>
<tr><td>11</td><td>Max Potion</td></tr>
<tr><td>12</td><td>Hyper Potion</td></tr>
<tr><td>13</td><td>Super Potion</td></tr>
<tr><td>14</td><td>Potion</td></tr>
<tr><td>1D</td><td>Escape Rope</td></tr>
<tr><td>1E</td><td>Repel</td></tr>
<tr><td>1F</td><td>Old Amber</td></tr>
<tr><td>20</td><td>Fire Stone</td></tr>
<tr><td>21</td><td>Thunder Stone</td></tr>
<tr><td>22</td><td>Water Stone</td></tr>
<tr><td>23</td><td>HP Up</td></tr>
<tr><td>24</td><td>Protein</td></tr>
<tr><td>25</td><td>Iron</td></tr>
<tr><td>26</td><td>Carbos</td></tr>
<tr><td>27</td><td>Calcium</td></tr>
<tr><td>28</td><td>Rare Candy</td></tr>
<tr><td>29</td><td>Dome Fossil</td></tr>
<tr><td>2A</td><td>Helix Fossil</td></tr>
<tr><td>2B</td><td>Secret Key</td></tr>
<tr><td>2D</td><td>Bike Voucher</td></tr>
<tr><td>2E</td><td>X Accuracy</td></tr>
<tr><td>2F</td><td>Leaf Stone</td></tr>
<tr><td>30</td><td>Card Key</td></tr>
<tr><td>31</td><td>Nugget</td></tr>
<tr><td>32</td><td>PP UP</td></tr>
<tr><td>33</td><td>PokeDoll</td></tr>
<tr><td>34</td><td>Full Heal</td></tr>
<tr><td>35</td><td>Revive</td></tr>
<tr><td>36</td><td>Max Revive</td></tr>
<tr><td>37</td><td>Guard Spec</td></tr>
<tr><td>38</td><td>Super Repel</td></tr>
<tr><td>39</td><td>Max Repel</td></tr>
<tr><td>3A</td><td>Dire Hit</td></tr>
<tr><td>3B</td><td>Coin</td></tr>
<tr><td>3C</td><td>Fresh Water</td></tr>
<tr><td>3D</td><td>Soda Pop</td></tr>
<tr><td>3E</td><td>Lemonade</td></tr>
<tr><td>3F</td><td>S.S. Ticket</td></tr>
<tr><td>40</td><td>Gold Teeth</td></tr>
<tr><td>41</td><td>X Attack</td></tr>
<tr><td>42</td><td>X Defend</td></tr>
<tr><td>43</td><td>X Speed</td></tr>
<tr><td>44</td><td>X Special</td></tr>
<tr><td>45</td><td>Coin Case</td></tr>
<tr><td>46</td><td>Oak's Parcel</td></tr>
<tr><td>47</td><td>Item Finder</td></tr>
<tr><td>48</td><td>Silph Scope</td></tr>
<tr><td>49</td><td>PokeFlute</td></tr>
<tr><td>4A</td><td>Lift Key</td></tr>
<tr><td>4B</td><td>Exp. All</td></tr>
<tr><td>4C</td><td>Old Rod</td></tr>
<tr><td>4D</td><td>Good Rod</td></tr>
<tr><td>4E</td><td>Super Rod</td></tr>
<tr><td>4F</td><td>PP Up</td></tr>
<tr><td>50</td><td>Ether</td></tr>
<tr><td>51</td><td>Max Ether</td></tr>
<tr><td>52</td><td>Elixer</td></tr>
<tr><td>53</td><td>Max Elixer</td></tr>
<tr><td>C4</td><td>HM01 - Cut</td></tr>
<tr><td>C5</td><td>HM02 - Fly</td></tr>
<tr><td>C6</td><td>HM03 - Surf</td></tr>
<tr><td>C7</td><td>HM04 - Strength</td></tr>
<tr><td>C8</td><td>HM05 - Flash</td></tr>
<tr><td>C9</td><td>TM01 - Mega Punch</td></tr>
<tr><td>CA</td><td>TM02 - Razor Wind</td></tr>
<tr><td>CB</td><td>TM03 - Swords Dance</td></tr>
<tr><td>CC</td><td>TM04 - Whirlwind</td></tr>
<tr><td>CD</td><td>TM05 - Mega Kick</td></tr>
<tr><td>CE</td><td>TM06 - Toxic</td></tr>
<tr><td>CF</td><td>TM07 - Horn Drill</td></tr>
<tr><td>D0</td><td>TM08 - Body Slam</td></tr>
<tr><td>D1</td><td>TM09 - Take Down</td></tr>
<tr><td>D2</td><td>TM10 - Double-Edge</td></tr>
<tr><td>D3</td><td>TM11 - Bubble Beam</td></tr>
<tr><td>D4</td><td>TM12 - Water Gun</td></tr>
<tr><td>D5</td><td>TM13 - Ice Beam</td></tr>
<tr><td>D6</td><td>TM14 - Blizzard</td></tr>
<tr><td>D7</td><td>TM15 - Hyper Beam</td></tr>
<tr><td>D8</td><td>TM16 - Pay Day</td></tr>
<tr><td>D9</td><td>TM17 - Submission</td></tr>
<tr><td>DA</td><td>TM18 - Counter</td></tr>
<tr><td>DB</td><td>TM19 - Seismic Toss</td></tr>
<tr><td>DC</td><td>TM20 - Rage</td></tr>
<tr><td>DD</td><td>TM21 - Mega Drain</td></tr>
<tr><td>DE</td><td>TM22 - Solar Beam</td></tr>
<tr><td>DF</td><td>TM23 - Dragon Rage</td></tr>
<tr><td>E0</td><td>TM24 - Thunderbolt</td></tr>
<tr><td>E1</td><td>TM25 - Thunder</td></tr>
<tr><td>E2</td><td>TM26 - Earthquake</td></tr>
<tr><td>E3</td><td>TM27 - Fissure</td></tr>
<tr><td>E4</td><td>TM28 - Dig</td></tr>
<tr><td>E5</td><td>TM29 - Psychic</td></tr>
<tr><td>E6</td><td>TM30 - Teleport</td></tr>
<tr><td>E7</td><td>TM31 - Mimc</td></tr>
<tr><td>E8</td><td>TM32 - Double Team</td></tr>
<tr><td>E9</td><td>TM33 - Reflect</td></tr>
<tr><td>EA</td><td>TM34 - Bide</td></tr>
<tr><td>EB</td><td>TM35 - Metronome</td></tr>
<tr><td>EC</td><td>TM36 - Self Destruct</td></tr>
<tr><td>ED</td><td>TM37 - Egg Bomb</td></tr>
<tr><td>EE</td><td>TM38 - Fire Blast</td></tr>
<tr><td>EF</td><td>TM39 - Swift</td></tr>
<tr><td>F0</td><td>TM40 - Skull Bash</td></tr>
<tr><td>F1</td><td>TM41 - Softboiled</td></tr>
<tr><td>F2</td><td>TM42 - Dreameater</td></tr>
<tr><td>F3</td><td>TM43 - Sky Attack</td></tr>
<tr><td>F4</td><td>TM44 - Rest</td></tr>
<tr><td>F5</td><td>TM45 - Thunder Wave</td></tr>
<tr><td>F6</td><td>TM46 - Psywave</td></tr>
<tr><td>F7</td><td>TM47 - Explosion</td></tr>
<tr><td>F8</td><td>TM48 - Rock Slide</td></tr>
<tr><td>F9</td><td>TM49 - Tri Attack</td></tr>
<tr><td>FA</td><td>TM50 - Substitute</td></tr>
<tr><td>FB</td><td>TM51 - Cut</td></tr>
<tr><td>FC</td><td>TM52 - Fly</td></tr>
<tr><td>FD</td><td>TM53 - Surf</td></tr>
<tr><td>FE</td><td>TM54 - Strength</td></tr>
<tr><td>FF</td><td>TM55 - Flash</td></tr>

</tbody>
</table>

</div>

<div class="col-md-4">

<h4>Pokemon Cristal / Plata / Oro</h4>

<table class="table table-striped">
<thead>
<tr>
    <th>C&oacute;digo</th>
    <th>Nombre</th>
</tr>
</thead>
<tbody>
<tr><td>01</td><td>Master Ball</td></tr>
<tr><td>02</td><td>Ultra Ball</td></tr>
<tr><td>03</td><td>BrightPowder</td></tr>
<tr><td>04</td><td>Great Ball</td></tr>
<tr><td>05</td><td>PokeBall</td></tr>
<tr><td>06</td><td>Teru-Sama</td></tr>
<tr><td>07</td><td>Bicycle</td></tr>
<tr><td>08</td><td>Moon Stone</td></tr>
<tr><td>09</td><td>Antidote</td></tr>
<tr><td>0A</td><td>Burn Heal</td></tr>
<tr><td>0B</td><td>Ice Heal</td></tr>
<tr><td>0C</td><td>Awakening</td></tr>
<tr><td>0D</td><td>Parlyz Heal</td></tr>
<tr><td>0E</td><td>Full Restore</td></tr>
<tr><td>0F</td><td>Max Potion</td></tr>
<tr><td>10</td><td>Hyper Potion</td></tr>
<tr><td>11</td><td>Super Potion</td></tr>
<tr><td>12</td><td>Potion</td></tr>
<tr><td>13</td><td>Escape Rope</td></tr>
<tr><td>14</td><td>Repel</td></tr>
<tr><td>15</td><td>Max Elixer</td></tr>
<tr><td>16</td><td>Fire Stone</td></tr>
<tr><td>17</td><td>ThunderStone</td></tr>
<tr><td>18</td><td>Water Stone</td></tr>
<tr><td>1A</td><td>HP Up</td></tr>
<tr><td>1B</td><td>Protein</td></tr>
<tr><td>1C</td><td>Iron</td></tr>
<tr><td>1D</td><td>Carbos</td></tr>
<tr><td>1E</td><td>Lucky Punch</td></tr>
<tr><td>1F</td><td>Calcium</td></tr>
<tr><td>20</td><td>Rare Candy</td></tr>
<tr><td>21</td><td>X Accuracy</td></tr>
<tr><td>22</td><td>Leaf Stone</td></tr>
<tr><td>23</td><td>Metal Powder</td></tr>
<tr><td>24</td><td>Nugget</td></tr>
<tr><td>25</td><td>Poke Doll</td></tr>
<tr><td>26</td><td>Full Heal</td></tr>
<tr><td>27</td><td>Revive</td></tr>
<tr><td>28</td><td>Max Revive</td></tr>
<tr><td>29</td><td>Guard Spec</td></tr>
<tr><td>2A</td><td>Super Repel</td></tr>
<tr><td>2B</td><td>Max Repel 2c Dire hit</td></tr>
<tr><td>2E</td><td>Fresh Water</td></tr>
<tr><td>2F</td><td>Soda Pop</td></tr>
<tr><td>30</td><td>Lemonade</td></tr>
<tr><td>31</td><td>X Attack</td></tr>
<tr><td>33</td><td>X Defend</td></tr>
<tr><td>34</td><td>X Speed</td></tr>
<tr><td>35</td><td>X Special</td></tr>
<tr><td>36</td><td>Coin Case</td></tr>
<tr><td>37</td><td>Item Finder</td></tr>
<tr><td>39</td><td>Exp Share</td></tr>
<tr><td>3A</td><td>Old Rod</td></tr>
<tr><td>3B</td><td>Good Rod</td></tr>
<tr><td>3C</td><td>Silver Leaf 3d Super Rod</td></tr>
<tr><td>3E</td><td>PP Up</td></tr>
<tr><td>3F</td><td>Ether</td></tr>
<tr><td>40</td><td>Max Ether</td></tr>
<tr><td>41</td><td>Elixer</td></tr>
<tr><td>42</td><td>Red Scale</td></tr>
<tr><td>43</td><td>SecretPotion</td></tr>
<tr><td>44</td><td>S.S. Ticket</td></tr>
<tr><td>45</td><td>Mystery Egg</td></tr>
<tr><td>46</td><td>Clear Bell</td></tr>
<tr><td>47</td><td>Silver Wing</td></tr>
<tr><td>48</td><td>Moomoo Milk</td></tr>
<tr><td>49</td><td>Quick Claw</td></tr>
<tr><td>4A</td><td>PsnCureBerry</td></tr>
<tr><td>4B</td><td>Gold Leaf</td></tr>
<tr><td>4C</td><td>Soft Sand</td></tr>
<tr><td>4D</td><td>Sharp Beak</td></tr>
<tr><td>4E</td><td>PrzCureBerry</td></tr>
<tr><td>4F</td><td>Burnt Berry</td></tr>
<tr><td>50</td><td>Ice Berry</td></tr>
<tr><td>51</td><td>Poison Barb</td></tr>
<tr><td>52</td><td>King's Rock</td></tr>
<tr><td>53</td><td>Bitter Berry</td></tr>
<tr><td>54</td><td>Mint Berry</td></tr>
<tr><td>55</td><td>Red Apricorn</td></tr>
<tr><td>56</td><td>TinyMushroom</td></tr>
<tr><td>57</td><td>Big Mushroom</td></tr>
<tr><td>58</td><td>SilverPowder</td></tr>
<tr><td>59</td><td>Blu Apricorn</td></tr>
<tr><td>5B</td><td>Amulet Coin</td></tr>
<tr><td>5C</td><td>Ylw Apricorn</td></tr>
<tr><td>5D</td><td>Grn Apricorn</td></tr>
<tr><td>5E</td><td>Cleanse Tag</td></tr>
<tr><td>5F</td><td>Mystic Water</td></tr>
<tr><td>60</td><td>TwistedSpoon</td></tr>
<tr><td>61</td><td>Wht Apricorn</td></tr>
<tr><td>62</td><td>Blackbelt</td></tr>
<tr><td>63</td><td>Blk Apricorn</td></tr>
<tr><td>65</td><td>Pnk Apricorn</td></tr>
<tr><td>66</td><td>Blackglasses</td></tr>
<tr><td>67</td><td>SlowPoketail</td></tr>
<tr><td>68</td><td>Pink Bow</td></tr>
<tr><td>69</td><td>Stick</td></tr>
<tr><td>6A</td><td>Smoke Ball</td></tr>
<tr><td>6B</td><td>NeverMeltIce</td></tr>
<tr><td>6C</td><td>Magnet</td></tr>
<tr><td>6D</td><td>MiracleBerry</td></tr>
<tr><td>6E</td><td>Pearl</td></tr>
<tr><td>6F</td><td>Big Pearl</td></tr>
<tr><td>70</td><td>Everstone</td></tr>
<tr><td>71</td><td>Spell Tag</td></tr>
<tr><td>72</td><td>RageCandybar</td></tr>
<tr><td>73</td><td>GS Ball</td></tr>
<tr><td>74</td><td>Blue Card</td></tr>
<tr><td>75</td><td>Miracle Seed 76 Thick Club</td></tr>
<tr><td>77</td><td>Focus Band</td></tr>
<tr><td>79</td><td>EnergyPowder</td></tr>
<tr><td>7A</td><td>Enery Root</td></tr>
<tr><td>7B</td><td>Heal Powder</td></tr>
<tr><td>7C</td><td>Revival Herb</td></tr>
<tr><td>7D</td><td>Hard Stone</td></tr>
<tr><td>7E</td><td>Lucky Egg</td></tr>
<tr><td>7F</td><td>Card Key</td></tr>
<tr><td>80</td><td>Machine Part</td></tr>
<tr><td>81</td><td>Egg Ticket</td></tr>
<tr><td>82</td><td>Lost Item</td></tr>
<tr><td>83</td><td>Stardust</td></tr>
<tr><td>84</td><td>Star Piece</td></tr>
<tr><td>85</td><td>Basement Key</td></tr>
<tr><td>86</td><td>Pass</td></tr>
<tr><td>8A</td><td>Charcoal</td></tr>
<tr><td>8B</td><td>Berry Juice</td></tr>
<tr><td>8C</td><td>Scope Lens</td></tr>
<tr><td>8F</td><td>Metal Coat</td></tr>
<tr><td>90</td><td>Dragon Fang</td></tr>
<tr><td>92</td><td>Leftovers</td></tr>
<tr><td>96</td><td>MysteryBerry</td></tr>
<tr><td>97</td><td>Dragon Scale</td></tr>
<tr><td>98</td><td>Berserk Gene</td></tr>
<tr><td>9C</td><td>Sacred Ash</td></tr>
<tr><td>9D</td><td>Heavy Ball</td></tr>
<tr><td>9E</td><td>Flower Mail</td></tr>
<tr><td>9F</td><td>Level Ball</td></tr>
<tr><td>A0</td><td>Lure Ball</td></tr>
<tr><td>A1</td><td>Fast Ball</td></tr>
<tr><td>A3</td><td>Light Ball</td></tr>
<tr><td>A4</td><td>Friend Ball</td></tr>
<tr><td>A5</td><td>Moon Ball</td></tr>
<tr><td>A6</td><td>Love Ball</td></tr>
<tr><td>A7</td><td>Normal Box</td></tr>
<tr><td>A8</td><td>Gorgeous Box</td></tr>
<tr><td>A9</td><td>Sun Stone</td></tr>
<tr><td>AA</td><td>Polka Dot</td></tr>
<tr><td>AC</td><td>Up-Grade</td></tr>
<tr><td>AD</td><td>Berry</td></tr>
<tr><td>AE</td><td>Gold Berry</td></tr>
<tr><td>AF</td><td>SquirtBottle</td></tr>
<tr><td>B1</td><td>Park Ball</td></tr>
<tr><td>B2</td><td>Rainbow Wing</td></tr>
<tr><td>B4</td><td>Brick Piece</td></tr>
<tr><td>B5</td><td>Surf Mail</td></tr>
<tr><td>B6</td><td>LiteBlueMail</td></tr>
<tr><td>B7</td><td>PortraitMail</td></tr>
<tr><td>B8</td><td>Lovely Mail</td></tr>
<tr><td>B9</td><td>Eon Mail</td></tr>
<tr><td>BA</td><td>Morph Mail</td></tr>
<tr><td>BB</td><td>BlueSky Mail</td></tr>
<tr><td>BC</td><td>Music Mail</td></tr>
<tr><td>BD</td><td>Mirage Mail</td></tr>

</tbody>
</table>

</div>

<div class="col-md-4">

<h4>Pokemon Rub&iacute; / Zafiro / Esmeralda / Rojo Fuego / Verde Hoja</h4>

<table class="table table-striped">
<thead>
<tr>
    <th>C&oacute;digo</th>
    <th>Nombre</th>
</tr>
</thead>
<tbody>
<tr><td>0100</td><td>Master Ball</td></tr>
<tr><td>0200</td><td>Ultra Ball</td></tr>
<tr><td>0300</td><td>Great Ball</td></tr>
<tr><td>0400</td><td>Poke Ball</td></tr>
<tr><td>0500</td><td>Safari Ball</td></tr>
<tr><td>0600</td><td>Net Ball</td></tr>
<tr><td>0700</td><td>Dive Ball</td></tr>
<tr><td>0800</td><td>Nest Ball</td></tr>
<tr><td>0900</td><td>Repeat Ball</td></tr>
<tr><td>0A00</td><td>Timer Ball</td></tr>
<tr><td>0B00</td><td>Luxury Ball</td></tr>
<tr><td>0C00</td><td>Premier Ball</td></tr>
<tr><td>0D00</td><td>Potion</td></tr>
<tr><td>0E00</td><td>Antidote</td></tr>
<tr><td>0F00</td><td>Burn Heal</td></tr>
<tr><td>1000</td><td>Ice Heal</td></tr>
<tr><td>1100</td><td>Awakening</td></tr>
<tr><td>1200</td><td>Parlyz Heal</td></tr>
<tr><td>1300</td><td>Full Restore</td></tr>
<tr><td>1400</td><td>Max Potion</td></tr>
<tr><td>1500</td><td>Hyper Potion</td></tr>
<tr><td>1600</td><td>Super Potion</td></tr>
<tr><td>1700</td><td>Full Heal</td></tr>
<tr><td>1800</td><td>Revive</td></tr>
<tr><td>1900</td><td>Max Revive</td></tr>
<tr><td>1A00</td><td>Fresh Water</td></tr>
<tr><td>1B00</td><td>Soda Pop</td></tr>
<tr><td>1C00</td><td>Lemonade</td></tr>
<tr><td>1D00</td><td>Moomoo Milk</td></tr>
<tr><td>1E00</td><td>Energy Powder</td></tr>
<tr><td>1F00</td><td>Energy Root</td></tr>
<tr><td>2000</td><td>Heal Powder</td></tr>
<tr><td>2100</td><td>Revival Herb</td></tr>
<tr><td>2200</td><td>Ether</td></tr>
<tr><td>2300</td><td>Max Ether</td></tr>
<tr><td>2400</td><td>Elixer</td></tr>
<tr><td>2500</td><td>Max Elixer</td></tr>
<tr><td>2600</td><td>Lava Cookie</td></tr>
<tr><td>2700</td><td>Blue Flute</td></tr>
<tr><td>2800</td><td>Yellow Flute</td></tr>
<tr><td>2900</td><td>Red Flute</td></tr>
<tr><td>2A00</td><td>Black Flute</td></tr>
<tr><td>2B00</td><td>White Flute</td></tr>
<tr><td>2C00</td><td>Berry Juice</td></tr>
<tr><td>2D00</td><td>Sacred Ash</td></tr>
<tr><td>2E00</td><td>Shoal Salt</td></tr>
<tr><td>2F00</td><td>Shoal Shell</td></tr>
<tr><td>3000</td><td>Red Shard</td></tr>
<tr><td>3100</td><td>Blue Shard</td></tr>
<tr><td>3200</td><td>Yellow Shard</td></tr>
<tr><td>3300</td><td>Green Shard</td></tr>
<tr><td>3F00</td><td>HP Up</td></tr>
<tr><td>4000</td><td>Protein</td></tr>
<tr><td>4100</td><td>Iron</td></tr>
<tr><td>4200</td><td>Carbos</td></tr>
<tr><td>4300</td><td>Calcium</td></tr>
<tr><td>4400</td><td>Rare Candy</td></tr>
<tr><td>4500</td><td>PP Up</td></tr>
<tr><td>4600</td><td>Zinc</td></tr>
<tr><td>4700</td><td>PP Max</td></tr>
<tr><td>4900</td><td>Guard Spec</td></tr>
<tr><td>4A00</td><td>Dire Hit</td></tr>
<tr><td>4B00</td><td>X Attack</td></tr>
<tr><td>4C00</td><td>X Defend</td></tr>
<tr><td>4D00</td><td>X Speed</td></tr>
<tr><td>4E00</td><td>X Accuracy</td></tr>
<tr><td>4F00</td><td>X Special</td></tr>
<tr><td>5000</td><td>Poke Doll</td></tr>
<tr><td>5100</td><td>Fluffy Tail</td></tr>
<tr><td>5300</td><td>Super Repel</td></tr>
<tr><td>5400</td><td>Max Repel</td></tr>
<tr><td>5500</td><td>Escape Rope</td></tr>
<tr><td>5600</td><td>Repel</td></tr>
<tr><td>5D00</td><td>Sun Stone</td></tr>
<tr><td>5E00</td><td>Moon Stone</td></tr>
<tr><td>5F00</td><td>Fire Stone</td></tr>
<tr><td>6000</td><td>Thunder Stone</td></tr>
<tr><td>6100</td><td>Water Stone</td></tr>
<tr><td>6200</td><td>Leaf Stone</td></tr>
<tr><td>6700</td><td>Tiny Mushroom</td></tr>
<tr><td>6800</td><td>Big Mushroom</td></tr>
<tr><td>6A00</td><td>Pearl</td></tr>
<tr><td>6B00</td><td>Big Pearl</td></tr>
<tr><td>6C00</td><td>Stardust</td></tr>
<tr><td>6D00</td><td>Star Piece</td></tr>
<tr><td>6E00</td><td>Nugget</td></tr>
<tr><td>6F00</td><td>Heart Scale</td></tr>
<tr><td>7900</td><td>Orange Mail</td></tr>
<tr><td>7A00</td><td>Harbor Mail</td></tr>
<tr><td>7B00</td><td>Glitter Mail</td></tr>
<tr><td>7C00</td><td>Mech Mail</td></tr>
<tr><td>7D00</td><td>Wood Mail</td></tr>
<tr><td>7E00</td><td>Wave Mail</td></tr>
<tr><td>7F00</td><td>Bead Mail</td></tr>
<tr><td>8000</td><td>Shadow Mail</td></tr>
<tr><td>8100</td><td>Tropic Mail</td></tr>
<tr><td>8200</td><td>Dream Mail</td></tr>
<tr><td>8300</td><td>Fab Mail</td></tr>
<tr><td>8400</td><td>Retro Mail</td></tr>
<tr><td>8500</td><td>Cheri Berry</td></tr>
<tr><td>8600</td><td>Chesto Berry</td></tr>
<tr><td>8700</td><td>Pecha Berry</td></tr>
<tr><td>8800</td><td>Rawst Berry</td></tr>
<tr><td>8900</td><td>Aspear Berry</td></tr>
<tr><td>8A00</td><td>Leppa Berry</td></tr>
<tr><td>8B00</td><td>Oran Berry</td></tr>
<tr><td>8C00</td><td>Persim Berry</td></tr>
<tr><td>8D00</td><td>Lum Berry</td></tr>
<tr><td>8E00</td><td>Sitrus Berry</td></tr>
<tr><td>8F00</td><td>Figy Berry</td></tr>
<tr><td>9000</td><td>Wiki Berry</td></tr>
<tr><td>9100</td><td>Mago Berry</td></tr>
<tr><td>9200</td><td>Aguav Berry</td></tr>
<tr><td>9300</td><td>Iapapa Berry</td></tr>
<tr><td>9400</td><td>Razz Berry</td></tr>
<tr><td>9500</td><td>Bluk Berry</td></tr>
<tr><td>9600</td><td>Nanab Berry</td></tr>
<tr><td>9700</td><td>Wepear Berry</td></tr>
<tr><td>9800</td><td>Pinap Berry</td></tr>
<tr><td>9900</td><td>Pomeg Berry</td></tr>
<tr><td>9A00</td><td>Kelpsy Berry</td></tr>
<tr><td>9B00</td><td>Qualot Berry</td></tr>
<tr><td>9C00</td><td>Hondew Berry</td></tr>
<tr><td>9D00</td><td>Grepa Berry</td></tr>
<tr><td>9E00</td><td>Tamato Berry</td></tr>
<tr><td>9F00</td><td>Cornn Berry</td></tr>
<tr><td>A000</td><td>Magost Berry</td></tr>
<tr><td>A100</td><td>Rabuta Berry</td></tr>
<tr><td>A200</td><td>Nomel Berry</td></tr>
<tr><td>A300</td><td>Spelon Berry</td></tr>
<tr><td>A400</td><td>Pamtre Berry</td></tr>
<tr><td>A500</td><td>Watmel Berry</td></tr>
<tr><td>A600</td><td>Durin Berry</td></tr>
<tr><td>A700</td><td>Belue Berry</td></tr>
<tr><td>A800</td><td>Liechi Berry</td></tr>
<tr><td>A900</td><td>Ganlon Berry</td></tr>
<tr><td>AA00</td><td>Salac Berry</td></tr>
<tr><td>AB00</td><td>Petaya Berry</td></tr>
<tr><td>AC00</td><td>Apicot Berry</td></tr>
<tr><td>AD00</td><td>Lansat Berry</td></tr>
<tr><td>AE00</td><td>Starf Berry</td></tr>
<tr><td>AF00</td><td>Enigma Berry</td></tr>
<tr><td>B300</td><td>Bright Powder</td></tr>
<tr><td>B400</td><td>White Herb</td></tr>
<tr><td>B500</td><td>Macho Brace</td></tr>
<tr><td>B600</td><td>Exp. Share</td></tr>
<tr><td>B700</td><td>Quick Claw</td></tr>
<tr><td>B800</td><td>Soothe Bell</td></tr>
<tr><td>B900</td><td>Mental Herb</td></tr>
<tr><td>C000</td><td>Deep Sea Tooth</td></tr>
<tr><td>C100</td><td>Deep Sea Scale</td></tr>
<tr><td>C200</td><td>Smoke Ball</td></tr>
<tr><td>C300</td><td>Everstone</td></tr>
<tr><td>C400</td><td>Focus Band</td></tr>
<tr><td>C500</td><td>Lucky Egg</td></tr>
<tr><td>C600</td><td>Scope Lens</td></tr>
<tr><td>C700</td><td>Metal Coat</td></tr>
<tr><td>C800</td><td>Leftovers</td></tr>
<tr><td>C900</td><td>Dragon Scale</td></tr>
<tr><td>CA00</td><td>Light Ball</td></tr>
<tr><td>CB00</td><td>Soft Sand</td></tr>
<tr><td>CC00</td><td>Hard Stone</td></tr>
<tr><td>CD00</td><td>Miracle Seed</td></tr>
<tr><td>CE00</td><td>Black Glasses</td></tr>
<tr><td>CF00</td><td>Black Belt</td></tr>
<tr><td>D000</td><td>Magnet</td></tr>
<tr><td>D100</td><td>Mystic Water</td></tr>
<tr><td>D200</td><td>Sharp Beak</td></tr>
<tr><td>D300</td><td>Poison Barb</td></tr>
<tr><td>D400</td><td>Never Melt Ice</td></tr>
<tr><td>D500</td><td>Spell Tag</td></tr>
<tr><td>D600</td><td>Twisted Spoon</td></tr>
<tr><td>D700</td><td>Charcoal</td></tr>
<tr><td>D800</td><td>Dragon Fang</td></tr>
<tr><td>D900</td><td>Silk Scarf</td></tr>
<tr><td>DA00</td><td>Up-Grade</td></tr>
<tr><td>DB00</td><td>Shell Bell</td></tr>
<tr><td>DC00</td><td>Sea Incense</td></tr>
<tr><td>DD00</td><td>Lax Incense</td></tr>
<tr><td>DE00</td><td>Lucky Punch</td></tr>
<tr><td>DF00</td><td>Metal Powder</td></tr>
<tr><td>E000</td><td>Thick Club</td></tr>
<tr><td>E100</td><td>Stick</td></tr>
<tr><td>FE00</td><td>Red Scarf</td></tr>
<tr><td>FF00</td><td>Blue Scarf</td></tr>
<tr><td>0001</td><td>Pink Scarf</td></tr>
<tr><td>0101</td><td>Green Scarf</td></tr>
<tr><td>0201</td><td>Yellow Scarf</td></tr>
<tr><td>0301</td><td>Mach Bike</td></tr>
<tr><td>0401</td><td>Coin Case</td></tr>
<tr><td>0501</td><td>Item Finder</td></tr>
<tr><td>0601</td><td>Old Rod</td></tr>
<tr><td>0701</td><td>Good Rod</td></tr>
<tr><td>0801</td><td>Super Rod</td></tr>
<tr><td>0901</td><td>S.S. Ticket</td></tr>
<tr><td>0A01</td><td>Contest Pass</td></tr>
<tr><td>0C01</td><td>Wailmer Pail</td></tr>
<tr><td>0D01</td><td>Devon Goods</td></tr>
<tr><td>0E01</td><td>Soot Sack</td></tr>
<tr><td>0F01</td><td>Basement Key</td></tr>
<tr><td>1001</td><td>Acro Bike</td></tr>
<tr><td>1101</td><td>Pokeblock Case</td></tr>
<tr><td>1201</td><td>Letter</td></tr>
<tr><td>1301</td><td>Eon ticket</td></tr>
<tr><td>1401</td><td>Red Orb</td></tr>
<tr><td>1501</td><td>Blue Orb</td></tr>
<tr><td>1601</td><td>Scanner</td></tr>
<tr><td>1701</td><td>Go-Goggles</td></tr>
<tr><td>1801</td><td>Meteorite</td></tr>
<tr><td>1901</td><td>RM. 1 Key</td></tr>
<tr><td>1A01</td><td>RM. 2 Key</td></tr>
<tr><td>1B01</td><td>RM. 4 Key</td></tr>
<tr><td>1C01</td><td>RM. 6 Key</td></tr>
<tr><td>1D01</td><td>Storage Key</td></tr>
<tr><td>1E01</td><td>Root Fossil</td></tr>
<tr><td>1F01</td><td>Claw Fossil</td></tr>
<tr><td>2001</td><td>Devon Scope</td></tr>
<tr><td>2101</td><td>TM01</td></tr>
<tr><td>2201</td><td>TM02</td></tr>
<tr><td>2301</td><td>TM03</td></tr>
<tr><td>2401</td><td>TM04</td></tr>
<tr><td>2501</td><td>TM05</td></tr>
<tr><td>2601</td><td>TM06</td></tr>
<tr><td>2701</td><td>TM07</td></tr>
<tr><td>2801</td><td>TM08</td></tr>
<tr><td>2901</td><td>TM09</td></tr>
<tr><td>2A01</td><td>TM10</td></tr>
<tr><td>2B01</td><td>TM11</td></tr>
<tr><td>2C01</td><td>TM12</td></tr>
<tr><td>2D01</td><td>TM13</td></tr>
<tr><td>2E01</td><td>TM14</td></tr>
<tr><td>2F01</td><td>TM15</td></tr>
<tr><td>3001</td><td>TM16</td></tr>
<tr><td>3101</td><td>TM17</td></tr>
<tr><td>3201</td><td>TM18</td></tr>
<tr><td>3301</td><td>TM19</td></tr>
<tr><td>3401</td><td>TM20</td></tr>
<tr><td>3501</td><td>TM21</td></tr>
<tr><td>3601</td><td>TM22</td></tr>
<tr><td>3701</td><td>TM23</td></tr>
<tr><td>3801</td><td>TM24</td></tr>
<tr><td>3901</td><td>TM25</td></tr>
<tr><td>3A01</td><td>TM26</td></tr>
<tr><td>3B01</td><td>TM27</td></tr>
<tr><td>3C01</td><td>TM28</td></tr>
<tr><td>3D01</td><td>TM29</td></tr>
<tr><td>3E01</td><td>TM30</td></tr>
<tr><td>3F01</td><td>TM31</td></tr>
<tr><td>4001</td><td>TM32</td></tr>
<tr><td>4101</td><td>TM33</td></tr>
<tr><td>4201</td><td>TM34</td></tr>
<tr><td>4301</td><td>TM35</td></tr>
<tr><td>4401</td><td>TM36</td></tr>
<tr><td>4501</td><td>TM37</td></tr>
<tr><td>4601</td><td>TM38</td></tr>
<tr><td>4701</td><td>TM39</td></tr>
<tr><td>4801</td><td>TM40</td></tr>
<tr><td>4901</td><td>TM41</td></tr>
<tr><td>4A01</td><td>TM42</td></tr>
<tr><td>4B01</td><td>TM43</td></tr>
<tr><td>4C01</td><td>TM44</td></tr>
<tr><td>4D01</td><td>TM45</td></tr>
<tr><td>4E01</td><td>TM46</td></tr>
<tr><td>4F01</td><td>TM47</td></tr>
<tr><td>5001</td><td>TM48</td></tr>
<tr><td>5101</td><td>TM49</td></tr>
<tr><td>5201</td><td>TM50</td></tr>
<tr><td>5301</td><td>HM01</td></tr>
<tr><td>5401</td><td>HM02</td></tr>
<tr><td>5501</td><td>HM03</td></tr>
<tr><td>5601</td><td>HM04</td></tr>
<tr><td>5701</td><td>HM05</td></tr>
<tr><td>5801</td><td>HM06</td></tr>
<tr><td>5901</td><td>HM07</td></tr>
<tr><td>5A01</td><td>HM08</td></tr>
<tr><td>5D01</td><td>Oak's Parcel</td></tr>
<tr><td>5E01</td><td>Poke Flute</td></tr>
<tr><td>5F01</td><td>Secret Key</td></tr>
<tr><td>6001</td><td>Bike Voucher</td></tr>
<tr><td>6101</td><td>Gold Teeth</td></tr>
<tr><td>6201</td><td>Old Amber</td></tr>
<tr><td>6301</td><td>Card Key</td></tr>
<tr><td>6401</td><td>Lift Key</td></tr>
<tr><td>6501</td><td>Helix Fossil</td></tr>
<tr><td>6601</td><td>Dome Fossil</td></tr>
<tr><td>6701</td><td>Silphscope</td></tr>
<tr><td>6801</td><td>Bicycle</td></tr>
<tr><td>6901</td><td>Town Map</td></tr>
<tr><td>6A01</td><td>Vs Seeker</td></tr>
<tr><td>6B01</td><td>Fame Checker</td></tr>
<tr><td>6C01</td><td>Tm Case</td></tr>
<tr><td>6D01</td><td>Berry Pouch</td></tr>
<tr><td>6E01</td><td>Teachy Tv</td></tr>
<tr><td>6F01</td><td>Tri-pass</td></tr>
<tr><td>7001</td><td>Rainbow Pass</td></tr>
<tr><td>7101</td><td>Tea</td></tr>
<tr><td>7201</td><td>Mystic Ticket</td></tr>
<tr><td>7301</td><td>Aurora Ticket</td></tr>
<tr><td>7401</td><td>Powder Jar</td></tr>
<tr><td>7501</td><td>Ruby</td></tr>
<tr><td>7601</td><td>Sapphire</td></tr>
<tr><td>7701</td><td>Magma Emblem</td></tr>
<tr><td>7801</td><td>Old Sea Chart</td></tr>
</tbody>
</table>

</div>

</div>
</div>


<div class="col-md-1">
</div>

</div>

<H3>Si lo quieres hackear con Android</H3>

Si lo tuyo es Android, y te gustar&iacute;a hackearlo all&iacute; y no en Windows, te recomiendo bajar <a target="_blank" href="https://play.google.com/store/apps/details?id=tuba.tools&hl=es_419">este</a> editor Hex para Android (o baja otro si prefieres), y el excelente emulador <a target="_blank" href=" https://play.google.com/store/apps/details?id=com.bslapps2.gbc&hl=es_419">Gameboy Color A.D.</a>. &#161;Con esto podr&aacute;s editar en el mismo Android tus ROMs!, lo que s&iacute; recuerda respaldarla antes de modificarla.

<br><br>
Bueno, espero les sirva esta peque&ntilde;a y r&aacute;pida gu&iacute;a.

<br><br>
<hr style="width: 100%; height: 2px;">

<a href="<?php echo "../" . $linkVolver ?>">Volver</a>
<br><br>

<?php 
include '../disq.php';
?>

<center><?php include ('../pie.php'); ?></center>
</small>
</div>
</body>
</html>