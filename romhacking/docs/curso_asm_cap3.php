<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>La Web de Dark-N - Curso ASM SNES. Cap�tulo 3: Modos de Direccionamiento</title>
</head>


<body>
<span style="font-family: Verdana;">
<small><a href="../doc_traduc.php">Volver</a></small>

<hr style="width: 100%; height: 2px;">
<center>
<big>Cap�tulo 3: Modos de Direccionamiento</big>
<small>
<br><br>
Por Dark-N: <?php include ('../mailsolo.php'); ?>
<br>
</small>
<span style="font-family: Verdana;">
<small>
<a href="http://darknromhacking.com/">http://darknromhacking.com</a></small></span><small><br>
</small>
</center>
<hr style="width: 100%; height: 2px;"><br>
<small>

<b><font size=3 face="Verdana">Versiones</font></b><br>
<br><li>1.2:
<br>&nbsp;&nbsp;&nbsp;-Pasado a HTML.

<br><li>1.1:
<br>&nbsp;&nbsp;&nbsp;-Revisado y corregido algunos errores de explicaci�n en algunos MD complicados.

<br><li>1.0:
<br>&nbsp;&nbsp;&nbsp;-Versi�n original.


<br><br><br><b><font size=3 face="Verdana">Introducci�n</font></b><br><br>
�Hola a todos! Si me he demorado en sacar este cap�tulo es que ahora realmente estoy aprendiendo bien este asunto del asm y no quer�a hacer una gu�a simplona donde liste los modos de direccionamiento en no mas de 3 p�ginas, sino que quise profundizar en los temas de como obtener la Direcci�n Efectiva y otras cosas.<br><br>

Los <b>Modos de Direccionamiento (MD desde ahora)</b> es la forma como el procesador interpreta un comando o un estilo de escribir una direcci�n, as� si escribimos LDA #$20 no es lo mismo que poner LDA $20, el procesador entiende diferente ambas instrucciones.<br><br>

En este capitulo ense�are los MD b�sicos, como leerlos y escribirlos. Tambi�n dejar� en la secci�n <b>Gusto a Poco</b>, algunos ejercicios con sus respuestas.<br><br>

Nota: en este cap�tulo las cosas se empiezan a complicar, as� que no avancen hasta que entendieron o creen que entendieron.

<br><br><br><b><font size=3 face="Verdana">Los Modos de Direccionamiento de la SNES</font></b><br><br>

Estos son los modos de direccionamiento b�sicos usados por la SNES, quiz�s no lo entiendan simplemente mirando la lista, pero mas abajo ense�are como leerlos y escribir cualquier MD:

<br><table align="center" border="0" width="70%">
<tr bgcolor="#CCCCCC"><td>Modo de Dir</td><td>Ejemplo</td></tr>
<tr><td>Immediate</td><td>LDA #$12</td></tr>
<tr><td>Absolute</td><td>LDA $1234</td></tr>
<tr><td>Absolute Long</td><td>LDA $123456</td></tr>
<tr><td>Absolute Long Indirect</td><td>LDA ($123456)</td></tr>
<tr><td>Absolute Long Indexed X</td><td>LDA $123456,X</td></tr>
<tr><td>Absolute Long Indexed Y</td><td>LDA $123456,Y</td></tr>
<tr><td>Absolute Indexed X Indirect</td><td>JMP ($1234,X)</td></tr>
<tr><td>Absolute Indexed Y Indirect</td><td>LDA ($1234,Y)</td></tr>
<tr><td>Absolute Indirect Long</td><td>JMP [$1234]</td></tr>
<tr><td>Direct Page</td><td>LDA $12</td></tr>
<tr><td>Direct Page Indirect</td><td>LDA ($12)</td></tr>
<tr><td>Direct Page Indirect Indexed,Y</td><td>STA ($12), Y</td></tr>
<tr><td>Direct Page Indirect Indexed,X</td><td>STA ($1234), X</td></tr>
<tr><td>Direct Page Indirect Long</td><td>LDA [$12]</td></tr>
<tr><td>Direct Page Indexed,X</td><td>LDA $12,X</td></tr>
<tr><td>Direct Page Indexed,Y</td><td>LDA $12,Y</td></tr>
<tr><td>Direct Page Indirect Long Indexed,X</td><td>LDA [$1C],X</td></tr>
<tr><td>Direct Page Indirect Long Indexed,Y</td><td>LDA [$1C],Y</td></tr>
<tr><td>Program Counter Relative</td><td>BEQ $04</td></tr>
<tr><td>Program Counter Relative Long</td><td>BRL $1234</td></tr>
<tr><td>Stack Relative</td><td>LDA 15,S</td></tr>
<tr><td>Stack Relative Indirect Indexed X</td><td>LDA (09,S),X</td></tr>
<tr><td>Stack Relative Indirect Indexed Y</td><td>LDA (2B,S),Y</td></tr>
<tr><td>Block Move</td><td>MVP 0,0</td></tr>
<tr><td>Accumulator</td><td>ASL A</td></tr>                   		
</table>

<br><br><br><b><font size=3 face="Verdana">Notaciones de los MD</font></b><br><br>

Lo primero es saber las notaciones de Long, Indirect, Indexed, Indirect Long y Stack Relative que son las m�s usadas en los Modos de Direccionamiento.

<br><br><li><b>Indirect = ( )</b>
<br>&nbsp;&nbsp;&nbsp;Indirecto quiere decir que accedemos al <b>contenido</b> de una direcci�n de 1/2 bytes.

<br><br><li><b>Indexed X = ,X</b>
<br>&nbsp;&nbsp;&nbsp;A un Direcci�n le agregamos el �ndice que en este caso es X, se escribe  con una &#8216;,&#8216; antes de la X.

<br><br><li><b>Indirect Long = [   ]</b>
<br>&nbsp;&nbsp;&nbsp;Este significa que accedemos a una direcci�n de 3 bytes y no de 2, es decir: Banco + Offset (ejemplo $FF:$1234).

<br><br><li><b>Stack Relative = ,S</b>
<br>&nbsp;&nbsp;&nbsp;Se le agrega a la direcci�n una S que es el Stack o Pila con una coma &#8216;,&#8217; antes.

<br><br><li><b>Direcci�n Efectiva (Efective Address o EA)</b>
<br>&nbsp;&nbsp;&nbsp;Es la direcci�n final que se procesa, es como la &#8220;ecuaci�n&#8221; o &#8220;c�digo&#8221; que indica la direcci�n donde debe acceder la CPU para realizar la operaci�n. Su notaci�n es: 
&nbsp;&nbsp;&nbsp;<b>EA= Mem< num_bytes dir leer/escribir > [< direcci�n >].</b>


<br><br><br><br><b><font size=3 face="Verdana">Los MD m�s Comunes</font></b><br><br>

Los MD se usan en cada cargada de un registro (alg�n LDA) o de un guardado/store en la memoria (STA). <br><br>
Ahora explicare los MD, algunos son iguales que otros, solo cambia el uso del registro X o Y.


<br><br><br><b>Immediate (Immediato)</b><br><br>

Aqu� cargamos DATOS en la instrucci�n. Aqu� no hay <b>Direcci�n Efectiva</b> o <b>EA</b>. <br>
<b>Effective Adress(EA) = no hay </b><br>
Esto de EA quedar� mas claro con otros MD.<br><br>

Ejemplo 1: <br>
LDA #$05   ; cargamos el valor/dato 5 en A. Si A esta seteado en modo de 8 bits entonces el valor a almacenar no puede ser mayor a 8 bits. Y si A esta seteado en modo de 16 bits, no puede almacenar un valor mayor a 0xFFFF.<br><br>

Recuerda esto:<br><br>
-Un valor/direcci�n de 8 bits puede ser de 00 a FF.<br>
-Un valor/direcci�n de 16 bits puede ser de 0000 a FFFF.<br>
-Un valor/direcci�n de 24 bits puede ser de 000000 a FFFFFF.<br>


<br><br><br><b>Absolute (Absoluto)</b><br><br>
<b>EA = Dir</b><br><br>

En este caso, EA = dir, ya que la direcci�n efectiva es la direcci�n a la que se accede. Aqu� siempre la direcci�n es siempre de 2 bytes.<br><br>

Ejemplo:<br>
LDA $123C  ; Carga UN byte de la direcci�n 123C en caso que el BIT del Modo del CPU del registro P, esta 8 bits y carga DOS bytes si el Bit de estado del CPU esta en 16. Esta direcci�n puede ser en la RAM o Rom seg�n el Banco, en este caso como no se dice nada, se asume el banco del registro PBR.<br><br>

Entonces la EA para este ejemplo ser�a: (lo har� paso a paso para que lo entiendan):<br>
EA= $123C

<br><br><br><b>Absolute Long (Absoluto Largo)</b><br><br>
<b>EA = Dir</b><br><br>

En Absolute Long/Indirect Long la direcci�n es de 3 bytes, as� que podemos en una sola instrucci�n acceder al mapa completo de instrucciones ya que podemos hacer LDA $AB3456 (banco $AB y direcci�n $3456). <br><br>

Ejemplo1:<br>
LDA $808000 ; Carga en A 1 o 2 bytes de $80:8000 (banco 80, direcci�n 8000). Recuerda 1 o 2 bytes debido a como este seteado el bit de Modo del CPU.<br>
EA=$808000


<br><br><br><b>Absolute Long Indirect (Aboluto Largo Indirecto)</b><br><br>
<b>EA=Mem2[dir]</b><br><br>

Accedemos a una direcci�n de 3 bytes y esa direcci�n contiene otra de 2 bytes.<br><br>

Ejemplo1:<br>
LDA ($123456) ; Dentro de la direcci�n $123456 esta la direcci�n $1234 y dentro de $1234 esta al valor que se asigna a A.<br><br>

�Mem2?<br>
EA=vamos por paso...<br>
EA= $123456  -> ir a memoria y  ubicar la direcci�n de 3 bytes<br>
EA=  Mem2[$123456]   ->dentro de esa direcci�n encontrar la siguiente direcci�n de 2 bytes, pero �Por qu� de 2 bytes? Es as� ya que hablamos de Absolute Long Indirect  y no Absolute Long Indirect Long, El &#8220;Indirect&#8221; es siempre de 2 bytes, el &#8220;Indirect Long&#8221; es de 3 bytes, por eso usamos un Mem2 y no Mem3.


<br><br><br><b>Absolute Long Indexed X/Y (Absoluto Largo Indexado X/Y)</b><br><br>
<b>EA=dir+X/Y</b>, simplemente la direcci�n efectiva es la direcci�n mas el valor del �ndice X/Y.<br><br>

El �ndice X/Y aparece aqu� por primera vez, ya que en los modos <b>indexados</b> se ocupan para hacer un &#8220;recorrido&#8221; de direcciones o asignaciones, es como hacer un &#8220;ciclo for&#8221;. 
<br>�Qu� pasa si queremos asignar a A los valores que estan desde una cierta direcci�n de memoria a otra direcci�n, con unas pocas instrucciones? 
<br>F�cil, con el �ndice X/Y podemos incrementarlos y usarlos con LDA, luego con un Salto (recuerden cap�tulo 2) podemos hacer que se ejecute de nuevo LDA con X/Y pero con el X/Y incrementado y as� sucesivamente.<br><br>

Ejemplo1:<br>
LDA $7E8000, X ; Carga en A 1/2 bytes de $7E:8000 + X. <br>
El CPU lo traduce as�: &#8220;cargar 1 /2 bytes en Acumulador lo que esta en la direcci�n [8000 + el valor del registro X], del banco 7E, es decir de la RAM. �f�cil, no?<br><br>

EA=$7E8000+X<br><br>

Ejemplo2:<br>
LDA $FF9090,Y ; Carga A con 1/2 bytes que estan en $FF:9090 + Y


<br><br><br><b>Absolute Indexed X/Y Indirect (Absoluto Indexado Indirecto)</b><br><br>
<b>EA= Mem2[dir+X/Y]</b><br><br>

Aqu� se empieza a complicar la cosa un poco, ya que aqu� hablamos de una Direcci�n de una Direcci�n Indexada, veamos como es esto con ejemplos.<br><br>

Ejemplo1:<br>
LDA ($1238,X)    ; Accedemos a la direcci�n 1238 + X y dentro hay otra direcci�n, y en esta ultima direcci�n hay 1 o 2 bytes que se asigna a A. �Quieren un ejemplo grafico de esta situaci�n? Aprovechar� para ense�ar varias cosas que utilizare de aqu� en adelante.<br>
Asumamos que tenemos esto en memoria RAM:
<br><img src="img/cap31.PNG"><br>

Asumiendo que X=4hex o 0x4 que es lo mismo, tenemos que 1238 + 4 = 123C y dentro de esta direcci�n esta el valor hex 0x14F0. Desde ahora los valores hex dentro de una direcci�n de memoria, le pondremos un &#8220;0x&#8221; antes. Es la notaci�n usada por los editores hex.
Bueno, dentro de 123C esta el valor 0x14F0, pero el MD que estamos analizando dice la direcci�n de la direcci�n indexada, por lo que el 0x14F0 pasa a ser una direcci�n. La buscamos y contiene el valor 0x15C1. Este es el valor que se le asigna al acumulador.<br><br>

EA=Mem2[$1238+X], y si dijimos que X = 4hex => EA = Mem2[$123C]


<br><br><br><b>Absolute Indirect Long (Absoluto Indirecto Largo)</b><br><br>
<b>EA= Mem3[dir]</b><br><br>

El concepto es simple: dentro de una direcci�n de 2 byte hay otra direcci�n de 3 bytes.<br><br>

Ejemplo1: <br>
LDA [$123A] ; En la direcci�n 123A hay otra direcci�n de 3 bytes (long) y esa direcci�n contiene el valor de 1/2 bytes que se cargar� en A.<br>
Ejemplo grafico, si tenemos en memoria:
<br><img src="img/cap32.PNG"><br>
 
En 123A hay un direcci�n de 3 bytes, $8014F0, al ir hacia ella y examinamos el valor que contiene es 0x15C1, este se le asigna a A. <br><br>

EA= Mem3[$123A]


<br><br><br><b>Direct Page (Pagina Directa)</b><br><br>
<b>EA = dir</b><br><br>

Este modo es igual al Absoluto pero aqu� la direcci�n es de 1 byte siempre.<br><br>

Ejemplo1:<br>
LDA $2C	; se carga en A lo que esta en la direcci�n 2C.<br><br>

EA=$2C


<br><br><br><b>Direct Page Indexed, X/Y</b><br><br>
<b>EA=dir + X/Y</b><br><br>

Dentro de la direcci�n de 1 byte + �ndice X/Y hay un valor.<br><br>

Ejemplo1:<br>
LDA $3B,X ;Se carga en A  lo que hay en la direcci�n 3B + X.<br><br>

EA=$3B+X


<br><br><br><b>Direct Page Indirect Long</b><br><br>
<b>EA=Mem3[dir]</b><br><br>

Aqu� dentro de una direcci�n de 1 byte hay otra direcci�n de 3 bytes.<br><br>

Ejemplo:<br>
LDA [$12] ; dentro de la direcci�n 12 hay una direcci�n 123456 y dentro de esa direcci�n esta el valor que se asigna a A.<br><br>

EA=Mem3[$12]

<br><br><br><b>Direct Page Indirect Long Indexed, X/Y</b><br><br>
<b>EA=dir -> Direct Page
<br>EA=Mem3[dir]   ->Indirect Long
<br>EA=Mem3[dir] +X   -> indexed X
</b><br><br>

Dentro de la direcci�n de 1 byte hay una direcci�n de 3 bytes, le sumamos X/Y a esa direcci�n y nos da la direcci�n final, la Direcci�n Efectiva.<br><br>

Ejemplo:<br>
LDA [$CC],X<br><br>

EA = Mem3[$CC] + X


<br><br><br><b>Program Counter Relative</b><br><br>
<b>EA = dir</b><br><br>
Este modo de direccionamiento se usa en los SALTOS (recuerden capitulo anterior) pero de direcciones de 1 byte, estos saltos pueden ser:<br><br>

BCC - branch if carry clear (Carry=0)<br>
BCS - branch if carry set (Carry=1)<br>
BEQ - branch if equal<br>
BMI - branch if minus<br>
BNE - branch if not equal<br>
BPL - branch if plus<br>
BRA - branch always<br>
BVC - branch if overflow clear<br>
BVS - branch if overflow set<br><br>

Ejemplo1<br>
BCC $E5	; salta a la direcci�n indicada dentro de $E5 si el Flag <b>Carry es 0</b>. Para saber como operan los dem�s Flag, ver cap�tulo 1.<br><br>

EA = $E5


<br><br><br><b>Program Counter Relative Long</b><br><br>
<b>EA = dir</b><br><br>

Igual que antes, pero ahora las direcciones son de 2 bytes.<br><br>

Ejemplo:<br>
BRA $123C ;salta a $123C siempre, es decir sin condiciones (no le importa el carry, ni ning�n otro Flag).

<br><br><br><b>Stack Relative</b><br><br>
<b>EA= dir 1/2 byte ->contenido Stack + direcci�n de 1 bytes.</b><br><br>

Este MD est� basado en operaciones que se hacen a la <b>Pila o Stack</b>, que es un registro (ocupa por lo tanto un �rea en la memoria) y que se usa para almacenar datos de los registros de la CPU de la Snes como A, X o Y que NO DEBEN perderse durante la ejecuci�n de una rutina determinada.<br> 

En una Pila los datos se &#8220;apilan&#8221;. Para meter (copiar) un valor del acumulador a la Pila se ocupa la instrucci�n <b>PHA</b>, y para sacar un valor de la Pila y dejarlo en el acumulador se usa <b>PLA</b>. La imagen muestra como son estos simples procesos:

<br><img src="img/cap33.PNG"><br>

Como se fijaron la Pila la ASUMIMOS por ahora de tama�o 1 byte. Ahora que saben m�s o menos como trabaja una pila.<br><br>

Ejemplo:<br>
LDA $01,S ; El contenido del Tope del Stack ($01 indica la primera posici�n, un $02 la segunda, etc&#8230;) forma la Direcci�n Efectiva donde esta contenido el dato que se asignara a A.<br><br>

EA= $01


<br><br><br><b>Stack Relative Indirect Indexed X/Y</b><br><br>
<b>EA=Mem2[$01,S] + X</b> -> Lo que se hace es ir sumar la direcci�n de 1 byte dada en el operando al contenido del Stack, luego buscamos esa direcci�n en memoria y el contenido es otra direcci�n que sumada a X nos da la direcci�n Efectiva.<br><br>

Ejemplo:
LDA ($01,S),X	; si el stack tiene el valor 0x05 en el tope ($01 indica TOPE) y X tiene 0x2, entonces se pasa a A lo que esta en el Tope de la pila (0x05)+ X(0x02) = 0x07=>buscamos valor que esta dentro de direcci�n $07.


<br><br><br><b>Acumulator (Accumulador)</b><br><br>
<b>EA = no tiene</b><br><br>

Simplemente cualquier instrucci�n que opere directamente el acumulador estar�a dentro del Modo de D. Acumulador.<br><br>

Ejemplo:<br>
ASL A ;multiplica A por 2<br><br>


<br><br><hr style="width: 100%; height: 2px;"><br>
<b>Gusto a Poco</b>

<br><br>Esta vez les pongo los ejercicios. Las respuestas est�n al final.

<br><br>1. Dado el MD escribe un ejemplo de:<br>

<br>a) absolute indirect indexed X
<br>b) absolute indexed indirect
<br>c) direct page indexed indirect long stack relative 
<br>d) absolute long indirect indexed indirect stack relative
<br>e) stack relative indirect indexed,X indirect long indexed,Y (Asume que el stack relative es de 1 byte)
<br>f) direct page indirect indirect long indexed Y indirect indexed X
<br>g) direct page indexed indirect
<br>h) stack relative indirect long indexed X indirect indexed Y indirect long

<br><br>2. Responde: <br>

<br>a) En una traza obtenida con <b>Snes9x Debugger</b> del juego Mystical Ninja se tiene esta linea:<br>
&nbsp;&nbsp;&nbsp;&nbsp;$00/A08A 91 03       STA ($03),y[$7E:2155]   A:00FF X:0155 Y:0155 P:envMxdIzc
<br>&nbsp;&nbsp;&nbsp;&nbsp;Explicar que hace e indicar el MD.
<br>b) �Cual es la diferencia entre  Indirect address de una Indirect long address?
<br>c) �Qu� hace: [$123456],Y  ? Explique.
<br>d) Defina brevemente Modo de Direccionamiento. 

<br><br>3. Escribe el c�digo de EA de las siguientes MD:<br>
&nbsp;&nbsp;&nbsp;&nbsp;(Recuerda: Mem[..] para el Indirect y usa +X o +Y para Indexed)<br>

<br>a) [$123456],Y: 
<br>b) direct page indirect indexed X indirect long indexed Y
<br>c) Abolute indirect
<br>d) absolute indexed x indirect long indirect stack relative

<br><br>4. Dado el c�digo EA escribe el MD:<br>

<br>a) EA = Mem2[$12+X]+Y
<br>b) EA = Mem3[Mem2[$12 + Y] + Y] + S
<br>c) EA = Mem2[$12+S]+X+Y

<br><br>5. Programaci�n b�sica en ASM. Realiza rutinas dado el seudo-c�digo (Si no pueden no se preocupen ya que el capitulo siguiente ser� de este puro tema, pero deber�an poder hacerlo usando este capitulo y el anterior, y obviamente&#8230;sus neuronas.<br>

<br>a) X = A &#8211; 3
<br>b) X = Y + 2
<br>c) A = A &#8211; 5 + 1
<br>d) Si carry=0 salta a lo que haya en $12, sino, retorna y sale de la actual subrutina


<br><br><br><b>Respuestas</b>

<br><br>1.<br>
<br>a) ($421C),X

<br>b) ($02FC,X)

<br>c) direcci�n de 1 byte: $12<br>
&nbsp;&nbsp;&nbsp;&nbsp;indexed X : $12,X (mas adelante sabr�s que X es mejor que Y)<br>
&nbsp;&nbsp;&nbsp;&nbsp;y luego usa [] en vez de ( ):   [$12,x],S

<br>d) (([$1234]),x),s

<br>e) ($01,S),X
<br>&nbsp;&nbsp;&nbsp;&nbsp;[($01,S),X],Y

<br>f) ($12)
<br>&nbsp;&nbsp;&nbsp;&nbsp;[($12)],Y
<br>&nbsp;&nbsp;&nbsp;&nbsp;([($08)],y),x

<br>g) ($08,x)

<br>h) [([$01,S],X), Y]

<br><br><br>2.<br>
<br>a) $00/A08A: direcci�n de memoria
<br>&nbsp;&nbsp;&nbsp;&nbsp;STA ($03),y: Modo de Direccionamiento es Direct Page Indirect Indexed,Y. Explicaci�n: Dentro de la direcci�n de 1 byte ($03) hay una direcci�n de 3 bytes, le sumamos Y=0x155 (ver traza Y:0155) a esa direcci�n y nos da la direcci�n final o la Direcci�n Efectiva $7E:2155. 
<br>&nbsp;&nbsp;&nbsp;&nbsp;[$7E:2155] es la direcci�n efectiva o final donde se guardar� el dato, el Log lo muestra para que uno no se pierda.
<br>&nbsp;&nbsp;&nbsp;&nbsp;P:envMxdIzc: nos dice que Flags e,n,v,x,d,z,c est�n en 0, y M,I en 1.

<br><br>b) Indirect long address es una indirect address que lee un puntero de 3 bytes en vez de 2 bytes.
<br><br>c) Va a $123455, lee 3 bytes y a�ade Y a ese valor, y luego toma la informaci�n de esa direcci�n.
<br><br>d) Es una manera para describir una direcci�n que nos ayuda a determinar una direcci�n final o direcci�n efectiva.

<br><br><br>3.<br>
<br>a) EA = Mem3[$123456] + Y
<br>b) EA = Mem3[Mem2[$12]+X]+Y
<br>c) EA = Mem2[$1234]
<br>d) EA = Mem2[Mem3[$1234+X]]+S

<br><br><br>4.<br>
<br>a) Direct page indexed x indirect indexed y
<br>b) DP indexed Y indirect indexed Y indirect long stack relative
<br>c) DP stack relative indirect indexed x indexed Y

<br><br><br>5.<br>
<br>a) X = A &#8211; 3   
<br>SEC ; deja el carry en 1, siempre debe ir antes de SBC
<br>SBC #$03 ; A=A-3 + carry (vale 1 por SEC) &#8211; 1 => A = A - 3
<br>TAX ; X vale lo de  A

<br><br>b) X = Y + 2
<br>INY
<br>INY   
<br>TYX ; x vale lo que vale Y

<br><br>c) A = A &#8211; 5 + 1
<br>SEC ;(pr�ximo cap�tulo mas del uso de SEC)
<br>SBC #$04

<br><br>d) Si carry=0 salta a lo que haya en $12, sino, retorna y sale de la actual subrutina
<br>BCC $12
<br>RTS ; Return from Subroutine 


<br><br>
<hr style="width: 100%; height: 2px;">

<a href="../doc_traduc.php">Volver</a>
<br>
<font color="#FFFFFF">
<?php 
include '../disq.php';
?>
</font>
<center><?php include ('../pie.php'); ?></center>
</small>
</body>
</html>