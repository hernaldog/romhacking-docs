<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>Ys III Tutorial Gr&aacute;fico de Edici&oacute;n de Tiles &quot;Gold&quot;</title>
</head>


<body>
<span style="font-family: Verdana;">
<small>
<a href="../../doc_traduc.php">Volver</a>
</small>

<hr style="width: 100%; height: 2px;">

<p>
<font size="4"><span style="font-weight: 700">Hacking de TileMaps de </span>
</font><big>
<span style="font-weight: bold;">Ys'III para la edici&oacute;n de Tiles &quot;Gold&quot;</span></big><small><br style="font-weight: bold;"> 
Por Dark-N: <?php include ('../../mailsolo.php'); ?>
<br><a href="https://darknromhacking.com/">https://darknromhacking.com</a>
<br>Update: 04-03-2021<br>
</small></p>

<hr style="width: 100%; height: 2px;">
<small>1. Esto es lo que se modificar&aacute;. Abrir rom con <b>Geider's Snes9X Debugger</b> y 
dejar que le saque el header. Luego abrir la rom con el <b>ZSNES</b> y sacar una 
salvada .ZST.<br>

<img border="0" src="1_Snes9X_Debugger_Que_vamos_a_modificar_(dejar_que_Snes9X_saque_el_header_de_la_ROM).PNG" width="538" height="454"><br>
<br>
2. Con el Geiger's Snes9x Debugger marcar &quot;What's Used&quot; para ver detalles del BG 
usado y del Base Address (BA).<br>
<img border="0" src="2_Snes9x_Debugger_What_is_used_usa_BG2_y_BA=3C00.PNG" width="754" height="598"><br>
<br>
3. Abrir el ZST con el TLP e ir a $24C13. Dejar en modo Game Boy (2bpp).<br>
<img border="0" src="3_TLP_ZST_Goto_24C13.PNG" width="336" height="147"><br>
<br>
4. Veremos las tiles &quot;GLD&quot; que forman con la &quot;O&quot; de m&aacute;s atr&aacute;s la palabra &quot;GOLD&quot;.<br>
<img border="0" src="4_TLP_ZST_24C13_View_format_Game_Boy.PNG" width="520" height="506"><br>
<br>
5. Abrir la Rom con el TLP e ir a $F1A8, tambi&eacute;n se ver&aacute;n la Tiles.<br>
<img border="0" src="5_TLP_ROM_F1A8_View_format_Game_Boy.PNG" width="540" height="440"><br>
<br>
6. Marcamos la O con un punto en el centro.<br>
<img border="0" src="6_TLP_edicion_Letra_O.PNG" width="709" height="156"><br>
<br>
7. Jugamos y nos damos cuenta que la O aparece 3 veces en la pantalla. Esto es 
gracias al &quot;Tile Map&quot; (Mapa de Tiles) de la SNES.<br>
<img border="0" src="7_Snes9X_rom_usa_Tile_Map.PNG" width="520" height="117"><br>
<br>
8. Con el <b>vSNES 2.52</b> abrimos el LZT. Escogemos el bot&oacute;n que dice &quot;EsceneViewer&quot;.<br>
<img border="0" src="8_VSNES_SceneViewer.PNG" width="663" height="83"><br>
<br>9. Adentro nos fijamos los valores X/Y de la tile &quot;G&quot; que muestra.<br><img border="0" src="9_VSNES_posicion_X_Y_de_Tile_G.PNG" width="676" height="564"><br>
<br>
10. Con esos valores de G sacamos la direcci&oacute;n de G en el Tile Map de VRAM 
gracias a una f&oacute;rmula.<br>
<img border="0" src="10_Formula_calculo_direccion_Tile_G_en_Tile_Map_VRAM.PNG" width="666" height="139"><br>
<br>
11. Abrimos el <b>Lunar Adress</b> y ponemos all&iacute; la direcci&oacute;n obtenida (una 
direcci&oacute;n VRAM SNES) para pasarla a una ZNES address.
<br>
<img border="0" src="11_Lunar_SNES_VRAM_3F74_es_ZST_028AFB.PNG" width="297" height="368"><br>
<br>
12. Abrimos el ZST con el HexWorkshop u otro editor Hex y vamos a la direcci&oacute;n 
obtenida.<br>
<img border="0" src="12_HexWorkshop_Goto_028AFB.PNG" width="377" height="184"><br>
<br>
13. Vemos 2 bytes 1520 que es la tile &quot;G&quot;. La 0F20 es la &quot;0&quot;. Estamos dentro 
del tile Map.<br>
<img border="0" src="13_HexWorkshop_bytes1520=bytes_tile_G_en_Tile_Map.PNG" width="581" height="92"><br>
<br>
14. Editamos y ponemos 0F20 donde hab&iacute;a un 1520. Luego jugamos con el ZNES y 
cargamos (F4) inmediatamente y se ver&aacute; OOLD.<br>
<img border="0" src="14_HexWorkshop_cambio_bytes_1520_de_Tile_G_por_0F20_de_0.PNG" width="430" height="213"><br>
<br>
15. ahora si lo dejamos 0020 nos muestra &quot; &quot;OLD.<br>
<img border="0" src="15_HexWorkshop_dejar_tile_G_1520_por_una_tile_vacia_con_0020.PNG" width="504" height="218"><br>
<br>
16. Miramos el ZST con el TLP y a partir de la tile vac&iacute;a fabricamos la tabla 
completa de bytes que forman el Tile Map.<br>
<img border="0" src="16_TLP_hacer_tabla_de_todos_bytes_Tile_Map.PNG" width="494" height="218"><br>
<br>
17. Escribimos &quot;ORO&quot; usando 0F20 1320 0F20 ya que en la tabla anterior obtuvimos 
los bytes de cada letra.<br>
<img border="0" src="17_HexWorkshop_de_GOLD_a_ORO_modif_de_tile_map.PNG" width="574" height="263"><br>
<br>
18. Jugamos con el ZSNES y cargamos de inmediato y veremos el cambio.<br><img border="0" src="18_ZSNES_cargar_salvada_modificada_y_ver_ORO.PNG" width="450" height="138"><br>
<br>
19. Con el HexWorkshop buscamos el la ROM las tiles GOLD con los bytes 
respectivos a cada letra. Y lo editamos por ORO como lo hicimos en el ZST.<br>
<img border="0" src="19_HexWorkshop_buscar_bytes_GOLD_de_Tile_map_en_ROM.PNG" width="543" height="686"><br>
20. Vemos con el emulador como cambio ahora definitivamente a ORO. Faltar&iacute;a 
cambiar la palabra RING.<br>
<img border="0" src="20_Snes9x_ORO_listo.PNG" width="538" height="454"><br>
<br>
21. Las tiles que antes eran GLD ahora las modificamos con el TLP por ANILLOS. 
En el Tile Map hay que decirle que lo que era RING ahora ser&aacute; GOLD. Para eso 
vemos los bytes de GOLD para cambiarlos por los de RING.<br>
<img border="0" src="21_TLP_editar_ROM_Tiles_GLD_por_ANILLO.PNG" width="539" height="207"><br>
<br>
22. Con el HexWorkShop buscamos en la ROM los bytes de GOLD y los cambiamos por 
los de RING.<br>
<img border="0" src="22_HexWorkshop_cambio_bytes_Ring_Anillo.PNG" width="408" height="619"><br>
23. Finalmente vemos como nos queda todo nuestro hacking.<br>
<img border="0" src="23_Snes9X_ANILLOS_ORO_en_ROM_Finalizada_edicion.PNG" width="574" height="492">

<hr style="width: 100%; height: 2px;">

<a href="../../doc_traduc.php">Volver</a>
<br>
<font color="#FFFFFF">
<?php 
include '../../disq.php';
?>
</font>
<center><?php include ('../../pie.php'); ?></center>
</small>
</body>
</html>
