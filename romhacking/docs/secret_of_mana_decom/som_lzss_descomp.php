<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Descompresion intro Secret of Mana-Snes (LZSS)</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<div align="left"> 
  <p align="left"><a href="../../doc_traduc.php">
  <font size="2" face="Verdana">volver</font></a></p>
  <p align="center"><strong><br>
    - LZSS Decompress -</strong><br>
    <strong>_____________________________________________________________________</strong></p>
</div>
<p align="center"><strong><font color="#000099" size="+2">Descompresi&oacute;n 
  del Intro del Secret of Mana</font></strong></p>
<p align="center"><strong><font color="#0000CC" size="+1">PARTE 1</font></strong></p>
<p align="center"><strong>- SNES -<br>
  V 1.0</strong></p>
<p align="center">
Por Dark-N: <?php include ('../../mailsolo.php'); ?>
</p>
<p align="center">
<span style="font-family: Verdana;">
<small>
<a href="http://darknromhacking.com/">http://darknromhacking.com</a></small></span><strong><font color="#006699"><br>
  </font></strong><strong>____________________________________________________________________</strong></p>
<p align="center"><strong>Nota: Debes tener la versi&oacute;n en Ingles del juego, 
  ya que con esta versi&oacute;n hice la gu&iacute;a.</strong></p>
<p align="left"><font color="#000099"><strong><img src="../../imag/index_bullet.gif" width="20" height="15"><a href="#Introducci�n">Introducci&oacute;n</a> 
  <br>
  <img src="../../imag/index_bullet.gif" width="20" height="15"><a href="#Antes de Empezar">Antes de Empezar
</a> 
  <br>
  <img src="../../imag/index_bullet.gif" width="20" height="15"><a href="#La Intro">La Intro</a><br>
  <img src="../../imag/index_bullet.gif" width="20" height="15"><a href="#Punteros LZSS. Regla de los 4 pasos">Punteros LZSS. 
  Regla de los 4 pasos</a><br>
  <img src="../../imag/index_bullet.gif" width="20" height="15"><a href="#Analizando la compresi�n de la ROM">Analizando la 
  compresi&oacute;n de la ROM</a><br>
  <img src="../../imag/index_bullet.gif" width="20" height="15"><a href="#Una vez descomprimida, �Qu� sigue">Una vez descomprimida, 
  &iquest;Qu&eacute; sigue?</a><br>
  <img src="../../imag/index_bullet.gif" width="20" height="15"><a href="#Fin de primera parte">Fin de primera 
  parte</a></strong></font></p>
<p>_________________________________________________________________________________________________</p>
<p><strong><font color="#000099" size="+1"><a name="Introducci�n">Introducci&oacute;n</a></font></strong></p>
<p>Esta gu&iacute;a la hice con intenciones de explicar como descomprimir la Intro 
  del Secreto of Mana, que est&aacute; comprimida con el algoritmo LZSS, muy parecido 
  a LZ77. <strong>Magno</strong> me ha orientado y explicado muchas cosas que 
  aqu&iacute; detallo (<strong>&iexcl;muchas gracias</strong>!), ya que el lo 
  tradujo al 100% ;)</p>
<p><font color="#000099" size="+1"><strong><br>
  <a name="Antes de Empezar">Antes de Empezar</a></strong></font></p>
<p>Primero hay que decir que trabajaremos con la intro que se muestra antes de 
  jugar, es decir, en la que sale si no tocas ning&uacute;n bot&oacute;n del control 
  pad y habla de cuando el man&aacute; estaba desapareciendo. <br>
  La intro est&aacute; en <strong>ASCII</strong> pero comprimida, es decir que 
  debes verla sin usar B&uacute;squeda relativa. La compresi&oacute;n LZSS es 
  progresiva, es decir, que si bien los primeros caracteres los puedes ver bien, 
  conforme empiecen a haber letras repetidas, &eacute;stas ser&aacute;n sustituidas 
  por punteros LZSS a donde ya aparecieron anteriormente, con lo que dejar&aacute;s 
  de verlas. </p>
<p>En la versi&oacute;n en espa&ntilde;ol de Magno, al ver el intro en un editor 
  (en la versi&oacute;n en espa&ntilde;ol y en la US, el offset es de la intro 
  es <strong>0x7BC78</strong>) se ver&aacute;:<br>
  <img src="foto1.JPG" width="565" height="424"><br>
  Si te fijas bien dice:&quot;...oscuridadXXXcierne...&quot; en donde est&aacute;n 
  las tres 'X' deber&iacute;a decir &quot; se &quot;, para que el trozo completo 
  quede &quot;...oscuridad se cierne...&quot;, pero como &quot; se &quot; ha aparecido 
  ya anteriormente donde dice &quot;mientras se debilita&quot; justo al principio, 
  pues sustituimos esas 4 letras por un puntero de 3 bytes a esa zona; esta es 
  la base del LZSS. </p>
<p>Como ver&aacute;s, es poco efectiva para trozos peque&ntilde;os de texto, pero 
  efectiva para datos repetitivos. Adem&aacute;s, si cambias alguna letra de las 
  que no est&aacute;n comprimidas (es decir, las que ves como texto ASCII), puedes 
  que estropees toda la estructura de la compresi&oacute;n, puesto que de los 
  tres bytes que forman el puntero LZSS, uno de ellos dice cu&aacute;ntos bytes 
  vienen a continuaci&oacute;n en texto sin comprimir, por lo que si a&ntilde;ades 
  o quitas uno a los comprimidos, puedes desalinear el algoritmo o la partida 
  de la rom. </p>
<p>Tambi&eacute;n hay problemas si sustituyes al azar alguno de los 3 bytes del 
  puntero por alg&uacute;n car&aacute;cter especial que quiera ponerle, ya que 
  uno de esos bytes guarda informaci&oacute;n si lo que sigue a continuaci&oacute;n, 
  si lo que sigue es un puntero o solo texto. Esto pasar&iacute;a si por ejemplo 
  pusieras en una tabla la letra &ntilde; como hex 80 ya que el hex 80 es muy 
  usado en este puntero, al menos en esta rom.<br>
  <br>
  En cuanto al texto del juego, no tiene ninguna compresi&oacute;n, ni la fuente 
  tampoco; lo m&aacute;s dif&iacute;cil que tiene el juego es, sin duda alguna, 
  tanto la intro comprimida en LZSS.</p>
<p><strong><br>
  <font color="#000099" size="+1"><a name="La Intro">La Intro</a></font></strong></p>
<p>La Intro original en ingles, el texto que sale es este:</p>
<p>darkness sweeps the<br>
  troubled land, as mana's<br>
  power fades...<br>
  people await a hero who<br>
  will wield the sword...<br>
  excalibur, herald, gigas...<br>
  the blade had many<br>
  names, for it has been <br>
  celebrated in myths and<br>
  leyends throughout time. <br>
  but all of these speak to<br>
  just one weapon:<br>
  the sword of mana.</p>
<p><br>
  Ahora si vamos al editor, vamos al offset 0x7BC78 (como se dijo en los antecedentes) 
  veremos esta intro en completo ASCII, como se ve abajo:<br>
  <img src="foto2.JPG" width="637" height="207"> <br>
  Esta ser&aacute; la intro que trabajaremos en esta gu&iacute;a. </p>
<p>Ahora si le aplicamos le aplicamos la siguiente tabla:</p>
<p>61=a<br>
  62=b<br>
  63=c<br>
  64=d<br>
  65=e<br>
  66=f<br>
  67=g<br>
  68=h<br>
  69=i<br>
  6A=j<br>
  6B=k<br>
  6C=l<br>
  6D=m<br>
  6E=n<br>
  6F=o<br>
  70=p<br>
  71=q<br>
  72=r<br>
  73=s<br>
  74=t<br>
  75=u<br>
  76=v<br>
  77=w<br>
  78=x<br>
  79=y<br>
  7A=z<br>
  20= <br>
  5E='<br>
  7B=.<br>
  7C=,<br>
  5F=:<br>
  (A0E0Eh)intro_texto<br>
  (92B19h)texto_inicio<br>
  (92A25h)texto_jugando<br>
  (7BC78h)intro principio</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Es 
  decir, nos queda esto:<br>
  <img src="foto3.JPG" width="637" height="220"></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p><span lang=EN-GB
style='mso-ansi-language:EN-GB'>darkness sweeps the<span style='color:blue'>&lt;80&gt;&lt;27&gt;&lt;18&gt;</span>&lt;04&gt;<span
style='color:blue'><o:p></o:p></span></span></p>
<p class=MsoNormal style='margin-left:27.0pt'><span lang=EN-GB
style='mso-ansi-language:EN-GB'>troubled land,`as mana's<span style='color:
blue'>&lt;80&gt;&lt;1b&gt;&lt;0e&gt;&lt;</span>09&gt;<o:p></o:p></span></p>
<p class=MsoNormal style='margin-left:27.0pt'><span lang=EN-GB
style='mso-ansi-language:EN-GB'>power fades...<span style='color:blue'>&lt;82&gt;&lt;2D&gt;&lt;16&gt;<o:p></o:p></span></span></p>
<p class=MsoNormal style='margin-left:27.0pt'><span lang=EN-GB
style='mso-ansi-language:EN-GB'>people await a hero who<span style='color:blue'>&lt;82&gt;&lt;1a&gt;&lt;09&gt;</span><o:p></o:p></span></p>
<p class=MsoNormal style='margin-left:27.0pt'><span lang=EN-GB
style='mso-ansi-language:EN-GB'>will wield<span style='color:blue'>&lt;82&gt;&lt;5A&gt;&lt;80&gt;&lt;65&gt;&lt;02&gt;</span>ord<span
style='color:blue'>&lt;86&gt;&lt;35&gt;&lt;0b&gt;</span>&lt;02&gt;<o:p></o:p></span></p>
<p class=MsoNormal style='margin-left:27.0pt'>excalibur,`<span
style='color:blue'>&lt;80&gt;&lt;31&gt;&lt;01&gt;</span>al&lt;80&gt;g&lt;03&gt;giga&lt;88&gt;T&lt;00&gt;&lt;05&gt;</p>
<p class=MsoNormal style='margin-left:27.0pt'>&lt;82&gt;.&lt;01&gt;bl&lt;80&gt;e&lt;01&gt; 
  h&lt;80&gt;~&lt;02&gt;had&lt;82&gt;&lt;82&gt;&lt;00&gt;y&lt;82&gt;&lt;19&gt;&lt;0A&gt;</p>
<p class=MsoNormal style='margin-left:27.0pt'><span lang=EN-GB
style='mso-ansi-language:EN-GB'>names,`for &lt;80&gt;o&lt;82&gt;&lt;1d&gt;&lt;03&gt;been&lt;82&gt;m&lt;07&gt;<o:p></o:p></span></p>
<p class=MsoNormal style='margin-left:27.0pt'><span lang=EN-GB
style='mso-ansi-language:EN-GB'>celebrat&lt;80&gt;&lt;b8&gt;&lt;08&gt;in myths 
  &lt;80&gt;&lt;c0&gt;&lt;82&gt;&lt;1a&gt;&lt;05&gt;<o:p></o:p></span></p>
<p class=MsoNormal style='margin-left:27.0pt'><span lang=EN-GB
style='mso-ansi-language:EN-GB'>legend&lt;82&gt;&lt;e0&gt;&lt;80&gt;&lt;da&gt;&lt;09&gt;ghout 
  time&lt;82&gt;j&lt;01&gt;&lt;03&gt;<o:p></o:p></span></p>
<p class=MsoNormal style='margin-left:27.0pt'><span lang=EN-GB
style='mso-ansi-language:EN-GB'>b&lt;80&gt;&lt;0c&gt;&lt;00&gt;a&lt;80&gt;&lt;a7&gt;&lt;01&gt;of&lt;82&gt;&lt;a4&gt;&lt;00&gt;s&lt;80&gt;&lt;a6&gt;&lt;05&gt;peak 
  t&lt;82&gt;&lt;c1&gt;&lt;10&gt;&lt;08&gt;<o:p></o:p></span></p>
<p class=MsoNormal style='margin-left:27.0pt'><span lang=EN-GB
style='mso-ansi-language:EN-GB'>just one weapon:&lt;80&gt;&lt;13&gt;&lt;00&gt;&lt;07&gt;<o:p></o:p></span></p>
<p class=MsoNormal style='margin-left:27.0pt'><span lang=EN-GB
style='mso-ansi-language:EN-GB'>&lt;8c&gt;&lt;ca&gt;&lt;82&gt;2&lt;83&gt;&lt;19&gt;&lt;82&gt;F&lt;8a&gt;&lt;01&gt;&lt;18&gt;&lt;04&gt;<o:p></o:p></span></p>
<p class=MsoNormal style='margin-left:27.0pt'><span lang=EN-GB
style='mso-ansi-language:EN-GB'>P[\\]|[~&lt;7f&gt;`QRSTUV`co{|`ltd&lt;82&gt;$&lt;00&gt;&lt;06&gt;&lt;82&gt;g&lt;07&gt;rights 
  <o:p></o:p></span></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-GB style='mso-ansi-language:EN-GB'><o:p>&nbsp;</o:p></span>Quiz�s no se 
  entienda a simple vista, pero el texto se puede seguir, hasta que se empieza 
  a comprimir.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>�Por 
  qu� esta de color <span style='color:blue'>azul</span> algunas partes? Estos 
  son los <b style='mso-bidi-font-weight:normal'>punteros LZSS</b> que debemos 
  analizar antes de continuar.</p>
<p>&nbsp;</p>
<p><font color="#000099" size="+1"><strong>
<a name="Punteros LZSS. Regla de los 4 pasos">Punteros LZSS. Regla de los 4 
pasos</a></strong></font></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Un 
  puntero LZSS es un grupo de <b style='mso-bidi-font-weight:normal'>3 bytes seguidos</b> 
  (es decir de 24 Bits que enumeraremos del 0 al 23, 0 es el bit de mas a la izquierda 
  y 23 el de mas a la derecha) que hacen referencia a otra parte de la ROM, puede 
  ser un texto, un c�digo (bajar una l�nea, l�nea en blanco, etc.), u otro puntero 
  que a su vez nos puede llevar a otro lugar. </p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Hay<b
style='mso-bidi-font-weight:normal'> excepciones</b>, donde un puntero <b
style='mso-bidi-font-weight:normal'>puede ser de 2 bytes</b>, <b
style='mso-bidi-font-weight:normal'>cuando hay 2 punteros seguidos</b>, uno tras 
  otro. El primero es solo de 2 bytes y el segundo de 3. Debido a que el 3er byte 
  de un puntero indica la distancia al pr�ximo puntero, y como es 0, entonces 
  se omite. Ya lo ver�n con m�s detalle en esta secci�n.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Como 
  Trabajar un puntero, supongamos que tenemos 3 bytes en el texto de la intro, 
  como por ejemplo 70151C �pordr�a ser un puntero LZSS? Si lo pasamos a binario 
  podr�amos verlo mejor:</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>70 
  = 11100000 b <span style='mso-spacerun:yes'>��</span>-&gt;el bit mas significativo 
  es el 1 <b style='mso-bidi-font-weight:normal'>mas a la izquierda</b> que haya, 
  en este caso seria el 1.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>15 
  = 00010101 b</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>1C 
  = 00011100 b</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>quedan 
  finalmente en binario como: <b style='mso-bidi-font-weight:normal'>1110000 00010101 
  00011100</b> en total, 24 bits. Todav�a no sabemos si es un puntero LZ o un 
  simple texto.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>LZSS 
  nos dice ciertas &#8220;reglas&#8221; para identificarlos y trabajar un puntero del resto 
  del texto, le llamaremos la <b style='mso-bidi-font-weight:normal'>Regla de 
  los 4 Pasos</b>, estas son:<br>
  <o:p></o:p></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>1)- 
  El <b style='mso-bidi-font-weight:normal'>bit m�s significativo (MSB)</b> o 
  <b
style='mso-bidi-font-weight:normal'>primer bit</b> est� activo (a &quot;1&quot;) 
  cuando lo que viene a continuaci�n son 23 bits de puntero, no de texto. </p>

<p><img src="foto5.JPG" width="605" height="85"><br>
  Aqu� surge de inmediato un problema ya que si una letra de TEXTO es por ejemplo, 
  el hexadecimal 70, que convertido a binario ser�a <b style='mso-bidi-font-weight:
normal'>1</b>1100000, el primer bit es 1 lo que LZSS tomar�a como un puntero y 
  no como texto.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Entonces 
  necesitamos otro mecanismo para detectar d�nde empieza un puntero y as� distinguirlo 
  del texto no referenciado (es decir, aqu�l que se obtendr� repetido de alguna 
  parte anterior del texto). </p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>2)- 
  Este mecanismo lo aporta el <b style='mso-bidi-font-weight:normal'>tercer byte</b> 
  del puntero (desde el bit 16 al 23), que nos dice cu�ntos bytes vienen a continuaci�n, 
  <span style='mso-spacerun:yes'>�</span>de TEXTO en que NO aparecer� ning�n puntero. 
</p>
<p> <img src="foto6.JPG" width="605" height="59"><br>
</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Esto 
  tiene la limitaci�n de que <b style='mso-bidi-font-weight:normal'>s�lo puedes 
  almacenar aqu� 255 bytes</b> de texto no referenciado (sin punteros). Esto es 
  ya que si del 16 al 23 hay 1 byte de informaci�n, y un byte puede ir desde 00 
  a FF. </p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Por 
  ejemplo si esta parte del puntero seria 00 quiere decir que despu�s del puntero 
  hay 0 bytes de texto puro.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Si 
  seria FF, quiere decir que hay 255 bytes de texto puro. Es decir que despu�s 
  de 255 bytes limpios, debe seguir obligatoriamente un nuevo puntero. </p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>3)- 
  Nos falta por saber qu� son los bits de en medio, es decir, los bits 1 al 15. 
  Lo Dividimos en 2 grupos: del bit 1 al 6 y del 7 al 15 ya que nos distinta informaci�n.</p>
<img src="foto7.JPG" width="610" height="104"> 
<p>Los 4 primeros bits de estos 15 indicaban (bit 1 al bit 6) <b style='mso-bidi-font-weight:
normal'>cu�ntas letras tienes que copiar</b> a partir de donde diga el puntero. 
  Si tenemos 4 bits entonces podemos tener 2<sup>6</sup> = 64 valores m�ximos 
  posibles, es decir, copiar 64 letras consecutivas de un texto que esta atr�s 
  del puntero. Pero como m�nimo debes tener estos 6 bits con un 03 ya que eso 
  nos dice que se copiaran 3 letras que equivalen a 24 bits. En total puedes copiar 
  hasta 64+3 letras. </p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Seria 
  absurdo usar un puntero LZ si no coparas mas de 3 letras ya que estas gastando 
  24 bits en el puntero para poner all� un texto mayor �no? Si quieres copiar 
  un texto de 3 bytes (3 letras) no ganas ni pierdes espacio ya que el puntero 
  es de 3 bytes.<br>
</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>4)-El 
  resto de bits (bit 7 al bit 15) indicaban los bytes antes del puntero que nos 
  tenemos que desplazar para encontrar las letras repetidas. </p>
<p><img src="foto8.JPG" width="617" height="94"><br>
  Se puede desplazar desde 0 hasta <span style='mso-spacerun:yes'>�</span>512 
  posiciones hacia atr�s para encontrar una letra repetida. </p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p><o:p></o:p>Finalmente 
  todo se resume as�:<o:p></o:p></p>
<p><img src="foto9.JPG" width="683" height="47"> </p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-GB style='mso-ansi-language:EN-GB'><o:p></o:p></span><strong>X:<span style='mso-tab-count:1'>�</span></strong><span style='mso-tab-count:1'>������ 
  </span>Indicador de puntero. Si X = 0 =&gt; Lo que viene a continuaci�n es texto. 
  Si X 0=&gt;1=Lo que sigue es un puntero.<o:p></o:p></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'> <strong>Y:</strong><span
style='mso-tab-count:1'>������� </span>&#8216;Y&#8217; bytes repetidos a leer. El M�nimo que 
  se guarda en estos 6 bit es el valor 03h.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><strong>Z:<span style='mso-tab-count:1'>�</span></strong><span style='mso-tab-count:1'>������� 
  </span>&#8216;Z&#8217; posiciones hacia atr�s hemos de ir a leer los &#8216;Y&#8217; bytes. </p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><strong>W:<span
style='mso-tab-count:1'>�</span></strong><span
style='mso-tab-count:1'>����� </span>'w' bytes siguientes en los que no aparecer� 
  ning�n puntero.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><b
style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt'><o:p>&nbsp;</o:p></span></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><b
style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt'><o:p></o:p></span></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><b
style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt'><o:p><font color="#000099" size="+1">
<a name="Analizando la compresi�n de la ROM">Analizando 
  la compresi&oacute;n de la ROM</a></font></o:p></span></b></p>
<p>Tomamos de nuevo el texto que se ve en el editor, enumeramos las l&iacute;neas 
  para ir una a una analizando:</p>
<p>1. darkness sweeps the&lt;80&gt;&lt;27&gt;&lt;18&gt;&lt;04&gt;<br>
  2. troubled land,`as mana's&lt;80&gt;&lt;1b&gt;&lt;0e&gt;&lt;09&gt;<br>
  3. power fades...&lt;82&gt;&lt;2D&gt;&lt;16&gt;<br>
  4. people await a hero who&lt;82&gt;&lt;1a&gt;&lt;09&gt;<br>
  5. will wield&lt;82&gt;&lt;5A&gt;&lt;80&gt;&lt;65&gt;&lt;02&gt;ord&lt;86&gt;&lt;35&gt;&lt;0b&gt;&lt;02&gt;<br>
  6. excalibur,`&lt;80&gt;&lt;31&gt;&lt;01&gt;al&lt;80&gt;g&lt;03&gt;giga&lt;88&gt;T&lt;00&gt;&lt;05&gt;<br>
  7. &lt;82&gt;.&lt;01&gt;bl&lt;80&gt;e&lt;01&gt; h&lt;80&gt;~&lt;02&gt;had&lt;82&gt;&lt;82&gt;&lt;00&gt;y&lt;82&gt;&lt;19&gt;&lt;0A&gt;<br>
  8. names,`for &lt;80&gt;o&lt;82&gt;&lt;1d&gt;&lt;03&gt;been&lt;82&gt;m&lt;07&gt;<br>
  9. celebrat&lt;80&gt;&lt;b8&gt;&lt;08&gt;in myths &lt;80&gt;&lt;c0&gt;&lt;82&gt;&lt;1a&gt;&lt;05&gt;<br>
  10. legend&lt;82&gt;&lt;e0&gt;&lt;80&gt;&lt;da&gt;&lt;09&gt;ghout time&lt;82&gt;j&lt;01&gt;&lt;03&gt;<br>
  11. b&lt;80&gt;&lt;0c&gt;&lt;00&gt;a&lt;80&gt;&lt;a7&gt;&lt;01&gt;of&lt;82&gt;&lt;a4&gt;&lt;00&gt;s&lt;80&gt;&lt;a6&gt;&lt;05&gt;peak 
  t&lt;82&gt;&lt;c1&gt;&lt;10&gt;&lt;08&gt;<br>
  12. just one weapon:&lt;80&gt;&lt;13&gt;&lt;00&gt;&lt;07&gt;<br>
  13. &lt;8c&gt;&lt;ca&gt;&lt;82&gt;2&lt;83&gt;&lt;19&gt;&lt;82&gt;F&lt;8a&gt;&lt;01&gt;&lt;18&gt;&lt;04&gt;P[\\]|[~&lt;7f&gt;`QRSTUV`co{|`ltd&lt;82&gt;$&lt;00&gt;&lt;06&gt;&lt;82&gt;g&lt;07&gt;rights 
  <br>
</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Empecemos 
  a analizar CADA l�nea:</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p><b style='mso-bidi-font-weight:normal'>1. 
  </b><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB style='mso-ansi-language:
EN-GB'>darkness sweeps the&lt;80&gt;&lt;27&gt;&lt;18&gt;&lt;04&gt;<o:p></o:p></span></b></p>
<p class=MsoNormal><span lang=EN-GB style='mso-ansi-language:EN-GB'><o:p>&nbsp;</o:p></span>Los 
  bytes del puntero LZSS aqu� son <span style='color:red'>&lt;80&gt;&lt;27&gt;&lt;18&gt;</span><b
style='mso-bidi-font-weight:normal'>&lt;</b>04&gt;. El &lt;04&gt; es una nueva 
  l�nea, o l�nea en blanco. Pasemos estos<span style='mso-spacerun:yes'>� </span>3 
  bytes a binario:</p>
<p class=MsoNormal><o:p>&nbsp;</o:p><b style='mso-bidi-font-weight:normal'><span
style='color:#FF6600'>1</span><span style='color:#3366FF'>000000</span><span
style='color:#339966'>0</span><span style='mso-spacerun:yes'>� </span><span
style='color:#339966'>00100111 </span><span style='mso-spacerun:yes'>�</span><span
style='color:purple'>00011000 </span><span style='mso-spacerun:yes'>�</span><o:p></o:p></b></p>
<p class=MsoNormal><span style='mso-spacerun:yes'>����� </span>80<span
style='mso-spacerun:yes'>�������������� </span>27<span
style='mso-spacerun:yes'>����������� </span>18</p>
<p class=MsoNormal><o:p>&nbsp;</o:p>El <b
style='mso-bidi-font-weight:normal'>$80</b> primero implica que el bit m�s significativo 
  est� puesto a &quot;1&quot; puesto que en binario es %<b
style='mso-bidi-font-weight:normal'>1</b>0000000 (% significa notaci�n binaria, 
  as� como h es hexadecimal), as� que nos encontramos con un puntero LZSS. </p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Los 
  6 bits siguientes est�n a 0, es decir, es el valor 0+3 (por lo que te expliqu� 
  del m�nimo de caracteres que te puedes traer de la zona donde hay repetici�n), 
  lo cu�l quiere decir que te vas a <b style='mso-bidi-font-weight:normal'>traer 
  3 bytes</b> de la posici�n que diga el puntero.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Los 
  siguientes bits son %0 0010 0111, es decir, directamente el valor hexadecimal 
  <b
style='mso-bidi-font-weight:normal'>$27</b>, que es igual a 39 dec, es decir 39 
  posiciones hacia atr�s (contando desde 1) hay que tirar el puntero de lectura 
  para traerte los 3 bytes esos; en este caso, no movemos 39 bytes hacia atr�s. 
</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Si 
  est�s usando la versi�n USA de la ROM, ver�s que si echas hacia atr�s 39 posiciones 
  desde la posici�n donde aparece el $80 (el puntero LZ) los <b
style='mso-bidi-font-weight:normal'>tres bytes a leer son $00 03 02</b>. Luego 
  descifraremos que significan, ya que no es texto.</p>
<p><img src="foto10.JPG" width="618" height="63"> <br>
  Ahora, s�lo queda el <b style='mso-bidi-font-weight:normal'>$18</b> (18 hex 
  = 24 decimal) ese que es el tercer byte del puntero LZ; �ste indica que los 
  pr�ximos 24 bytes +1 (siempre se la suma 1) son de texto normal. Si lo miras 
  en el editor hexadecimal ver�s como 25 bytes despu�s del puntero (par�ndote 
  en el 04 que esta justo despu�s del puntero y cu�ntalo desde all� como 0, 1&#8230;25) 
  m�s all� aparece un nuevo puntero LZ que es el &lt;80&gt;&lt;1B&gt;&lt;0E&gt; 
  (siguiente l�nea a analizar).</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Por 
  �ltimo, queda <b style='mso-bidi-font-weight:normal'>descifrar</b> los bytes 
  que has copiado y que <b style='mso-bidi-font-weight:normal'>no son texto</b>; 
  pues ni m�s ni menos que el c�digo para <b style='mso-bidi-font-weight:normal'>bajar 
  de l�nea y el llenado de la l�nea siguiente de pantalla con una l�nea de tiles 
  negros (sin texto).<o:p></o:p></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Como 
  ver�s en la INTRO, entre las l�neas &quot;darkness sweeps the&quot; y &quot;troubled 
  land, as mana's&quot;, hay una l�nea sin texto del tama�o de un tile 8x8; esa 
  l�nea en blanco se consigue con los tres bytes estos misteriosos que has copiado 
  con el puntero m�s el $04 ese que no sabemos qu� significa; por tanto, cuando 
  encuentras $00 $03 $02 $04 quiere decir que hay una l�nea nueva, una l�nea en 
  blanco y algo m�s que no recuerdo muy bien.<o:p></o:p></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p></p>
<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=EN-GB
style='mso-ansi-language:EN-GB'>2. troubled land,`as mana's&lt;80&gt;&lt;1b&gt;&lt;0e&gt;&lt;09&gt;<o:p></o:p></span></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-GB style='mso-ansi-language:EN-GB'><o:p>&nbsp;</o:p></span>Los bytes del 
  puntero LZSS aqu� son <b style='mso-bidi-font-weight:normal'><span
style='color:red'>&lt;80&gt;&lt;1B&gt;&lt;0E&gt;. </span></b>El &lt;09&gt; es 
  un salto de l�nea u otra cosa similar. En binario ser�a:</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p><b style='mso-bidi-font-weight:normal'><span
style='color:#FF6600'>1</span><span style='color:#3366FF'>000000</span><span
style='color:#339966'>0</span><span style='mso-spacerun:yes'>� </span><span
style='color:#339966'>00011011 </span><span style='mso-spacerun:yes'>�</span><span
style='color:purple'>00001110 </span><span style='mso-spacerun:yes'>�</span><o:p></o:p></b></p>
<p class=MsoNormal><span style='mso-spacerun:yes'>����� </span>80<span
style='mso-spacerun:yes'>������������� </span>1B<span
style='mso-spacerun:yes'>����������� </span>0E</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>El 
  $80 indica que hay lo que sigue es un puntero y no texto. </p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Los 
  6 bits siguientes est�n en 0, pero le sumamos 3, quedando finalmente en 3.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Los 
  siguientes bits son 000 00011011 = 1B hex = 27 decimal. Es decir que hay que 
  ir 27 posiciones hacia atr�s a copiar 3 bytes.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Los 
  siguientes bits son 00001110 = 0E hex = 14 decimal. Es decir que despu�s del 
  puntero, 14+1 posiciones mas adelante estar� el siguiente puntero. P�rate en 
  el 09 contando 0, luego sigue con 1, 2&#8230;y en la posici�n 15 estar�s en el $82 
  del siguiente puntero que es &lt;82&gt;&lt;2D&gt;&lt;16&gt;.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Ahora 
  hay que descifrar que hay que copiar, si vamos hacia atr�s 27 posiciones, contando 
  desde 1, nos encontramos con el puntero anterior <b style='mso-bidi-font-weight:
normal'>&lt;80&gt;&lt;27&gt;&lt;18&gt;, pero alli no esta el puntero, </b>all�<b
style='mso-bidi-font-weight:normal'> </b>esta el c�digo descomprimido, sol oque 
  en el editor se ve el puro puntero.<b style='mso-bidi-font-weight:normal'> <o:p></o:p></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><b
style='mso-bidi-font-weight:normal'>Un puntero LZ nunca apunta a otro, solo a 
  c�digo o letras. <o:p></o:p></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Bueno, 
  alli se copia lo que esta descomprimido en 1. que es el c�digo para <b
style='mso-bidi-font-weight:normal'>bajar de l�nea y el llenado de la l�nea siguiente 
  de pantalla con una l�nea de tiles negros (sin texto). </b><o:p></o:p></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><b
style='mso-bidi-font-weight:normal'><o:p>&nbsp;</o:p></b><span
style='color:red'>�Por qu� este puntero no copia directamente desde $00 $03 $02, 
  ubicado 64 posiciones mas arriba si no que del que se acaba de descomprimir 
  25 posiciones arriba?<o:p></o:p></span></p>
<p class=MsoNormal>Respuesta: pronto lo sabr�, no lo tengo muy claro aun.<o:p></o:p></p>
<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><o:p>&nbsp;</o:p></b></p>
<p class=MsoNormal><b style='mso-bidi-font-weight:normal'>3. </b><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB style='mso-ansi-language:
EN-GB'>power fades...&lt;82&gt;&lt;2D&gt;&lt;16&gt;<o:p></o:p></span></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Ahora 
  vamos m�s r�pido. Puntero: <b style='mso-bidi-font-weight:normal'><span
style='color:red'>&lt;82&gt;&lt;2D&gt;&lt;16&gt;<o:p></o:p></span></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><b
style='mso-bidi-font-weight:normal'>Binario:<o:p></o:p></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><b
style='mso-bidi-font-weight:normal'><o:p>&nbsp;</o:p></b><b style='mso-bidi-font-weight:normal'><span
style='color:#FF6600'>1</span><span style='color:#3366FF'>000001</span><span
style='color:#339966'>0</span><span style='mso-spacerun:yes'>� </span><span
style='color:#339966'>00101101 </span><span style='mso-spacerun:yes'>�</span><span
style='color:purple'>00010110 </span><span style='mso-spacerun:yes'>�</span><o:p></o:p></b></p>
<p class=MsoNormal><span style='mso-spacerun:yes'>����� </span>82<span
style='mso-spacerun:yes'>������������ </span>2D<span
style='mso-spacerun:yes'>����������� </span>16</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>El 
  bit mas significativo del $82=1000010 es 1 lo que indica que hay lo que sigue 
  es un puntero y no texto. </p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Los 
  6 bits siguientes est�n en 0, pero le sumamos 3, entonces copiaremos 3 bytes.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Los 
  siguientes bits son 0 00101101 = 45 decimal. Es decir que hay que ir 45 posiciones 
  hacia atr�s a copiar 3 bytes.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Los 
  siguientes bits son 00010110 = 22 decimal. Es decir que despu�s del puntero, 
  22+1 posiciones mas adelante estar� el siguiente puntero.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Ahora 
  hay que descifrar que hay que copiar, si vamos hacia atr�s 45 posiciones constando 
  desde 0, nos encontraremos con. 802718. al igual que el anterior (l�nea 2.), 
  nos encontramos con <b style='mso-bidi-font-weight:normal'>el c�digo descomprimo 
  que reverenciaba el puntero mostrado</b> en 1. Ya sabemos lo que hace, entonces 
  sigamos.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p><b style='mso-bidi-font-weight:normal'><span lang=EN-GB
style='mso-ansi-language:EN-GB'>4. people await a hero who&lt;82&gt;&lt;1a&gt;&lt;09&gt;<o:p></o:p></span></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-GB style='mso-ansi-language:EN-GB'><o:p>&nbsp;</o:p></span>Ahora vamos 
  m�s r�pido. Puntero: <b style='mso-bidi-font-weight:normal'><span
style='color:red'>&lt;82&gt;&lt;1A&gt;&lt;09&gt;<o:p></o:p></span></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><b
style='mso-bidi-font-weight:normal'>Binario:<o:p></o:p></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><b
style='mso-bidi-font-weight:normal'><o:p>&nbsp;</o:p></b><b style='mso-bidi-font-weight:normal'><span
style='color:#FF6600'>1</span><span style='color:#3366FF'>000001</span><span
style='color:#339966'>0</span><span style='mso-spacerun:yes'>� </span><span
style='color:#339966'>00011010 </span><span style='mso-spacerun:yes'>�</span><span
style='color:purple'>00001001 </span><span style='mso-spacerun:yes'>�</span><o:p></o:p></b></p>
<p class=MsoNormal><span style='mso-spacerun:yes'>����� </span>82<span
style='mso-spacerun:yes'>������������ </span>1A<span
style='mso-spacerun:yes'>������������ </span>9</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-GB style='mso-ansi-language:EN-GB'><o:p>&nbsp;</o:p></span>El bit mas 
  significativo es 1, entonces lo que sigue es un puntero LZ.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Los 
  6 bits siguientes est�n en 0, pero le sumamos 3, entonces copiaremos 3 bytes. 
</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Los 
  siguientes bits son 0 00011010 = 26 decimal. Es decir que hay que ir 26 posiciones 
  hacia atr�s a copiar esos 3 bytes.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Los 
  siguientes bits son 00001001 = 9 decimal. Es decir que despu�s del puntero, 
  9+1 posiciones mas adelante estar� el siguiente puntero.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Ahora 
  hay que descifrar que hay que copiar, si vamos hacia atr�s 26 posiciones nos 
  encontraremos con 822D16, es decir, el puntero anterior. Ya sabemos lo que hace 
  el puntero anterior, por lo que este hace exactamente lo mismo.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p><o:p>&nbsp;</o:p></p>
<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=EN-GB
style='mso-ansi-language:EN-GB'>5. will wield&lt;82&gt;&lt;5A&gt;&lt;80&gt;&lt;65&gt;&lt;02&gt;ord&lt;86&gt;&lt;35&gt;&lt;0b&gt;&lt;02&gt;<o:p></o:p></span></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-GB style='mso-ansi-language:EN-GB'><o:p>&nbsp;</o:p></span>Esta l�nea 
  es importante ya que se ve la primera compresi�n de TEXTO.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Tenemos 
  aqu� 5 bytes, y el puntero es SOLO de 3 bytes �qu� pasa? Lo que pasa es estamos 
  en un caso de Excepci�n ya que hay 2 punteros, <b style='mso-bidi-font-weight:
normal'>uno seguido de otro. </b>Seg�n lo explicado en &#8220;Puntero LZSS&#8221; el primero 
  puntero puede ser de 2 bytes y el segundo de 3 bytes, esto sucede aqu�. </p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Puntero1: 
  <b style='mso-bidi-font-weight:normal'><span lang=EN-GB style='color:red;
mso-ansi-language:EN-GB'>&lt;82&gt;&lt;5A&gt;</span></b><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB style='mso-ansi-language:EN-GB'><o:p></o:p></span></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-GB style='mso-ansi-language:EN-GB'>Puntero2: <b style='mso-bidi-font-weight:
normal'><span style='color:#FF6600'>&lt;80&gt;&lt;65&gt;&lt;02&gt;</span><span
style='color:red'><o:p></o:p></span></b></span></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB style='color:red;
mso-ansi-language:EN-GB'><o:p>&nbsp;</o:p></span></b>Veamos primero el puntero 
  1 en Binario:</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p><b style='mso-bidi-font-weight:normal'><span
style='color:#FF6600'>1</span><span style='color:#3366FF'>000001</span><span
style='color:#339966'>0</span><span style='mso-spacerun:yes'>� </span><span
style='color:#339966'>01011010 </span><o:p></o:p></b></p>
<p class=MsoNormal><span style='mso-spacerun:yes'>����� </span>82<span
style='mso-spacerun:yes'>������������ </span>5A<span
style='mso-spacerun:yes'>�� </span><span
style='mso-spacerun:yes'>����������</span></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>El 
  bit mas significativo es 1, entonces lo que sigue es un puntero LZ.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Los 
  6 bits siguientes est�n en 00000001=1 decimal, pero le sumamos 3, entonces copiaremos 
  4 bytes. </p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Los 
  siguientes bits son 0 01011010 = 90 decimal. Es decir que hay que ir 90 posiciones 
  hacia atr�s a copiar esos 4 bytes.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Los 
  siguientes bits NO EXISTEN ya que este puntero esta atr�s del segundo, y la 
  distancia con el<span style='mso-spacerun:yes'>� </span>segundo puntero es 0.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Ahora 
  hay que descifrar que hay que copiar, si vamos hacia atr�s 90 posiciones, nos 
  encontraremos con <b style='mso-bidi-font-weight:normal'>s</b> de &#8220;sweep<b
style='mso-bidi-font-weight:normal'>s th</b>e&#8221; de la linea 1. </p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Pero 
  deber�amos copiar <b style='mso-bidi-font-weight:normal'>&lt;espacio&gt;the</b> 
  para que esto funcione (no se porque sucede esto).</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Luego 
  si copiamos los 4 bytes a partir del espacio tendr�amos el texto &#8220; the&#8221;.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Veamos 
  primero el puntero 2 en Binario:</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p><b style='mso-bidi-font-weight:normal'><span
style='color:#FF6600'>1</span><span style='color:#3366FF'>000000</span><span
style='color:#339966'>0</span><span style='mso-spacerun:yes'>� </span><span
style='color:#339966'>01100101 </span><span style='mso-spacerun:yes'>�</span><span
style='color:purple'>00000010 </span><span style='mso-spacerun:yes'>�</span><o:p></o:p></b></p>
<p class=MsoNormal><span style='mso-spacerun:yes'>����� </span>80<span
style='mso-spacerun:yes'>������������ </span>65<span
style='mso-spacerun:yes'>������������ </span>02</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>El 
  bit mas significativo es 1, entonces lo que sigue es un puntero LZ.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Los 
  6 bits siguientes est�n en 0, pero le sumamos 3, entonces copiaremos 3 bytes. 
</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Los 
  siguientes bits son 0 01100101 = 101 decimal. Es decir que hay que ir 101 posiciones 
  hacia atr�s a copiar esos 3 bytes.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Los 
  siguientes bits son 00000010 = 2 decimal, hay que ir 2+1 posici�n mas adelante 
  para encontrar el siguiente puntero.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Ahora 
  hay que descifrar que hay que copiar, si vamos hacia atr�s 101 posiciones, nos 
  encontraremos con TEXTO (en la e de darkn<b style='mso-bidi-font-weight:normal'>e</b>ss), 
  por lo que se restan 3 bytes, entonces llegamos el espacio que esta justo despu�s 
  de &#8220;darkness&lt;aqui&gt;<b style='mso-bidi-font-weight:normal'><span
style='color:blue'>sw</span></b>eeps&#8221;.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Luego 
  si copiamos los siguientes 3 bytes tendr�amos el texto &#8220; sw&#8221;.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Ahora 
  si juntamos ambos punteros descifrados tenemos: </p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p><span
lang=EN-GB style='mso-ansi-language:EN-GB'>Antes: <span style='mso-tab-count:
1'> </span><span style='mso-tab-count:1'>����������� </span><b
style='mso-bidi-font-weight:normal'>will wield<span style='color:red'>&lt;82&gt;&lt;5A&gt;</span><span
style='color:#FF6600'>&lt;80&gt;&lt;65&gt;&lt;02&gt;</span>ord<o:p></o:p></b></span></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-GB style='mso-ansi-language:EN-GB'><o:p>&nbsp;</o:p></span><span
lang=EN-GB style='mso-ansi-language:EN-GB'>Despu�s:<span style='mso-tab-count:
1'>��������� </span><b style='mso-bidi-font-weight:normal'>will wield<span
style='color:red'> the sw</span>ord<o:p></o:p></b></span></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-GB style='mso-ansi-language:EN-GB'>SIIII, lo hicimos!!!<span
style='mso-spacerun:yes'>� </span>(aplausos)<o:p></o:p></span></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-GB style='mso-ansi-language:EN-GB'><o:p>&nbsp;</o:p></span>Ahora que viste 
  como estaba comprimido y lo descomprimimos a mano, podemos seguir con la ultima 
  parte de esta linea.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p><b
style='mso-bidi-font-weight:normal'>ord&lt;86&gt;&lt;35&gt;&lt;0b&gt;&lt;02&gt;<o:p></o:p></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><b
style='mso-bidi-font-weight:normal'><o:p>&nbsp;</o:p></b>Puntero: <b style='mso-bidi-font-weight:normal'><span style='color:red'>&lt;86&gt;&lt;35&gt;&lt;0B&gt;<o:p></o:p></span></b></p>
<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
style='color:#FF6600'><o:p>&nbsp;</o:p></span></b><b style='mso-bidi-font-weight:normal'><span
style='color:#FF6600'>1</span><span style='color:#3366FF'>000011</span><span
style='color:#339966'>0</span><span style='mso-spacerun:yes'>� </span><span
style='color:#339966'>00110101 </span><span style='mso-spacerun:yes'>�</span><span
style='color:purple'>00001011 </span><span style='mso-spacerun:yes'>�</span><o:p></o:p></b></p>
<p class=MsoNormal><span style='mso-spacerun:yes'>����� </span>86<span
style='mso-spacerun:yes'>������������ </span>35<span
style='mso-spacerun:yes'>������������ </span>0B</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><b
style='mso-bidi-font-weight:normal'><span style='color:red'><o:p>&nbsp;</o:p></span></b>El 
  bit mas significativo es 1, entonces lo que sigue es un puntero LZ.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Los 
  6 bits siguientes est�n en 000011 = 3 y le sumamos 3, entonces copiaremos 6 
  bytes. </p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Los 
  siguientes bits son 0 00110101 = 53 decimal. Es decir que hay que ir 53 posiciones 
  hacia atr�s a copiar esos 6 bytes.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Los 
  siguientes bits son 00001011 = 11 decimal, hay que ir 11+1 posici�n mas adelante 
  para encontrar el siguiente puntero.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Ahora 
  hay que descifrar que hay que copiar, si vamos hacia atr�s 53 posiciones, nos 
  encontraremos con <b style='mso-bidi-font-weight:normal'>des </b>de &#8220;power fa<b
style='mso-bidi-font-weight:normal'>des&#8230;&lt;82&gt;&lt;2D&gt;&lt;16&gt;</b>&#8221;, si 
  copiamos 6 letras estas son: <b style='mso-bidi-font-weight:normal'>des&#8230;<o:p></o:p></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Aqu� 
  hay un error (que aun no entiendo cuando o porque sucede) ya que deber�amos 
  copiar estos 6 bytes<span style='mso-spacerun:yes'>�� </span><b
style='mso-bidi-font-weight:normal'>&#8230;&lt;82&gt;&lt;2D&gt;&lt;16&gt;</b>, en vez 
  de <b style='mso-bidi-font-weight:normal'>des&#8230;</b> ya que despu�s de &#8220;wield 
  the sword&#8221; siguen 3 puntos (&#8230;)<span style='mso-spacerun:yes'>� </span>y luego 
  el c�digo para que baje, que seria &lt;82&gt;&lt;2D&gt;&lt;16&gt;. Por lo tanto, 
  se copiaron 6 bytes que no corresponden, esto es debido a los bit 00110101 = 
  35 hex = 53 decimal, si hubiera sido 110010 = 50 decimal, seria perfecto ya 
  que se copiar�an:<span style='mso-spacerun:yes'>� </span><span
style='mso-spacerun:yes'>�</span><b style='mso-bidi-font-weight:normal'>&#8230;&lt;82&gt;&lt;2D&gt;&lt;16&gt;</b>, 
  quedando la frase completa:</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB style='mso-ansi-language:
EN-GB'>will wield<span style='color:red'> the sw</span>ord<span
style='color:red'>&#8230;</span></span></b><span lang=EN-GB style='mso-ansi-language:
EN-GB'><o:p></o:p></span></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-GB style='color:red;mso-ansi-language:EN-GB'><o:p>&nbsp;</o:p></span></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-GB style='color:red;mso-ansi-language:EN-GB'><o:p>&nbsp;</o:p></span><b style='mso-bidi-font-weight:normal'>6. 
  excalibur,`&lt;80&gt;&lt;31&gt;&lt;01&gt;al&lt;80&gt;g&lt;03&gt;giga&lt;88&gt;T&lt;00&gt;&lt;05&gt;<o:p></o:p></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
style='color:red'><o:p>&nbsp;</o:p></span>Puntero 1:<b style='mso-bidi-font-weight:normal'> 
  <span
style='color:red'>&lt;80&gt;&lt;31&gt;&lt;01&gt;<span
style='mso-spacerun:yes'>� </span></span>=&gt;<span style='color:#FF6600'>1</span><span
style='color:#3366FF'>000000</span><span style='color:#339966'>0</span><span
style='mso-spacerun:yes'>� </span><span style='color:#339966'>00110001 </span><span
style='mso-spacerun:yes'>�</span><span style='color:purple'>00000001 </span><span
style='mso-spacerun:yes'>�</span><o:p></o:p></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><b
style='mso-bidi-font-weight:normal'><span style='color:red'><o:p>&nbsp;</o:p></span></b>El 
  bit mas significativo es 1, entonces lo que sigue es un puntero LZ.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Los 
  6 bits siguientes est�n en 0 y le sumamos 3, entonces copiaremos 3 bytes. </p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Los 
  siguientes bits son 0 00110001 = 49 decimal. Es decir que hay que ir 49 posiciones 
  hacia atr�s a copiar esos 3 bytes.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Los 
  siguientes bits son 00000001 = 1 decimal, hay que ir 1+1=2 posiciones m�s adelante 
  para encontrar el siguiente puntero.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Ahora 
  hay que descifrar que hay que copiar, si vamos hacia atr�s 49 (deber�a ser 44 
  y no 49) <span style='mso-spacerun:yes'>�</span>posiciones, nos encontraremos 
  con <b style='mso-bidi-font-weight:normal'><span
style='mso-spacerun:yes'>�</span>her</b> de &#8220;a hero who&#8221;. El texto a copiar es 
  <b
style='mso-bidi-font-weight:normal'>her</b>.<span style='color:red'><o:p></o:p></span></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Tenemos 
  entonces:</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><b
style='mso-bidi-font-weight:normal'>excalibur,`<span style='color:red'>her</span>al&lt;80&gt;g&lt;03&gt;giga&lt;88&gt;T&lt;00&gt;&lt;05&gt;<o:p></o:p></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Esa 
  comilla simple (&#8216;) no se que hace ni porque esta all�, ya que en el juego solo 
  se ve un espacio.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Puntero 
  2: <b style='mso-bidi-font-weight:normal'>&lt;80&gt;g&lt;03&gt; </b>pasa a ser<b
style='mso-bidi-font-weight:normal'> <span style='color:red'>&lt;80&gt;&lt;67&gt;&lt;03&gt;</span> 
  </b>ya que <b style='mso-bidi-font-weight:normal'>g</b> en el editor tiene el 
  c�digo 67.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>En 
  binario:<span style='mso-spacerun:yes'>�� </span><b style='mso-bidi-font-weight:
normal'><span style='color:#FF6600'>1</span><span style='color:#3366FF'>000000</span><span
style='color:#339966'>0</span><span style='mso-spacerun:yes'>� </span><span
style='color:#339966'>01100111 </span><span style='mso-spacerun:yes'>�</span><span
style='color:purple'>00000011 </span><span style='mso-spacerun:yes'>�</span><o:p></o:p></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Esta 
  vez, vamos mas r�pido ya que debes haber captado la idea:</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Primer 
  bit = 1, hablamos de puntero LZ.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>6 bits 
  siguientes en 0, entonces 0+3 bytes se copian.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Luego 
  01100111 binario = 103 decimal, volver 103 posiciones atr�s a copiar 3 bytes, 
  que a la vez son &#8220;ed &#8220; de &#8220;troubl<b style='mso-bidi-font-weight:normal'>ed</b> 
  land&#8221;</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Luego 
  00000011 binario = 3 decimal, entonces 3 posiciones mas adelante esta el pr�ximo 
  puntero.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Tenemos 
  entonces:</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><b
style='mso-bidi-font-weight:normal'>excalibur,`<span style='color:red'>her</span>al<span
style='color:red'>ed</span> giga&lt;88&gt;T&lt;00&gt;&lt;05&gt;<o:p></o:p></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>En 
  el texto original tenemos &#8220;herald,&#8221; pero no se por donde salio esa &#8220;,&#8221;.<o:p></o:p></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Puntero3: 
  <span style='mso-tab-count:1'>������� </span>&lt;88&gt;T&lt;00&gt; pasa a ser: 
  <b
style='mso-bidi-font-weight:normal'><span style='color:red'>&lt;88&gt;&lt;54&gt;&lt;00&gt;</span></b> 
  por que T en el editor es 54. </p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>En 
  binario: <span style='mso-spacerun:yes'>��</span><b style='mso-bidi-font-weight:
normal'><span style='color:#FF6600'>1</span><span style='color:#3366FF'>000100</span><span
style='color:#339966'>0</span><span style='mso-spacerun:yes'>� </span><span
style='color:#339966'>01010100 </span><span style='mso-spacerun:yes'>�</span><span
style='color:purple'>00000000 </span><span style='mso-spacerun:yes'>�</span><o:p></o:p></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Primer 
  bit = 1, hablamos de puntero LZ.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>6 bits 
  siguientes son 000100 = 4 decimal, entonces 4+3 = 7 bytes se copian.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Luego 
  01010100 binario = 84 decimal, volver 84 posiciones atr�s a copiar 7 bytes, 
  que a la vez son &#8220;r fades&#8220; de &#8220;power fades&#8221;.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Otro 
  error (no se porqu�) ya que deber�amos copiar <b style='mso-bidi-font-weight:
normal'>s&#8230;&lt;82&gt;&lt;2D&gt;&lt;16&gt;</b> que est�n despu�s de &#8220;fades&#8230;&#8221;</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Luego 
  los �ltimos bits est�n en 0, por lo que el siguiente puntero esta XXX.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Tenemos 
  entonces:</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><b
style='mso-bidi-font-weight:normal'>excalibur,`<span style='color:red'>her</span>al<span
style='color:red'>ed</span> giga<span style='color:red'>s&#8230;</span>&lt;82&gt;&lt;2D&gt;&lt;16&gt;<span
style='color:red'><o:p></o:p></span></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p><o:p>&nbsp;</o:p></p>
<p class=MsoNormal><b style='mso-bidi-font-weight:normal'>7. &lt;82&gt;.&lt;01&gt;bl&lt;80&gt;e&lt;01&gt;h&lt;80&gt;~&lt;02&gt;had&lt;82&gt;&lt;82&gt;&lt;00&gt;y&lt;82&gt;&lt;19&gt;&lt;0A&gt;<o:p></o:p></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Desde 
  aqu� no seguir�, ya que creo que captan la idea, pero dir� los punteros. Al 
  final mostrare e</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>La 
  l�nea y en rojo lo que los punteros copiaban desde atr�s. </p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Para 
  los que en verdad quieren aprender y aplicar lo que ense�o, les dejo de <span
style='color:red'>tarea</span> encontrar los textos que reverencian cada puntero 
  (que no sea se salto o fin de l�nea) que pongo de aqu� en adelante, bueno, por 
  lo menos hagan de 1 l�nea o 2 para que sepan si aprendieron.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Puntero1: 
  &lt;82&gt;.&lt;01&gt; pasa a ser &lt;82&gt;&lt;2E&gt;&lt;01&gt;</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Puntero2: 
  &lt;80&gt;E&lt;01&gt; pasa a ser &lt;80&gt;&lt;65&gt;&lt;01&gt;</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Puntero3: 
  &lt;80&gt;~&lt;02&gt; pasa a ser &lt;80&lt;7E&gt;&lt;02&gt;</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Puntero4: 
  &lt;82&gt;&lt;82&gt;&lt;00&gt;</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Puntero5: 
  &lt;82&gt;&lt;19&gt;&lt;0A&gt;<span style='mso-spacerun:yes'>������ </span>-&gt;puntero 
  de fin de l�nea</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>L�nea 
  descomprimida:<o:p></o:p></p>
<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=EN-GB
style='color:red;mso-ansi-language:EN-GB'>the</span></b><b style='mso-bidi-font-weight:
normal'><span lang=EN-GB style='mso-ansi-language:EN-GB'> bl<span
style='color:red'>ade </span>had <span style='color:red'>man</span>y<o:p></o:p></span></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-GB style='mso-ansi-language:EN-GB'><o:p>&nbsp;</o:p></span><span
lang=EN-GB style='mso-ansi-language:EN-GB'><o:p>&nbsp;</o:p></span></p>
<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=EN-GB
style='mso-ansi-language:EN-GB'>8. names,`for &lt;80&gt;o&lt;82&gt;&lt;1d&gt;&lt;03&gt;been&lt;82&gt;m&lt;07&gt;<o:p></o:p></span></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Puntero1: 
  &lt;80&gt;o pasa a ser &lt;80&gt;&lt;6F&gt;<span style='mso-spacerun:yes'>� 
  </span>puntero especial, recu�rdenlo.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Puntero2: 
  &lt;82&gt;&lt;1D&gt;&lt;03&gt;</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Puntero3: 
  &lt;82&gt;m&lt;07&gt; pasa a ser &lt;82&lt;6D&gt;&lt;07&gt; -&gt;salto de l�nea</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p><span lang=EN-GB style='mso-ansi-language:EN-GB'>L�nea 
  descomprimida:<o:p></o:p></span></p>
<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=EN-GB
style='mso-ansi-language:EN-GB'>names, <span style='color:red'>for it has</span> 
  been <o:p></o:p></span></b></p>
<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=EN-GB
style='mso-ansi-language:EN-GB'><o:p>&nbsp;</o:p></span></b></p>
<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=EN-GB
style='mso-ansi-language:EN-GB'><o:p>&nbsp;</o:p></span></b><b style='mso-bidi-font-weight:normal'><span lang=EN-GB
style='mso-ansi-language:EN-GB'>9. celebrat&lt;80&gt;&lt;b8&gt;&lt;08&gt;in myths 
  &lt;80&gt;&lt;c0&gt;&lt;82&gt;&lt;1a&gt;&lt;05&gt;<o:p></o:p></span></b></p>
<p class=MsoNormal><span lang=EN-GB style='mso-ansi-language:EN-GB'><o:p>&nbsp;</o:p></span>Puntero1: 
  &lt;80&gt;&lt;B8&gt;&lt;08&gt;<o:p></o:p></p>
<p class=MsoNormal>Puntero2: &lt;80&gt;&lt;C0&gt;<span
style='mso-spacerun:yes'>� </span><span style='mso-tab-count:1'>����������� </span>-&gt; 
  puntero especial<o:p></o:p></p>
<p class=MsoNormal>Puntero3: &lt;82&gt;&lt;1A&gt;&lt;05&gt;<span
style='mso-tab-count:1'>���� </span>-&gt;puntero de fin de l�nea<o:p></o:p></p>
<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><o:p>&nbsp;</o:p></b>L�nea 
  descomprimida:<o:p></o:p></p>
<p class=MsoNormal><b style='mso-bidi-font-weight:normal'>celebrat<span
style='color:red'>ed </span>in myths <span style='color:red'>and<o:p></o:p></span></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p><o:p>&nbsp;</o:p></p>
<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=EN-GB
style='mso-ansi-language:EN-GB'>10. legend&lt;82&gt;&lt;e0&gt;&lt;80&gt;&lt;da&gt;&lt;09&gt;ghout 
  time&lt;82&gt;j&lt;01&gt;&lt;03&gt;<o:p></o:p></span></b></p>
<p class=MsoNormal><span lang=EN-GB style='mso-ansi-language:EN-GB'><o:p>&nbsp;</o:p></span>Puntero1: 
  &lt;82&gt;&lt;E0&gt;<span style='mso-tab-count:
2'>������������� </span>-&gt;puntero especial<o:p></o:p></p>
<p class=MsoNormal>Puntero2: &lt;80&gt;&lt;DA&gt;&lt;09&gt;<o:p></o:p></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Puntero3: 
  &lt;82&gt;j&lt;01&gt; pasa a ser &lt;82&gt;&lt;6A&gt;&lt;01&gt; <span
style='mso-tab-count:1'>��� </span>-&gt;puntero de fin de l�nea<o:p></o:p></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p><span lang=EN-GB style='mso-ansi-language:EN-GB'>L�nea 
  descomprimida:<o:p></o:p></span></p>
<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=EN-GB
style='mso-ansi-language:EN-GB'>leyends <span style='color:red'>throu</span>ghout 
  time<span style='color:red'>. </span><o:p></o:p></span></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-GB style='mso-ansi-language:EN-GB'><o:p>&nbsp;</o:p></span><span
lang=EN-GB style='mso-ansi-language:EN-GB'><o:p>&nbsp;</o:p></span></p>
<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=EN-GB
style='mso-ansi-language:EN-GB'>11. b&lt;80&gt;&lt;0c&gt;&lt;00&gt;a&lt;80&gt;&lt;a7&gt;&lt;01&gt;of&lt;82&gt;&lt;a4&gt;&lt;00&gt;s&lt;80&gt;&lt;a6&gt;&lt;05&gt;peak 
  t&lt;82&gt;&lt;c1&gt;&lt;10&gt;&lt;08&gt;<o:p></o:p></span></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-GB style='mso-ansi-language:EN-GB'><o:p>&nbsp;</o:p></span>Puntero1: &lt;80&gt;&lt;0C&gt;&lt;00&gt;<o:p></o:p></p>
<p class=MsoNormal>Puntero1: &lt;80&gt;&lt;A7&gt;&lt;01&gt;<o:p></o:p></p>
<p class=MsoNormal>Puntero1: &lt;82&gt;&lt;A4&gt;&lt;00&gt;<o:p></o:p></p>
<p class=MsoNormal>Puntero1: &lt;80&gt;&lt;A6&gt;&lt;05&gt;<o:p></o:p></p>
<p class=MsoNormal>Puntero1: &lt;82&gt;&lt;C1&gt;&lt;10&gt;<span
style='mso-tab-count:1'>���� </span>-&gt;puntero de fin de l�nea<o:p></o:p></p>
<p class=MsoNormal><o:p>&nbsp;</o:p>L�nea descomprimida:<o:p></o:p></p>
<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=EN-GB
style='mso-ansi-language:EN-GB'>b<span style='color:red'>ut </span>a<span
style='color:red'>ll </span>of <span style='color:red'>the</span>s<span
style='color:red'>e</span> <span style='color:red'>sp</span>eak t<span
style='color:red'>o</span><o:p></o:p></span></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-GB style='mso-ansi-language:EN-GB'><o:p>&nbsp;</o:p></span></p>
<p class=MsoNormal><b style='mso-bidi-font-weight:normal'>12. just one weapon:&lt;80&gt;&lt;13&gt;&lt;00&gt;&lt;07&gt;<o:p></o:p></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Puntero 
  1: &lt;80&gt;&lt;13&gt;&lt;00&gt; <span
style='mso-spacerun:yes'>�</span>-&gt;puntero fin de l�nea</p>
<p class=MsoNormal><o:p>&nbsp;</o:p>L�nea descomprimida:<o:p></o:p></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><b
style='mso-bidi-font-weight:normal'>just one weapon:<span
style='mso-spacerun:yes'>� </span></b>(queda igual ya que esta l�nea no tiene 
  letras comprimidas)<o:p></o:p></p>
<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><o:p>&nbsp;</o:p></b><b style='mso-bidi-font-weight:normal'><o:p>&nbsp;</o:p></b></p>
<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span lang=EN-GB
style='mso-ansi-language:EN-GB'>13. &lt;8c&gt;&lt;ca&gt;&lt;82&gt;2&lt;83&gt;&lt;19&gt;&lt;82&gt;F&lt;8a&gt;&lt;01&gt;&lt;18&gt;&lt;04&gt; 
  <span style='mso-spacerun:yes'>�</span><span
style='mso-spacerun:yes'>�</span>-&gt; linea de &#8220;the sword of mana&#8221;<o:p></o:p></span></b></p>
<p class=MsoNormal><b style='mso-bidi-font-weight:normal'>P[\\]|[~&lt;7f&gt;`QRSTUV`co{|`ltd&lt;82&gt;$&lt;00&gt;&lt;06&gt;&lt;82&gt;g&lt;07&gt;rights 
  <span style='mso-spacerun:yes'>�</span><span style='mso-spacerun:yes'>�</span><span
style='mso-spacerun:yes'>�</span>-&gt;esta es otra l�nea<o:p></o:p></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Este 
  es lejos, la m�s dif�cil ya que no hay texto puro, hay pura compresi�n.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Puntero 
  1: &lt;8C&gt;&lt;CA&gt;<span style='mso-tab-count:1'>���������� </span><span
style='mso-spacerun:yes'>��� </span>-&gt;copia 6+3 letras<span
style='mso-tab-count:1'>�� </span>de &#8221;ero who&#8221; (creo que ese t�pico error, ya 
  que no puede copiar tantas letras, 6+3=9, y en nueve letras tienes &#8220;the sword&#8221; 
  y eso no esta atr�s, en ningun lado, ademas no se copia desde &#8220;ero who&#8221; ya que 
  no nos sirve)<o:p></o:p></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Puntero 
  2: &lt;82&gt;2<span style='mso-spacerun:yes'>� </span>que pasa a ser &lt;82&gt;&lt;32&gt;<span style='mso-spacerun:yes'>�� 
  </span>-&gt; copia 1+3 letras</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Puntero 
  3: &lt;83&gt;&lt;19&gt; <span style='mso-tab-count:2'>������������ </span>-&gt; 
  copia 1 + 3 letras</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Puntero 
  4: &lt;82&gt;F<span style='mso-spacerun:yes'>� </span>que pasa a ser &lt;82&gt;&lt;46&gt;<span style='mso-spacerun:yes'>�� 
  </span>-&gt; copia 1+3 letras -&gt; 70 posiciones atr�s, lleva al puntero &lt;80&gt;&lt;DA&gt;&lt;09&gt; 
  del punto 10.<o:p></o:p></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Puntero 
  5: &lt;8a&gt;&lt;01&gt;&lt;18&gt;<o:p></o:p></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Creo 
  que aqu� se acaba el intro, ya que sigue un &lt;04&gt; que debe ser fin de l�nea. 
</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Lo 
  que sigue en:</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><b
style='mso-bidi-font-weight:normal'><span lang=EN-GB style='mso-ansi-language:
EN-GB'>P[\\]|[~&lt;7f&gt;`QRSTUV`co{|`ltd&lt;82&gt;$&lt;00&gt;&lt;06&gt;&lt;82&gt;g&lt;07&gt;rights<span
style='mso-spacerun:yes'>��� </span></span></b><span lang=EN-GB
style='mso-ansi-language:EN-GB'><o:p></o:p></span></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>es 
  eso lo que se muestra despu�s del intro &#8220;�1993, 1994 Square.&#8221;</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p><span lang=EN-GB style='mso-ansi-language:EN-GB'>L�nea 
  descomprimida:<o:p></o:p></span></p>
<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
style='color:red'>the sword of mana.<span style='mso-spacerun:yes'>� </span></span><span
style='color:black'>(toda la linea estaba comprimida)<o:p></o:p></span></b></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Bueno, 
  acabamos por fin de entender como se descomprime cada l�nea. Ahora necesitamos 
  hacer un texto en espa�ol que remplace todas estas l�neas traducidas, claro, 
  con puntero nuevos hechos por ustedes. �No estar� pidiendo mucho?</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p><font color="#000099" size="+1"><strong><a name="Una vez descomprimida, �Qu� sigue">Una 
  vez descomprimida, �Qu� sigue?</a></strong></font></p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><b
style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt'><o:p>&nbsp;</o:p></span></b>La 
  intro la descomprimimos en un txt o en un papel simplemente, ahora hay que hacer 
  un nuevo intro en espa�ol, con nuevos punteros. Pero no es tan f�cil como llegar 
  y reemplazar los punteros por otros, o moverlos. NO ES TAN FACIL.</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Hay 
  que saber varias cosas&#8230;pero quiz�s mas adelante pondr� en la <b
style='mso-bidi-font-weight:normal'>segunda parte</b> de esta gu�a, creo que es 
  mucho por hoy, �o no?</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p></p>
<h1><font color="#000066" size="+1"><strong><a name="Fin de primera parte">Fin de primera parte</a></strong></font></h1>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p></o:p>Bueno, 
  espero que les haya gustado. Me he pasado horas en esto, pregunt�ndole a Magno 
  y analizando cada bit, dej�ndole colores bonitos para que no se pierdan, etc. 
</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'>Ah, 
  y ojal� que ustedes mismos hagan una descompresi�n de al menos 2 punteros. �no 
  sean vagos!</p>
<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><o:p>&nbsp;</o:p>Nos 
  vemos en la segunda parte de esta apasionante gu�a �no quer�an aprender compresi�n 
  y descompresi�n?</p>

<hr style="width: 100%; height: 2px;">

<a href="../../doc_traduc.php">Volver</a>
<br>
<font color="#FFFFFF">
<?php 
include '../../disq.php';
?>
</font>
<center><?php include ('../../pie.php'); ?></center>
</small>
</body>
</html>
