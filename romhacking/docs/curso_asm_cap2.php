<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>La Web de Dark-N - Curso ASM SNES. Cap�tulo 2: Instrucciones B�sicas</title>
</head>


<body>
<span style="font-family: Verdana;">
<small><a href="../doc_traduc.php">Volver</a></small>

<hr style="width: 100%; height: 2px;">
<center>
<big>Cap�tulo 2: Instrucciones B�sicas</big>
<small>
<br><br>
Por Dark-N: <?php include ('../mailsolo.php'); ?>
<br>
</small>
<span style="font-family: Verdana;">
<small>
<a href="http://darknromhacking.com/">http://darknromhacking.com</a></small></span><small><br>
</small>
</center>
<hr style="width: 100%; height: 2px;"><br>
<small>

<b><font size=3 face="Verdana">Versiones</font></b><br>
<br><li>1.2:
<br>&nbsp;&nbsp;&nbsp;-Pasada a html. 
<br>&nbsp;&nbsp;&nbsp;-La rele� y mejor� explicaciones. Se agrega secci�n "Gusto a Poco". 
<br><li>1.1:
<br>&nbsp;&nbsp;&nbsp;-Revisado completamente sobre todo la sem�ntica de lo que trato de explicar.
<br>&nbsp;&nbsp;&nbsp;-Correg� lo que me dijeron en el Foro de Todotradus.

<br><li>1.0:
<br>&nbsp;&nbsp;&nbsp;-Versi�n original del tutorial.

<br><br><br><b><font size=3 face="Verdana">Introducci�n</font></b><br><br>
En este capitulo se ver�n las instrucciones <b>ASM</b> principales y las que m�s se repiten en un <b>Log</b> (Tracer.log) que arroja el <b>Snes9X trace</b>.

<br><br><br><b><font size=3 face="Verdana">Operaciones Binarias</font></b><br><br>

Existen 3 operaciones l�gicas: AND, OR y NOT, pero existe un operador compuesto llamado XOR, todos se aplican sobre varios bit, pero en la SNES generalmente es sobre 2 bits. 

<pre><font size="3">
   AND  (ambos bits deben ser 1 para que el resultado sea 1)
1 AND 1	 = 1	=> aqu� los 2 bits son 1 y 1
1 AND 0	 = 0
0 AND 1	 = 0  	=> aqu� los 2 bits son 0 y 1
0 AND 0	 = 0


   OR  (basta un 1 para que el resultado sea 1)
1 OR 1  = 1
1 OR 0  = 1
0 OR 1  = 1
0 OR 0  = 0


 NOT (es el negado de algo, el NOT de "algo" da como resultado lo contrario)
NOT 1  = 0
NOT 0  = 1


XOR (si todos son 1 o si todos son 0 entonces el resultado es 0)
1 OR 1	= 0
1 OR 0	= 1
0 OR 1	= 1
0 OR 0	= 0

Ejemplos:

    11011011       11101100          01101011
AND 01101000    OR 11001111      XOR 10101000
   ----------     ----------        ----------
    01001000       11101111          11000011</font></pre>

<br><b><font size=3 face="Verdana">Shifting (Rotaciones de D�gitos)</font></b><br><br>

Cualquier n�mero decimal se puede componer de varias sumas de otros n�meros que tienen una base. Tenemos varios base: 10 (decimal), 2(binario) o 8(octal). Por ejemplo el 12 en base 2 ser�a:

<pre><font size="3">12 = 1*2^3  + 1*2^2  + 0*2^1 + 0*2^0  = 8 + 4 + 0 + 0 = >  en binario es 1100

Ahora si un d�gito <b>Binario</b> lo invertimos o rotamos a la <b>izquierda</b> se <b>multiplica</b> por 2 ese n�mero:

20          *          2      =       40
010100  =>  rotar a la izquierda =>  101000 = 40 decimal


Y podemos rotar ese d�gito a la <b>derecha</b> para <b>dividir</b>.

20          /           2     =      10
010100  =>  rotar a la derecha =>   001010 = 10 decimal</font></pre>

Shifting se usa mucho para el dibujo de <b>fuentes</b>, ya que permite crear las <b>sombras</b> de las letras. Para m�s informaci�n mira este 
<a href="articulos_fortaleza/shifting.htm">post</a> que hice en <b>Fortaleza Romhack</b> cuando el foro estaba hospedado en Nekein.

<br><br><br><b><font size=3 face="Verdana">Cargas y Almacenamientos</font></b><br><br>

Todo se resume en meter datos a la memoria y sacar datos desde la memoria. 
<br>En la snes un <b>valor</b> es un d�gito <b>hexadecimal</b> (hex abreviado) y se representa como <b>#$</b>Valor o <b>0x</b>Valor, por ejemplo #$AC, #$123, 0x12CA, 0xA, etc. <br>
Otra forma sencilla de escribir un valor es usar <b>hex</b> o una <b>h</b> por ejemplo: BC hex o 0Ah. Esta forma sencilla solo es aplicable para ense�arles, no se usa en los LOG ni al programar en asm.
<br>Una <b>direcci�n</b> en la snes igual es hexadecimal y se representa con <b>$</b>Direcci�n, por ejemplo la direcci�n de 16 bits $1238, o esta de 8 bits $FF.<br><br>

<b>LDA</b><br>
Carga al acumulador un valor de 8/16 bits.<br><br>

Ejemplos:<br>
LDA  $05 ; carga acumulador con el valor que est� dentro de la direcci�n $05.<br>
LDA #$AC ; carga acumulador con el valor AC hex.<br>
LDA $CCE602, X  ; carga el valor que esta en la direcci�n  [CCE602 +X], X es un �ndice que se puede incrementar as� podemos sacar valores desde $CCE602, $CCE603, $CCE604, etc. <br><br>

<b>LDX, LDY</b><br>
Carga al �ndice X/Y un valor de 8/16 bits.<br><br>

Ejemplos:<br>
LDX #$AC ; carga X con el valor AC hex.<br>
LDX $AC	 ; carga X con el valor que esta dentro de la direcci�n AC.<br><br>

<b>STA, STX, STY</b><br>
Almacena en una direcci�n de memoria un valor dado por A, X o Y.<br><br>

Ejemplos:<br>
STA $3AFF ; guarda lo que est� en A en la direcci�n $3AFF.<br>
STX $BB   ; guarda lo que est� en X en la direcci�n $00BB.

<br><br><br><b><font size=3 face="Verdana">Modificaciones del ancho de bit</font></b><br><br>

Se puede cambiar el ancho de bits desde <b>8</b> a <b>16</b> bits y viceversa del Acumulador e �ndices. Esto sirve para muchas cosas, que lo veremos luego, pero como ejemplo pr�ctico, si queremos meter el valor 0xCA en el acumulador, conviene m�s tener un acumulador definido con un ancho de 8 bits (ya que 0xCA es de 8 bits), si A esta seteado con un ancho de 16 bits esta bien pero no es tan adecuado. 
<br>Otro ejemplo ser�a si queremos meter en A un 0x123D conviene tener un ancho de 16 bits.<br><br>
Ejemplos:<br>
REP #$20 ; Hacemos que el acumulador est� en modo de 16 bits (puede almacenar valores de 16 bits)<br>
REP #$30 ; pone los �ndices X e Y en modo 16 bits.<br>
SEP #$30 ; X,Y,A los dejamos en modo de 8 bit.<br>
SEP #$20 ; Acumulador lo dejamos en modo de 8 bit.


<br><br><br><b><font size=3 face="Verdana">Incrementos/Decrementos</font></b><br><br>

Para <b>decrementar</b> o <b>aumentar</b> en <b>1</b> el Acumulador o el �ndice X/Y tenemos 2 instrucciones b�sicas: <b>DEC</b> que resta 1 y <b>INC</b> que suma 1. <br>Si conoces PHP, JAVA o C#, DEC es igual a i-- o el i=i-1 de C++ y INC es como el i++ o i=i+1.

<pre><font size="3">Ejemplos:
INC 	 ; incremento el valor del Acumulador en 1hex.
INC A	 ; incremento el valor del Acumulador en 1hex (da lo mismo que poner INC).
DEC X	 ; decremento el valor del registro X en 1hex.
DEX 	 ; decrementa el registro X en 1hex.
DEY 	 ; decrementa el registro X en 1hex.
INX 	 ; incrementa el registro X en 1hex.
INY 	 ; incrementa el registro Y en 1hex.
DEC $Dir ; decrementa lo que haya dentro de la direcci�n Dir en 1.
INC $Dir ; incrementa lo que haya dentro de la direcci�n Dir en 1.

C�digo comentado 1:
Si A=0x123C
INC   ; A valdr� 0x123D
DEC
DEC   ; aqu� A valdr�a 0x123B

C�digo comentado 2:
$1234: 0xBC ; dentro de $1234 esta el valor 0xBC
INC $1234   ; $1234 contiene ahora un 0xBD</font></pre>

<br><br><b><font size=3 face="Verdana">Saltos y Ciclos</font></b><br><br>

Con algunas instrucciones podemos saltar a un cierto <b>label</b> o direcci�n. Tambi�n puedo emular un t�pico <b>IF-Then</b> o incluso un ciclo <b>for</b> ya que puedo comparar y luego saltar. 
<br>En general tenemos 2 tipos de salto: <br><br>
<li>1. Instrucciones de Salto sin condici�n: <b>JSR</b> que salta a una subrutina, <b>JSL</b> que salta a una direcci�n o label.

<pre><font size="3">Ejemplo 1: uso del JSR

  LDA #$00
hola   ; esto es un Label
  INC A
  JSR hola    

Este ciclo hace que se carge A=0x00, luego se incremente a A=01, luego vaya a "hola", 
es decir que ejecute de nuevo INC (A=0x02), y as� sucesivamente, A llegar� a 0xFF si 
esta en modo de 8 bits o a 0xFFFF si esta en modo de 16 bits.

Ejemplo 2: uso de RTS

STA $05
JSR Hola2	; salta a Hola2 sin condici�n
LDA #$10

Hola2
  LDA #$12	; cargamos A=12h   
  RTS 		; retorna de la subrutina (sale de ella) y llega a la instrucci�n que sigue a 
                ; JSR que la llamo, es decir que al hacer RTS ejecuta LDA #$10.</font></pre>


<li>2. Instrucciones de Salto con condici�n o con comparaci�n: las que permiten simular un if-else o ciclos for/while. 
<br><br>Hay que tener claro el tema de los <b>Flag C y Z del registro P</b> que se vio en el cap�tulo anterior.
<br>Aqu� 9 tipos de saltos que dependen de los flag Z, C y V:<br><br>

<table><tr><font size="3"><td bgcolor="#CCCCCC">
<b>BRA</b> (branch Always)<br>
<b>BEQ</b> (branch on equal)<br>
<b>BNE</b> (branch on not equal)<br>
<b>BCC</b> (branch on carry clear)<br>
<b>BCS</b> (branch on carry set)<br>	
<b>BVC</b> (branch on overflow clear)<br>
<b>BVS</b> (branch on overflow set)</td><td bgcolor="#CCCCCC">
Branchea (salta) siempre. <br>
Branchea si el flag z est� a 1<br>
Branchea si el flag z est� a 0<br>
Branchea si el flag c est� a 0<br>
Branchea si el flag c est� a 1<br>
Branchea si el flag v est� a 0<br>
Branchea si el flag v est� a 1</td></tr>
<tr><td bgcolor="#CCCCCC">
-<b>BPL</b> es igual a BCS<br>
-<b>BMI</b> es igual a BCC
</td></font></tr></table>

<br>Si no sabes que es C, recuerda el Capitulo 1 del curso.<br>
Para aprender a hacer una condici�n if-else, debes aprender a comparar. Las 6 comparaciones que b�sicas son:

<table><tr><font size="3"><td bgcolor="#CCCCCC">
<b>CMP #$valor</b> compara valor con Acumulador<br>
<b>CMP $Dir</b> compara lo que hay en la direcci�n con Acumulador<br>
<b>CPX #$valor</b> compara el valor con el registro X<br>
<b>CPY #$valor</b> compara el valor con el registro Y<br>
<b>CPX $Dir</b> compara lo que hay en la direcci�n con el registro X<br>
<b>CPY $Dir</b> compara lo que hay en la direcci�n con el registro Y<br>

</td></font></tr></table>

<br>Adem�s los CMP/CPX/CPY pueden <b>modificar el flag Carry (C) o Zero (Z)</b> de esta forma:<br><br>

<table><tr><font size="3"><td bgcolor="#CCCCCC">
Si el operando es <b>mayor</b> a A/X/Y pone el flag <b>c a 0</b> y el flag <b>z a 0</b><br>
Si el operando es <b>menor</b> a A/X/Y pone el flag <b>c a 1</b> y el flag <b>z a 0</b><br>
Si el operando es <b>igual</b> a A/X/Y pone el flag <b>z a 1</b><br>
Y el flag v (overflow) se activa cuando una operaci�n hace desbordamiento en su resultado. Ver cap�tulo 1.
</td></font></tr></table>

<pre><font size="3">Ejemplo 1: uso del CMP

CMP $CCE600 	; compara A con lo que hay en $CC:E600, esta instrucci�n siempre va seguida de 
		; un de salto, as� si la comparaci�n se cumple salta a otra direcci�n (nos 
		; salemos de esta rutina y vamos a otra), si no se cumple la comparaci�n, sigue leyendo.

Ejemplo 2: CPX con BNE

   LDX #$0800	; cargamos X con 0800h
bucle:
   LDA $7EC800,x  ;cargamos a con lo que est� en la direcci�n $7EC800 + $X=$7ED000
   STA $7F0000,x  ;ese valor se guarda en la direcci�n $7F0000+$800=$7F0800
   DEX		  ;X=X-1, es decir, X = 0800-1 = 07FFh
   CPX #$0000	  ;se compara con X con 0000h �son iguales? No. �El operando es MENOR a X? Si. Entonces el Flag c=1 y Flag z=0.
   BNE bucle	  ;como carry=0, BCC salta a "bucle". Entonces sube y hace el ciclo de nuevo.
   ...		  ;Pero ahora X=07FF y as� sigue haciendo el ciclo hasta que X sea 0000. Luego se pregunta 
		  ;�X es igual a 0000h? Si. El Flag Z cambia a 1. BNE salta si el Flag Z=0, por lo tanto no salta y sigue con ....
		  ;En resumen se copiaron 800 datos desde la <b>RAM</b> (ya sabes seg�n en al capitulo anterior, 
                  ;que si se habla de $7EXXXX o $7FXXXX es que accedimos a la RAM) y se dejan en
                  ;posici�n RAM de $7F0000 en adelante.

</font></pre>

<br><b><font size=3 face="Verdana">Transferencia</font></b><br><br>

Estas instrucciones sirven para copiar el contenido de un registro a otro. 
<br>Las transferencias mas conocidas son: <br><br>

TAX ; se transfiere (es lo mismo que "copiar") el valor de A a X.<br>
TAY ; se transfiere el valor de A a Y.<br>
TCD ; se transfiere el valor de A al registro DP.<br>
TXS ; Transfiere de X a la Pila.<br>
TXY ; Transfiere de X a Y.<br>
TYX ; Transfiere de Y a X.<br>
TYA ; Transfiere de Y al acumulador.<br>
TXA ; Transfiere de X al acumulador.

<br><br><hr style="width: 100%; height: 2px;"><br>
<b>Gusto a Poco</b>

<br><br>1. Si tenemos
<br>LDA #$0000
<br>BEQ $1234
<br>BNE $4321
<br>BRA $0001

<br><br>�Salta a $1234, a $4321 o a $0001?

<br><br>R: Salta a $1234 ya que LDA #$0000 pone Z=1 y BEQ salta si Z=1.

<br><br><br>2. �Con cuanto queda Y?<br><br>
Direc / Instrucci�n<br>
------ ----------<br>
$0998: LDY #$0000<br>
$1000: LDA #$0002<br>
$1002: INC<br>
$1003: INY<br>
$1004: CMP #$0A<br>
$1006: BEQ $2000<br>
$1008: BRA $1002<br>
$1010: DEC<br>
...<br>
$2000: DEC<br><br>

R: Es un ciclo que aumenta A e Y hasta que A sea igual a 0Ah. A parte con 0002h, y hasta 0Ah se le suman 8hex. El registro Y cuenta cada iteraci�n por lo que Y=8. 

<br><br><hr style="width: 100%; height: 2px;"><br>
<b><font size=3 face="Verdana">Referencia R�pida de Instrucciones</font></b><br><br>
Aqu� est� la mayor�a de las instrucciones en Ingles claro, no es necesario aprenderlas todas, pero es para que empiecen a acostumbrarse.

<pre><font size="3">ADC - add with carry
AND - logical AND
BCC - branch if carry clear
BCS - branch if carry set
BEQ - branch if equal
BIT - bit test
BMI - branch if minus
BNE - branch if not equal
BPL - branch if plus
BRA - branch always
BRK - break point instruction
BVC - branch if overflow clear
BVS - branch if overflow set
CLC - clear the carry flag
CLD - clear the decimal flag
CLI - clear the interrupt flag
CLP - clear bits in P
CLR - store a zero into memory
CMP - compare accumulator
CPX - compare x register
CPY - compare y register
CSP - call system procedure
DEC - decrement acc or memory
DEX - decrement x register
DEY - decrement y register
EOR - exclusive-or accumulator
HLT - halt (stop) the clock
INC - increment acc or memory
INX - increment x register
INY - increment y register
JMP - jump to new location
JSR - jump to subroutine
LDA - load accumulator
LDX - load x register
LDY - load y register
MVN - block move (decrement)
MVP - block move (increment)
NOP - no operation
ORA - logical or accumulator
PHA - push accumulator
PHP - push p
PHX - push x register
PHY - push y register
PLA - pop accumulator
PLP - pop p
PLX - pop x register
PLY - pop y register
PSH - push operand
PUL - pop operand
RET - return from subroutine
ROL - rotate left acc/mem
ROR - rotate right acc/mem
RTI - return from interrupt
RTL - return from long subroutine
RTS - return from short subroutine
SBC - subtract with carry
SED - set decimal flag
SEI - set interrupt flag
SEP - set bits in P
SHL - shift left acc/mem
SHR - shift right acc/mem
STA - store accumulator
STX - store x register
STY - store y register
SWA - swap accumulator halves
TAD - transfer acc to D
TAS - transfer acc to S
TAX - transfer acc to x
TAY - transfer acc to y
TCB - test and clear bit
TDA - transfer D to acc
TSA - transfer S to acc
TSB - test and set bit
TSX - transfer S to X
TXA - transfer x to acc
TXS - transfer x to S
TXY - transfer x to y
TYA - transfer y to acc
TYX - transfer y to x
WAI - wait for interrupt
XCE - exchange carry with emulation bit</font></pre>

<hr style="width: 100%; height: 2px;">

<a href="../doc_traduc.php">Volver</a>
<br>
<font color="#FFFFFF">
<?php 
include '../disq.php';
?>
</font>
<center><?php include ('../pie.php'); ?></center>
</small>
</body>
</html>

