<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>La Web de Dark-N - Hacking de Sprites en la SNES</title>
</head>

<body>
<small>
<span style="font-family: Verdana;">
<big>
<span style="font-weight: bold;"></span>
</big>
<a href="../doc_traduc.php">Volver</a>
<hr style="width: 100%; height: 2px;">

<big>
<span style="font-weight: bold;">Edici&oacute;n de Sprites en la SNES: cuando el texto no aparece por ninguna parte</span>
</big>
<br style="font-weight: bold;">
<br>por Dark-N <?php include ('../mailsolo.php'); ?><br>
<a href="https://darknromhacking.com/">https://darknromhacking.com</a>
<br>Update: 04-03-2021<br>
<hr style="width: 100%; height: 2px;"><span style="font-family: Verdana;">

<br><b>La Idea</b><br><br>

Muchas veces hemos intentado buscar un texto en el editor Hex y este no aparece por ninguna parte, por lo que las posibilidades son:
<br><br>
<li> El texto est&aacute; <b>comprimido</b>: esta es la peor de los casos, hay que identificar cual es el m&eacute;todo o algoritmo de compresi&oacute;n, LZSS u otro, encontrar la rutina que descomprime y editarla, y otras cosas, que no competen en este tutorial. </li>
<li> El texto est&aacute; en formato de <b>Tiles</b>: hay que encontrar la Tabla de Tiles, para esto, puede ver un antiguo tutorial que hice al respecto, usando el juego <a href="YsIII/YsIII.php">Ys'III (SNES)</a>.</li>
<li> El texto est&aacute; en formato de <b>Sprites</b>: con esta &uacute;ltima posibilidad es la que nos basamos para explicar este tutorial.</li>

<br><br><b>Asegur&aacute;ndonos que el texto no est&aacute; en la ROM en formato ASCII</b><br><br>

Para comprobar que el texto no est&aacute; en ninguna parte en la ROM, como ASCII al menos, si buscamos las palabras "START", u "OPTIONS" que aparecen en el men&uacute; principal, estas no aparecen por ninguna parte en el editor hex, en este caso <b>Translhextion</b> buscando en modo "SCAN Relative", tampoco aparecen si las buscamos en min&uacute;sculas:

<br><br><img src="img/AddamsMenu.PNG" width="300" eight="260">
<br><br><img src="img/AddamsMenu_START_NoEncontrado.PNG" width="500" eight="460">

<br><br><br><b>Herramientas</b><br><br>

<li>La Rom en Ingl&eacute;s. La pueden obtener de <a href="http://www.coolrom.com">CoolRoms</a></li>
<li>Emulador ZSNES para obtener un salvada ZST: <a href="../archivos/zsnesw151-debug.rar">ZSnes 1.51 Debugger</a></li>
<li>Emulador SNES9X con Debugger: <a href="../archivos/snes9x1.43.ep9r8_debugger.zip">Snes9X 1.43 Debugger</a></li>
<li>Editor Hexadecimal: <a href="../archivos/translhextion16c.zip">Translhextion</a></li>
<li>Editor de Tiles: <a href="../archivos/tlp1.1.rar">Tile Layer Pro 1.1</a></li>
<li>Visor de ZST: <a href="http://vsnes.aep-emu.de/">vSNES</a></li>
<li>Conversor de Direcciones SNES/Hex: <a href="../archivos/hex2snes.rar">Hex2Snes</a></li>


<br><br><b>Buscando la Tabla de Sprites</b><br><br><br>

<u>Depurando para obtener la direcci&oacute;n de la ROM desde donde se copian los datos por DMA a la VRAM</u><br><br>

Lo primero es recordar que DMA es un t&eacute;cnica donde se utilizan ciertos registros de la SNES, llamados registros DMA, para copiar datos la ROM a VRAM directamente sin pasar por la RAM. Para nosotros como romhackers, tiene la ventaja de que los datos copiados, no est&aacute;n comprimidos, ya que para que los datos como textos o tiles est&eacute;n comprimdias, necesariamente se utiliza la RAM.<br><br>

- Lo primero es ejecutar la ROM con el Debugger y cuando llegues pasadito del punto que indica la imagen inferior (justo la imagen se pone negra):
<br><br><img src="img/AddamsAntesDebug.PNG" width="300" eight="260"><br><br>

Justo en la imagen negra, debes activar el check <b>DMA</b> de la secci&oacute;n <b>Special Tracing</b>:
<br><br><img src="img/AddamsDebug.PNG"><br><br>

Y ver&aacute;s mas o menos en la l&iacute;nea 10:<b>DMA[7]: write Mode: 1 0x95ECDF->0x2118 Bytes: 8192 (inc) V-Line:8 VRAM: 6000 (1,0) word</b><br>
esto es un paso de bytes por medio de los registros <b>DMA</b>, esto quiere decir, pasar desde la ROM a VRAM directamente sin pasar por la RAM.

<br><br>por lo tanto, la salida de esta parte es la Direcci&oacute;n SNES de los datos: <b>0x95ECDF</b>.

<br><br><br>
<u>Obteniendo la Direcci&oacute;n Hex de los datos de la ROM a partir de la direcci&oacute;n SNES</u>

<br><br>Con tal de buscar en un Editor Hex o de Tiles los datos obtenidos, debemos convertir a Direcci&oacute;n Hex la direcci&oacute;n anterior, para esto usamos el <b>Hex2Snes</b>:

<br><br><img src="img/AddamsHex2Snes.PNG" width="280" eight="400"><br><br>

Por lo tanto, la salida de esta parte es la Direcci&oacute;n Hexadecimal de los datos en la ROM: <b>0x0AECDF</b>.

<br><br><br>
<u>Buscando los Sprites (no tiles) en la ROM con un Editor de Tiles en caso que querramos editarlos</u>
<br><br>Con la direcci&oacute;n anterior podemos buscar los Sprites con el <b>Tile Layer Pro</b>:

<br><br><img src="img/AddamsSpriteEnRom.PNG" width="350" eight="300"><br><br>

Como ya tenemos ubicados los sprites (no confundir con las tiles del texto del juego, que est&aacute; en otro lado de la ROM), podemos editarlos si deseamos y agregar s&iacute;mbolos especiales. En nuestro caso tenemos los textos:
<br>
<br>
START -> INICIO<br>
OPTIONS -> OPCIONES<br>
PASSWORD -> se deja igual<br>

<br>por lo que no requerimos s&iacute;mbolos especiales.


<br><br><br>
<u>Obteniendo una salvada .ZSX con emulador ZSNES para verificar que est&eacute;n los Sprites</u>
<br><br>
Una archivo de salvada del ZSNES contiene una copia exacta de lo que hay en la memoria <b>VRAM</b> de la SNES justo en ese momento. 
<br> Entonces, cuando la pantalla muestre:
<br><br><img src="img/AddamsMenu.PNG" width="300" eight="260"><br><br>

Presiona <b>F2</b> en el emulador, se generar&aacute; un archivo ZS1, por ejemplo "Addams3 (esp).zs1".
<br>
Luego si editas ese archivo con el <b>Tile Layer Pro</b> y vas a la direcci&oacute;n <b>0x2CC13</b> te mostrar&aacute; los Sprites en la VRAM.

<br><br><img src="img/AddamsSpriteEnZST.PNG" width="360" eight="320"><br><br>


<br><br>
<u>Abriendo la Salvada con el VSNES para ver detalles de los Sprites</u>
<br><br>

Si abrimos el archivo ZS1 con el programa VSNES, este nos entrega much&iacute;sima informaci&oacute;n de la Tiles, Background, Sprites, SPC (sonido), etc. del momento en que se presion&oacute; F2 en el juego.
<br>
Primero se debe abrir el ZST con el &iacute;cono de carpeta de la izquierda, y luego ir a <b>SceneViewer</b>

<br><br><img src="img/AddamsVSNESMenu.PNG" width="500" eight="240"><br>
<br>Si seleccionas <b>Layers</b> que significa en ingl&eacute;s "capas", y luego <b>Sprites</b>, se nos ver&aacute;n los famosos sprites que tanto buscamos:<br>
<br><img src="img/AddamsVSNESSprites.PNG" width="500" eight="440"><br><br>

La clave est&aacute; aqu&iacute;: se muestra que para el Sprite <b>S</b> de la palabra START, tiene una posici&oacute;n X de <b>108</b> y una posici&oacute;n Y <b>184</b> y el n&uacute;mero o &iacute;ndice del tile es el <b>22</b>. Por lo tanto si convertimos esos valores a hexadecimal nos dan:
<br>
<br>
108 dec = 0x6C hex<br>
184 dec = 0xB8 hex<br>
22 dec  = 0x16 hex<br>

<br>Es decir, tenemos estos 3 bytes del Sprite 'S': <b>6CB816</b>.
<br>


<br><br>
<u>Entendiendo lo b&aacute;sico de los Sprites en al SNES</u>
<br><br>

Ya que tenemos estos 3 bytes <b>6C B8 16</b>, podemos investigar un poco acerca de como se almacenan los sprites en la ROM y para eso miramos el excelente documento <a href="../archivos/QwertieSNESDocumentation(ingles).doc">Qwertie's SNES Documentation</a> (por David Piepgrass) o el buen Tutorial en espa&ntilde;ol de Sprites en la SNES (escrito por David Senavre) que est&aacute; en <a href="http://www.consolasparasiempre.net/">http://www.consolasparasiempre.net/</a> donde podemos resumir que:<br><br>
<li>Los sprites en SNES se llaman objetos o OAM.</li>
<li>Para manejar dichos Sprites, la SNES tiene la llamada "tabla OAM" que es una zona de memoria que tiene toda la informaci&oacute;n sobre todos los sprites.</li>
<li>En esta tabla solo pueden haber 128 sprites, y por lo tanto la SNES puede manejar un m&aacute;ximo de 128 sprites a la vez. La NES solo puede manejar 64.</li>
<li>Si se utilizan muchos Sprites a la vez en un juego, lo m&aacute;s seguro es que se vea lento.</li>
<li>Cada Sprite de la tabla OAM es de 4 bytes que se forman as&iacute;:</li><br>

Byte 1 : xxxxxxxx		Posici&oacute;n X del sprite en la pantalla (0-255)<br>
Byte 2 : yyyyyyyy		Posici&oacute;n Y del sprite en la pantalla (0-255)<br>
Byte 3 : tttttttt		N&uacute;mero de tile que representa al sprite</br>
Byte 4 : vhoopppt<br><br>

De este ultimo byte: <br>
v : mirroring vertical<br>
h : mirroring horizontal<br>
o : bits de prioridad<br>
p : bits de paleta<br>
t : bit alto de n&uacute;mero de tile<br>

<br>
Si aplicamos esta estructura a nuestros 3 bytes <b>6CB816</b> tenemos el significado de los 3 bytes y coincide claramente con lo que nos dice el vSNES:
<br><br>
-6C = Posici&oacute;n X = 108<br>
-B8 = Posici&oacute;n Y = 184<br>
-16 = Numero de Tile = 22<br>

<br><br><b>Edici&oacute;n de Tabla OAM de prueba</b><br><br>

Vamos buscar nuestros 3 bytes del Sprite "S" en la ROM y los encontramos en la posici&oacute;n <b>0xAEAB7</b>, para esto abres la ROM en Translhextion, luegp te posicionas al comienzo del archivo y buscas con la selecci&oacute;n <b>Hex</b>:<br><br>
<img src="img/Addams6CB816.png"><br><br>

<img src="img/AddamsSpriteSROM.PNG"><br><br>

Como dato aparte, si buscamos en el archivo de salvada ZST igualmente se encuentra all&iacute;:<br><br>

<img src="img/AddamsSpriteSZST.PNG"><br><br>

Si modificamos el n&uacute;mero de Tile tenemos (0x16) por un 0x0 nos deber&iacute;a mostrar otra letra en vez de la "S":<br><br>

<img src="img/AddamsCambioSprite16por0.PNG"><br><br>
<img src="img/AddamsCambioSprite16por0Juego.PNG"><br><br>
Se ve una "ATART" �Lo tenemos!

<br><br><b>Edici&oacute;n de Tabla OAM (en serio)</b><br><br>

Ya que vimos que funciona, ahora vamos a colocar las palabras traducidas al Espa&ntilde;ol:<br><br>
<li>START -> INICIO -> I = 08, N = 0D, I = 08, C = 02, I = 08, 0 = 0E. NOTA: esta &uacute;ltima la letra "0" de INICIO no alcanza exacto con START, por lo que la agregaremos a la tabla OAM al final, en donde hayan tiles vac&iacute;as.</li>
<li>OPTIONS -> OPCIONES -> O y P quedan igual, T se cambia por C = 02, I, O y N quedan igual, S se cambia por E. NOTA: esta &uacute;ltima la letra "S" de OPCIONES tampoco alcanza exacto con OPTIONS, por lo que la agregaremos a la tabla OAM al final, en donde hayan tiles vac&iacute;as.</li>
<li>PASSWORD -> lo dejaremos igual</li>

<br>
<img src="img/AddamsSpriteHexFinal.PNG"><br>
NOTA: en la imagen superior se ven posiciones X de cada tile que cambiaron al final cuando ajustaba que el men&uacute; se viera centrado, pero el las posiciones Y y el n&uacute;mero de tile, son los mismos de la imagen.<br>

<br>Como se ve arriba, aparte de editar que tile del sprite se vea en que posici&oacute;n, movimos los sprites a la izquierda (cambiando el byte que maneja la posici&oacute;n X del Sprite) ya que est&aacute;n muy movidos por las nuevas letras, de esta forma se ve m&aacute;s centrado.

<br><br>
Finalmente, nuestro romhack quedar&iacute;a as&iacute;:<br><br>

<img src="img/AddamsSpriteFinal.PNG"><br><br>

NOTA: Al seleccionar con el dedo INICIO o OPCIONES, las nuevas letras que agregamos (O de INICIO y S de OPCIONES) no parpadean, si el resto de la palabra, por lo que quedar&iacute;a ese detalle por resolver a futuro.

<br><br>
<hr>

<a href="../doc_traduc.php">Volver</a>
<br>
<font color="#FFFFFF">
<?php 
include '../disq.php';
?>
</font>
<center><?php include ('../pie.php'); ?></center>
</small>
</body>
</html>
