<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>La Web de Dark-N - Curso ASM SNES. Cap�tulo 1: Elementos B�sicos de la SNES</title>
</head>


<body>
<span style="font-family: Verdana;">
<small>
<a href="../doc_traduc.php">Volver</a>
</small>

<hr style="width: 100%; height: 2px;">
<center>
<big>Cap�tulo 1: Elementos B�sicos de la SNES</big>
<small>
<br><br>
Por Dark-N: <?php include ('../mailsolo.php'); ?>
<br><a href="http://darknromhacking.com/">http://darknromhacking.com</a><br>
</small>
</center>
<hr style="width: 100%; height: 2px;"><br>
<small>

<b><font size=3 face="Verdana">Versiones</font></b><br>
<br><li>1.4:
<br>&nbsp;&nbsp;&nbsp;-Se agrega explicaci�n del registro P necesaria para los pr�ximos cap�tulos
<br><li>1.3:
<br>&nbsp;&nbsp;&nbsp;-Versi�n revisada por Kaosoft y pasada a html.
<br><li>1.0:
<br>&nbsp;&nbsp;&nbsp;-Versi�n original del tutorial en .doc.

<br><br><br><b><font size=3 face="Verdana">Introducci�n</font></b><br><br>
Esta gu�a la hab�a estado pensando hacer desde hace mucho tiempo. Si no es por Magno que me ha ayudado en todo, no hubiese sido posible hacerla. Lo mismo que a Jonas. �Gracias a los dos!<br><br>
En este primer cap�tulo se estudiar�n los elementos b�sicos de la SNES. Pero partamos con una pregunta b�sica:<br><br>
 
�Qu� es el <b>65816</b>?<br><br>

Es un <b>microprocesador</b> hecho por <b>Western Design</b>, el cual es b�sicamente una actualizaci�n de <b>16 bits</b> del conocido microprocesador <b>6502</b> (usado en la NES y en las viejas computadoras como el Commodore 64, etc.) y se usa en los sistemas Apple IIgs. 
<br>El 65816 es el usado por la SNES/Super Famicom. El procesador tiene registros internos de 16 bits y bus de datos de 8 bits.<br><br>

�Que es la <b>Super Famicom</b>?<br><br>

Es la versi�n japonesa de la SNES (Super Nintendo Entertainment System) americana. El hardware en s� es el mismo y los cartuchos de SF (con una peque�a modificaci�n externa) se pueden jugar en la SNES y viceversa. Lo que las diferencia principalmente es la apariencia.


<br><br><br><b><font size=3 face="Verdana">Elementos B�sicos de la SNES</font></b><br><br>

Dentro de la SNES tenemos los siguientes componentes:<br><br>
<li>Una CPU con <b>Registros Internos</b>:<br><br>
&nbsp;&nbsp;&nbsp;-A: Acumulador<br>
&nbsp;&nbsp;&nbsp;-X: �ndice X<br>
&nbsp;&nbsp;&nbsp;-Y: �ndice Y<br>
&nbsp;&nbsp;&nbsp;-D: Registro de P�gina Directa<br>
&nbsp;&nbsp;&nbsp;-S: Stack Register (Registro de Pila)<br>
&nbsp;&nbsp;&nbsp;-PBR: Program Bank Register (Banco de Programa)<br>
&nbsp;&nbsp;&nbsp;-DBR: Data Bank Register (Banco de Programa)<br>
&nbsp;&nbsp;&nbsp;-PC: Contador de Programa<br>
&nbsp;&nbsp;&nbsp;-P: Status Register (Registro de Banderas/Registro de Estado)<br><br>
       
<li>Una <b>Memoria</b> que se subdivide en: <br><br>
&nbsp;&nbsp;&nbsp;1. <b>Memoria ROM</b>: aqu� esta el <b>c�digo</b> del juego, y los <b>datos iniciales</b> como Tiles (comprimidas o no), m�sica, fonts, etc.<br>
&nbsp;&nbsp;&nbsp;2. <b>Memoria RAM</b>: aqu� se guardan <b>datos temporales</b> como tiles o fonts que se descomprimen en <b>tiempo de ejecuci�n</b> (runtime) si la rom esta comprimida.<br>
&nbsp;&nbsp;&nbsp;3. <b>Memoria de Video o VRAM</b>: se usa para <b>mostrar en pantalla</b> las tiles, font, maps, sprites, etc.<br><br>

NOTA: El orden 1,2,3 es el que siguen los datos para ser mostrados en pantalla.<br><br>

<li><b>Registros Externos</b> al Procesador: Son puertos mapeados de memoria fijos de Entrada/Salida usados para enviar datos al <b>PPU</b>. Estos registros est�n asociados a ciertos bancos y direcciones, es decir, de cierta direcci�n de la memoria principal a otra. Por ejemplo: El registro de los controles o joypad ocupa de la direcci�n $4000 a la $41FF. <br><br>

Los registros externos son:
<br>&nbsp;&nbsp;&nbsp;-Registros OAM (Object Attribute Memory): Informaci�n de sprites en la pantalla; elige una ubicaci�n en VRAM donde el car�cter y la medida de los sprites en la pantalla son guardados.
<br>&nbsp;&nbsp;&nbsp;-Registros de Color.
<br>&nbsp;&nbsp;&nbsp;-Registros de Transferencia/Direcci�n de VRAM.
<br>&nbsp;&nbsp;&nbsp;-Registros de Video: Controla el tama�o de los tiles de background.
<br>&nbsp;&nbsp;&nbsp;-Registros Contadores/IRQ/VBL/NMI.
<br>&nbsp;&nbsp;&nbsp;-Registros Windowing: Usados para cortar porciones de pantalla, por ejemplo en el Mario World cuando aparece el c�rculo de entrada al empezar a jugar.
<br>&nbsp;&nbsp;&nbsp;-Registros de Joypad/Joystick: Ocupados para manejar los controles, setear n�mero de ellos y los botones.
<br>&nbsp;&nbsp;&nbsp;-Registros DMA (Direct Memory Access): usados para copiar datos como tiles, de ROM a la VRAM (la pantalla) de forma "r�pida" sin pasar por RAM.<br><br>

NOTA: Por ahora no hay que entender estos complicados registros, los explicar� en su debido momento.


<br><br><br><b><font size=3 face="Verdana">Registros Internos: A, X/Y y P</font></b><br><br>

Estos 4 registros son los esenciales para trabajar con asm al hackear una rom.<br><br>

<b>A: Registro Acumulador</b><br><br> 

Es el acumulador se usa para c�lculo y almacenamiento general. El acumulador puede estar en modo de 8 � 16 bits. Si est� en 8 bits -1 byte-, s�lo puede contener valores de 0 a 255 (00 a FF hex) y en 16 bits -2 bytes- valores de 0 a 65535 (0000 a FFFF hex). <br><br> 

Para saber si el acumulador est� en 8 o en 16 bits, hay que chequear el bit n� 5 o "m" del registro de banderas P (este registro se ver� con m�s detalle en otro cap�tulo). Si el bit 5 llamado m est� seteado (es decir m = 1), el acumulador est� en 8 bits y si el bit 5 est� re-seteado (m = 0) el acumulador est� en 16 bits. <br><br> 


Ejemplos:<br> 

LDA #$AC ; carga A con el valor AC hex. Los valores en ASM SNES se escriben con <b>#$</b>VALOR.<br>
LDA #$25 ; carga A con el valor 25 hex. <br>
LDA $1234 ; carga A con el valor que se encuentre dentro de la direcci�n $1234. Las direcciones en ASM SNES se escriben con <b>$</b>DIRECCION.<br>
STA $0ABC ; mete lo de A dentro de la direcci�n $0ABC.<br><br>


<b>X,Y: Registros �ndices</b><br><br>

Los registros X y Y est�n asociados al acumulador. Puedes guardar valores dentro de estos registros y luego realizar operaciones matem�ticas.<br>
Estos registros son usados para indexar localizaciones de memoria. �C�mo es eso? Si se quiere leer de una direcci�n de memoria y de all� para abajo (se desea recorrer para revisar cada contenido), se necesita un �ndice que se vaya aumentando. Es como recorrer un arreglo (si no saben que es un arreglo, revisen documentaci�n de casi cualquier lenguaje de alto nivel como C/C++/PHP/VB6 o vena en google alg�n manual) .<br><br>

Estos dos registros tambi�n pueden ser de 8 � 16 bits, dependiendo del flag (bandera) X del registro P. Si X = 1, entonces los registros (X y Y) estar�n en modo de 8 bits. Si X = 0, est�n en 16 bits. <br><br>
LDX #$AC ; carga X con el valor AC hex.<br>
LDY #$20 ; carga Y con el valor 20 hex.<br>
LDY $10 ; carga en Y lo que hay en la direcci�n $10.<br>
LDA $1234, Y ; caso t�pico del uso del �ndice Y, ya que con esto cargamos en A lo que haya en la direcci�n $1234+Y, si Y vale 8 por ejemplo, cargamos en A lo que haya en $1234+$8 = $123C. <br><br>

<b>Sacando Datos de la Memoria</b><br><br>

El acumulador o los �ndices se pueden llenar con valores de alguna direcci�n. Por ejemplo:<br><br>

LDA $30 ; carga el acumulador con el valor que est� dentro de la direcci�n 30 hex (las direcciones de la SNES siempre est�n en hexadecimal, as� que de ahora en adelante, si hablo de direcci�n, es en HEX). <br><br> 

LDX $4A ; carga X con el valor que est� dentro de la direcci�n $4A.<br><br>
 

<b>Introduciendo Datos a la Memoria</b><br><br> 

Se puede llenar una casilla de memoria con los valores del acumulador o de los �ndices.<br><br> 

STA $3AFF ; guarda lo que est� en A en la direcci�n 3AFF.<br>
STY $05C0 ; guarda lo que est� en Y en la direcci�n 05C0.<br>
STX $BB ; guarda lo que est� en X en la direcci�n 00BB.


<br><br><br><b><font size=3 face="Verdana">Registro de Banderas P</font></b><br><br>

El registro P es de 8 bits, cada bit representa un <b>Flag</b> (un bit con valor 1 o 0) o <b>Banderas de Estado</b> de la Snes: <br><br>

<img src="img/cap11.PNG"><br><br>

n: Negative (Negativo)<br>
v: Overflow (Bit de Desbordamiento)<br>
m: Memory/Accumulator Select (Bit Acumulador)<br>
x: Index Register select (Bit de Registros �ndices X e Y)<br>
d: Decimal<br>
i: Interrupt(Bit de la M�scara de Interrupci�n)<br>
z: Zero (Bit Cero)<br>
c: Carry (Bit de Acarreo)<br>
e: Emulation (Emulaci�n)

<br><br><b>n: Negative (Negativo)</b><br><br>

Se pone a 1 si el resultado de la �ltima operaci�n aritm�tica, l�gica o de manipulaci�n de bits fue negativo. <br><br>

Ejemplo: <br>
LDY #$0002 ; se carga Y con 0x0002 <br>
DEY ;Y=0001 <br>
DEY ;Y=0000 -> solo para saber, aqu� Z pasa a 1 ya que Y lleg� a 0.<br>
DEY ;Y=FFFF -> aqu� N pasa a 1, ya que si al 0 le restamos 1 nos da...FFFF pero se activa N.<br><br>

<b>v: Overflow (Bit de Desbordamiento)</b><br><br>

Es parecido al de Acarreo pero act�a con n�meros en complemento a dos y se activa cuando se pasa del mayor numero (el +127 en 1 byte) al menor (-127 en 1 byte). <br><br>

<b>m: Memory/Accumulator Select (Bit Acumulador)</b><br><br>

Este bit deja el Acumulador en 8 o 16 bits. Si este bit esta seteado (es decir m = 1) el acumulador esta en 8 bit y si el bit 5 esta re-seteado (m = 0) el acumulador est� en 16 bit. Para dejar solo el acumulador en modo de 8-bits usa SEP #$20. 

<br><br>Esto es as� ya que ya que 20 hex es igual a 00100000 en binario y si comparamos 00100000 con el Registro de Flag queda:<br><br>

00100000<br>
nvmxdizc, es decir m = 1<br><br>

En resumen:<br>
Para dejar solo el acumulador en modo de 8 bits usa SEP #$20.<br>
Para dejar solo el acumulador en modo de 16 bits usa REP #$20.<br>
Para dejar el registro X/Y y A en modo de 8 bits usa: SEP #$30<br>
Para dejar el registro X/Y y A en modo de 16 bits usa: REP #$30<br>

<br>Entonces lo correcto al hacer un LDA #$1234, es hacer justo antes un REP #$20.<br>Lo mismo si se quiere hacer un LDA #$05 es hacer antes un SEP #$20.<br><br>

<b>x: Index Register select (Bit de Registros �ndices X e Y)</b><br><br>

Este bit deja permite dejar los registros X e Y en 8 o 16 bits. <br><br>
Para dejar el registro X/Y en modo de 8 bits usa SEP #$10.<br>
Para dejar el registro X/Y en modo de 16 bits usa REP #$10.<br>
Para dejar el registro X/Y y A en modo de 8 bits usa: SEP #$30.<br>
Para dejar el registro X/Y y A en modo de 16 bits usa: REP #$30.<br><br>

Explicaci�n de porque ese "10":<br>
REP #$10    ; 10 hex = 10000 bin = bit 5 (X) �ndices X/Y en modo 16 bit.<br>
SEP #$10    ; 10 hex = 10000 bin = bit 5 (X) �ndices X/Y en modo 8 bit.<br><br>

<b>d: Decimal</b><br><br>

Permite setear el uso de valores decimales en vez de hexadecimales. Por defecto esta en 0, es decir 
en modo Hexadecimal: <br>
d=0 =&gt; Modo Hexadecimal<br>
d=1 =&gt; Modo Decimal<br><br>


<b>i: Interrupt (Bit de la M�scara de Interrupci�n)</b><br><br>

La m�scara de interrupci�n I se pone a 1 mediante hardware o software para deshabilitar (enmascarar) todas las posibles fuentes de interrupci�n, tanto exteriores como interiores.<br><br>

<b>z: Zero (Bit Cero)</b><br><br>

Se pone a 1 si el resultado de la �ltima operaci�n aritm�tica, l�gica o de manipulaci�n de bits fue nulo (cero).<br><br>
Ejemplo 1: <br>
LDA #$0000 ; esto hace que Z pase a 1.<br>
LDX #$0000 ; esto hace que Z pase a 1.<br><br>

Ejemplo 2:<br><br>

LDY #$0002 ; se carga Y con 0x0002 <br>
DEY ;Y=1 <br>
DEY ;Y=0 -> aqu� Z pasa a 1<br><br>


<b>c: Carry (Bit de Acarreo)</b><br><br>

Hay veces en que una operaci�n hace que un n�mero se desborde y esto se debe registrar de alguna forma. Cuando un n�mero se desborda el flag de acarreo se pone a 1. El carry puede afectar a algunas operaciones de Suma y Resta, es decir, no afecta a las operaciones de INC y DEC, en cambio si a las de ADC y SBC.<br><br>

Para dejar el Carry en 0 se usa la instrucci�n CLC.<br>
Para dejar el Carry en 1 se usa la instrucci�n SEC.<br><br>

�Pero para que dejar el Carry en 1 o en 0?<br>
Bueno, aunque esto lo veremos en cap�tulo 3 y 4, dir� que si queremos hacer esta operaci�n matem�tica: X = A+4, en verdad es un seudo-c�digo que representa la suma de registros. Donde A es el acumulador con valor inicial 2 y X es el registro X vac�o.<br>
Entonces haremos esto:<br><br>

ADC #$04 ;sumamos el valor 0x4 a A, aqu� el Operando es 0x4 <br>
TAX ; transferimos(copiamos) lo de A hacia X, supuestamente tendr�amos que X = A = 2 + 4 = 6, X vale 6 hex. <br>

Pero aqu� hay algo mal ya que ADC es "add with carry", es decir "Sumar con Carry", en otras palabras la "f�rmula" para ADC es: <br><br>

<b>ADC Operando => A = A + Operand + Carry </b><br><br>

Entonces el <b>Carry AFECTA la Suma</b>, �se dan cuenta? A ver, asumamos que el carry esta en 1, entonces si hacemos la misma operaci�n que antes:<br><br>

ADC #$04<br>
TAX<br><br>

�C�mo queda A finalmente? <br>
La respuesta es A = 4 + 2 +1 = 7, entonces X = 7. �se dan cuenta ahora como afecta a la suma?<br>
Entonces lo que hay que hacer es dejar el carry en 0 ANTES de la suma, entonces vemos como quedar�a:<br><br>

CLC<br>
ADC #$04<br>
TAX<br><br>

�Listo! As� nos queda la operaci�n X = A + 4. 
<br><br>Tambi�n el <b>Carry afecta a la Resta</b> y su "f�rmula" es: 
<br><br><b>SBC Operando => A = A &#8211; Operand + Carry &#8211; 1</b><br>
Entonces ustedes pueden ver como se escribir�a esta operaci�n: X = A &#8211; 3. <br><br>
Sigamos adelante entonces. <br><br>


<b>e: Emulation (Bit de Emulaci�n)</b><br><br>

Si E = 1, entonces el procesador esta en modo de Emulaci�n 6502 (emula el procesador de NES), Pero si esta en E = 0 el procesador esta en modo Nativo, es decir, en modo SNES.<br><br>

Para cambiar a Modo Nativo (E=0) hay que hacer:<br>
CLC	; limpia el flag de acarreo a 0.<br>
XCE	; intercambia el bit de emulaci�n con el de acarreo.<br><br>

Para cambiar a Modo de Emulaci�n (E=1):<br>
SEC	; bit de acarreo en 1.<br>
XCE	; intercambia el bit de emulaci�n con el de acarreo


<br><br><br><b><font size=3 face="Verdana">Memoria</font></b><br><br>

La memoria es la encargada de almacenar:<br><br>

<li>Conjunto de instrucciones: Aqu� est� el c�digo del juego, donde se puede hackear --> Usamos Memoria ROM.
<li>Datos fijos que se usan en el juego, como tiles, sonidos --> Usamos Memoria ROM. 
<li>Datos temporales que usa el juego, como tiles, trozos de c�digo que descomprime, etc. --> Usamos la RAM.
<li>Datos finales, listos para ser mostrados en pantalla --> Usamos memoria VRAM.
<br><br>
La memoria <b>RAM</b> (Random Access Memory, Memoria de Acceso Aleatorio) almacena los datos a procesar. Generalmente es un tipo de memoria vol�til (su contenido se pierde cuando se apaga la computadora o la consola en nuestro caso), pero depende del tipo de memoria RAM. <br><br>

 

La memoria <b>RAM</b> en la SNES se subdivide en:<br><br>
<li><b>WRAM (Working RAM)</b> de 124 KBytes, que es la misma RAM interna de la SNES. Es la que importa, ya que la WRAM se compone de 3 Sub-RAMs que son: LowRAM, HighRAM y Expanded RAM. Estas tres RAMs nos guardar�n los datos temporalmente al momento de extraer un texto comprimido, por ejemplo. 
<br><br><li><b>VRAM (Video RAM)</b> de 64 KBytes, que es la memoria de video que se accede por los registros y es encargada de mostrar los gr�ficos finales en pantalla.
<br><br><li><b>SRAM (Save RAM)</b> de 526 o 512 KBytes, que es la memoria est�tica que est� dentro de un cartucho de SNES; tiene el tama�o un poco mayor que una pila de esas de bot�n. En ella es en donde se guarda una copia de la RAM en el momento que se salva la partida jugando con una SNES real. Es m�s o menos como los archivos que contienen las partidas guardadas (no SaveStates) de los emuladores. El contenido de la SRAM no se pierde al apagar la consola, debido a que se encuentra en el cartucho mismo.<br>

Cuando abres un juego con cualquier emulador, se crea un archivo .srm con el mismo nombre del archivo de la ROM y que contiene lo mismo que contendr� esa memoria RAM de un cartucho de SNES. En realidad se presenta s�lo en juegos que tienen opciones para continuar, al estilo de los RPG y ZELDAs. Cuando salvas (usando SaveStates con ZSNES), te queda un archivo .zst, puesto que estos mantienen la copia EXACTA de los dos bancos de RAM y de toda la VRAM en el momento de guardar.<br><br>

La memoria <b>ROM</b> (Read Only Memory, Memoria de S�lo Lectura) es una memoria permanente en la que no se puede escribir (viene pregrabada por el fabricante de juegos). Los programas almacenados aqu� no se pierden al apagar la consola y tampoco se pueden modificar mientras �sta (la consola) est� corriendo. Esta es memoria no vol�til. <br> Los �nicos que modificamos la ROM somo nosotros, los romhackers :)<br> 

La memoria principal la representaremos como un conjunto de direcciones hexadecimales donde cada direcci�n tiene un dato asociado.<br>
<img src="img/cap12.gif"><br><br>

En la imagen se ve un modelo t�pico y muy b�sico de una Memoria de SNES. Se ve una <b>direcci�n</b> que est� compuesta por un <b>BANCO</b> y la <b>DIRECCI�N</b>. Al conjunto Banco:Direcci�n se le conoce como <b>Offset</b>. El banco parte de 00 hasta FF y la direcci�n de 0000 hasta FFFF.<br><br>
Abajo se muestra un diagrama m�s concreto sobre la memoria: <br>

<img src="img/cap13.gif"><br><br>

En la memoria principal se encuentra la ROM y RAM. Estas direcciones var�an si la ROM es <b>LoROM</b> (LowROM) o <b>HiROM</b>(HighROM). Las LoRom fueron pensadas para juegos livianos, con poca data y las HiRom fueron hechas para rom con mucha data como algunos RPGs. 
<br>Todas las ROMs que conocemos est�n dentro de uno de estos 2 grupos. Las LoROM son m�s livianas, ya que contienen menos informaci�n. 
<br>Cada banco en LoROM es de 32 KB, y en HiROM es de 64 KB. 
<br>Como dije m�s arriba, en la memoria ROM se escribe el c�digo o programa principal de una ROM, es decir que una ROM (un juego cualquiera .smc, .fig, etc.), es la copia exacta del chip ROM de un cartucho de SNES (en este caso). En la RAM se guardan s�lo datos temporales. La RAM, como los Registros Internos y Externos, los provee la arquitectura de la SNES, no est�n dentro del cartucho.

<br><br><br><b><font size=3 face="Verdana">Mapa de Memoria</font></b><br><br>

Es una tabla donde se indican los bancos y direcciones de RAM, ROM y Registros Externos. Hay varios documentos en ingl�s en Zophar's Domain; recomiendo ver uno llamado "SNESMem.txt" hecho por ]SiMKiN[. S�lo pondr� aqu� lo m�s importante por ahora, las direcciones de RAM y ROM para una HiROM y LoROM: <br><br>

<img src="img/cap14.gif"><br><br>
<img src="img/cap15.gif"><br><br>

Estos bancos est�n definidos por la arquitectura de la SNES, no los invent� porque si. Y como dije, es una versi�n muy simplificada de un mapa que est� en Zophar's Domain. Lo que importa es que sepas c�mo se "ve" en la memoria la RAM y ROM para entender cuando yo explique algo as�: "seg�n esta direcci�n se accede a la RAM..." o "sacamos este dato de la ROM".

<br><br><hr style="width: 100%; height: 2px;"><br>
<b>Gusto a Poco</b>

<br><br>1. Si tenemos la instrucci�n LDA $7EB200, �de d�nde sacamos datos? 
<br>R: De la RAM, ya que tenemos 7E:B200, y 7E es la direcci�n de la RAM. 

<br><br>2. Si tenemos la instrucci�n STA $CC0150, �d�nde lo almacenamos?
<br>En la ROM, pero la memoria ROM var�a de posici�n dependiendo de si es HiROM o LoROM, en este caso la direcci�n es $0150 y la LoROM no accede en direcciones menores de $8000, s�lo desde $8000 a $FFFF para cada banco, por lo que STA $CC0150 lo que hace es almacenar el dato del acumulador en la ROM de una HiROM.

<br><br>3. �En un programa real es posible hacer STA $1234?
<br>R: Depende, ya que no se sabe el banco, si fuera un banco de la ROM, como por ejemplo el banco 01: STA $011234, NO seria posible ya que en la ROM no es posible escribir. Pero si fuera un banco de la RAM, como 7E: STA $7E1234, SI ser�a posible ya que aqu� si es posible escribir.

<br><br>
<hr style="width: 100%; height: 2px;">

<a href="../doc_traduc.php">Volver</a>
<br>
<font color="#FFFFFF">
<?php 
include '../disq.php';
?>
</font>
<center><?php include ('../pie.php'); ?></center>
</small>
</body>
</html>
