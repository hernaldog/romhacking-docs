<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DMALog en Espa�ol por Dark-N</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.Estilo1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-style: normal;
	font-weight: bold;
}
.Estilo3 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.Estilo4 {color: #0000FF}
.Estilo5 {color: #FF0000}
.Estilo6 {font-size: 10px}
-->
</style>
</head>

<body>
<div align="center">
<small><p align="left"><a href="../../doc_traduc.php"><span style="font-family: Verdana;">Volver</span></a></small>
  <hr>
  <p><span class="Estilo1">DmaLog en Espa&ntilde;ol<br>
    Por Dark-N<br>
    </span>
<span style="font-family: Verdana;">
<small>
	<a href="http://darknromhacking.com/">http://darknromhacking.com</a></small></span><br>
	<span class="Estilo1">
    <br>
    </span><span class="Estilo3">Basado en el Log que estaba en:<br>
    <span class="Estilo4"><a href="http://agtp.romhack.net/dmalog/">http://agtp.romhack.net/dmalog/</a> </span>(abajo)<br>
	<br>
	</span><span class="Estilo1"><font size="1">- �ltima actualizaci�n: 
	31/05/2007 -</font></span></p>
  <p align="center" class="Estilo3">Resumen: Log de IRC donde se ense&ntilde;a como usar <strong>DMA</strong> 
	en una rom de SNES</span>.<br>
    <br>
  <img src="imagenes/titulo.PNG" width="256" height="223"><br>
	Usaremos la rom <strong>Eien no Filerna (J)</strong>.</p>
</div>
<span class="Estilo1">
<hr>
</span><span class="Estilo3"><span class="Estilo4">(<strong>**</strong> = notas del traductor) </span><br>
<br>
Odio escribir introducciones a estas cosas.
</span>
<p class="Estilo3">B&aacute;sicamente este log te ense&ntilde;ar&aacute; como usar assembler 65816 para saltarte un rutina de descompresi&oacute;n y usar tus propios 
  datos predescomprimidos en vez de los datos originales comprimidos. Esto servir&aacute; si lo que quieres es traducir un juego con fuentes comprimidas y no deseas escribir un descompresor/recompresor (o no sabes como hacerlo).<br>
  Este documento no sirve mucho para grandes proyectos con numerosos bloques comprimidos que necesitan ser modificados
  y reinsertados (**como algunos RPGs). Usa este documento con discreci&oacute;n.</p>
<p class="Estilo3">El Log usa el juego &quot;Eien no Filerna&quot; como prueba. Necesitar&aacute;s el emulador ZSNES (preferentemente la versi&oacute;n de ZSNES DOS), ASM2Hex assembler de LordTech, SNES9X de LordTech (idealmente la versi&oacute;n 1.37), un editor de Tiles y un editor
  de texto. Tambi&eacute;n es recomendado tener el programa Hex2SNES de J2E.</p>
<p class="Estilo3">Los siguientes archivos son entregados con este log:</p>
<p class="Estilo3"> * SNES9X LordTech v1.37<br>
  * Hex2SNES (** Tambi�n sirve <a href="../../archivos/LunarAddress1.02.rar">Lunar Address 
1.0.2</a>)<br>
  * <a href="recursos/asm2hex.zip">ASM2Hex</a><br>
  * <a href="recursos/Eien_no_Filena.zs1">filerna.zst</a><br>
  * <a href="recursos/enf-dma-esp.txt">enf-dma-esp.txt</a><br>
  * <a href="recursos/enf-dma-solucion.txt">enf-dma-solucion.txt</a><br>
  * <a href="recursos/Filena.log">trace.log</a><br>
  <br>
  <b>**</b>Programas que debes tener no mencionados aqu&iacute;: 
<a href="recursos/Zsnesv1.337_Win.zip">Zsnes v1.337 de 
Windows</a>, <a href="recursos/Zsnes_0.988c_DOS.zip">Zsnes 0.988c de DOS</a>, 
<a href="recursos/snes9x1.43.ep9r8_debugger.zip">Geiger's Snes9x 1.43 Debugger</a>, 
<a href="recursos/snes9x141_trace.zip">SNES 9x 1.41 Trace</a>, 
<a href="../../archivos/tlp1.1.rar">Tile Layer Pro 1.1.</a><br>
<b>**</b>A medida que avanza la conversaci�n, pongo separaciones para entender el 
proceso general dentro del Log.</p>
<p class="Estilo3">Puedes obtener la &uacute;ltima versi&oacute;n de ZSNES en <a href="http://www.zsnes.com">http://www.zsnes.com</a>. No me pidan a mi (**RadioSprite es el que ense&ntilde;a) o a satsu (**el que aprende en la conversaci&oacute;n) por la ROM de &quot;<span class="Estilo5">Eien no Filerna</span>&quot; (** 
la puedes encontrar en <a href="http://www.rom-world.com/">http://www.rom-world.com/</a> ), bajo ninguna circunstancia les pasar&eacute; una. Cuando este log se 
mueva finalmente a <a href="http://www.romhacking.org">http://www.romhacking.org</a>, si una persona sube este archivo a RHDO (**http://www.romhacking.org) por favor elimine esta frase y cambie todos los links a los archivos espec&iacute;ficos en romhacking.org, excepto 
con ASM2Hex, ya que el zip puede o no estar fuera de fecha;
  y el c&oacute;digo asm compila con esta versi&oacute;n (**de ASM2Hex), esta 
versi�n no es como otras versiones disponibles.</p>
<hr>
<p class="Estilo3"><b>** A Quien va Dirigido este Documento<br>
<br>
</b>Este documento va dirigido a todos aquellos romhackers que se quedan pegados 
con alguna traducci�n de SNES justo en le momento en que no pueden cambiar las 
fuentes de un rom ya que no las encuentran de ninguna forma, es decir que han 
probado todos los formatos conocidos y en varios editores de tiles.<b><br>
<br>
<br>
T�cnicamente lo que haremos...</b></p>
<p class="Estilo3">Esto no estaba en el LOG pero creo que es importante entender 
que haremos con la rom. En pantalla no se ver� ning�n cambio ya que 
modificaremos la rom internamente.<br>
Este es el diagrama b�sico y mas abajo el que utiliza DMA, para el trato de 
alguna fuente u otro dato 
comprimido en la SNES:<br>
<br><img src="imagenes/diagrama_basico_DMA.PNG" border="1"><br>
<br>
Como se ve en los diagramas, lo que haremos b�sicamente ser�:</p>
<p class="Estilo3">1. Crear nuestras propias tiles descomprimidas en alg�n lugar 
vac�o de la ROM. Podemos cambiar las tiles por otro formato, o agregar tiles 
nuevas, cosa antes no pod�amos por estar comprimidas ;)<br>
2. Por lo anterior, tampoco usaremos  
el algoritmo que las procesa.<br>
3. Crearemos una rutina en un espacio vac�o de la ROM. Es una rutina que use registros DMA copiaremos nosotros mismos las tiles a 
RAM, sin usar el algoritmo original.<br>
<br>
Como nos damos cuenta, de la RAM en adelante no nos preocupamos ya que la SNES las trabaja como antes.<br>
<br>
<b><br>
Beneficios de USAR DMA<br>
</b><br>
-Permite solucionar el cl�sico problema de que no se ven las fuentes en la rom 
en ning�n modo ni con ning�n editor de tiles.<br>
-Las Tiles de las fuentes quedan descomprimidas en ROM por lo que es llegar y 
editarlas con el Tile Layer Pro por ejemplo. <br>
-Permite agregar acentos u otros s�mbolos.</p>
<hr>
<p class="Estilo3"><b>Preparando Ambiente</b></p>
<p class="Estilo3"><span class="Estilo4">[02:40] &lt;@RadioSprite&gt;</span> satsu<br>
  <span class="Estilo4">[02:40] &lt;@RadioSprite&gt;</span> si te queda poder cerebral a esta hora de la noche<br>
  <span class="Estilo4">[02:40] &lt;@RadioSprite&gt;</span> &iquest;quieres hacer 
lo de Filerna ahora?<br>
  <span class="Estilo4">[02:41] &lt;satsu&gt;</span> S&iacute;, seguro. D&eacute;jame hacer una r&aacute;pida edici&oacute;n y tendr&aacute;s mi tonta atenci&oacute;n.<br>
  <span class="Estilo4">[02:41] &lt;@RadioSprite&gt;</span> env&iacute;ame tambi&eacute;n el juego.<br>
  <span class="Estilo4">[02:41] &lt;@RadioSprite&gt;</span> estas autorizado (**Creo que lo dice por un tema de seguridad del IRC)<br>
  <span class="Estilo4">[02:41] &lt;satsu&gt;</span> Esta bien.<br>
  <span class="Estilo4">[02:41] &lt;@RadioSprite&gt;</span> Este tipo de modificaci&oacute;n toma alrededor de 5 minutos, si sabes lo que est&aacute;s haciendo.<br>
  <span class="Estilo4">[02:42] &lt;satsu&gt;</span> &iexcl;yay!<br>
  <span class="Estilo4">[02:42] &lt;satsu&gt;</span> desafortunadamente, muy pocas veces puedo. :P<br>
  <span class="Estilo4">[02:42] &lt;@RadioSprite&gt;</span> Te ense&ntilde;ar&eacute; unas pocas cosas<br>
  <span class="Estilo4">[02:43] &lt;@RadioSprite&gt;</span> #1, Voy a ense&ntilde;ar como encontrar una rutina de compresi&oacute;n y como salt&aacute;rtela.<br>
  <span class="Estilo4">[02:43] &lt;@RadioSprite&gt;</span> #2, Voy a ense&ntilde;ar como usar la capacidad de la SNES llamada &quot;direct-memory access&quot; (**DMA) <br>
  <span class="Estilo4">[02:43] &lt;satsu&gt;</span> ya veo. o_o<br>
  <span class="Estilo4">[02:44] &lt;@RadioSprite&gt; </span>necesitas saber unas peque&ntilde;as cosas<br>
  <span class="Estilo4">[02:44] &lt;@RadioSprite&gt;</span> necesitas saber como cargar y guardar datos usando ASM<br>
  <span class="Estilo4">[02:44] &lt;@RadioSprite&gt;</span> necesitar&aacute;s el Hex2SNES de Lordtech.<br>
  <span class="Estilo4">[02:44] *** Disconnected<br>
  [02:47] *** Attempting to rejoin...<br>
  [02:47] *** Rejoined channel #romhacking</span><br>
  <span class="Estilo4">[02:48] &lt;@RadioSprite&gt;</span> er<br>
  <span class="Estilo4">[02:48] &lt;@RadioSprite&gt;</span> quise decir ASM2Hex<br>
  <span class="Estilo4">[02:48] &lt;@RadioSprite&gt;</span> Hex2SNES de J2E (**un grupo de Traducci&oacute;n) es una utilidad de conversi&oacute;n de direcciones, tambi&eacute;n muy &uacute;til<br>
  <span class="Estilo4">[02:48] &lt;@RadioSprite&gt;</span> Tambi&eacute;n deber&aacute;s logear esto, as&iacute; puedo subirlo como art&iacute;culo a RHDO<br>
  <span class="Estilo4">[02:48] &lt;satsu&gt;</span> si, tengo la opci&oacute;n logging de IRC siempre encendido.<br>
  <span class="Estilo4">[02:49] &lt;satsu&gt;</span> que bien que esta cosa no est&aacute; en #romhack, o tendr&iacute;a m&aacute;s de 12mb en logs. :P<br>
  <span class="Estilo4">[02:49] &lt;@RadioSprite&gt;</span> Tambi&eacute;n necesitar&aacute;s SNES9X tracelogger de LordTech.<br>
  <span class="Estilo4">[02:49] &lt;@RadioSprite&gt; </span>est&aacute;s otra vez autorizado.<br>
  <span class="Estilo4">[02:49] &lt;@RadioSprite&gt;</span> Ensamblar&eacute; tus logs, por si acaso.<br>
  <span class="Estilo4">[02:49] &lt;satsu&gt;</span> ok.<br>
  <span class="Estilo4">[02:50] &lt;satsu&gt;</span> bien, tengo asm2hex y snes9x.<br>
  <span class="Estilo4">[02:50] &lt;@RadioSprite&gt;</span> &iquest;Filerna es 
una HiRom o LoRom?<br>
  <span class="Estilo4">[02:50] &lt;satsu&gt;</span> creo que es una Lorom.<br>
  <span class="Estilo4">[02:50] &lt;@RadioSprite&gt;</span> Generalmente, HiROM y LoROM no te crean  problemas, excepto cuando se calculan punteros<br>
  <span class="Estilo4">[02:50] * @RadioSprite</span> maldice<br>
  <span class="Estilo4">[02:50] &lt;@RadioSprite&gt; </span>Odio las LoROM.<br>
  <span class="Estilo4">[02:51] &lt;@RadioSprite&gt;</span> Bien, comencemos.<br>
  <span class="Estilo4">[02:51] &lt;@RadioSprite&gt;</span> Abre el Hex2SNES si 
lo tienes por ah� ya que te ayudar&aacute; un mont&oacute;n para conversiones LoRom-&gt;Absolute.<br>
  <span class="Estilo4">[02:51] &lt;satsu&gt;</span> entiendo.<br>
  <span class="Estilo4">[02:51] &lt;@RadioSprite&gt;</span> Ok.<br>
  <span class="Estilo4">[02:51] &lt;@RadioSprite&gt; </span>Ahora, a lo mejor sabes la diferencia entre entre HiROM y LoROM<br>
  <span class="Estilo4">[02:52] &lt;@RadioSprite&gt;</span> pero para beneficio de quien leer&aacute; el Log, voy a explicarlos.<br>
  <span class="Estilo4">[02:52] &lt;satsu&gt;</span> si, &iexcl;sensei!<br>
  <span class="Estilo4">[02:52] &lt;@RadioSprite&gt;</span> Act&uacute;a como si tu no supieras que son ^_^<br>
  <span class="Estilo4">[02:52] &lt;satsu&gt;</span> apenas las conozco, as&iacute; que esta bien. ^^;<br>
  <span class="Estilo4">[02:52] &lt;@RadioSprite&gt;</span> B&aacute;sicamente, los programas SNES se dividen en 2 &quot;bancos&quot; de datos. Databanks, si prefieres.<br>
  <span class="Estilo4">[02:53] &lt;@RadioSprite&gt;</span> Cuando est&aacute;s tratando con 
<b>LoROMs</b>, cada banco es de 32K de tama�o, o $8000<span class="Estilo4"><br>
[02:53] &lt;@RadioSprite&gt; </span>Cuando est&aacute;s tratando con 
<b>HiROMs</b>, cada banco es de 64K de tama&ntilde;o, o $10000.<br>
  <span class="Estilo4">[02:54] &lt;@RadioSprite&gt;</span> Cuando escribes c&oacute;digo que mueve datos de aqu&iacute; para all&aacute;, tu -no puedes- exceder esos l&iacute;mites.<br>
  <span class="Estilo4">[02:54] &lt;@RadioSprite&gt;</span> Puedes armar cosas que lean de diferentes bancos de donde est&aacute;s, pero no puedes tener una continua pieza de datos cruzando los l&iacute;mites del banco.<br>
  <span class="Estilo4">[02:54] &lt;@RadioSprite&gt;</span> &iquest;Entendiste todo?<br>
  <span class="Estilo4">[02:54] &lt;satsu&gt;</span> puedo guardarlo<br>
  <span class="Estilo4">[02:54] &lt;@RadioSprite&gt;</span> Bien, ahora pasemos al Direccionamiento.<br>
  <span class="Estilo4">[02:55] &lt;@RadioSprite&gt;</span> El &quot;origen&quot; de un programa SNES puede variar, pero pongamos el caso que llamamos $0 al primer byte en el archivo.<br>
  <span class="Estilo4">[02:55] &lt;@RadioSprite&gt;</span> Cuando hacemos un direccionamiento a una HiROM, es en realidad a $C0:0000 o $C0/0000. (**Y no $00/0000) <br>
  <span class="Estilo4">[02:56] &lt;satsu&gt;</span> ok. (a prop&oacute;sito, la rom que te envi&eacute; no tiene header.)<br>
  <span class="Estilo4">[02:56] &lt;@RadioSprite&gt; </span>Pienso que puede ser tambi&eacute;n $00/0000 pero en verdad, nunca antes he visto una HiROM mirrored (**reflejada).<br>
  <span class="Estilo4">[02:56] &lt;@RadioSprite&gt;</span> satsu- muy bien, esta bien.<br>
  <span class="Estilo4">[02:56] &lt;@RadioSprite&gt;</span> eso hace las cosas mucho menos dolorosas en el trasero.<br>
  <span class="Estilo4">[02:56] &lt;satsu&gt;</span> &iexcl;pregunta!<br>
  <span class="Estilo4">[02:56] &lt;satsu&gt; </span>&iquest;Que significa mirrored?<br>
  <span class="Estilo4">[02:56] &lt;@RadioSprite&gt;</span> Chicos, &iexcl;siempre quiten los header de sus roms de SNES antes de jugar con ellos! (**con SNES9X Debugger abre la rom con header y 
el programa te preguntar&aacute; si deseas sac&aacute;rselo, ponle que si. Recuerda hacer antes un respaldo de la rom original. Ver imagen inferior)<br>
<br>
  <img src="imagenes/Snes9x_debugger_quita_header.PNG" width="339" height="272"><br>
  <span class="Estilo4"><br>
[02:56] &lt;ipkiss&gt;</span> heh<br>
  <span class="Estilo4">[02:56] &lt;@RadioSprite&gt;</span> B&aacute;sicamente...<br>
  <span class="Estilo4">[02:56] &lt;@RadioSprite&gt;</span> esto es -mas o menos- dif&iacute;cil de explicar<br>
  <span class="Estilo4">[02:57] &lt;@RadioSprite&gt;</span> Cuando tu has 
reflejado direcciones, esto significa la misma cosa<br>
  <span class="Estilo4">[02:57] &lt;@RadioSprite&gt;</span> D&eacute;jame usar una LoRom como ejemplo, porque s&eacute; como funciona el reflejado de ellas.<br>
  <span class="Estilo4">[02:57] &lt;@RadioSprite&gt;</span> Cuando tratas con una LoRom, el origen es $00/8000<br>
  <span class="Estilo4">[02:58] &lt;@RadioSprite&gt;</span> o $80/8000, cualquiera de estas dos.<br>
  <span class="Estilo4">[02:58] &lt;@RadioSprite&gt; </span>As&iacute; que b&aacute;sicamente, los Bancos $00 y $80 de la LoRom est&aacute;n &quot;reflejados&quot;<br>
  <span class="Estilo4">[02:59] &lt;@RadioSprite&gt;</span> Cuando accedes a datos tanto en $80/8000 o en $00/8000, siempre se obtiene lo misma cosa.<br>
  <span class="Estilo4">[02:59] &lt;satsu&gt;</span> hum, ya veo.<br>
  <span class="Estilo4">[02:59] &lt;@RadioSprite&gt;</span> Tambi&eacute;n nota que cuando est&aacute;s tratando con LoROM, las direcciones de cada banco individual
  comienzan en $8000 (**Ver Mapa de Memoria de mi Gu�a ASM #1).<br>
  <span class="Estilo4">[03:00] &lt;@RadioSprite&gt;</span> Si intentes acceder a datos dentro del rango de direcciones $0000-$7FFF, est&aacute;s
  saliendo de la memoria (** recuerda que la LoRom parte de $8000)<br>
  <span class="Estilo4">[03:00] &lt;@RadioSprite&gt; </span>No querr&aacute;s hacerlo a menos que tengas que 
hacerlo :)<br>
  <span class="Estilo4">[03:00] &lt;@RadioSprite&gt;</span> a menos que -realmente- sepas lo que estas haciendo.<br>
  <span class="Estilo4">[03:00] &lt;satsu&gt;</span> lo que no es, as&iacute; que dej&eacute;moslo como est&aacute;. :)<br>
  <span class="Estilo4">[03:00] * @RadioSprite</span> asiente con la cabeza<br>
  <span class="Estilo4">[03:01] &lt;@RadioSprite&gt;</span> Deja la ROM de Filerna en el disco, comenzaremos con las rutinas y lo dem&aacute;s.</p>
<hr>
<p class="Estilo3"><b>Tiles a Editar<br>
</b><br>
  <span class="Estilo4">[03:01] &lt;@RadioSprite&gt;</span> Necesitar&aacute;s el ZSNES para esto.<br>
  <span class="Estilo4">[03:01] &lt;satsu&gt;</span> ok.<br>
  <span class="Estilo4">[03:01] * satsu</span> toma apuntes<br>
  <span class="Estilo4">[03:02] &lt;@RadioSprite&gt;</span> lo cual me recuerda que tambi&eacute;n aprender&aacute;s como la memoria de la SNES setea un ZSNES savestate.<br>
  <span class="Estilo4">[03:02] &lt;@RadioSprite&gt;</span> Te&oacute;ricamente tambi&eacute;n se puede hacer tambi&eacute;n con un SNES9X savestate, pero el formato es 
  un poco confuso, y est&aacute; comprimido m&aacute;s encima<br>
  <span class="Estilo4">[03:02] &lt;@RadioSprite&gt;</span> El formato de un ZSNES es constante<br>
  <span class="Estilo4">[03:03] &lt;@RadioSprite&gt;</span> Por consiguiente es mas f&aacute;cil trabajar con el ^_^<br>
  <span class="Estilo4">[03:03] * satsu</span> asiente con la cabeza<br>
  <span class="Estilo4">[03:04] &lt;satsu&gt;</span> probablemente notar&aacute;s que Filerna no funciona con los &uacute;ltimos Zsnes.<br>
  <span class="Estilo4">[03:04] &lt;satsu&gt;</span> ZSNES 1.337 funciona bien sin embargo. 
(** para sacar salvadas ZST puedes usar el Zsnesv1.337 de Windows)<br>
  <span class="Estilo4">[03:05] &lt;@RadioSprite&gt;</span> &iquest;Que tal la versi&oacute;n .988c? :p<br>
  <span class="Estilo4">[03:05] &lt;@RadioSprite&gt;</span> Yo uso ZSNES DOS para la mayor&iacute;a de mis depuraciones, cargan m&aacute;s r&aacute;pido<br>
  <span class="Estilo4">[03:05] &lt;satsu&gt;</span> Yo no s�.<br>
  <span class="Estilo4">[03:06] &lt;satsu&gt;</span> mi Zsnes DOS carga REALMENTE lento en mi computador por alguna raz&oacute;n.<br>
  <span class="Estilo4">[03:06] &lt;@RadioSprite&gt;</span> muy bien, ahora la pieza que quieres es la fuente de las batalla, &iquest;correcto?<br>
<br>
  <img src="imagenes/dialogo_batalla.PNG" width="256" height="223"><br>
**
Las Tiles a modificar ser�n las del cuadro azul, y no las superiores. <br>
Imagen obtenida con Geiger's Snes9x Debugger, aunque igual se puede con el 
Zsnesv1.337 de Windows.<br>
  <br>
  <span class="Estilo4">[03:06] &lt;@RadioSprite&gt;</span> &iquest;La que es de 8x8? (**se refiere al tama&ntilde;o en pixels de las tiles 
del cuadro azul)<br>
  <span class="Estilo4">[03:06] &lt;satsu&gt;</span> si.<br>
  <span class="Estilo4">[03:06] &lt;satsu&gt;</span> <b>2bpp, 8x8</b> (** bpp = bit per pixels, el modo 2bpp permite 4 colores m&aacute;ximos por tile).<br>
  <span class="Estilo4">[03:06] &lt;@RadioSprite&gt;</span> Usa el que quieras, no deber&iacute;a haber mucha diferencia. (**se refiere a 
alg�n ZSNES)<br>
  <span class="Estilo4">[03:06] &lt;@RadioSprite&gt;</span> mientras puedas obtener un ZSNES savestate.<br>
  <span class="Estilo4">[03:06] &lt;satsu&gt;</span> ok.</p><hr>
<p class="Estilo3"><b>Encontrando las Tiles en la VRAM<br>
</b><br>
  <span class="Estilo4">[03:08] &lt;@RadioSprite&gt;</span> Ok bien, esto va a ser un poco dif&iacute;cil.<br>
  <span class="Estilo4">[03:08] * Looking up Naoya user info...</span><br>
  <span class="Estilo4">[03:08] &lt;@RadioSprite&gt;</span> En mi primer intento, 
en el savestate apareci&oacute; la fuente en 
<b>$2CC13</b>. (** se refiere al ZST que lo abri� en el TLP)<br>
<br>
  <img src="imagenes/dialogo_batalla.PNG" width="256" height="223"><br>
** Usa el Zsnes 1.337 Win y Salva justo aqu� con F2, pero si quieres, usa la 
salvada que yo obtuve &quot;Eien no Filena (J)_orig.zs1&quot;.<br>
Las tiles que ves en la imagen ya est�n en VRAM, la idea es que luego puedas &quot;capturarlas&quot; antes, 
cuando pasan por la RAM.<br>
<br>
  <img src="imagenes/ZST_TLP_2CC13.PNG" width="499" height="351" > <br>
  **
  Si abres este archivo .zs1 en el TLP, 
en modo Game Boy, se ver&aacute; distinto, pero le das a GoTo: $02CC13 y se ajustar&aacute; como se ve en la imagen. <br>
  <br>
  <span class="Estilo4">[03:08] &lt;@RadioSprite&gt;</span> Ahora es tiempo para una lecci&oacute;n de savestate.<br>
  <span class="Estilo4">[03:09] &lt;satsu&gt;</span> &iexcl;con un engrish agregado! :D (** <a href="http://es.wikipedia.org/wiki/Engrish">http://es.wikipedia.org/wiki/Engrish</a> )<br>
  <span class="Estilo4">[03:09] &lt;@RadioSprite&gt;</span> &iexcl;Y no cualquier engrish!<br>
  <span class="Estilo4">[03:09] &lt;@RadioSprite&gt;</span> &iexcl;Ese MACROSS engrish!<br>
  <span class="Estilo4">[03:09] &lt;@RadioSprite&gt;</span> De todos modos, sigamos.<br>
  <span class="Estilo4">[03:09] &lt;@RadioSprite&gt;</span> Los primeros <b>$C12</b> bytes de una salvada de ZSNES 
es informaci&oacute;n para el ZSNES en si. Un header, si prefieres.<br>
  <span class="Estilo4">[03:09] &lt;@RadioSprite&gt;</span> Comenzando en <b>$C13</b> es el primer banco de la RAM, $7E. (**$00:0C13 es RAM 7E)<br>
  <span class="Estilo4">[03:10] &lt;@RadioSprite&gt;</span> Comenzando en <b>$10C13</b> es el -segundo- banco de RAM, $7F. (**$01:0C13 es RAM 7F)<br>
  <span class="Estilo4">[03:10] &lt;@RadioSprite&gt;</span> Los Bancos $7E y $7F son iguales sin importar en que modo de direccionamiento est&aacute;s.<br>
  <span class="Estilo4">[03:11] &lt;@RadioSprite&gt;</span> Excepto quiz&aacute;s para los modos de direccionamientos 
enredados que usan las ROMs mayores a 32mbit/4mbyte, pero no nos desviemos.<br>
  <span class="Estilo4">[03:11] * satsu</span> asiente con la cabeza<br>
  <span class="Estilo4">[03:11] &lt;@RadioSprite&gt;</span> Comenzando en <b>$20C13 </b> es la 
<b>VRAM</b>.<br>
  <span class="Estilo4">[03:11] &lt;@RadioSprite&gt;</span> La VRAM es algo maldita.<br>
  <span class="Estilo4">[03:11] &lt;@RadioSprite&gt;</span> Bueno, no realmente.<br>
  <span class="Estilo4">[03:12] &lt;@RadioSprite&gt;</span> Pero generalmente hablando (y me puedo estar comiendo mis propias palabras luego) tu puedes -solamente- escribir en VRAM durante la etapa de 
<b>NMI</b>, el cual dura cerca de 8000 ciclos de CPU. (** NMI, Interrupcion no 
enmascarable, -o no desabilitable-, que sucede cuando se ha acabado de dibujar 
una pantalla entera en la TV y se esta haciendo el <b>blanking</b> en pantalla)<br>
  <span class="Estilo4">[03:12] &lt;@RadioSprite&gt; </span>hacer este duro proceso, como la descompresi&oacute;n de una fuente, generalmente es algo 
  en el que no quieres tener un tiempo l�mite (** se refiere a que es 
lento pasar de ROM a VRAM de una vez descomprimiendo la data en el momento)<br>
  <span class="Estilo4">[03:12] &lt;@RadioSprite&gt;</span> As&iacute; que normalmente 
esta data comprimida es dumpeada (**copiada) al alg&uacute;n lugar de la RAM y 
  luego es transferida a VRAM.<br>
  <span class="Estilo4">[03:13] &lt;@RadioSprite&gt;</span> Debes intentar sacar un savestate con la fuente comprimida en RAM 
y no en VRAM.<br>
  <span class="Estilo4">[03:13] &lt;@RadioSprite&gt;</span> Es muy probable que sea un c&oacute;digo de inicializaci&oacute;n de una batalla el cual hace esto (durante 
  la pantalla negra despu&eacute;s que dice FILERNA 6 escrito en kanji)<br>
  <span class="Estilo4">[03:14] &lt;@RadioSprite&gt;</span> y antes de que la batalla comience<br>
  <span class="Estilo4">[03:14] &lt;satsu&gt;</span> ok.</p><hr>
<p class="Estilo3" align="left"><b>Encontrando las Tiles en la RAM, m�todo con ZSNES<br>
</b><br>
  <span class="Estilo4">[03:14] &lt;@RadioSprite&gt;</span> as� que intenta 
sacar&nbsp;varios savestates y hazlo r&aacute;pido durante ese tiempo, y si  
  puedes obtener la fuente en la <b>RAM</b>, genial<br>
  <span class="Estilo4">[03:14] &lt;@RadioSprite&gt;</span> si no puedes, yo lo intentar&eacute; tambi&eacute;n y si la obtengo antes que ti, te 
la enviar&eacute;<br>
  <span class="Estilo4">[03:14] &lt;satsu&gt;</span> &iexcl;de acuerdo!<br>
  <span class="Estilo4">[03:14] &lt;@RadioSprite&gt;</span> wow, reci&eacute;n obtuve una BSOD (**Blue Screen of Death: pantalla azul de error, cl&aacute;sica en sistemas operativos Windows 9X) y se recuper&oacute; o_O<br>
  <span class="Estilo4">[03:15] &lt;satsu&gt;</span> @_@<br>
  <span class="Estilo4">[03:15] &lt;satsu&gt;</span> &iquest;quieres reiniciar el sistema?<br>
  <span class="Estilo4">[03:15] &lt;@RadioSprite&gt;</span> no<br>
  <span class="Estilo4">[03:15] &lt;@RadioSprite&gt;</span> Estar&eacute; bien<br>
  <span class="Estilo4">[03:16] &lt;satsu&gt;</span> ok.<br>
  <span class="Estilo4">[03:19] &lt;@RadioSprite&gt;</span> (Esta es generalmente la parte dif&iacute;cil, 
por si no lo has adivinado ^^)<br>
  <span class="Estilo4">[03:19] &lt;satsu&gt;</span> &iexcl;hurra!<br>
  <span class="Estilo4">[03:21] &lt;@RadioSprite&gt;</span> demonios, lo olvid&eacute;<br>
  <span class="Estilo4">[03:21] &lt;@RadioSprite&gt;</span> aqu� es donde la <b>tecla retardadora
</b>del Zsnes se vuelve &uacute;til (**Tecla Slow-down del ZSNES -&gt; se configura en Misc-&gt; Game Keys, se usa para que el 
juego corra lento y as&iacute; puedas obtener una salvada cuando no tienes mucho tiempo)<br>

<table width="66%"  border="0" align="left" height="285">
  <tr>
    <td width="49%" height="281"><div align="center">
      <img src="imagenes/ZSNES_Misc.PNG" width="387" height="186" align="absbottom"></div></td>
    <td width="42%"><div align="center">
		<img src="imagenes/ZSNES_SlowDown_key.PNG" width="333" height="271"></div></td>
  </tr>
</table>
<p class="Estilo3"> &nbsp;</p>
<p class="Estilo3"> &nbsp;</p>
<p class="Estilo3"> &nbsp;</p>
<p class="Estilo3"> &nbsp;</p>
<p class="Estilo3"> &nbsp;</p>
<p class="Estilo3"> &nbsp;</p>
<p class="Estilo3"> &nbsp;</p>
<p class="Estilo3"> &nbsp;</p>
<p class="Estilo3"> <br>
** Asigna una tecla cualquiera al modo Slown Down, esto har&aacute; que el juego ande lento cuando la presiones, 
<br>
ayudando a 
sacar salvadas en peque&ntilde;os instantes del juego.
 <span class="Estilo4"><br>
<br>
[03:21] &lt;satsu&gt;</span> si, esto igual se me hab&iacute;a olvidado. :P<br>
<span class="Estilo4">[03:22] &lt;satsu&gt;</span> &iquest;tecla retardadora? o_O<br>
  <span class="Estilo4">[03:22] &lt;@RadioSprite&gt;</span> Excepto que mi versi&oacute;n no tiene una ^^; (** Se refiere supongo a su versi&oacute;n de ZSNES)<br>
  <span class="Estilo4">[03:22] &lt;satsu&gt;</span> demonios, nunca supe eso.<br>
  <span class="Estilo4">[03:22] * satsu</span> asigna la tecla retardadora.<br>
  <span class="Estilo4">[03:25] &lt;@RadioSprite&gt; </span>HMMM<br>
  <span class="Estilo4">[03:25] &lt;@RadioSprite&gt; </span>ahah<br>
  <span class="Estilo4">[03:25] &lt;@RadioSprite&gt;</span> Creo que debo tener algo.<br>
  <span class="Estilo4">[03:25] &lt;satsu&gt;</span> !!<br>
  <span class="Estilo4">[03:26] &lt;@RadioSprite&gt;</span> no, no importa<br>
  <span class="Estilo4">[03:26] &lt;satsu&gt;</span> -_-<br>
  <span class="Estilo4">[03:26] &lt;@RadioSprite&gt;</span> conf&iacute;a en mi, es mejor :p<br>
  <span class="Estilo4">[03:28] &lt;@RadioSprite&gt;</span> estamos casi.<br>
  <span class="Estilo4">[03:30] &lt;satsu&gt;</span> tengo un savestate con ALGO de la fuente en RAM...<br>
  <span class="Estilo4">[03:31] &lt;@RadioSprite&gt;</span> eso esta bien, mientras que esta sea -la primera parte- de la fuente, 
  y no la &uacute;ltima parte.<br>
  <span class="Estilo4">[03:31] &lt;@RadioSprite&gt;</span> ya que necesitamos saber donde comienza 
y no donde termina.<br>
  <span class="Estilo4">[03:31] &lt;satsu&gt;</span> oh.<br>
  <span class="Estilo4">[03:32] &lt;satsu&gt; </span>es mas dif&iacute;cil que los anillos de Saturno se intercambien. @_@<br>
  <span class="Estilo4">[03:32] &lt;@RadioSprite&gt;</span> hahah :)<br>
  <span class="Estilo4">[03:33] &lt;satsu&gt;</span> diablos, obtuve la parte final de la fuente de nuevo. &gt;_&lt;;<br>
  <span class="Estilo4">[03:35] &lt;@RadioSprite&gt;</span> muy bien, obtuve 
algo con la rutina entera.<br>
  <span class="Estilo4">[03:35] &lt;@RadioSprite&gt; </span>un segundo, d&eacute;jame comprimirlo<br>
  <span class="Estilo4">[03:35] &lt;satsu&gt;</span> ah, me perd&iacute;. :(<br>
  <span class="Estilo4">[03:36] &lt;@RadioSprite&gt;</span> para ser honesto, termin&eacute; usando el 
<b>Depurador de Zsnes</b> <b>de DOS</b> (**
Zsnes 0.988c versi�n DOS) para avanzar 1000 
instrucciones a la vez hasta que ve&iacute;a cuando comenzaba a aparecer :p<br>
<span class="Estilo4">[03:36] &lt;@RadioSprite&gt;</span> (eso es zsnes -d nombre_rom para ti 
y para los adictos a al DOS ^^)<br>
<span class="Estilo4">[03:37] &lt;satsu&gt; </span>tramposo :P<br>
<br>
** Ve a <b>Inicio-&gt;Ejecutar-&gt;cmd </b>(en Win98 es command en WindowsXP 
puede ser cmd o command), entrar&aacute;s a la Consola de Comandos, deja una carpeta en el disco con el Zsnes 0.988c de DOS, y la rom sin header, luego escribe:<br>
&gt;<b>zsnes -d romfilerna.smc </b>y aparecer&aacute; lo como esto:</p>
<p class="Estilo3">  <img src="imagenes/zsnes_-d_romname_win98.PNG" width="500" height="312"><br>
  <br>
  ** Presiona una tecla y se ver&aacute; lo de abajo.<br>
  <img src="imagenes/zsnes_-d_romname2_win98.PNG" width="480" height="301">  <br>
  ** Presiona F1 para que se vea el juego. <br>
  ** Justo desde aqu&iacute; presiona F1 para volver a la pantalla anterior y poner un Breakpoint 
con la tecla B y F1 para jugar de nuevo. Con la tecla Enter vas instrucci�n a 
instrucci�n.<br>
  <span class="Estilo4"><img src="imagenes/desde_aqui.PNG" width="250" height="251"><br>  
  </span>** Yo no pude hacerlo de esta forma, lo hice con 
el Snes9x Debugger (ver m�s abajo). En el paquete original
viene un <b>filerna.zst</b>, esa es la salvada esta con las fuentes en RAM del ZST que 
consigui� RadioSprite.<br>
&nbsp;<br>

  <span class="Estilo4">[03:39] * @RadioSprite</span> va a obtener un sprite mientras espera que satsu acepte 
  el archivo<br>
  <span class="Estilo4">[03:40] &lt;@RadioSprite&gt;</span> buen ritmo ^_^<br>
  <span class="Estilo4">[03:40] &lt;@RadioSprite&gt;</span> bien, entonces<br>
  <span class="Estilo4">[03:40] &lt;@RadioSprite&gt;</span> como tu podr&aacute;s ver, la fuente est&aacute; ubicada en $11D13 en el savestate (**$01:1D13 en 
el ZST est� en el banco 7F de la <b>RAM</b>)<br>
<span class="Estilo4">[03:41] &lt;@RadioSprite&gt;</span> PREGUNTA SORPRESA: &iquest;Cual es la direcci&oacute;n completa de aquella fuente en la RAM?  <br>
  <span class="Estilo4">[03:41] &lt;satsu&gt; </span>agh$Q%%!<br>
  <span class="Estilo4">[03:42] * @RadioSprite</span> taptaptap<br>
  <span class="Estilo4">[03:42] &lt;satsu&gt;</span> espera un segundo, estoy intentado saber como funciona esto. x_x<br>
  <span class="Estilo4">[03:42] &lt;@RadioSprite&gt;</span> ok<br>
  <span class="Estilo4">[03:42] &lt;@RadioSprite&gt;</span> deber&iacute;a ser muy simple, pero supongo que he estado haciendo esto bastante tiempo...<br>
  <span class="Estilo4">[03:42] &lt;satsu&gt;</span> se que comienza en 7F al menos. :P<br>
  <span class="Estilo4">[03:42] &lt;@RadioSprite&gt;</span> es  mejor que no hagas trampas :p<br>
  <span class="Estilo4">[03:42] &lt;satsu&gt;</span> aun no. :P<br>
  <span class="Estilo4">[03:42] &lt;@RadioSprite&gt;</span> si, eso est&aacute; bien<br>
  <span class="Estilo4">[03:43] &lt;satsu&gt;</span> &iquest;es 7F:9D13?<br>
  <span class="Estilo4">[03:43] &lt;@RadioSprite&gt;</span> No.<br>
  <span class="Estilo4">[03:43] &lt;satsu&gt;</span> &iexcl;oh no!.<br>
  <span class="Estilo4">[03:43] &lt;@RadioSprite&gt;</span> No estas cerca x_x<br>
  <span class="Estilo4">[03:43] &lt;@RadioSprite&gt;</span> tienes la idea, 
pero estas cerca<br>
  <span class="Estilo4">[03:43] &lt;@RadioSprite&gt;</span> recuerda el  header  
que hay dentro de los savestates.<br>
  <span class="Estilo4">[03:44] &lt;satsu&gt;</span> oh si. :P<br>
  <span class="Estilo4">[03:44] &lt;@RadioSprite&gt;</span> er, el repugnante header, mejor dicho.<br>
  <span class="Estilo4">[03:44] &lt;satsu&gt;</span> Entonces no es 7F:9101, &iquest;verdad? :P<br>
  <span class="Estilo4">[03:44] &lt;@RadioSprite&gt;</span> estas muy cerca<br>
  <span class="Estilo4">[03:44] &lt;@RadioSprite&gt;</span> es $7F:0100<br>
  <span class="Estilo4">[03:44] &lt;@RadioSprite&gt;</span> er<br>
  <span class="Estilo4">[03:44] &lt;@RadioSprite&gt;</span> mejor dicho<br>
  <span class="Estilo4">[03:45] &lt;@RadioSprite&gt;</span> $7F:1100</p>
<p class="Estilo3">  **No te calientes mucho la cabeza y usa el <b>Lunar Address</b>, 
ver imagen inferior. Coloca la direcci�n $11D13 que es la del ZST y queremos 
obtener la Direcci�n RAM SNES.<br>
  <img src="imagenes/Lunar_11D13-7F1100.PNG" width="297" height="368"><br>
<br>
  <span class="Estilo4">[03:45] &lt;@RadioSprite&gt;</span> Ahora, antes dije eso de las LoRoms y $8000, pero eso solo se aplica a la data de la ROM, y no a la RAM<br>
  <span class="Estilo4">[03:45] &lt;satsu&gt;</span> oh, ok.<br>
  <span class="Estilo4">[03:45] &lt;@RadioSprite&gt;</span> pero deber&iacute;as ser capaz de obtener al menos $7F:X100 ^^;<br>
  <span class="Estilo4">[03:46] &lt;satsu&gt;</span> si, creo que te dije un pocas veces antes que las matem&aacute;ticas no son mi fuerte. ^^;;;;;<br>
  <span class="Estilo4">[03:46] &lt;satsu&gt;</span> o el tipeo, como parece. :P <br>
  <span class="Estilo4">[03:46] * @RadioSprite </span>se encoge de hombro<br>
  <span class="Estilo4">[03:46] &lt;@RadioSprite&gt;</span> bien, as&iacute; que ahora sabes que la fuente empieza en 
<b>RAM</b>, en <b>$7F:1100</b></p><hr>
<p class="Estilo3">**<b>Encontrando las Tiles en la RAM, m�todo con Geiger's Snes9X 
Debugger (recomendado)<br>
<br>
</b>Lo primero es obtener la direcci�n VRAM que entiende la SNES (SNES VRAM) 
para esto usamos el <b>Lunnar Addres</b> y ponemos el $02CC13 obtenido antes.<br>
<img src="imagenes/lunar_obtener_snes_VRAM.PNG"><br>
<br>
**
Nos arroja una direcci�n SNES VRAM $00:6000. Ahora abrimos la Rom con el <b>Snes9X Debugger</b> y si la Rom aun tiene el header, al abrirla dir� que es necesario quitarle 
header, ponle que si. Luego se cargar� esta pantalla:<br>
<img src="imagenes/snes_debuger1.PNG"><br>
<br>
**
Dale a RUN para empezar el juego, y llega a la parte donde se ve la pantalla 
negra con ve esos kanjis y el n�mero 6, justo <b>cuando se vaya despareciendo 
esta frase</b>, has clic en la ventana de Debug<b> </b>y all� selecciona la casilla <b>DMA</b> y dale a 
<b>RUN</b> y debes fijarte en el log/historial 
inferior lo que mostrar� la Direcci�n RAM (Ver imagen a). Cuando ya est� le 
di�logo en pantalla, con la batalla a punto de empezar (Ver imagen b) haz clic 
en el Log/historial para pausar el debug:<br><img src="imagenes/snes_debuger2.PNG"><br>
**Imagen a: Con el Debug pausado, buscamos en el Log/Historial alg�n &quot;6000&quot; y 
nos encontramos con la direcci�n RAM: <b>$7F:1100.</b><br>
<br>
<img src="imagenes/snes_debuger3.PNG"><br>
** Imagen b:
Aqu� det�n la depuraci�n, haciendo clic fuera de la ventana del juego, con los 5 
segundos pasados capturamos la direcci�n RAm que busc�bamos.</p><hr>
<p class="Estilo3"><b>Buscando alg�n Espacio Vac�o para poner Rutina DMA m�s 
adelante</b></p>
<p class="Estilo3">  <span class="Estilo4">[03:46] &lt;@RadioSprite&gt;</span> tambi&eacute;n vas a necesitar 
un espacio vac&iacute;o en la ROM para esto, tu sabes<br>
  <span class="Estilo4">[03:46] &lt;@RadioSprite&gt;</span> un segundo<br>
  <span class="Estilo4">[03:47] &lt;@RadioSprite&gt;</span> de hecho<br>
  <span class="Estilo4">[03:47] &lt;@RadioSprite&gt;</span> si decimos que la 
fuente no comprimida es de 8x8, ha de funcionar igual<br>
<span class="Estilo4">[03:47] &lt;satsu&gt;</span> si alguna fuente que no este comprimida es de 4bpp, &iquest;hay 
alguna diferencia?<br>
<span class="Estilo4">[03:48] &lt;@RadioSprite&gt;</span> ergh, si.<br>
<span class="Estilo4">[03:48] &lt;@RadioSprite&gt;</span> mucho en ese caso.<br>
<span class="Estilo4">[03:48] &lt;satsu&gt;</span> hay lo que parece ser un espacio libre comenzando en 106500... (** $10:6500 Ver imagen inferior)<br>
<br>
<img src="imagenes/106500.PNG"><br>
<br>
<span class="Estilo4">[03:49] &lt;satsu&gt;</span> suficiente para a fuente, creo<br>
<span class="Estilo4">[03:49] &lt;@RadioSprite&gt;</span> Solo voy a decir que hay un trozo de espacio decente en $FE300. (** $0F:E300 Ver imagen inferior)<br>
<br>
<img src="imagenes/FE300.PNG"><br>
<br>
<span class="Estilo4">[03:49] &lt;satsu&gt;</span> ese tambi&eacute;n sirve.<br>
<span class="Estilo4">[03:49] &lt;@RadioSprite&gt;</span> pero usaremos tu 
espacio ya que tu te fijaste en ese espacio y este es tu juego y tu tema. (** es 
curioso que dice que usar� el espacio de satsu ya que se usar� $1F:E300)<br>
<span class="Estilo4">[03:49] &lt;@RadioSprite&gt;</span> recuerda considerar lo que 
es necesario para la fuente, tambi&eacute;n lo necesitaremos para el nuevo c&oacute;digo.<br>
<span class="Estilo4">[03:50] &lt;satsu&gt;</span> hm, d&eacute;jame ver que tan largo es. 
(** Tiene que haber suficiente para la rutina DMA)<br>
<span class="Estilo4">[03:50] * @RadioSprite</span> se encoge se hombros<br>
<span class="Estilo4">[03:50] &lt;@RadioSprite&gt;</span> es cosa tuya ^_^<br>
<span class="Estilo4">[03:50] &lt;@RadioSprite&gt;</span> no importa ya que no va a hacer mucho c&oacute;digo.<br>
<span class="Estilo4">[03:50] &lt;satsu&gt;</span> en cualquier caso, pareciera que el espacio tuyo es mas grande que el 
m�o.</p><hr>
<p class="Estilo3"><b>Encontrando la Rutina de Descompresi�n</b></p>
<p class="Estilo3">  <span class="Estilo4">[03:52] &lt;@RadioSprite&gt;</span> bien satsu<br>
<span class="Estilo4">[03:52] &lt;@RadioSprite&gt;</span> hora de sacar el <b>Snes9x </b>
(** Se refiere a un SNES antiguo, de preferencia usa el Snes9X 1.41 trace. Con la tecla <b>/</b> del teclado num�rico empiezas 
la traza y con esa misma tecla la detienes, luego en la carpeta del juego habr� 
un &quot;Filerna.log&quot; que contendr� toda la Traza &quot;resumida&quot; de esos 4 segundos. 
Es 
&quot;resumida&quot; ya que con el SNES Debugger puedes hacer una traza completa de las 
instrucciones del CPU pero en esos 4 segundos se generan como 15 archivos de 5MB 
cada uno!)<br><img src="imagenes/instante_traza.PNG" width="726" height="330"><br>
**Estos son los instantes donde sucede la descompresi�n de Tiles, por lo que 
aqu� debes hacer la traza, recomiendo usar el <br>
Snes9X 1.41 trace. Finalmente generar� un archivo .log de 800 KB aprox.<br>
<br>
<span class="Estilo4">[03:52] &lt;@RadioSprite&gt;</span> la tecla <b>BLOQ NUM
</b>(NUM LOCK) activa el <b>Tracing</b><br>
<span class="Estilo4">[03:53] &lt;@RadioSprite&gt;</span> juega hasta el punto donde se muestra el texto FILERNA 6(kanji)<br>
<span class="Estilo4">[03:53] &lt;@RadioSprite&gt;</span> justo antes de la primera batalla<br>
<span class="Estilo4">[03:53] &lt;@RadioSprite&gt;</span> cuando el texto se este desapareciendo, pulsa la tecla NUM LOCK<br>
<span class="Estilo4">[03:53] &lt;satsu&gt;</span> ok.<br>
<span class="Estilo4">[03:53] &lt;@RadioSprite&gt;</span> una vez que la batalla aparece, pulsa NUM LOCK de nuevo para terminar el Trace 
(** conocido igualmente como Traza)<br>
<span class="Estilo4">[03:53] &lt;@RadioSprite&gt;</span> de esta forma, capturamos la rutina de descompresi&oacute;n cuando suceda.<br>
<span class="Estilo4">[03:53] &lt;satsu&gt;</span> por cierto, para facilitar la escritura, ese kanji se lee como 'sai'. :P<br>
<span class="Estilo4">[03:53] &lt;@RadioSprite&gt;</span> muy bien.<br>
<span class="Estilo4">[03:53] &lt;satsu&gt;</span> ok, comenzar&eacute; ahora.<br>
<span class="Estilo4">[03:54] &lt;@RadioSprite&gt;</span> Hazme saber cuando tengas 
la traza.<br>
<span class="Estilo4">[03:57] &lt;satsu&gt;</span> &iexcl;ups!, 800kb de traza<br>
<span class="Estilo4">[03:57] &lt;satsu&gt;</span> &iquest;supongo que part&iacute; muy pronto? :P<br>
<span class="Estilo4">[03:57] * @RadioSprite</span> se encoge de hombros<br>
<span class="Estilo4">[03:57] &lt;@RadioSprite&gt;</span> quiz&aacute;s si, quiz&aacute;s no<br>
<span class="Estilo4">[03:57] &lt;satsu&gt;</span> ok. &iquest;vamos con ese trace entonces?<br>
<span class="Estilo4">[03:58] &lt;@RadioSprite&gt;</span> est&aacute; bien.<br>
<span class="Estilo4">[03:58] &lt;@RadioSprite&gt;</span> Solo necesito jugar con esto un poco<br>
<span class="Estilo4">[03:58] &lt;@RadioSprite&gt;</span> ver&eacute; si puedo encontrar la rutina<br>
<span class="Estilo4">[03:59] &lt;@RadioSprite&gt;</span> puede que haya comenzado 
demasiado antes, hmm<br>
[<span class="Estilo4">03:59] &lt;satsu&gt;</span> ver&eacute; si puedo comenzar mas cercano a la batalla.<br>
<span class="Estilo4">[04:00] &lt;@RadioSprite&gt;</span> yo igual.<br>
<span class="Estilo4">[04:00] &lt;satsu&gt;</span> excelente, solo obtuve como 600kb esta vez. :P<br>
<span class="Estilo4">[04:02] &lt;@RadioSprite&gt;</span> heh :p<br>
<span class="Estilo4">[04:02] &lt;satsu&gt;</span> ok, creo que es lo mejor que voy a conseguir.<br>
<span class="Estilo4">[04:02] * @RadioSprite</span> cabecea<br>
<span class="Estilo4">[04:02] &lt;@RadioSprite&gt;</span> este no es exactamente el mejor juego para ense&ntilde;arte esto<br>
<span class="Estilo4">[04:03] &lt;@RadioSprite&gt;</span> hace cosas muy extra&ntilde;as<br>
<span class="Estilo4">[04:03] &lt;@RadioSprite&gt;</span> pero no obstante, har&eacute; 
el intento<br>
<span class="Estilo4">[04:03] &lt;satsu&gt;</span> heh, no creo que alguna vez haya trabajado con alg&uacute;n juego que sea normal. :P<br>
<span class="Estilo4">[04:04] &lt;@RadioSprite&gt;</span> hah.<br>
<span class="Estilo4">[04:04] &lt;@RadioSprite&gt;</span> Muy bien, de todas formas encontr&eacute; la rutina.<br>
<span class="Estilo4">[04:05] &lt;@RadioSprite&gt;</span> A menudo, cuando ya sabes 
que la fuente es dumpeada en $7F:1100, puedes sencillamente buscas
por algo que siga a $7F:11 en el trace<br>
<span class="Estilo4">[04:05] &lt;@RadioSprite&gt;</span> pero eso por lo visto no funciona aqu&iacute; 
(** a mi si me funcion�, aunque no es la rutina que buscamos, ver el Log de la carpeta recursos)<br>
<span class="Estilo4">[04:05] &lt;@RadioSprite&gt;</span> as&iacute; que busqu&eacute; por 1100 y intent&eacute; buscar el lugar donde el juego esta cargando $7F 
y lo colocaba en un sitio similar<br>
<span class="Estilo4">[04:06] &lt;@RadioSprite&gt;</span> algo como, si guardo en 1100 el $00A0, va a guardar el $7F en $00A2<br>
<span class="Estilo4">[04:06] &lt;@RadioSprite&gt;</span> (por ejemplo.)<br>
<span class="Estilo4">[04:06] &lt;@RadioSprite&gt;</span> Entonces busco en la subrutina alg&uacute;n salto (t&iacute;picamente JSL o JSR)<br>
<span class="Estilo4">[04:07] &lt;@RadioSprite&gt;</span> Encontr&eacute; una, abr&iacute; la ROM, y 
sobrescrib� cada byte (** de JSL/JSR) con $EA, el cual cuando es traducido dentro de 65816 es una instrucci&oacute;n &quot;NOP&quot; o &quot;No Operation&quot; (** usar un editor Hex)<br>
<span class="Estilo4">[04:07] &lt;@RadioSprite&gt;</span> usando esa informaci&oacute;n, ve que puedes encontrar.<br>
<span class="Estilo4">[04:07] &lt;satsu&gt;</span> hm, ok...<br>
<span class="Estilo4">[04:07] &lt;@RadioSprite&gt;</span> ponemos NOPs a la rutina entonces la testeamos y &quot;Sistem&aacute;ticamente la hacemos caer.&quot;<br>
<span class="Estilo4">[04:08] &lt;@RadioSprite&gt; </span>Si le pones esos breaks y algo que deber&iacute;a pasar no sucede (como que 
una ventana no aparezca, 
por ejemplo) entonces sabes que le diste al clavo.<br>
<span class="Estilo4">[04:08] &lt;satsu&gt;</span> ok.<br>
<span class="Estilo4">[04:14] &lt;@RadioSprite&gt; </span>sigamos<br>
<span class="Estilo4">[04:15] &lt;@RadioSprite&gt;</span> satsu- &iquest;donde estaba la rutina que le pusiste 
los breaks?<br>
<span class="Estilo4">[04:15] &lt;satsu&gt;</span> �para el fondo? (** El fondo 
del di�logo o el fondo de la ventana azul inferior?<br>
<span class="Estilo4">[04:15] &lt;@RadioSprite&gt;</span> Te puedo decir si te estas 
cerca.<br>
<span class="Estilo4">[04:15] &lt;@RadioSprite&gt;</span> si.<br>
<span class="Estilo4">[04:15] &lt;satsu&gt;</span> espera un segundo.<br>
<span class="Estilo4">[04:15] &lt;@RadioSprite&gt;</span> naturalmente.<br>
<span class="Estilo4">[04:16] &lt;satsu&gt;</span> est&aacute; en 13651F.<br>
<span class="Estilo4">[04:16] &lt;@RadioSprite&gt;</span> Creo que te pasaste, pero 
yo podr&iacute;a estar equivocado.<br>
<span class="Estilo4">[04:16] &lt;satsu&gt;</span> espera, &iquest;en cual notaci&oacute;n 
debe estar? X_X<br>
<span class="Estilo4">[04:16] &lt;@RadioSprite&gt;</span> hmm, un segundo.<br>
<span class="Estilo4">[04:17] &lt;@RadioSprite&gt;</span> 0-0<br>
<span class="Estilo4">[04:17] &lt;@RadioSprite&gt;</span> yo la convierto de atr&aacute;s para adelante<br>
<span class="Estilo4">[04:17] &lt;@RadioSprite&gt;</span> esa es $26/E51F LoROM, 
�cierto? <br>
<span class="Estilo4">[04:17] &lt;satsu&gt;</span> bien, en el trace es $26/E51F<br>
<span class="Estilo4">[04:17] &lt;satsu&gt;</span> si.<br>
<br><img src="imagenes/26E51F_log.PNG"><br>
** Como dice RadioSprite, buscamos por LDA/X/Y #1100 y por un LDA/X/Y #$7F y nos 
encontramos con esta rutina. Donde est� en verde es la direcci�n donde Satsu cree que hay que colocar los NOPs. <br>
<br>
<span class="Estilo4">[04:17] &lt;@RadioSprite&gt;</span> si, observa ya que te pasaste<br>
<span class="Estilo4">[04:18] &lt;@RadioSprite&gt;</span> pero tienes la idea, 
as�<br>
<span class="Estilo4">[04:18] &lt;@RadioSprite&gt;</span> que puede que te hayas perdido<br>
<span class="Estilo4">[04:18] &lt;@RadioSprite&gt;</span> ya que el JSL que necesitas hacerle break 
est� en $02/895F en el trace.<br>
<br><img src="imagenes/02895F_log.PNG"> <br>
** Si seguimos buscando solo por si acaso, por la traza alguna LDA/X/Y #$1100 
junto con un LDA/X/Y #7F y nos encontramos otra vez con la direcci�n 26:E61F, 
que deber�a ser la rutina que descomprime las fuentes.</p><hr>
<p class="Estilo3"><b>Colocando NOP al Salto que lleva a la Rutina</b></p>
<p class="Estilo3">  **$02/895F es la direcci�n a la cual se le hace el NOP. As� que abre la rom con 
un editor Hex y busca los bytes: A000F3221FE626 (en verde en la imagen superior)<br>
que son los que corresponden a LDY #$F300 y JSL $26E61F. Recuerda ver documento 
&quot;A 65816 Primer&quot; para saber los opcodes de JSL y otros.<br>
<br><img src="imagenes/buscando_02895F.PNG"> <br>
<br>
**Una vez encontrado, cambia los bytes que corresponden al salto JSL por EAs: <br>
<br><img src="imagenes/colocando_NOP.PNG" border="1">
<br>
<br>
**Graba y ve el juego en alg�n emulador (Snes9X o SNES), se ver� esto:<br>
<br><img src="imagenes/rom_con_NOP.PNG">
<br>
**Estamos ok, no se cargaron las tiles inferiores por los EA (NOP) colocados.<br>
<br>
<span class="Estilo4">[04:18] &lt;@RadioSprite&gt;</span> Afortunadamente para ti, esto lo hace la DMA 
para que todo lo de VRAM se haga despu�s de ese JSL, as&iacute; que podemos hacer lo que diablos queramos 
con el.<br>
<span class="Estilo4">[04:18] &lt;satsu&gt;</span> &iexcl;Woohoo!</p><hr>
<p class="Estilo3"><b>Copiando Mis Propias Tiles a la Rom</b></p>
<p class="Estilo3">  
<span class="Estilo4">[04:19] &lt;@RadioSprite&gt;</span> Ahora, si no tienes una fuente de 2bpp Pre-preparada para meterla dentro de la ROM (** realizar una copia de Tiles)<br>
<span class="Estilo4">[04:19] &lt;@RadioSprite&gt;</span> vas a tener que sacarla de la RAM<br>
<span class="Estilo4">[04:19] &lt;@RadioSprite&gt;</span> (o de VRAM, si es mas f&aacute;cil)<br>
<span class="Estilo4">[04:19] &lt;satsu&gt;</span> ok, espera un segundo.<span class="Estilo4"><br>
[04:19] * satsu</span> abre tlp (**Tile Layer Pro)<br>
<span class="Estilo4">[04:19] &lt;@RadioSprite&gt;</span> usa lo que normalmente usas para copiar fuentes, tlp, hex workshop, lo que sea<br>
<span class="Estilo4">[04:21] &lt;@RadioSprite&gt; </span>C�pialas dentro de la 
ROM en $FF000<br>
<br><img src="imagenes/espacio_0FF000.PNG" width="382" height="312"><br>** Aqu� es donde insertaremos 
nuestras fuentes usando Tile Layer Pro. Para esto abre el archivo &quot;Eien no 
Filena (J)_orig.zs1&quot; y la rom &quot;Filena.smc&quot;. Deja ambas en formato GameBoy. 
En el ZST ve con CONTROL + G (Goto) a la direcci�n $2CC13 y con la Rom ve a $FF000. 
Ahora desde el ZST 
copiamos todas las tiles desde $2CC13 a $2DC13 a la Rom. <br>
Una manera f�cil es 
seleccionando las tiles y con CONTROL+C copiamos y luego cambiamos a la ROM y 
presionas CONTROL+V para pegarlas all�.
<br><img src="imagenes/tlp_tiles_a_copiar.PNG" width="684" height="296"><br>
<br><img src="imagenes/tlp_tiles_copiadas.PNG" width="446" height="327"><br>
Las tiles copiadas caben justo en ese espacio que se ve en rojo. No se den 
sobreescribir aquellas tiles que no sean negras o vac�as.
Salvamos la rom y listo.
<span class="Estilo4"><br>
<br>
[04:21] &lt;satsu&gt;</span> ok.<br>
<span class="Estilo4">[04:21] &lt;@RadioSprite&gt;</span> y ya que estas usando el TLP, -aseg&uacute;rate- de que est&eacute; en el mismo formato que el savestate<br>
<span class="Estilo4">[04:22] &lt;satsu&gt;</span> demonios, desear&iacute;a librarme de esos molestos toolbars del Tile Layer Pro &not;&not;<br>
<span class="Estilo4">[04:23] &lt;@RadioSprite&gt;</span> Si, en verdad.<br>
<span class="Estilo4">[04:26] &lt;satsu&gt; </span>ok, ya est&aacute;n copiadas en la rom �y 
luego?</p><hr>
<p class="Estilo3"><b>Ver donde colocar la Rutina DMA</b></p>
<p class="Estilo3">  <span class="Estilo4">[04:27] &lt;@RadioSprite&gt; </span>Muy bien, d&eacute;jame comentar esta rutina para ti. Desde 
aqu� es b&aacute;sicamente llenar los espacios en blanco. (** @RadioSprite est&aacute; haciendo un archivo de texto, enf-dma.txt, con una rutina dentro)<br>
<span class="Estilo4">[04:27] &lt;@RadioSprite&gt;</span> no obstante, vas a tener que darme unos pocos minutos, no tengo memorizado todos los registros DMA y tengo que hojear mis documentos<br>
<span class="Estilo4">[04:27] &lt;satsu&gt; </span>ok. mientras tanto, me voy a reconectar ya que el tiempo sobre esta sesi&oacute;n, esta 
yendo lento. (** se refiere al IRC)<br>
<span class="Estilo4">[04:28] &lt;satsu&gt;</span> en otras palabras, voy a llenar los metros (** 
�va al ba�o?). :P<br>
<span class="Estilo4">[04:28] &lt;@RadioSprite&gt;</span> hahah, muy bien.<br>
[04:28] *** Desconectado<br>
[04:31] *** Intentando reconectar...<br>
[04:31] *** Reconectado al canal #romhACKing<br>
<span class="Estilo4">[04:31] &lt;@RadioSprite&gt;</span> ooh, estas de suerte<br>
<span class="Estilo4">[04:31] &lt;@RadioSprite&gt;</span> M activa el Acumulador (** Registro Flag P)<br>
<span class="Estilo4">[04:32] &lt;satsu&gt;</span> recu&eacute;rdame que significa eso. ^^;<br>
<span class="Estilo4">[04:32] &lt;@RadioSprite&gt;</span> mejor dicho, M setea el estado del registro<br>
<span class="Estilo4">[04:32] &lt;@RadioSprite&gt; </span>lo que significa que el acumulador siempre va a leer un solo byte 
y no 2 como cuando lee en modo de 16-bit (2-byte). (** si M=1 =&gt; Acumulador A en modo de 8 bits, M=0 =&gt; A en modo de 16 bits)<br>
<span class="Estilo4">[04:32] &lt;@RadioSprite&gt; </span>En tu trace si miras donde dice &quot;P:&quot; podr&aacute;s ver lo que esta haciendo el registro de estado<br>
<font color="#0000FF">[04:33] &lt;@RadioSprite&gt;</font> si la letra est� en may&uacute;scula, entonces 
ese registro est� activado<br>
<font color="#0000FF">[04:33] &lt;satsu&gt;</font> ok.<br>
<br><img src="imagenes/registro_P.PNG" border="1"><br>
**En la imagen se ve el registro de estado P, donde se ve M est� en may�scula, lo 
que indica que est� activada. <br>
M es el flag del acumulador y X el flag de los registros �ndices X e Y. <br>
<br>
<font color="#0000FF">[04:33] &lt;@RadioSprite&gt;</font> Si no est&aacute; activada, tienes que hacer SEP #$20 para activarla 
(** SEP #$20 deja M en 1 y REP #$20 deja M=0)<br>
<font color="#0000FF">[04:34] &lt;@RadioSprite&gt;</font> entonces si deseas asegurarte hace un REP #$20 para limpiar el registro 
cuando hagas algo as&iacute; no interfiere con las operaciones normales<br>
<font color="#0000FF">[04:34] &lt;@RadioSprite&gt;</font> (siempre tienes que poner las cosas de vuelta 
de como las encuentras,&nbsp;como dicen los viejos refranes de las mam�s)<br>
<font color="#0000FF">[04:34] &lt;satsu&gt;</font> ok. :P<br>
<font color="#0000FF">[04:42] &lt;@RadioSprite&gt;</font> Casi lo tengo hecho.<br>
<font color="#0000FF">[04:43] &lt;satsu&gt;</font> ok.<br>
<font color="#0000FF">[04:47] &lt;@RadioSprite&gt;</font> &iquest;Se puede puede afirmar que sabes como cargar y guardar constantes en 65816?<br>
<font color="#0000FF">[04:48] &lt;satsu&gt;</font> en verdad no. vamos r&aacute;pidamente con ello.<br>
<font color="#0000FF">[04:48] &lt;@RadioSprite&gt;</font> bien, entonces<br>
<font color="#0000FF">[04:48] &lt;@RadioSprite&gt;</font> dado que el bit M bit es activado en el registro de estado (** registro P), vamos a trabajar con un acumulador de 8-bit<br>
<font color="#0000FF">[04:48] &lt;@RadioSprite&gt;</font> (el cual, es en verdad bueno para nuestros prop&oacute;sitos.)<br>
<font color="#0000FF">[04:48] &lt;@RadioSprite&gt;</font> LDA carga el acumulador con un 
valor<br>
<font color="#0000FF">[04:49] &lt;@RadioSprite&gt;</font> Dado que el acumulador es cargado tan pronto como nuestra rutina se resuelve (ve el Trace, baja al RTL y despu&eacute;s del banco $26 pasa a ser de nuevo $02s)<br>
<br><img src="imagenes/02s.PNG" border="1">
<font color="#0000FF"><br>
<br>
[04:50] &lt;@RadioSprite&gt;</font> no necesitamos preservar el valor en el acumulador. &iexcl;Hurra por nosotros!<br>
<font color="#0000FF">[04:50] &lt;satsu&gt;</font> &iexcl;si! somos geniales.<br>
<font color="#0000FF">[04:50] &lt;@RadioSprite&gt;</font> Ahora, si quieres cargar el acumulador con un valor guardado en ROM...(** Un valor no constante)<br>
<font color="#0000FF">[04:50] &lt;@RadioSprite&gt;</font> dado que trabajamos con una LoROM, puedes hacer LDA $8000<br>
<font color="#0000FF">[04:50] &lt;@RadioSprite&gt;</font> que carga en A lo que est&eacute; en $(DB)/8000<br>
<font color="#0000FF">[04:51] &lt;@RadioSprite&gt;</font> donde (DB) es el contenido del Registro Databank, el DBR<br>
<font color="#0000FF">[04:51] &lt;@RadioSprite&gt;</font> o DB: en nuestros traces (** En el Log se ve DB:02)<br>
<font color="#0000FF">[04:51] &lt;satsu&gt;</font> ok...<br>
<font color="#0000FF">[04:51] &lt;@RadioSprite&gt;</font> una forma f&aacute;cil de cambiar que cargue A con una constante, 
agregarlo al Stack y luego sacar el DBR afuera del Stack<br>
<font color="#0000FF">[04:51] &lt;@RadioSprite&gt;</font> si queremos cargar el acumulador con una constante, decimos LDA #$80<br>
<font color="#0000FF">[04:52] &lt;@RadioSprite&gt;</font> que copia el valor #$80 dentro del acumulador<br>
<font color="#0000FF">[04:52] &lt;@RadioSprite&gt;</font> el signo # significa una constante<br>
<font color="#0000FF">[04:52] &lt;@RadioSprite&gt;</font> (tambi&eacute;n nota que podemos hacer LDA #$808000 para cargar directamente desde la rom sin considerar lo que est&eacute; en DBR, deb&iacute; haberlo mencionado antes.)<br>
<font color="#0000FF">[04:52] &lt;@RadioSprite&gt;</font> er<br>
<font color="#0000FF">[04:52] &lt;@RadioSprite&gt;</font> quise decir LDA $808000. (** LDA #$808000 no se puede 
hacer, ya que estar&iacute;as metiendo en A 32 bits, y solo soporta como m&aacute;ximo 16 bits)<br>
<font color="#0000FF">[04:52] &lt;satsu&gt;</font> ok.<br>
<font color="#0000FF">[04:53] &lt;@RadioSprite&gt;</font> Pero dado que solo cargaremos lo que haya en $808000, si lo que hay en $808000 es una constante (Read Only Memory, recuerda) ser&iacute;a algo sin sentido. 
(** muy importante ya que ya cuando se hace por ejemplo LDA $0A, si adentro hay 
una constante 1, es mejor hacer directamente LDA #$01. LDA $XXXXXX se usa para 
almacenar valores variables)<br>
<font color="#0000FF">[04:53] &lt;@RadioSprite&gt;</font> No quiero entrar en cargas indexadas o indirectas, eso ser&iacute;a tema de otra clase.<br>
<font color="#0000FF">[04:53] &lt;satsu&gt;</font> as&iacute; que por lo visto, &iquest;buscaremos donde dice &quot;saca esto de la RAM&quot;, y cambiarlo por &quot;saca esto de la ROM&quot;?<br>
<font color="#0000FF">[04:53] &lt;@RadioSprite&gt;</font> ...mas o menos, pero no exactamente.<br>
<font color="#0000FF">[04:54] &lt;@RadioSprite&gt; </font>Vamos a sobreescribir la rutina que saca desde la rom y hace todo una &quot;cosa&quot; binaria y lo copia a RAM con nuestra propia rutina que solo saca datos de ROM y los copia a RAM sin la &quot;cosa&quot; binaria.<br>
<font color="#0000FF">[04:55] &lt;satsu&gt;</font> ah, entiendo.<br>
<font color="#0000FF">[04:55] &lt;@RadioSprite&gt;</font> tienes que entender la idea<br>
<font color="#0000FF">[04:55] &lt;satsu&gt;</font> eso fue de hecho lo que pensaba, antes de que me dijeras acerca de cargar directamente de la rom. ^^<br>
<font color="#0000FF">[04:55] &lt;@RadioSprite&gt;</font> pero en vez de sacarla de la ram, vamos a sacarla de la rom y ponerla en la ram.<br>
<font color="#0000FF">[04:55] &lt;@RadioSprite&gt;</font> Bien, podr&iacute;amos cargar directamente desde rom, pero eso es lento y tedioso<br>
<font color="#0000FF">[04:56] &lt;@RadioSprite&gt;</font> &iexcl;as&iacute; que usaremos las capacidad de transferencia de alta velocidad de la SNES!<br>
<font color="#0000FF">[04:56] &lt;satsu&gt;</font> &iexcl;y nadie quiere cosas tediosas!<br>
<font color="#0000FF">[04:56] &lt;satsu&gt; </font>ok.<br>
<font color="#0000FF">[04:56] &lt;@RadioSprite&gt;</font> Escribimos unos pocos bytes en registros especiales y 
con eso hacemos todo mas r&aacute;pido que si lo hici�ramos con una rutina diferente.<br>
<font color="#0000FF">[04:56] &lt;@RadioSprite&gt; </font>Por ahora, todo lo que necesitamos saber es que JSL &lt;direcci&oacute;n $24 bit&gt; saltar&aacute; a una nueva subrutina de 24 bit que especificaremos 
(** saltar� a nuestra propia rutina)<br>
<font color="#0000FF">[04:57] &lt;@RadioSprite&gt;</font> RTL nos retornar&aacute; de la subrutina<br>
<font color="#0000FF">[04:57] &lt;satsu&gt;</font> ok.<br>
<font color="#0000FF">[04:57] &lt;@RadioSprite&gt;</font> ese LDA #$80 cargar&aacute; la constante #$80 dentro del acumulador<br>
<font color="#0000FF">[04:57] &lt;@RadioSprite&gt;</font> y el STA $direcci&oacute;n copia el contenido del acumulador dentro de la direcci&oacute;n que especificamos.<br>
<font color="#0000FF">[04:57] &lt;@RadioSprite&gt;</font> En este caso, las direcciones que especificamos son registros especiales que controlan transferencia DMA de alta velocidad de la SNES.<br>
<font color="#0000FF">[04:58] &lt;satsu&gt;</font> (fanfarrea)<br>
<font color="#0000FF">[04:58] &lt;@RadioSprite&gt;</font> nota que asm2hex es un programa en DOS, estamos bajo la restricci&oacute;n 8.3 (** 
&quot;cada nombre de archivo debe tener un m&aacute;ximo de ocho caracteres de nombre propiamente dicho m&aacute;s un m&aacute;ximo de tres caracteres de extensi&oacute;n, separada del nombre por un punto&quot;)<br>
<font color="#0000FF">[04:58] &lt;@RadioSprite&gt;</font> al cual casi siempre me apego por si acaso<br>
<font color="#0000FF">[04:59] &lt;satsu&gt;</font> ok.<br>
<font color="#0000FF">[04:59] &lt;@RadioSprite&gt;</font> En el archivo de texto he explicado como el DMA funciona, con mi rutina de ejemplo<br>
<font color="#0000FF">[04:59] &lt;@RadioSprite&gt;</font> as&iacute; que todo lo que necesitas es llenar los espacios en blanco de &quot;asm enf-dma.txt&quot; y deber&iacute;a andar.<br>
<font color="#0000FF">[04:59] &lt;satsu&gt;</font> &iexcl;vale!<br>
<font color="#0000FF">[05:00] &lt;@RadioSprite&gt;</font> Hazme saber si tienes problemas, llenar&eacute; los blanco por mi cuenta.<br>
&nbsp;</p><hr>
<p class="Estilo3"><b>Rutina DMA enf-dma.txt<br>
<br>
**</b>La Rutina propuesta y que debes completar los espacios en blanco, se puede ver 
<a href="recursos/enf-dma-esp.txt">aqu�</a>.<br>
<br>
<font color="#0000FF">[05:00] &lt;satsu&gt; </font>muy bien.<br>
<font color="#0000FF">[05:03] &lt;@RadioSprite&gt;</font> Bingo, consegu&iacute; que funcionara ^_^<br>
<font color="#0000FF">[05:04] &lt;satsu&gt;</font> tardar&eacute; unos pocos minutos, voy a tratar de entenderlo. Creo que lo estoy pillando. ^^<br>
<font color="#0000FF">[05:05] &lt;@RadioSprite&gt;</font> &iexcl;seguro!<br>
<font color="#0000FF">[05:05] &lt;satsu&gt;</font> &iquest;son los &uacute;ltimos 3 espacios #$00, #$F0 y #$9F?<br>
<font color="#0000FF">[05:07] &lt;@RadioSprite&gt;</font> ESTAS CERCA.<br>
[<font color="#0000FF">05:07] &lt;@RadioSprite&gt;</font> Pero no mucho.<br>
<font color="#0000FF">[05:07] &lt;@RadioSprite&gt;</font> Filerna es un LoROM $00, y no LoROM $80.<br>
<font color="#0000FF">[05:07] &lt;satsu&gt;</font> ah, ya veo.<br>
<font color="#0000FF">[05:07] &lt;@RadioSprite&gt;</font> (Puedes confirmarlo mirando los tracelogs, si todos los bancos 
fueran de $80 hacia arriba, estar�as en lo correcto.) (** En el LOG se ve que 
los banco son 00/, 02/.., es decir, una LoRom $00)<br>
<font color="#0000FF">[05:08] &lt;@RadioSprite&gt;</font> Y pareces que 
olvidaste lo del largo en los espacios en blanco :)<br>
<font color="#0000FF">[05:08] &lt;satsu&gt; </font>oh si.<br>
<font color="#0000FF">[05:08] &lt;satsu&gt; </font>no me di cuenta de los scrollbar, oops.<br>
<font color="#0000FF">[05:09] &lt;satsu&gt;</font> diablos, escrib&iacute; abajo el n&uacute;mero de bytes cuando copiaba y pegaba del savestate. :P<br>
<font color="#0000FF">[05:10] &lt;satsu&gt;</font> &iquest;quieres el SPC de Filerna mientras esperas? :P (** SPC: dumpeo del chip SPC700 que se encarga de la m&uacute;sica de la SNES)<br>
<font color="#0000FF">[05:10] &lt;@RadioSprite&gt;</font> heh ^_^<br>
<font color="#0000FF">[05:11] &lt;satsu&gt;</font> ok. &iquest;el largo de lo 
bancos son #$30 y #$0F?<br>
<font color="#0000FF">[05:11] &lt;@RadioSprite&gt;</font> er, no :p<br>
<font color="#0000FF">[05:11] &lt;@RadioSprite&gt;</font> ni cerca ^^;<br>
<font color="#0000FF">[05:12] &lt;satsu&gt;</font> hm.<br>
  <font color="#0000FF">[05:12]</font> * satsu relee el comentario <br>
  <font color="#0000FF">[05:13] &lt;satsu&gt;</font> hm, estoy algo perdido<br>
  <font color="#0000FF">[05:13] &lt;@RadioSprite&gt;</font> bien entonces<br>
  <font color="#0000FF">[05:13] &lt;@RadioSprite&gt; </font>&iquest;qu&eacute; tan grande es el trozo de datos?<br>
  <font color="#0000FF">[05:14] &lt;@RadioSprite&gt;</font> estas transfiriendo la fuente completa, verdad<br>
  <font color="#0000FF">[05:14] &lt;@RadioSprite&gt;</font> &iquest;comienza en $FF000, y termina donde? (** Recuerda que satsu copio all&iacute; sus fuentes, 
al igual que nosotros)<br>
  <font color="#0000FF">[05:14] &lt;satsu&gt;</font> si.<br>
  <font color="#0000FF">[05:14] &lt;satsu&gt;</font> $FFFD0 (** FFFD0 - FF000 = 
$FD0 bytes)<br>
  <font color="#0000FF">[05:14] &lt;@RadioSprite&gt;</font> no muy cerca<br>
  <font color="#0000FF">[05:14] &lt;@RadioSprite&gt;</font> debiste olvidar accidentalmente algunas tiles<br>
  <font color="#0000FF">[05:15] &lt;@RadioSprite&gt;</font> salva en la batalla, y saca toda la fuente de la VRAM <br>
  <font color="#0000FF">[05:15] &lt;satsu&gt;</font> ok, espera.<br>
  <font color="#0000FF">[05:17] &lt;satsu&gt;</font> gracias a dios que en di&aacute;logo de la batalla no usan fuentes separadas. &not;&not;<br>
  <font color="#0000FF">[05:17] &lt;@RadioSprite&gt;</font> hahah, si, eso requerir&iacute;a 
algo de magia para corregirlo<br>
  <font color="#0000FF">[05:18] &lt;satsu&gt;</font> si. aunque no tengo la sombra en la batalla. :(<span lang="es">&nbsp; 
(** al parecer sastu copi� las tiles sin la sombra)</span><br>
  <font color="#0000FF">[05:18] &lt;@RadioSprite&gt;</font> No creo que sea algo que pueda ense&ntilde;arte en una simple sesi&oacute;n de tutorial, esta ya ha durado 2 horas<br>
  <font color="#0000FF">[05:18] &lt;satsu&gt;</font> si, en verdad. :P<br>
  <font color="#0000FF">[05:18] &lt;@RadioSprite&gt;</font> ...yo le agrego 
la sombra por ti<br>
<font color="#0000FF">[05:18] &lt;@RadioSprite&gt;</font> pero no creo que quiera intentarlo<br>
  <font color="#0000FF">[05:18] &lt;satsu&gt;</font> ah, no te preocupes por esto.<br>
  <font color="#0000FF">[05:19] &lt;satsu&gt;</font> hm, parece que tengo la fuente completa. x_x<br>
  <font color="#0000FF">[05:19] &lt;@RadioSprite&gt;</font> es extra&ntilde;o<br>
  <font color="#0000FF">[05:20] &lt;@RadioSprite&gt;</font> &iquest;donde est&aacute;, hmm?<br>
  <font color="#0000FF">[05:20] &lt;satsu&gt;</font> &iquest;Donde termina?<br>
  <font color="#0000FF">[05:20] &lt;@RadioSprite&gt;</font> los &uacute;ltimos tiles deber&iacute;an ser los bordes de pantalla<br>
  <font color="#0000FF">[05:20] &lt;@RadioSprite&gt;</font> el &uacute;ltimo deber&iacute;a estar en blanco 
(** f�jate que el �ltimo tile que copiamos est� vac�o)<br>
  <font color="#0000FF">[05:20] &lt;satsu&gt;</font> si, solo agregu&eacute; 2 tiles blancas solo para asegurarme.<br>
  <font color="#0000FF">[05:20] &lt;@RadioSprite&gt;</font> &iquest;Asegurarte de qu&eacute;? o_O<br>
  <font color="#0000FF">[05:21] &lt;@RadioSprite&gt;</font> &iquest;de lo que est&aacute; mostrando?<br>
  <font color="#0000FF">[05:21] &lt;satsu&gt;</font> no lo se, quiz&aacute;s se usen en alg&uacute;n lugar. :P<br>
  <font color="#0000FF">[05:21] &lt;@RadioSprite&gt;</font> &iquest;entonces est&aacute;s aun intentando hacer un break para asegurarte que funcione?<br>
  <font color="#0000FF">[05:21] &lt;@RadioSprite&gt;</font> si es eso lo que estas haciendo, est&aacute; bien<br>
  <font color="#0000FF">[05:21] &lt;@RadioSprite&gt;</font> pero pong&aacute;monos en el caso de que se transfieren todo el bloque de $1000 de datos<br>
  <font color="#0000FF">[05:22] &lt;satsu&gt;</font> #$00 y #$10, bien<br>
  <font color="#0000FF">[05:22] &lt;@RadioSprite&gt; </font>porque probablemente necesitar&aacute;s poner esos tiles de vuelta, y te preguntar&aacute;s porque no se est&aacute;n mostrando:p<br>
  <font color="#0000FF">[05:22] &lt;@RadioSprite&gt;</font> si.<br>
  <font color="#0000FF">[05:23] &lt;satsu&gt;</font> hm, espera un segundo.<br>
  <font color="#0000FF">[05:26] &lt;satsu&gt; </font>todav&iacute;a tengo que llenar mi primer set de blancos. Vamos a escribir la misma direcci&oacute;n en la RAM como 
estaba en el juego, �verdad? (** 0F:F000)<br>
  <font color="#0000FF">[05:28] &lt;@RadioSprite&gt;</font> Correcto.<br>
  <font color="#0000FF">[05:28] &lt;@RadioSprite&gt;</font> O, si prefieres, puedes escribir en otro lado y ver que vomita el juego.<br>
  <font color="#0000FF">[05:28] &lt;satsu&gt;</font> quiz&aacute;s para otra ocasi&oacute;n.<br>
  <font color="#0000FF">[05:29] &lt;satsu&gt; </font>&iquest;es 7F:1121, verdad?<br>
  <font color="#0000FF">[05:29] &lt;satsu&gt;</font> espera, creo que me perd&iacute; de nuevo. :P<br>
  <font color="#0000FF">[05:29] &lt;@RadioSprite&gt;</font> si :p<br>
  <font color="#0000FF">[05:30] &lt;@RadioSprite&gt; </font>excepto que quieras hacer que el juego vomite ^_<br>
  <font color="#0000FF">[05:30] &lt;@RadioSprite&gt;</font> ^<br>
  <font color="#0000FF">[05:30] * @RadioSprite</font> murmulla algo como &quot;editando esto luego&quot;<br>
  <font color="#0000FF">[05:30] * satsu</font> escribe futuros mensajes en c&oacute;digo borroso pata futuras personas<br>
  <font color="#0000FF">[05:31] &lt;satsu&gt;</font> ok. as&iacute; que, en el savestate, la fuente empieza en 011D33. As&iacute; que resto C12 desde el inicio, &iquest;si?<br>
  <font color="#0000FF">[05:32] &lt;@RadioSprite&gt;</font> resta $C13.<br>
  <font color="#0000FF">[05:32] &lt;satsu&gt;</font> ah, ok.<br>
  <font color="#0000FF">[05:32] &lt;@RadioSprite&gt;</font> y este no comienza en verdad en 011D33<br>
  <font color="#0000FF">[05:32] &lt;@RadioSprite&gt;</font> comienza en 011D13, pero all&iacute; solo hay 2 tiles en blanco<br>
  <font color="#0000FF">[05:32] &lt;satsu&gt;</font> !!<br>
  <font color="#0000FF">[05:33] &lt;satsu&gt;</font> ok, despu�s de hacer esto, 
necesito corregir mi ROM. :P<br>
  <font color="#0000FF">[05:33] &lt;@RadioSprite&gt;</font> generalmente hablando, la 
mayor�a de los juegos escriben direcciones con al menos 2 ceros al final :p<br>
  <font color="#0000FF">[05:33] &lt;@RadioSprite&gt;</font> heheh ^^;<br>
  <font color="#0000FF">[05:33] &lt;satsu&gt;</font> ok, de cualquier forma, despu&eacute;s de restar lo del header tengo 011120. (** 011D33 - C13) As&iacute; que solo tengo que convertir eso dentro de direcci&oacute;n RAM, &iquest;verdad?<br>
  <font color="#0000FF">[05:35] &lt;@RadioSprite&gt;</font> Otra vez estas trabajando con la vieja direcci&oacute;n :p<br>
  <font color="#0000FF">[05:35] &lt;satsu&gt;</font> oh si.<br>
  <font color="#0000FF">[05:35] &lt;@RadioSprite&gt;</font> Necesitas estar trabajando con 011D13, recuerda<br>
  <font color="#0000FF">[05:35] &lt;satsu&gt;</font> ok, entonces luego de hacerlo obtuve 011100.<br>
  <font color="#0000FF">[05:36] &lt;satsu&gt;</font> la cual es 7f:1100, &iquest;cierto?<br>
  <font color="#0000FF">[05:36] &lt;@RadioSprite&gt;</font> bien<br>
  <font color="#0000FF">[05:36] &lt;@RadioSprite&gt; </font>bien, pero mejor re-lees los comentarios antes de meter 7f1100 dentro de los registros&nbsp; 
(** se refiere a los comentarios del txt)<br>
  <font color="#0000FF">[05:36] &lt;satsu&gt;</font> si, lo tengo<br>
  <font color="#0000FF">[05:36] &lt;satsu&gt;</font> as&iacute; &iquest;lo que va dentro de los espacios es 00, 11 y 01?<br>
  <font color="#0000FF">[05:37] &lt;@RadioSprite&gt;</font> &iexcl;si!<br>
  <font color="#0000FF">[05:37] &lt;satsu&gt;</font> &iexcl;yay!<br>
  <font color="#0000FF">[05:37] * satsu</font> se r&iacute;e burlonamente<br>
  <font color="#0000FF">[05:38] &lt;satsu&gt;</font> mientras ordeno esta fuente, &iquest;debo enviarte lo que tengo?<br>
  <font color="#0000FF">[05:38] &lt;satsu&gt;</font> mas f&aacute;cil que leer los blancos y copiar trozos de c&oacute;digos a la vez.<br>
  <font color="#0000FF">[05:38] &lt;@RadioSprite&gt; </font>�funciona?<br>
  <font color="#0000FF">[05:38] &lt;@RadioSprite&gt;</font> nah<br>
  <font color="#0000FF">[05:38] &lt;satsu&gt;</font> no lo he intentado aun, porque tengo que corregir la fuente.<br>
  <font color="#0000FF">[05:38] &lt;satsu&gt;</font> ok.<br>
  <font color="#0000FF">[05:38] &lt;@RadioSprite&gt;</font> sabr&aacute;s si est&aacute; mala<br>
  <font color="#0000FF">[05:38] &lt;satsu&gt;</font> heh. :P<br>
  <font color="#0000FF">[05:42] &lt;satsu&gt;</font> ok. espero que funcione. :P<br>
  <font color="#0000FF">[05:43] &lt;satsu&gt;</font> &iquest;la sintaxis para ASM.exe es 
<b>ASM enf-dma.txt filerna.smc?</b><br>
  <font color="#0000FF">[05:43] * @RadioSprite</font> cruza los dedos<br>
  <font color="#0000FF">[05:43] &lt;@RadioSprite&gt;</font> no<br>
  <font color="#0000FF">[05:44] &lt;@RadioSprite&gt;</font> ya que ya hay una linea con %filename en el enf-dma.txt<br>
  <font color="#0000FF">[05:44] &lt;@RadioSprite&gt;</font> puedes omitir el filerna.smc<br>
  <font color="#0000FF">[05:44] &lt;satsu&gt;</font> ok.<br>
  <font color="#0000FF">[05:44] &lt;satsu&gt;</font> hm, me dice que tengo un error de sintaxis. :P<br>
  <font color="#0000FF">[05:45] &lt;satsu&gt;</font> &quot;enf-dma.txt: 3: syntax error at or before '#'.<br>
  <font color="#0000FF">[05:45] &lt;satsu&gt;</font> #ROMTYPE LOROM<br>
  <font color="#0000FF">[05:45] &lt;satsu&gt;</font> demonios, lo recuerdo de LaL. :P (** LaL ?)<br>
<br><img src="imagenes/error_romtype.PNG" border="1">
  <font color="#0000FF"><br>
</font> **Usa la versi�n ASM2Hex que est� en la carpeta recursos y no tendr�s 
problemas. <br>
Usalo en DOS as�: <b>&gt;asm enf-dma-esp.txt</b><font color="#0000FF"><br>
<br>
[05:46] &lt;@RadioSprite&gt;</font> si, parece que tienes una versi&oacute;n antigua del asm2hex<br>
  <font color="#0000FF">[05:46] &lt;@RadioSprite&gt;</font> y desafortunadamente, lordtech 
no le puso versi�n<br>
  <font color="#0000FF">[05:46] &lt;satsu&gt;</font> lo saqu&eacute; de romhacking.com...<br>
  <font color="#0000FF">[05:46] &lt;satsu&gt;</font> digo .org<br>
  <font color="#0000FF">[05:46] &lt;@RadioSprite&gt;</font> d&eacute;jame enviarte la versi&oacute;n que yo tengo, lo pondr&eacute; en el paquete con el log<br>
  <font color="#0000FF">[05:46] &lt;satsu&gt;</font> ok.<br>
  <font color="#0000FF">[05:46] &lt;satsu&gt;</font> ehh, est� aclarando afuera, creo que voy a dormir mucho hoy.<br>
  <font color="#0000FF">[05:47] &lt;@RadioSprite&gt;</font> vas a tener que 
hacer dccallow (**un comando de IRC)<br>
  <font color="#0000FF">[05:47] &lt;satsu&gt;</font> $^%%$&quot;&amp;^%&pound;&quot;^dccallow<br>
  <font color="#0000FF">[05:47] &lt;@RadioSprite&gt;</font> si &not;&not;<br>
  <font color="#0000FF">[05:47] &lt;satsu&gt;</font> %$&amp;^^%&pound;%&quot;$^agregado<br>
  <font color="#0000FF">[05:48] &lt;satsu&gt;</font> &iexcl;ok! &iexcl;vamos a probar!<br>
  <font color="#0000FF">[05:49] &lt;@RadioSprite&gt;</font> &iexcl;muy bien!<br>
  <font color="#0000FF">[05:49] &lt;satsu&gt;</font> &iexcl;por poco!<br>
  <font color="#0000FF">[05:49] &lt;satsu&gt;</font> parece ser que se perdieron algunos bordes de la ventana, y la marca -.<br>
  <font color="#0000FF">[05:49] &lt;@RadioSprite&gt;</font> &iquest;la marca -?<br>
  <font color="#0000FF">[05:49] &lt;satsu&gt;</font> para los katakana.<br>
  <font color="#0000FF">[05:50] &lt;satsu&gt;</font> hm, la marca de &quot; igual.<br>
  <font color="#0000FF">[05:50] &lt;@RadioSprite&gt;</font> hmm.<br>
  <font color="#0000FF">[05:50] &lt;satsu&gt;</font> y hiragana @_@<br>
  [<font color="#0000FF">05:50] &lt;@RadioSprite&gt;</font> &iquest;estas seguro que pusiste el largo correcto?<br>
  <font color="#0000FF">[05:51] &lt;satsu&gt;</font> deja chequearlo.<br>
  <font color="#0000FF">[05:52] &lt;satsu&gt; </font>hm. tengo 0A00 como largo, el cual estoy seguro que esta malo.<br>
  <font color="#0000FF">[05:52] &lt;satsu&gt;</font> probablemente un error temprano de c&aacute;lculo.<br>
  <font color="#0000FF">[05:53] &lt;@RadioSprite&gt;</font> heh, definitivamente es eso.<br>
  <font color="#0000FF">[05:53] &lt;satsu&gt;</font> &iquest;era 1000?<br>
  <font color="#0000FF">[05:55] &lt;@RadioSprite&gt;</font> sip<br>
  <font color="#0000FF">[05:55] &lt;satsu&gt;</font> hm. tengo 00 y 10 en los espacios en blanco, pero lo sigue mostrando mal.<br>
  <font color="#0000FF">[05:55] &lt;satsu&gt;</font> quiz&aacute;s tenga mala la direcci&oacute;n de lectura inicial, pero obtengo bien el katakana de &quot;filerna&quot;.<br>
  <font color="#0000FF">[05:56] &lt;@RadioSprite&gt;</font> &iquest;que tienes en los campos donde va la direcci&oacute;n de inicio?<br>
  <font color="#0000FF">[05:56] &lt;satsu&gt;</font> 00. F0 y 1F.<br>
  <font color="#0000FF">[05:57] &lt;@RadioSprite&gt; </font>eso deber&iacute;a estar bien.<br>
  <font color="#0000FF">[05:57] &lt;satsu&gt;</font> hmm.<br>
  <font color="#0000FF">[05:58] &lt;@RadioSprite&gt;</font> As&iacute; que tienes $00, $11, $01<br>
  <font color="#0000FF">[05:58] &lt;@RadioSprite&gt;</font> $00, $F0, $1F<br>
  <font color="#0000FF">[05:58] &lt;satsu&gt;</font> si.<br>
  <font color="#0000FF">[05:58] &lt;@RadioSprite&gt;</font> $00, $10<br>
  <font color="#0000FF">[05:58] &lt;satsu&gt;</font> sip.<br>
  <font color="#0000FF">[05:58] &lt;satsu&gt;</font> de nuevo creo que se me fue 
algo de la fuente.<br>
  <font color="#0000FF">[05:58] &lt;@RadioSprite&gt;</font> &iquest;y copiaste los $1000 completos en $FF0000? 
(** se equivoc� ya que es $FF000)<br>
  <font color="#0000FF">[05:59] &lt;satsu&gt;</font> si, esta todo all&iacute;.<br>
  <font color="#0000FF">[06:00] &lt;satsu&gt;</font> &iquest;comienza con 2 tiles blancas, verdad?<br>
  <font color="#0000FF">[06:00] &lt;@RadioSprite&gt;</font> Env&iacute;ame una imagen de tu ventana del TLP.<br>
  <font color="#0000FF">[06:00] &lt;@RadioSprite&gt; </font>en $FF000<br>
  <font color="#0000FF">[06:03] &lt;@RadioSprite&gt; </font>si, olvidaste copiar 
algo de la fuente de nuevo :p<br>
  <font color="#0000FF">[06:03] &lt;satsu&gt;</font> oh. &iquest;donde me equivoqu&eacute;?<br>
  <font color="#0000FF">[06:03] &lt;@RadioSprite&gt;</font> en la parte inferior pero (partiendo con la parte baja de la ventana de dialogo) deber&iacute;a ser 2 tiles m�s a la derecha<br>
  <font color="#0000FF">[06:04] &lt;@RadioSprite&gt;</font> quise decir la peque&ntilde;a parte de abajo<br>
  <font color="#0000FF">[06:04] &lt;@RadioSprite&gt;</font> el cual probablemente notar&aacute;s si miras de nuevo la fuente en VRAM<br>
  <font color="#0000FF">[06:04] &lt;@RadioSprite&gt;</font> $2CC13<br>
  <font color="#0000FF">[06:05] &lt;satsu&gt;</font> ok, gracias<br>
  <font color="#0000FF">[06:07] &lt;@RadioSprite&gt;</font> ah, pero<br>
  <font color="#0000FF">[06:07] &lt;@RadioSprite&gt;</font> �Me imagino que 
tienes tu fuente en vram? <br>
<font color="#0000FF">[06:07] &lt;@RadioSprite&gt;</font> as&iacute; que si quieres usa un viejo savestate o una rom no hackeada para sacar la fuente <br>
  <font color="#0000FF">[06:07] &lt;satsu&gt;</font> si, eso es lo que voy a hacer...<br>
  <font color="#0000FF">[06:08] &lt;@RadioSprite&gt;</font> ok, solo chequea ^_^<br>
  <font color="#0000FF">[06:08] &lt;@RadioSprite&gt;</font> veremos todas las 
alternativas<br>
<font color="#0000FF">[06:08] &lt;satsu&gt;</font> ah, veo mi ?? (**
<font color="#FF0000">slaik</font> ?).<br>
  <font color="#0000FF">[06:09] &lt;@RadioSprite&gt;</font> Eso es bueno, sin embargo, aprendes m�s con los errores.<br>
  <font color="#0000FF">[06:09] &lt;@RadioSprite&gt;</font> No me refiero a que nunca 
se aprende si no tienes errores, as&iacute; que probablemente no es verdad<br>
  <font color="#0000FF">[06:10] &lt;satsu&gt; </font>si. y espero que quien quiera que lea esto, est&aacute; aprendiendo de muchos de mis errores.<br>
  <font color="#0000FF">[06:10] &lt;@RadioSprite&gt;</font> pero tu puedes aprender de los errores, y espero que no cometas el mismo error 
otra vez<br>
  [<font color="#0000FF">06:10] &lt;@RadioSprite&gt;</font> o si no, tendr&aacute;s que reconocer tu error mejor.<br>
  <font color="#0000FF">[06:10] </font>*** Hakai-zo (ZenMaster@h0005022d946a.ne.client2.attbi.com) has joined #romhacking<br>
  <font color="#0000FF">[06:10] </font>*** ChanServ sets mode: +o Hakai-zo<br>
  <font color="#0000FF">[06:10] </font>&lt;@Hakai-zo&gt; Zen!<br>
  <font color="#0000FF">[06:10] </font>*** Hakai-zo (ZenMaster@h0005022d946a.ne.client2.attbi.com) has left #romhacking<br>
  <font color="#0000FF">[06:11] &lt;satsu&gt;</font> heh.<br>
  <font color="#0000FF">[06:12] &lt;satsu&gt;</font> &iexcl;victoria!<br>
  <font color="#0000FF">[06:12] &lt;@RadioSprite&gt;</font> &iexcl;Genial!</p>
<p class="Estilo3"><b>**</b>La Rutina resuelta se puede ver 
<a href="recursos/enf-dma-solucion.txt">aqu�</a>.<br>
<br><img src="imagenes/imagen_rom_final.PNG"><br>
**Si lo hiciste bien, el di�logo y el cuadro azul inferior se deber�an ver igual 
que antes. Ahora puedes probar a cambiar la fuente, algo que no pod�as antes por estar 
comprimida :)<br>
<img src="imagenes/tlp_cambiando_fuente.PNG" width="383" height="283" border="1"><img src="imagenes/tlp_cambiando_fuente_ok.PNG" width="383" height="283" border="1"><br>
**Ahora con DMA podemos cambiar las fuentes, agregar acentos u otros s�mbolos 
especiales.<br>
<br>
<font color="#0000FF">[06:12] &lt;@RadioSprite&gt; </font>Ahora, algo sabes de como hacer una transferencia DMA<br>
  <font color="#0000FF">[06:12] &lt;@RadioSprite&gt;</font> puedes irte a dormir 
contento �o no?<font color="#0000FF"><br>
[06:13] &lt;satsu&gt;</font> bien, no realmente.<br>
  <font color="#0000FF">[06:13] &lt;@RadioSprite&gt;</font> Ok, quiz�s no,
<font color="#FF0000">but it sure didn't hurt. (**?)</font><br>
  <font color="#0000FF">[06:13] &lt;satsu&gt;</font> creo que antes de ir a dormir editar� la fuente para que coincida con los cambios que hice a la fuente de 4bpp.<br>
  <font color="#0000FF">[06:13] * @RadioSprite</font> cabecea<br>
  <font color="#0000FF">[06:13] &lt;satsu&gt;</font> &iexcl;de todas formas!<br>
  <font color="#0000FF">[06:13] &lt;satsu&gt;</font> muchas gracias por la lecci&oacute;n. 
La aprecio mucho.<br>
  <font color="#0000FF">[06:13] &lt;@RadioSprite&gt;</font> y eso es todo.<br>
  <font color="#0000FF">[06:13] &lt;@RadioSprite&gt;</font> Espero haber sido un 
buen profesor.<br>
  <font color="#0000FF">[06:13] &lt;@RadioSprite&gt;</font> Es la primera vez que intento hacer algo como esto.<br>
  <font color="#0000FF">[06:14] &lt;satsu&gt;</font> @_@<br>
  <font color="#0000FF">[06:14] &lt;satsu&gt;</font> creo hiciste un buen trabajo.<br>
  <font color="#0000FF">[06:14] &lt;@RadioSprite&gt;</font> Idealmente, esta lectura particular deber&iacute;a ser parte de un curso, y no tendr&iacute;a que haber tratado el tema de modos de direccionamiento y como cargar y almacenar y notaciones y lo dem&aacute;s<br>
  <font color="#0000FF">[06:14] &lt;@RadioSprite&gt;</font> ni como encontrar un rutina<br>
  <font color="#0000FF">[06:15] &lt;satsu&gt;</font> bien, de todas formas eso estuvo bien ya que no sab&iacute;a ninguno de esos tema.<br>
  <font color="#0000FF">[06:15] &lt;@RadioSprite&gt;</font> pero en general, estoy contento como sali&oacute; esto.<br>
  <font color="#0000FF">[06:15] &lt;satsu&gt;</font> demonios, reci&eacute;n recuerdo que edit� los nombres de monstruos. Quiz&aacute;s dormir&eacute; antes de trabajar algo mas en el juego.<br>
  <font color="#0000FF">[06:15] &lt;satsu&gt;</font> Adem&aacute;s, ni siquiera puedo teclear bien ya.<br>
  <font color="#0000FF">[06:15] &lt;@RadioSprite&gt;</font> Hah, ok :)<br>
  <font color="#0000FF">[06:16] &lt;@RadioSprite&gt;</font> De todas formas me alegra poder ayudar.<br>
  <font color="#0000FF">[06:16] &lt;satsu&gt;</font> si, siendo posible leer los nombres y dem&aacute;s en la pelea es probablemente la parte m&aacute;s importante de este parche ^^;<br>
  <font color="#0000FF">[06:16] &lt;@RadioSprite&gt; </font>Bien, estas haciendo el siguiente dialogo, &iquest;cierto?<br>
  <font color="#0000FF">[06:17] &lt;satsu&gt;</font> si, si consigo un traductor.<br>
  <font color="#0000FF">[06:17] &lt;@RadioSprite&gt;</font> Deber&iacute;as solo copiar lo trozos de datos dentro de MO's board (<font color="#FF0000">*un 
foro* ?</font>)<br>
  <font color="#0000FF">[06:17] &lt;@RadioSprite&gt;</font>
<font color="#FF0000">y/o submit a whirlpool ad (**?)<br>
</font><font color="#0000FF">[06:17] &lt;satsu&gt;</font> bien, he ingresado un ad...
<font color="#FF0000">(**ad??)<br>
  </font>
  <font color="#0000FF">[06:17] &lt;@RadioSprite&gt;</font> hrm, oh si<br>
  <font color="#0000FF">[06:17] &lt;@RadioSprite&gt;</font> eso esta bien<br>
  <font color="#0000FF">[06:18] &lt;@RadioSprite&gt;</font> estabas en el primer ad o luego de mi Heian Fuuunden ad.
<font color="#FF0000">(** ?)</font><br>
  <font color="#0000FF">[06:18] &lt;satsu&gt; </font>ph33r.<br>
  <font color="#0000FF">[06:18] &lt;satsu&gt;</font> hm. &iquest;estaba en lennus 2 o algo parecido que tenia todos los datos de monstruos entre los nombres de monstruos?<br>
  <font color="#0000FF">[06:18] &lt;satsu&gt;</font> de hecho, &iquest;fue uno de los romasagas?<br>
  <font color="#0000FF">[06:19] &lt;@RadioSprite&gt;</font> Bien, seguro fue Blue Sphere :p<br>
  <font color="#0000FF">[06:19] &lt;satsu&gt;</font> heh. Creo que filerna hace eso igual. :P<br>
  <font color="#0000FF">[06:19] &lt;satsu&gt;</font> not&eacute; muchas m&aacute;s de esas mierdas que las normales entre los nombres. :P<br>
  <font color="#0000FF">[06:19] &lt;@RadioSprite&gt;</font> bien, de hecho, tiene sentido<br>
  <font color="#0000FF">[06:19] &lt;@RadioSprite&gt;</font> carga todo de una pasada<br>
  <font color="#0000FF">[06:19] &lt;satsu&gt;</font> si, es verdad. Aunque me pregunto por qu� es 
tan poco com&uacute;n :P<br>
  <font color="#0000FF">[06:20] &lt;@RadioSprite&gt;</font> en vez de sacar piezas de 15 diferentes lugares de la rom<br>
  <font color="#0000FF">[06:20] &lt;@RadioSprite&gt;</font> dicho sea de paso, Neil_ est&aacute; feliz de tener un nuevo art&iacute;culo.<br>
  <font color="#0000FF">[06:20] &lt;satsu&gt;</font> si, me lo imagino.<br>
  <font color="#0000FF">[06:21] &lt;satsu&gt;</font> te enviar&eacute; otro SPC de filerna y luego ire a la cama.<br>
  <font color="#0000FF">[06:21] &lt;@RadioSprite&gt;</font> ok<br>
  <font color="#0000FF">[06:21] &lt;@RadioSprite&gt;</font> Creo que voy a la cama igual<br>
  <font color="#0000FF">[06:21] &lt;@RadioSprite&gt;</font> esto fue agotador :p<br>
  <font color="#0000FF">[06:21] &lt;satsu&gt;</font> de paso, �all&aacute; no es medianoche?<br>
  <font color="#0000FF">[06:21] &lt;@RadioSprite&gt;</font> pero luego, quiz&aacute;s deber&iacute;a trabajar en RS2 (** Romancing Saga 2) y terminar de corregir los &uacute;ltimos bits de la rutina del intro<br>
  <font color="#0000FF">[06:21] * @RadioSprite</font> hora am Tuesday, August 6, 2002 1:29:28 AM<br>
  <font color="#0000FF">[06:21] &lt;satsu&gt;</font> La hora local actual en Gran Breta&ntilde;a es 6:21:55 AM<br>
  <font color="#0000FF">[06:21] &lt;satsu&gt;</font> �te sorprend�!<br>
  <font color="#0000FF">[06:22] &lt;@RadioSprite&gt;</font> La &uacute;ltima semana me fui a la cama a las 7 AM :p<br>
  <font color="#0000FF">[06:22] &lt;satsu&gt;</font> ayer me fui a la cama a las 7am. :P<br>
  <font color="#0000FF">[06:22] &lt;@RadioSprite&gt; </font>heheh<br>
  <font color="#0000FF">[06:22] &lt;@RadioSprite&gt;</font> de cualquier forma, ajusta el log para que se vea como html, y env&iacute;amelo o algo<br>
  <font color="#0000FF">[06:22] &lt;@RadioSprite&gt;</font> ma&ntilde;ana, no ahora<br>
  <font color="#0000FF">[06:23] &lt;satsu&gt;</font> ok. &iquest;Hay algo que pueda hacer mientras ??? 
<font color="#FF0000">(** something i can run it through to make it html kosher?)<br>
  </font>
  <font color="#0000FF">[06:23] * satsu</font> flojea<br>
  <font color="#0000FF">[06:23] &lt;@RadioSprite&gt;</font> No tengo idea, nunca he posteado un log IRC log en 
alg�n lugar.<br>
  <font color="#0000FF">[06:23] &lt;@RadioSprite&gt;</font> pero los &lt;RadioSprite&gt;s y &lt;satsu&gt;s no se ver&aacute;n apropiadamente.<br>
  <font color="#0000FF">[06:23] &lt;satsu&gt;</font> si, estar&aacute;n todos coloreados. :P<br>
  <font color="#0000FF">[06:24] &lt;@RadioSprite&gt;</font> hehe<br>
  <font color="#0000FF">[06:24] &lt;satsu&gt;</font> en mi configuraci&oacute;n de mi Mirc, los nick se ven de color naranjo brillante. :P<br>
  <font color="#0000FF">[06:24] &lt;@RadioSprite&gt;</font> bien, no se ven coloreados, o algo 
as�<br>
  <font color="#0000FF">[06:25] &lt;satsu&gt;</font> yay, encontr&eacute; algo.<br>
  <font color="#0000FF">[06:25] &lt;satsu&gt;</font> muy bien, en cualquier caso, ahora me voy a la cama.<br>
  <font color="#0000FF">[06:25] &lt;satsu&gt;</font> dice buenas noches satsu.<br>
  <font color="#0000FF">[06:25] &lt;@RadioSprite&gt;</font> ok, nos vemos.<br>
  <font color="#0000FF">[06:25] &lt;satsu&gt;</font> y gracias de nuevo.<br>
  <font color="#0000FF">[06:25] ***</font> Disconnected<br>
  Session Close: Tue Aug 06 06:25:50 2002</p>

  <hr> 
 
<a href="../../doc_traduc.php">Volver</a>
<br>
<font color="#FFFFFF">
<?php 
include '../../disq.php';
?>
</font>
<center><?php include ('../../pie.php'); ?></center>
</small>
</body>
</html>
