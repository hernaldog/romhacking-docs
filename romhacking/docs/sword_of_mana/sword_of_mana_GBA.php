<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Gu&iacute;a Sword of Mana - GBA (por Dark-N)</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.Estilo1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
}
.Estilo2 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold}

.Estilo6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold}
		
.Estilo5 {font-weight: bold}
.Estilo7 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; }
.Estilo8 {color: #0000FF}
.Estilo9 {color: #0000CC}
-->
</style>
</head>

<body>
<div align="left"><p class="Estilo1"><a href="../../guias_juegos.php">Volver</a></p></div>
<div align="center">
<p><img src="imagenes/titulo.JPG" width="583" height="394"></p>

<p class="Estilo1">v1.0<br> Empezada en Abril del 2006<br>Terminada en Diciembre del 2006<br>
<?php include '../../mail.php'; ?><br>
<a name="#contenido" href="http://darknromhacking.com/">
http://darknromhacking.com/</a></p>
	
	
<p align="left" class="Estilo2"><strong><a name="#contenido">Contenido</a><a href="#1"><br>
</a><br></strong></p>

  <table width="200" border="0" align="left">
    <tr class="Estilo1">
      <td colspan="2"><div align="left"><strong><a href="#1">1. Notas </a> <a href="#2"><br>
          2. Introducci&oacute;n </a><br>
        </strong><strong><a href="#3">3. Personajes</a><br>
        <a href="#4">4. Controles</a><br>
        <a href="#5">5. Habilidades</a><br>
        <a href="#6">6. Subir de Nivel</a><br>
        <a href="#7">7. Clases Especiales</a><br>
        <a href="#8">8. Niveles de Habilidad</a><br>
        <a href="#9">9. Tiempo</a><br>
        <a href="#10">10. Efecto del Status</a><br>
        <a href="#11">11. Trampas</a><br>
        <a href="#12">12. Combos</a><br>
        <a href="#13">13. Preguntas Frecuentes</a><br>
      <a href="#14">14. Gu&iacute;a de Pasos</a></strong></div></td>
    </tr>
    <tr class="Estilo1">
      <td><div align="left"></div></td>
      <td><div align="left"><a href="#14.1" class="Estilo5">H&eacute;roe</a></div></td>
    </tr>
    <tr class="Estilo1">
      <td colspan="2"><div align="left">
	  <strong>
	    <a href="#15">15. Mapa del Mundo</a><br>
        <a href="#16">16. Items</a><br>		
        <a href="#17">17. Magias</a><br>
        <a href="#18">18. Quests</a><br>
        <a href="#19">19. Secretos</a><br>
		</strong></div></td>
    </tr>
  </table>
  
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
 
  <hr>
  
  <p align="left" class="Estilo2"><strong><a name="#1">1. Notas </a></strong>
  <a href="#contenido"><img src="imagenes/subir.jpg" width="15" height="15" border="0"></a>  </p>
  <p align="left" class="Estilo1">-v1.0: Versi&oacute;n con Gu&iacute;a paso a paso de H&eacute;roe.</p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo2"><strong><a name="#2">2. Introducci�n </a><a href="#contenido"><img src="imagenes/subir.jpg" width="15" height="15" border="0"></a></strong></p>
  <p align="left" class="Estilo1">Sword of Mana me gust&oacute; desde el primer momento, sus gr&aacute;ficos al estilo SNES, estaba en completo Espa&ntilde;ol, era de la saga &quot;Mana&quot; o &quot;Seiken Densetsu&quot;, hab&iacute;a que jugarlo. <br>
    Lo primero que me di cuenta fue  la dificultad del juego y la facilidad de perderse.
    Esto me llev&oacute; a buscar material en espa&ntilde;ol y nada. Busqu&eacute; por internet aquella &eacute;poca alguna gu&iacute;a de pasos o lista de Magias y no encontr&eacute; algo  en espa&ntilde;ol.<br>
    B&aacute;sicamente esto fue lo que me impuls&oacute; a realizar esta gran gu&iacute;a. <br>
    <br>

  Recomiendo jugar la versi&oacute;n en espa&ntilde;ol del Juego llamada: &#8220;2659 - Sword of Mana&#8221; as&iacute; mismo busquen en el Google. No me pidan la ROM ya que no la tengo. </p>
  <p align="left" class="Estilo1">Muchos de los Tips y la forma de explicar este juego, las segu&iacute; de una gran gu&iacute;a en Ingl&eacute;s hecha por un tal ShdwRlm3 que encontr&eacute; en <a href="http://www.gamefaqs.xom">http://ww.gamefaqs.com</a>, 
	as� que deben verla ya que est� muy completa.</p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo2"><strong><a name="#3">3. Personajes </a><a href="#contenido"><img src="imagenes/subir.jpg" width="15" height="15" border="0"></a></strong></p>
  <p align="left" class="Estilo1">Aqu&iacute; algunos importantes personajes &quot;buenos&quot;: </p>
  <table width="75%" border="0" align="left">
    <tr class="Estilo1">
      <td width="70"><div align="left"><img src="imagenes/heroe.jpg" width="53" height="53"></div></td>
      <td width="590"> <div align="left"><strong>H&eacute;roe:</strong> La Historia es vengar a sus padres. Escapas con Hero&iacute;na cuando ni&ntilde;o mientras El Se&ntilde;or Oscuro mataba a tus padres. No tiene nombre espec&iacute;fico, simplemente le llamaremos H&eacute;roe. </div></td>
    </tr>
    <tr class="Estilo1">
      <td><div align="left"><img src="imagenes/heroina.jpg" width="53" height="53"></div></td>
      <td> <div align="left"><strong>Hero&iacute;na:</strong><span class="Estilo3">  La historia es descubrir su misterioso pasado. Tampoco tiene nombre espec&iacute;fico, le llamaremos Hero&iacute;na. </span></div></td>
    </tr>
    <tr class="Estilo1">
      <td class="Estilo3"><div align="left"><img src="imagenes/consul.jpg" width="53" height="53"></div></td>
      <td class="Estilo3"><div align="left"><strong>Hermann:</strong> C&oacute;nsul del Reino de Granz. Padre del H&eacute;roe. Cuidaba tambi&eacute;n a Hero&iacute;na. Ya que el Se&ntilde;or Oscuro encontraba que el cuidaba &#8220;herejes&#8221;, lo mata por eso. </div></td>
    </tr>
    <tr class="Estilo1">
      <td class="Estilo3"><div align="left"><img src="imagenes/mama.jpg" width="53" height="53"></div></td>
      <td class="Estilo3"> <div align="left">Mam&aacute; del H&eacute;roe. </div></td>
    </tr>
    <tr class="Estilo1">
      <td class="Estilo3"><div align="left"><img src="imagenes/lord.jpg" width="53" height="53"></div></td>
      <td class="Estilo3"><div align="left"><strong>Lord Granz: </strong>Un trovador que sabe muchas historias. H&eacute;roe toma clases de canto con &eacute;l. M&aacute;s adelante se disfraza para sacar al H&eacute;roe de la c&aacute;rcel.</div></td>
    </tr>
    <tr class="Estilo1">
      <td class="Estilo3"><div align="left"><img src="imagenes/willy.jpg" width="53" height="53"></div></td>
      <td class="Estilo3"> <div align="left"><strong>Willy:</strong> Un sobreviviente del Mana Clan y es otro  esclavo gladiador. </div></td>
    </tr>
    <tr class="Estilo1">
      <td class="Estilo3"><div align="left"><img src="imagenes/baron%20bogard.JPG" width="53" height="53"></div></td>
      <td class="Estilo3"><div align="left"><strong>Bar&oacute;n Bogard:</strong> Uno de los 3 Caballeros Gemma (Gemma Knight) que derrotaron en el pasado al Imperio de Vandole. El jura proteger a la Hero&iacute;na. </div></td>
    </tr>
    <tr class="Estilo1">
      <td class="Estilo3"><div align="left"><img src="imagenes/cibba.JPG" width="53" height="53"></div></td>
      <td class="Estilo3"><div align="left"><strong>Cibba: </strong>Caballero Legendario que les ense&ntilde;&eacute; a los Gemma a usar el poder Mana. &Eacute;l sabe mucho de la Espada Mana, necesaria para eliminar al Se&ntilde;or Oscuro. </div></td>
    </tr>
    <tr class="Estilo1">
      <td class="Estilo3"><div align="left"><img src="imagenes/niccolo.JPG" width="53" height="53"></div></td>
      <td class="Estilo3">
        <div align="left"><strong>Niccolo:</strong> Mercader ambulante. Salva la vida a h&eacute;roe la primera vez al principio del juego. </div></td>
    </tr>
    <tr class="Estilo1">
      <td colspan="2" class="Estilo3"><div align="left"><strong>Lester: </strong>Un trovador.</div></td>
    </tr>
    <tr class="Estilo1">
      <td colspan="2" class="Estilo3"><div align="left"><strong>Cibba:</strong> En gran sabio de Wendel, dirige a los Caballeros Gemma a la victoria.</div></td>
    </tr>
    <tr class="Estilo1">
      <td colspan="2" class="Estilo3"><div align="left"><strong>Marshall:</strong> Un Robot (Warbot) superviviente de la &eacute;poca de Vandole. </div></td>
    </tr>
    <tr class="Estilo1">
      <td colspan="2" class="Estilo3"><div align="left"><strong>Watts:</strong>  Un Enano. </div></td>
    </tr>
    <tr class="Estilo1">
      <td colspan="2" class="Estilo3"> <div align="left"><strong>Caballeros Gemma: </strong>Antiguamente derrotaron al imperio del Emperador Vandole que fue corrompido por el poder del Mana. Para el Se&ntilde;or Oscuro esto es una legenda que quiere dejar en el pasado.</div></td>
    </tr>
  </table>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo1">Algunos &quot;chicos malos&quot;:</p>
  <table width="75%" border="0" align="left">
    <tr class="Estilo1">
      <td width="70" class="Estilo3"><div align="left"><img src="imagenes/oscuro.jpg" width="53" height="53"></div></td>
      <td width="590" class="Estilo3"><div align="left"><strong>Se&ntilde;or Oscuro (Dark Lord):</strong> el malo del juego. Su nombre verdadero es Pr&iacute;ncipe Stroud. Incendi&oacute; el pueblo de Mana hace a&ntilde;os matando a muchos del Mana Clan ya que los acusa de Herejes.  No cree en la fuerza de la Diosa. </div></td>
    </tr>
    <tr class="Estilo1">
      <td class="Estilo3"><div align="left"><img src="imagenes/julius.JPG" width="53" height="53"></div></td>
      <td class="Estilo3"><div align="left"><strong>Julius :</strong> es un misterioso hombre que trabaja para Se&ntilde;or Oscuro. </div></td>
    </tr>
    <tr class="Estilo1">
      <td colspan="2" class="Estilo3"><div align="left"><strong>Count Lee:</strong> Un Mavole quien ha sido raptado por una mujer del Mana Clan. </div></td>
    </tr>
    <tr class="Estilo1">
      <td colspan="2" class="Estilo3"><div align="left"><strong>Ebony Butler: </strong>Sirviente de Count Lee que desea volver a Mavolia. </div></td>
    </tr>
    <tr class="Estilo1">
      <td colspan="2" class="Estilo3"><div align="left"><strong>Ivory Butler: </strong>Sirviente de Count Lee. Tambi&eacute;n desea volver a Mavolia. </div></td>
    </tr>
    <tr class="Estilo1">
      <td colspan="2" class="Estilo3"><div align="left"><strong>Isabella:</strong>  Otra Mavole con deseos desconocidos. </div></td>
    </tr>
    <tr class="Estilo1">
      <td colspan="2" class="Estilo3"><div align="left"><strong>Devius:</strong>  Conde de Devius Manor. </div></td>
    </tr>
    <tr class="Estilo1">
      <td colspan="2" class="Estilo3"><div align="left"><strong>Goremand:</strong> Un ser que consume almas. </div></td>
    </tr>
  </table>
  <p align="left" class="Estilo5">&nbsp;</p>
  <p align="left" class="Estilo5">&nbsp;</p>
  <p align="left" class="Estilo5">&nbsp;</p>
  <p align="left" class="Estilo5">&nbsp;</p>
  <p align="left" class="Estilo5">&nbsp;</p>
  <p align="left" class="Estilo5">&nbsp;</p>
  <p align="left" class="Estilo5">&nbsp;</p>
  <p align="left" class="Estilo2"><a name="#4">4. Controles </a><a href="#contenido"><img src="imagenes/subir.jpg" width="15" height="15" border="0"></a></p>
  <p align="left" class="Estilo1"><strong>En Batalla: </strong></p>
  <blockquote>
    <p align="left" class="Estilo1">Pad de Direcciones - Moverse <br>
    Bot&oacute;n A - Ataque <br>
    Bot&oacute;n B - Correr <br>
    Bot&oacute;n L - Habilidad<br>
    Bot&oacute;n R - Magia<br>
    Select &#8211; Intercambia personaje <br>
    Start &#8211; Men&uacute; Anillo.</p>
  </blockquote>
  <p align="left" class="Estilo1"><strong><br>
  En el Menu Anillo: </strong></p>
  <blockquote>
    <p align="left" class="Estilo1">Pad de Direcciones: </p>
    <blockquote>
      <p align="left" class="Estilo1">Izquierda &#8211; Mueve iconos a la derecha <br>
      Derecha &#8211; Mueve iconos a izquierda <br>
      Arriba &#8211; Te mueve a otro set de iconos <br>
      Abajo &#8211; Te mueve a otro set de iconos </p>
    </blockquote>
    <p align="left" class="Estilo1">Bot&oacute;n A - Selecciona <br>
    Bot&oacute;n B &#8211; Ve al nivel superior de iconos del Ring Menu <br>
    Bot&oacute;n L &#8211; Men&uacute; de armas (Batalla solamente) <br>
    Bot&oacute;n R &#8211; Men&uacute; de Esp&iacute;ritus (Batalla solamente) <br>
    Select - Nada <br>
    Start &#8211; Sale del Men&uacute; completamente </p>
  </blockquote>
  <p align="left"><br>
    <span class="Estilo1"><strong>Dentro de un Men&uacute;: </strong></span></p>
  <blockquote>
    <p align="left" class="Estilo1">Pad de Direcciones &#8211; Mueve el cursor <br>
    Bot&oacute;n A - Confirma <br>
    Bot&oacute;n B - Cancela <br>
    Bot&oacute;n L &#8211; Cambia a otro personaje <br>
    Bot&oacute;n R - Cambia a otro personaje <br>
    Select - Descripci&oacute;n <br>
    Start - Nada </p>
  </blockquote>
  <p align="left" class="Estilo5">&nbsp;</p>
  <p align="left" class="Estilo2"><strong><a name="#5">5. Habilidades </a><a href="#contenido"><img src="imagenes/subir.jpg" width="15" height="15" border="0"></a></strong></p>
  <p align="left" class="Estilo1">Solo tienes 2: </p>
  <p align="left" class="Estilo1"><strong>Saltar </strong></p>
  <blockquote>
    <p align="left" class="Estilo1">Comando: Bot&oacute;n L <br>
    Descripci&oacute;n: Permite saltar. <br>
    Se Adquiere : H&eacute;roe y Hero�na en Casita Cascada (Waterfall
Cabin) </p>
  </blockquote>
  <p align="left" class="Estilo1"><strong>Rezar</strong></p>
  <blockquote>
    <p align="left" class="Estilo1">Comando: Botones L + A <br>
    Descripci&oacute;n: Te sientas en cuchillas y as&iacute; sube tu MP lentamente. <br>
    Se adquiere: H&eacute;roe en Mansi&oacute;n Vinquette y Hero�na en Casita 
	Cascada</p>
  </blockquote>
  
  
  <p align="left" class="Estilo2">&nbsp;</p>
  <p align="left" class="Estilo2"><strong><a name="#6">6. Subir de Nivel </a><a href="#contenido"><img src="imagenes/subir.jpg" width="15" height="15" border="0"></a></strong></p>
  <p align="left" class="Estilo1">Cuando subes de nivel, tienes <strong>12</strong> puntos que pueden ser distribuidos a diferentes atributos. Si escoges Random, 8 puntos se van a MP/HP y los 4 restantes se dividen al azar. Esta es la tabla de distribuci&oacute;n de puntos seg&uacute;n el tipo de personaje que te puedes tener:</p>
  <table width="80%" cellpadding="0" cellspacing="0">
    <tr class="Estilo1">
      <td width="76" valign="top"><p>&nbsp; </p></td>
      <td width="84" valign="top" bgcolor="#B5C1CE"><p align="center"><strong>HP </strong></p></td>
      <td width="85" valign="top" bgcolor="#B5C1CE"><p align="center"><strong>MP </strong></p></td>
      <td width="90" valign="top" bgcolor="#B5C1CE"><p align="center"><strong>PODER/POW </strong></p></td>
      <td width="85" valign="top" bgcolor="#B5C1CE"><p align="center"><strong>DEFENSA/DEF </strong></p></td>
      <td width="84" valign="top" bgcolor="#B5C1CE"><p align="center"><strong>INTELIGENCIA/INT </strong></p></td>
      <td width="82" valign="top" bgcolor="#B5C1CE"><p align="center"><strong>SABIDURIA/MND </strong></p></td>
      <td width="76" valign="top" bgcolor="#B5C1CE"><p align="center"><strong>AGILIDAD/AGI </strong></p></td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td width="76" valign="top"><p><strong>Guerrero </strong></p></td>
      <td width="84" valign="top"><p align="center">+7 </p></td>
      <td width="85" valign="top"><p align="center">+1 </p></td>
      <td width="90" valign="top"><p align="center">+1 </p></td>
      <td width="85" valign="top"><p align="center">+1 </p></td>
      <td width="84" valign="top"><p align="center">&nbsp; </p></td>
      <td width="82" valign="top"><p align="center">&nbsp; </p></td>
      <td width="76" valign="top"><p align="center">+1 </p></td>
    </tr>
    <tr class="Estilo1">
      <td width="76" valign="top"><p><strong>Monje </strong></p></td>
      <td width="84" valign="top"><p align="center">+8 </p></td>
      <td width="85" valign="top"><p align="center">&nbsp; </p></td>
      <td width="90" valign="top"><p align="center">+2 </p></td>
      <td width="85" valign="top"><p align="center">+1 </p></td>
      <td width="84" valign="top"><p align="center">&nbsp; </p></td>
      <td width="82" valign="top"><p align="center">&nbsp; </p></td>
      <td width="76" valign="top"><p align="center">+1 </p></td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td width="76" valign="top"><p><strong>Mago </strong></p></td>
      <td width="84" valign="top"><p align="center">+3 </p></td>
      <td width="85" valign="top"><p align="center">+5 </p></td>
      <td width="90" valign="top"><p align="center">&nbsp; </p></td>
      <td width="85" valign="top"><p align="center">&nbsp; </p></td>
      <td width="84" valign="top"><p align="center">+2 </p></td>
      <td width="82" valign="top"><p align="center">+1 </p></td>
      <td width="76" valign="top"><p align="center">+1 </p></td>
    </tr>
    <tr class="Estilo1">
      <td width="76" valign="top"><p><strong>Sabio </strong></p></td>
      <td width="84" valign="top"><p align="center">+2 </p></td>
      <td width="85" valign="top"><p align="center">+6 </p></td>
      <td width="90" valign="top"><p align="center">&nbsp; </p></td>
      <td width="85" valign="top"><p align="center">&nbsp; </p></td>
      <td width="84" valign="top"><p align="center">+1 </p></td>
      <td width="82" valign="top"><p align="center">+2 </p></td>
      <td width="76" valign="top"><p align="center">+1 </p></td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td width="76" valign="top"><p><strong>Ladr&oacute;n </strong></p></td>
      <td width="84" valign="top"><p align="center">+5 </p></td>
      <td width="85" valign="top"><p align="center">+3 </p></td>
      <td width="90" valign="top"><p align="center">+1 </p></td>
      <td width="85" valign="top" bgcolor="#DCEBEF"><p align="center">+1 </p></td>
      <td width="84" valign="top"><p align="center">&nbsp; </p></td>
      <td width="82" valign="top"><p align="center">&nbsp; </p></td>
      <td width="76" valign="top"><p align="center">+2 </p></td>
    </tr>
    <tr class="Estilo1">
      <td width="76" valign="top"><p><strong>Random </strong></p></td>
      <td width="84" valign="top"><p align="center">+? </p></td>
      <td width="85" valign="top"><p align="center">+? </p></td>
      <td width="90" valign="top"><p align="center">+? </p></td>
      <td width="85" valign="top"><p align="center">+? </p></td>
      <td width="84" valign="top"><p align="center">+? </p></td>
      <td width="82" valign="top"><p align="center">+? </p></td>
      <td width="76" valign="top"><p align="center">+? </p></td>
    </tr>
  </table>
  <p align="center" class="Estilo1"><img src="imagenes/up_level.JPG" width="275" height="220"><br>
  </p>
  <p align="left" class="Estilo2"><strong><a name="#7">7. Clases Especiales </a><a href="#contenido"><img src="imagenes/subir.jpg" width="15" height="15" border="0"></a></strong></p>
  <p align="left" class="Estilo1">Dependiendo del nivel que est&eacute;s, puedes ganar ciertas Clases  y con eso algunas buenas mejoras. Mejoras que pueden ser aumentar un ataque o aumentar defensa luego de usar magia. Dependiendo del tipo de personaje que escojas (Guerrero, Mago, etc.) puedes tener clases dentro de ese tipo. Hay 3 tipos de Clases: Amarillo (clase base), Azul (secundaria) y Roja (final). Las clases Amarillas son del nivel 5 en adelante, Las azules desde el nivel 15 y las rojas del 35 hacia arriba. No puedes cambiarte de clase si ya tienes una, por ejemplo, si est&aacute;s en clase Amarilla del tipo Guerrero, no puedes cambiarte luego a azul de Mago. <br>
  </p>
  <table width="64%"  border="0">
    <tr>
      <td width="52%" class="Estilo1"><img src="imagenes/screen5_sin_clase.JPG" width="275" height="220"></td>
      <td width="48%" class="Estilo1">Este es el Status de tu personaje, <strong>sin Clase</strong> claro. </td>
    </tr>
  </table>

  <p>&nbsp;</p>
  <table width="70%" border="0">
    <tr bgcolor="#99CCFF" class="Estilo1">
      <td colspan="3"><div align="center">
        <strong>Clases de Guerrero (Warrior) </strong>
      </div></td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td width="85"><div align="left">Guerrero -&gt; </div></td>
      <td width="101"> <div align="left">Caballero -&gt; </div></td>
      <td width="104"> <div align="left">Mariscal</div></td>
    </tr>
    <tr class="Estilo1">
      <td><div align="left"></div></td>
      <td><div align="left"></div></td>
      <td bgcolor="#DCEBEF"> <div align="left">Palad&iacute;n</div></td>
    </tr>
    <tr class="Estilo1">
      <td> <div align="left"></div></td>
      <td bgcolor="#DCEBEF"> <div align="left">Gladiador -&gt; </div></td>
      <td bgcolor="#DCEBEF"> <div align="left">Duelista </div></td>
    </tr>
    <tr class="Estilo1">
      <td><div align="left"></div></td>
      <td><div align="left"></div></td>
      <td bgcolor="#DCEBEF"> <div align="left">Maestro de Armas</div></td>
    </tr>
    <tr bgcolor="#EEF2EF" class="Estilo1">
      <td colspan="3"><div align="left"><strong>Guerrero</strong><br>
          Tipo: Amarillo<br>
          Requerimientos: Seleccionar Guerrero hasta llegar al Nivel 5 (es decir Guerrero Nivel 5) <br>
        Efectos: Ataque de Espada/Vara +5<br>
        <br>
        <img src="imagenes/screen5_clase_guerrero.JPG" width="275" height="220">        <img src="imagenes/screen5_clase_guerrero_status.JPG" width="275" height="220"><br>
        <strong>Observa el cambio, ahora se ve con Amarillo la Clase Guerrero. <br>
        <br>
        Caballero</strong><br>
  Tipo: Azul<br>
  Requerimientos: Guerrero Nivel 10 + Sabio Nivel 5<br>
  Efectos: Ataque de Espada/Vara +10, 
  Luz Curativa +10<br>
  <strong><br>
Mariscal</strong><br>

Tipo: Rojo<br>
Requerimientos: Guerrero Nivel 20 + Sabio Nivel 15<br>
Efectos: Ataque de Espada/Vara  +20, 
Luz Curativa +40  <br>
<br>
<strong>Palad&iacute;n</strong><br>

Tipo: Rojo<br>
Requerimientos: Guerrero Nivel 15 + Mago Nivel 10 + Sabio Nivel 10<br>
Efectos: Ataque de Espada/Vara  +20, 
Luz Curativa +20, 
Ataque Magia Blanca +10<br>
<br>
<strong>Gladiador</strong><br>

Tipo: Azul<br>
Requerimientos: Guerrero Nivel 10 + Random Nivel 5<br>
Efectos: Ataque de Espada/Vara  +15<br>
<br>
<strong>Duelista</strong><br>

Tipo: Rojo<br>
Requerimientos: Guerrero Nivel 25 + Random Nivel 10<br>
Efectos: Ataque de Espada/Vara  +30, 
Ataque Magia Oscura +10<br>
<strong><br> 
Maestro de Armas
</strong><br>

Tipo: Rojo<br>
Requerimientos: Guerrero Nivel 20 + Mago Nivel 5 + Random Nivel 10<br>
Efectos: Ataque de Espada/Vara  +25, 
Poder M&aacute;gico de Ataque: Fuego, Agua, Aire, Tierra +15<br>
      </div></td>
    </tr>
  </table>
  <br>
  <table width="70%" border="0">
    <tr bgcolor="#99CCFF" class="Estilo1">
      <td colspan="3"><div align="center">
          <strong>Clases de Monje (Monk)</strong>
      </div></td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td width="290"><div align="left">Luchador -&gt; </div></td>
      <td width="290"> <div align="left">Monje -&gt; </div></td>
      <td width="290">
        <div align="left">Monje Guerrero </div></td>
    </tr>
    <tr class="Estilo1">
      <td><div align="left"></div></td>
      <td><div align="left"></div></td>
      <td bgcolor="#DCEBEF"> <div align="left">Pu&ntilde;o Divino </div></td>
    </tr>
    <tr class="Estilo1">
      <td> <div align="left"></div></td>
      <td bgcolor="#DCEBEF"> <div align="left">Bashkar -&gt; </div></td>
      <td bgcolor="#DCEBEF"> <div align="left">Pu&ntilde;o Mortal </div></td>
    </tr>
    <tr class="Estilo1">
      <td><div align="left"></div></td>
      <td><div align="left"></div></td>
      <td bgcolor="#DCEBEF"> <div align="left">Derviche</div></td>
    </tr>
    <tr bgcolor="#EEF2EF" class="Estilo1">
      <td colspan="3"><div align="left"><strong>Luchador</strong><br>
      Tipo: Amarillo<br>
      Requerimientos: Monje Nivel 5<br>
      Efectos: Ataque de Manopla  +5 </div>
        <p align="left"> <strong>Monje</strong><br>
      Tipo: Azul<br>
      Requerimientos: Monje Nivel 10 + Sabio Nivel 5<br>
      Efectos: Ataque de Manopla +10, 
      Luz Curativa +10</p>
          <p align="left"> <strong>Monje Guerrero</strong><br>
      Tipo: Rojo<br>
      Requerimientos: Monje Nivel 20 + Sabio Nivel 15<br>
      Efectos: Ataque de Manopla +20, 
      Luz Curativa +40</p>
          <p align="left"> <strong>Pu&ntilde;o Divino</strong><br>
      Tipo: Rojo<br>
      Requerimientos: Monje Nivel 15 + Mago Nivel 10 + Sabio Nivel 10<br>
      Efectos: Ataque de Manopla +20, 
      Luz Curativa +20<br>
      Ataque Magia Blanca +10</p>
          <p align="left"> <strong>Bashkar</strong><br>
      Tipo: Azul<br>
      Requerimientos: Monje Nivel 10 + Random Nivel 5<br>
      Efectos: Ataque de Manopla +15</p>
          <p align="left"> <strong>Pu&ntilde;o Mortal </strong><br>
      Tipo: Rojo<br>
      Requerimientos: Monje Nivel 25 + Random Nivel 10<br>
      Efectos: Ataque de Manopla +40<br>
      <br>
      <strong>Derviche</strong><br>
      Tipo: Rojo<br>
      Requerimientos: Monje Nivel 20 + Mago Nivel 5 + Random Nivel 10<br>
      Efectos: Ataque de Manopla +30, 
      Ataque Magia Luna +10</p></td>
    </tr>
  </table>
  <br>
  <table width="70%" border="0">
    <tr bgcolor="#99CCFF" class="Estilo1">
      <td colspan="3"><div align="center">
          <strong>Clases de  Mago (Magician) </strong>
      </div></td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td width="85"><div align="left">Mago  -&gt; </div></td>
      <td width="101"> 

        <div align="left">Hechicero
  

  
 -&gt; </div></td>
      <td width="104"> <div align="left"> Archimago </div></td>
    </tr>
    <tr class="Estilo1">
      <td><div align="left"></div></td>
      <td><div align="left"></div></td>
      <td bgcolor="#DCEBEF"> <div align="left">Gran Agorero</div></td>
    </tr>
    <tr class="Estilo1">
      <td> <div align="left"></div></td>
      <td bgcolor="#DCEBEF"> 

        <div align="left">Or&aacute;culo
  

  
 -&gt; </div></td>
      <td bgcolor="#DCEBEF"> <div align="left">Magus</div></td>
    </tr>
    <tr class="Estilo1">
      <td><div align="left"></div></td>
      <td><div align="left"></div></td>
      <td bgcolor="#DCEBEF"> <div align="left">Cabalista</div></td>
    </tr>
    <tr bgcolor="#EEF2EF" class="Estilo1">
      <td colspan="3"><div align="left"> <strong>Mago</strong><br>
        Tipo: Amarillo<br>
        Requerimientos: Mago Nivel 5<br>
        Efectos: Poder M&aacute;gico de Ataque: Fuego, Agua, Aire, Tierra +5
        <p> <strong>Hechicero</strong><br>
          Tipo: Azul<br>
          Requerimientos: Mago Nivel 10 + Sabio Nivel 5<br>
          Efectos: Poder M&aacute;gico de Ataque: Fuego, Agua, Aire, Tierra +10, Ataque Magia Luz 
  +5</p>
        <p> <strong>Archimago</strong><br>
          Tipo: Rojo<br>
          Requerimientos: Mago Nivel 20 + Sabio Nivel 15<br>
          Efectos: Poder M&aacute;gico de Ataque: Fuego, Agua, Aire, Tierra +10, 
  Ataque Magia Luz +10<br>
  Poder M&aacute;gico de Defensa: Fuego, Agua, Aire, Tierra +20<br>
  8 Hit de Esp&iacute;ritus de Viento</p>
        <p> <strong>Gran Agorero</strong><br>
          Tipo: Rojo<br>
          Requerimientos: Guerrero Nivel 1 + Monje Nivel 1 + Mago Nivel 20 + Sabio Nivel 10 +
  Ladr&oacute;n Nivel 3<br>
  Efectos: Poder M&aacute;gico de Ataque: Fuego, Agua, Aire, Tierra +15, 
  Ataque Magia Luz +10<br>
  Poder M&aacute;gico de Defensa: Fuego, Agua, Aire, Tierra +15<br>
  8 Hit Esp&iacute;ritus de Agua</p>
        <p> <strong>Or&aacute;culo</strong><br>
          Tipo: Azul<br>
          Requerimientos: Mago Nivel 10 + Random Nivel 5<br>
          Efectos: Poder M&aacute;gico de Ataque: Fuego, Agua, Aire, Tierra +10, 
  Ataque Magia Negra +5</p>
        <p> <strong>Magus</strong><br>
          Tipo: Rojo<br>
          Requerimientos: Mago Nivel 25 + Random Nivel 10<br>
          Efectos: Poder M&aacute;gico de Ataque: Fuego, Agua, Aire, Tierra +30, 
  Ataque Magia Negra +10<br>
  8 Hit de Esp&iacute;ritus de Fuego</p>
        <p> <strong>Cabalista</strong><br>
          Tipo: Rojo<br>
          Requerimientos: Guerrero Nivel 1 + Monje Nivel 1 + Mago Nivel 20 + Ladr&oacute;n Nivel 3 + 
  Random Nivel 10<br>
  Efectos: Poder M&aacute;gico de Ataque: Fuego, Agua, Aire, Tierra +20, Ataque Magia Negra +10<br>
  Poder M&aacute;gico de Defensa: Fuego, Agua, Aire, Tierra +10<br>
  8 Hit de Esp&iacute;ritus de Tierra<br>
        </p>
      </div>
      </td>
    </tr>
  </table>
  <br>
  <table width="70%" border="0">
    <tr bgcolor="#99CCFF" class="Estilo1">
      <td colspan="3"><div align="center">
         <strong>Clases de  Sabio (Sage)</strong>
      </div></td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td width="85"><div align="left">Cl&eacute;rigo  -&gt; </div></td>
      <td width="101"> <div align="left">Sacerdote
  
 -&gt; </div></td>
      <td width="104"> <div align="left">Obispo</div></td>
    </tr>
    <tr class="Estilo1">
      <td><div align="left"></div></td>
      <td><div align="left"></div></td>
      <td bgcolor="#DCEBEF"> <div align="left">Sabio</div></td>
    </tr>
    <tr class="Estilo1">
      <td> <div align="left"></div></td>
      <td bgcolor="#DCEBEF"> <div align="left">Invocador
  
 -&gt; </div></td>
      <td bgcolor="#DCEBEF"> <div align="left">Cham&aacute;n</div></td>
    </tr>
    <tr class="Estilo1">
      <td><div align="left"></div></td>
      <td><div align="left"></div></td>
      <td bgcolor="#DCEBEF"> <div align="left">Nigromante</div></td>
    </tr>
    <tr bgcolor="#EEF2EF" class="Estilo1">
      <td colspan="3"> <div align="left"><strong>Cl&eacute;rigo</strong><br>        
          Tipo: Amarillo<br>
          Requerimientos: Sabio Nivel 5<br>
          Efectos: Luz Curativa +10
        </div>
        <p align="left"> <strong>Sacerdote</strong><br>
          Tipo: Azul<br>
          Requerimientos: Mago Nivel 5 + Sabio Nivel 10<br>
          Efectos: Defensa Magia Luna, Agua, Bosque, Viento, Tierra +5, 
  Luz Curativa +20</p>
        <p align="left"> <strong>Obispo</strong><br>
          Tipo: Rojo<br>
          Requerimientos: Mago Nivel 10 + Sabio Nivel 25<br>
          Efectos: Defensa Magia Luz, Luna, Fuego, Agua, Bosque, Viento, Tierra  +10, 
  Luz Curativa +20<br>
  Ataque de Magia Luz +20<br>
  8 Hit de Esp&iacute;ritus de Luz</p>
        <p align="left"> <strong>Sabio</strong><br>
          Tipo: Rojo<br>
          Requerimientos: Mago Nivel 10 + Sabio Nivel 15 + Ladr&oacute;n Nivel 10<br>
          Efectos: Defensa Magia Luz, Luna, Fuego, Agua, Bosque, Viento, Tierra +10, 
  Luz Curativa +40<br>
  Ataque Magia Bosque +20<br>
  8 Hit de Esp&iacute;ritu Bosque </p>
        <p align="left"> <strong>Invocador</strong><br>
          Tipo: Azul<br>
          Requerimientos: Sabio Nivel 10 + Random Nivel 5<br>
          Efectos: Defensa Magia  Luna, Fuego, Agua, Bosque, Viento, Tierra  +10, 
  Luz Curativa +10</p>
        <p align="left"> <strong>Cham&aacute;n</strong><br>
          Tipo: Rojo<br>
          Requerimientos: Mago Nivel 10 + Sabio Nivel 15 + Random Nivel 10<br>
          Efectos: Defensa Magia Oscura, Luna, Fuego, Agua, Bosque, Viento, Tierra +10, 
  Luz Curativa +10<br>
  Ataque Magia Oscura +25<br>
  8 Hit de Esp&iacute;ritu Oscuro</p>
        <p align="left"> <strong>Nigromante</strong><br>
          Tipo: Rojo<br>
          Requerimientos: Mago Nivel 5 + Sabio Nivel 15 + Ladr&oacute;n Nivel 5 + Random Nivel 10<br>
          Efectos: Defensa Magia Oscura, Luna, Fuego, Agua, Bosque, Viento, Tierra +20, 
  Luz Curativa +10<br>
  Ataque Magia Luna +15<br>
  8 Hit en Esp&iacute;ritu Lunar</p></td>
    </tr>
  </table>
  <br>
  <table width="70%" border="0">
    <tr bgcolor="#99CCFF" class="Estilo1">
      <td colspan="3"><div align="center">
          <strong>Clases de un Ladr&oacute;n (Thief)</strong>
      </div></td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td width="85"><div align="left">Ladr&oacute;n  -&gt; </div></td>
      <td width="101"> <div align="left">Explorador
  
 -&gt; </div></td>
      <td width="104"> <div align="left">N&oacute;mada</div></td>
    </tr>
    <tr class="Estilo1">
      <td><div align="left"></div></td>
      <td><div align="left"></div></td>
      <td bgcolor="#DCEBEF"> <div align="left">Forajido</div></td>
    </tr>
    <tr class="Estilo1">
      <td> <div align="left"></div></td>
      <td bgcolor="#DCEBEF"> <div align="left">Ninja
  
 -&gt; </div></td>
      <td bgcolor="#DCEBEF"> <div align="left">Sombra Siniestra</div></td>
    </tr>
    <tr class="Estilo1">
      <td><div align="left"></div></td>
      <td><div align="left"></div></td>
      <td bgcolor="#DCEBEF"> <div align="left">Maestro Ninja </div></td>
    </tr>
    <tr bgcolor="#EEF2EF" class="Estilo1">
      <td colspan="3"><div align="left"><strong>Ladr&oacute;n</strong><br>        
          Tipo: Amarillo<br>
          Requerimientos: Ladr&oacute;n Nivel 5<br>
          Efectos: Precisi&oacute;n +5, 
  Golpe Cr&iacute;tico +5
      </div>
        <p align="left"> <strong>Explorador</strong><br>
  Tipo: Azul<br>
  Requerimientos: Sabio Nivel 5 + Ladr&oacute;n Nivel 10<br>
  Efectos: Precisi&oacute;n +10, 
  Golpe Cr&iacute;tico +10, 
  Ataque Arco +5</p>
<p align="left"> <strong>N&oacute;mada</strong><br>
  Tipo: Rojo<br>
  Requerimientos: Sabio Nivel 15 + Ladr&oacute;n Nivel 20<br>
  Efectos: Precisi&oacute;n +20, 
  Golpe Cr&iacute;tico +15, 
  Ataque Arco +10, 
  Ataque Mayal +10</p>
<p align="left"> <strong>Forajido</strong><br>
  Tipo: Rojo<br>
  Requerimientos: Mago +10 + Sabio Nivel 10 + Ladr&oacute;n Nivel 15<br>
  Efectos: Precisi&oacute;n +10, 
  Golpe Cr&iacute;tico +15,
  Ataque Arco +20, 
  Ataque de Magia Luna, Bosque +10</p>
<p align="left"> <strong>Ninja</strong><br>
  Tipo: Azul<br>
  Requerimientos: Ladr&oacute;n Nivel 10 + Random Nivel 5<br>
  Efectos: Precisi&oacute;n +5, 
  Golpe Cr&iacute;tico +10, 
  Evasi&oacute;n +10</p>
<p align="left"> <strong>Sombra Siniestra </strong><br>
  Tipo: Rojo<br>
  Requerimientos: Ladr&oacute;n Nivel 25 + Random Nivel 10<br>
  Efectos: Precisi&oacute;n +10, 
  Golpe Cr&iacute;tico +20, 
  Evasi&oacute;n +10, 
  Ataque Falce +20</p>
<p align="left"> <strong>Maestro Ninja</strong><br>
  Tipo: Rojo<br>
  Requerimientos: Mago Nivel 5 + Ladr&oacute;n Nivel 20 + Random Nivel 10<br>
  Efectos: Precisi&oacute;n +10,
  Golpe Cr&iacute;tico +20,
  Evasi&oacute;n +20, <br>
  Poder M&aacute;gico de Ataque: Fuego, Agua, Aire, Tierra +10</p></td>
    </tr>
  </table>
  <br>
  <table width="70%" border="0">
    <tr bgcolor="#99CCFF" class="Estilo1">
      <td colspan="3"><div align="center">
          <strong>Clase Random</strong>
      </div></td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td width="85"><p align="left">B&aacute;rbaro -&gt; </p></td> <td width="101"> <div align="left">Aesir -&gt; </div></td>
      <td width="104"> <div align="left">Od&iacute;n</div></td>
    </tr>
    <tr class="Estilo1">
      <td><div align="left"></div></td>
      <td><div align="left"></div></td>
      <td bgcolor="#DCEBEF"> <div align="left">Lancero Celeste </div></td>
    </tr>
    <tr class="Estilo1">
      <td> <div align="left"></div></td>
      <td bgcolor="#DCEBEF"> <div align="left">Caballero Cabal
  
 -&gt; </div></td>
      <td bgcolor="#DCEBEF"> <div align="left">Caballero Fenrir</div></td>
    </tr>
    <tr class="Estilo1">
      <td><div align="left"></div></td>
      <td><div align="left"></div></td>
      <td bgcolor="#DCEBEF"> <div align="left">Drag&oacute;n</div></td>
    </tr>
    <tr bgcolor="#EEF2EF" class="Estilo1">
      <td colspan="3"> <div align="left"><strong>B&aacute;rbaro</strong><br>        
          Tipo: Amarillo<br>
          Requerimientos: Guerrero Nivel 2 + Monje Nivel 2 + Ladr&oacute;n Nivel 1<br>
          Efectos: Ataque con Lanza +5<br>
          <br>
          <img src="imagenes/screen5_clase_barbaro.JPG" width="275" height="220"><img src="imagenes/screen5_clase_barbaro_status.JPG" width="275" height="220"></div>
        <p align="left"> <strong>Aesir</strong><br>
  Tipo: Azul<br>
  Requerimientos: Guerrero Nivel 4 + Monje Nivel 4 + Mago Nivel 2 + Sabio Nivel 2 + Ladr&oacute;n Nivel 3<br>
  Efectos: Ataque con Lanza +10<br>
  Poder M&aacute;gico de Defensa: Fuego, Agua, Aire, Tierra +5</p>
<p align="left"> <strong>Od&iacute;n</strong><br>
  Tipo: Rojo<br>
  Requerimientos: Guerrero Nivel 8 + Monje Nivel 8 + Mago Nivel 7 + Sabio Nivel 5 + Ladr&oacute;n Nivel 7<br>
  Efectos: Ataque con Lanza +20<br>
  Poder M&aacute;gico de Defensa: Fuego, Agua, Aire, Tierra +10<br>
  Ataque Magia Luz +10</p>
<p align="left"> <strong>Lancero Celeste </strong><br>
  Tipo: Rojo<br>
  Requerimientos: Guerrero Nivel 10 + Monje Nivel 8 + Mago Nivel 5 + Sabio Nivel 7 +
  Ladr&oacute;n Nivel 5<br>
  Efectos: Ataque con Lanza +25<br>
  Poder M&aacute;gico de Defensa: Fuego, Agua, Aire, Tierra +15<br>
  <br> 
  <strong>Caballero Cabal</strong><br>
  Tipo: Azul<br>
  Requerimientos: Guerrero Nivel 4 + Monje Nivel 4 + Ladr&oacute;n Nivel 2 + Random Nivel 5<br>
  Efectos: Ataque con Lanza +15<br>
  <img src="imagenes/caballero%20cabal.JPG" width="275" height="220"><img src="imagenes/caballero%20cabal_statusJPG.JPG" width="275" height="220"></p>
<p align="left"><strong>Caballero Fenrir</strong><br>
  Tipo: Rojo<br>
  Requerimientos: Guerrero Nivel 10 + Monje Nivel 10 + Ladr&oacute;n Nivel 5 + Random Nivel 10<br>
  Efectos: Ataque con Lanza +30<br>
  Ataque Magia Oscura +10</p>
<p align="left"> <strong>Drag&oacute;n</strong><br>
  Tipo: Rojo<br>
  Requerimientos: Guerrero Nivel 8 + Monje Nivel 8 + Mago Nivel 5 + Ladr&oacute;n Nivel 4 +
  Random Nivel 10<br>
  Efectos: Ataque con Lanza +25<br>
  Poder M&aacute;gico de Ataque: Fuego, Agua, Aire, Tierra  +15<br><img src="imagenes/dragon.JPG" width="275" height="220">
  <img src="imagenes/dragon_status.JPG" width="275" height="220"> </p></td>
    </tr>
  </table>
  <p align="left" class="Estilo1">Tambi&eacute;n en vez de escoger por una clase, puedes escoger por <strong>Accesorios </strong> bonus que aumentan tus habilidades much&iacute;simo. Para acceder a esto debes alcanzar el nivel <strong>41</strong> pero en un &uacute;nico <strong>Estilo,</strong> por ejemplo seguir Mago hasta el nivel 40, sin tomar ninguna clase en el camino. Al llegar al nivel 41 recibir&aacute;s un mensaje que dice que recibiste un Objeto. </p>
  <table width="80%" border="0">
    <tr bgcolor="#99CCFF" class="Estilo1">
      <td class="Estilo1"><div align="center"><strong>Tipo</strong></div></td>
      <td><div align="center"><strong>Accesorio (en ingl&eacute;s) </strong></div></td>
      <td><div align="center"><strong>Pow</strong></div></td>
      <td><div align="center"><strong>Def</strong></div></td>
      <td><div align="center"><strong>Int</strong></div></td>
      <td><div align="center"><strong>Mnd</strong></div></td>
      <td><div align="center"><strong>Agi</strong></div></td>
    </tr>
    <tr class="Estilo1">
      <td> Guerrero Nivel 40 </td>
      <td> General Crest </td>
      <td><div align="center"></div></td>
      <td><div align="center">+50</div></td>
      <td><div align="center"></div></td>
      <td><div align="center"></div></td>
      <td><div align="center"></div></td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td bgcolor="#DCEBEF"> Monje Nivel 40 </td>
      <td bgcolor="#DCEBEF"> Dragon Ring </td>
      <td><div align="center">+50</div></td>
      <td><div align="center"></div></td>
      <td><div align="center"></div></td>
      <td><div align="center"></div></td>
      <td><div align="center"></div></td>
    </tr>
    <tr class="Estilo1">
      <td> Mago Nivel 40 </td>
      <td> Rune Earrings </td>
      <td><div align="center"></div></td>
      <td><div align="center"></div></td>
      <td><div align="center">+50</div></td>
      <td><div align="center"></div></td>
      <td><div align="center"></div></td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td bgcolor="#DCEBEF"> Sabio Nivel 40 </td>
      <td bgcolor="#DCEBEF"> Code Bead </td>
      <td><div align="center"></div></td>
      <td><div align="center"></div></td>
      <td><div align="center"></div></td>
      <td><div align="center">+50</div></td>
      <td><div align="center"></div></td>
    </tr>
    <tr class="Estilo1">
      <td> Ladr&oacute;n Nivel 40 </td>
      <td> Wishbone </td>
      <td><div align="center"></div></td>
      <td><div align="center"></div></td>
      <td><div align="center"></div></td>
      <td><div align="center"></div></td>
      <td><div align="center">+50</div></td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td bgcolor="#DCEBEF"> Random Nivel 40 </td>
      <td bgcolor="#DCEBEF"> Crystal Ring </td>
      <td><div align="center">+20</div></td>
      <td><div align="center">+20</div></td>
      <td><div align="center">+20</div></td>
      <td bgcolor="#DCEBEF"><div align="center">+20</div></td>
      <td bgcolor="#DCEBEF"><div align="center">+20</div></td>
    </tr>
  </table>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo2"><strong><a name="#8">8. Niveles de Habilidad </a><a href="#contenido"><img src="imagenes/subir.jpg" width="15" height="15" border="0"></a></strong></p>
  <p align="left" class="Estilo1">Todas las armas y esp&iacute;ritus en Sword of Mana tienen &quot;Niveles de Habilidad&quot; que sirven para muchas cosas. Lo primero que debes saber es que <strong>por cada nivel </strong>de tu Arma o Esp&iacute;ritu  M&aacute;gico que subas, se  <strong>incrementa en 1</strong> el poder de esa arma o magia. No es mucho, pero si alcanzas el nivel de ataque 99 en tu arma, ser&aacute;n  99 puntos extra de da&ntilde;o. Tambi&eacute;n para cada nivel que gane el esp&iacute;ritu L&uacute;minal, el poder de &quot;HealingLight&quot; (bot&oacute;n R brevemente presionado) curar&aacute; 1 punto de HP mas adicional.<br>
  Los niveles del arma determinan cuan r&aacute;pido tu Indicador de<strong> Golpe Mortal</strong> se incrementa. Son 30 las barras de dicho indicador. La siguiente tabla muestra cuantos puntos se incrementa por golpe, basado en el nivel que estas con esa arma que das el golpe:</p>
 
  <table width="23%" border="0">
    <tr bgcolor="#99CCFF" class="Estilo1">
      <td><div align="center">Nivel de Habilidad de tu arma </div></td>
      <td><div align="center">Puntos de barra que da al golpear </div></td>
    </tr>
    <tr class="Estilo1">
      <td>1-9</td>
      <td>1</td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td bgcolor="#DCEBEF">10-19</td>
      <td>2</td>
    </tr>
    <tr class="Estilo1">
      <td>20-29</td>
      <td>3</td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td>30-39</td>
      <td>4</td>
    </tr>
    <tr class="Estilo1">
      <td>40-49</td>
      <td>5</td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td>50-59</td>
      <td>6</td>
    </tr>
    <tr class="Estilo1">
      <td>60-69</td>
      <td>7</td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td>70-79</td>
      <td>8</td>
    </tr>
    <tr class="Estilo1">
      <td>80-89</td>
      <td>9</td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td>90-99</td>
      <td>10</td>
    </tr>
  </table>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo2">Habilidad y Experiencia</p>
  <p align="left" class="Estilo1">Para determinar cuanta Experiencia en Habilidad (Skill EXP) necesitas para subir un nivel de cierta Arma o Esp&iacute;ritu, usa la f&oacute;rmula:<br>
    <strong><br>
  ( Actual nivel de Habilidad  + 1 ) * 20 = Cuanta EXP falta para el siguiente nivel </strong></p>
  <p align="left" class="Estilo1">Por ejemplo,  si tu Nivel de Espada es 1, necesitas  (1+1)*20 = 40 Skill 
    EXP falta para subir al nivel 2. <br>
    Si tu nivel es  98, necesitas (98+1)*20 = 1980 
  Skill EXP para pasar al m&aacute;ximo nivel 99. <br>
  <br>
  Skill EXP se gana golpeando a enemigos con tu Arma o con Magias de Ataque de un Esp&iacute;ritu (una magia de defensa o apoyo no da EXP). Si haces 0 de da&ntilde;o IGUAL ganas SKILL EXP (un dato muy bueno), pero si haces un MISS (cuando el enemigo te esquiva el golpe o magia) no te da nada de EXP.<br>
  Cuanta SKILL EXP te da golpear un enemigo depende en general de cuanta experiencia normal te da el enemigo. Usa la siguiente f&oacute;rmula para determinar cuanta SKILL EXP te da un enemigo:<br>
  <br>
  <strong>Experiencia en Habilidad de Arma</strong> (Weapon Skill EXP)<strong>: EXP de Enemigo / 5<br>
  Experiencia en Habilidad de Magia </strong>(Magic Skill EXP)<strong>: EXP de Enemigo / 3 <br>
  <br>
  </strong>Cada enemigo te da un m&iacute;nimo de 1 SKILL EXP.<br>
  <br>
TIP: Para ganar mas experiencia f&aacute;cil debes golpear <strong> Metaballs </strong>y <strong> Stone Seals </strong>ya que te dan <strong>1</strong> Skill EXP por <strong>cada golpe que les des.</strong></p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo2"><strong><a name="#9">9. Tiempo </a><a href="#contenido"><img src="imagenes/subir.jpg" width="15" height="15" border="0"></a></strong></p>
  <p align="left" class="Estilo1">Ya en Seiken Densetsu 3 de SNES exist&iacute;a el concepto de tiempo, es decir que se colocaba de noche, de ma&ntilde;ana, tarde, etc. En Sword of Mana el tiempo solo pasa cuando pasas de una pantalla a otra. Pero aqu&iacute; el tiempo no pasa cuando estas en pantallas cercanas a alguna ciudad. Si te duermes en la noches, despertar&aacute;s en la ma&ntilde;ana. Dentro de los calabozos el tiempo pasa igual, con excepci&oacute;n de la etapa <span class="Estilo5">Mansi&oacute;n Vinquette</span>. Saber la hora dentro de un calabozo es muy dif&iacute;cil, solo se podr&iacute;a saber por medio de los tipos de enemigos que salen.</p>
  <p align="left" class="Estilo1"> El tiempo gira en este sentido seg&uacute;n estos siguientes &quot;bloques de hora&quot;:</p>
  <p align="left" class="Estilo1"> 
    <strong>Ma&ntilde;ana-&gt;D&iacute;a-&gt;D&iacute;a-&gt;D&iacute;a-&gt;Tarde-&gt;Noche-&gt;Noche-&gt;Noche-&gt;Ma&ntilde;ana-&gt; (se repite el ciclo)</strong><br>
    <br>
    Como en  Seiken Densetsu 3 y Legend of Mana, en Sword of Mana cada d&iacute;a de la semana es asignado a alg&uacute;n evento que puede ocurrir, o alg&uacute;n enemigo especial. Tambi&eacute;n el Poder de Ataque de alg&uacute;n Esp&iacute;ritu se incrementa si corresponde a su d&iacute;a (Los Esp&iacute;ritus tiene su d&iacute;a de suerte por as&iacute; decirlo). En el d&iacute;a <strong>Mana Holy Day</strong> todos las  magias incrementan su poder. <br>
    <br>
    El ciclo de D&iacute;as es en este sentido:<br>
    <br>
    <strong>Mana Holy Day -&gt; D&iacute;a de Astro -&gt; D&iacute;a de Salamandra -&gt; D&iacute;a de Dr&iacute;ada - D&iacute;a de Ondina -&gt; D&iacute;a de C&eacute;firo -&gt; D&iacute;a de Duende -&gt; Mana Holy Day -&gt; (se repite el ciclo)</strong></p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo2"><strong><a name="#10">10. Efecto </a>de Status <a href="#contenido"><img src="imagenes/subir.jpg" width="15" height="15" border="0"></a></strong></p>
  <p align="left" class="Estilo1">Hay varios Efectos de Status que puedes tener en el curso del juego, algunos bueno, otros malos.</p>
  <table width="50%" border="0">
    <tr bgcolor="#99CCFF" class="Estilo1">
      <td bgcolor="#99CCFF"><strong>Efectos Positivos </strong></td>
    </tr>
    <tr class="Estilo1">
      <td bgcolor="#EEF2EF"><p align="left">        <strong>PowerUp (Pow Up)</strong><br>
          Duraci&oacute;n : ~10 segundos (~ aproximadamente) <br>
          Efecto : Ataque incrementado<br>
          Magia : Fuego (Fire)<br>
          Continuo : Si</p>
        <p align="left"><strong>D-Fence (Def Up)</strong><br>
          Duraci&oacute;n : ~10 segundos<br>
          Efecto : Defensa incrementada<br>
          Magia : Tierra (Earth)<br>
          Continuo : Si</p>
        <p align="left"><strong>SpeedUp (Spd Up)</strong><br>
          Duraci&oacute;n : ~10 segundos<br>
          Efecto : Velocidad incrementada<br>
          Magia : Viento (Wind)<br>
          Continuo : No</p>
        <p align="left"><strong>PsychicShield (Mdf Up)</strong><br>
          Duraci&oacute;n : ~10 segundos<br>
          Efecto : Magia Defensa incrementada<br>
          Magia : Fuego (Fire)<br>
          Continuo : Si</p>
        <p align="left"><strong>BubbleBoat</strong><br>
          Duraci&oacute;n : ~5 segundos<br>
          Efecto : Flotar en una burbuja que puede ser solo detectada por la vista.<br>
          Magia : Agua (Water)<br>
          Continuo : No</p>
        <p align="left"><strong>Mooglemorphosis</strong><br>
          Duraci&oacute;n : ~10 segundos<br>
          Efecto : Transformarse en un  Moogle que puede ser detectado solo por  el olor.<br>
          Magia : Luna (Moon)<br>
          Continuo : No</p>
        <p align="left"><strong>Silhouette</strong><br>
          Duraci&oacute;n : ~10 segundos<br>
          Efecto : Te haces invisible que puede ser solo detectado por quien te oiga.<br>
          Magia : Oscuro (Dark)<br>
          Continuo : No</p>
        <p align="left"><strong>Best</strong><br>
          Duraci&oacute;n : 6 pantallas <br>
          Efecto : Aumentar el PowerUp y D-Fence<br>
          Magia : Ninguna <br>
          Continuo : Si      </p>      </td>
    </tr>
  </table>
  <br>
  <table width="50%" border="0">
    <tr bgcolor="#99CCFF" class="Estilo1">
      <td><strong>Efectos Negativos </strong></td>
    </tr>
    <tr class="Estilo1">
      <td bgcolor="#EEF2EF"><p align="left">        <strong>Death/Muerte</strong><br>
        Duraci&oacute;n : Infinito<br>
        Efecto : El personaje no puede entrar en pelea y no gana EXP<br>
        Item Cura : C&aacute;liz Ang&eacute;lico/Angel's Grail<br>
        Continuo : Si</p>
          <p align="left"><strong>FireMan/Ardiendo </strong><br>
        Duraci&oacute;n : ~5 segundos<br>
        Efecto : 1/10 del total de tu HP se agota cada ciertos segundos.<br>
        Item Cura : Hierba Estelar<br>
        Continuo : No</p>
          <p align="left"><strong>Petrify/Petrificado</strong><br>
        Duraci&oacute;n : N/A<br>
        Efecto : 1/2 del total de tu HP se consume. No puedes ser herido cuando estas as&iacute;.<br>
        Item Cura : Hierba Estelar<br>
        Continuo : No</p>
          <p align="left"><strong>Numb/Entumecido</strong><br>
        Duraci&oacute;n : ~5 segundos<br>
        Efecto : Armas y Magia no pueden ser usados.<br>
        Item Cura : Hierba Estelar<br>
        Continuo : No</p>
          <p align="left"><strong>SnowMan</strong><strong>/Mu&ntilde;eco de nueve </strong><br>
        Duraci&oacute;n : ~5 segundos<br>
        Efecto : Te transformas en un  Snowman que no se puede mover.<br>
        Item Cura : Hierba Estelar<br>
        Continuo : No</p>
          <p align="left"><strong>Sleep/Dormir</strong><br>
        Duraci&oacute;n : ~5 segundos<br>
        Efecto : Caes Dormido. Puedes ser da&ntilde;ado mientras duermes.<br>
        Item Cura : Hierba Estelar<br>
        Continuo : No</p>
          <p align="left"><strong>Wimp/Enano</strong><br>
        Duraci&oacute;n : ~10 segundos<br>
        Efecto : Te achicas, se decrementa tu ataque y defensa y no puedes usar magia.<br>
        Item Cura : Hierba Estelar, Miniaturizador<br>
        Continuo : No</p>
          <p align="left"><strong>Darkness/Oscuro</strong><br>
        Duraci&oacute;n : ~10 segundos<br>
        Efecto : Decrementa tu % de hit (te baja precisi&oacute;n)<br>
        Item Cura : Hierba Estelar<br>
        Continuo : Si</p>
          <p align="left"><strong>Silence/Silencio</strong><br>
        Duraci&oacute;n : ~10 segundos<br>
        Efecto : Te desactiva usar magia<br>
        Item Cura : Hierba Estelar<br>
        Continuo : Si</p>
          <p align="left"><strong>Poison/Envenenado</strong><br>
        Duraci&oacute;n : ~10 segundos<br>
        Efecto : 1/20 del total de tu HP se consume cada ciertos segundos.<br>
        Item Cura : Vegeant&iacute;doto<br>
        Continuo : Si</p>
          <p align="left"><strong>Toxin/T�xico</strong><br>
        Duraci&oacute;n : ~10 segundos<br>
        Efecto : 1/10 del total de tu HP se consume cada ciertos segundos desde que te envenenas.<br>
        Item Cura : Vegeant&iacute;doto<br>
        Continuo : Si</p>
          <p align="left"><strong>Confusi&oacute;n</strong><br>
        Duraci&oacute;n : ~10 segundos<br>
        Efecto : El control pad es invertido<br>
        Item Cura : Hierba Estelar<br>
        Continuo : Si</p>
          <p align="left"><strong>PowerDown (Pow Down)</strong><br>
        Duraci&oacute;n : ~10 segundos<br>
        Efecto : Ataque se decrementa<br>
        Item Cura : Vegeant&iacute;doto<br>
        Continuo : Si</p>
          <p align="left"><strong>Bedraggled (Def Down)</strong><br>
        Duraci&oacute;n : ~10 segundos<br>
        Efecto : Defensa F&iacute;sica se decrementa<br>
        Item Cura : Vegeant&iacute;doto<br>
        Continuo : Si</p>
          <p align="left"><strong>SpeedDown (Spd Down)</strong><br>
        Duraci&oacute;n : ~10 segundos<br>
        Efecto : Tu velocidad se decrementa<br>
        Item Cura : Vegeant&iacute;doto<br>
        Continuo : No</p>
          <p align="left"><strong>Bewitched (Mdf Down)</strong><br>
        Duraci&oacute;n : ~10 segundos<br>
        Efecto : Tu defensa m&aacute;gica se decrementa<br>
        Item Cura : Vegeant&iacute;doto<br>
        Continuo : Si</p>
          <p align="left"><strong>Pose (Stop)/Detenido</strong><br>
        Duraci&oacute;n : ~5 segundos<br>
        Efecto : Completa inmovilizaci&oacute;n<br>
        Item Cura : Hierba Estelar<br>
        Continuo : No<br>
        </p></td>
    </tr>
  </table>
  <p>&nbsp;</p>
  <p align="left" class="Estilo2"><strong><a name="#11">11. Trampas</a>
	<a href="#contenido"><img src="imagenes/subir.jpg" width="15" height="15" border="0"></a></strong></p>
  <p align="left" class="Estilo1">Cuando eliminas a todos los enemigos de una pantalla, aparece un cofre y cuando lo abres una Ruleta de Trampas se muestra. Si presionas el bot&oacute;n y la detienes en Ok, se te regala un item, pero si lo paras en alg&uacute;n s&iacute;mbolo de trampa, se activar&aacute; una de las 7 trampas que existen.<br>
    Cada enemigo en el juego es asignado a un Tipo de Trampa el cual esta determinado en la Ruleta de Trampas. <br>
    Puedes ir de  caza de &Iacute;tem con las Trampas, especialmente con enemigos tipo H es decir, los mas complicados, pero que dan mejores Item. Un truco f&aacute;cil es  siendo Ladr&oacute;n abrir el cofre y se reemplazar&aacute; un s&iacute;mbolo Trampa por uno Ok, siendo un N&oacute;mada o Ninja, se reemplazan 2 Ok y para las clases  finales se reemplazan 3 Ok. <br>
  </p>
 
  <table width="83%" height="275"  border="0">
    <tr>
      <td width="40%" height="271" class="Estilo1"><img src="imagenes/ruleta.JPG" width="280" height="225"></td>
      <td width="60%"><table width="97%" border="0" align="left">
        <tr bgcolor="#99CCFF" class="Estilo1">
          <td width="30%"><strong>Tipo de Trampa </strong></td>
          <td width="9%"><strong>#OK</strong></td>
          <td width="8%"><strong>Spi</strong></td>
          <td width="8%"><strong>Arr</strong></td>
          <td width="9%"><strong>Sto</strong></td>
          <td width="10%"><strong>Bom</strong></td>
          <td width="9%"><strong>Smo</strong></td>
          <td width="8%" bgcolor="#99CCFF"><strong>Pol</strong></td>
          <td width="9%"><strong>Kai</strong></td>
        </tr>
        <tr class="Estilo1">
          <td>A</td>
          <td>4</td>
          <td>o</td>
          <td>o</td>
          <td>x</td>
          <td>x</td>
          <td>x</td>
          <td>x</td>
          <td>x</td>
        </tr>
        <tr bgcolor="#DCEBEF" class="Estilo1">
          <td>B</td>
          <td>3</td>
          <td>o</td>
          <td>o</td>
          <td>o</td>
          <td>x</td>
          <td>x</td>
          <td>x</td>
          <td>x</td>
        </tr>
        <tr class="Estilo1">
          <td>C</td>
          <td>3</td>
          <td>o</td>
          <td>o</td>
          <td>o</td>
          <td>x</td>
          <td>x</td>
          <td>o</td>
          <td>x</td>
        </tr>
        <tr bgcolor="#DCEBEF" class="Estilo1">
          <td>D</td>
          <td>3</td>
          <td>x</td>
          <td>x</td>
          <td>o</td>
          <td>o</td>
          <td>o</td>
          <td>o</td>
          <td>x</td>
        </tr>
        <tr class="Estilo1">
          <td>E</td>
          <td>3</td>
          <td>x</td>
          <td>x</td>
          <td>o</td>
          <td>o</td>
          <td>o</td>
          <td>o</td>
          <td>x</td>
        </tr>
        <tr bgcolor="#DCEBEF" class="Estilo1">
          <td>F</td>
          <td>3</td>
          <td>x</td>
          <td>x</td>
          <td>x</td>
          <td>o</td>
          <td>o</td>
          <td>x</td>
          <td>o</td>
        </tr>
        <tr class="Estilo1">
          <td>G</td>
          <td>2</td>
          <td>x</td>
          <td>x</td>
          <td>o</td>
          <td>o</td>
          <td>o</td>
          <td>o</td>
          <td>o</td>
        </tr>
        <tr bgcolor="#DCEBEF" class="Estilo1">
          <td>H</td>
          <td>1</td>
          <td>o</td>
          <td>o</td>
          <td>o</td>
          <td>o</td>
          <td>o</td>
          <td>o</td>
          <td>o</td>
        </tr>
        <tr bgcolor="#DCEBEF" class="Estilo1">
          <td height="112" colspan="9" bgcolor="#FFFFFF">Spi - Pinchos - 1/10 del m&aacute;ximo de tu HP.<br>
Arr - Flechas - 1/7 max HP.<br>
Sto - Piedras - 1/5 max HP.<br>
Bom - Bombas - 1/3 max HP.<br>
Smo - Humo - 1/3 max MP.<br>
Pol - Polter Box - Pelea contra un Polter Box.<br>
Kai - Kaiser Mimic - Pelea contra un Kaiser Mimic.</td>
          </tr>
      </table>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p><br>
        <br>
      </td>
    </tr>
  </table>
  <p align="left" class="Estilo1">NOTA: Parar la Ruleta en <strong>Polter Box </strong>o <strong>Kaiser Mimic </strong>puede ser malo ya que pierdes el Item que hab&iacute;a en el cofre, as&iacute; que siempre trata de sacar un  OK.</p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo2"><strong><a name="#12">12. Combos </a><a href="#contenido"><img src="imagenes/subir.jpg" width="15" height="15" border="0"></a></strong></p>
  <p align="left" class="Estilo1">Hay armas que te permiten hacer un combo. Presiona bot&oacute;n para golpear, luego en la direcci&oacute;n que esta el enemigo presiona el pad y luego el bot&oacute;n de golpe inmediatamente para hacer un combo. La idea de los combos es que el primer golpe  hace un <span class="Estilo5">da&ntilde;o normal</span> el segundo hace un <strong>150%</strong> y el 3ro hace un <strong>200%.</strong></p>
  <table width="55%" border="0">
    <tr bgcolor="#99CCFF" class="Estilo1">
      <td bgcolor="#99CCFF">Arma</td>
      <td>Golpe1</td>
      <td>Golpe 2 </td>
      <td>Golpe 3 </td>
    </tr>
    <tr class="Estilo1">
      <td>Espada (Sword)</td>
      <td class="Estilo1">Slash</td>
      <td class="Estilo1">Return Slash</td>
      <td>Jump Slash</td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td>Vara (Rod)</td>
      <td>Strike</td>
      <td>Return Strike</td>
      <td>Jump Strike</td>
    </tr>
    <tr class="Estilo1">
      <td>Manopla (Knuckle)</td>
      <td>Punch</td>
      <td>Kick</td>
      <td>Jumping Upper</td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td bgcolor="#DCEBEF">Lanza (Lance) </td>
      <td>Thrust</td>
      <td>Throw</td>
      <td>Swing</td>
    </tr>
    <tr class="Estilo1">
      <td>Hacha (Axe) </td>
      <td>Slash</td>
      <td>Return Slash</td>
      <td>Jump Slash</td>
    </tr>
  </table>
  <p>&nbsp;</p>
  <p align="left" class="Estilo2"><strong><a name="#13">13. Preguntas Frecuentes </a><a href="#contenido"><img src="imagenes/subir.jpg" width="15" height="15" border="0"></a></strong></p>
  <table width="100%"  border="0">
    <tr>
      <td width="20%"><img src="imagenes/ffa_seiken_gb.JPG" width="243" height="220"></td>
      <td width="80%"><span class="Estilo1"><span class="Estilo6">a) &iquest;Qu&eacute; es Shinyaku Seiken Densetsu/Sword of Mana?</span><br>
        <br>
      Sword of Mana o Shinyaku Seiken Densetsu (nombre en japon&eacute;s) es un &quot;remake&quot; del primer juego de la serie Seiken Densetsu conocido como <strong>Seiken Densetsu: Final Fantasy Gaiden</strong> o <strong>Legend of Holy Sword</strong>, o <strong>Final Fantasy Adventure</strong> en Estados Unidos, que apareci&oacute; en la port&aacute;til <strong>Game Boy</strong>. Sword of Mana es casi un juego nuevo ya que mejora gr&aacute;ficos, tiene una historia mas profunda, mejora la forma de jugar y se puede jugar multiplayer.</span></td>
    </tr>
  </table>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo6">b) &iquest;Que juegos componen la serie de Seiken Densetsu? </p>
  <p align="left" class="Estilo1">Los lanzamientos fueros los siguientes:</p>
  <table width="70%" border="0">
    <tr bgcolor="#99CCFF" class="Estilo1">
      <td width="13%"><div align="center"><strong>A&ntilde;o</strong></div></td>
      <td width="46%"><div align="center"><strong>Jap&oacute;n</strong></div></td>
      <td width="41%"><div align="center"><strong>Estados Unidos </strong></div></td>
    </tr>
    <tr class="Estilo1">
      <td><div align="center">1991</div></td>
      <td><div align="center">Seiken Densetsu: Final Fantasy Gaiden (GB)</div></td>
      <td><div align="center">Final Fantasy Adventure  (*) </div></td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td><div align="center">1993</div></td>
      <td><div align="center">Seiken Densetsu 2 (Super Famicon o SFC) </div></td>
      <td><div align="center">Secret of Mana</div></td>
    </tr>
    <tr class="Estilo1">
      <td><div align="center">1995</div></td>
      <td><div align="center">Seiken Densetsu 3 (SFC) </div></td>
      <td><div align="center">No hubo lanzamiento (**) </div></td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td bgcolor="#DCEBEF"><div align="center">1999</div></td>
      <td><div align="center">Seiken Densetsu: Legend of Mana (PSX)</div></td>
      <td><div align="center">Legend of Mana</div></td>
    </tr>
    <tr class="Estilo1">
      <td><div align="center">2003</div></td>
      <td><div align="center">Shinyaku Seiken Densetsu (GBA)</div></td>
      <td><div align="center">Sword of Mana</div></td>
    </tr>
    <tr class="Estilo1">
      <td bgcolor="#DCEBEF"><div align="center">2005</div></td>
      <td bgcolor="#DCEBEF"><div align="center">NDS</div></td>
      <td bgcolor="#DCEBEF"><div align="center">Sword of Mana</div></td>
    </tr>
  </table>
  <p align="left" class="Estilo1">*: La mayor&iacute;a de la gente lo considera como el primer Final Fantasy.<br>
    **: Este juego que sali&oacute; en Jap&oacute;n solamente, fue traducido primero al Ingl&eacute;s por un grupo de Traductores, luego al Franc&eacute;s y luego al Espa&ntilde;ol (por fortuna de muchos) por un <a href="http://www.wikipedia.org/romhacker">romhacker</a> llamado <b>Magno</b>. A este juego se le conoce como <strong>Secret of Mana 2.</strong></p>
  <p align="center" class="Estilo1"><img src="imagenes/secret%20of%20mana%202.jpg" width="280" height="220"><br>
  Secret of Mana 2 al Espa&ntilde;ol.</p>
  <p align="left" class="Estilo6">c) &iquest;Que es  el Secreto del Mana (Secret of Mana)?</p>
  <p align="left" class="Estilo1">El secreto de mana es una <strong>espada</strong>, que en el original Seiken Densetsu le llamaron Excalibur.</p>
  <p align="left" class="Estilo6">d) &iquest;Donde conecta Sword of Mana en la l&iacute;nea de tiempo de la serie de <span class="Estilo1">Seiken Densetsu</span>? </p>
  <p align="left" class="Estilo1">No entra en ninguna l&iacute;nea de tiempo ya que oficialmente no est&aacute;n conectados los juegos de la serie Mana. Esto ser&iacute;a posible en los Final Fantasy. Sin embargo hay gente que cree que el SD1 y SD2 est&aacute;n conectados y los otros son independientes. Y tambi&eacute;n hay gente que dice que los 3 primeros SD est&aacute;n conectados. </p>
  <p align="left" class="Estilo6">e) &iquest;Quien es Anaguma?</p>
  <p align="left" class="Estilo1">Es mas conocido como Dudbears en Am&eacute;rica, juego un rol extra&ntilde;o en Sword of Mana. Es un trabajador esclavo para los  Dwarven Miners.</p>
  <p align="left" class="Estilo6">f) &iquest;Porqu&eacute; mi personaje brilla?</p>
  <p align="left" class="Estilo1">Es porque tu barra de <strong>Golpe Mortal </strong> est&aacute; al m&aacute;ximo y puedes realizar un ataque especial. Manten el bot&oacute;n A presionado y su&eacute;ltalo sobre el enemigo y har&aacute;s un ataque con el doble de da&ntilde;o de lo normal y adem&aacute;s aturdir&aacute;s a tus enemigos. Cada arma tiene un ataque especial distinto. </p>
  <p align="left" class="Estilo6">g) &iquest;El H&eacute;roe puede usar Magias de Ataque?</p>
  <p align="left" class="Estilo1">Si puede. El primer esp&iacute;ritu  Salamander esta en  Villa Vinquette. En total, en el juego podr&aacute; obtener 8 esp&iacute;ritus.</p>
  <p align="left" class="Estilo6">h) &iquest;Puedo asignar los puntos de mi ayudante cuando este  sube de nivel?</p>
  <p align="left" class="Estilo1">No puedes escoger los puntos de subida de tus amigos. Por lo tanto no puedes escoger sus clases.</p>
  <p align="left" class="Estilo6">i) Me gusta mucho la m&uacute;sica de Sword of Mana &iquest;donde puedo obtenerla?</p>
  <p align="left" class="Estilo1">En Jap&oacute;n (o quiz&aacute;s en eMule). Una empresa llamada <strong>Digicube</strong> lanz&oacute; un OST con la versi&oacute;n japonesa del juego. Son 2 CDs. El primer CD es el OSV<br>
(original sound version) y el segundo CD son pianos de algunas canciones. Cuesta 3,000 yenes y se puede comprar desde <a href="www.cdjapan.co.jp">www.cdjapan.co.jp</a>.</p>
  <p align="left" class="Estilo1">&nbsp;</p>
  <p align="left" class="Estilo2"><strong><a name="#14">14. Gu&iacute;a de Pasos </a><a href="#contenido"><img src="imagenes/subir.jpg" width="15" height="15" border="0"></a></strong></p>
  <p align="left" class="Estilo1">NOTA: Como el H&eacute;roe y Hero&iacute;na no tienen nombres predefinidos, simplemente se les llamar&aacute;n H&eacute;roe y Hero&iacute;na. </p>
  <p align="left" class="Estilo1"><br>
    <span class="Estilo2"><strong><a name="#14.1">14.1 H&eacute;roe </a></strong></span><span class="Estilo2"><strong><a href="#contenido"><img src="imagenes/subir.jpg" width="15" height="15" border="0"></a></strong></span><br>
  </p>
  <p align="left" class="Estilo1"><strong>Castillo de Granz. Arena.</strong><br>
  Cuando comienza el juego, H&eacute;roe recuerda en un sue&ntilde;o el pasado, un sue&ntilde;o que se repite constantemente, all&iacute; est&aacute;n sus papas, el Se&ntilde;or Oscuro y una extra&ntilde;a ni&ntilde;a. Luego est&aacute;s encerrado con otros chicos y planeas escapar. Aqu&iacute; empieza jugar.<br>
  Solo habla con todos y espera. Luego habla con el tipo del centro y listo. <br>
  Willy tendr&aacute; un plan de escape, luego te llaman a pelear. </p>
  <table width="100%"  border="0">
    <tr>
      <td width="29%" class="Estilo1"><img src="imagenes/screen1.JPG" width="280" height="225"></td>
      <td width="71%" class="Estilo1"><div align="left">
        <p>Elimina al  monstruo llamado <strong>Jackal </strong>(HP 55, Nivel 7) y escapa por el norte. All&iacute; dices a Willy que quieres vengarte del Se&ntilde;or Oscuro y Willy dice que ha o&iacute;do el nombre de la chica de los sue&ntilde;os del H&eacute;roe pero que despu&eacute;s hablar&aacute; de ello. Luego llega el se&ntilde;or Oscuro, H&eacute;roe se enfrente a &eacute;l, pierde y cae por un puente. Te salva un conejo llamado<strong> Niccolo</strong> el vendedor ambulante. Debes ir a <strong>Toople. </strong>
  
          <br>
            <br>
            OJO: 
            Desde aqu&iacute; ya puedes empezar a pelear y a ganar Niveles. Pone OJO ya que desde el Nivel 1 se define tu CLASE, as&iacute; que cada vez que subas un nivel mira la Secci&oacute;n <a href="#7">Clases Especiales</a> para escoger bien a que cualidad darle el punto que definir&aacute; tu clase. A mi me gust&oacute; la clase <span class="Estilo5">Random</span> que se especializa en el arma Lanza. <br>
            <br>
            <strong>Camino a Toople</strong><br>
            <br>
            Enemigos: Gazapo (Rabite)   HP: 20, Chobin Hood   HP: 40<br>
            <br>
            Baja al sur y te encuentras con una chica, ella tambi&eacute;n va a Toople que queda al Noroeste de all&iacute;,  ve por el sur, pero antes ay&uacute;dale a encontrar un libro que te pide que lo busques entre la hierba. <br>
            Sigue al sur, luego al este, luego al norte hasta que llegues a Toople.<br>
            <br>      
            <strong><br>
          </strong></p>
        </div></td>
    </tr>
  </table>
  <p align="left" class="Estilo1"><strong>Toople<br>
    <br>
  </strong><strong>
  </strong>Aqu&iacute; te informas que alguien cerr&oacute; las cuevas hacia <strong>Wendel, </strong>se dice que fue el due&ntilde;o de la <span class="Estilo5">Villa Vinquette</span>, que es de raza  Mavole (como raza vampiro). Tambi&eacute;n te dicen que para derrotar al Se&ntilde;or Oscuro debes tener la <strong>Espada Mana</strong>, y <strong>Cibba</strong> el Sabio sabe de esto, pero vive en <strong>Wendel</strong>. Un abuelo de la casa de la derecha te regala la <span class="Estilo5">Cuerda m&aacute;gica </span>(Magic Rope) que sirve cuando est&eacute;s dentro de alguna mazmorra o cueva y al usarla te deja en la entrada. Tambi�n con esto ganar�s la <strong>Nota de Popoi #5</strong> (una nota que se almacena en una agenda llamada Libreta de Popoi). Tambi&eacute;n te dicen que hay un maestro espadach&iacute;n, Cibba, en una Casita Cascada afuera del pueblo. <br>
  <br>
  Sube al norte y ver&aacute;s la estatua donde debes rezar a la diosa para salvar, luego ve a la izquierda por el camino que hay y Niccolo el conejo vendedor ambulante, te pregunta que har&aacute;s, elige la primera opci&oacute;n, luego te ofrece la <strong>Libreta de Popoi</strong> (Popoi's Notebook) por 10 Lucres. Si los tienes, c&oacute;mprasela, si no, ve afuera del pueblo, elimina  unos conejos y vuelve. Esta libreta almacena monstruos que has eliminado, notas del juego, etc. Es imprescindible.<br>
  <br>
  Habla durante el d&iacute;a con la Mujer Topplense para obtener <strong>Nota de Popoi #13 </strong>que se agrega a la Libreta de Popoi. <br>
  En la noche caminando abajo del Inn habla con el Abuelo Topplense para obtener <strong>Nota de Popoi #21.</strong><br>Dentro del Inn el Se&ntilde;or Topplense  te da <strong>Nota de Popoi #2.<br>
  </strong>En el Sur durante el d&iacute;a, en la tienda de abajo-izquierda, habla con el Abuelo Topplense para obtener <strong>Nota de Popoi #14.</strong>Tambi&eacute;n hay un abuelo que te hace un Elixir si le llevas ciertos ingredientes. <strong><br>
  </strong>Duerme en el Inn para recuperar toda la fuerza y estar al m&aacute;ximo con la Defensa y Poder (aparecer&aacute; un &quot;Super&quot; en la Cabeza del personaje). Luego en el segundo piso hay un cofre con un Caramelo.<br>
  Si entras de noche al Inn, en el segundo piso esta <strong>Blassie el III</strong>, un tipo te pedir&aacute; <strong>Bolsas de Sangre </strong>(Blood Pouches) de una cueva con murci&eacute;lagos, parar&aacute; 100 Lucres por cada una. Entra a la secci&oacute;n de <a href="#18">Quest</a> para mas detalles. <br>
  Lo mismo si entras de d&iacute;a en la tienda de abajo-izquierda, habla con el ni�o y te pide que le <strong>lleves comida</strong> (un Trit�n Asado) a su padre Marco en Wendel. Ve secci�n <a href="#18">Quest</a> para m�s info.<br>
  Antes de irte, aseg&uacute;rate de hablar con la anciana que anda en las calles y con el hombre (visiblemente parece mujer) que vive de la casa del noroeste que habla de la cantidad de nuevo monstruos que han aparecido &uacute;ltimamente.<br>
  Sal por el camino de la izquierda del pueblo.</p>
  <p align="left"><span class="Estilo1 Estilo5">Camino a la Cueva<br>
    </span><span class="Estilo1"><br>
    Enemigos: Gazapo (Rabite)   HP: 20</span><span class="Estilo1">, Avisp&oacute;n (BeBe) HP: 17</span><span class="Estilo1">, Batmo   HP: 23 (solo noche) <br>
    <br>
    Estos nuevos enemigos que salen de noche, llamados Batmo te pueden dejar confuso e invertir el control. Avanza hasta que te encuentres con un cartel que dice &quot; &iexcl;No hay acceso a la cueva!&quot; por lo que no podr&aacute;s seguir por el camino del norte, as&iacute; que sigue a la izquierda. Llegar&aacute;s a Casita Cascada.

<br>
<br><strong>Casita Cascada<br>
</strong>

<br>
Viniste aqu&iacute; a buscar al maestro espadach&iacute;n y solo encuentras una carta que dice &quot;Nos encontramos en Wendel&quot;. Justo llega Hero&iacute;na, la carta era para ella y cree  que eres un  Cazaherejes para el reino. Le aclaras que tu no trabajas para el se&ntilde;or Oscuro. Ella busca a <strong>Bar&oacute;n Bogard</strong>. All&iacute; ella invoca un Esp&iacute;ritu llamado <strong>Lumina</strong> (en ingl&eacute;s le llaman Wisp), es un esp&iacute;ritu de Luz de magia curativa y  tambi&eacute;n te ense&ntilde;a a usar magia.  Como tu tienes que ir a ver a Cibba a Wendel y ambos a Bar&oacute;n Bogard, entonces deciden ir juntos, as&iacute; que se une a ti. <br>
Sal de la casa y salva con la diosa de afuera a la izquierda. 
Luego ve a la derecha y ella te ense&ntilde;a la habilidad de <strong>saltar.</strong> Con el Bot&oacute;n <strong>L</strong> saltas. Ahora sal por el este.<br>
    <br>
    Vuelve por el camino donde dec&iacute;a que estaba cerrada la cueva, ahora sube saltando. Mas arriba ver�s a Niccolo que te dice que mas adelante hay unos enemigos llamados <strong>Metabols</strong> (en ingl&eacute;s Metaballs) y que para poder pasar, debes encontrar su debilidad y que cada uno de ellos tiene una debilidad. Con ellos podr&aacute;s subir algunas armas al nivel 99 (Ver <a href="#19">Secretos</a>). Esta conversaci�n pasa a guardarse en <strong>Nota de Popoi #17.</strong> Salva en la estatua del norte y entra a la cueva. 
<br>
</span></p>
  <table width="100%"  border="0">
    <tr>
      <td width="28%" class="Estilo1"><img src="imagenes/screen2.JPG" width="280" height="225"></td>
      <td width="72%"><div align="left" class="Estilo1"><br>
          <strong>Cueva Vampi�laga (Batmo Cave)</strong><br>
          <br>
Enemigos: Vampi�lago (Batmo) HP: 23, Chobin Hood HP: 40, Baba Lima (Lime Slime) HP: 20.<br>
<br>
Avanza sin problemas, hasta que te encuentres con unas bolas grises, estas son los Metabols. Algunos mueren con un golpe de espada, otros con flechas y otros con tu Arco o Manopla. Los enemigos como babosas verdes (Babalimas) solo mueren con flechas o magia de Hero�na.<br>
<br>
Sigue hasta que te encuentres con una camino a la derecha y otro arriba, si subes y matas a todos los enemigos que tiran flechas, obtendr�s el <strong>Arco.</strong> Luego sigue a la derecha, luego baja y en un camino a la izquierda, hay un vampi�lago de esos azules, elim&iacute;nalo y te dar� Bolsa de Sangre (te lo piden de noche, en el segundo piso del Inn de Toople). Un poco mas a abajo te encuentras un con Metabol complicado (imagen izquierda) que no muere con espadasos ni con flechas, a este debes golpearlo con la VARA de Hero&iacute;na (juega con ella). Al eliminarlo, podr&aacute;s salir de la cueva. <br>
        <br>
        <strong><br>
        </strong><br>
        <br>
      </div></td>
    </tr>
  </table>
  <p align="left" class="Estilo1"><strong>Aleda&ntilde;os de Vinquette </strong>(Debes tener 50 Lucres por obligaci&oacute;n para comprar algo) <strong><br>
    </strong><br>
    Enemigos: Needlebeak HP: 45 solo d&iacute;a, Baba Lima HP: 20 solo noche. <br>
    <br>
    A la salida de la cueva a veces est&aacute; Niccolo, al que as&iacute; que podr&aacute;s comprarle items. Luego ve por la izquierda-abajo. 
    <br>
    Sigue a la derecha y luego bajando hasta que te encuentres con un cartel que dice:<br>
&quot;Al norte: Villa Vinquette, Al Sur: Estero Escamado&quot;. Ve al Sur. <br>
A penas avances a la derecha aparecer&aacute; Niccolo ofreciendo por 50 Lucres una <strong>Escencia de Cactus</strong>. &iexcl;C&oacute;mpraselo o Niccolo se va!. Con la escencia puedes plantarla a la derecha, y podr&aacute;s crear un <strong>Casa Cacto</strong> (Hot House). Adentro hay un cactus en recepci&oacute;n llamado <strong>Cactolillo</strong> (en ingl&eacute;s le pusieron Li'l Cactus) que habla poco, a la izquierda una herrer&iacute;a vac&iacute;a por ahora, y arriba un &Aacute;rbol llamado <strong>Trent</strong> que te pide 2 semillas para hablar (te las dan los conejos cuando los eliminas). <br>
Cuando termines sal de la Casa Cacto y ver&aacute;s que el cactus camina y escribe algo en el cactus grande y verde de la derecha. Vuelve a construir una casa Cacto y revisa lo que escribi&oacute;. All&iacute; hay un diario con todo lo que pasa en el juego al H&eacute;roe y Hero&iacute;na. Rev&iacute;salo cada vez que levantes una casa cacto. <br>
Si tienes semillas ll&eacute;vaselas al &aacute;rbol grande y luego sal, ve a pelear y vuelve mas tarde y ver&aacute;s arriba del Trent muchas frutas o vegetales, que puedes cortar y caer&aacute;n y as&iacute; las coges. Diferentes semillas producen diferentes vegetales o frutas. Las frutas y vegetales permiten mejorar tus armas y armadura con los enanos en esta misma Casa Cacto, pero mas adelante. <br>
</p>
  <table width="100%"  border="0">
    <tr>
      <td><img src="imagenes/screen4.JPG" width="280" height="225"></td>
      <td><div align="left" class="Estilo1">Sal de Casa Cacto y a la izquierda r&eacute;zale a la diosa (salva el juego). Ahora regresa al norte, luego al norte pero por la derecha, para ir a las Rejas de Mansi&oacute;n, pero debes ir de noche, as&iacute; que pasa de pantalla en pantalla o regresa hasta que se haga de noche, as&iacute; las rejas estar&aacute;n abiertas. <br>
        Al entrar un hombre misterioso te dice que te apartes y que su maestro rapta mujeres para chuparles la sangre. Hero&iacute;na no esta all&iacute;, ya que vio un supuesto fantasma de un hombre triste. Sube y entras a la Villa Vinquette. <span class="Estilo7"><br>
        <br>
        Villa Vinquette<br>
        </span>Esta villa es en verdad una mansi&oacute;n (como un hotel). Al entrar un mayordomo dice que est&aacute; dispuesto a atenderlos mientras su amo se despierta, luego llega una cliente que al parecer estaba afuera, se llama <strong>Srta. Isabela</strong>. Al final decides quedarte a dormir. </div></td>
    </tr>
  </table>
  <p align="left"><span class="Estilo1">En el cuarto abre el cofre, luego sal del cuarto, afuera graba. Luego arriba esta el cuarto de Isabella, entra all�. <br>
    Ella dir� que es una hu&eacute;sped pero conoce mucho al <strong>Conde,  </strong>due&ntilde;o de aquel lugar. Ella es su "novia" y le pedir� al Conde que habr&aacute; las cuevas a Wendel. Te dice que hay un <strong>Esp�ritu de Agua</strong> atrapado en la cueva <span class="Estilo5">Pantanal</span> (al este del all�). Sal del cuarto y explora el primer piso, pero todas las puertas est�n cerradas as� que vuelve a tu cuarto. All� te quedas dormido. <br>
Sue�as de nuevo con la chica que intentaste salvar. En el sue&ntilde;o  te caes y le dices que ella se salve, mientras tras de ti llegan unos soldados. Al despertar, Hero�na no est� en su cama. Sal del cuarto, el mayordomo dir� que la chica promete mucho. Ve al cuarto de Isabella y lee el libro que hay all&iacute;. Habla de un trovador con una hermosa voz llamado <strong>Granz</strong>. El diario no es de ella. Baja al primer piso y ve a la puerta de la izquierda.<br>
<strong><br>
Dentro del Cuarto<br>
</strong><br>
Enemigos:  Bestia Testa (Skull Beast)   HP: 25   solo noche, Zombie   HP: 34   solo noche<br>
<br>
Equipa el Arco y ve a
la izquierda. Abre el cofre y encontrar&aacute;s un Moneda de Luz. Entra a la puerta que est&aacute; a la izquierda-arriba. Sigue avanzando, en un cuarto hay un ba&uacute;l con Nuez M&aacute;gica (Magic Walnut). Luego al fondo de un pasillo hay un cuarto, elimina al Metabol con tu arco y ganar&aacute;s el <strong>Esp&iacute;ritu Salamandra </strong>(Salamander). Este esp&iacute;ritu te da la habilidad de <span class="Estilo5">Rezar</span> manteniendo el bot&oacute;n A y luego L. Con esto podr&aacute;s rezar o sentarte y as&iacute; luego de unos pocos segundos podr&aacute;s recuperar MP de a poco. Ahora ve al Men&uacute; Anillo y debes equipar el esp&iacute;ritu y podr&aacute;s usarlo con el bot&oacute;n <strong>R</strong>. <br>
<br>
Sal del cuarto y regresa al pasillo y prueba tu nueva magia. Si lo haces quieto te da poder. El ataque depende del arma que tengas equipada, si tienes la espada saldr&aacute; una bola del piso, y si es un arco, sale una bola de fuego, si presionas R solo un instante te llena tu poder al M&aacute;ximo (magia PowerUP). <br>
Ahora sube al primer piso, ve por la pasada de la derecha y ve al cuarto del fondo de la derecha, adentro hay un nuevo enemigo, un Zombie. Estos mueren con magia fuego. Lo que mas f&aacute;cil es equiparte el arco y equipar Salamadra y luego presionar R para hacer una bola de fuego y tir&aacute;rsela al Zombie. La t&eacute;cnica es ponerte bajo el candelabro y el sobre el candelabro y as&iacute; el no avanzar&aacute; -no tiene mucha Inteligencia Artificial- y le das con las bolas de fuego. Las bolas  usan 6 de MP as&iacute; que si tienes 11 de MP podr&aacute;s tirarle 1 bola y luego debes cargar con los botones A + L y as&iacute; seguir hasta acabarlo. Luego entra por la puerta norte. <br>
Aqu&iacute; sube pero no entres a menos que quieras pelear con 3 Zombies, cosa poca recomendable por la poca experiencia que dan (4 EXP). Tampoco entres a la ultima puerta (3 Zombies m&aacute;s), as&iacute; que ve por la de al medio. Sube y luego baja las escaleras. Luego entra la primera puerta que te lleva el sub-jefe, la &uacute;ltima tiene 3 zombies. <br>
    Adentro te pillas con el hombre que estaba en la entrada de la Villa Vinquette, y dice buscar una prometida para el conde, luego se va. Llega el mayordomo macabro que se convierte en el Sub-Jefe.
  </span></p>
  <table width="100%"  border="0">
    <tr>
      <td width="29%"><img src="imagenes/conde.JPG" width="280" height="225"></td>
      <td width="71%"><div align="left" class="Estilo1"><strong><strong>SUB-JEFE: Mayordomo</strong> <br>
HP: 90, Exp: 5 <br>
</strong>Cualquier arma sirve, dale con las flechas hasta liquidarlo. Ataque con Salamandra no sirve a menos que la tengas en un buen nivel. Lo que si sirve es con Salamandra hacer el PowerUP (con R presionado una vez, quita 3 MP).<br>
<br>
Luego sigue derecho hasta que te pilles con 2 puertas, entra a la segunda. Avanza hasta encontrar la Diosa, salva alli y entra al norte para encontrarte con el Jefe. Adentro esta Hero&iacute;na con Isabella, y el Conde tiene a Isabella en el aire, durmiendo. Las quiere para &eacute;l. <strong><br>
<br>
JEFE: Conde Lee</strong><br>
  HP: 130, Exp: 10<br>
  El Arco es ineficiente. Usa la espada cuando est&eacute;s cerca de &eacute;l.<br>
  <br>
  Al eliminarlo
   aparecer&aacute; el fantasma de Lord Granz y dice el &eacute;l le pidi&oacute; al conde que protegiera a las mujeres de los cazaherejes (el conde ser&iacute;a un h&eacute;roe entonces y no un chupasangre). Te dice que en la Cueva Pantanal, camino a Wendel encontrar&aacute;s el esp&iacute;ritu del Agua. Luego te vas y sales corriendo con Hero&iacute;na. All&iacute; te das cuenta reci&eacute;n que ella es la ni&ntilde;a de tus sue&ntilde;os. Y que el se&ntilde;or Oscuro mat&oacute; a tus padres por que la quer&iacute;a a ella. Luego debes ir a Wendel. <br>
   <br>
   </div></td>
    </tr>
  </table>
  <p align="left" class="Estilo1">Ve al Sur, si quieres pasas por la <span class="Estilo5">Casa Cacto</span> a buscar fruta o a plantar semillas, luego sigue al sur. Sigue al sur cruzando el puente, luego coge el Cofre (Aros Albalorios) y a la derecha hay una cueva, pero necesitas Magia de Agua adentro, as&iacute; que sube por el puente y ve a la derecha y luego al norte. <br>
    <br>
    <strong>Camino a Wendel</strong><br>
    <br>
    Enemigos: Flora Mort&iacute;fera (Death Flora)   HP: 35, Picafilado (Needlebeak)   HP: 45 solo d&iacute;a, Tonpole   HP: 40, Lizardon   HP: 62<br>
  <br>
  Avanza por el norte, luego cuando haya un camino al norte y a la derecha, ve primero al norte a buscar un C&aacute;liz Angelical y luego a la derecha continua el camino. Luego tendr&aacute;s un camino arriba-derecha y otro arriba-izquierda, primero ve por el de la derecha, salva con la Diosa y regresa por el de arriba-izquierda. Continua por el camino hasta encontrar la Cueva Pantanal. </p>
  <p align="left" class="Estilo1"><strong>Cueva Pantanal</strong><br>
    <br>
    Enemigos: Sanguijuela (Land Leech)   HP: 31, Lagart&oacute;n (Lizardon)   HP: 62, Patoldado (Duck GI)   HP: 68<br>
    <br>
    Sube y encontrar&aacute;s una especie de volc�n met�lico, estos son los 
	famosos <strong>Stone Seal </strong>que se pone de un color que indica cual 
	es su debilidad. Este se pone rojo al tirarle una flecha, entonces&nbsp; equipa Salamandra y disp&aacute;rale la bola de fuego, caer&aacute; y podr&aacute;s seguir. <br>
    Avanza, sin problemas, avanza al sur, por las plantas sobre el pantano coge el cofre, luego ve por el sur. Aqu&iacute; podr&aacute;s  bajar o ir a la derecha, escoge abajo (hay 2 cobres abajo a la derecha). <br>
    En el siguiente cuarto, solo sigue para abajo, habr&aacute; un Metabol bloqueando el paso a la derecha. <br>
    Abajo encontrar&aacute;s un cofre con una Semilla Larga (Long Seed). Vuelve al norte.<br>
    Aqu&iacute; elimina al metabol con un espadazo y sal por la derecha. <br>
    En este gran cuarto con hojas verdes sal por abajo-izquierda,  y encontrar&aacute;s una Semilla Torcida (Crooked Seed). Vuelve a la sala anterior y ve a la derecha para salir por una pasada que hay all&iacute; hacia el sur. Al pasar escuchar&aacute;s un grito del H&eacute;roe que se cae. <br>
    En este cuarto mata a todos los enemigos y recibir&aacute;s un <strong>Mayal</strong> (Flail). Usa esta misma arma para pasar por arriba-derecha ya que se engancha en los postes. Sal por el norte.<br>
  En este cuarto
sigue al norte, ver&aacute;s un poste, usa tu Mayal y sigue al norte. <br>
En este cuarto sube, ve a la izquierda, ver&aacute;s un cofre y obtiene una Semilla Fina (Oblonga Seed's) . Aseg&uacute;rate de cortar con la espada las plantas que alrededor para ganar mas 
	�tems. Ahora ve a la derecha y usa el Mayal en el poste. Sal por la derecha. <br>
Camina y usa tu Mayal para salir por arriba-derecha. <br>
Ahora ve al norte, coge el Cofre, luego camina a la derecha y sal por el sur. <br>
R&eacute;zale a la diosa y ve al Sur para enfrentarte al Jefe. Es recomendado estar en alguna Clase Azul o Nivel 16. <br>
En la cueva entra, sube y coge eso parecido a un espejo (Espejo Lunar) que est&aacute; en el suelo, Hero&iacute;na dir&aacute; que se lo lleves a Isabella y aparecer&aacute; el Jefe.</p>
  <table width="100%"  border="0">
    <tr>
      <td width="29%"><img src="imagenes/jefe%20pantanal.JPG" width="280" height="225"></td>
      <td width="71%"><div align="left" class="Estilo1"><strong>JEFE: Hidra</strong><br>
        HP: 170, Exp: 15<br>
        Lo mejor aqu&iacute; es el Arco, o el Mayal si lo tienes en buen nivel. Salamandra es bueno para darle de lejos. Recomiendo quedarse en la parte superior. La cabeza azul es problem&aacute;tica ya que te envenena y congela. <br>
        <br>
        Al eliminarlo aparecer&aacute; el esp&iacute;ritu de agua casi muerto, as&iacute; que decides llevarlo a un lago cercano. <br>
        As&iacute; que sal de all&iacute;, salva con la diosa, ve al norte. <br>
        En la sala sube, izquierda, luego sal por el sur. Ve a la izquierda, usa el mayal y sal por la izquierda-arriba. <br>
        En la siguiente sala sale por la izquierda otra vez. <br>
        En la siguiente corre a la izquierda y sal por el norte. Aqu&iacute; sigue al norte, luego camina a la izquierda, luego sal por el sur. Ahora s�lo corre al sur y sal de la cueva.</div></td>
    </tr>
  </table>
  <p align="left" class="Estilo1"><strong>Estero Escamado<br>
  </strong><br>
  Sigue al sur, luego sal por la derecha. Corre al sur. Aqu&iacute; baja y ve a la derecha-arriba y sal por all&iacute;. <br>
  A penas entras, el Esp&iacute;ritu de Agua se recobrar&aacute; y dir&aacute; que se llama <strong>Ondina</strong> y se une a ti. Salva con la diosa.<br>
  Ahora devu&eacute;lvete. En la pantalla siguiente ve al oeste. <br>
  En la siguiente ve a la izquierda, y sal por el sur. <br>
  Sigue bajando hasta llegar al puente, cr&uacute;zalo, ve a la derecha y entra a la cueva.<br>
  <br>
  <strong>Camino a Wendel</strong>  <br>
  <br>
   Enemigos: Ficafilado (Needle Beak)   HP: 45, Pezado (Iffish)   HP: 43, Centollo (Pincher Crab)   HP: 48<br>
   <br>
   Sube y camina a la derecha y ver&aacute;s un <strong>Stone Seal</strong> bloqueando el camino, al golpearlo brilla azul. Ahora ponte el Arco y presiona 
	<b>R</b> con Ondina equipada y lanzar&aacute;s una bola de agua al Stone Beal y este se derrumbar&aacute;. Sigue a la derecha y sal por el Sur. <br>
  <br>
  <strong>Costa de Wendel</strong> <br>
  <br>
  Estar&aacute;s en la Costa. Baja a la derecha veras un cofre, espera que haya bajamar para cogerlo, luego sal por el sur. <br>
  Luego sigue bajando y luego a la izquierda. Si sigues a la izquierda ver&aacute;s un ca&ntilde;&oacute;n que no es operable por ahora, as&iacute; que ve a la derecha y sube. <br>
  A veces te puedes pillar con <b>Niccolo</b> por aqu&iacute;, y vende a parte de 
	�tems normales, 2 que son muy importantes pero los vende a 10.000 Lucres cada uno, uno es <strong>Cascabelle</strong> que es para ganar mas puntos de EXP en las batallas y otro es <strong>Iris Lucrativa</strong> que es para ganar mas Lucres en las batallas (presiona 
	<b>SELECT</b> en el men&uacute; de objetos que te ofrece y ver&aacute;s la descripci&oacute;n). Recomiendo definitivamente el Cascabelle ya que da 100% m&aacute;s de experiencia, es decir si un enemigo que me daba 5 de EXP, ahora  me da 10 ;) Si sabes como hacer Cheats en el emulador 
	<b>VBA</b> podr&aacute;s tener mucho dinero y comprarlo, si no, tendr&aacute;s que aguantarte y pelear mucho para comprarlo.<br>
  Sube al norte y encontrar&aacute;s la ciudad de Wendel.<br>
  </p>
  <table width="100%"  border="0">
    <tr>
      <td width="29%"><img src="imagenes/wendel.JPG" width="280" height="225"></td>
      <td width="71%"><div align="left" class="Estilo1"><strong>Ciudad de Wendel</strong> <br>
        Aqu&iacute; ella va a ver a <b>Boggard</b> y t&uacute; al maestro <b>Cibba</b>. Durante el d&iacute;a hay un chico caminando a la derecha-abajo del pueblo, 
		�l te da  <strong>Nota de Popoi #4.</strong> Arriba de &eacute;l est� la tienda donde puedes comprar 
		�tems y accesorios y aceptar el <a href="#18">Quest</a> de entregar encargos. Tambi&eacute;n hay otro  <a href="#18">Quest</a> aqu&iacute; de conseguir una m&aacute;scara negra.  Un hombre caminando mas al centro te da <strong>Nota de Popoi #3.</strong> En el noroeste hay una casa, en el segundo piso hay una Semilla. Habla con el tipo Holgaz&aacute;n que hay all&iacute; para continuar con el Quest ese del Trit&oacute;n Asado, el Quest se llama &quot;&iquest;Donde esta mi padre?&quot;. Luego sal de la casa y ve a la parte norte. <br>
        <br>
        Al norte est� <span class="Estilo5">Lester,</span> un trovador que toca la canci&oacute;n que le tocaba Lord Granz a H&eacute;roe. Dice que va al 
		<b>Castillo Granz</b>. Hay un hombre all&iacute; muy p&aacute;lido llamado <strong>Devius</strong> que se va. Luego habla con un hombre que esta echado en el piso, en una casa a la izquierda del trovador, se llama Veerapol y te da <strong>Nota de Popoi #19 y #20.</strong> Justo arriba est&aacute; 
		la <b>Herrer&iacute;a</b>  de Vendel (Wendel Blacksmith) donde puedes templar las armas para hacerlas poderosas pero debes tener <strong>verduras</strong> del &aacute;rbol Cacto.  Si juegas como Hero&iacute;na conocer&aacute;s a <strong>Watts</strong> que termina yendo a la casa Cacto para que puedas forjar tus armas all&iacute; y recibes la 
		<b>Manopla</b> (Sickle). Si juegas como H&eacute;roe conocer&aacute;s a 
		<b>Watt</b> mas tarde. Habla con el enano <b>Navali</b> para seguir con el Quest &quot;&iquest;Donde esta mi padre?&quot;. Sal de all&iacute; y ve al noreste, all&iacute; esta el Inn, adentro hay un chico que perdi&oacute; su cuchillo, ver <a href="#18">Quest</a>. Hay otro tipo adentro llamado Vegas y que quiere un Espada Centelleante, ver <a href="#19">Quest</a>. Tambi&eacute;n adentro del Inn segundo piso, puerta izquierda, est&aacute; Devius habla con &eacute;l, luego en el cuarto de la derecha hay un enano,  habla con &eacute;l. Sal del Inn, y ve al Norte, a la Catedral. </div></td>
    </tr>
  </table>
  <p align="left" class="Estilo1">En la Catedral estar&aacute;n el Bar&oacute;n Bogard y un extra&ntilde;o anciano que resulta ser <strong>Cibba</strong>. Como no lo toman muy en serio eso de matar al Se&ntilde;or Oscuro, H&eacute;roe se enoja y sale, justo en ese momento llega un  Zepelin del Imperio de Granz con el <strong>Se&ntilde;or Oscuro</strong> y su subordinado <strong>Julius</strong>. Llegan frente a la Catedral y le hablan a H&eacute;roe y Julius lo manda a volar con una magia, pero gracias al 
	<b>Espejo Lunar</b> que H&eacute;roe llevaba encima se salva de morir. <br>
	El Sr. Oscuro dice que Hero&iacute;na se entregue o destruir&aacute;  todo el pueblo, as&iacute; que Hero&iacute;na se entrega por voluntad propia.<br>
    H&eacute;roe despierta en una pieza con Lester y Bogard que lo cuidaban. Sal del Inn, habla con Devius y Lester, luego Devius te pide que liberes a sus p&aacute;jaros en su cuarto, as&iacute; que vuelve al Inn, ve a su cuarto y para cada una de las 3 jaulas presiona A para 
	liberarlos. Ve a la catedral, habla con Cibba y Bogard y dir&aacute; que el imperio Vandole que destruyeron juntos hace mucho tiempo est&aacute; atr&aacute;s de todo esto. Ellos dicen que el Zepelin aterrizar&aacute; en el <strong>Lago oscuro</strong> pero primero se debe cruzar la <strong>Cueva Gaia</strong>, el problema es que cueva Gaia esta viva, y quiz&aacute;s note deja pasar, as&iacute; que te da el <strong>Esp&iacute;ritu L&uacute;mina</strong> para que te ayude. Luego cada uno sigue su camino. 
	<br>
	Mas tarde se muestran otros eventos donde sale el se&ntilde;or oscuro con Isabella peleando, pero ninguno vence, ella es una Mavole (de Mavolia) y anda en busca de un hombre fuerte. 
	<br>
	Ella se va por las buenas con el Se&ntilde;or Oscuro y le gusta la idea de dominar todo el mundo.    <br>
    Baja al pueblo y sal por el Oeste.<br>
    <br>
    <strong>Bosque Champibom<br>
    </strong><br>
 Enemigos: Mushboom   HP: 45<br>
	<br>
Entra a la casa Cacto, luego adentro habla con el cactus, sube y dale todas las semillas que puedas al &aacute;rbol Trent y recoge las verduras que tiene si es que tiene. Ahora sal de la casa y el cactus escribir&aacute; algo, ahora entra y sale varias veces hasta que &eacute;l no escriba nada. Sal por el Oeste. <br>
Aqu&iacute; es la oportunidad de subir niveles si tienes un Cascabelle equipado (200% de Experiencia te dan los enemigos). 
Ver&aacute;s un cartel que dice que al norte est&aacute; la Fachada Gaia, pero est� cerrada por ahora, as&iacute; que ve primero a la <strong>Cueva de los Enanos</strong> (Dwarf Cave) yendo al Oeste. <br>
Lo primero que debes hacer es primero recoger los cofres que hay en esta  &aacute;rea, sube y ve un poco a la izquierda, y ver�s uno. 
Y un poco mas a la izquierda, ver&aacute;s otro con una Semilla Fina. Ahora regresa a la derecha y sal por el norte. en esta pantalla coge los 3 cofres y luego baja por el suroeste. Regresar&aacute;s a la pantalla con el cartel, ahora sigue a la izquierda y usa tu Mayal sobre la roca izquierda y sal por el oeste. <br>
En la siguiente pantalla solo corre al Oeste para llegar a la Cueva de Los Enanos. <br>
<br>
<strong>Cueva de los Enanos</strong><br>
<br>
Adentro te dir&aacute;n que su amigo <strong>Watt</strong> quiso hacer su propia espada Mana (un espadach&iacute;n elegante vino a la cueva y se lo  pidi&oacute;) y fue en busca de un material llamado 
	<b>Mitrilo</b> en la <strong>Mina Abandonada</strong>. <br>
Habla con el enano rojo, llamado Enano Peludo y le pides que te digan alguna manera de pasar por la Cueva Gaia, te dicen que con Mitrilo podr&aacute;s pasar y Mitrilo hay en la Mina Abandonada que queda al norte y que de pasada busques a su amigo Watts. <br>
Sal de la cueva y ve al este.
<br><br><strong>Bosque Champibom</strong><br> 
<br>
Corre al este y sal ahora por el norte. <br>
Sube un poco y entra por el oeste. <br>
Sigue al oeste. <br>
Ve a la izquierda, saca el cofre oculto entre los &aacute;rboles en la zona centro y sal por la izquierda-arriba. <br>
Arriba usa tu Mayal, cruza y sube. <br>
<br>
<strong>Camino a la Mina<br>
</strong><br>
Enemigos: Locud   HP: 30 solo d&iacute;a, Buhocida (Blood Owl)  HP: 48 solo noche, Toposo (Molebear)   HP: 54<br>
<br>
Sube, coge el cofre y sal por el este. <br>
Salva con la Diosa y coge los cofres. Ahora devu&eacute;lvete a la derecha, sigue a la derecha y luego al norte y ver&aacute;s la entrada a la Mina derrumbada y entre ellas estar&aacute;n 3 Dubbear y el enano Watts enojado ya que se perdi&oacute;. <br>
Watts est&aacute; all&iacute; ya que busca Mitrilo para hacer una Ultra Espada. Al final Watts se une a ti. 

<br><br><strong>Mina Abandonada (Abandoned Mine)</strong><br>
<br>
Enemigos: Insectosaurios (Insectaur)   HP: 82,  Molebear   HP: 54<br>

<br>
Sal por abajo-izquierda y sal por el este. <br>
Salva con la diosa, luego ve a la derecha otra vez. <br>
Salta sobre el carro y te llevar&aacute; a otra pantalla, golpea el switch desde el carro y seguir&aacute;s avanzando, golpea el otro switch y te pasar&aacute; a otra pantalla. Finalmente cuando el carro se detenga salta y sal por el Sur.<br>
Baja y coge el cofre con Caramelo. Luego sal por la derecha.<br> 
En esta pantalla ten cuidado ya que hay 5 Locud (Cada uno EXP 6). Si quieres aqu&iacute; te quedas un rato ya que es muy f&aacute;cil aqu&iacute; subir de nivel con tu Mayal y si tienes el accesorio Cascabelle ya que ganaras 5 * 6 * 2 = 60 EXP de una pasada, 
	y si te esperas un rato y saldr&aacute;n los 5 enemigos de nuevo. Cuando te aburras, sal por el este.<br>
Aqu&iacute; ver&aacute;s 5 Insectosaurios, al eliminarlos recibir&aacute;s una nueva arma, un <strong>Falce</strong> (Sickle). Sal por el norte. (En esta etapa  gane m&aacute;s experiencia, llegu&eacute; al Nivel 36 y obtuve la <strong>Clase Drag&oacute;n</strong>).<br> 
	Elimina a los enemigos y sal por
el noroeste. <br>
	Elimina a los enemigos y sal por el oeste. <br>
Ve al suroeste, salta al carro, luego p&eacute;gale al switch y pasar&aacute;s a la pantalla de la izquierda. <br>
Golpea los 2 switch y pasar&aacute;s a otra sala. Golpea el switch 1 vez y pasa a otra sala. <br>
Ahora es importante que golpees el primer switch que veas o sino volver&aacute;s a la sala anterior. Pasar&aacute;s a otra sala.<br>
Finalmente el carro se detendr&aacute;. Mata a los bichos que hay all&iacute;, luego coge los 2 cofres y sal por el norte. <br>
Salva con la Diosa y te ver&aacute;s con el jefe del &aacute;rea. Watts se tirar&aacute; de una al Mitrilo que hay all&iacute; y se escuchar&aacute; una voz que dice &quot;Ankheg, exp&uacute;lsalos&quot;. </p>
  <table width="100%"  border="0">
    <tr>
      <td width="29%"><img src="imagenes/Ankheg.JPG" width="280" height="225"></td>
      <td width="71%"><div align="left">
        <p class="Estilo1"><strong>JEFE: Ankheg</strong> <strong>el Sirviente </strong><br>
          HP: 300, Exp: 35 <br>
          Lo mejor aqu&iacute; es usar tu nueva arma  <b>Falce</b>. Pero hay un truco. Si estas en un buen nivel (yo estaba en el 36) y est&aacute;s verde parpadeando, espera que venga hacia ti y dale de lleno en la cabeza &iexcl;y lo matar&aacute;s de un solo golpe!<br>
          Si no, tendr&aacute;s que usar tu Falce y darle en la cabeza cuando puedas hasta eliminarlo. <br>
          <br>
          Luego que lo mates, aparecer&aacute; un <strong>Dudbear</strong> (esos tipos con los que estaba Watts) y Watts se mete por una cueva, luego tu sigues a Watts. <br>
Aparecer&aacute;s en la <strong>Cueva de los Enanos</strong>. A H�roe no le gusta mucho que los enanos construyan armas para reino que persigue y mata a los herejes. Luego Watts te dice que har� para ti la 
		<b>Ultra Espada</b> pero necesita un lugar para trabajar, la Casa Cacto. As&iacute; que desde ahora podr�s ir a visitarlo y mejorar tus armas. Sal de la casa y luego sal por el este. </p>
        </div></td>
    </tr>
  </table>
  <p align="left" class="Estilo1"><strong>Bosque Champibom (Mushboom Forest)<br>
	</strong><br> 
Ahora puedes ir a la <b>Casa Cacto</b> o a <b>Gaia</b>. Vamos a Gaia para seguir con el juego. Ve a la derecha y sal por el norte.<br>
Sube y sal por el oeste.<br>
Corre a la izquierda y sal por el este.<br>
Sube y sal por el norte.<br> Abre el cofre que te da una semilla y sal por el sur.<br>
Ve a la izquierda y sal por el norte.<br>Sube un poco y usa tu Mayal, sal por el norte.


<br>
  </p>
  <table width="100%"  border="0">
    <tr>
      <td width="29%" class="Estilo1"><strong><img src="imagenes/gaia.JPG" width="280" height="225"></strong></td>
      <td width="71%"><div align="left">
          <p class="Estilo1"><strong>Camino a la mina (Path to Mine)</strong><br>
            <br>
            Ve a la izquierda  y sal por el oeste. <strong><br>
<br>
<strong>Camino a Gaia (Road to Gaia)</strong><br>
</strong><br>
R&eacute;zale a la Diosa y sal por el Oeste.<br>
Ve a la izquierda y sal por el norte. <strong><br>
<br>
Fachada Gaia (Gaia's Facade)</strong><br>
<br>
Avanza y presiona A frente a la Roca, habla con ella y como tienes Mitrillo, te dejar&aacute; pasar. </p>
      </div></td>
    </tr>
  </table>
  <p align="left" class="Estilo1">  <strong>Cueva Gaia (Gaia Cave)
  </strong><br>
  <br>
  Enemigos: Veo veo (Eye Spy) HP: 48 EXP 8, Earth Cyclops<br>
  
  <br>
  Elimina a los Molebears y sal por el Norte.<br>
  Corre y r�pidamente mata al Veo Veo. Arriba usa Salamandra con la espada o arco para matar a esa especia de volc&aacute;n que se pone rojo, estos son los <strong>Stone Seal</strong> ideales para subir el nivel de alguna arma ya que aunque te diga  0, igual te da experiencia. <br>
  Adentro hay 2 cofres, luego regresa. Sal por el oeste.<br>
  Sube, cuidado con las rocas ya que te empujan , sal por el este. <br>
  Mata la ojo, sube usa Salamandra sobre el volc&aacute;n, coge el cofre, baja y regresa por donde viniste.<br>
  Ve a la izquierda y sigue al oeste. <br>
  Mata a los 3 Insectosaurios y sigue a la izquierda, luego baja y a la derecha hay un Veo veo, elim&iacute;nalo y coge el cofre con metal. Luego regresa a la izquierda, sube y sal por el oeste. <br>
  Avanza esquivando las rocas y arriba estar&aacute; <span class="Estilo5">Duende (Gnome)</span> el Esp&iacute;ritu de Tierra y te ayudar&aacute;. 
  Equ&iacute;palo y sal por el oeste. <br>
  Con
  Duende equipado al presionar levemente R te sube la Defensa temporalmente, Salamandra te sube el ataque.<br>
  Coge lo cofre y con Duende equipado golpea al volc&aacute;n de color caf&eacute; de abajo, luego coge le cofre (te da una Semilla Peque&ntilde;a) y sal de la cueva por el suroeste. <br>
  <br>
  <strong>Alrededores del Lago (Lake Vicinity)<br>
  </strong><br>
  Salva en la estatua de la diosa y sal por el oeste. <br>
  All&iacute; encuentras a Cibba, te dice que el Zepelin aterrizar&aacute; cerca del <strong>Lago Ocaso</strong> y se une a ti (le� 
	por all� que esto no sucede en la versi&oacute;n original del juego de GameBoy). <br>
  Cibba tiene buen status y magia, as&iacute; que ve a  la opci&oacute;n de Comportamiento en el Men&uacute;, y d&eacute;jalo  mas tirado para la Magia y para Acercase (ofensiva). Sal por el oeste.<br>
  <br>
  <strong>Camino al Lago<br>
  </strong><br>
  Baja y coge un cofre que hay. Sal por el oeste. <br>
  Sal por el oeste.<br>
  Elimina a los 2 hombres lobos y al soldado y te dar&aacute;n una nueva arma: la <strong>Manopla</strong> (Knuckle) especial si te gusta golpear con pu&ntilde;os y patadas a tus enemigos. Entra a la nave.<br>
  </p>
  <table width="100%"  border="0">
    <tr>
      <td width="29%"><img src="imagenes/nave.JPG" width="280" height="225"></td>
      <td width="71%"><div align="left">
          <p class="Estilo1"><strong>Nave (Airship)<br>
          </strong><br>
          Enemigos: Cursed Doll HP: 52, Imp HP: 45, Granz Archer HP: 50<br>
          <br>
          Salva con la diosa y entra por la derecha-arriba. 
Ve a la izquierda
y sube, entra por la puerta que esta indicada con una flecha en el piso, la de mas a la derecha, luego coge los cofres de adentro y sal. <br>
Ahora regresa y ve a la izquierda (en la puerta de la izquierda no hay nada), baja por el pasillo, elimina al Veo veo y sigue por el 
			sur y entra por la puerta que  donde indica la flecha.<br> 
Baja, sube las escaleras, coge los cofres y rompe los vidrios del sur con tu Manopla. Sal por el sur.<br>
Afuera, camina a la derecha y ver&aacute;s
una escena donde <b>Julius</b> le pide una  llave a <b>Hero&iacute;na</b> y ella se niega y la golpea. Luego 
			<b>Cibba</b> le tira un magia a Julius pero Julius le tira otra  magia que lo deja en el piso. Luego ella le tira a H&eacute;roe la <strong>Llave del Santuario</strong>, un objeto de la madre de Hero&iacute;na. <br>
Despu&eacute;s Julius le tira una magia a H&eacute;roe  y lo manda a volar fuera de la nave. <br>
Caes en el piso y una mujer te rescata. <br>
Estas en el pueblo <strong>Menos.</strong> <br>
<br>
          </p>
      </div></td>
    </tr>
  </table>

  <p align="left" class="Estilo1"><strong>Pueblo Menos (Menos Village)<br>

    </strong><br>
    <strong>Amanda</strong>, la mujer, te dice que est&aacute;s en el pueblo de Menos. Ella estaba al principio del juego, como esclava al igual que tu y escap&oacute; aquella vez. <br>
    Willy, tu amigo, dice que se lo llevaron los soldados esa vez. <br>
    Te quedas dormido y luego llega Devius (el de los p&aacute;jaros de Wendel), dice que ahora vive en <strong>Jadd</strong>. Luego aparece Hero&iacute;na y Bar&oacute;n y dicen que Amanda sali&oacute; corriendo, al parecer ella  te rob&oacute; el pendiente que te dio Hero&iacute;na. 
	Debemos encontrar a Amanda.<br>
    Sal de la casa y habla con la gente de alrededor. Ganar&aacute;s varias <strong>Nota de Popoi </strong>como la<strong> #6,#7, #8 y #12.</strong><br>
    En la casa de suroeste hay una chica llamada <strong>Pancera</strong> que te 
	pide aerolitos. <br>
    A la derecha hay una casa donde un tipo te habla de un hombre que le gustaba el <strong>Triton Asado</strong> y que paso por Menos, pero ahora est&aacute; en Jadd. <br>
    Amanda se fue tambi&eacute;n a <strong>Jadd</strong> as&iacute; que vuelve a la casa donde partiste y les dir&aacute;s al Bar&oacute;n y a Hero&iacute;na que Amanda se fue a Jadd, as&iacute; que ellos se adelantar&aacute;n e ir&aacute;n all&iacute;.<br>
    Ahora puedes salir de Menos, puedes ir al sur (Jadd) o ir al norte, a Menos Outskirts, claro que esta &uacute;ltima parte no es necesario para la historia del H&eacute;roe, pero si par subir niveles y ganar items. <br>
    Sal del pueblo por el sur.</p>
  <table width="100%"  border="0">
    <tr>
      <td width="29%"><img src="imagenes/desierto_espinoso.JPG" width="280" height="225"></td>
      <td width="71%"><div align="left">
          <p class="Estilo1"><strong>Desierto Espinoso (Prickly Desert)<br>
          </strong><br>
          Enemigos: Diablillo (Imp) HP: 45 solo noche, Conejasesino (Rabillion) HP: 73<br>
          <br>
          Sal por el sur. Construye la Casa Cacto y habla con Cactolillo. Ve al segundo piso y dale semillas al &aacute;rbol y recoge la cosecha anterior. Baja y ve a la izquierda ya que estar&aacute; Watts y te templar&aacute; alg&uacute;n arma o armadura si tienes materiales.<br>
Por ejemplo el arma Espada Ferrea lo us&eacute;  con material Hierro Forsena y me sub&iacute;a el Poder de 2 a 8, Fuerza de 2 a 8 y Certeza de 2 a 8. Manopla Jaspe con material M&aacute;rmol me daba Certeza de 2 a 10, Elemento Poder de 0 a 5, l&iacute;mite de 10 a 15. Prueba otras armas y cuando te aburras, sal de la Casa Cacto y ver&aacute;s a Cactolillo escribir algo, vuelve a armar la Casa Cacto hasta que deje de escribir.<br>
Sal por el Oeste ya que en el suroeste hay un ca&ntilde;&oacute;n que no esta funcionando aun.<br>
Sal por el sur, coge el cofre y regresa.<br>
Ve al norte, luego izquierda, luego baja y sal por el sur. <br>
Salta, coge el cofre, ve a la derecha y sube.<br>
Coge el cofre que tiene un Gofre (Chocolump, da 80 puntos de vida) y regresa.<br>
Salta abajo y ve a la derecha, sal por el Sur. Salva con la diosa. <br>
Baja y entra por las puertas a Jadd. <br>
          </p>
      </div></td>
    </tr>
  </table>
  <p align="left" class="Estilo1"><strong>Ciudad de Jadd</strong><strong> (Town of Jadd)<br>
  </strong><br>
  Aqu&iacute;  ve a la casa de la izquierda, y habla con Radley para seguir con el Quest "donde est� mi Padre?" (ese del Trit�n Asado). Luego puedes templar tus armas con el enano de all&iacute;. <br>
  Sal y ve a otras casas, revisa el pueblo y habla con la gente y sabr�s que Amanda fue a 
	<b>Villa Devius</b> (Devius Manor). Sal del pueblo por el oeste.


  <p align="left" class="Estilo1"><span class="Estilo1 Estilo5">Villa Devius</span><br>
     <span class="Estilo1"><br>
     Devius es una gran mansi�n con muchos tesoros. Habla con las 3 sirvientas de Devius: 
	<b>Bridget</b>, <b>Priscilla</b>, y <b>Genoa</b>. <br>
     Luego ve por la derecha, sube, en la siguiente sala baja de inmediato por una peque�a pasada que hay all�, luego abajo coge el cofre que te da un Semilla Torcida. <br>
     Luego sube a la sala anterior. Luego ve a la izquierda y baja, coge el cofre que te da Colmillo Marfil. Sube y entra por la Puerta del Norte. <br>
     Sube la escalera y habla con las 3 sirvientas. Abajo a la izquierda hay un cofre con 
	el <b>Icono Espiritual</b> <b>Astro </b>(Luna Icon). Los �conos infligen 
	da�o de magia a los enemigos, les cambian el estado: los congelan, los dejan 
	ciegos, los achican, etc. Devu�lvete por el sur. <br>
   Baja las escaleras y ve por el norte.
   <br>
   Ve a la derecha y habla con las damas, luego sube y te dejar�n pasar. </span>
  <p align="left" class="Estilo1"><strong>Cuarto de Medusa</strong><br>
    <br>
    Te encontrar�s con <strong>Amanda</strong>, te dice que te sac&oacute; el colgante para salvarte de la vida ya que <strong>Devius</strong> no te matar&iacute;a si ella se lo llevaba a &eacute;l.
A lado de ella est&aacute; <strong>Medusa</strong>, supuesta amiga de Amanda, muy callada. Te das cuenta que 
	<b>Lester</b> fue convertido en p&aacute;jaro al igual que los otros probadores. <br>
Tratas de sacarlo pero Medusa tira un rayo. Luego llegan sirvientas y Medusa se va al &quot;<strong>Altar del Tiempo</strong>&quot;. <br>
Revisa a una de las sirvientas ca&iacute;das y s&aacute;cale la llave de la jaula, libera al p&aacute;jaro y s&iacute;guelo.<br> 
Habla con <b>Genoa</b>, luego baja por la izquierda-abajo y pasa al siguiente cuarto, sube la escalera, habla con Precio, te hablar&aacute; del <strong>Desierto de Jadd.</strong> <br>
Sal de Villa Devius. <br>
<br>
<strong>Ciudad de Jadd</strong><strong> (Town of Jadd)</strong><br>
<br>
Prep&aacute;rate y ve por el Sur.</p>
  <table width="100%"  border="0">
    <tr>
      <td width="29%"><img src="imagenes/desierto%20jadd.JPG" width="280" height="225"></td>
      <td width="71%"><div align="left">
          <p class="Estilo1"><strong>Desierto de Jadd (Jadd Desert)</strong><br>
            <br>
            Enemigos: Cobra HP: 71, Serpiegallo (Cockatrice) HP: 95, Escorpena (Sand Scorpio) HP:
166 debes usar Manoplas, Cajaquemuerde (Polter Box) HP: 88, Esquelafrio (Skeleton) HP: 72 solo noche, muere solo con Magia, Trasgo (Goblin) HP: 100, Shadow Zero HP: 65, Silkspitter HP: 175 solo d&iacute;a<br>
<br>
Ve a la derecha, abajo y sal por el Este.<br>
Avanza y sal por el este.<br>
Sal por el norte.<br>
Aqu&iacute; el aire estar&aacute; envenenado y te forzar&aacute;n a regresar, as&iacute; que baja por el sur.<br>
Corre a la izquierda y sal por el sur.<br>
Ve al sureste y encontrar&aacute;s 2 palmeras, corre haciendo un 8 entre ellas (&iquest;recuerdas el Donkey Kong 3 SNES que hab&iacute;a que hacer lo mismo?) como se ve la imagen y entrar&aacute;s a <strong>D&eacute;dalo de Dunas</strong>. <br>
          </p>
      </div></td>
    </tr>
  </table>
  <p align="left"><span class="Estilo7">D&eacute;dalo de Dunas (Dune Maze) <br>
    </span><span class="Estilo1"><br>
    Aqu&iacute; elimina a los Esquelafrio con la magia del esp&iacute;ritu de Duende y con el arma manoplas de hierro para tirarle  bolas de magia. <br>
    Avanza, cruza con el Mayal, coge el cofre con Seda Sultana, y sal por el Sur. <br>
    <br>
    <strong>Desierto de Jadd</strong><br>
    <br>
    Sal abajo y sal por el Sur. <br>
    Ve al suroeste, aseg&uacute;rate que sea de D&Iacute;A y SALTA dentro del remolino que hay.<br> 
    <br>
    <span class="Estilo7">D&eacute;dalo de Dunas</span><br>
    <br>
    Si entras de noche, sal de la cueva ya que hay muchos enemigos que solo mueren con magia y son dif&iacute;ciles de matar. <br>
    Ve a la derecha, equ&iacute;pate con tu Manopla para matar a los enemigos aqu&iacute;. <br>
    Sube presiona el bot&oacute;n del piso, sube y presiona el otro bot&oacute;n, sal por la derecha.<br>
    <br>
    <strong>Desierto de Jadd</strong><br>
    <br>
    Salta abajo y sal por el sur.<br>
    Ve a la izquierda y no entres por el remolino de
    arena, sino que sigue a la izquierda y entra por la cueva. Recuerda entrar de D&iacute;a y no de noche.<br>
    <br>
    <span class="Estilo7">D&eacute;dalo de Dunas</span>    <br>
    <br>
    Arriba usa con tu Manopla  la magia Duende, con R para tirarle un rayo al Stone Seal  que bloquea el paso. <br>
    Ve a la derecha al fono y baja, presiona el bot&oacute;n, luego ve a la izquierda y sal por el sur. <br>
    <br>
    <strong>Desierto de Jadd</strong><br>
    <br>
    Coge el cofre de la izquierda, y de la derecha y luego entra a la cueva de la derecha. <br>
    <br>
    <span class="Estilo7">D&eacute;dalo de Dunas</span> <br>
    <br>
    Ve arriba, luego derecha y entra a la cueva. <br>
Aqu&iacute; elimina a los 5 Goblins que son algo molestos (si estas en alguna clase alta ser&aacute;n &quot;pan comido&quot;) y recibir&aacute;s una nueva arma <strong>Hacha (Axe)</strong>. <br>
Esta arma es muy poderosa pero algo lenta, recomiendo equip&aacute;rtela. Sal por el norte.<br>
Presiona el bot&oacute;n y regresa por donde viniste.<br>
Ve al sur y salta por los costados, y sal por el sur.<br> 
Ve a la izquierda, baja y presiona al bot&oacute;n, ve a la derecha, sube, derecha y sal por el sur.<br> 
</span></p>
  <table width="100%"  border="0">
    <tr>
      <td width="29%"><img src="imagenes/duna.JPG" width="280" height="225"></td>
      <td width="71%"><div align="left">
          <p class="Estilo1"><strong>Desierto de Jadd</strong><br>
            <br>
            Ve a la derecha, sube, izquierda, y sal por el Norte.<br>
Ve a la derecha, coge el cofre (Cuerno Celeste) y luego entra a la cueva.<br>
<br>
<span class="Estilo7">D&eacute;dalo de Dunas</span><br>
<br>
Hay muchos Cockatrice aqu&iacute; (parecen pollos), usa tu Mayal para eliminarlos. Al hacerlo, ver&aacute;s que hay una roca redonda arriba, en verdad es
el <strong> Esp�ritu C&eacute;firo </strong>(en el juego Ingl&eacute;s le pusieron Jinn) el Esp&iacute;ritu del Viento. Entra por la cueva.<br>
Presiona el bot&oacute;n y regresa. <br>
Ve a la derecha regresando por donde entraste. <br>
<br>

<strong>Desierto de Jadd</strong><br>
<br>
Ve a la derecha arriba y ver&aacute;s un Stone Seal de color azul, usa C&eacute;firo con Hacha equipado, sobre el para derrumbarlo. Entra por la cueva. <br>
          </p>
      </div></td>
    </tr>
  </table>
  <p align="left"><span class="Estilo1">
    <span class="Estilo7">D&eacute;dalo de Dunas</span> <br>
    <br>
    Arriba salva con la diosa, ve a la derecha y sal por all&iacute;. <br>
  <br>
  <strong>Desierto de Jadd</strong><br>
  <br>
  Sal por el este.<br>
  Avanza a la derecha y sal por el sur.<br>
  Sal por el sur.<br>
  Ve a la derecha, coge los 2 cofres (Semilla peque&ntilde;a y Semilla fina), entra a la cueva. <br>
    <br>
    <span class="Estilo7">D&eacute;dalo de Dunas</span> <br> 
    <br>
    Entra de d&iacute;a. Ve a la 
derecha, usa el Mayal para cruzar arriba, presiona el bot&oacute;n y regresa por donde viniste. <br>
<br>
<strong>Desierto de Jadd</strong><br> 
<br>
Ve a la derecha y sal por el norte. <br>
Ya estuviste aqu&iacute;, hay muchos cofres, algunos son falsos. Arriba a la derecha, ahora se abri&oacute; una nueva cueva, entra por all&iacute;. <br>
<br>
<span class="Estilo7">Altar del Tiempo </span> <br>
<br>
Salva con la diosa, coge los cofres y ve por el norte. Puedes volver aqu&iacute; con el &iacute;tem<span class="Estilo5"> Tiny Tapper </span>(te hace peque&ntilde;o).<br>
Prep&aacute;rate para una pelea con Medusa. </span></p>
  <table width="100%"  border="0">
    <tr>
      <td width="29%"><img src="imagenes/medusa.JPG" width="280" height="225"></td>
      <td width="71%"><div align="left">
          <p class="Estilo1"><strong>JEFE: Medusa</strong><strong></strong><br>
          HP: 355, Exp: 60 <br>
          <br>
			<b>Amanda</b> le pide a <b>Medusa</b> (que  es esposa de <b>Lord Granz</b>) que le regresa a su hermano 
			<b>Lester</b> a la normalidad ya que est&aacute; convertido en p&aacute;jaro. Ella se enoja, se transforma y empieza la pelea.<br>
          <br>
          Ella se posa en unos pilares y te tira unos poderosos rayos que te dejan paralizado por un tiempo y te quitan mucha energ&iacute;a. Recomiendo usar tu Manopla, par&aacute;ndote frente a ella, mir&aacute;ndola hacia arriba y d&aacute;ndole muchos golpes. Cuando puedas, t&iacute;rale magia Esp&iacute;ritu Viento C&eacute;firo. <br>
          <br>
          Al eliminarla, aparecer&aacute;n <b>Bogard</b> y <b>Hero&iacute;na</b>. 
			Esta �ltima le tira una magia y ella se recupera, te da el <b>La&uacute;d</b> de Lord Granz, dice que esta vivo en alg&uacute;n lugar.<br>
Llega <b>El Se&ntilde;or Oscuro</b> y <b>Devius</b> que se impresiona ya que su madre
			<b>Medusa</b> est&aacute; muerta. <br>
Amanda dir&aacute; que ahora no podr&aacute;n regresar a la forma humana a su hermano Lester. <br>
Devius le dice al Se&ntilde;or Oscuro que tome a su madre (&iquest;de ellos? &iquest;son hermanos?), 
			y la lleve del <b>Santuario</b>. <br>
			Le llama <strong>Stroud</strong>, el nombre real del se&ntilde;or oscuro y esta molesto con &eacute;l. 
			<br>
			El Se&ntilde;or Oscuro mando a Devius a cuidar de su madre y Devius fall&oacute;, ahora el se&ntilde;or Oscuro se va y Devius se queda para vengarse. Luego les dice que los espera en su Villa y se va.<br>
          </p>
      </div></td>
    </tr>
  </table>
  <p align="left"><span class="Estilo1">Amanda dice que la mordi� Medusa y 
	empieza a enloquecer, H�roe le dice que vuelva en si ya que ellos estar�an 
	por siempre juntos... (�y lo dice enfrente de Hero&iacute;na!) <br>
Le pide a H&eacute;roe que acabe con su vida, luego H&eacute;roe le hace caso y la corta, Hero&iacute;na con magia la salva y con la sangre en el piso, sangre de Medusa, el p&aacute;jaro (Lester) la bebe y se recupera, Lester vuelve a la vida.<br>
Pero de repente Amanda se desploma y le dice a H�roe que no podr&aacute; cumplir su promesa y que cuides a Hero&iacute;na ... luego muere...<br>
Con mucha pena, Lester te d� lo que queda de la sangre de su hermana y de Medusa, tiene poderes m&aacute;gicos. <br>
  </span><span class="Estilo1">H&eacute;roe tiene que volver con Bogard y promete vengarse de Devius que mat&oacute; a Amanda. <br>
    <br>
    <strong>Villa Devius<br>
	<br>
	</strong>Enemigos:
Espectro (Specter) HP: 76 EXP 14 solo noche, Hombrelobo (Werewolf) HP: 102 EXP 14, Polter Box HP: 88, Sombra Cero (Shadow Zero) HP: 65 EXP 13 dale con el Mayal, Tin Knight HP: 130 d&iacute;a <br>
<br>
    Ve al noreste.<br>
  Sube por la puerta principal. <br>
	En el cuarto de Medusa ve a la derecha y habla con Genoa y te dice que el Conde Devius te espera. Entra por la puerta a la Mazmorra.</span></p>
  <table width="100%"  border="0">
    <tr>
      <td width="29%"><img src="imagenes/villa%20devius.JPG" width="280" height="225"></td>
      <td width="71%"><div align="left">
          <p class="Estilo1">A los enemigos con forma de payaso, elim&iacute;nalos con L&uacute;mina con Hacha Equipado. A veces quedar&aacute;n con una coraz&oacute;n (enamorados) y no te atacar&aacute;n. Apenas subas un poco, salta a la rueda redonda a la izquierda, y sal por el sur.<br>
Ve a la derecha y golpea la campana 3 veces. Sal por el sur. <br>
Coge el cofre y regresa.<br>
Sube las escaleras. <br>
Ve a la derecha, cruza por la rueda y sal por el norte. <br>
Avanza hasta que tengas que bajar las escaleras.<br>
Solo baja un poco y baja las escaleras.<br>
Sal por el sur.<br>
Coge el cofre y regresa por arriba.<br>
Ve a la izquierda por la rueda y sube por las escaleras.<br>
Sube por el norte.<br>
			Ve al sur, usa la magia de C�firo con el Hacha equipado sobre el Stone Seal, 
			el volc&aacute;n met&aacute;lico azul que hay all&iacute;. Luego sigue al sur y sube las escaleras.<br>
Otra vez usa la magia de C&eacute;firo sobre el volc&aacute;n y sube. Coge el cofre, ve a la derecha, baja, izquierda, y sube las escaleras. <br>
          </p>
      </div></td>
    </tr>
  </table>
  <p align="left"><span class="Estilo1">Ve a la derecha por las maderas, sube la escalera.<br>
    Avanza por la madera, salta a la rueda y abajo ver&aacute;s  2 campanas, golpea la de la izquierda 2 veces y la derecha 1 vez. Luego sube y baja las escaleras. <br>
    Ve por la madera y baja las escaleras.<br>
    Ve a la izquierda, baja, lanza magia C&eacute;firo al Stone Seal y ve a la derecha, sube las escaleras. <br>
    Baja, salta a la izquierda, sube las escaleras.<br>
    Corre a la derecha, salta, sube y entra por la puerta. Si est&aacute;n cerradas, regresa a golpear las campanas de nuevo.<br>
    Aqu� hay 4 campanas, golp�alas en este orden: arriba-izquierda 3 veces, arriba-derecha 2 veces, abajo-derecha 1 vez y abajo-izquierda 2 veces. La puerta de abajo se abrir&aacute;, entra.<br>
    Salva en la diosa y espero que este en un buen nivel, sobre 35 ojala, ya que se 
    viene un jefe dif�cil. Sube por la puerta norte.<br>
    Encontraras a <strong>Bogard</strong> atrapado en un tipo de jaula, Devius lo meti&oacute; all&iacute; y tiene una poderosa
    magia que H&eacute;roe no puede romper. Ve a la derecha y entra por las puertas del norte.</span></p>
  <table width="100%"  border="0">
    <tr>
      <td width="29%"><img src="imagenes/devius_boss1.JPG" width="280" height="225"></td>
      <td width="71%"><div align="left">
          <p class="Estilo1"><strong>JEFE: Mindflare</strong><strong></strong><br>
          HP: 428, Exp: 70<br>
&nbsp;<br>
          Devius est&aacute; enojado ya que muri&oacute; Amanda, as� que se transforma en un ser horrendo.<br>
          La plataforma gira y &eacute;l se aleja. No es bueno seguirlo, mejor da la 
			vuelta y esp&eacute;ralo y cuando baje, corre y golp&eacute;alo. Su ataque mas poderoso son 3  proyectiles azules
          teledirigidos que tira, quitan como 50 a 60 HP y te deja en estado Oscuro. Esqu&iacute;valos a toda costa.<br>
          La espada y vara son buenos aqu&iacute;. Ojala este en verde y le 
			das de cerca. Usa L&uacute;mina ya que te da energ&iacute;a (en nivel 3 te da 34 de HP de una sola vez). </p>
          <p class="Estilo1">Cuando lo derrotes, <strong>Julius</strong> dice que lo mates, no le haces caso y al fin Devius muere solo. Aparece un tipo llamado <strong>Goremand</strong> que viene y se lleva el alma de Devius... <br>
          </p>
      </div></td>
    </tr>
  </table>
  <p align="left"><span class="Estilo1"><strong>Ciudad de Jadd<br>
	</strong><br>
    Aqu&iacute; <b>Lester</b> toca una linda canci�n que la escuchan todos, hasta el Se&ntilde;or Oscuro. Luego Genoa aparece y le regala a H&eacute;roe el esp&iacute;ritu que era de 
	<b>Medusa</b>, se llama <strong>Astro</strong> (en ingl&eacute;s le llaman Luna) es un esp&iacute;ritu Lunar. Luego Bogard se quedar&aacute; con Heroina y tu debes ir al <strong>Castillo Granz</strong>, cruzando la <strong>Hoya Misma. </strong>Se te une a ti Lester.<br>
    Puedes ir al norte al <strong>Desierto Espinoso </strong>y visitar la Casa Cacto para  templar armas, plantar semillas y hablar con el cactus Li antes de seguir. 
	<br>
	Luego vuelve a Ciudad Jadd y sal por el sur. <br>
    <br>
    <span class="Estilo7">Desierto Jadd<br>
	</span> <br>
Sal por el sureste.    <br>
Corre a la derecha y sal por el este.<br>
Ve al norte.<br>
  </span><span class="Estilo1">
    <br>
    <span class="Estilo7">Hoya Miasma</span> <strong>(Miasma Glen)<br>
	</strong><br>
    Enemigos: Espectro (Specter)   HP: 76   Noche, Assassinant   HP: 105 D&iacute;a, Wood Cylcops (ver Bestiario)<br>
	<br>
    Ve por el norte.<br>
    Coge el cofre, ve a la derecha y sal por el sur.<br>
    Bajo un poco y ve a la derecha, y sube por el norte.<br>
    Ve a la izquierda, y sube por ahora.<br>
    Coge el cofre y regresa.<br>
    Baja un poco, no te tires, y sal por la izquierda.<br>
    Ve a la izquierda, coge el cofre, baja y sal por la derecha.<br>
    Baja caminando, no te tires y coge el cofre que tiene un Caramelo. Regresa por el norte y sal por el oeste.<br>
    Ve a la izquierda, sube todo lo que puedas, ve a la derecha
    y sal por el norte.<br>
    Ve a la derecha y sal por el norte.<br>
    Salva con la diosa y entra a la cueva.
    </span></p>
  <table width="100%"  border="0">
    <tr>
      <td width="29%"><img src="imagenes/cueva%20cascada.JPG" width="280" height="225"></td>
      <td width="71%"><div align="left">
          <p class="Estilo1"><strong>Cueva Cascada (Cascade Cave) </strong><br>
			Enemigos: Calabaza (Bumpkin) HP: 135, Nubarr&oacute;n (Spiny Cone) HP: 92, Killer Pansy HP: 116, Nocopolilla (Gloomoth) HP: 115, Malboro HP: 206 
			solo noche.<br>
&nbsp;<br> 
Como los enemigos son la mayor�a tipo plantas, las armas de corte son buenas como hacha, espada y la magia tipo Lunar como el tu esp�ritu Astro.<br>
Sube y ve a la izquierda. Corta los arbustos del piso con tu espada o hacha, ya que tienen items ocultos. Luego corta la enredadera del norte y entra por 
			all�. 
<br>Al norte rompe las ramas y entra por all�.
<br>Aqu� hay Calabazas que se te acercan y autodestruyen, al�jate de ellas. Corre a la derecha y sal por el sur.
<br>Coge el cofre (Hueso Animal) y regresa.
<br>Corre a la izquierda y sal por el oeste.
<br>Sube y sal por el este.
<br>Ve a la derecha, sube, corta las ramas y entra por el pasaje del norte.
<br>
          </p>
          </div></td>
    </tr>
  </table>
  <p align="left"><span class="Estilo1">Sube, ve a la izquierda y veras un Stone Seal bloqueando una pasada. T�rale tu magia amarilla Astro y entra. <br>
    Coge el cofre que contiene Carb&oacute;n y regresa. <br>
    Corre a la derecha y sal por el norte. <br>
    Sube, y sal por el oeste. <br>
    Sube, ve a la izquierda, luego arriba veras una entrada bloqueada con ramas, no vayas por all&iacute;, sino que ve a la derecha, sal por el este. <br>
  Salva con la diosa ya que se viene un jefe medio dif�cil (yo estaba en nivel 42). Sal por el este.  <br> 
  Al entrar <b>Julius</b>
te ofrece aliarte con &eacute;l y restaurar el ruin imperio de Vandole... </span></p>
  <table width="100%"  border="0">
    <tr>
      <td width="29%"><img src="imagenes/boison.JPG" width="280" height="225"><br>      </td>
      <td width="71%"><div align="left">
          <p class="Estilo1"><strong>JEFE: </strong><span class="Estilo5">Boison Vine (Chica) </span><br>
          HP: 256, Exp: 10<br>
			<br>
          Solo ponte sobre ella, mirando hacia abajo y dale duro con tu hacha, los rayos que tirar&aacute; no te har&aacute;n da&ntilde;o. <br>
          Al poco rato se transformar&aacute;.<br>
			<br>
          <br>
          <strong>JEFE: </strong><span class="Estilo5">Boison Vine</span><br>
HP: 484, Exp: 75<br>
			<br>
Golpea su est&oacute;mago cuando brille, luego cuando se coloque negro
sube cuando muestra la imagen de la derecha, y dale con las fechas a su cabeza. Usa tu magia de L&uacute;mina para darte energ&iacute;a a penas 
			est�s bajo los 70 HP ya que su veneno es muy poderoso. <br>
Cuando lo elimines, aparecer&aacute; <strong>Dr&iacute;ada (Dryad)</strong>, un esp&iacute;ritu de 
			<b>Madera</b>. Sal por el oeste.</p>
          </div></td>
      <td width="71%"><img src="imagenes/boison2.JPG" width="280" height="225"></td>
    </tr>
  </table>
  <p align="left" class="Estilo1">De nuevo, sal por el oeste. <br>
    Salva con la diosa y sal por el oeste.
      <br>
      Aqu� ve a la izquierda y sube, golpea las ramas del norte que bloquean una puerta y aparecer&aacute; otra vez 
	<b>Dr&iacute;ada</b>, abrir&aacute; la pasada y se unir&aacute; a ti.<span class="Estilo1">
	<br>
	Algo curioso es que si te fijas en Lester, en las condiciones de &eacute;l y veras que tiene ese esp&iacute;ritu en un nivel muy alto, por eso &eacute;l tiene tanta MP. Entra por el norte.<br>
      Ve a la izquierda y sal por el sur.
      </span></p>
  <p align="left"><span class="Estilo1"><span class="Estilo7">Camino a Monte Ilusia 
	(Road to Mt. Illusia)<br>
	</span> <br>
  Salva a la izquierda con la diosa, luego entra a la cueva. </span></p>
  <p align="left"><span class="Estilo1">      <span class="Estilo7">Monte Ilusia </span>
	<b>(Mt. Illusia)<br>
	</b><br>
	Enemigos: Aullidor (Howler)   HP: 148, Cerezacieno (Cherry Slime)   HP: 121, Tortuzazo (Tin Tortoise)   HP: 136 solo d&iacute;a, Gray Ox   HP: 212 solo d&iacute;a.
	<br>
	<br> 
    Los Cerezacieno que andan por el piso, son vulnerables a armas de largo alcance como tu Arco o Mayal. Y a la magia de L&uacute;mina 
	(Wisp), Ondina (Undina) y Dr&iacute;ada (Dryad). <br>
	As� que este es un buen lugar para subir de nivel tanto tu experiencia como tus magias ya que afuera esta muy cerca la Diosa. Cada uno de ellos te da 19 de EXP. Yo sub&iacute; esas 3 magias a nivel 15 y las armas Arco y Mayal a 15 igual. Esos enemigos al golpearlos con la espada o hacha te dan 0, pero igual ganas Skill Levels a esa arma.
	<br>
	Aqu� llegu&eacute; al nivel 99 de mi espada y hacha, gracias al emulador <span class="Estilo5">VisualBoy Advance</span>. Ver secci&oacute;n <a href="#19">Secretos</a>.<br>
    A la izquierda ver&aacute;s 2 caras, golp�alas para que queden sonriendo. Sal por el norte.<br>
    Ve por la izquierda y sal por el sur.
    <br>
    Los Aullidores (Howlers) que salen por las noches son fuertes pero si tienes tu hacha o espada sobre nivel 50 (con el  <a href="#20">Truco</a>) mueren de 1 o 2 golpes. Hay 3 rocas all&iacute;, haz que tengas 
	expresi�n neutra (un golpe m&aacute;s cuando est�n sonriendo), para abrir una pasada a la cueva  de la izquierda . Entra por esa cueva.<br> 
    Sigue a la izquierda, y sal por el sur.<br>
    Ve a la izquierda, hay un camino oculto justo abajo del Stone Seal (volc&aacute;n met&aacute;lico, ojo 
	aqu� con este enemigo puedes subir tu flecha al nivel 99 con el truco, ver 
	secci�n Secretos), encu&eacute;ntralo y sigue a la izquierda, sal por el oeste. <br>
    Abre los cofre, salva con la diosa y sal por el este. <br>
    Sube las escaleras, disp�rale al Stone Seal que hay al norte la magia de Dr&iacute;ada y entra por 
	all�. <br>
    Sube, ve a la derecha, coge el cofre, sigue y sal por el sur.<br>
    Avanza a la derecha, sube, coge los cofres de esta zona, en el centro hay una cara-roca que no puedes golpear, sal por el norte. <br>
    Aqu&iacute; hay 5 Tortuzazos, elim�nalos a todos (20 EXP cada uno) y 
	recibir�s un <span class="Estilo5">Mazo</span> <b>(Maze)</b>. Entra a la cueva del norte. Si quieres subirle el nivel a esta arma, solo debes hacer el truquillo explicado en la secci&oacute;n Secretos 
	:).<br>
    Golpea la cara que esta a la izquierda y ponla con expresi&oacute;n enojada, regresa a la pantalla anterior. <br>
    Ve abajo y sal por el sur.<br>
    Entra a la cueva reci&eacute;n formada. Hay 4 cara-rocas, ponlas todas enojadas. Se abrir&aacute; una puerta arriba a la izquierda, entra por all&iacute;.<br>
    Coge todos los cofres
    y regresa.<br>
    Ahora pon todas las cara-rocas con expresi&oacute;n triste y se abrir&aacute; una puerta arriba-derecha. <br>
    Coge el cofre de arriba, tiene un Icono Duende, luego avanza y sal por el sur.<br>
    Avanza a la derecha, coge el cofre y sigue a la derecha para llegar al viejo Castillo Granz.<br> 
    <br>
<br>
<span class="Estilo7">Puente del Castillo Granz<br>
	</span> <br> 
Corre a la derecha y cruza el puente. Ojo que no podr&aacute;s salir hasta que termines tu tarea aqu&iacute;, 
	as� que aseg&uacute;rate de terminar cualquier Quest que tengas pendiente antes de entrar al castillo.</span></p>
  <p align="left"><span class="Estilo1"> <span class="Estilo7">Castillo Granz (Granz Castle)<br>
	</span> <br>
  Avanza y entra.<br> 
  Te encontrar&aacute;s con Isabela, novia del Se&ntilde;or Oscuro seg�n un display que mostraron antes (�recuerdas?), luego ella est&aacute; protegiendo el castillo, te ataca y luego aparece
	<b>Goremand</b> (ese devora almas) y le dice a ella que te deje entrar, son ordenes del Se&ntilde;or Oscuro. Ambos se van luego. Tu entra. </span></p>
  <table width="100%"  border="0">
    <tr>
      <td width="29%"><img src="imagenes/granz_castle.JPG" width="280" height="225"></td>
      <td width="71%"><div align="left">
          <p class="Estilo1"><span class="Estilo7">Castillo Granz</span> (adentro) <br>
						Enemigos: Anthrosect HP: 142, Chobin Hoodlum HP: 138, Duck General HP: 155 solo d&iacute;a, Goblin Guard HP: 160 solo noche, Dainslaif HP: 64 solo noche, Polter Box HP: 88, Granz Soldier HP: 154, Granz Wizard 
			HP: 125<br>
			<br>
La puerta de salida se cerrar&aacute;, salva con la Diosa. Sal por el norte.<br>
			Sube y tendr&aacute;s 3 puertas, no las tomes en cuenta, ve a izquierda, salta hacia abajo, y sal por el sur. <br>
Coge el cofre y regresa.<br>
Salta hacia arriba y entra a la puerta.<br>
Ve a la izquierda -abajo y coge el cofre (Caliz Angelical), luego sal por el sur. <br>
Baja y llegar&aacute;s a las celdas, se cerrar&aacute; la puerta tras tuyo y pelearas con un Granz Wizard, si estas con tu hacha al nivel 99 con 1 golpe morir&aacute;. Tambi&eacute;n hay 2 caja-mounstros, al eliminarlos te dar&aacute;n una llave. Regresa por el norte.<br>
Sube las escaleras y regresa al pasillo principal.<br>
Ve a la derecha y entra a la puerta del final.<br>
Baja las escaleras y sal por el sur.<br>
Ve a la derecha, coge el cofre, luego abajo abre la celda. Te encontrar&aacute;s con tu amigo de infancia <strong>Willy</strong>. Aqu&iacute; hay un Quest.<br>
Regresa por donde viniste.<br>
Sube las escaleras y sal por al sur. <br>
En el pasillo principal usa la llave en la puerta central.<br>
          </p>
      </div></td>
    </tr>
  </table>
  <p align="left" class="Estilo1">Sube, cuidado con las puntas del piso, entra al norte.<br>
    Mata a los enemigos y sal por el norte.<br>
    En esta etapa elimina  todos
los enemigos para ganar una nueva arma, la <strong>Lanza (Lance)</strong>. Esta es mas poderosa que la espada, pero mas lenta que esta 
	y menos lenta que el hacha. Aconsejo subirla al nivel 99 para que le saques el jugo (con las caja-mounstro es f&aacute;cil ya que al golpearla te da 0). <br>
Sube por la izquierda, y sal por el norte.<br>
Sube y a la derecha coge el cofre (Acero Granz), luego baja las escaleras de abajo y sal por el norte. <br> 
Sube un poco y corre a la izquierda, entra por el norte.<br>
	Aqu� hay 4 cofre, pero 2 son reales. �brelos y regresa.<br> 
Camina a la derecha y entra a la puerta verde.<br>
	Aqu� es el cuarto de<strong> Lord Granz</strong> y <strong>Bogard</strong> dice que 
	Hero�na esta bien y viene en camino. Luego llega Hero�na con tu amigo Willy (por lo visto lo saco de la celda) y Lester le dice que Amanda (que 
	muri� recuerda) era su hermana. Dicen que deben buscar alguna pista de Lord Granz ya que creen que esta en ese castillo. Luego 
	Hero�na se te unir&aacute;.<br>
Cuando acabe la conversaci&oacute;n ve arriba a la izquierda y revisa la 
	biblioteca, lee 3 veces sobre los libros que Lord Granz
escribi&oacute; y faltar&aacute; uno, te preguntar&aacute; si quieres poner el libro, dile  que SI.<br>
Sal del cuarto por abajo-izquierda. <br>
Ve a la derecha y sal por el norte.<br>
Ve a la derecha y entra por la puerta.<br>
Sube, salva con
la diosa, coge los 3 cofres de abajo-izquierda, y sal por el oeste.<br>
Prep&aacute;rate a pelear al subir las escaleras.<br>
  </p>
  <table width="100%"  border="0">
    <tr>
      <td width="29%"><img src="imagenes/Garuda.JPG" width="280" height="225"></td>
      <td width="71%"><div align="left">
          <p class="Estilo1"><strong>JEFE: </strong><span class="Estilo5">Garuda</span><br>
HP: 567, Exp: 100<br>
			<br> 
Esta ave lo primero que hace es &quot;barrer&quot; la pantalla de izquierda a derecha, 
			as� que baja r�pido para que no te golpee. Luego se va para arriba y te puede tirar magia verde que te confunde. 
			<br>
			Tambi�n tiene un poderoso ataque, se aleja de la pantalla y te tira bombas, 
			as� que esqu&iacute;valas.<br> 
Usa tus Flechas, ojala en nivel 99, d�ndole cuando este arriba de la pantalla. Con solo 4 o 5 flechas la eliminar&aacute;s (yo quitaba entre 120 y 160 
			HP por flecha). <br>
<br>
Cuando la elimines ve por al derecha.
<br>
Salva con la diosa y sigue a la derecha, te enfrentar�s con el <strong>Se&ntilde;or Oscuro</strong>.<br>
          </p>
      </div></td>
    </tr>
  </table>
  <table width="100%"  border="0">
    <tr>
      <td width="29%"><img src="imagenes/senor_oscuro1.JPG" width="280" height="225"></td>
      <td width="71%"><div align="left">
          <p class="Estilo1"><strong>JEFE: </strong><span class="Estilo5">Se&ntilde;or Oscuro (Dark Lord)</span><br>
          HP: 528, Exp: 120<br>
			<br>
          Apenas termine la conversaci�n, �l te atacar�, su golpe causa 
			Oscuridad as� que al�jate de &eacute;l r&aacute;pidamente ya que sus golpes 
			tambi�n causan par&aacute;lisis. La Lanza es buena, pero lo elimin� con la flecha, mientras &eacute;l golpeaba a 
			Hero�na, yo le tiraba flechas. Como las flechas las tenia en nivel 99 lo 
			elimin� con 6 flechas.<br>
          <br>
          Al eliminarlo &eacute;l llama a <strong>Isabela</strong>, se saca la m&aacute;scara y le pide a 
			<b>Isabela</b> que cuide de <b>Julius</b>. Luego muere. <br>
          Aparece despu�s <strong>Goremand</strong> el devora almas y le pide el amuleto a 
			<b>Isabela</b> y dice que se devorar&aacute; el alma del Se&ntilde;or Oscuro y 
			prontamente la de su hermano <b>Julius</b>. Isabela se transforma en un mounstro tipo conejo y enfrenta a
			<b>Goremand</b>. <br>
			No se sabe en que queda la pelea ya que mostrar&aacute;n unos 
			p�jaros que ayudar�n a Lester que viaj&oacute; al <b>Desierto 
			Cristalino</b>, Lester  salv&oacute; a H&eacute;roe y Hero�na. <br>
          </p>
      </div></td>
    </tr>
  </table>
  <p align="left" class="Estilo1"><span class="Estilo7">Pueblo de Ishe</span> <b>(Town of Ishe) </b> <br>
    <br>
    Luego tendr&aacute;s un lindo sue&ntilde;o y al despertar habla con todos. Lester dir&aacute; que  <strong>Selah </strong>lo salv&oacute;. Ella conoce a Bogard de antes. Cibba dice que Julius tiene la Llave de Santuario, y con ella puede controlar el poder Mana. H&eacute;roe le pide como encontrar la espada Mana y Cibba responde que esta bajo el <strong>Volc&aacute;n Sumergido</strong>, pasado el <strong>Reino de Lorimar</strong>. Lester se une a ti. <br>
    Ve a visitar a Herrer&iacute;a.
    <br>
    Al noreste esta le Ishe-Herreria. Un enano te pide <strong>perrocotones.</strong><br>
  Al centro arriba puedes visitar el Inn para un 
	<strong style="font-weight: 400">Quest</strong> donde te gusanos <strong>bubu</strong> que 
	est�n en el est&oacute;mago de los monstruos, para hacer una medicina. (Ver Quest #5) <br>
  Al noroeste del pueblo esta la tienda principal, all&iacute; un ni&ntilde;o te pide ayuda para explicar como meditar (L+A) y ver el mapa (la libreta de Popoi) y te dar&aacute; una <strong>Pluma de Cancun</strong> (Ver Quest #3). All&iacute; mismo hay un anciano que quiere ver una <strong>Carta de los 7 Juicios</strong>. <br>
  Abajo-derecha del pueblo, por la noche hay una joven que te habla del Quest del  <strong>Trit&oacute;n Asado</strong>. Dice que el hombre aquel esta en el Castillo Lorimar. <br>
  Sal por el Sur, al Desierto Cristalino.
  </p>
  <p align="left"><span class="Estilo1"> <span class="Estilo7">Desierto Cristalino</span><b> (Glass Desert)
	</b><br>
	<br>
  Enemigos: Basilisco (Basilisk) HP: 146<br>
  <br>
  Los
  Basiliscos andan por todos lados, la espada u hacha son efectivos. Corre a la derecha, sube por la pendiente y sigue a la derecha, luego corre y sal por el sur. <br>
  Sigue hacia abajo y sal por el Sur.<br>
  Coge el cofre y regresa.<br>
  Sube un poco y ve a la izquierda, sube.<br>
  Arriba coge los 2 cofres, salta a la izquierda, y sal por el sur.<br>
  Baja un  poco, ver&aacute;s un pendiente y sube, sal por el norte. <br>
  Sube, coge el cofre (Semilla fina) y regresa.<br>
  Baja todo el camino al sur.<br>
  Ve abajo, derecha y sal por el sur.<br>
  A la derecha arma la Casa Cacto 
  y planta semillas. Cuando termines sal por el Sur.<br>
  <br>
  <span class="Estilo7">Armer&iacute;a del Desierto Cristalino<br>
	</span> <br>
  Si jugaste la versi&oacute;n original de <font color="#0000FF">Game Boy</font>, all&iacute; el <strong>Profesor  Proyectil (Prof. Bomb)</strong> te ayudaba a viajar, mejorando  tu <strong>Chocobo</strong> pero como aqu&iacute; no hay Chocobos, entonces tenemos estos <strong>Ca&ntilde;ones</strong> que te ayudar&aacute;n a viajar por el mundo. 
	<br>
	Salva en la estatua y habla con el Profesor Proyectil que esta frente al ca&ntilde;&oacute;n y te mostrar&aacute; el 
  <a href="#15">Mapa</a>. Ahora puedes ir cualquier parte, incluso a terminar los Quest. 
	<br>
	Desde ahora quedar&aacute;n activados todos los ca&ntilde;ones. <br>
  Despu�s de ir donde quieras,  finalmente debes ir a <strong>Castillo Lorimar </strong>(noreste del mapa). 
	<br>
		NO PODR�S HACER MAS QUEST EN LORIMAR, AS� QUE SI TIENES QUEST PENDIENTES 
	TERM�NALOS AHORA. <br> 
  <br>
  <span class="Estilo7">Campo Glacial-Ca��n </span><br>
  <br>
  Baja y ver&aacute;s al Profesor Proyectil congelado, no podr�s ir a otro lado hasta descongelarlo. Coge el 
	cofre (Icono Ondina), salva en la estatua y ve por el oeste. <br>
  <br>
  </span><span class="Estilo1"><span class="Estilo7">Campo Glacial</span> <b>(Snowfield)</b><br>
	<br>
  Enemigos: Gazapo &Aacute;rtico (Snowy Gazapo) HP: 148<br>
	<br>
    Elimina a los Gazapos y ve por la izquierda pero por el camino de arriba (abajo solo hay un Caramelo).<br>
    Ve a la izquierda y sal por el oeste.<br>
    Ve a la izquierda, abre el cofre (Semilla Plana) y sal por el norte.<br>
    Sube y sal por el norte. <br>
    Sube, ve a la derecha, abre el cofre, luego izquierda y sal por el norte.<br>
    Solo sube al norte y encontrar&aacute;s el Castillo Lorimar.
    <br>
    <br>
    </span><span class="Estilo1"> <span class="Estilo7">Castillo de Lorimar<br>
	</span> <br>
  Salva en la diosa y ve por las escaleras del centro, sal por el norte. <br>
  Aqu� H&eacute;roe se da cuenta que todo esta congelado, menos  el <strong>Rey Lorimar..</strong> Luego aparece una joven llamada <strong>Marley</strong> (ama de llaves del Castillo) y cuenta que el Rey hizo un trato con el Imperio Vandole y entreg&oacute; la vida de sus s&uacute;bditos por un poco de Poder Mana. 
	<br>
	Vandole le dio un Esp&iacute;ritu mal�volo que congel&oacute; todo, se llama <strong>Malyris</strong> y esta en  <strong>Cordillera Kahla</strong>. Y te da la llave del Castillo Lorimar. <br>
  Baja las escaleras y regresa a la sala anterior. <br>
 All&iacute; baja y usa las llaves en la puerta izquierda, luego entra.<br>
  Sube las escaleras
y sal por la puerta norte.  <br>Afuera,  sal por el norte.
<br>
<br>
	<span class="Estilo5">Cordillera Kahla<br>
	</span> <br>
Enemigos: Tampole HP: 167 solo d&iacute;a, Dragodon HP: 182 solo d&iacute;a, Colmillot&oacute;n (Sabre Kitty) HP: 170, Hielijuela (Ice Leech) HP: 162, Vampi&eacute;lagoscuro (Dark Batmo) HP: 135 solo noche, Poto HP: 176 solo d&iacute;a, Sky Dragon HP: 236 solo d&iacute;a<br>
	<br>
Aqu&iacute; las armas que puedes usar son Espada, Hacha y la Magia Salamandra. Sube, rompe el hielo azul a golpes, y aparecer&aacute; un cofre (Caramelo) y sal por la izquierda.<br>
Rompe el hielo azul sube rompe el otro hielo (Nuez M&aacute;gica) y regresa al 
	�rea anterior.<br>
Baja, ve a la derecha, rompe los 3 hielo azules y sal por el este. <br>
Ve al fondo de la derecha, sube, rompe el hielo, ve a la izquierda, y sal por el norte.<br>
Sube, a la derecha-arriba hay un cofre (moneda Agua), sal por el oeste.<br>
Sal por el norte.<br>
Sube, rompe hielo y gana Semilla Grande, luego a la derecha, rompe otro y consigue 
	M�rmol. Sal por arriba-derecha. <br>
Ve a la derecha, rompe todos los hielo, coge los cofres, sube y sal por la izquierda.<br>
Ve a la izquierda, rompe los hielos, coge los cofres, sube y entra a la cueva de arriba-derecha.<br>
Salva en la estatua y ve a la derecha, te enfrentar&aacute;s al Jefe Malyris, 
	el que congel� todo.</span></p>
  <table width="100%"  border="0">
    <tr>
      <td width="29%"><img src="imagenes/boss_cordillera.JPG" width="280" height="225"></td>
      <td width="71%"><div align="left">
          <p class="Estilo1"><strong>JEFE: Malyris</strong><br>
HP: 547, Exp: 140<br>
			<br>
Este enemigo sale da arriba de la pantalla, tirando un rayo que te convierte en una bola de nieve. 
			Tambi�n puede tirar una bolas amarillas que te dejan en estado mudo/mute (no podr&aacute;s usar magia). Lo otro es que desaparece y deja caer 3 puntas de hielo que quitan poca energ&iacute;a pero si las tocas te dejan convertido en bola de nieve. Y por ultimo a veces desaparece y usa un tipo de tent&aacute;culo ya sea desde arriba o abajo de la pantalla y hace un &quot;barrido&quot;, 
			as� que al&eacute;jate de el cuando lo haga. <br>
<br>
La t&eacute;cnica es usar tu Mayal. Ojala lo tengas en Nivel 99 y entra cargado (con la barra de golpes en verde)
			de esta forma le das 2 golpes y lo liquidas. Con un nivel sobre 50 y el mayal cargado le quitas 250 puntos promedio, y con un golpe del Mayal bien puesto, le haces 2 veces da&ntilde;o, quitando 500 de vida. luego otro golpe simple y lo liquidas. <br>
<br>
Al vencerle, el esp&iacute;ritu har&aacute; una lluvia de magia que descongelar&aacute; a todo el castillo y las tierras. La gente despierta, el rey es destronado y su hijo <strong>Durac</strong> lo sucede.<br>
<br>
<br> 
<br>
          </p>
      </div></td>
    </tr>
  </table>
  <p align="left"><span class="Estilo1">Una vez en el Castillo, habla con Durac y sal por el sur.<br>
Ahora puedes explorar el castillo. Luego salva en la estatua y ve por la puerta de la izquierda .<br>
A la izquierda, en las escaleras habla con <strong>Kaz</strong>, el te pide que le encuentres <strong>Geodas</strong>, hay 8 tipos, cada uno tiene un esp&iacute;ritu encerrado dentro. El te puede actualizar tus esp&iacute;ritus si les traes sus Geodas. Regresa a la anterior sala. cruza a la derecha, y entra por la puerta de la derecha de la estatua.<br>
Adentro, sube las escaleras y entra por la puerta. <br>
A la derecha encontrar&aacute;s al hombre del Quest del <strong>Trit�n Asado</strong> (por fin), se llama Marco y es de Topple. Dale el Trit&oacute;n y te 
	dar� un <strong>Rabo de Trit&oacute;n</strong>. <br>
Al fondo encontrar&aacute;s el Inn, 300 lucres por dormir. <br>
Regresa al sal&oacute;n principal y sal del castillo.<br>
<br>
  </span><span class="Estilo1"><span class="Estilo7">Campo Glacial</span><br>
  Regresa al ca��n yendo al sur 4 pantallas  y al este 3 pantallas.
  <br>
  <br>
  <span class="Estilo7">Campo Glacial-Ca��n</span><br>
	<br>
  Habla con el profesor y tendr�s 2 destinos: <strong>Ishe</strong> y <strong>Ca&ntilde;on de Taiga Rocosa</strong> (Rocky Wilds). 
	<br>
	Si quieres hacer algo aparte, puedes ir a  <strong>Ishe</strong> (suroeste del mapa), o ir al Pueblo de Wendel antes por el 
	�tem <strong>Miniaturizador</strong> para sacar luego el gran accesorio <strong>Brownie Ring</strong> (recomendado). <br>
  Si quieres seguir con el juego ve a <strong>Ca&ntilde;on de Taiga Rocosa</strong> (oeste del mapa).<br>
  <br>
  </span><span class="Estilo1"> <span class="Estilo7">Ca��n de Taiga Rocosa</span> (Rocky Wilds Armory)<br>
	<br>
  Salva en la estatua y ve al sur.<br>
  <br>
  <strong>Taiga Rocosa<br>
	</strong>  <br>
  Enemigos: 
  Griffon Claw   HP: 198 solo d&iacute;a, Pricklebeak   HP: 167 solo d&iacute;a, Denden Tezla   HP: 142 solo d&iacute;a, Gremlin   HP: 158 solo noche, Nocto Buho (Night Sniper)   HP: 181 solo noche, Garuda   HP: 274 solo d&iacute;a, Infradem&oacute;n (Punkster)   HP: 204 solo noche.<br>
	<br>
  Aqu� es bueno la Lanza y Arco. La Magia de Gnomo es buena. <br>
  Baja un poco y sal por el este.  Cuidado con los enemigos que no les afectan los golpes y las rocas que flotan debes esquivarlas. <br>
  Sal por la derecha.
  <br>
  Ve a la derecha, luego sube, derecha y abajo, coge el cofre (Caramelo), regresa a la pantalla anterior. <br>
  Ve un poco a la izquierda, usa el Mayal y sal por el Norte.<br>
  Tendr�s 2 opciones, primero ve a la derecha, sube las escaleras y sal por el norte. <br>
  Sube, ve a la derecha, usa Mayal y coge C�liz Ang&eacute;lico, regresa a la pantalla anterior.<br>
  Ve por el noroeste ahora.<br>
  Ve a la izquierda, usa el Mayal, sal por el norte.<br>
  Ve arriba, sube las escaleras y sal por el norte. <br>
  Encontrar&aacute;s una <span class="Estilo5">Casa Cacto</span>. Puedes plantar semillas, mejorar armas, etc. Recuerda entrar y salir de la Casa Cacto hasta que Cactosillo deje de escribir. Luego regresa a la etapa anterior. <br>
  Baja las escaleras y sal por el oeste.<br>
  Ve a la izquierda y sal por el norte.<br>
  Sube, salva con la diosa y entra a la cueva.<br>
  <br>
  <span class="Estilo7">R&iacute;o Subterr&aacute;neo (Subland River)<br>
  <span style="font-weight: 400">�tem importante:</span> </span>Brownie Ring</span><span class="Estilo1">.<br>
	<br>
    Enemigos:  Pezasesino (Killafish)   HP: 188, Taxibird   HP: 211 solo d&iacute;a, Clincher Crab HP: 198, Big Baby   HP: 297 solo d&iacute;a, Seadragon   HP: 230 solo noche, Polter Box   HP: 88, Sahagin   HP: 201 solo d&iacute;a. <br>
    <br>
    Sube y sal por la derecha. Ve a la derecha y sal por el norte.<br>
    Sigue a la derecha y sal por el norte. <br>
    Ve a la derecha y ver&aacute;s un peque&ntilde;o hueco. <br>
    Si quieres el �tem <strong>Brownie Ring</strong>: Debes tener usa el Miniaturizador y seguir los pasos de la Secci&oacute;n <a href="#19">Secretos</a>.<br>
    Si no quieres el s�per anillo, sigue a la izquierda por detr�s de las ca�das de agua y coge lo 2 cofres, ve a la derecha, baja, y a la izquierda usa el Mayal para cruzar. Luego salta en diagonal y luego sale por el oeste. <br>
    Usa el mayal arriba para cruzar el r�o, sube las escaleras y ve a la izquierda, habr&aacute; una peque&ntilde;a roca, p&aacute;rate enfrente y el agua  te botar&aacute;. Ve a la derecha, usa el mayal para cruzar, sube otra vez las escaleras y ahora sal por la derecha. <br>
Ve a la derecha y habr&aacute; una peque&ntilde;a roca, p�rate como antes y el agua te botar&aacute;. Luego ve por la izquierda. <br>
Ve a la izquierda y sube y luego a la derecha, llegar&aacute;s a la parte de la cascada, pero ahora 
usa el mayal para cruzar, habr� una peque&ntilde;a roca, el agua te botar&aacute; abajo. Abajo ve por el centro-izquierda y usa de nuevo el mayal y sal por el oeste. <br>
Ve a la izquierda y sube con el mayal, luego sube las escaleras y sal por la derecha.<br>
Sigue a la derecha, en la misma pantalla de las cascadas y sigue a la derecha y luego al norte.<br>
Sube y ve a la izquierda.<br>
Ve a la izquierda y coge los 4  cofres. Regresa a la derecha. <br>
Ve a la derecha, baja y ve a la derecha, sube las escaleras
y ver&aacute;s como un pozo de agua y una roca en el centro si no haz drenado toda el agua, 
	as� que regresa y una vez que lo hagas vuelve aqu&iacute;. Una vez que lo hagas podr&aacute;s subir.<br>
Sal por la derecha, a la izquierda hay cofres.<br>
Salva con la diosa y h�blale al tipo que hay en le piso.  
</span></p>
  <table width="100%"  border="0">
    <tr>
      <td width="29%"><img src="imagenes/jefe_Kraken.JPG" width="280" height="225"></td>
      <td width="71%"><div align="left">
          <p class="Estilo1"><strong>JEFE: Kraken </strong><br>
          HP: 635, Exp: 170<br>
			<br>
          Este jefe
          es como un mega pulpo que aparece por 3 lugares diferentes. Deja adem&aacute;s un pez que es muy r&aacute;pido pero quita poca 
			energ�a. Cuando salga  este pez s&oacute;lo al&eacute;jate y de lejos dale con la Lanza o Mayal.
			<br>
			Kraken te puede disparar un proyectil que te causa oscuridad. Tambi&eacute;n puede usar sus tent&aacute;culos que salen de alguno de los pozos y te golpean, 
			as� que mantente en movimiento siempre. <br>
          <br>
          Una vez que lo elimines, sal por abajo-izquierda.<br>
          Salva en la estatua y ve al noroeste y encontrar&aacute;s una cueva, <strong>Volc&aacute;n Sumergido. </strong><br>
          <br>
          </p>
      </div></td>
    </tr>
  </table>
  <p align="left"><span class="Estilo1">      <span class="Estilo7">Volc&aacute;n Sumergido (Subsea Volcano)<br>
	</span> <br>
      Enemigos:  Knollbear   HP: 227, Grumpkin   HP: 258,  Flame Moth   HP: 184, Tomat&oacute;n (Tomato Man)   HP: 210 solo d&iacute;a, Cumulus   HP: 135 solo les afecta la magia, Land Dragon   HP: 343 d&iacute;a, Polter Box   HP: 88<br>
      <br>
    Bienvenido a uno de los m&aacute;s dif�ciles calabozos del juego. Baja la escalera y sal por arriba. <br>
    La Lava si la tocas te quita mucha energ&iacute;a, as� que cuidado. Ve al noroeste y sal por el norte. <br>
    Sube las escaleras, sal por la derecha. <br>
    Abre el cofre y regresa.<br>
    Ve a la izquierda y ver&aacute;s un Stone Seal, usa la magia Astro (amarilla) y pasa al norte. <br>
    Esas bolas de lava
    al acercarte se pondr&aacute;n rojas, esa es una se&ntilde;al para alejarse de inmediato ya que explota y quita much&iacute;sima energ&iacute;a. Sigue a la derecha y sal por el Sur. <br>
    Salta abajo, baja y ve por el camino de la izquierda y sal por el oeste. <br>
    Abre el cofre y regresa. <br>
    Ve a la derecha coge el cofre, luego sal por el norte.<br>
    Aqu&iacute; aparecer&aacute;n varios Metaballs, usa primero la Espada, luego arriba Mazo, pegado abajo Falce, luego la Lanza y finalmente Lanza otra vez. Sal por el este. (Si tienes el 
	�tem <strong>Miniaturizador</strong> ve por la lava de la izquierda del cuarto y llegar&aacute;s a una entrada, 
	�salo y llegar&aacute;s a una parte donde hay 2 cofres con muy buenos &iacute;tems.)<br>
    Ve al final a la derecha y sal por el 
    norte.<br>
    En esta &aacute;rea hay enemigos como nubes, solo mueren con magia Lumina. Sube por la lava y al centro hay como una roca mas clara, 
	r�mpela con un golpe y pasa, luego sal por la izquierda.<br>
    Ve a la izquierda, rompe de nuevo la roca y sigue por el norte.<br>
    Coge el cofre y regresa.<br>
    Ve a la derecha
    y sal.<br>
    Devu&eacute;lvete por donde subiste y baja, ve al suroeste y sal por la izquierda .<br>
    Coge el cofre, sube las escaleras, ve a la izquierda y sal por el sur.<br>
    Coge el cofre y regresa. <br>
    Sube y por la parte superior-izquierda golpea, abrir&aacute;s una pasada, entra. <br>
    Salva con la diosa y prep&aacute;rate para una pelea, cuando est&eacute;s listo habla con el robot. <br>
    Luego que hables con &eacute;l, te acercar&aacute;s a la <strong>Espada de Mana</strong> y empezar&aacute; a temblar...
</span></p>
  <table width="100%"  border="0">
    <tr>
      <td width="29%"><img src="imagenes/jefe_efflite.JPG" width="280" height="225"></td>
      <td width="71%"><div align="left">
          <p class="Estilo1"><strong>JEFE: Efflite</strong><br>
          HP: 682, Exp: 180<br>
			<br>
          Este jefe es dif�cil ya que primero debes preocuparte de no tocar la lava. Adem&aacute;s el jefe se transforma en una bola de lava y gira por la pantalla, si te toca te da&ntilde;a, para esto debes alejarte de &eacute;l. <br>
          Lo otro es que se quede quieto sobre ti y te tire bolas de fuego muy da&ntilde;inas, aqu&iacute; es la 
			oportunidad de golpearle con alguna arma a distancia como el Mayal o la Lanza, subiendo y par&aacute;ndote a la altura de su cabeza. <br>
                    <br>
          Una vez que lo elimines, el H&eacute;roe llevar&aacute; la espada al maestro <strong>Cibba</strong> en Ishe<strong>. </strong><br>
          Cibba dice que <strong>Julius</strong> tiene la llave del Santuario y no se puede llegar antes que &eacute;l, 
			as� lo que lo �nico que se espera es que el camino al Santuario est&eacute; sellado, pero la <strong>Torre Dime</strong> es un puente al Santuario, est&aacute; enterrada en el desierto Cristalino y Cibba la 
			enterr� hace tiempo con magia. <br>
          <br>
          Luego de hablar, comer y algo de romance de H&eacute;roe y Hero�na est&aacute;s listo para seguir. <br>
          Habla con todo el grupo y Hero�na se te unir&aacute;. Es hora de ir a <strong>Torre Dime. </strong> Sal de la casa de Selah. 
			<br>
			Ve al Inn, salva y sal de la ciudad por el norte. Vamos camino a <span class="Estilo7">Pasadizo Ruinoso</span>.<br>
          <br>
          </p>
      </div></td>
    </tr>
  </table>
  <p align="left"><span class="Estilo1"><span class="Estilo7">Desierto Cristalino<br>
	</span><br>
    Enemigos: Calaver&aacute;nade (Skull Drake)   HP: 213, Fierce Face   HP: 235 solo noche, Tyrranos   HP: 260 solo 
	d�a, Drag&oacute;n (Kid Dragon)   HP: 148 solo d�a<br>
    <br>
    Ten mucho cuidado con los Calaver&aacute;nades ya que con una magia brillante te hacen rebotar los golpes que les des y si estas muy fuerte, te autoeliminas si los golpeas, y puedes morir de un solo golpe. Ve arriba, derecha, baja y sal por el este.<br>
    Aqu&iacute; hay unos  puentes que solo aparecen de  noche, as� que si 
    sino est&aacute;n, regresa y entra de nuevo. Cruza los puentes que se ven como brillantes, ve al noreste y sal por el norte.<br>
    Sal por la izquierda.<br>
    Sube las escaleras, sube y sal por el sur.<br>
    Sigue al sur, abre el cofre y regresa.<br>
    Corre y sal por el norte.<br>
    Si el puente no est&aacute;, regresa y entra de nuevo hasta que aparezca. Sube coge los 2 cofre y en la esquina superior derecha hay una cosa brillando, es una <strong>Geoda de Luz</strong>, c&oacute;gela y regresa. <br>
    Corre al sur y en la mitad ve al este y sal por all&iacute;.<br>
    Ve a la derecha y cruza por el puente que solo aparece de noche.
    Ve a la derecha, sube, coge los 2 cofres y sal por el sur. <br>
    Baja, coge los cofres que encuentres y sal por el oeste.<br>
    Abre el cofre y regresa. <br>
    Corre a la derecha y sal por el norte.<br>
    Ve al noroeste, baja el terrapl�n  y sal por el oeste. <br>
    Ve a la izquierda y sal por el norte.<br>
    Cruza el puente, ve a la derecha, coge los cofres con Icono Umbra y Nuez M&aacute;gica (recupera 80 PM), ve a la izquierda y cruza el de arriba y sal por el norte.<br>
    Salva con la diosa y sal por el norte.
    <br>
    <br>
<span class="Estilo7">Pasadizo Ruinoso</span> (Ruined Passage)<br>
	<br>
    Coge los 2 cofres y presiona luego el bot&oacute;n A sobre la puarte central de arriba, hablar&aacute;s con <strong>Goremand</strong> y dice que no mat&oacute; a Isabela. Adem&aacute;s dice que debes ir a la Cueva Sellada primero y luego regresar aqu&iacute;. Sal por el sur.<br>
    <br>
    <span class="Estilo7">Desierto Cristalino<br>
	</span> <br>
  Salva en la estatua y sal por el sur.<br>
  Ve a la izquierda, cruza 2 puentes y sal por sur.
  <br>
  Baja y sal por la derecha.<br>
  Ve a la derecha, sube las escaleras, baja y ve a la izquierda, cruza el puente y sal por el oeste.<br>
  Corre a la izquierda y sal por el sur.<br>
  Sigue al
  sur y salta al sur, luego baja y sal por el sur. Llegar&aacute;s a Ishe.<br>
  <br>
  <span class="Estilo7">Ciudad de Ishe<br>
	</span> <br> 
  Ve al sur y sal.
<br>
<br>
<span class="Estilo7">Desierto Cristalino<br>
	</span> <br> 
Sal por el sur.<br>
Baja, sube las escalas, baja y sal por el sur.<br> 
Ve abajo, luego derecha, baja las escalas, y sal por el sur.
<br>
Activa la Casa Cacto y cuando termines sal por el sur.<br> 
<br>
<span class="Estilo7">Desierto Cristalino-Ca&ntilde;&oacute;n</span> <br>
	<br> 
Habla con el Profesor Proyectil y un nuevo lugar aparecer&aacute; al Suroeste, <strong>Cueva Sellada</strong>, 
	selecci�nalo y vamos all&aacute;.<br>
<br>
<span class="Estilo7">Cueva Sellada-Ca&ntilde;&oacute;n<br>
	</span> <br> 
Salva 
con la estatua y sal por el este. <br>
<br>
<span class="Estilo7">Cueva Sellada<br>
	</span> <br>
Enemigos: Hoppin' Tick   HP: 235 solo d&iacute;a, Sombra Zed (Shadow Zed)   HP: 143, Gran M&iacute;mico (Kaiser Mimic)   HP: 235 solo d&iacute;a, Ape Mummy   HP: 364 solo noche, Malignus (Ghoul)   HP: 263 solo noche, Sangre Sed (Bloody Bjorn)   HP: 216 solo noche.<br>
<br>
Aqu&iacute; los Malignos son d&eacute;biles a la magia Ondina. Entra  a la cueva.<br>
Ve a la derecha, sube las escaleras, ve a la izquierda, y sal por el norte.<br>
Sube, ve a la izquierda, veras un switch, salta a la derecha y presi�nalo, salta a la izquierda y sal por el sur.<br>
Baja, ve a la
derecha, presiona el switch. Baja, ve a la izquierda, presiona el otro switch y regresa a la sala anterior.<br>
Sube y sal por el norte.<br>
Ve a la derecha, sube la escalera, 
ve a la izquierda y sal por el norte.<br> 
Sube y sal por la derecha saltando los escalones.<br>
A la derecha presiona otro switch, baja y salta abajo, presiona otro switch (dir&aacute; algo del quinto sello se ha roto) y sal por la izquierda.<br>
Ve al suroeste y sal por abajo.<br>
Baja un poco ve a la derecha y sal por el sur.<br>
Ve al sureste y sal por el este.<br>
Baja un poco, ve a la derecha y sube, coge los 2 cofres, y entra a la cueva de la izquierda.<br>
Ahora se viene un jefe, as&iacute; que, prep&aacute;rate. Cuando est&eacute;s listo, sube, lee la l&aacute;pida y pon Si. A pelear con el Jefe <strong>Lich.</strong></span></p>
  <table width="100%"  border="0">
    <tr>
      <td width="29%"><img src="imagenes/jefe_lich.JPG" width="280" height="225"></td>
      <td width="71%"><div align="left">
          <p class="Estilo1"><strong>JEFE: Lich</strong><br>
          HP: 707, Exp: 200<br>
			<br>
          Cuidado con su  guada&ntilde;a que gira y no te deja acercarte, no hace mucho da&ntilde;o pero si la tocas  muchas veces y no te curas, morir&aacute;s. Espera a que desaparezca su guada&ntilde;a para atacarlo. Aparte te tira unos rayos muy r&aacute;pidos que debes esquivar. <br>
          Las armas a distancia le hacen buen da&ntilde;o, como la Lanza o Mayal. 
          Si entras de color verde, con nivel 51 y la Lanza en nivel 99, con una solo golpe de cerca lo eliminas. <br>
          <br>
          Al eliminarlo te da la <strong>Llave</strong> del pasadizo Ruinoso, un Esp&iacute;ritu Oscuro llamado <strong>Umbra</strong>. Es hora de volver a las ruinas. Sal por el sur.<br>
          Ve al  derecha, baja, izquierda y sal por el oeste.<br>
          Ve al suroeste y sal de la cueva.<br>
          Ve a la izquierda.
          <br>
          </p>
      </div></td>
    </tr>
  </table>
  <p align="left" class="Estilo1">  <span class="Estilo7">Cueva Sellada-Ca&ntilde;&oacute;n<br>
	</span><br>
  Habla con el Profesor Proyectil y vuelve a Ishe. <br>
  <br>
  <span class="Estilo7">Pueblo de Ishe<br>
&nbsp;</span><br> 
  Duerme en el Inn
y ve al norte, camino a <span class="Estilo7">Pasadizo Ruinoso</span>. Tambi�n ahora que est&aacute;s aqu&iacute;, puedes elegir pasar las misiones que tengas 
	pendientes antes de seguir. <br>
<br>
<span class="Estilo7">Desierto Cristalino<br>
	<br>
</span>Sal por el este. <br>
Sube, cruza el puente y sal por en norte.<br>
Ve a la izquierda.<br>
Sube las escaleras y sal por la derecha.
<br> 
Cruza el puente, sube, baja el terrapl�n y sal por el oeste.<br>
Sal por el norte.<br>
Cruza el puente y sal por el noreste.<br>
Salva en la estatua y entra por el norte.
<br>
<span class="Estilo7"><br>
Pasadizo Ruinoso<br>
	</span> <br> 
	Ac�rcate a la puerta y presiona A. Ahora con los poderes de Luz (L&uacute;mina) y de la Oscuridad (Umbra) se abrir&aacute; la puerta.<br>
<br>
Enemigos: Wolfiend   HP: 275 solo noche, Dulahan (Dullahan)   HP: 300 solo noche, Ojo Cabal (Wizard Eye)   HP: 214, Springball   HP: 373 solo d&iacute;a, 
Perronfermo (Hellhound)   HP: 261 solo noche, Viper   HP: 237 solo d&iacute;a, Beholder   HP: 221 solo d&iacute;a.<br>
<br>
Sube, salta y sal por la 
salida noroeste.<br> 
Avanza por el corredor hasta que salgas por la derecha.<br>
Coge el cofre y sal por la izquierda.<br>
Baja un poco y sube las escaleras, ve a la derecha.<br>
Coge el cofre (Icono Astro) y salta abajo. Ve a la derecha, sube y sal por la derecha.<br>
Elimina los Metaballs con la Espada y Falce y sal por el sur. <br>
A la derecha usa magia L&uacute;mina sobre la Stone Seal de abajo y usa Umbral
sobre la de arriba. Sal por el norte.<br>
Coge los 
cofres y sal por el norte.<br>
Ve a la izquierda, salta 
a la plataforma y sal  por el oeste. <br>
Presiona los 2 switch y regresa.<br>
Ve saltando por las plataformas y sal por la derecha. <br>
Coge los 2 cofres, y activa el switch de la derecha solamente. Regresa al cuarto anterior.
<br>
Estar&aacute; cambiado el cuarto, ve a la izquierda y entra por la puerta.<br>
Ve a la izquierda y presiona el switch. Ve mas arriba y luego pasa a la derecha.<br>
<br>
<span class="Estilo7">Pasadizo Ruinoso - Nivel 3</span> <br> 
Usa el mayal abajo, ve a la derecha y sal por el norte.<br>
Ve a la derecha 
un poco, y sube, si quieres encontrarte con un <b>Dulahan</b> esta es la &uacute;nica parte del juego donde aparece. <br>
Luego ve m&aacute;s a la derecha y usa la magia Astro y Salamandra sobre las 2 Stone Seal que hay. Sigue y coge los 2 cofres, luego regresa y ve a la izquierda. 
	<br>
	Sube las escaleras, sigue y sube otras escaleras y sal por el norte. <br> 
Sube las escaleras y se te preguntar&aacute; si realmente quieres seguir. <span class="Estilo5">
	<br>
	Nota:</span> aqu&iacute; entrar&aacute;s a la Torre Dime (recuerda que la idea es llegar al Santuario) y all&iacute; no se puede regresar de ninguna forma, ni con la cuerda. As&iacute; que aseg&uacute;rate de hacer las misiones y subir niveles antes de entrar. <br>
<br>
<span class="Estilo7">Torre Dime </span><b>(Dime Tower)<br>
	</b><br> 
Enemigos: 
Voo Doll   HP: 294 solo d&iacute;a, Ghost   HP: 245, Copper Knight   HP: 318 solo d&iacute;a, Stained Sword   HP: 273 solo noche, Chess Knight   HP: 322 solo d&iacute;a, Machine Golem   HP: 393 solo d&iacute;a.<br>
<br>
Salva en 
la estatua y sube. <br> 
H&eacute;roe y Hero�na deciden explorar y ella se va a la derecha. Ve a la izquierda, sube, otra vez a la izquierda y baja las escaleras.
<br>
<br>
Torre Dime -1S<br>
Ve a la derecha, cruza las puntas, presiona A sobre la media bola azul, ve a la izquierda y baja.<br>
Ver�s varias de esas m&aacute;quinas que te encontraste en Rio Subterraneo. Resulta ser un buen 
	<b>Robot</b> y se te une. <br>
		Por si acaso se llama <b>Marshall</b>. Coge el cofre de la derecha y luego ve a la izquierda, sube donde estaban las puntas en el piso, ve a la derecha y sube las escaleras. <br>
<br>
Torre Dime -PB<br>
Coge el cofre y baja.
<br>
<br>
Torre Dime -1S<br>
Ve a la izquierda y sube.
<br>
<br>

Torre Dime -PB <br>
Ve al centro y lee la inscripci�n que brilla en el centro, el robot te ayudar&aacute;. Ve a la derecha y sube las escaleras, sal por el norte. <br>
<br>
Torre Dime -1P<br> 
Lee la l&aacute;pida y golpea la media bola  3 veces. Ve a la derecha, sube la escala y sal por el norte. <br>
<br>
Torre Dime -2P<br>
Ve a la izquierda y baja golpea las 2 media bolas veces hasta que brillen, luego devu&eacute;lvete al cuarto anterior. <br>
<br>
Torre Dime -1P<br>
Cruza a la izquierda y sube las escaleras. Sal por el norte.
<br>

<br>
Torre Dime -2P<br> 
Lee la l&aacute;pida y golpea la esfera hasta que brille. Sal por el norte.<br>
Salva en la estatua y sigue subiendo. <br>
<br>
Torre Dime -3P<br>
Lee la l&aacute;pida, ve a la izquierda, sube la escala, coge el cofre. Baja la escala, ve a la derecha y coge el otro cofre. Ve al sur y empezar&aacute; a temblar y los pisos anteriores colapsar&aacute;n. Ve al sur, esquiva los fuegos y sube la escalera. <br>
<br>
Torre Dime -4P<br>
A la izquierda usa el Mayal para cruzar. Coge el cofre, salta en diagonal a arriba-derecha, coge el otro cofre y sube la escalera.<br>
<br>
Torre Dime -5P<br>
A la derecha lee la l&aacute;pida, luego ve a la izquierda, baja usa el mayal, cruza abajo. Usa la Manopla cargado en verde para tirar un rayo y golpear el Metaball de la izquierda y cruza y usa el Mayal cargado para destruir el Metaball de la derecha. Con esto, se abrir&aacute; un pasaje a la derecha, entra all&iacute;. <br>
Salva en la estatua y sube. <br>
<br>

Torre Dime -6P<br>
Lee la l&aacute;pida, activa los 3 cuadrados verdes golpe�ndolos, primero el de abajo del cuarto, luego el de la derecha-arriba y luego el de la izquierda-arriba, se abrir&aacute; un pasaje, entra por all&iacute;. <br>
<br>
Torre Dime -7P<br>
Lee la l&aacute;pida y golpea la esfera 3 veces y brillar&aacute;. Lo mismo con la de que est&aacute; a la izquierda. Luego 3 veces a  la que est&aacute; en la derecha. Luego ve a la derecha-abajo y pisa ese cuadrado amarillo que hay en el piso y empezar&aacute; a temblar y se caer&aacute;n los pisos anteriores. Usa el Mayal a la izquierda y sube las escaleras. <br>
<br>
Torre Dime -8P<br>
Lee la l&aacute;pida, luego pisa los cuadrados amarillos-verdes que hay a la izquierda y derecha del cuarto y brillar&aacute;n las esferas, luego sube y sal por el norte.<br>
Salva en la estatua y sube.
<br>
<br>
Torre Dime -9P<br> 
Ve a la derecha, golpea la esfera, lee la l&aacute;pida, luego ve a la izquierda y golpea la otra esfera. Finalmente ve a la derecha de nuevo, y golpea la esfera de la derecha. Regresa a la izquierda y sube por la escalera. <br>
<br>

Torre Dime -10P<br>
Ve a la derecha y lee la l&aacute;pida. Ve a la izquierda y lee la otra l&aacute;pida. Cambia de personaje y juega con Marshall, usa los 
	l�ser para matar a los 3 enemigos y abajo se abrir&aacute; una pasada. <br>
	F�jate que las 3 luces que est&aacute;n en la l&aacute;pida de la izquierda se deben encender, sino, algo te falto. Ve por la pasada de abajo.<br>
<br>
Torre Dime -11P<br>
Ve a la izquierda y salta al nivel inferior, all� puedes llegar al cofre de la izquierda-abajo saltando. Contiene la poderosa Aleaci&oacute;n Altena para mejorar armas. Salta arriba, sube y ve a buscar el cofre de arriba-derecha, pero debes dejar brillando el cuadrado del piso cerca del cofre. Ve a la estatua y salva, ahora ve arriba-derecha y lee la l&aacute;pida. Justo abajo a la derecha de la l&aacute;pida hay un cuadrado que debes pisar y dejar brillando. Ve al centro y sube la escalera. Los pisos se caer&aacute;n una vez m&aacute;s, continua subiendo y entra a la puerta. 
	<br>
	Prep&aacute;rate ya que pelear&aacute;s contra un jefe.<br>
<br>
Torre Dime -12P</p>
  <table width="100%"  border="0">
    <tr>
      <td width="29%"><img src="imagenes/jefe_golem.JPG" width="280" height="225"></td>
      <td width="71%"><div align="left">
          <p class="Estilo1"><strong>JEFE: Golem</strong><br>
          HP: 729, Exp: 220<br>
          <br>
          Golem puede ser un jefe dif�cil si no est&aacute;s preparado. No se mueve muy r&aacute;pido, de repente tira un 
			l�ser alrededor suyo, pero si estas muy cerca no te afecta. <br>
          Se puede desarmar en 5 partes, que te siguen, golpea la del centro y luego corre ya que te aplastar&aacute;.<br>
          Si te arrancas de &eacute;l, te tirar&aacute; una parte de si, as� que debes estar en movimiento.<br>
          Si estas en un nivel sobre 50 y la Espada o Manopla lo eliminar&aacute;s sin problema, solo debes golpearlo cuando puedas.<br>
          <br>
          Al eliminarlo llegar&aacute; Hero�na (&iquest;como habr&aacute; subido si los pisos colapsaron?). Luego te encuentras con la <strong>Sombra de Julius</strong>, despu&eacute;s tiembla y se separa el piso, quedando el 
			H�roe separado del grupo, el robot la arroja y el se queda, todo el piso se derrumba luego. <br>
          Ambos entran a un rayo que los teletransporta lejos... </p>
      </div></td>
    </tr>
  </table>
  <p align="left" class="Estilo1"><span class="Estilo7">Santuario Mana</span> <b>(Mana Sanctuary)<br>
	</b> <br>
Ambos est�n frente a las puertas, as� que entra.<br>
Adentro salva en la estatua dorada (te llena PM y HP) y a la derecha arma la Casa Cacto. Luego ve al norte.<br>
<br>
Enemigos: Earth Basilisk   HP: 324, Ache Cone   HP: 305 noche, Bonny Matango   HP: 312, Petit Poseidon   HP: 350, Polter Box   HP: 88 solo d&iacute;a, Wooding   HP: 427 solo noche.<br>
<br> 
NOTA: debes ir con cuidado ya que el camino es es ENREDADO y si no sigues el camino correcto deber&aacute;s empezar desde 0. Para una r&aacute;pida referencia el camino es ARRIBA, DERECHA, ABAJO, DERECHA, ARRIBA.<br>
<br>
Ve al noroeste coge el  cofre y sal por el norte.<br>
Sal por la derecha-arriba.<br>
Sal por el sur.
<br>
Baja y sal por el este.<br>
Ve a la derecha y salta hacia arriba y sal por el norte.<br>
Ambos dicen que  <strong>Julius</strong> est&aacute; manipulando el Mana as� que deciden apurarse.
Sal por el norte, si vas al sur te devolver&aacute; todo el camino.<br>
<br>
Sal por el noreste.<br>
Corre y sal por la derecha.<br>
Salva en la estatua de oro, y regresa por la izquierda.<br>
Ve abajo, izquierda, norte y sal por el norte.<br>
Coge el cofre de la izquierda y regresa.<br>
Baja un poco y corre a la izquierda y sal.<br> 
Sal por la izquierda.<br>
Ve a la izquierda, salta al nivel superior y sal por el noreste.
<br>
Sube y sal por el norte.<br>
Sube y sal por el oeste.<br>
Coge los 4 cofres y regresa.
<br>
Ve a la derecha, sube y entra a la construcci&oacute;n. <br>
Coge los 3 cofres y regresa.<br>
Baja un poco y sal por la derecha.<br>
Corre a la derecha y sube la escala del norte.
<br>
Aparecer&aacute; la <span class="Estilo5">Sombra de Julius</span> y te dejar&aacute; un amigo con el que debes luchar. <br>
  </p>
  <table width="100%"  border="0">
    <tr>
      <td width="29%"><img src="imagenes/jefe_demagon.JPG" width="280" height="225"></td>
      <td width="71%"><div align="left">
          <p class="Estilo1"><strong>JEFE: Demag&oacute;n </strong><br>
          HP: 800, Exp: 250<br>
          <br>
          Demag&oacute;n es un enemigo tipo planta. Aparecer&aacute;s en el piso, por lo que deber&aacute;s subir por las 
			ra�ces para verlo y golpearlo. <br>
          Te tirar&aacute; bolas de fuego que quitan mucho. Tambi&eacute;n 
			ataca con una punta que sale por la izquierda o derecha y que debes esquivar, a veces te deja en estado STOP, y no puedes moverte por un rato. <br>
          La mejor t&eacute;cnica es ponerte al costado izquierdo con el Falce y solo atacarlo 
			de frente cuando puedas. <br>
          <br>
          Despu&eacute;s de eliminarlo sal por la derecha para llegar al <strong>Templo Mana</strong>.</p>
      </div></td>
    </tr>
  </table>
  <p align="left" class="Estilo1">  <span class="Estilo7">Templo Mana</span> <b>(Mana Temple) 
	</b> <br>
  Enemigos: Earth Basilisk   HP: 324, Ache Cone   HP: 305 solo noche, Bonny Matango   HP: 312, Petit Poseidon   HP: 350, Turtle Shelly   HP: 375 solo d&iacute;a, Shamanion   HP: 317 solo d&iacute;a, Mean Mask   HP: 345 solo noche, Moldy Goo   HP: 428 solo d&iacute;a, Wormwood   HP: 460 solo noche. <br>
  <br>
  Templo Mana -1P<br>
  Ve a la derecha y sal por las escalas.<br><br>
  Templo Mana -PB<br>
  Ve a la izquierda y sube las escaleras.
  <br>
  <br>
  Templo Mana -1P<br>
  Ve a la derecha, baja, luego izquierda, sube y pasa por el arco, sal por el oeste.<br>
  Ve a la izquierda, pasa el arco y baja las escaleras.  <br>
<br>
Templo Mana -PB<br> 
Coge los 2 cofres y regresa.
<br>
<br>
Templo Mana -1P<br>
Ve a la izquierda, baja, derecha, sube y sal por la derecha. <br>
Ve a la derecha, baja y luego corre a la derecha, sube y baja las escaleras.
<br>

<br>
Templo Mana -PB<br>
Corre a la derecha y baja las escalas. <br>
  <br>
  Templo Mana -1S<br>
  Baja y sube las escaleras de la derecha.
  <br>
  <br>
  Santuario Mana<br>
  Coge los 3 cofres y regresa.
  <br>

<br>
Templo Mana -1S<br>
Ve al noroeste y sal por la izquierda.<br> 
Ve al noroeste y sube las escaleras.
<br>
<br>
Templo Mana -PB<br>
En este cuarto hay 2 cofres, c&oacute;gelos y luego ve  al noroeste y sube las escaleras.<br>

<br>
Templo Mana -1P<br>
Ve al norte y coge los 3 cofres. Uno tiene Elixir de miel que te llena toda tu HP. Baja un poco y sal por la derecha.<br>
Corre a la derecha y baja la escala.
<br>
<br>
Templo Mana -PB<br>
Ya afuera, baja y sal por la izquierda.<br>
Ve a la esquina noroeste, coge el cofre y regresa.<br>
Corre a la derecha y sal por arriba. <br>
Sube y entra al Templo.<br>
Coge los 2 cofres que est&aacute;n a la izquierda y derecha del cuarto 
y entra por el norte. <br>
Salva en la diosa dorada y prep�rate para la <strong>Batalla Final. </strong>Cuando est&eacute;s listo, entra al cuarto superior. <br>
<br>
<br>


  <span class="Estilo7">Batalla Final<br>
	</span><br>
  La <b>Sombra de Julius</b> dice que solo un &quot;Dios Julius&quot; debe pisar este templo sagrado. 
	Recuerda que Julius te mand� a volar en la nave cuando fuiste en rescate de 
	Hero�na. Luego te muestra a un Bogard negro y te confiesa algo. Al parecer la sombra de Julius te muestra el verdadero sentimiento hacia ti en los corazones de <strong>Lester</strong>, <strong>Willy</strong> y incluso de 
	Hero�na. Luego los hechiza para que se armen y ataquen, pero H�roe la convence y muchos amigos de 
	Hero�na le dan sus poderes para que se libre del hechizo. <br>
		Todos se liberan de la magia de Julius y llega el sabio <strong>Cibba</strong>.  Bar&oacute;n Bogard le 
	confiesa a Hero�na que el es su <strong>padre</strong> (por fin despejada la duda, no son hermanos H&eacute;roe y 
	Hero�na). <br>
  La Sombra de Julius llama al verdadero Julius y le pide poder. <br>
		Luego aparece Granz medio muerto (no estaba muerto) y dice que con su alma &eacute;l y otros han vivido consumi&eacute;ndosela. <br>
  La sombra le tira magia a todos y quedan en el piso, pero Granz se para y le tira una magia y saca del lugar  a 
	H�roe y Hero�na. <br>
  Granz, Bogard, Lester y Willy pelean contra la sombra y ganan. No lo 
	muestran pero H�roe y Hero�na al menos lo dicen.<br>
  <br>
  Sube 
  al norte ya que te enfrentar&aacute;s contra el mismo <strong>Julius.</strong><br>
  Antes de pelear te dice que quiere el poder Mana para conocer a la Diosa que creo el mundo y humillarla. Pero 
	Hero�na recuerda algo importante, que su madre es el <strong>&Aacute;rbol Mana</strong>. Por eso Bogard dec&iacute;a que &eacute;l entreg&oacute; a su esposa al Santuario 
	(H�roina y su madre deben ser parte del �rbol, es su destino), para que el centro del Mana estuviera all&iacute;. 
	<br>
		Ahora Julius dice que H�roe debe entregar a Hero�na al Santuario, es su destino. 
	H�roe se molesta pero luego conf�a en ella, lo acepta como un caballero Mana. 
	<br>
	La espada Mana brilla y esta lista para la pelea. <br>
  </p>
  <table width="100%"  border="0">
    <tr>
      <td width="29%"><img src="imagenes/jefe_julius1.JPG" width="280" height="225"></td>
      <td width="71%"><div align="left">
          <p class="Estilo1"><strong>JEFE: Julius 1 </strong><br>
          HP: 500, Exp: 200<br>
          <br>
          <strong>Esbirros: Oxens </strong><br>
HP: 410, Exp: 2<br>
          <br>
          Lo primero es eliminar a sus Oxen lo m&aacute;s r&aacute;pido posible. Usa tu mejor arma. Julius invocar&aacute; varios Oxen para atacar.<br>
          Tambi&eacute;n a veces se queda quieto y te tira una Blaze Bomb que te impide atacar y tirar magia por un rato. <br>
          A veces se poner una barrera m&aacute;gica que no permite ataques de Magia, pero si f&iacute;sicos. <br>
          Cuando puedas ac�rcate a Julius y dale con la Lanza
          u Espada. Tambi&eacute;n cualquier magia funciona bien con este jefe.<br>
          <br>
          </p>
      </div></td>
    </tr>
    <tr>
      <td><img src="imagenes/jefe_julius2.JPG" width="280" height="225"></td>
      <td><div align="left"><span class="Estilo1"><strong>JEFE: Julius 2 </strong><br>
  HP: 700, Exp: 250<br>
  <br>
  
No es tan dif�cil pero tiene m�s HP. Comienza proyectando m�ltiples 
		im�genes de &eacute;l, pero solo una es la correcta. Adivina para golpearlo con la Espada o Lanza. Luego si una flamas lo rodean, debes correr ya que tirar&aacute; un gran poder de fuego. Si las flamas son rojas, usar&aacute; Lava Wave. Si lo tocas te dejar&aacute; en estado Petrificado o estado FlameMan.<br>
  Si lo rodea una flama azul, usar&aacute; Ice Storm, si lo tocas mientras est&aacute; as&iacute;, te deja en estado de Silencio o SnowMan. Luego tirar&aacute; un remolino que te persigue, debes correr. <br>
  Cuando le quede poca HP, lo rodear&aacute; una flama p&uacute;rpura y har&aacute; el poder de Galaxy Drain. Si lo tocas te quitar&aacute; la mitad de tu HP. Tambi&eacute;n te puede absorber la mitad de tu HP y este ataque no puede ser bloqueado. <br>
  		As� que lo
  mejor es atacarlo a penas puedas, con tu mejores armas. <br>
  <br>
  Al eliminarlo Julius extrae todo el Poder Mana que hay en el templo y se convierte en un demonio, &iexcl;a pelear la &uacute;ltima batalla!</span></div></td>
    </tr>
    <tr>
      <td><img src="imagenes/jefe_julius3.JPG" width="280" height="225"></td>
      <td><span class="Estilo1"><strong>JEFE: Julius 3 </strong><br>
HP: 1500, Exp: 0 <br>
<br> 
El &uacute;ltimo Julius es el m&aacute;s dif�cil. Tiene m&aacute;s HP que los 2 anteriores juntos.<br>
Generalmente parte con su  Ataque Continuo, corre ya que son muchas bolas de corto 
		alcance que te da&ntilde;an y envenenan.<br>
Cuando se coloque escudo azul, anula ataques f&iacute;sicos, si es verde anula la ataques de magia.<br>
		At�calo mientras esta cambiando de escudo en el aire. <br>
		Cuando le quites algo de HP se mover&aacute; m&aacute;s r&aacute;pido y har&aacute; su Charge attack, donde se tira horizontalmente por la pantalla, y puede 
		causarte estado Sue&ntilde;o, por lo que debes estar atento a correr 
		arriba o abajo para esquivarlo. <br>
		Luego de este ataque tambi�n puede usar Gravity Strike, que te causa Sue&ntilde;o, solo 
		movi�ndote r&aacute;pido lo esquivas.<br>
Cuando su ataque est&eacute; bajo, usar&aacute; Meteo attack que no puede ser esquivado y quita hasta un 71% de tu HP.<br>
Recomiendo usar  una arma r&aacute;pida como la Lanza o Espada en nivel 99 y golpearlo desde el centro hacia arriba cuando puedas. <br>
<br>
<br>
		Si lo eliminas lo �nico que te puedo decir es �Felicitaciones, has 
		terminado el juego!
<br> 
</span></td>
    </tr>
  </table>
  <p align="left" class="Estilo1">    
    <br>
  </p>
  <strong>
  <p align="left" class="Estilo2"><a name="#15">15. Mapa del Mundo</a> <a href="#contenido"><img src="imagenes/subir.jpg" width="15" height="15" border="0"></a></p>
 </strong>
 <p align="left" class="Estilo1">El mejor mapa del mundo de <b>Sword of Mana</b> que encontr&eacute; en la Web, cortes&iacute;a de www.fantasyanime.com.<strong><br>
    <img src="imagenes/swordofmana_worldmap.gif" width="664" height="543">  </strong>
 
   
  </p>
 <p align="left" class="Estilo2">&nbsp;</p>
  <p align="left" class="Estilo2"><strong><a name="#16">16. Items</a> <a href="#contenido"><img src="imagenes/subir.jpg" width="15" height="15" border="0"></a></strong></p>
  <table width="30%" border="0">
    <tr bgcolor="#99CCFF" class="Estilo1">
      <td colspan="2"><div align="center"> <strong>Objetos Curativos</strong></div></td>
    </tr>
    <tr class="Estilo1">
      <td width="32%" align="left"><p align="left">Caramelo</p>        </td>
      <td width="68%" align="left">Recupera 30 HP. </td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td align="left"><p align="left">Gofre</p></td>
      <td align="left">Recupera 80 HP.</td>
    </tr>
    <tr class="Estilo1">
      <td align="left"><div align="left"></div>
          <div align="left">Honey Elixir</div></td>
      <td align="left">Recupera toda la HP. </td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td bgcolor="#DCEBEF" align="left"><p align="left">Nuez M&aacute;gica </p></td>
      <td align="left">Recupera 80 MP.</td>
    </tr>
    <tr class="Estilo1">
      <td align="left"><div align="left"></div>
          <div align="left">Vegeant&iacute;doto</div></td>
      <td align="left">Recupera de Veneno. </td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td align="left"><div align="left"></div>
          <div align="left">Hierba Estelar </div></td>
      <td align="left">Anula los  efectos f&iacute;sicos. </td>
    </tr>
    <tr class="Estilo1">
      <td align="left">C&aacute;liz Ang&eacute;lico</td>
      <td align="left">Revive a un aliado ca&iacute;do. </td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td align="left"><div align="left">Gunmirama</div>        </td>
      <td align="left">Llena al m&aacute;ximo barra de Golpe Mortal. </td>
    </tr>
  </table>
  <br>
  <table width="40%" border="0">
    <tr bgcolor="#99CCFF" class="Estilo1">
      <td colspan="2"><div align="center"> <strong>Objetos Misteriosos </strong></div></td>
    </tr>
    <tr class="Estilo1">
      <td width="27%" align="left"><p align="left">Miniaturizador</p></td>
      <td width="73%" align="left">Te achica por un rato, permiti�ndote llegar a zonas secretas. </td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td align="left"><p align="left">Piedra de Tono </p></td>
      <td align="left">Atrae enemigos que usan el o&iacute;do. </td>
    </tr>
    <tr class="Estilo1">
      <td align="left"><div align="left">Flor Fragante </div>
      </td>
      <td align="left">Atrae enemigos que usan el olfato. </td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td bgcolor="#DCEBEF" align="left"><p align="left">Ciza&ntilde;a Ocular </p></td>
      <td align="left">Atrae enemigos que usan la vista. </td>
    </tr>
    </table>
  <br>
  <table width="30%" border="0">
    <tr bgcolor="#99CCFF" class="Estilo1">
      <td colspan="2"><div align="center"> <strong>Monedas</strong></div></td>
    </tr>
    <tr class="Estilo1">
      <td width="37%" align="left"><p align="left">Moneda Luz</p></td>
      <td width="60%" align="left">Usa Luz Curativa.</td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td height="16" align="left"><p align="left">Moneda Aire</p></td>
      <td height="16" align="left">Causa Prisa</td>
    </tr>
    <tr class="Estilo1">
      <td align="left"><div align="left"></div>
          <div align="left">Moneda Madera</div></td>
      <td align="left">Causa Escudo Ps�quico</td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td bgcolor="#DCEBEF" align="left"><p align="left">Moneda Agua</p></td>
      <td align="left">Causa efecto Burbuja</td>
    </tr>
    <tr class="Estilo1">
      <td align="left"><div align="left"></div>
          <div align="left">Moneda Fuego</div></td>
      <td align="left">Causa efecto de Animo</td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td align="left">
		<p align="left">Moneda Oscura</td>
      <td align="left">
		<p align="left">Causa efecto Silueta (Invisible)</td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td bgcolor="#FFFFFF" align="left">Moneda Tierra</td>
      <td bgcolor="#FFFFFF" align="left">
		Causa efecto Defensa</td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td align="left"><div align="left">Moneda Lunar</div></td>
      <td align="left">
		<p align="left">Causa efecto Morfomoguuri</td>
    </tr>
  </table>
  <br>
  <table width="30%" border="0">
    <tr bgcolor="#99CCFF" class="Estilo1">
      <td colspan="2"><div align="center"> <strong>&Iacute;conos Espirituales</strong></div></td>
    </tr>
    <tr class="Estilo1">
      <td width="37%" align="left"><p align="left">Icono Astro</p></td>
      <td width="60%" align="left">Inflinge estado Micro. </td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td align="left"><p align="left">Icono Ondina</p></td>
      <td align="left">Inflinge estado Helado. </td>
    </tr>
    <tr class="Estilo1">
      <td align="left"><div align="left">Icono Umbra</div></td>
      <td align="left">Inflinge estado Ceguera. </td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td bgcolor="#DCEBEF" align="left"><p align="left">Icono L�mina</p></td>
      <td align="left">Inflinge estado Cupido. </td>
    </tr>
    <tr class="Estilo1">
      <td align="left">
          <div align="left">Icono Duende</div></td>
      <td align="left">Inflinge estado Petrificar. </td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td align="left">Icono C�firo</td>
      <td align="left">Inflinge estado Paralizar. </td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td align="left" bgcolor="#FFFFFF">Icono Dr�ada</td>
      <td align="left" bgcolor="#FFFFFF">Inflinge estado Sue�o. </td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td align="left"><div align="left">Icono Salamandra</div>
          <div align="left"></div></td>
      <td align="left">Inflinge estado FireMan. </td>
    </tr>
  </table>
  <br>
  <table width="40%" border="0">
    <tr bgcolor="#99CCFF" class="Estilo1">
      <td colspan="2"><div align="center"> <strong>Objetos de Evento</strong></div></td>
    </tr>
    <tr class="Estilo1">
      <td width="31%"><p align="left">Escencia de Cacto</p></td>
      <td width="69%">Pl&aacute;ntala para crear una Casa Cacto. </td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td><p align="left">Gusano Bubu </p></td>
      <td>D�sela a viejo del Inn de Ishe </td>
    </tr>
    <tr class="Estilo1">
      <td><div align="left"></div>
          <div align="left">Cola Asada </div></td>
      <td>Rabo de trit�n asado.</td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td bgcolor="#DCEBEF"><p align="left">Miel</p></td>
      <td>Delicioso n�ctar.</td>
    </tr>
    <tr class="Estilo1">
      <td><div align="left"></div>
          <div align="left">Espada Centella </div></td>
      <td>Una reluciente espada dorada.</td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td><div align="left">LLave Dorada </div>
          <div align="left"></div></td>
      <td>Una llave dorada.</td>
    </tr>
    <tr class="Estilo1">
      <td>Geoda de Luz </td>
      <td>Piedra que apresa un esp�ritu de Luz.</td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td>Espada Mana</td>
      <td>Legendaria Espada Mana.</td>
    </tr>
  </table>
  <br>
  <br>
  <table width="30%" border="0">
    <tr bgcolor="#99CCFF" class="Estilo1">
      <td colspan="2"><div align="center"> <strong>Semillas</strong></div></td>
    </tr>
    <tr class="Estilo1">
      <td width="50%"><p align="left">Semilla Redonda </p></td>
      <td width="50%">Una semilla. </td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td><p align="left">Semilla Grande </p></td>
      <td>Una semilla. </td>
    </tr>
    <tr class="Estilo1">
      <td><div align="left"></div>
          <div align="left">Semilla Plana </div></td>
      <td>Una semilla. </td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td bgcolor="#DCEBEF"><p align="left">Semilla Torcida </p></td>
      <td>Una semilla. </td>
    </tr>
    <tr class="Estilo1">
      <td><div align="left">Semilla Larga </div>
          <div align="left"></div></td>
      <td>Una semilla. </td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td><div align="left"></div>
          <div align="left">Semilla Fina </div></td>
      <td>Una semilla. </td>
    </tr>
    <tr class="Estilo1">
      <td><div align="left">Semilla Peque&ntilde;a </div>
          <div align="left"></div></td>
      <td>Una semilla. </td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td><div align="left"></div>
          <div align="left">Semilla Puntiaguda</div></td>
      <td>Una semilla. </td>
    </tr>
    </table>
  <br>
  <br>
  <table width="30%" border="0">
    <tr bgcolor="#99CCFF" class="Estilo1">
      <td colspan="2"><div align="center"> <strong>Frutas</strong></div></td>
    </tr>
    <tr class="Estilo1">
      <td width="50%"><p align="left">Campanuvas</p></td>
      <td width="50%">Una fruta. </td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td><p align="left">Calcemanzanas</p></td>
      <td>Una fruta. </td>
    </tr>
    <tr class="Estilo1">
      <td><div align="left">Perrocot&oacute;n</div>
          <div align="left"></div></td>
      <td>Una fruta. </td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td bgcolor="#DCEBEF"><p align="left">Citrilamar</p></td>
      <td>Una fruta. </td>
    </tr>
    </table>
  <br>
  <br>
  <table width="30%" border="0">
    <tr bgcolor="#99CCFF" class="Estilo1">
      <td colspan="2"><div align="center"> <strong>Verduras</strong></div></td>
    </tr>
    <tr class="Estilo1">
      <td width="50%"><p align="left">Zanahoriaspina</p></td>
      <td width="50%">Una verdura. </td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td><p align="left">Mascaratata</p></td>
      <td>Una verdura. </td>
    </tr>
    <tr class="Estilo1">
      <td><div align="left"></div>
          <div align="left">Lisvainas</div></td>
      <td>Una verdura. </td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td bgcolor="#DCEBEF"><p align="left">Orcaplanta</p></td>
      <td>Una verdura. </td>
    </tr>
    <tr class="Estilo1">
      <td><div align="left"></div>
          <div align="left">Calabaza</div></td>
      <td>Una verdura. </td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td><div align="left"></div>
          <div align="left">Mielebolla</div></td>
      <td>Una verdura. </td>
    </tr>
  </table>
  <p align="left" class="Estilo1"><br> 
  </p>
  <p align="left" class="Estilo2"><strong><a name="#17">17. Magias</a> <a href="#contenido"><img src="imagenes/subir.jpg" width="15" height="15" border="0"></a></strong></p>
  <p align="left" class="Estilo1">En tu viaje te encontrar&aacute;s con 8 Esp&iacute;ritus Elementales:  
	<br>
&nbsp;</p>
  <div align="left">
  <table border="0" width="55%" id="table1">
	<tr  class="Estilo1">
		<td width="57" align="center">
		<img border="0" src="imagenes/1lumina.JPG" width="31" height="29"></td>
		<td bgcolor="#DCEBEF"><b>L�mina (Wisp)<br>
	</b><br>
    HealingLight: 3 MP, te da un poco de energ�a y a tu compa�ero si esta cerca.<br>
	Wisp: 6 MP, magia blanca que causa da�o a enemigos, puede causar estado 
	Cupido.<br>
	Sword Wisp: 6 MP, pilar de magia que causa gran da�o si golpea de lleno al 
	enemigo</td>
	</tr>
	<tr class="Estilo1">
		<td width="57" align="center">
		<img border="0" src="imagenes/2duende.JPG" width="31" height="29"></td>
		<td bgcolor="#DCEBEF"><b>Duende (Gnome)<br>
	<br>
	</b>D-Fence: 3 MP, aumenta tu defensa.<br>
	Gnome: 6 MP, ataque de tierra. Puede causar Petrificaci�n.<br>
	Sword Gnome: una punta sale del piso y golpea desde abajo.</td>
	</tr>
	<tr class="Estilo1">
		<td width="57" align="center">
		<img border="0" src="imagenes/3cefiro.JPG" width="29" height="28"></td>
		<td bgcolor="#DCEBEF">
    <b>C&eacute;firo (Jinn)</b><br>
    <br>
	SeedUp: 3 MP, aumenta tu velocidad.<br>
	Jinn: 6 MP, ataque con poder del viento que puede causar Par�lisis.<br>
	Sword Jinn: 6 MP, un pilar de viento golpea a los enemigos.</td>
	</tr>
	<tr class="Estilo1">
		<td width="57" align="center" height="69">
		<img border="0" src="imagenes/4driada.JPG" width="28" height="29"></td>
		<td bgcolor="#DCEBEF" height="69">
    <b>Dr&iacute;ada (Dryad)<br>
	<br>
	</b>PsychicShield: 3 MP, aumenta la defensa m�gica.<br>
	Dryad: 6 MP, un poderoso golpe de magia tipo planta, puede causar Sue�o.<br>
	Sword Dryad: Varia ramas salen del suelo y golpean a los enemigos.</td>
	</tr>
	<tr class="Estilo1">
		<td width="57" align="center">
		<img border="0" src="imagenes/5ondina.JPG" width="29" height="26"></td>
		<td bgcolor="#DCEBEF"><b>Ondina (Undine)<br>
	<br>
	</b>BubbleBoat: 3 MP, flotas un poco sobre los enemigos.<br>
	Undine: 6 MP, un poderoso rayo de agua que puede causar estado Helado.<br>
	Sword Undine: salpica agua a los enemigos de alrededor.<br>
    &nbsp;</td>
	</tr>
	<tr class="Estilo1">
		<td width="57" align="center">
		<img border="0" src="imagenes/6salamandra.JPG" width="28" height="26"></td>
		<td bgcolor="#DCEBEF">
    <b>Salamandra (Salamander)</b><br>
	<br>
	PowerUP: 3 MP, incrementa el poder ofensivo.<br>
	Salamander: 6 MP, ataca con fuego. Puede causar estado Quemado.<br>
	Sword Salamander: 6 MP, rayos de fuego golpean al enemigo r�pidamente.</td>
	</tr>
	<tr class="Estilo1">
		<td width="57" align="center">
		<img border="0" src="imagenes/7astro.JPG" width="28" height="27"></td>
		<td bgcolor="#DCEBEF">
    <b>Astro (Luna)<br>
    <br>
	</b>Mooglemorphosis: 3 MP, te cambia de forma a un Moogle.<br>
    Luna: 6 MP, ataca pon el poder de la Luna. Puede causar Cambio de forma.<br>
	Sword Luna: muchos pilares de energ�a golpean varias veces.</td>
	</tr>
	<tr class="Estilo1">

		<td width="57" align="center">
		<img border="0" src="imagenes/8umbra.JPG" width="30" height="29"></td>
		<td bgcolor="#DCEBEF">
  <p align="left" class="Estilo1">
	<b>Umbra (Shade)<br>
	<br>
	</b>Silhouette: 3 MP, te hace invisible.<br>
	Shade: 6 MP, ataca con golpe oscuro. Puede causar Oscuridad.<br>
	Sword Shade: 6 MP, una bola de energ�a negativa golpea a los enemigos.</p></td>
	</tr>
	</table>
  </div>
 <p align="left" class="Estilo2">&nbsp;</p>
	<p align="left" class="Estilo2"><strong><a name="#18">18. Quests</a> <a href="#contenido"><img src="imagenes/subir.jpg" width="15" height="15" border="0"></a></strong></p>
	
  <p align="left" class="Estilo1"><strong>1) Entregar 15 folletos en  la Ciudad de Wendell<br>
    Ganas: </strong>Lucres<strong>
    <br>
  </strong>En la tienda del Sureste de Wendel, un tipo lamado Dhomi te pide que repartas 15 folletos por el pueblo:  </p>
  <p align="left" class="Estilo1"><strong> </strong>En la Parte Sur: <br>
  1) Mujer &#8211; En el negocio donde obtienes te piden este Quest, a la derecha del viejo que te da los folletos (A cualquier hora) <br>
  2) Hombre &#8211; Por afuera (A cualquier hora) <br>
  3) Mujer &#8211; Por afuera (Ma&ntilde;ana-Tarde) <br>
  4) Hombre sospechoso &#8211; Afuera caminando cerca del negocio (Noche) <br>
  5) Abuelo &#8211; Afuera, al centro de la ciudad (A cualquier hora) <br>
  6) Viajero &#8211; Afuera, cerca del centro (Noche) <br>
  7) Viejo que no trabaja &#8211; Casa de Wendel, segundo piso (A cualquier hora) <br>
  <br>
  En la parte Norte:<br>
  8) Chica &#8211; Afuera, cerca de la herrer&iacute;a (A cualquier hora) <br>
  9) Hombre &#8211; Afuera cerca de fuente (Ma&ntilde;ana-Noche) <br>
  10) Hombre - Afuera cerca de fuente (A cualquier hora) <br>
  11) Doncella - Afuera cerca de fuente (A cualquier hora) <br>
  12) Abuelo - Afuera cerca de fuente (Tardes). Es el mas dif&iacute;cil de pillar, recomiendo salir, darte una vuelta y cuando sea Tarde (el sol hace que se vea casi todo anaranjado) 
  vuelves con la Cuerda M&aacute;gica. <br>
  13) Enano Hijo de Herrero - herrer&iacute;a (A cualquier hora) <br>
  14) Joven n&oacute;mada &#8211; En la posada (A cualquier hora) <br>
  15) Vagabundo &#8211; Posada, pieza de arriba a la derecha (Noche) <br>
  <br>
  <strong>2) Ciudad de Wendel. Espada Centellante</strong><br>
  <strong>Ganas: </strong>  <br>
  Un tipo llamado Vega del Inn te pide una Espada Centellante, se consigue matando a los Mushboom (callampas) a la salida de Wendel. Te la entregan al azar. El tipo te dar&aacute; 100 Lucres por la espada. NOTA: Este quest solo puede realizarse antes de abordar la nave (Hero) o despu&egrave;s de encontrarte con Cibba (Heroina). <br>
  <br>
  <strong>3) Ishe. Pluma Cancun</strong><br>
  <strong>Ganas: </strong>Actualizar Esp&iacute;ritus<br>
En la casa del noroeste,  primero un chico llamado Abdul te  pregunta como se hace para meditar, que responde primero A+L, luego la segunda no importa. Te dar&aacute; <strong>Pluma Cancun</strong>. 
Luego all&iacute; mismo hay otro chico llamado Messier que te ofrece 50 Lucres por la Pluma, sile que no. Habla de nuevo con el tipo que te dio la pluma y te 
la pedir&aacute;, dile que Si. Ahora habla de nuevo con Messier y te dar&aacute; Oro Dudbear (Dudbears Gold) util para <strong>mejorar un  Esp&iacute;rit&uacute;. </strong>Alli mismo en el Inn, en el segundo piso, est&aacute; <strong>Asaad</strong>, ella te actualiza un Esp&iacute;ritu por 3 Oro Dudbear. Si quieres actualizar 
el esp&iacute;ritu de Luna, debes ir en un d&iacute;a de la semana que corresponda a Luna (presiona Start para ver que en d&iacute;a de esp&iacute;ritu est&aacute;s). </p>
  <p align="left" class="Estilo1"><strong>4) Ishe.  </strong><strong>Carta de los 7 Juicios</strong><br>
    <strong>Ganas:</strong> Oro Dudbear    <br> 
Un anciano en la tienda del noroeste del pueblo, te pide ver esta carta antes que &eacute;l muera. Necesitas tener el item Summons Cards y te dar&aacute; un Oro Dudbear.

<br><br>
<strong>5) Ishe. Gusanos Bubu </strong><br>
<strong>Ganas:</strong> Lucres <br>
En el Inn te piden gusanos. Mata enemigos del Desierto Cristalino y coge el cofre que dejan (10% probabilidad). Dependiendo del largo del gusano te dar&aacute; Lucres, eso si, debe ser  mayor a 50 cms.
Para
pasar este Evento (dejarlo Clear) debes darle algo mayor que 50 cms. <br>
<br>
<strong>6) Historia de Detectives</strong><br>
<strong>Ganas: </strong>item Miniaturizador (Tiny Tapper) y 200 Lucres. <br> 
Ve a Wendel de Noche, ve a la Catedral al norte, all&iacute; Inger te dir&aacute; que su amuleto fue  robado. Habla con ella antes de irte y dile que SI encontrar&aacute;s su amuleto. Ahora tu tarea es capturar al ladr&oacute;n. Ve a la pantalla donde esta el Inn y la Herrer&iacute;a, el ladr&oacute;n estar&aacute; abajo, entre una caja y un tejado, tiene el pelo azul, habla con &eacute;l y correr&aacute;. Ve a pillarlo de nuevo. <br>
Baja la parte sur del pueblo y ve a la tienda principal  de items, al sureste del pueblo (Wendel-Ultramarino), el ladr&oacute;n estar&aacute; a la derecha, habla de nuevo con &eacute;l y huir&aacute;. <br>
Esta parte ES IMPORTANTE, ve al INN y qu&eacute;date a dormir la noche, al despertar ser&aacute; de ma&ntilde;ana, sal de la posaba y ve a la izquierda y justo a la izquierda de la Herrer&iacute;a estar&aacute; semi escondido el ladr&oacute;n, habla con &eacute;l y esta vez te dar&aacute; el amuleto. Sube a la catedral y Inger estar&aacute; all&iacute;, habla con ella y te preguntar&aacute; algo, responde cualquier cosa y te dar&aacute; un Miniaturizador m&aacute;s 200 Lucres. Ese Item puede encogerte, esto te ayuda a encontrar otros item secretos como el magn&iacute;fico amuleto <strong>Brownie Ring</strong> (Ver secretos).<br> 
<br>
<strong>7) Bolsa de Sangre  </strong><br> 
De noche en la Villa Topple, en el Inn, en el 2do piso
habla con Blassie el III, que te pedir&aacute; bolsas de Sangre. Estas se las puedes robar a los Batmos con 10% de probabilidad de conseguir una al abrir el Cofre que te dejan cuando los eliminas. Con 10 Bolsas te dar&aacute; 1000 Lucres y Oro Dublear. Con 5 te dar&aacute; 500 Lucres. Puedes usar este m&eacute;todo para conseguir muchos Oro Dublear. El quest queda ok cuando le das al menos 1 Bolsa de Sangre. <br><br>
	NOTA: Son 50 los Quest, aqu� solo puse los que yo obtuve a la r�pida.</p>
  <p align="left" class="Estilo2">&nbsp;</p>
	<p align="left" class="Estilo2"><strong><a name="#19">19. Secretos </a><a href="#contenido"><img src="imagenes/subir.jpg" width="15" height="15" border="0"></a></strong></p>
  <table width="100%"  border="0">
    <tr>
      <td width="29%"><img src="imagenes/truco.JPG" width="250" height="190"><span class="Estilo1"><br>
		En Monte Ilusia hay estos enemigos que solo mueren con ciertas Magias, Flechas o Mayal. Faciles para subir niveles de magia o espada, hacha, etc. <br>
      Lo mejor es que hay tambi&eacute;n en este lugar un Stone Seal, esos volcanes met&aacute;licos que solo mueren con un tipo de magia esp&eacute;fica, con ellos puedes subir a nivel 99 tus Flechas o Mayal. </span></td>
      <td width="71%"><div align="left">
          <p class="Estilo1"><strong>1. Como obtener un Skill Level de armas al nivel 99.</strong><br>
            <br>
La clave esta en golpear a un enemigo pero que no le hagas da&ntilde;o, que te diga 0 al golpearlo. Esto te dar&aacute; igualmente 1 Skill Level a esa arma que usas. Asi que si la golpeas una hora subiras unos 10 a 15 niveles, si, el proceso es largo. Gracias al emulador VisualBoy Advance 1.8b3 (el que use para hacer esta guia) puedes acelerar el proceso y dejar automatcamente programado que el personaje golpee. Para esto debes hacer esto:<br>
-Tu amigo/a (si tienes) dejalo morir, ya lo recuperaras luego de ganar niveles.<br>
-<strong>B&uacute;scate un enemigo</strong> facil de golpear, que no se mueva ojal&aacute;. Los Metaballs y Stone Seals son buenos. Yo use un Cerezacieno en la entrada de Monte Ilusia, es como una babosa roja.<br>
-Acorrala a tu enemigo si es que se mueve y golpealo de forma que no lo dejes moverse (como se ve en la imagen), le quitar&aacute;s 0, esa es la idea.<br>
-Con el emulador presiona <strong>ALT+1</strong>, esto genera un <strong>AUTOFIRE</strong> para el bot&oacute;n A (como un bot&oacute;n turbo), entonces tu personaje seguir&aacute; golpeando AUTOMATICAMENTE al enemigo.<br>
-Ve a <strong>Options-&gt;Emulator-&gt; desmarca &quot;Synchronize&quot;</strong> luego 
ve a <strong><br>
Options-&gt;Frame Skip-&gt;Throttle-&gt;Marca &quot;No Throttle&quot;</strong>, esto generar&aacute; una c&aacute;mara r&aacute;pida. Andar&aacute; el juego mucho mas r&aacute;pido (a mi me anda al entre 225%-300%, lo normal es que ande al 100%) y el tiempo que demoras en subir niveles es de 2 a 3 veces mas r&aacute;pido de lo normal. En media hora subir&aacute;s 25 a 35 niveles dependiendo de los golpes que le des a tu enemigo. <br>
-Ve a <strong>Options-&gt;Emulator-&gt;saca el tick &quot;Pause when inactive Windows&quot;</strong>. Con esto  puedes dejar la ventana del emulador corriendo solita de fondo, la mueves para un lado, y puedes hacer otra cosa mientras el juego solo corre. Puedes ver una peli por ejemplo :P <br>
Una vez terminada de subir de nivel, debes nuevamente marcar el <strong>Synchronize</strong>.<br> 
<br>
<strong>NOTA: </strong>Debes mirar de vez en cuando como va la HP de tu personaje, ya que a veces hay enemigos que haces ataques sorpresas y cuando menos crees, te puedes quitar un poco de energia, pero si lo dejas unas 2 horas y no lo has mirado, quizas te hayan matado. Asi que preocupate de mirar a tu personaje como esta su HP, si te queda poca, presiona en el emulador ALT+1 y se quitar&aacute; el Autofire, entonces entra al menu y con la magia L&uacute;mina o algun objeto curativo, date HP, luego sigue con el proceso.<br>
          </p>
      </div></td>
      <td width="29%"><img src="imagenes/truco_status.JPG" width="250" height="190"><span class="Estilo1"><br>
				Este es mi estatus 99 en Espada y Hacha. La obtuve con los Cerecacieno de 
		Monte Ilusia y nivel 99 flechas y Mayal con un Stone Seal. Ahora haces 99 puntos adicionales de da&ntilde;o. Ning&uacute;n jefe se resistir&aacute; a tus golpes :P </span></td>
    </tr>
  </table>
  <p align="left" class="Estilo1">&nbsp;</p>
  <table width="100%"  border="0">
    <tr>
      <td width="26%"><img src="imagenes/Brownie_ring1.JPG" width="280" height="225"></td>
      <td width="56%"><div align="left">
          <p class="Estilo1"> <strong>2. Como obtener Anillo <strong>Brownie (</strong>Brownie Ring) en R&iacute;o Subterraneo<br>
            <br>
          </strong>Partamos que es un Quest secreto y que la mayoria de la gente no lo hace. Debes tener el Item <strong>Miniaturizador/Tiny Tapper</strong> (item que te encoge) ver Secci&oacute;n de <a href="#18">Quest</a> #6 para saber como obtener este item. <br>
Ve al primer hoyo que encuentres en la cueva (imagen izquierda) usa el item <strong>Miniaturizador </strong>para encogerte (Men&iacute; Items-&gt;Obj. Misteriosos), adentro hay una semilla, luego enc&oacute;gete de nuevo, entra por la cueva del norte. Aqu&iacute; viene la parte dificil, debes ir arriba y un poco a la izquierda, luego solo haz arriba en el pad para esperar que pase el efecto de encogimiento, luego presiona salto y arriba para entrar a una zona secreta. Adentro estar&aacute; un tal <strong>Kanuma Brown</strong> lider del Clan Brown y te ofrece 3 items: <strong>Mermelada Duelle, Sotherbee C&iacute;trico, Anillo Brownie. </strong> Solo te dejar&aacute; escojer este &uacute;ltimo. Equ&iacute;palo ya que  te dar&aacute; <span class="Estilo9">+55</span> a Poder, Defensa, Sabidur&iacute;a, Agilidad e Inteligencia. Este item el mejor accesorio del juego. <br>
          Luego regresa al sur, alli a la derecha arriba hay un cofre, luego baja y sal por el sur. <br>
          <br><br><br></p>
      </div></td>
      <td width="18%"><img src="imagenes/brownie_ring_correcto.JPG" width="280" height="225"></td>
    </tr>
  </table>
  
  <p align="left" class="Estilo1"></p>
</div>
  <p align="left" class="Estilo1"><strong>3. Obtener S�per Armas<br>
	</strong> <br>
  <span class="Estilo1">1. Cristal Ore<br>
	Para esto, debes eliminar a un enemigo <strong>1000</strong> veces, para que aparezca negro y mucho mas 
	dif�cil. Luego debes eliminar a esos enemigos negros para que te den el cofre, 
	�brelo y si tienes suerte (1 de 32) saldr&aacute; un  Cristal Ore. <br>
  <br>
  <img src="imagenes/negros.JPG" width="280" height="225"> <br>
  En la imagen se ven en la imagen Terrabasiliscos Negros, luego de haber eliminado m&aacute;s de 1000. Son mucho mas agresivos que los corrientes. <br>
  Necesitas 31 Cristal Ore.<br>
  <br>
  2. Debes tener
  30 Spade Basil, que salen al plantar semillas peque&ntilde;as o torcidas en le 
	d�a de Driada.<br>
  <br>
  3.
  10600 Lucres.<br>
  <br>
  Una vez que tengas esto, ve a ver a enano y Forja  el Arma que desees con un  cristal.
  Los 30 Cristales que sobren, t�mplalos en 30 Basils a 20 lucres cada uno, usando todos los slots LMT. El arma estar&aacute; con todos los niveles al m&aacute;ximo y todos los elementos incrustados.<br>
  Finalmente puedes tener estas armas:</span></p>
<table border="0" width="100%" id="table3">
	<tr>
		<td><table width="65%" border="0" align="left" id="table2">
    <tr bgcolor="#99CCFF" class="Estilo1">
      <td width="20%"><p align="left">Espada Cristal </p></td>
      <td width="20%">Vara Cristal </td>
      <td width="20%">Manopla Cristal </td>
      <td width="20%">Mayal Cristal </td>
      <td width="20%">Falce Cristal </td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td width="20%"><p align="left">Elemento: Todos </p></td>
      <td width="20%"><p align="left">Elemento: Todos </p></td>
      <td width="20%"><p align="left">Elemento: Todos </p></td>
      <td width="20%"><p align="left">Elemento: Todos </p></td>
      <td width="20%"><p align="left">Elemento: Todos </p></td>
    </tr>
    <tr class="Estilo1">
      <td width="20%"><div align="left"></div>
          <div align="left">Poder: 100 </div></td>
      <td width="20%"><div align="left"></div>
          <div align="left">Poder: 65 </div></td>
      <td width="20%"><div align="left"></div>
          <div align="left">Poder: 70 </div></td>
      <td width="20%"><div align="left"></div>
          <div align="left">Poder: 80 </div></td>
      <td width="20%"><div align="left"></div>
          <div align="left">Poder: 75 </div></td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td bgcolor="#DCEBEF"><p align="left">Fuerza: 100 </p></td>
      <td width="20%"><p align="left">Fuerza: 65 </p></td>
      <td width="20%"><p align="left">Fuerza: 120 </p></td>
      <td width="20%"><p align="left">Fuerza: 80 </p></td>
      <td width="20%"><p align="left">Fuerza: 75 </p></td>
    </tr>
    <tr class="Estilo1">
      <td width="20%"><div align="left"></div>
          <div align="left">Certeza: 100 </div></td>
      <td width="20%"><div align="left"></div>
          <div align="left">Certeza: 65 </div></td>
      <td width="20%"><div align="left"></div>
          <div align="left">Certeza: 70 </div></td>
      <td width="20%"><div align="left"></div>
          <div align="left">Certeza: 80 </div></td>
      <td width="20%"><div align="left"></div>
          <div align="left">Certeza: 75 </div></td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td width="20%"><div align="left"></div>
          <div align="left">Elemento Poder: 80 </div></td>
      <td width="20%"><div align="left"></div>
          <div align="left">Elemento Poder: 100 </div></td>
      <td width="20%"><div align="left"></div>
          <div align="left">Elemento Poder: 65 </div></td>
      <td width="20%"><div align="left"></div>
          <div align="left">Elemento Poder: 70 </div></td>
      <td width="20%"><div align="left"></div>
          <div align="left">Elemento Poder: 65 </div></td>
    </tr>
    <tr class="Estilo1">
      <td width="20%">LMT: 0 </td>
      <td width="20%">LMT: 0 </td>
      <td width="20%">LMT: 0 </td>
      <td width="20%">LMT: 0 </td>
      <td width="20%">LMT: 0 </td>
    </tr>
  
    <tr bgcolor="#99CCFF" class="Estilo1">
      <td width="20%"><p align="left">Arco Cristal </p></td>
      <td width="20%">Lanza Cristal </td>
      <td width="20%">Hacha Cristal </td>
      <td width="20%">Mazo  Cristal </td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td width="20%"><p align="left">Elemento: Todos </p></td>
      <td width="20%"><p align="left">Elemento: Todos </p></td>
      <td width="20%"><p align="left">Elemento: Todos </p></td>
      <td width="20%"><p align="left">Elemento: Todos </p></td>
    </tr>
    <tr class="Estilo1">
      <td><div align="left"></div>
          <div align="left">Poder: 85 </div></td>
      <td><div align="left"></div>
          <div align="left">Poder: 90 </div></td>
      <td><div align="left"></div>
          <div align="left">Poder: 110 </div></td>
      <td><div align="left"></div>
          <div align="left">Poder: 120 </div></td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td bgcolor="#DCEBEF"><p align="left">Fuerza: 85 </p></td>
      <td><p align="left">Fuerza: 90 </p></td>
      <td><p align="left">Fuerza: 110 </p></td>
      <td><p align="left">Fuerza: 120 </p></td>
    </tr>
    <tr class="Estilo1">
      <td><div align="left"></div>
          <div align="left">Certeza: 120 </div></td>
      <td><div align="left"></div>
          <div align="left">Certeza: 90 </div></td>
      <td><div align="left"></div>
          <div align="left">Certeza: 110 </div></td>
      <td><div align="left"></div>
          <div align="left">Certeza: 120 </div></td>
    </tr>
    <tr bgcolor="#DCEBEF" class="Estilo1">
      <td><div align="left"></div>
          <div align="left">Elemento Poder: 75 </div></td>
      <td><div align="left"></div>
          <div align="left">Elemento Poder: 85 </div></td>
      <td><div align="left"></div>
          <div align="left">Elemento Poder: 85 </div></td>
      <td><div align="left"></div>
          <div align="left">Elemento Poder: 75 </div></td>
    </tr>
    <tr class="Estilo1">
      <td>LMT: 0 </td>
      <td>LMT: 0 </td>
      <td>LMT: 0 </td>
      <td>LMT: 0 </td>
    </tr>
  </table></td>
	</tr>
	<tr>
		<td><p align="left" class="Estilo1"><br>NOTA: esto no lo he comprobado ya que mat� 
	a 1000 enemigos pero me aburr� de abrir cofres sin que saliera un Cristal 
	Ore.</p></td>
	</tr>
</table>

 
<hr size="2">
<div align="left"><p class="Estilo1"><a href="../../guias_juegos.php">Volver</a></p></div>

<font color="#FFFFFF">
<?php 
include '../../disq.php';
?>
</font>


<div align="center"><?php include '../../pie.php';?></div>



</body>
</html>