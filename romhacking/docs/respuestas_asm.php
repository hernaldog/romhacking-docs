<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<title>La Web de Dark-N - ASM Online: Reglas.</title>
<link rel="stylesheet" type="text/css" href="estilo0.css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body bgcolor="#000000">

<div align="center">
  <center>

<table width="95%" height="96" border="1" cellpadding="0" cellspacing="0" bordercolor="#111111" id="AutoNumber1" style="border-collapse: collapse">
  <tr> 
    <td width="54%" height="1" bgcolor="#999999"><div align="center">
		<font size="5" color="#FFFF00">RESPUESTA de EJERCICIO</font></div>
      </td>
  </tr>
  <tr> 
    <td width="100%" height="62">
    
     <br>   
    <div align="center">
      <center>   
<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; background-color:#FFFFFF" bordercolor="#111111" width="91%" id="AutoNumber2" height="88">
  <tr>
                  <td width="90%" height="88">

<table> <tr><td width="90%">
<p align="left"><font color="#336699" size="2" face="Verdana, Arial, Helvetica, sans-serif"> 
                    &nbsp;
                    
                     
                    
<?php
if (isset($_GET['cod'])) {
if ($_GET['cod']=='fa1') 
$msg ='
1. LDA $1234  <br>
-> no se sabe que valor hay en $1234 por lo que no se sabe como satear A si en modo de 8 o 16 bits

 <br>
2. LDA #$458   <br>
-> Setear registro A a modo de 16 Bits para esto hay que RESET FLAG M de P.

 <br>
3. LDX $1   <br>
-> no se sabe que valor hay en $1 por lo que no se sabe como satear X si en modo de 8 o 16 bits

 <br>
4. STY #$ 85   <br>
-> Error, no existe tal instruccion, STY $85 si existe
                    	
';
                    	
elseif ($_GET['cod']=='fa2') 
$msg = '
1. Si quiero cargar en A el valor 35.  <br>
 SEP #$20	 <br>
 LDA #$35   	 <br>
2. Si quiero almacenar en X el valor que esta dentro de la direcci�n $1234.   <br>
   STY $1234     <br> 
3. Si quiero almacenar en Y el valor que esta dentro de $FF48FC.  <br>
   STY $FF48FC  
';
elseif ($_GET['cod']=='fa3') 
$msg = '
1. �Es posible hacer esto?  <br>
SEP #$20     ->A en modo 8 bits    <br>
LDA $1234	 <br> <br>

Si, ya que lo que hay en $1234 es un valor de 8 bits (siempre es asi) por lo que 
sacaremos un valor de 8 bits.   <br> <br>

2. �y esto?   <br>
LDA #$05      <br>
STA $811234   <br> <br>

Aqui da lo mismo lo que guardamos dentro de la direccion, el problema aqui es que estamos
metiendo algo en el banco 81 esta dentro de la ROM (READ ONLY MEMORY) 
solo se puede leer Y NO ESCRIBIR con sta, esto SALE en el Capitulo 1 y se supone que debias haberlo visto aunque sea :P
';
elseif ($_GET['cod']=='fa4') 
$msg = '
1. Que sucede con A cuando esta en modo de 8 bits 
y en modo de 16 bits para estos 2 casos : LDA $1234 y LDA #$1234  <br> <br>

Modo de 8 bits:  <br>
Para el caso de LDA $1234 no importa como esta A (si en modo de 8 o 16 bits)
ya que no se sabe si el valor a cargar dentro de $1234 es de 8 o 16 bits.  <br>

Para el caso de LDA #$1234 (un valor de 16 bits) hay un error, ya que A esta en modo
en 8 bits.  <br> <br>

Para modo de 16 bits:  <br>
Para LDA $1234. No importa ya que no se sabe lo que esta dentro de $1234. Carga el valor en A.  <br>
Para LDA #$1234 cargaria en A sin problemas ya que A esta seteado
a modo de 16 bit y el valor es de 16 bits.  <br> <br>

2. �Cual es el Largo de un banco (en bytes) y de un Offset? �Cuantas direcciones en TOTAL en la memoria podemos tener?  <br>
Banco = 1 byte  = 00-FF     = 255 bancos  <br>
Offset= 2 bytes = 0000-FFFF = 65535  offsets   <br>
Direccion total: 00:0000-FF:FFFF   <br> <br>

Entonces la cantidad de ditrecciones totales es 255 X 65535 = 16711425 direciones.
';
elseif ($_GET['cod']=='fa5') 
$msg = '
Hacer un trozo de c�digo que haga esto:  <br>

1. Quiero meter el valor 100 a la pila, luego meter 20 a $500 
y luego sacar lo de la pila y dejarlo finalmente en A.  <br> <br>
REP #$20    -> A en modo de 16 bits	 <br>
LDA #$0100		 <br>
PHA			 <br>
LDA #$0020		 <br>
STA $0500		 <br>
PLA 			 <br> <br>

2. Que saque de la direcci�n $556C un valor (siempre en las direcciones hay valores de 8 bits) y lo deje en X, que a ese valor le sume 4 y 
el resultado lo deje en $556D. (que los bancos no te importen)  <br> <br>
SEP #$10   -> X Y EN 8 BITS	 <br>
STX $556C 		 <br>
TXA	-> movemos de X a A (sale en Capitulo 2)	 <br>
CLC	-> Carry en 0	 <br>
ADC #$04	 <br>
STA $556D	 <br>
';
elseif ($_GET['cod']=='fa6') 
$msg = '
Detectar los 2 errores que hay en este trozo de c�digo:  <br>
Suponer que en $80AABB esta el valor 0x1234 y que este c�digo empieza a 
ejecutarse a partir de la instrucci�n  $01552C.  <br> <br>
SEP #$20 -> deja XYA en modo de 8 bits	 <br>
LDA #$02	 <br>
LDY $80AABB -> error 1: deber�a ser REP #$20 para que aguante un valor de 16 bits  <br>
PHK -> guardamos el banco 01	 <br>
PHA -> guardamos en pila 02	 <br>
STX #$A1F234 -> Error 2: esta instrucci�n no existe, no se puede "almacenar" en un valor
PLX 	-> x=02	 <br>
PLY 	-> y=01	 <br> <br>
�Como quedan A,X,Y al final?	 <br> <br>
A=0x02	 <br>
X=0x02	 <br>
Y=0x01
';
elseif ($_GET['cod']=='fa7') 
$msg = '
1. Al hacer REP #$30 lo que hago es:  <br> <br>
  <b>Resetear</b> (opciones: cargar/almacenar/setear/resetear) XYA (X/Y/A) para que se pueda <b>Cargar</b> (opciones: almacenar/cargar/setear/resetear)
  <b>Valores</b> (opciones: datos/valores/direciones/offsets) en modo de <b>16</b> (opciones: 8/16/24) bits.  <br> <br>

2. �Se podria deducir alguna "regla" a partir de esta sentencia?  <br>
SR[30axy-20a-10xy]  <br> <br>

SEP/REP #$30 es para A,X,Y 	 <br>
SEP/REP #$20 es solo para A	 <br>
SEP/REP #$10 es solo para XY	

';
elseif ($_GET['cod']=='me1') 
$msg = '1.  <br>

STX $10 <br>
STY $20 <br>
STA $30 <br>
LDA $10 <br>
CLC <br>
ADC $20 <br>
SEC <br>
SBC $30 <br>
ASL    ->x2 <br>
ASL    ->x4 <br>
ASL    ->x8 <br>
ASL    ->x16 <br>
STA $100    ->resultado guardado aqui <br>
PLA  <br>
STA $40     -> sacamos un valor de la pila y lo dejamos en $40 <br>
LDA $100 <br>
PHA    -> recuperamos nuestro valor y lo dejamos en la Pila <br>

';
elseif ($_GET['cod']=='me2') 
$msg = '2. <br>
PLA <br>
STA $10  -> 0C <br>
PLA <br>
STA $20 -> 0A <br>
PLA  <br>
STA $30  -> 04 <br>
LDA $10 <br>
PHA <br>
LDA $30 <br>
PHA <br>
LDA $20 <br>
PHA <br> <br>

PLA   -> saco 0A <br>
PLA   -> saco 04 <br>
PLA   -> saco 0C <br>
LDA $20 <br>
PHA	-> meto 0A <br>
LDA $10 -> A =0C <br>
STX $50 -> guardamos lo de X en 50 <br>
CLC <br>
ADC $50  -> A= 0C + lo que hay en $50 = 0C + X <br>
PHA <br>
LDA $30  -> metemos 04 a la pila <br>
PHA <br>
';
elseif ($_GET['cod']=='me3') 
$msg = '3. <br>
SEP #$30 <br>
LDA #$0B <br>
STA $4202 <br>
LDA #$0A <br>
STA $4203 <br>
LDA $4216 -> A GUARDA RESULTADO 1 (EN ESTE CASO DE 8 BITS) <br> 
             Y LO TIRAMOS A UNA DIRECION TEMP $10 <br>
STA $10 <br> <br>

SEP #$30 -> AHORA LO MISMO PARA LA SEGUNDA MULTIPLICACION <br>
LDA #$05 <br>
STA $4202 <br>
LDA #$04 <br>
STA $4203 <br>
LDA $4216 -> AHORA RESULTADO 2(QUE ES DE 8 BITS) LO GUARDAMOS EN $20 <br>
STA $20 <br> <br>

SEP #$30 <br>
LDA $10   -> AQUI ES EL DIVIDENDO (RESULTADO 1) <br>
STA $4204    <br>
LDA #$00  <br>
STA $4205   
LDA $20   -> AQUI ES EL DIVISOR (RESULTADO 2) <br>
STA $4206 <br>
LDA $4214   -> OBTENEMOS CUOCIENTE <br>
SEC  <br>
SBC #$03  <br>
TAY         -> SE LE RESTA 03 Y SE DEJA EN Y <br>
REP #$30 <br>
LDA $4216   -> OBTENEMOS RESTO  <br>
CLC <br>
ADC #$07 <br>
TAX         -> SE LE SUMA 07 Y SE DEJA EN X <br>
';
elseif ($_GET['cod']=='me4') 
$msg = '4 <br>
SEP #$30 <br>
LDA #$6A   -> DIVIDENDO  016A-> primero se mete el byte menor <br>
STA $4204  <br>
LDA #$01 <br>
STA $4205    <br>
LDA #$05   -> AQUI ES EL DIVISOR 5 <br>
STA $4206 <br>
LDA $4214   -> OBTENEMOS CUOCIENTE Y LO ALMACENAMOS TEMPORALMENTE EN $10 <br>
STA $10 <br>
LDA $4216   -> OBTENEMOS RESTO Y LE SUMAMOS EL CUOC QUE ESTA EN $10 <br>
CLC <br>
ADC $10 <br> <br>

STA $4202   -> METEMOS EL PRIMER VALOR DE LA MULTIPLICACION <br>
LDA #$03    -> LUEGO CARGAMOS EL SEGUNDO <br>
STA $4203  <br>
REP #$30    -> NOS ASEGURAMOS Y HACEMOS QUE A SEA DE 16 BITS <br>
LDA $4216   -> RESULTADO DE LA MULTIPLICACION <br>
SEC <br>
SBC #$08    -> LE RESTAMOS 08 <br>
PHA         -> LO METIMOS A LA PILA FINALMENTE <br>

';
elseif ($_GET['cod']=='me5') 
$msg = '5. <br>
SEP #$30 <br>
TXA <br>
CLC
ADC #$01 <br>

STA $4202     <br>
STA $4203  -> (X + 1)*(X + 1) <br>
LDA $4216  <br>

STA $4202  <br>
STA $4203   -> (X + 1)*(X + 1)*(X + 1)  <br>
LDA $4216  <br>

STA $4202 <br>
STA $4203   -> (X + 1)*(X + 1)*(X + 1)*(X + 1) <br>
LDA $4216  <br>

STA $4202 <br>
STA $4203   ->(X + 1)*(X + 1)*(X + 1)*(X + 1)*(X + 1) = (X + 1)^5  <br>
LDA $4216  <br> <br>

STA $4204   -> METEMOS DIVIDENDO <br>
STZ $4205 <br>
TYA <br>
STA $4206   ->METEMOS DIVISOR <br>
LDA $4216   ->OBTENEMOS RESTO <br>
TCD         ->LO PASAMOS AL DIRECT PAGE <br>

';
elseif ($_GET['cod']=='me6') 
$msg = '6. <br>
LDA #$00  <br>
LDY #$00  <br> <br> 

CICLO <br>
 &nbsp; LDA $3000,Y  <br>
 &nbsp; STA $5000,Y  <br>
 &nbsp; INX  <br>
 &nbsp; INY  <br>
 &nbsp; CPX #$C8   ; 200 VALORES SON DEL <br>&nbsp;&nbsp;&nbsp;&nbsp;HEX 000(00 HEX) AL 199(C7 HEX),   <br>
            &nbsp;&nbsp;&nbsp; ; ES DECIR QUE SI X NO ES C8,
             <br>&nbsp;&nbsp;&nbsp;&nbsp;SIGA CON EL CICLO CUBRIENDO DE 00-C7  <br>
  BNE CICLO  <br>
  ;FIN  <br>

';
elseif ($_GET['cod']=='me7') 
$msg = '7.  <br>
LDA #$10  <br>
STA $10  <br>  <br>

CICLO  <br>
 &nbsp; CPY #$10  <br>
 &nbsp; BMI CICLO2   ;SI Y ES MENOR A 10 SALTE A <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CICLO, EJECUTA MIENTRAS { }  <br>
 &nbsp; JMP FIN  <br>  <br>

CICLO2   <br>
 &nbsp; CPY #$05    ; SI Y = 5  <br>
 &nbsp;BEQ PARTE1  <br>
 &nbsp; JSR CHAO  <br>
PASO       <br>
 &nbsp; INY  <br>
 &nbsp; JMP CICLO  <br>  <br>
 
PARTE1  <br>
 &nbsp; JSR HOLA  <br>
 &nbsp; JMP PASO  <br>  <br>

HOLA  <br>
 &nbsp; LDX $10  <br>
 &nbsp; STY $1000,X  <br>
 &nbsp; INX  <br>
 &nbsp;  STX $10  <br>
 &nbsp; RTS  <br>  <br>

CHAO  <br>
 &nbsp; TYA  <br>
 &nbsp; ASL  <br>
 &nbsp; TAX  <br>
 &nbsp; PHA  <br>
 &nbsp; RTS  <br>
';










echo $msg;                    	
} ?>
                    
                    
                    </font></p></td></tr></table> 
                    <p align="center"> 
<font color="#336699" size="2" face="Verdana, Arial, Helvetica, sans-serif">


<p><a href="javascript:window.close();">cerrar</a></p> 

</td>
</tr>
</table>
      </center>
    </div>

    </td>
  </tr>
</table>

  </center>
</div>

</body>
</html>