<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>Tutorial de Creaci�n de Video Juegos con SDL .NET</title>
</head>


<body>
<span style="font-family: Verdana;">
<small>
<a href="../../desarrollo_juegos.php">Volver</a>
</small>
<hr style="width: 100%; height: 2px;">

<table width="100%">
<tr>
<td>
<big>
<span style="font-weight: bold;">Tutorial de Creaci�n de Video Juegos con SDL .NET</span>
</big>
<small>
<br style="font-weight: bold;"> 
Por Dark-N: <?php include '../../mail.php'; ?><br>
�ltima Actualizaci�n: 19-06-2011
<br><a href="http://darknromhacking.com/">http://darknromhacking.com</a><br>

<br>SDL .NET P�gina Oficial: <a href="http://cs-sdl.sourceforge.net">http://cs-sdl.sourceforge.net</a>

</small>
</td>
<td align="center">
<img src="imagenes/sdlnet.gif">
<td>
</tr>
</table>

<hr style="width: 100%; height: 2px;"><br>

<span style="font-family: Verdana;"><small>
SDL.NET es un conjunto de instrucciones .NET orientadas a objetos para <a href="http://www.libsdl.org">SDL</a>, una librer�a multi plataforma dise�ada para proveer acceso al audio, teclado, mouse, joystick, gr�ficas 3D via OpenGL y gr�ficas 2D.
<br><br>En espa�ol: Librer�a gratuita para Microsoft Studio .Net para hacer juegos 2D/3D, una versi�n mejorada de la librer�a SDL que es para C/C++.

<br><br><b>Requerimientos para correr una aplicaci�n hecha en SDL.NET: </b>
<li>Tener instalado
<a href="http://www.microsoft.com/downloads/details.aspx?FamilyID=0856EACB-4362-4B0D-8EDD-AAB15C5E04F5&displaylang=en">
Microsoft .NET Framework 2.0</a>.
<li>Tener instalada la �ltima versi�n de los <b>runtime</b> de <a href="http://cs-sdl.sourceforge.net">SDL.NET</a>, que a la fecha 
est� liberada la 6.1 (sdldotnet-6.1.0-runtime-setup.exe liberada el 01-Mayo-2008).

<br><br><b>Requerimientos para desarrollar aplicaciones con SDL. NET: </b>
<li><b>SDL.NET SDK 4.0</b> + Microsoft .NET FrameWork 1.1 si usas VS 2003 o <a href="http://www.icsharpcode.net/OpenSource/SD/">SharpDevelop 1.1</a>.
<li><b>SDL.NET SDK 6.0 o superior</b> + Microsoft .NET FrameWork 2.0 si usas VS 2005/2008 o <a href="http://www.icsharpcode.net/OpenSource/SD/">SharpDevelop 2.2</a>.
<br><br>
Recomiendo usar VS 2005 o 2008, son un poco pesados pero valen la pena. Para que no se asusten, es posible que en alg�n cap�tulo use el <a href="http://www.icsharpcode.net/OpenSource/SD/">SharpDevelop 2.2</a> ya que es gratis y pesa solo 8 MB!.
<br>En cuantos a los Frameworks.net, ambos son gratis y descargables desde microsoft. De todas formas si instalas el Visual Studio 2003 viene con la versi�n 1.1 y si usas VS 2005 viene con la versi�n 2.0. Si usas el VS 2008 instala el Framework 3.5. Para no complicarte, usa la versi�n 2.0 del Framework. Solo por conocimiento general, el Framework es como una "m�quina virtual" como la cl�sica m�quina virtual de Java pero en versi�n Microsoft.

<br><br>
<li>Puedes usar Linux o Windows. Si es Windows debes instalar el IDE <b>Microsoft Visual Studio .NET 2003 o 2005</b>, aunque los c�digos est�n probados con la versi�n 
VS 2003 de Windows. Si es <b>
<a href="http://cs-sdl.sourceforge.net/index.php/The_absolute_newbies_guide_to_SDL.NET#Installation_Under_Linux">Linux</a></b> 
o <b>MAC OSX</b> puedes usar <a href="http://www.mono-project.com/">Mono</a>. Ver mas detalles t�cnicos en 
<a href="http://cs-sdl.sourceforge.net/index.php/Frequently_asked_questions">p�gina oficial</a>.
<li>Es ideal conocer el IDE con que trabajaremos, en este caso <b>Microsoft Visual Studio .NET</b>. Se debe conocer la forma de crear proyectos, clases, como compilar y hacer referencias.
<li>Es ideal tener conocimientos del Lenguaje de Programaci�n Orientado a Objetos <b>C#</b> o en Visual Basic .NET. Personalmente me quedo C# pero ahora puedes usar cualquiera de estos dos lenguajes.<br>

<br><br><b>Nota:</b>Los Cap�tulos 1 al 4 del tutorial son hechos con SDL 4.0 y 6.0 
y con Visual Studio 2003, 2005 y ahora con VS 2008 usando C#. Los siguientes los har� con SDL .NET 6 (sdldotnet-6.1.0-sdk-setup.exe) 
y Visual Studio 2005 o 2008 igualmente con C#. 
Hay que decir que la sintaxis entre versiones SDL cambia, lo mismo para algunos m�todos de C#/VB en VS 2003 versus VS 2005.

<br></small>
</span><br>
<table align="center" width="100%">
	<tr>
		<td align="center"><a href="cap0-1.php">
		<img width="330" height="230" src="imagenes/cap0.PNG"><br>Preparar Ambiente</a> 
		 <a href="cap0-1.php">1</a>,<a href="cap0-2.php">2</a>,<a href="cap0-3.php">3</a>
		</td>
		<td align="center"><a href="cap1.php">
		<img width="330" height="230" src="imagenes/cap1.PNG"><br>Cap�tulo 1: Hola Mundo</a></td>
		<td align="center"><a href="cap2.php">
		<img width="330" height="230" src="imagenes/cap2.PNG"><br>Cap�tulo 2: Mover Sprites</a></td>
	</tr>
	<tr>
		<td height="30"></td>
	</tr>
	<tr>
		<td align="center"><a href="cap3.php">
		<img width="330" height="230" src="imagenes/cap3_final.PNG"><br>Cap�tulo 3: Mouse y Sonido</a></td>
		<td align="center"><a href="cap4.php">
		<img width="330" height="230" src="imagenes/cap4.PNG"><br>Cap�tulo 4: Animaci�n B�sica de Sprites</a></td>
		<td align="center"><a href="cap5-1.php">
		<img width="330" height="230" src="imagenes/cap521.PNG"><br>
		Cap�tulo 5: Pong</a> <a href="cap5-1.php">1</a>,<a href="cap5-2.php">2</a></td>
	</tr>
	<tr>
		<td height="30"></td>
	</tr>
	<tr>
		<td align="center"><a href="cap6.php">
		<img width="330" height="230" src="imagenes/cap6.PNG"><br>Cap�tulo 6: FadeIn y FadeOut</a></td>
		<td align="center"><a href="cap7.php">
		<img width="330" height="230" src="imagenes/cap7.PNG"><br>Cap�tulo 7: Scrolling Surface</a></td>
		<td align="center"><a href="cap8-1.php">
		<img width="330" height="230" src="imagenes/cap81.PNG"><br>Cap�tulo 8: Part�culas</a>
		 <a href="cap8-1.php">1</a>,<a href="cap8-2.php">2</a>,<a href="cap8-3.php">3</a>
		</td>
	</tr>
	<tr>
		<td height="30"></td>
	</tr>
	<tr>
		<td align="center"><a href="cap9.php">
		<img width="330" height="230" src="imagenes/cap9.PNG"><br>Cap�tulo 9: Personajes Caminando</a></td>
		<td align="center"><a href="cap10.php">
		<img width="330" height="230" src="imagenes/cap10.PNG"><br>Cap�tulo 10: Velocidad de Animaci�n</a></td>
	</tr>
</table><small><br>

<?php
include '../../piecdisq.php';
?>