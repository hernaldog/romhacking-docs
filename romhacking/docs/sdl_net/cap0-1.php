<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>Tutorial de Creaci�n de Video Juegos con SDL .NET</title>
</head>


<body>
<small>
<span style="font-family: Verdana;">
<a href="cap0-2.php">Ir a Parte 2</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap1.php">Siguiente Lecci�n</a>
</small>

<hr style="width: 100%; height: 2px;">

<table width="100%">
<tr>
<td>
<big>
<span style="font-weight: bold;">Preparar Ambiente</span>
</big>
<small>
<br style="font-weight: bold;"> 
Por Dark-N: naldo_go@hotmail.com
<br>

<span style="font-family: Verdana;">

<a href="http://darknromhacking.com/">http://darknromhacking.com</a><br>
<br>
<a href="cap0-2.php">Ir a Parte 2</a>
</td>
<td align="center">
<img src="imagenes/sdlnet.gif">
</td></tr>
</table>

<hr style="width: 100%; height: 2px;"><br>

<span style="font-family: Verdana;"><small>




<b>Objetivo:</b> dejar preparado el ambiente 
para empezar a programar. En este cap�tulo se utiliz� VS 2003 con SDL 4.0, y 
luego SDL 6.0 con VS 2005.<br>
<br>
Vamos a la p�gina de  <a href="http://cs-sdl.sourceforge.net">SDL.NET</a> y bajamos la �ltima versi�n. Al momento de realizar esta gu�a, el �ltimo release es el 
6. B�jate la versi�n que se llama <b>sdldotnet-6.0.0-sdk-setup.exe</b> que pesa 
7.62 MB, ya que contiene las DLL runtime y documentaci�n.
<br>La documentaci�n de SDL, referencia de Clases, instrucciones, etc., viene en el instalador sdk pero igual se puede ver online en <a href="http://cs-sdl.sourceforge.net/apidocs/html/SdlDotNet/">
http://cs-sdl.sourceforge.net/apidocs/html/SdlDotNet/</a>.

<br><br>Ejecuta el instalador y se abrir� la ventana t�pica de instalaci�n, Luego dale a Next hasta que te encuentres con la ventana que detalla lo que se instalar�, marca Runtime y si tiene Documentaci�n tambi�n seleccionalo.
<br>Una vez finalizada la instalaci�n, la librer�a por defecto quedar� en <b>C:\Program Files\SdlDotNet</b> o si tienes Windows en espa�ol quedar� en <b>C:\Archivos de Programa\SdlDotNet</b>, ahora abre <b>Microsoft Studio .NET</b>.

<br><br><img src="imagenes/vs20031.PNG"><br><br>

Ahora selecciona File->Blank Solution

<br><br><img src="imagenes/vs20032.PNG"><br><br>

A la soluci�n, col�cale el nombre "SDL" y d�jala en C:.

<br><br><img src="imagenes/vs20033.PNG"><br><br>

Ahora debemos agregar un proyecto que contendr� nuestras ventanas. Usaremos un solo proyecto para todos los cap�tulos del tutorial. Notar que cada proyecto nos permite generar un solo ejecutable de salida (un juego .exe), por lo que cuando se termine un tutorial, generas el .exe y lo renombras para que en el siguiente tutorial no lo sobreescribas.

<br><br><img src="imagenes/vs20034.PNG"><br><br>

Ser� un proyecto tipo <b>Windows Application</b>, le pondremos nombre "tutorial":

<br><br><img src="imagenes/new_proyect.PNG"><br><br>

Nuestra <b>Solution Explorer</b> nos muestra las referencias a otras dll o proyectos, nuestros proyectos, clases (.cs) y objetos que trabajamos:

<br><br><img src="imagenes/solution_explorer.PNG"><br><br>

Cada vez que queramos usar librer�as externas, como en ese caso, se debe agregar una <b>referencia</b> a ella, para esto se selecciona el proyecto y con el 2do. bot�n del mouse->Add Reference:

<br><br><img src="imagenes/add_ref.PNG"><br><br>

En la pesta�a .NET selecciona el �tem <b>SdlNotNet</b> solo si est� en la lista:

<br><br><img src="imagenes/add_ref1.PNG"><br><br>

En caso que NO est�, dale a <b>Browse</b> en esa misma ventana y escoge manualmente el archivo <b>SdlDotNet.dll</b> que est� dentro de <b>C:\Program Files\SdlDotNet\runtime\bin</b>

<br><br><img src="imagenes/add_ref2.PNG"><br><br>

Otra cosa importante es que se deben crear dentro del proyecto las <b>carpetas</b> que contendr�n nuestras <b>im�genes</b> (que se usar�n para crear nuestros <b>sprites</b>) y las <b>fuentes</b> que usaremos para dibujar palabras en pantallas, para esto selecciona el protecto y con 2do. bot�n del mouse->New Folder:

<br><br><img src="imagenes/new_folder.PNG"><br><br>

Ponle el nombre "imagenes" y crea otra con el nombre "fuentes". Luego te quedar� asi:

<br><br><img src="imagenes/new_folder2.PNG"><br><br>

Agregemos las fuentes que usaremos, vamos al directorio <b>C:\WINDOWS\Fonts</b> y ponle vista Detalles:

<br><br><img src="imagenes/fuente.PNG" border="1"><br><br>

Copia la fuente (Control+C) llamada <b>"Arial (True Type)"</b> y p�gala en el Visual Studio, en la carpeta Fuentes:
<br><br><img src="imagenes/fuente2.PNG" border="1"><br><br>
 
Despu�s de este paso, asi deber�as tener tu ambiente:

<br><br><img src="imagenes/fin_ambiente.PNG" border="1"><br><br>

Ahora estamos listos para empezar a programar ya que tenemos las carpetas bases 
como &quot;imagenes&quot; y &quot;fuentes&quot; que se pueden usar en el proyecto. <br>
<br>

<a href="cap0-2.php">Ir a Parte 2</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap1.php">Siguiente Lecci�n</a>

<?php
include '../../piecdisq.php';
?>
