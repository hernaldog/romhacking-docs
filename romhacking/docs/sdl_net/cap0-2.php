<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>Tutorial de Creaci�n de Video Juegos con SDL .NET</title>
</head>


<body>
<small>
<span style="font-family: Verdana;">
<a href="cap0-1.php">Ir a Parte 1</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap0-3.php">Ir a Parte 3</a> | <a href="cap1.php">Siguiente Lecci�n</a>
</small>

<hr style="width: 100%; height: 2px;">

<table width="100%">
<tr>
<td>
<small>
<font size="4">Generaci�n de Instalador</font><br style="font-weight: bold;"> 
Por Dark-N: <?php include '../../mail.php'; ?>
<br>

<span style="font-family: Verdana;">

<a href="http://darknromhacking.com/">http://darknromhacking.com</a><br>
<br>

<a href="cap0-1.php">Ir a Parte 1</a>

</td>
<td align="center">
<img src="imagenes/sdlnet.gif">
</td></tr>
</table>

<hr style="width: 100%; height: 2px;"><br>

<small>

<span style="font-family: Verdana;"><b>Objetivo:</b> Explicar como se realiza un 
instalador en VS .NET 2005 para as� probar tus juegos en otros PC. En otras 
palabras, como exportar tus juegos y aplicaciones SDL.NET.<br>
<br>
<a href="proyectos/VS2005_SDL6.0_cap1_fuentes+setup.rar">Bajar Proyecto 
&quot;HolaMundo&quot; completo (Proyecto c�digo fuente + proyecto Setup)</a><br>
<br>
Para probar una aplicaci�n se pueden hacer pruebas r�pidas o instaladores 
completos.</span></small><p><small>

<br>
</small>
<span style="font-family: Verdana;">
<small>

<b><font size="3">Requisitos de un Sistema que correr� aplicaciones SDL.NET</font><br></b>
<font size="2"><br>
-Instalar .Net Framework 2.0<br>
-Instalar <b>SDL.NET 6.0</b> <b>Runtime </b>(sdldotnet-6.0.0-runtime-setup.exe) 
que es m�s liviana que el SDK ya que contiene solo las librer�as base para 
correr aplicaciones hechas en SDL.NET.<br>
<br>
<img border="0" src="imagenes/setup4.PNG" width="362" height="283"><br>
La instalaci�n del Runtime es muy f�cil y no requiere reiniciar el pc.<br>
<br>
</font>


<br>
<b><font size="3">Como Hacer Pruebas R�pidas</font><br>
<br>
</b>Se puede realizar un test sencillo para ver como anda un ejecutable, 
copiando ciertos archivos al PC externo donde quieres probar tu aplicaci�n o 
juego hecho con SDL.NET.

<br><br>

Ahora se har� lo siguiente:<br>

<li style="font-family: Verdana;">Ir a la carpeta donde se genera el ejecutable de tu proyecto, si no has cambiado 
ninguna configuraci�n deber�a estar en la carpeta de tu proyecto llamada <b>Bin/Debug</b>.
<img border="1" src="imagenes/setup1.PNG"><br>
<span style="font-family: Verdana;">
En la imagen se ven los archivos del proyecto &quot;HolaMundo&quot;, correspondiente al cap�tulo 
1 del tutorial.</li>
<br>
<li style="font-family: Verdana;">En la m�quina donde deseas probar genera una carpeta donde dejaras los archivos.</li> 
yo crear� la carpeta &quot;SDLNet&quot;. 
Para este ejemplo usar� una imagen de <b>Windows Server 2003</b> montada en <b>Virtual PC 2007</b>. Esa 
m�quina debe tener el <b>Microsoft .Net Framework 2.0</b> instalado.<br>
<br>
<img border="1" src="imagenes/setup2.PNG"><br>
<br>
<li style="font-family: Verdana;">Arrastra y suelta adentro los siguientes archivos: <br>
&nbsp;-SDLNet_2005.exe.</li> Es el juego en si, si hay referencias a im�genes 
o iconos en el c�digo, tambi�n deben estar.<br>
&nbsp;-Cartetas y recursos necesarios para que funcione la aplicaci�n, por ejemplo la carpeta bin, imagenes y fuentes (marcadas en rojo en la imagen superior)<br>
<img border="1" src="imagenes/setup5.PNG" width="697" height="189"><br>
Adentro de la carpeta SDL_Net copiamos el ejecutable (dentro de Bin\Debug) con 
su estructura de im�genes y recursos, que en este caso usa.<br>
<br>
<li>Esta listo, basta ejecutar el archivo <b>SDLNEt_2005.exe</b> y se abrir� nuestra 
simple aplicaci�n:</li><br>
<img border="0" src="imagenes/2005_salidaCap1.PNG" width="296" height="243"><br>
<br>
<font size="3">
<b><br>
Como Hacer un Instalador (MSI)</b><br></font>
<br>La forma profesional de exportar un juego, es haciendo un instalador, y para 
ello Visual Studio te permite hacerlo de forma f�cil y personalizada. Por 
ejemplo haremos un instalador de la aplicaci�n HolaMundo. <br>
<br>
La gracia es que con un instalador puedes:<br>
-Exportar tus aplicaciones a distintos PCs, con otros SO incluso.<br>
<span style="font-family: Verdana;">
-Si haces el Proyecto Setup y luego cambias un aspecto del proyecto como un 
c�digo o algo de la l�gica, no debes hacer de nuevo el proyecto Setup, sino que 
simplemente re-compilarlo.</span><br>
<br>
Pasos Generales:<br>
&nbsp;<li>Abrimos la Soluci�n y agregamos un nuevo proyecto tipo <b>Build o 
Implementaci�n.<br>
<img border="0" src="imagenes/setup8.PNG" width="498" height="331"><br>
<br>
</b><li>Selecciona el Proyecto Setup y arriba ver�s un icono que dice Editor de 
Sistema de Archivos.<br>
<br>
<img border="1" src="imagenes/Setup9.PNG" width="295" height="241"><br>
<br>
<li>Selecciona Carpeta de Aplicaci�n y con el segundo bot�n del mouse escoge 
Agregar-&gt;Resultados del Proyecto. Selecciona el proyecto arriba y la salida 
principal que quieres agregar al setup.<br>
<br>
<img border="1" src="imagenes/Setup10.PNG" width="616" height="292"><span style="font-family: Verdana;"><br>
<br>
<img border="0" src="imagenes/Setup11.PNG" width="324" height="386"></span><br>
<br>
<li>Agrega de igual manera una carpeta que se llame &quot;Bin&quot; y adentro otra que se llame 
&quot;Juego&quot;. Luego mueve todas las salidas (las 2 dll y resultado principal de 
HolaMundo) adentro de Juego.<br>
<br>
<img border="1" src="imagenes/Setup12.PNG" width="610" height="222"><br>
<br>
<li>Crea 2 carpetas, una llamada &quot;imagenes&quot; y otra &quot;fuentes&quot;. Y adentro de cada una 
con el segundo bot�n escoge Agregar-&gt;Archivo y escoge la fuente en una carpeta y 
el icono en la otra, ambos recursos son que usa tu juego. La idea es que el 
propio Setup instale y copie todo lo necesario para que el juego ande.<br>
<br>
<img border="1" src="imagenes/Setup13.PNG" width="488" height="241"><br>
<br>
<li>Selecciona el Proyecto Setup y con F4 puedes ver y editar algunas propiedades 
como autor, descripci�n, versi�n (s�bela cada vez que hagas una mejora al c�digo 
por ejemplo 1.0.0-&gt;1.0.1-&gt;1.0.2, pero si es una gran mejora o un gran cambio 
p�sala a 2.0.0 y as� sucesivamante), ProductName, Manufacturer (empresa), etc.<br>
Ojo que la ruta de instalaci�n por defecto es [ProgramFilesFolder][Manufacturer]\[ProductName] 
asi que debes setear bien esos parametros.<br>
<br>
<img border="1" src="imagenes/Setup14.PNG" width="284" height="592"><br>
<br>
<li>Estamos listos, solo selecciona el Proyecto Setup y con el Segundo bot�n escoge 
Rebuill/Volver a Generar, para generar el instalador. En la barra de estado 
inferior dir� si fue generado correctamente.<br>
<br>
<img border="1" src="imagenes/Setup15.PNG" width="244" height="259"><br>
<br>
<li>Revisa la carpeta donde esta el Proyecto y ver�s un archivo MSI que contiene 
nuestra aplicaci�n. Ese archivo lo puedes tomar y llevar a otro PC e instalar.<br>
<br>
<img border="1" src="imagenes/Setup16.PNG" width="492" height="178"><br>
<br>
<li>Para instalar solo dale doble clic al MSI y 
sigue las instrucciones como cualquier Setup.<br>
<br>
<img border="0" src="imagenes/Setup17.PNG" width="477" height="393"><br>
<br>
<li>Luego revisa donde qued� instalado y ver�s las carpetas que se generaron 
autom�ticamente, ahora solo ejecuta el juego.<br>
<br>
<img border="1" src="imagenes/Setup18.PNG" width="452" height="251"><br>
<br>
<li>M�s adelante podr�s agregar m�s opciones a Setup, como permitir que se genere 
un acceso directo al escritorio y otras cosas.<br>
<br>
<li>Es bueno que en las Propiedades del Proyecto HolaMundo y en los proyectos en 
general, cambies la ruta de salida de los archivos de Bin\Debug a Bin solamante, 
esto es para que quede mas ordenado los archivos. Lo que si en el c�digo debes 
cambiar las rutas absolutas por ejemplo &quot;../../fuentes/comic.TTF&quot; por 
&quot;../fuentes/comic.TTF&quot;. Lo mismo con la salida del Setup, debes mover todo 
dentro de Bin y borrar la carpeta &quot;juego&quot;.<br>
<br>
<img border="1" src="imagenes/Setup19.PNG" width="505" height="234"><br><br>
<li>La desinstalaci�n es como cualquier aplicaci�n. En &quot;Product Information&quot; puedes 
ver detalles del creador, descripci�n, etc. Es decir lo que pusiste en las 
propiedades.<br>
<br>
<img border="1" src="imagenes/Setup20.PNG" width="585" height="366"><br>
<br>

<br>

<a href="cap0-1.php">Ir a Parte 1</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap0-3.php">Ir a Parte 3</a> | <a href="cap1.php">Siguiente Lecci�n</a>

<?php
include '../../piecdisq.php';
?>
