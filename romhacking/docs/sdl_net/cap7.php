<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>Tutorial de Creaci�n de Video Juegos con SDL .NET</title>
</head>


<body>
<small>
<span style="font-family: Verdana;">
<a href="cap6.php">Anterior</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap8-1.php">Siguiente</a>
</small>

<hr style="width: 100%; height: 2px;">

<table width="100%">
<tr>
<td>
<big>
<span style="font-weight: bold;">Cap�tulo 7: Recorrer un Fondo con Teclado (Scrolling Surface)</span>
</big>
<small>
<br style="font-weight: bold;"> 
Lenguaje: C#<br>
Para:  VS2003 / VS2005 / VS2008<br>
Por Dark-N: <?php include '../../mail.php'; ?>
<br>
</small>
<span style="font-family: Verdana;">
<small>
<a href="http://darknromhacking.com/">http://darknromhacking.com</a></small></td>
<td align="center">
<img src="imagenes/sdlnet.gif">
</table>

<br> 


<hr style="width: 100%; height: 2px;"><br>

<span style="font-family: Verdana;"><small>
<b>Objetivo:</b> A partir de una pantalla chica, vamos a recorrer una imagen mas grande de fondo con el teclado, y que a la vez, se vea una flecha indicando que nos movemos hacia all�.
<br><br>
Lo primero es descargar las 2 im�genes que usaremos (la imagen de fondo y una imagen con 4 flechas):<br><br>
<img src="imagenes/cursor.PNG" border="1">   <img src="imagenes/paisaje.PNG">
<br><br>
Estas im�genes las debes dejar en el directorio <b>bin/Debug</b> una vez compilado el proyecto.

Ahora el tema es simple ya que lo primero es definir una <b>enumeraci�n</b> con las 4 direcciones posibles:

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
private enum enumTeclas : int
{
  Up = 0,
  Down = 1,
  Left = 2,
  Right = 3
}
</pre></td></tr></table>

<br>Tambi�n creamos un arreglo llamado <i>teclas</i> que contendr� los movimientos y cuando se presione el teclado se almacene all� un numero: 

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
private static bool[] teclas = new bool[4];
...
case Key.UpArrow:
  teclas[(int)enumTeclas.Up] = args.Down;
  break;
case Key.DownArrow:
  teclas[(int)enumTeclas.Down] = args.Down;
  break;
...
</pre></td></tr></table>

<br>Debemos tener antes el game-loop la carga de las 2 im�genes: 

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
srf_cursor = new Surface( @"cursor.PNG").Convert(srf_videoScreen, true, false );
srf_cursor.TransparentColor = Color.FromArgb( 255, 255, 255 ); 
srf_fondo = new Surface( @"paisaje.PNG" ).Convert(srf_videoScreen, true, false );
</pre></td></tr></table>

<br>Aqu� dejamos transparente el blanco de fondo de la imagen con las flechas (imagen cursor.png), para esto usamos Color.FromArgb(Red=255, Green=255, Blue=255) = Blanco.
<br><br>
Finalmente en el evento Tick creamos 2 <b>Arraylist</b> (un Arraylist es un colecci�n de objetos, a grandes rasgos es parecido a un arreglo pero con mas atributos). El ArrayList <b>puntosFlechaDestino</b> tendr�n las posiciones a mover y <b>recanguloFlechaOrigen</b> contendr� el objeto Rect�ngulo de Sdl.Net con las posiciones de la imagen (flecha) a mostrar de cursor.png.

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
ArrayList recanguloFlechaOrigen = new ArrayList();
ArrayList puntosFlechaDestino = new ArrayList();
</pre></td></tr></table>

<br>Ahora veamos que pasa cuando presionamos con el teclado la fecha arriba o abajo 
<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
...
if( teclas[(int) enumTeclas.Up] )
{
    puntosFlechaDestino.Add( new Point( srf_videoScreen.Width/2 - 50, srf_videoScreen.Height/2 - 150 ));
    recanguloFlechaOrigen.Add( new Rectangle( 0, 0, 95, 95 ));
    if( y > 0 )
        y -= 5;
}

if( teclas[(int) enumTeclas.Down] )
{
    puntosFlechaDestino.Add( new Point( srf_videoScreen.Width/2 - 50, srf_videoScreen.Height/2 + 50 ));
    recanguloFlechaOrigen.Add( new Rectangle( 100, 100, 95, 95 ));
    if( y < srf_fondo.Height - srf_videoScreen.Height )
        y += 5;
}

if( teclas[(int) enumTeclas.Left] )
...

if( teclas[(int) enumTeclas.Right] )
...
</pre></td></tr></table>
			
Si se presiona arriba: <br>
<br>Se guardan la posiciones a movernos en <b>puntosFlechaDestino</b> tomando el centro de la imagen como referencia y le restamos 50 pixels hacia arriba y 150 de largo.
<br>Dentro de <b>recanguloFlechaOrigen</b> guardamos el rect�ngulo con las posiciones 0, 0, 95, 95 de la imagen cursor.png
<br>La variable <b>y</b> que maneja el eje Y se resta 5 pixels (si subes con el teclado Y se resta, si bajas Y se sube) 

<br><br>Y as� con las otras direcciones.
<br><br>Finalmente tenemos el render de los objetos, para esto como siempre usamos la propiedad <b>Blit</b>:

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
srf_videoScreen.Blit( srf_fondo, new Point( 0, 0 ), new Rectangle ( x, y, srf_videoScreen.Width, srf_videoScreen.Height ));

for( int index = 0; index < puntosFlechaDestino.Count; index ++ )
{
   srf_videoScreen.Blit(srf_cursor, (Point) puntosFlechaDestino[index], (Rectangle) recanguloFlechaOrigen[index]);
}

srf_videoScreen.Update();
</pre></td></tr></table>
Con srf_videoScreen.Blit( superficie, punto donde pintar, rectangulo) pintamos la imagen de fondo, pero le delimitamos que no sea completo, sino solo el largo y ancho de la superficie de video solamente. El rect�ngulo tiene los par�metros x,y que son calculados en el cuadro anterior. Estas variables son las encargadas de dar el efecto que se mueve le fondo.
<br><br>
Con el ciclo For lo que hacemos es pintar tantas veces el cursor como puntosFlechaDestino hayan. De esta forma solo se ve las fechas que corresponde al movimiento. Si presionas derecha, se ve una fecha apuntando a la derecha y a la vez, se mueve la imagen hacia all�.

<br><hr style="width: 100%; height: 2px;"><br>
C�digo fuente para <b>VS 2003</b>:

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
/*
  Ejemplo usando SDL.NET 4.0 y VS 2003 con C#
  Lo que haremos ser? un scrolling de una imagen, permitiendo mover el fondo usando el teclado 
  Recomendado usar PNG en vez de JPG o BMP ya que soporta Alpha Channels para aplicar Transparencia
  
  Autor: Dark-N (basado en el codigo de: Egon A. Rath)
  Fecha: 11-05-2009 
*/

using System;
using System.Drawing;
using SdlDotNet;
using SdlDotNet.Sprites;
using System.IO;
using System.Collections;


namespace Scrolling
{

    public class Superficie
    {
        private static Surface srf_videoScreen;
        private static Surface srf_cursor;
        private static Surface srf_fondo;
        private static bool[] teclas = new bool[4];
        private static int x = 0, y = 0;
        

        private enum enumTeclas : int
        {
            Up = 0,
            Down = 1,
            Left = 2,
            Right = 3
        }

        public static void Main()
        {
             
            srf_videoScreen = Video.SetVideoModeWindow(300, 300, 32);    
            Video.WindowCaption = "Scrolling de una Superficie";

            CargaImagenes();

            Events.Quit += new QuitEventHandler(Evento_Salir);
            Events.Tick += new TickEventHandler(Evento_Tick);
            Events.KeyboardDown +=new KeyboardEventHandler(TecladoManejador);
            Events.KeyboardUp +=new KeyboardEventHandler(TecladoManejador);            
            Events.Run();        
        }


        private static void Evento_Salir(object sender, SdlDotNet.QuitEventArgs e)
        {
            Events.QuitApplication();
        }
        

        private static void CargaImagenes()
        {                      
            srf_cursor = new Surface( @"cursor.PNG").Convert(srf_videoScreen, true, false );
            srf_cursor.TransparentColor = Color.FromArgb( 255, 255, 255 ); //Blanco (Red=255, Green=255, Blue=255)
            srf_fondo = new Surface( @"paisaje.PNG" ).Convert(srf_videoScreen, true, false );
        }

        
        private static void TecladoManejador( object sender, KeyboardEventArgs args )
        {
            switch( args.Key )
            {
                case Key.UpArrow:
                    teclas[(int)enumTeclas.Up] = args.Down;
                    break;
                case Key.DownArrow:
                    teclas[(int)enumTeclas.Down] = args.Down;
                    break;
                case Key.LeftArrow:
                    teclas[(int)enumTeclas.Left] = args.Down;
                    break;
                case Key.RightArrow:
                    teclas[(int)enumTeclas.Right] = args.Down;
                    break;
                case Key.Escape:
                    Events.QuitApplication();
                    break;
            }
        }


        private static void Evento_Tick( object sender, TickEventArgs args )
        {
            ArrayList recanguloFlechaOrigen = new ArrayList();
            ArrayList puntosFlechaDestino = new ArrayList();
            
            if( teclas[(int) enumTeclas.Up] )
            {
                puntosFlechaDestino.Add( new Point( srf_videoScreen.Width/2 - 50, srf_videoScreen.Height/2 - 150 ));
                recanguloFlechaOrigen.Add( new Rectangle( 0, 0, 95, 95 ));
                if( y > 0 )
                    y -= 5;
            }

            if( teclas[(int) enumTeclas.Down] )
            {
                puntosFlechaDestino.Add( new Point( srf_videoScreen.Width/2 - 50, srf_videoScreen.Height/2 + 50 ));
                recanguloFlechaOrigen.Add( new Rectangle( 100, 100, 95, 95 ));
                if( y < srf_fondo.Height - srf_videoScreen.Height )
                    y += 5;
            }

            if( teclas[(int) enumTeclas.Left] )
            {
                puntosFlechaDestino.Add( new Point( srf_videoScreen.Width/2 - 150, srf_videoScreen.Height/2 - 50 ));
                recanguloFlechaOrigen.Add( new Rectangle( 0, 100, 95, 95 ));
                if( x > 0 )
                    x -= 5;
            }

            if( teclas[(int) enumTeclas.Right] )
            {
                puntosFlechaDestino.Add( new Point( srf_videoScreen.Width / 2 + 50, srf_videoScreen.Height / 2 - 50 ));
                recanguloFlechaOrigen.Add( new Rectangle( 100, 0, 95, 95 ));
                if( x < srf_fondo.Width - srf_videoScreen.Width )
                    x += 5;
            }        
        

            //Mostramos imagen de fondo (la seccion que que alcanza a verse)
            srf_videoScreen.Blit( srf_fondo, new Point( 0, 0 ), new Rectangle ( x, y, srf_videoScreen.Width, srf_videoScreen.Height ));

            //Mostramos cursor. (la seccion del cursor ya que se saca una parte de la imagen para mostrar 1 de las 4 direcciones)
            for( int index = 0; index < puntosFlechaDestino.Count; index ++ )
            {
                srf_videoScreen.Blit(srf_cursor, (Point) puntosFlechaDestino[index], (Rectangle) recanguloFlechaOrigen[index]);
            }            

            srf_videoScreen.Update();
        }
    }
}
</pre></td></tr></table>

<br><hr style="width: 100%; height: 2px;"><br>

C�digo fuente para <b>VS 2005/2008</b>. Antes recuerda: <br>
<li>Agregar las referencias: SDL.Net 6.0 y System.Drawing.
<li>En las propiedades del Proyecto, en Output Type dejarlo como <b>Windows Application</b>
<li>Copiar las imagenes que coloqu� en la carpeta <b>bin/Debug</b> del proyecto.

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
//Archivo: Superficie.cs
//Autor: Dark-N (basado en el codigo de: Egon A. Rath)
//fecha: 25-05-2009

using System;
using System.Collections.Generic; //para eventos y otras cosas
using System.Collections; //para ArrayList
using System.Drawing; //colores

using SdlDotNet.Graphics; //para sprites y surfaces
using SdlDotNet.Core; //Eventos
using SdlDotNet.Input; //keyborad
namespace ScrollingSurface
{
    public class Superficie
    {
        private static Surface srf_videoScreen;
        private static Surface srf_cursor;
        private static Surface srf_fondo;
        private static bool[] teclas = new bool[4];
        private static int x = 0, y = 0;

        private enum enumTeclas : int
        {
            Up = 0,
            Down = 1,
            Left = 2,
            Right = 3
        }

        public static void Main()
        {

            srf_videoScreen = Video.SetVideoMode(320, 280, false, false, false, true);
            Video.WindowCaption = "Scrolling de una Superficie";

            CargaImagenes();

            SdlDotNet.Core.Events.KeyboardDown += new EventHandler < KeyboardEventArgs >(Evento_Teclado);
            SdlDotNet.Core.Events.KeyboardUp += new EventHandler < KeyboardEventArgs >(Evento_Teclado);
            SdlDotNet.Core.Events.Tick += new EventHandler < TickEventArgs >(Evento_Tick);
            SdlDotNet.Core.Events.Quit += new EventHandler < QuitEventArgs >(Evento_Salir);
            Events.Run();        
        }


        private static void CargaImagenes()
        {
            srf_cursor = new Surface(@"cursor.PNG").Convert(srf_videoScreen, true, false);
            srf_cursor.Transparent = true;
            srf_cursor.TransparentColor = Color.FromArgb(255, 255, 255); //Blanco (Red=255, Green=255, Blue=255)
            srf_fondo = new Surface(@"paisaje.PNG").Convert(srf_videoScreen, true, false);
        }

        private static void Evento_Salir(object sender, QuitEventArgs e)
        {
            Events.QuitApplication();
        }

        private static void Evento_Teclado( object sender, KeyboardEventArgs args )
        {
            switch( args.Key )
            {
                case Key.UpArrow:
                    teclas[(int)enumTeclas.Up] = args.Down;
                    break;
                case Key.DownArrow:
                    teclas[(int)enumTeclas.Down] = args.Down;
                    break;
                case Key.LeftArrow:
                    teclas[(int)enumTeclas.Left] = args.Down;
                    break;
                case Key.RightArrow:
                    teclas[(int)enumTeclas.Right] = args.Down;
                    break;
                case Key.Escape:
                    Events.QuitApplication();
                    break;
            }          
        }

        private static void Evento_Tick(object sender, TickEventArgs args)
        {
            ArrayList recanguloFlechaOrigen = new ArrayList();
            ArrayList puntosFlechaDestino = new ArrayList();

            if (teclas[(int)enumTeclas.Up])
            {
                puntosFlechaDestino.Add(new Point(srf_videoScreen.Width / 2 - 50, srf_videoScreen.Height / 2 - 150));
                recanguloFlechaOrigen.Add(new Rectangle(0, 0, 95, 95));
                if (y > 0)
                    y -= 5;
            }

            if (teclas[(int)enumTeclas.Down])
            {
                puntosFlechaDestino.Add(new Point(srf_videoScreen.Width / 2 - 50, srf_videoScreen.Height / 2 + 50));
                recanguloFlechaOrigen.Add(new Rectangle(100, 100, 95, 95));
                if (y < srf_fondo.Height - srf_videoScreen.Height)
                    y += 5;
            }

            if (teclas[(int)enumTeclas.Left])
            {
                puntosFlechaDestino.Add(new Point(srf_videoScreen.Width / 2 - 150, srf_videoScreen.Height / 2 - 50));
                recanguloFlechaOrigen.Add(new Rectangle(0, 100, 95, 95));
                if (x > 0)
                    x -= 5;
            }

            if (teclas[(int)enumTeclas.Right])
            {
                puntosFlechaDestino.Add(new Point(srf_videoScreen.Width / 2 + 50, srf_videoScreen.Height / 2 - 50));
                recanguloFlechaOrigen.Add(new Rectangle(100, 0, 95, 95));
                if (x < srf_fondo.Width - srf_videoScreen.Width)
                    x += 5;
            }

            srf_videoScreen.Blit(srf_fondo, new Point(0, 0), new Rectangle(x, y, srf_videoScreen.Width, srf_videoScreen.Height));

            for (int index = 0; index < puntosFlechaDestino.Count; index++)
            {
                srf_videoScreen.Blit(srf_cursor, (Point)puntosFlechaDestino[index], (Rectangle)recanguloFlechaOrigen[index]);
            }

            srf_videoScreen.Update();
        }
    }
}

</pre></td></tr></table>

<br>
Una vez codificado, si al compilar no arroja errores, debe aparecerte algo como esto:<br><br>
<img src="imagenes/cap7.PNG">

<br><br>Bajar proyecto para VS 2003 desde <a href="proyectos/VS2003_SDL4.0_cap7.zip">aqu�</a>.
<br><br>Bajar proyecto para VS 2008 desde <a href="proyectos/VS2008_SDL6.0_cap7.zip">aqu�</a>.


<hr style="width: 100%; height: 2px;"><br>
<a href="cap6.php">Anterior</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap8-1.php">Siguiente</a>

<?php
include '../../piecdisq.php';
?>
