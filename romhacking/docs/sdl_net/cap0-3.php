<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>Tutorial de Creaci�n de Video Juegos con SDL .NET</title>
</head>


<body>
<small>
<span style="font-family: Verdana;">
<a href="cap0-2.php">Ir a Parte 2</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap1.php">Siguiente Lecci�n</a>

</small>

<hr style="width: 100%; height: 2px;">

<table width="100%">
<tr>
<td>
<small>
<font size="4">Errores T�picos al Momento de ejecutar un aplicaci�n hecha con SDL.Net</font><br style="font-weight: bold;"> 
Por Dark-N: <?php include '../../mail.php'; ?>
<br>

<span style="font-family: Verdana;">

<a href="http://darknromhacking.com/">http://darknromhacking.com</a><br>
</td>
<td align="center">
<img src="imagenes/sdlnet.gif">
</td></tr>
</table>

<hr style="width: 100%; height: 2px;"><br>

<small>

<span style="font-family: Verdana;"><b>Objetivo:</b> Explicar como resolver los errores t�picos que se puede dar al monento de correr algun aplicaci�n hecha con SDL.Net.<br>

<br>

<span style="font-family: Verdana;">

<br><b><font size="3">Posibles Errores al Ejecutar una Aplicaci�n Instalada</font></b><br><br>
<li>Si el PC no tiene instalado el SDL .NET  Runtime o SDK arrojar� un error. Debe tener 
instalado ya sea el <b>Runtime</b> (por ejemplo <b>sdldotnet-6.0.0-runtime-setup.exe</b>)<b>
</b>o la versi�n SDK (<b>sdldotnet-6.0.0-sdk-setup.exe</b>), recuerda que el Runtime 
contiene las librer�as base para ejecutar cualquier aplicaci�n que requiera 
SDL.NET, la segunda en cambio es orientado al desarrollo, contiene las 
librer�as, ejemplos, documentaci�n, etc. <br><br>
<li>Si ese PC no tiene el <b>Microsoft .Net 
Framework 2.0</b> instalado, arrojar� un mensaje de error diciendo que necesita 
estar instalado para ejecutarse como muestra la imagen inferior.</li>
<br>

<img border="1" src="imagenes/setup3.PNG" width="615" height="120"><br>
Recuerda que antes de Instalar el .Net Framework 2.0 debes tener instalado el <b>
Windows Installer 3.0 </b>o superior.<br>

<br>

<li>Si no se encuentra alguna imagen o recurso que necesite el juego arrojara un 
error como algunos de estos:</li>
<br>

<img border="0" src="imagenes/setup6.PNG" width="412" height="261"><br><br>
Arriba se ve el error que arroja si tienes Visual Studio 2005/2008 instalado. Si seleccionas <b>Depurar/Debug</b> se abrir� una ventana que dice que se abrir� una nueva instancia de VS2008:<br><br>

<img border="1" src="imagenes/setup21.PNG"><br><br>

Luego si presionas <b>Yes</b> se ver� el error verdadero y dir� que el <b><a href="http://es.wikipedia.org/wiki/Ensamblado">Assembly</a></b> llamado <b>SDLDotNet 6.1.0.0 no est� instalado</b>: <br><br>


Para solucionar el problema basta instalar el sdldotnet-6.1.0-runtime-setup.exe
o sdldotnet-6.1.0-sdk-setup.exe (no ambos, ver Tips al final).

<img border="1" src="imagenes/setup22.PNG"><br>


<br>

Este es el error que arroja si tienes el Visual Studio 2003 instalado:<br><br>
<img border="1" src="imagenes/setup7.PNG" width="327" height="354">

<br><br>
<hr style="width: 100%; height: 2px;"><br>
<font size="4">Tips: Ver Assemblies instalados</font><br><br>
Si tienes dudas acerca si SDL.Net est� instalado en tu PC o que versi�n es la que tienes, puedes ir a <b>C:\windows\assembly</b> y ver�s la lista de Assemblies oficiales instalados<br><br>

<img border="1" src="imagenes/setup23.PNG">

<br><br>
<b>NOTA:</b> Al instalar el SDL.NET SDK o el Runtime solo se registra como un �nico Assembly. Es decir que si instalas ambos, en esta lista se ver� el �ltimo.<br>
Lo anterior puede llevar al <b>problema</b> de que si tienes ambas versiones instaladas, y desinstalas el Runtime solamente, el SDL queda no operativo.<br>
Por lo que recomiendo instalar solamente el SDK o el Runtime en una misma m�quina.

<br><br><br>
<a href="cap0-2.php">Ir a Parte 2</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap1.php">Siguiente Lecci�n</a>

<?php
include '../../piecdisq.php';
?>
