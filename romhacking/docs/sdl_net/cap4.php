<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>Tutorial de Creaci�n de Video Juegos con SDL .NET</title>
</head>

<body>
<small>
<span style="font-family: Verdana;">
<a href="cap3.php">Anterior</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap5-1.php">Siguiente</a>
</span>
</small>

<hr style="width: 100%; height: 2px;">

<table width="100%">
<tr>
<td>
<big>
<span style="font-weight: bold;">Cap�tulo 4: Animaci�n B�sica de Sprites</span>
</big>
<small>
<br style="font-weight: bold;"> 
Lenguaje: C#<br>
Para:  VS2003 / VS2005 / VS2008<br>
Por Dark-N: <?php include '../../mail.php'; ?>
<br>

<span style="font-family: Verdana;">
<a href="http://darknromhacking.com/">http://darknromhacking.com</a></small></td>
<td align="center">
<img src="imagenes/sdlnet.gif">
</table>

<hr style="width: 100%; height: 2px;"><br>

<span style="font-family: Verdana;"><small>
<b>Objetivo:</b> Animar un Sprite al moverse con el teclado.<br>
NOTA: Se obtuvo como base el ejemplo de la p�gina oficial de SDL.NET. <br><br>

Lo primero es copiar esta imagen en la carpeta <b>imagenes</b> del proyecto. Esta imagen contiene la animaci�n del personaje:
<br><img src="imagenes/Hero.png"><br><br>

Creamos una nueva clase <b>cap4.cs</b> y adentro pon este simple c�digo que inicializa la pantalla con un mensaje:

<br><table><tr><td bgcolor="#CCCCCC"><pre>
using System;
using System.Drawing;
using SdlDotNet;
using SdlDotNet.Sprites;

namespace tutorial
{    
    public class cap4
    {
        int resx = 400;
        int resy = 300;
    

        //Constructor
        public cap4()
        {            
            Video.SetVideoModeWindow(resx, resy);
            Video.WindowCaption = "Tutorial SDL: Capitulo 4";
            Video.WindowIcon(new System.Drawing.Icon("../../Imagenes/mario3.ico"));
            //Los Eventos iran en Run()
        }

        private void Events_Tick(object sender, TickEventArgs e)
        {

            Video.Screen.Fill(Color.Black);        
            Video.Screen.Update();
            juego();
        }

        // l�gica de nuestro juego
        private void juego() 
        {
            
            SdlDotNet.Font fuente = new SdlDotNet.Font("../../fuentes/ARIAL.TTF",14);            
            TextSprite texto = new TextSprite("Ejemplo Animaci�n de Sprite", fuente, Color.Yellow, new Point(0,0));            
            texto.Render(Video.Screen);
            
        }        

        public void Run()
        {    
            Events.KeyboardDown +=new KeyboardEventHandler(Events_KeyboardDown);
            Events.Run();
        }

        private void Events_KeyboardDown(object sender, KeyboardEventArgs e)
        {                
            if(e.Key == Key.Escape)
            {
                Events.QuitApplication();
            }        
        }

        [STAThread]
        public static void Main()
        {
            cap4 juego = new cap4();
            juego.Run();
        }
    }
    
}
</pre></td></tr></table><br>

Abajo de la declaraci�n resx, resy, creamos la variable <b>Hero</b> de tipo <b>AnimatedSprite</b>:
<br><table><tr><td bgcolor="#CCCCCC"><pre>
private AnimatedSprite hero = new AnimatedSprite();
</pre></td></tr></table><br>

Dentro del m�todo <b>Run()</b> se setea los <b>FPS</b> (Frames por Segundo) de la animaci�n y se crea un manejador de eventos para <b>KeyboardUp</b> es decir, un Event Handler que registre cuando soltemos alguna tecla. Recuerda que el Event Handler de <b>KeyboradDown</b> ya existe y la usamos para programar cuando se presiona <b>Escape</b>. Entonces <b>Run()</b> nos queda as�:
<br><table><tr><td bgcolor="#CCCCCC"><pre>
public void Run()
{	
    Events.Fps = 50;
    Events.Tick += new TickEventHandler(Events_Tick);
    Events.KeyboardDown += new KeyboardEventHandler(Events_KeyboardDown);
    Events.KeyboardUp += new KeyboardEventHandler(Events_KeyboardUp);
    Events.Quit += new QuitEventHandler(Events_Salir);
    Events.Run();
}
</pre></td></tr></table><br>

Dentro del <b>constructor</b> agregamos distintas cosas:
<li>Los <b>Frames de Animaci�n</b> gracias a la clase <b>SurfaceCollection(imagen, tama�o_en_pix, fila_numero)</b>.
<li>Agregamos las animaciones con <b>miAnimatedSprite.Animations.Add(alias_animacion, Animaci�n(miSurface),delay_frame)</b>.
<li>Sacamos el color purpura de fondo de la imagen con <b>miAnimatedSprite.TransparentColor</b>.
<li>Iniciamos el personaje no en movimiento con <b>miAnimatedSprite.CurrentAnimation</b>.
<li>Posicionamos el personaje en el centro de la pantalla con <b>miAnimatedSprite.Center</b>. 

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
public cap4()
{       
    Video.SetVideoModeWindow(resx, resy);
    Video.WindowCaption = "Tutorial SDL: Capitulo 4";
    Video.WindowIcon(new System.Drawing.Icon("../../Imagenes/mario3.ico"));

    // Se carga la imagen
    Surface image = new Surface("../Imagenes/Hero.png");

    // Se crean los Frames de Animaci�n
    SurfaceCollection walkUp = new SurfaceCollection(image, new Size(24,32), 0); //24x32 cada frame, fila 0, ser� cuando el personaje suba
    SurfaceCollection walkRight = new SurfaceCollection(image, new Size(24,32), 1);
    SurfaceCollection walkDown = new SurfaceCollection(image, new Size(24,32), 2);
    SurfaceCollection walkLeft = new SurfaceCollection(image, new Size(24,32), 3); //ultima fila de la imagen

    // Agregamos las animaciones al objeto hero
    hero.Animations.Add("WalkUp", new Animation(walkUp, 35));
    hero.Animations.Add("WalkRight", new Animation(walkRight, 35));
    hero.Animations.Add("WalkDown", new Animation(walkDown, 35));
    hero.Animations.Add("WalkLeft", new Animation(walkLeft, 35));

    // Pintamos transparente el fondo del sprite
    hero.TransparentColor = Color.Magenta;

    // Seteamos la animacion inicial y que no se mueva
    hero.CurrentAnimation = "WalkDown";
    hero.Animate = false;

    // Lo colocamos en el centro de la pantalla
    hero.Center = new Point(Video.Screen.Width/2, Video.Screen.Height/2);
}
</pre></td></tr></table><br>

Dentro de <b>Events_Tick</b> se cambia un poco lo que se tenia en un principioy se agrega el Evento <b>Events_Salir</b>:
<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
private void Events_Tick(object sender, TickEventArgs e)
{
    Video.Screen.Fill(Color.Black);        
    //se rendera aqu� el personaje
    hero.Render(Video.Screen);    
    SdlDotNet.Font fuente = new SdlDotNet.Font("../../fuentes/ARIAL.TTF",14);
    TextSprite texto = new TextSprite("Ejemplo Animaci�n de Sprite", fuente, Color.Yellow, new Point(0,0));
    texto.Render(Video.Screen);    
    Video.Screen.Update();
    juego();
}

private void Events_Salir(object sender, SdlDotNet.QuitEventArgs e)
{
    Events.QuitApplication();
}
</pre></td></tr></table><br>
Ahora dentro del m�todo <b>juego()</b> se mueve el objeto <b>hero</b>:
<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
private void juego() 
{  
  // Si Hero esta animado, el camina, as� que se mueve
  if(hero.Animate)
  {
    switch(hero.CurrentAnimation)
    {
        case "WalkLeft":
            // 2 es la velocidad cuando camina
            hero.X -= 2;
            break;
        case "WalkUp":
            hero.Y -= 2;
            break;
        case "WalkDown":
            hero.Y += 2;
            break;
        case "WalkRight":
            hero.X += 2;
            break;
    }
  }
}
</pre></td></tr></table><br>

Ahora vamos a los eventos del teclado que mover�n el personaje, primero el evento manejador de <b>presionar</b> alguna tecla llamado <b>Events_KeyboardDown(sender,arg)</b>:
<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
private void Events_KeyboardDown(object sender, KeyboardEventArgs e)
{
    // Chequea que tecla es presionada y cambia la animaci�n
    switch(e.Key)
    {        
        case Key.LeftArrow:   //se mueve a la izquierda
            hero.CurrentAnimation = "WalkLeft";
            hero.Animate = true;
            break;
        case Key.RightArrow:
            hero.CurrentAnimation = "WalkRight";
            hero.Animate = true;
            break;
        case Key.DownArrow:
            hero.CurrentAnimation = "WalkDown";
            hero.Animate = true;
            break;
        case Key.UpArrow:
            hero.CurrentAnimation = "WalkUp";
            hero.Animate = true;
            break;
        case Key.Escape:  //salimos del juego
            Events.QuitApplication();
            break;
    }
}
</pre></td></tr></table><br>

Y solo falta el c�digo al evento manejador que se gatilla al <b>soltar</b> una tecla que lo llamamos <b>Events_KeyboardUp</b>:

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
private void Events_KeyboardUp(object sender, KeyboardEventArgs e)
{
    // Al soltar la tecla deja de moverse Hero
    if(e.Key == Key.LeftArrow && hero.CurrentAnimation == "WalkLeft")
        hero.Animate = false;
    else if(e.Key == Key.UpArrow && hero.CurrentAnimation == "WalkUp")
        hero.Animate = false;
    else if(e.Key == Key.DownArrow && hero.CurrentAnimation == "WalkDown")
        hero.Animate = false;
    else if(e.Key == Key.RightArrow && hero.CurrentAnimation == "WalkRight")
        hero.Animate = false;
}
</pre></td></tr></table><br>

Ahora solo has el <b>Rebuild</b> para ver si hay errores y generar el ejecutable y luego presiona <b>F5</b> para ver como funciona:<br>

<img src="imagenes/cap4.PNG"><br><br>
Por �ltimo ve a la carpeta <b>bin/Debug </b>donde tienes el proyecto, en mi caso <b>C:SDL\tutorial\bin\Debug</b> y c�mbiale el nombre al archivo <b>tutorial.exe</b> por <b>cap4_animacion_sprite.exe</b>.

<br><hr style="width: 100%; height: 2px;"><br>
C�digo fuente de este cap�tulo para VS2003 y SDL 4.0:

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
//Tutorial de SDL.NET
//Archivo: cap4.cs
//Autor: Dark-N (naldo_go@hotmail.com)
//Fecha: 28-11-2006
//Capitulo4: Animaci�n b�sica de Sprites

using System;
using System.Drawing;
using SdlDotNet;
using SdlDotNet.Sprites;

namespace tutorial
{    
    public class cap4
    {
        int resx = 400;
        int resy = 300;
        
        private AnimatedSprite hero = new AnimatedSprite();
    
        //Constructor
        public cap4()
        {            
            Video.SetVideoModeWindow(resx, resy);            
            Video.WindowCaption = "Tutorial SDL: Capitulo 4";
            Video.WindowIcon(new System.Drawing.Icon("../../Imagenes/mario3.ico"));
            
            // Se carga la imagen
            Surface image = new Surface("../../Imagenes/Hero.png");
    
            // Se crean los Frames de Animaci�n
            SurfaceCollection walkUp = new SurfaceCollection(image, new Size(24,32), 0);
            SurfaceCollection walkRight = new SurfaceCollection(image, new Size(24,32), 1);
            SurfaceCollection walkDown = new SurfaceCollection(image, new Size(24,32), 2);
            SurfaceCollection walkLeft = new SurfaceCollection(image, new Size(24,32), 3);

            // Agregamos las animaciones al objeto hero
            hero.Animations.Add("WalkUp", new Animation(walkUp, 35));
            hero.Animations.Add("WalkRight", new Animation(walkRight, 35));
            hero.Animations.Add("WalkDown", new Animation(walkDown, 35));
            hero.Animations.Add("WalkLeft", new Animation(walkLeft, 35));

            // Pintamos transparente el fondo del sprite
            hero.TransparentColor = Color.Magenta;

            // Seteamos la animacion inicial y que no se mueva
            hero.CurrentAnimation = "WalkDown";
            hero.Animate = false;

            // Lo colocamos en el centro de la pantalla
            hero.Center = new Point(Video.Screen.Width / 2, Video.Screen.Height / 2);
        }

        private void Events_Tick(object sender, TickEventArgs e)
        {
            Video.Screen.Fill(Color.Black);        
            hero.Render(Video.Screen);

            SdlDotNet.Font fuente = new SdlDotNet.Font("../../fuentes/ARIAL.TTF",14);            
            TextSprite texto = new TextSprite("Ejemplo Animaci�n de Sprite", fuente, Color.Yellow, new Point(0,0));
            texto.Render(Video.Screen);

            Video.Screen.Update();
            juego();
        }

        // l�gica de nuestro juego
        private void juego() 
        {        
            // Si Hero esta animado, el camina, asi que se mueve
            if(hero.Animate)
            {
                switch(hero.CurrentAnimation)
                {
                    case "WalkLeft":
                        // 2 es la velocidad cuando camina
                        hero.X -= 2;
                        break;
                    case "WalkUp":
                        hero.Y -= 2;
                        break;
                    case "WalkDown":
                        hero.Y += 2;
                        break;
                    case "WalkRight":
                        hero.X += 2;
                        break;
                }
            }
        }        

        public void Run()
        {    
            Events.Fps = 50;
            Events.Tick += new TickEventHandler(Events_Tick);
            Events.KeyboardDown += new KeyboardEventHandler(Events_KeyboardDown);
            Events.KeyboardUp += new KeyboardEventHandler(Events_KeyboardUp);
            Events.Quit += new QuitEventHandler(Events_Salir);
            Events.Run();
        }

        private void Events_Salir(object sender, SdlDotNet.QuitEventArgs e)
        {
            Events.QuitApplication();
        }

        private void Events_KeyboardDown(object sender, KeyboardEventArgs e)
        {
            // Chequea que tecla es presionada y cambia la animaci�n
            switch(e.Key)
            {                
                case Key.LeftArrow:
                    hero.CurrentAnimation = "WalkLeft";
                    hero.Animate = true;
                    break;
                case Key.RightArrow:
                    hero.CurrentAnimation = "WalkRight";
                    hero.Animate = true;
                    break;
                case Key.DownArrow:
                    hero.CurrentAnimation = "WalkDown";
                    hero.Animate = true;
                    break;
                case Key.UpArrow:
                    hero.CurrentAnimation = "WalkUp";
                    hero.Animate = true;
                    break;
                case Key.Escape:
                    Events.QuitApplication();
                    break;
            }                            
        }

        private void Events_KeyboardUp(object sender, KeyboardEventArgs e)
        {
            // Al soltar la tecla deja de moverse Hero
            if(e.Key == Key.LeftArrow && hero.CurrentAnimation == "WalkLeft")
                hero.Animate = false;
            else if(e.Key == Key.UpArrow && hero.CurrentAnimation == "WalkUp")
                hero.Animate = false;
            else if(e.Key == Key.DownArrow && hero.CurrentAnimation == "WalkDown")
                hero.Animate = false;
            else if(e.Key == Key.RightArrow && hero.CurrentAnimation == "WalkRight")
                hero.Animate = false;
        }

        [STAThread]
        public static void Main()
        {
            cap4 juego = new cap4();
            juego.Run();
        }
    }	
}
</pre></td></tr></table>



<br><hr style="width: 100%; height: 2px;"><br>
C�digo fuente de este cap�tulo para VS2008 y SDL 6.1. Tiene una peque�os cambios pero la base es la misma:

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
//Tutorial de SDL.NET
//Archivo: cap4.cs
//Autor: Dark-N
//Fecha: 13-06-2011
//Capitulo4: Animaci�n b�sica de Sprites. Ahora usando VS2008 con SDL.NET 6.1

using System;
using System.Drawing;
using SdlDotNet;
using SdlDotNet.Graphics; //para Sprites y surface Video
using SdlDotNet.Core; //Eventos Tick y Quit
using SdlDotNet.Input; //eventos teclado
using SdlDotNet.Graphics.Sprites;  //textos y graficos

namespace tutorial
{	
	public class cap4
	{
		int resx = 400;
		int resy = 300;
		
		private AnimatedSprite hero = new AnimatedSprite();
		private SdlDotNet.Graphics.Font fuente = new SdlDotNet.Graphics.Font("../../fuentes/ARIAL.TTF", 14);  
	
		//Constructor
		public cap4()
		{			
			Video.SetVideoMode(resx, resy);			
			Video.WindowCaption = "Tutorial SDL: Capitulo 4 - VS2008 - SDL 6.1";
			
			
			// Se carga la imagen
			Surface image = new Surface("../../Imagenes/Hero.png");
    
			// Se crean los Frames de Animaci�n
			SurfaceCollection walkUp = new SurfaceCollection();
			SurfaceCollection walkRight = new SurfaceCollection();
			SurfaceCollection walkDown = new SurfaceCollection();
			SurfaceCollection walkLeft = new SurfaceCollection();
			walkUp.Add(image, new Size(24,32), 0);
			walkRight.Add(image, new Size(24, 32), 1);
			walkDown.Add(image, new Size(24, 32), 2);			
			walkLeft.Add(image, new Size(24, 32), 3);
			
			AnimationCollection an1 = new AnimationCollection();
			AnimationCollection an2 = new AnimationCollection();
			AnimationCollection an3 = new AnimationCollection();
			AnimationCollection an4 = new AnimationCollection();
			
			an1.Add(walkUp, 35);
			an2.Add(walkRight, 35);
			an3.Add(walkDown, 35);
			an4.Add(walkLeft, 35);

			// Agregamos las animaciones al objeto hero
			hero.Animations.Add("WalkUp", an1);
			hero.Animations.Add("WalkRight", an2);
			hero.Animations.Add("WalkDown", an3);
			hero.Animations.Add("WalkLeft", an4);

			// Pintamos transparente el fondo del sprite
			hero.TransparentColor = Color.Magenta;
			hero.Transparent = true;

			// Seteamos la animacion inicial y que no se mueva
			hero.CurrentAnimation = "WalkDown";
			hero.Animate = false;

			// Lo colocamos en el centro de la pantalla
			hero.Center = new Point(Video.Screen.Width / 2, Video.Screen.Height / 2);
		}

		private void Events_Tick(object sender, TickEventArgs e)
		{
			Video.Screen.Fill(Color.Black);
			Video.Screen.Blit(hero);
			TextSprite texto = new TextSprite("Ejemplo Animaci�n de Sprite", fuente, Color.Yellow, new Point(0,0));
			Video.Screen.Blit(texto);
			Video.Screen.Update();
			juego();
		}

		// l�gica de nuestro juego
		private void juego() 
		{		
			// Si Hero esta animado, el camina, asi que se mueve
			if(hero.Animate)
			{
				switch(hero.CurrentAnimation)
				{
					case "WalkLeft":
						// 2 es la velocidad cuando camina
						hero.X -= 2;
						break;
					case "WalkUp":
						hero.Y -= 2;
						break;
					case "WalkDown":
						hero.Y += 2;
						break;
					case "WalkRight":
						hero.X += 2;
						break;
				}
			}
		}		

		public void Run()
		{	
			Events.Fps = 60;
			Events.Tick += new EventHandler<TickEventArgs>(this.Events_Tick);
			Events.KeyboardDown += new EventHandler<KeyboardEventArgs>(Events_KeyboardDown);
			Events.KeyboardUp += new EventHandler<KeyboardEventArgs>(Events_KeyboardUp);
			Events.Quit += new EventHandler<QuitEventArgs>(this.Events_Salir);
			Events.Run();
		}

		private void Events_Salir(object sender, QuitEventArgs e)
		{
			Events.QuitApplication();
		}

		private void Events_KeyboardDown(object sender, KeyboardEventArgs e)
		{
			// Chequea que tecla es presionada y cambia la animaci�n
			switch(e.Key)
			{				
				case Key.LeftArrow:
					hero.CurrentAnimation = "WalkLeft";
					hero.Animate = true;
					break;
				case Key.RightArrow:
					hero.CurrentAnimation = "WalkRight";
					hero.Animate = true;
					break;
				case Key.DownArrow:
					hero.CurrentAnimation = "WalkDown";
					hero.Animate = true;
					break;
				case Key.UpArrow:
					hero.CurrentAnimation = "WalkUp";
					hero.Animate = true;
					break;
				case Key.Escape:
					Events.QuitApplication();
					break;
			}							
		}

		private void Events_KeyboardUp(object sender, KeyboardEventArgs e)
		{
			// Al soltar la tecla deja de moverse Hero
			if(e.Key == Key.LeftArrow && hero.CurrentAnimation == "WalkLeft")
				hero.Animate = false;
			else if(e.Key == Key.UpArrow && hero.CurrentAnimation == "WalkUp")
				hero.Animate = false;
			else if(e.Key == Key.DownArrow && hero.CurrentAnimation == "WalkDown")
				hero.Animate = false;
			else if(e.Key == Key.RightArrow && hero.CurrentAnimation == "WalkRight")
				hero.Animate = false;
		}

		[STAThread]
		public static void Main()
		{
			cap4 juego = new cap4();
			juego.Run();
		}
	}
	
}
</pre></td></tr></table>

<span style="font-family: Verdana;"><small>

<font size="2">
<br>Fuentes: <br><br>
<li><a href="proyectos/VS2003_SDL4.0_cap1-4.rar">Proyecto VS2003 usando SDL.NET 4.0 - Cap 1 al 4.</a>
<li><a href="proyectos/VS2008_SDL6.1_cap4.rar">Proyecto VS2008 usando SDL.NET 6.1 - Solo cap 4.</a>
<br>
</font> </small></span>
<br><hr style="width: 100%; height: 2px;"><br>
<a href="cap3.php">Anterior</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap5-1.php">Siguiente</a>

<?php
include '../../piecdisq.php';
?>
