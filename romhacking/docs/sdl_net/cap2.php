<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>Tutorial de Creaci�n de Video Juegos con SDL .NET</title>
</head>


<body>
<small>
<span style="font-family: Verdana;">
<a href="cap1.php">Anterior</a> | <a hre.phpf="SDL_NET_menu.php">�ndice</a> | <a href="cap3.php">Siguiente</a>
</small>

<hr style="width: 100%; height: 2px;">

<table width="100%">
<tr>
<td>
<big>
<span style="font-weight: bold;">Cap�tulo 2: Mover Sprites</span>
</big>
<small>
<br style="font-weight: bold;"> 
Lenguaje: C#<br>
Para:  VS2003<br>
Por Dark-N: <?php include '../../mail.php'; ?>
<br>

<span style="font-family: Verdana;">
<a href="http://darknromhacking.com/">http://darknromhacking.com</a></small></td>
<td align="center">
<img src="imagenes/sdlnet.gif">
</table>

<hr style="width: 100%; height: 2px;"><br>

<span style="font-family: Verdana;"><small>

<b>Objetivo:</b> Mover un Sprite por la pantalla con el teclado.<br><br> 

Un <b>Sprite</b> un <b>objeto 2D</b> como una imagen por ejemplo. Los Sprites tienen 2 propiedades b�sicas <b>Sprite.X</b> y <b>Sprite.Y</b> lo que nos permite setear coordenadas al Sprite y como el game-loop es un ciclo infinito de renders, ponemos aumentar o disminuir estos X, Y y el objeto se ver� en movimiento. <br>
Lo que explicar� ahora, ser� menos detallado que antes, pero pondr� m�s c�digo, de manera de avanzar m�s r�pido.

<br><br>Lo primero es agregar esta imagen a la Carpeta <b>Imagenes</b> del proyecto: 
<br><br><img src="imagenes/leon.PNG"><br><br>

Lo que sigue es crear un nueva clase llamada "cap2.cs" de la misma forma que se explic� en el cap�tulo anterior:

<br><br><img src="imagenes/cap2_imagen.PNG"><br><br>

Ahora <b>copia</b> todo el c�digo de la clase <b>cap1.cs</b> y p�galo en <b>cap2.cs</b>, ya que nos basaremos en esa estructura para programar. Ahora cambia todo lo que diga cap1 por cap2.

En el c�digo ahora hay que agregar un objeto <b>Surface</b> que contenga la imagen, el c�digo es:
<br><table><tr><td bgcolor="#CCCCCC"><pre>
Surface sur_leon = new Surface(@"..\..\Imagenes\leon.PNG");
//En C# el @ en un string es para indicarle al programa que lo que 
//viene es una URL o ruta de acceso
</pre></td></tr></table><br>


Luego un Sprite carga ese Surface, y se posiciona en el centro de la pantalla con:
<br><table><tr><td bgcolor="#CCCCCC"><pre>
Sprite  spr_leon = new Sprite(sur_leon);
spr_leon.Center = new Point(Video.Screen.Width/2,Video.Screen.Height/2);
</pre></td></tr></table><br>

Despu�s se renderea en pantalla con:
<br><table><tr><td bgcolor="#CCCCCC"><pre>
spr_leon.Render(Video.Screen);
</pre></td></tr></table><br>

Aunque igualmente se puede usar el m�todo <b>Blit</b> del Surface principal que pinta algo en pantalla:
<br><table><tr><td bgcolor="#CCCCCC"><pre>
Video.Screen.Blit(spr_leon, new Point(0,0));
</pre></td></tr></table><br>

Con esto ya se ver�a la imagen en pantalla, solo falta darle movimiento con el <b>Teclado</b>, si presionamos la fecha arriba se mueve hacia arriba, abajo se mueve abajo y as� con las 4 direcciones.
<br>Entonces usamos el <b>Evento</b> propio de SDL .NET para manejo de teclado llamado <b> SdlDotNet.Keyboard.IsKeyPressed(tecla)</b> que se gatilla al Presionar las Teclas del teclado. Entonces en la parte del <b>juego()</b> ponemos el c�digo que mover� el Sprite 5 pixels hacia la direcci�n que presiones en el teclado: <br>

<br><table><tr><td bgcolor="#CCCCCC"><pre>
if (SdlDotNet.Keyboard.IsKeyPressed(Key.UpArrow))
  spr_leon.Y-=5;

if (SdlDotNet.Keyboard.IsKeyPressed(Key.DownArrow))
  spr_leon.Y+=5;

if (SdlDotNet.Keyboard.IsKeyPressed(Key.LeftArrow))
  spr_leon.X-=5;

if (SdlDotNet.Keyboard.IsKeyPressed(Key.RightArrow))
  spr_leon.X+=5;
</pre></td></tr></table><br>

Notar que la resoluci�n en pantalla es (0,0) desde la esquina izquierda, llegando a (400,300) en la inferior derecha:
<br><br><img src="imagenes/res_x_y.PNG"><br><br>

Entonces para mover objetos en pantalla debes seguir esta regla:
<li>Si quieres <b>subir</b> un Sprite debes <b>aumentar</b> el valor de la propiedad Y del Sprite.</i>
<li>Si quieres <b>bajar</b> un Sprite debes <b>disminuir</b> el valor de la propiedad Y del Sprite.</i>
<li>Si quieres <b>mover a la derecha</b> un Sprite debes <b>aumentar</b> el valor de la propiedad X del Sprite.</i>
<li>Si quieres <b>mover a la izquierda</b> un Sprite debes <b>reducir</b> el valor de la propiedad X del Sprite.</i>

<br><br>Ahora para compilar, como tanto cap1.cs y cap2.cs tiene un objeto <b>Main</b>:

<br><table><tr><td bgcolor="#CCCCCC"><pre>
[STAThread]
public static void Main()
...
</pre></td></tr></table><br>

Entonces hay que decirle al .NET cual de las 2 clases tomar� el c�digo para generar el .EXE. Con Rebuild compila AMBOS en busca de errores, pero como est�n dentro de un mismo <b>Proyecto</b> solo un elemento debe ser la salida. Para esto de configura en las propiedades del Proyecto, como muestra la imagen:

<br><br><img src="imagenes/startup_object.PNG"><br><br>

Como ahora trabajaremos con cap2.cs entonces dejamos este �tem seleccionado en las propiedades.
<br>Una vez hecho esto, podemos hacer el <b>Rebuild</b> para que .NET compile y vea si hay errores. Tambi�n construye un ejecutable.

<br><br>Finalmente se testea lo que se hizo, presionando <b>F5</b> y nos muestra en pantalla:

<br><br><img src="imagenes/cap2.PNG"><br><br>

Mueve las teclas y se mover� el le�n.

<br><br>Recuerda RENOMBRAR el archivo <b>tutorial.exe</b> a <b>cap2_mover_sprite.exe</b> para que en el siguiente cap�tulo no lo sobreescribas:

<br><br><img src="imagenes/renombrar.PNG"><br><br>


<br><hr style="width: 100%; height: 2px;"><br>
C�digo fuente de este cap�tulo:
<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
//Archivo: cap2.cs
//Autor: Dark-N
using System;
using System.Drawing;
using SdlDotNet;
using SdlDotNet.Sprites;


namespace tutorial
{
    
    public class cap2
    {
        Surface sur_leon;
        Sprite  spr_leon;
        TextSprite texto;
        
        public cap2()
        {
            //modo ventana, resoluci�n 400x300
            Video.SetVideoModeWindow(400, 300);

            //surface leon
            sur_leon = new Surface(@"..\..\Imagenes\leon.PNG");

            //sprite leon en el centro de la pantalla
            spr_leon = new Sprite(sur_leon);

            //leon en el centro de la pantalla
            spr_leon.Center = new Point(Video.Screen.Width/2,Video.Screen.Height/2);
            

            //creamos un texto
            SdlDotNet.Font fuente = new SdlDotNet.Font("../../fuentes/ARIAL.TTF", 14);        
            texto = new TextSprite("Prueba de Teclas", fuente, Color.Yellow, new Point(5,5));
            
            
            Events.Tick += new TickEventHandler(Events_Tick);
        }

        private void Events_Tick(object sender, TickEventArgs e)
        {

            // Limpar la pantalla (dejarla negra)
            Video.Screen.Fill(Color.Black);

            //aqu� LOGICA del nuestro juego
            juego();

            
            // Se actualiza la pantalla
            Video.Screen.Update();
        }

        // l�gica de nuestro juego
        private void juego() 
        {
                        
            texto.Render(Video.Screen);

            //OJO: tambien se puede usar BLIT
            //Video.Screen.Blit(texto);

            //al presionar las teclas del teclado se mueve el Sprite
            if (SdlDotNet.Keyboard.IsKeyPressed(Key.UpArrow))
                spr_leon.Y-=5;

            if (SdlDotNet.Keyboard.IsKeyPressed(Key.DownArrow))
                spr_leon.Y+=5;

            if (SdlDotNet.Keyboard.IsKeyPressed(Key.LeftArrow))
                spr_leon.X-=5;

            if (SdlDotNet.Keyboard.IsKeyPressed(Key.RightArrow))
                spr_leon.X+=5;

            //igual se puede Video.Screen.Blit(spr_leon, new Point(0,0));
            spr_leon.Render(Video.Screen);
            
        }        

        public void Run()
        {    
            Events.KeyboardDown +=new KeyboardEventHandler(this.Keyboard);
            Events.Run();
        }

        private void Keyboard(object sender, KeyboardEventArgs e)
        {    
            //Salimos del juego con tecla Escape
            if(e.Key == Key.Escape)
            {
                Events.QuitApplication();
            }        
        }

        [STAThread]
        public static void Main()
        {
            cap2 juego = new cap2();
            juego.Run();
        }
    }
    
}
</pre></td></tr></table><br>

<span style="font-family: Verdana;"><small>

	<font size="2"><a href="proyectos/VS2003_SDL4.0_cap1-4.rar">Fuentes: Bajar 
	Proyecto VS2003 + SDL.NET 4.0 Cap 1 al 4.</a></font> </small></span>

<br><br><hr style="width: 100%; height: 2px;"><br>
<a href="cap1.php">Anterior</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap3.php">Siguiente</a>

<?php
include '../../piecdisq.php';
?>
