<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>Tutorial de Creaci�n de Video Juegos con SDL .NET</title>
</head>


<body>
<small>
<span style="font-family: Verdana;">
<a href="cap5-1.php">Anterior</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap7.php">Siguiente</a>
</small>

<hr style="width: 100%; height: 2px;">

<table width="100%">
<tr>
<td>
<big>
<span style="font-weight: bold;">Cap�tulo 6: Simple efecto de FadeIn y FadeOut</span>
</big>
<small>
<br style="font-weight: bold;"> 
Lenguaje: C#<br>
Para:  VS2003 / VS2005 / VS2008<br>
Por Dark-N: <?php include '../../mail.php'; ?>
<br>
</small>
<span style="font-family: Verdana;">
<small>
<a href="http://darknromhacking.com/">http://darknromhacking.com</a></small></td>
<td align="center">
<img src="imagenes/sdlnet.gif">
</table>

<hr style="width: 100%; height: 2px;"><br>

<span style="font-family: Verdana;"><small>
<b>Objetivo:</b> hacer un sencillo efecto de aparici�n y desaparici�n de una figura usando la propiedad Alpha.
<br><br>
Lo primero es descargar estas 2 imagenes: <br><br>
<img src="imagenes/Triangulo.PNG">   <img src="imagenes/Cuadrado.PNG">
<br><br>
Estas im�genes las debes dejar en el directorio <b>bin/Debug</b> una vez compilado el proyecto.

<br><br>Lo que haremos ser� cargar 2 im�genes, una que quedar� fija (el triangulo) y otra que ir� apareciendo de a poco y luego desaparecer� (cuadrado) en un ciclo. <br>
Para manejar transparencias en SDL.Net est� la propiedad <b>Alpha</b> que indica el grado de opacidad de una superficie (Surface). Este grado puede variar entre 0 (no se ve) y 255 (se ve complemente). Esto ser� muy �tiles para hacer efectos mas delicados al monento de hacer desaparecer objetos de la pantalla.
<br>
Es recomendable usar imagenes <b>PNG</b> en vez de BMP o JPG, ya que soportan <b>Canales Alpha</b> y son relativemente m�s peque�as.
<br><br>
La forma de usarlo en el c�digo es:

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
srf_cuadrado = new Surface(@"mi imagen.PNG").Convert(mi superficie, true, false);
srf_cuadrado.Alpha = 0;
srf_cuadrado.AlphaBlending = true;
srf_cuadrado.TransparentColor = Color.White;
</pre></td></tr></table><br>
Vamos l�nea a l�nea:<br>
<b>srf_cuadrado = new Surface(@"mi imagen.PNG").Convert(superficie, true, false);</b><br>
Cargamos una imagen y la convertimos de inmediato en el formato de video que usaremos esto es porque el archivo a cargar puede que no tenga el mismo formato (profundidad de pixels) como el de nuestra surface. Es m�s efeciente hacer esta conversi�n al principio una una vez, que esperar que en cada Blit la haga.
<br><br>
Los otros campos a definir son:<br> 
<li>Hardware Surace: True - Creamos la nueva superficie en la Memoria de Video de la trajeta de video. Siempre deber�a en True.
<li>Alpha Blending: False - No usaremos Alpha Blending en la superficie (por ahora).

<br><br><b>srf_cuadrado.Alpha = 0;</b><br>
0 = invisible, 255=totalmente visible

<br><br><b>srf_cuadrado.AlphaBlending = true;</b><br>
fuera de indicarle el valor de Alpha, debemos decirle al SDL expl�tamente que queremos usar Alpha en la superficie, para esto se deja este campo en true.

<br><br><b>srf_cuadrado.TransparentColor = Color.White;</b><br>
Le decimos que queremos dejar la superfice con el cuadrado azul, transparente el color blanco, esto es porque queremos poner una imagen sobre otra. En este caso el triangulo paracer� atr�s del cuadrado. Una alternativa a Color.White es usar Color.FromArgb( 255, 255, 255 ).

<br><br>Creo que esto es lo �nico a destacar por ahora. Lo otro es conocido por uds.

<br><hr style="width: 100%; height: 2px;"><br>
C�digo fuente de este cap�tulo (VS2003 + SDL.NET 4.0):

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
/*
  Ejemplo usando SDL.NET 4.0 y VS 2003 con C#
  Muestra una figura que se aparece con el efecto "fade-in" y "fade-out"
  Esto se logra usando la propiedad Alpha de una Surface cargado con una imagen .PNG
  
  Alpha: es el grado de opacidad o transaparencia de una objeto, 0: no se 
  ve el objeto y 255: se ve completamente
  La propiedad Alpha est� definida en bytes por lo que se puede escribir en 
  bytes (rango 0x00 a 0xFF) o en enteros (rango 0 a 255)
    
  Autor: Dark-N
  Fecha: 13-05-2009
*/

using System;
using SdlDotNet;
using SdlDotNet.Sprites;
using System.Drawing;

namespace FadeInOut
{

    public class FadeInOut
    {
        private static Surface srf_videoScreen;
        private static Surface srf_cuadrado, srf_triangulo;    
        private static int estadoBrillo = 0;

        private enum enumVisibilidad : int
        {
            MasVisible = 1,
            MenosVisible = 2
        }

        public static void Main()
        {
            srf_videoScreen = Video.SetVideoModeWindow(220, 200, 32);
            Video.WindowCaption = "Fade In y Fade Out";            
            Events.Fps = 25;

            CargaInicial();

            Events.Quit += new QuitEventHandler(Evento_Salir);
            Events.Tick += new TickEventHandler(Evento_Tick);
            Events.KeyboardDown +=new KeyboardEventHandler(TecladoManejador);
            Events.KeyboardUp +=new KeyboardEventHandler(TecladoManejador);    
            Events.Run();
        }

        static void CargaInicial()
        {
            srf_cuadrado = new Surface( @"Cuadrado.PNG").Convert(srf_videoScreen, true, false);
            srf_cuadrado.Alpha = 0; //0-invisible, 255-completamente visible
            srf_cuadrado.AlphaBlending = true;
            srf_cuadrado.TransparentColor = Color.White;

            srf_triangulo = new Surface( @"Triangulo.PNG").Convert(srf_videoScreen, true, false);
            srf_triangulo.TransparentColor = Color.White;

            estadoBrillo = (int) enumVisibilidad.MasVisible;
        }

        private static void Evento_Salir(object sender, SdlDotNet.QuitEventArgs e)
        {
            Events.QuitApplication();
        }


        private static void TecladoManejador( object sender, KeyboardEventArgs args )
        {
            if (args.Key == Key.Escape)
                Events.QuitApplication();
        }

        private static void Evento_Tick( object sender, TickEventArgs args )
        {        
            srf_videoScreen.Fill(Color.White);

            if (srf_cuadrado.Alpha <= 0)
                estadoBrillo = (int) enumVisibilidad.MasVisible;

            if (srf_cuadrado.Alpha >= 255)
                estadoBrillo = (int) enumVisibilidad.MenosVisible;
 
            if (estadoBrillo == (int) enumVisibilidad.MasVisible)
                srf_cuadrado.Alpha += 0x05;
            else
                srf_cuadrado.Alpha -= 0x05;            

            srf_videoScreen.Blit(srf_cuadrado, new Point( 0, 0 ));
            srf_videoScreen.Blit(srf_triangulo, new Point( 20, 20 )); //el triangulo se ve por encima del cuadrado
            srf_videoScreen.Update();
        }
    }
}
</pre></td></tr></table>
<br>




<br><hr style="width: 100%; height: 2px;"><br>

C�digo fuente para <b>VS 2005/2008</b>. Antes recuerda: <br>
<li>Agregar las referencias: SDL.Net 6.0 y System.Drawing.
<li>En las propiedades del Proyecto, en Output Type dejarlo como <b><a href="imagenes/vs2008-2.PNG">Windows Application</a></b>
<li>Copiar las imagenes que coloqu� arriba en la carpeta <b><a href="imagenes/cap6-2.PNG">bin/Debug</a></b> del proyecto.
<br><br>C�digo fuente VS 2008 + SDL.NET 6.0:

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
//Archivo: HolaMundo.cs
//Autor: Dark-N
//fecha: 25-05-2009

using System;
using System.Collections.Generic; //para eventos y otras cosas
using System.Drawing; //colores

using SdlDotNet.Graphics; //para sprites y surfaces
using SdlDotNet.Core; //Eventos
using SdlDotNet.Input; //keyborad

namespace FadeInOut
{
    public class Figura
    {
        private static Surface srf_videoScreen;
        private static Surface srf_cuadrado, srf_triangulo;    
        private static int estadoBrillo = 0;

        private enum enumVisibilidad : int
        {
            MasVisible = 1,
            MenosVisible = 2
        }

        public static void Main()
        {
            srf_videoScreen = Video.SetVideoMode(220, 200, false, false, false, true);
            Video.WindowCaption = "Fade In y Fade Out";
            SdlDotNet.Core.Events.Fps = 25; //25 veces se llamar� el tick por segundo
            
            CargaInicial();
	    
            SdlDotNet.Core.Events.KeyboardDown += new EventHandler < KeyboardEventArgs >(Evento_Teclado);
            SdlDotNet.Core.Events.Tick += new EventHandler < TickEventArgs >(Evento_Tick);
            SdlDotNet.Core.Events.Quit += new EventHandler < QuitEventArgs >(Evento_Salir);
            Events.Run();
        }
        
        static void CargaInicial()
        {
            srf_cuadrado = new Surface(@"Cuadrado.PNG").Convert(srf_videoScreen, true, false);
            srf_cuadrado.Alpha = 0; //0-invisible, 255-completamente visible
            srf_cuadrado.AlphaBlending = true;
            srf_cuadrado.TransparentColor = Color.White;

            srf_triangulo = new Surface(@"Triangulo.PNG").Convert(srf_videoScreen, true, false);
            srf_triangulo.Transparent = true;
            srf_triangulo.TransparentColor = Color.White;            
           
            estadoBrillo = (int)enumVisibilidad.MasVisible;
        }

        private static void Evento_Salir(object sender, QuitEventArgs e)
        {
            Events.QuitApplication();
        }

        private static void Evento_Teclado(object sender, KeyboardEventArgs e)
        {
            if (e.Key == Key.Escape)
                Events.QuitApplication(); 
        }

        private static void Evento_Tick(object sender, TickEventArgs args)
        {
            srf_videoScreen.Fill(Color.White);

            if (srf_cuadrado.Alpha <= 0)
                estadoBrillo = (int)enumVisibilidad.MasVisible;

            if (srf_cuadrado.Alpha >= 255)
                estadoBrillo = (int)enumVisibilidad.MenosVisible;

            if (estadoBrillo == (int)enumVisibilidad.MasVisible)
                srf_cuadrado.Alpha += 0x05;
            else
                srf_cuadrado.Alpha -= 0x05;

            srf_videoScreen.Blit(srf_cuadrado, new Point(0, 0));            
            srf_videoScreen.Blit(srf_triangulo, new Point(20, 20)); //el triangulo se ve por encima del cuadrado
            srf_videoScreen.Update();
        }
    }
}
</pre></td></tr></table><br>

Una vez codificado, tanto en VS2003 o VS2005/2008, si al compilar no arroja errores, debe aparecerte algo como esto:<br><br>
<img src="imagenes/cap6.PNG">

<br><br>Bajar proyecto para VS 2003 desde <a href="proyectos/VS2003_SDL4.0_cap6.zip">aqu�</a>.
<br><br>Bajar proyecto para VS 2008 desde <a href="proyectos/VS2008_SDL6.0_cap6.zip">aqu�</a>.


<br><hr style="width: 100%; height: 2px;"><br>
<a href="cap5-1.php">Anterior</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap7.php">Siguiente</a>

<?php
include '../../piecdisq.php';
?>

