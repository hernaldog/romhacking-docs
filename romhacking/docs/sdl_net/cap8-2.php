<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>Tutorial de Creaci�n de Video Juegos con SDL .NET</title>
</head>


<body>
<small>
<span style="font-family: Verdana;">
<a href="cap7.php">Anterior</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap9.php">Siguiente</a>
</small>

<hr style="width: 100%; height: 2px;">

<table width="100%">
<tr>
<td>
<big>
<span style="font-weight: bold;">Cap�tulo 8: Part�culas - Explosi�n</span>
</big>
<small>
<br style="font-weight: bold;"> 
Lenguaje: C#<br>
Para: VS2005 / VS2008 / SharpDevelop 2.2<br>
Por Dark-N: <?php include '../../mail.php'; ?>
<br>

<span style="font-family: Verdana;">
<a href="http://darknromhacking.com/">http://darknromhacking.com</a>
<br><br><a href="cap8-1.php">Ir a Parte 1</a> | <a href="cap8-3.php">Ir a Parte 3</a></td>

</td>

<td align="center">
<img src="imagenes/sdlnet.gif">
</table>

<hr style="width: 100%; height: 2px;"><br>

<span style="font-family: Verdana;"><small>
<b>Objetivo:</b> En este cap�tulo, un emitidor que simula una explosi�n. 
<br><br>Nota: Para todo el t�pico de part�culas use el c�digo libre hecho por uno de los programadores de SDL.Net <a href="http://cs-sdl.sourceforge.net/index.php/User:Rob_Loach">Rob Loach</a>.

<br><br>

La clave de una explosi�n de part�culas est� en al m�todo <b>ParticleCircleEmitter</b>, esta se inicializa as�:

<br><table><tr><td bgcolor="#CCCCCC"><pre>
ParticleSystem miParticula = new ParticleSystem();
ParticleCircleEmitter explosion = new ParticleCircleEmitter(miParticula, colorPrincipal, ColorSecundario,radioMinimo, radioMaximo);
</pre></td></tr></table>

<br>Tambi�n, esta clase tiene los atributos:
<li><b>Life</b>: cantidad de particulas por cada frecuencia
<li><b>SpeedMin y SpeedMax</b>: rango de velocidad de la explosi�n
<li><b>Frequency</b>: a mayor frecuencia, mayor explosion y mas duradera. Recomendado valores sobre 500 para que se vea algo

<br><br>Ejemplo de una explosi�n en la pocisi�n 100,200 de la pantalla: <br>

<br><table><tr><td bgcolor="#CCCCCC"><pre>
ParticleCircleEmitter explosion = new ParticleCircleEmitter(particles, Color.Red, Color.Orange, 1, 2);
explosion.X = 100; // donde explotar�
explosion.Y = 200;
explosion.Life = 3; 
</pre></td></tr></table>


<br><hr style="width: 100%; height: 2px;"><br>
C�digo fuente de est� secci�n (VS2005/V2008):

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
#region LICENSE
/*
 * $RCSfile: ParticlesExample.cs,v $
 * Copyright (C) 2005 Rob Loach (http://www.robloach.net)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#endregion LICENSE

using System;
using System.IO;
using System.Drawing;

using SdlDotNet.Particles;
using SdlDotNet.Particles.Emitters;
using SdlDotNet.Particles.Manipulators;
using SdlDotNet.Graphics.Sprites;
using SdlDotNet.Core;
using SdlDotNet.Input;
using SdlDotNet.Graphics;

namespace EjemploParticulas
{

	public class Ejemplo2 : IDisposable
	{
		
		ParticleSystem particles = new ParticleSystem();		

		public Ejemplo2()
		{			
			Video.WindowIcon();
			Video.WindowCaption = "SDL.NET - Explosion de Particulas (Haz clic en la pantalla)";
			Video.SetVideoMode(400, 300);
			Events.KeyboardDown += new EventHandler<KeyboardEventArgs>(this.KeyboardDown);
			Events.MouseButtonDown += new EventHandler<MouseButtonEventArgs>(this.MouseButtonDown);			
			Events.Fps = 30;
			Events.Tick += new EventHandler<TickEventArgs>(this.Tick);
			Events.Quit += new EventHandler<QuitEventArgs>(this.Quit);
		}

		public void Go()
		{			
			Events.Run();
		}

		[STAThread]
		public static void Main()
		{
			Ejemplo2 particula = new Ejemplo2();
			particula.Go();
		}
		
		private void Tick(object sender, TickEventArgs e)
		{			
			particles.Update();			
			Video.Screen.Fill(Color.Black);
			particles.Render(Video.Screen);
			Video.Screen.Update();			
		}

		private void KeyboardDown(object sender, KeyboardEventArgs e)
		{
			if (e.Key == Key.Escape)			
				Events.QuitApplication();						
		}

		private void Quit(object sender, QuitEventArgs e)
		{
			Events.QuitApplication();
		}
		
		private void MouseButtonDown(object sender, MouseButtonEventArgs e)
		{
			//se define la part�cula con el color primario y secundario y el radio de cada particula
			ParticleCircleEmitter explosion = new ParticleCircleEmitter(particles, Color.Red, Color.Orange, 1, 2);
			explosion.X = e.X; // donde explotar�
			explosion.Y = e.Y;
			explosion.Life = 10;
			explosion.Frequency = 10000; 
			explosion.LifeMin = 5;
			explosion.LifeMax = 20;
			explosion.LifeFullMin = 5;
			explosion.LifeFullMax = 5;
			explosion.SpeedMin = 8;
			explosion.SpeedMax = 20;
		}			
		

		#region IDisposable Members

		private bool disposed;

		/// <summary>
		/// Destroy object
		/// </summary>
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Destroy object
		/// </summary>
		public void Close()
		{
			Dispose();
		}

		/// <summary>
		/// Destroy object
		/// </summary>
		~Ejemplo2()
		{
			Dispose(false);
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="disposing"></param>
		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{				
				this.disposed = true;
			}
		}

		#endregion
	}
}
</pre></td></tr></table>
<br>
Una vez codificado, si al compilar no arroja errores, debe aparecerte algo como esto al hacer clic en la pantalla:<br><br>
<img src="imagenes/cap82.PNG">

<br><br>Bajar proyecto para SharpDevelop 2.2 (lo siento, esta vez no lo hice en VS.NET ;) <a href="proyectos/VS2008_SDL6.0_cap8.zip">aqu�</a>.

<br><br>
<a href="cap8-1.php">Ir a Parte 1</a> | <a href="cap8-3.php">Ir a Parte 3</a>

<br><hr style="width: 100%; height: 2px;"><br>
<a href="cap8-1.php">Anterior</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap8-3.php">Siguiente</a>

<?php
include '../../piecdisq.php';
?>
