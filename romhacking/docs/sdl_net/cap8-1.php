<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>Tutorial de Creaci�n de Video Juegos con SDL .NET</title>
</head>


<body>
<small>
<span style="font-family: Verdana;">
<a href="cap7.php">Anterior</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap9.php">Siguiente</a>
</small>

<hr style="width: 100%; height: 2px;">

<table width="100%">
<tr>
<td>
<big>
<span style="font-weight: bold;">Cap�tulo 8: Part�culas - Un emitidor b�sico</span>
</big>
<small>
<br style="font-weight: bold;"> 
Lenguaje: C#<br>
Para: VS2005 / VS2008 / SharpDevelop 2.2<br>
Por Dark-N: <?php include '../../mail.php'; ?>
<br>

<span style="font-family: Verdana;">
<a href="http://darknromhacking.com/">http://darknromhacking.com</a>
<br><br><a href="cap8-2.php">Ir a Parte 2</a> | <a href="cap8-3.php">Ir a Parte 3</a>
</td>

</td>

<td align="center">
<img src="imagenes/sdlnet.gif">
</table>

<hr style="width: 100%; height: 2px;"><br>

<span style="font-family: Verdana;"><small>
<b>Objetivo:</b> Aprender a usar los diferentes sistemas de Part�culas. En este cap�tulo, un emitidor b�sico. 
<br><br>Nota: Para todo el t�pico de part�culas use el c�digo libre hecho por uno de los programadores de SDL.Net <a href="http://cs-sdl.sourceforge.net/index.php/User:Rob_Loach">Rob Loach</a>.

<br><br>
Las part�culas son objetos reales que se renderean en pantalla. Pueden ser cualquier cosa, un Surface, una imagen, un pixel, etc. En general nunca se muestran solas, sino que se usan en conjunto para simular explosiones o efectos gr�ficos de tornados, o lluvia por ejemplo. Las part�culas derivan de la clase <b>Particles.</b><br><br>

Un dato, tengo entendido que esta clase Particles solo est� disponible para SDL.Net 6.0 en adelante.

<br><br>
Veremos ahora 3 tipos de sistemas de part�culas disponibles  como son:
<li><b>ParticleRectangleEmitter</b>: Emitidor rectangular de particulas, es como un chorro de part�culas arrojadas.
<li><b>ParticleCircleEmitter</b>: part�culas que giran o salen disparadas en torno a un centro. Visiblemente son como fuegos artificiales.
<li><b>AnimatedSprite</b>: Son sprites (una imagen por ejemplo) que la trabajamos como una Part�cula. 
<li><b>ParticlePixel</b>: Es un pixel que se usa como una part�cula. Visiblemente se ve como un punto que se mueve. No tiene mucha gracia si no se le aplica un <b>Modificador</b> que la haga mas interesante.

<br><br>En esta primera parte usaremos solo <b>ParticleRectangleEmitter</b>, en las siguientes partes usaremos el resto.

<br><br>Los <b>Modificadores</b> son Clases especiales usadas por la clase Particle que cambian las part�culas en tiempo real. Por ejemplo hay manipuladores para que al acerca el mouse a la part�cula, esta se le quiera pegar, como si us�ramos un im�n. Hay otro manipulador que hace que las part�culas revoten con menos gravedad si haces alguna acci�n con el mouse o teclado.
<br>Sin Modificadores las part�culas ser�an muy toscas en su representaci�n gr�fica, adem�s los manipuladores permiten hacer un efecto visual m�s completo.
<br><br>
La forma de uso del manipulador es:<br>
<br><table><tr><td bgcolor="#CCCCCC"><pre>
miParticula.Manipulators.Add( clase manipulador grav);

Ejemplo:

ParticleGravity grav = new ParticleGravity(0.5f); //Clase manipuladora de la la gravedad
particles.Manipulators.Add(grav);
</pre></td></tr></table>

<br><br>Ahora vamos por lo principal, los Namespaces que incluiremos son:
<br><table><tr><td bgcolor="#CCCCCC"><pre>
using SdlDotNet.Particles;
using SdlDotNet.Particles.Emitters;
using SdlDotNet.Particles.Manipulators;
</pre></td></tr></table>

<br>Lo segundo es crear como variables globales una variable que englobar� todas nuestras part�culas y otra que represente el emitidor:

<br><table><tr><td bgcolor="#CCCCCC"><pre>
ParticleSystem particles = new ParticleSystem();
ParticleRectangleEmitter emit;
</pre></td></tr></table>

<br>Luego en inicializaci�n definimos las propiedades de la part�cula:

<br><table><tr><td bgcolor="#CCCCCC"><pre>
emit = new ParticleRectangleEmitter(particles);
emit.Frequency = 50000; // emitira 100000 particulas por cada 1000 updates.
emit.LifeFullMin = 20;
emit.LifeFullMax = 50;
emit.LifeMin = 10;
emit.LifeMax = 30;
emit.DirectionMin = -2;
emit.DirectionMax = -1;
emit.ColorMin = Color.DarkBlue; // el rango de colores
emit.ColorMax = Color.LightBlue;
emit.SpeedMin = 5; //tiene distintas velocidades
emit.SpeedMax = 20;
emit.MaxSize = new SizeF(5, 5);  //el rango de tama�o de las particulas
emit.MinSize = new SizeF(1, 1);
emit.X = Video.Screen.Width/2;   //aqu� situamos la emisi�n, al centro abajo
emit.Y = Video.Screen.Height;
</pre></td></tr></table>

<br>Agregamos manipuladores a las part�culas:
<li><b>ParticleGravity</b>: permite darle gravedad vertical a las part�culas, de esta forma cuando rebotan, lo hacen m�s lento.
<li><b>ParticleFriction</b>: estable una fricci�n para que la part�cula se mueva m�s lenta. Se carga un n�mero de punto flotante de 32 bits. Por ejemplo 0.5f.
<li><b>ParticleBoundary</b>: establece lo marcos donde vivir� la part�cula, en este caso ser� toda la pantalla (Video.Screen.Size)

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
ParticleGravity grav = new ParticleGravity(0.5f);
particles.Manipulators.Add(grav); // Gravedad de 0.5f
particles.Manipulators.Add(new ParticleFriction(0.1f)); // part�culas mas lentas
particles.Manipulators.Add(new ParticleBoundary(SdlDotNet.Graphics.Video.Screen.Size));
</pre></td></tr></table>

<br>Finalmente el evento Tick debe tener el <b>Update y Render</b> para la Clase que creamos que representa las part�culas y para la clase que representa nuetro emitidor. Por lo tanto quedar�a as�:

<br><table><tr><td bgcolor="#CCCCCC"><pre>
private void Tick(object sender, TickEventArgs e)
{
	// actualizamos todas las particulas
	particles.Update();
	emit.Target.Update();

	// se pinta toda la escena
	Video.Screen.Fill(Color.Black);

	// pintamos las particulas
	particles.Render(Video.Screen);
	emit.Target.Render(Video.Screen);    

	Video.Screen.Update();
}
</pre></td></tr></table>


<br><hr style="width: 100%; height: 2px;"><br>
C�digo fuente de est� secci�n (VS2005/V2008):

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
using System;
using System.IO;
using System.Drawing;

using SdlDotNet.Particles;
using SdlDotNet.Particles.Emitters;
using SdlDotNet.Particles.Manipulators;
using SdlDotNet.Graphics.Sprites;
using SdlDotNet.Core;
using SdlDotNet.Input;
using SdlDotNet.Graphics;

namespace EjemploParticulas
{

public class Ejemplo1 : IDisposable
{
    //creamos el sistema de part�culas
    ParticleSystem particles = new ParticleSystem();

    // creamos un emitidor que lanzar� particulas
    ParticleRectangleEmitter emit;
    

    public Ejemplo1()
    {            
        Video.WindowIcon();
        Video.WindowCaption = "SDL.NET - Emitidor de Particulas";
        Video.SetVideoMode(400, 300);
        Events.KeyboardDown += new EventHandler < KeyboardEventArgs > (this.KeyboardDown);
        Events.Fps = 30;
        Events.Tick += new EventHandler < TickEventArgs >(this.Tick);
        Events.Quit += new EventHandler < QuitEventArgs >(this.Quit);
    }


    public void Go()
    {            
        emit = new ParticleRectangleEmitter(particles);
        emit.Frequency = 50000; // emitira 100000 particulas por cada 1000 updates.
        emit.LifeFullMin = 20;
        emit.LifeFullMax = 50;
        emit.LifeMin = 10;
        emit.LifeMax = 30;
        emit.DirectionMin = -2;
        emit.DirectionMax = -1;
        emit.ColorMin = Color.DarkBlue; // es de 2 colores
        emit.ColorMax = Color.LightBlue;
        emit.SpeedMin = 5; //tiene distintas velocidades
        emit.SpeedMax = 20;
        emit.MaxSize = new SizeF(5, 5);  //las particulas tienen distito tama�o
        emit.MinSize = new SizeF(1, 1);
        emit.X = Video.Screen.Width/2;  //aqu� situamos la emisi�n, al centro abajo
        emit.Y = Video.Screen.Height;

        // agregamos manipuladores de las particulas
        ParticleGravity grav = new ParticleGravity(0.5f);
        particles.Manipulators.Add(grav); // Gravedad de 0.5f
        particles.Manipulators.Add(new ParticleFriction(0.1f)); // particulas mas lentas
        particles.Manipulators.Add(new ParticleBoundary(SdlDotNet.Graphics.Video.Screen.Size));

        Events.Run();
    }


    [STAThread]
    public static void Main()
    {
        Ejemplo1 particula = new Ejemplo1();
        particula.Go();
    }

    
    private void Tick(object sender, TickEventArgs e)
    {
        // actualizamos todas las particulas
        particles.Update();
        emit.Target.Update();

        // se pinta toda la escena
        Video.Screen.Fill(Color.Black);
        particles.Render(Video.Screen);
        emit.Target.Render(Video.Screen);    

        Video.Screen.Update();
        
    }

    private void KeyboardDown(object sender, KeyboardEventArgs e)
    {
        if (e.Key == Key.Escape)
        {
            Events.QuitApplication();
        }            
    }

    private void Quit(object sender, QuitEventArgs e)
    {
        Events.QuitApplication();
    }
    

    #region IDisposable Members

    private bool disposed;

    /// < summary >
    /// Destroy object
    /// </ summary >
    public void Dispose()
    {
        this.Dispose(true);
        GC.SuppressFinalize(this);
    }

    /// < summary >
    /// Destroy object
    /// </ summary >
    public void Close()
    {
        Dispose();
    }

    /// < summary >
    /// Destroy object
    /// </ summary >
    ~Ejemplo1()
    {
        Dispose(false);
    }
    /// < summary >
    /// 
    /// </ summary >
    /// < param name="disposing" ></ param >
    protected virtual void Dispose(bool disposing)
    {
        if (!this.disposed)
        {                
            this.disposed = true;
        }
    }

    #endregion
}
}
</pre></td></tr></table>


<br>
Una vez codificado, si al compilar no arroja errores, debe aparecerte algo como esto:<br><br>
<img src="imagenes/cap81.PNG">

<br><br>Bajar proyecto para SharpDevelop 2.2 (lo siento, esta vez no lo hice en VS.NET ;) <a href="proyectos/VS2008_SDL6.0_cap8.zip">aqu�</a>.

<br><br>
<a href="cap8-2.php">Ir a Parte 2</a> | <a href="cap8-3.php">Ir a Parte 3</a>

<br><hr style="width: 100%; height: 2px;"><br>
<a href="cap7.php">Anterior</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap9.php">Siguiente</a>

<?php
include '../../piecdisq.php';
?>
