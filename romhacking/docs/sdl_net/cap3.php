<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>Tutorial de Creaci�n de Video Juegos con SDL .NET</title>
</head>

<body>
<small>
<span style="font-family: Verdana;">
<a href="cap2.php">Anterior</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap4.php">Siguiente</a>
</small>

<hr style="width: 100%; height: 2px;">

<table width="100%">
<tr>
<td>
<big>
<span style="font-weight: bold;">Cap�tulo 3: Mouse y Sonidos</span>
</big>
<small>
<br style="font-weight: bold;"> 
Lenguaje: C#<br>
Para:  VS2003<br>
Por Dark-N: <?php include '../../mail.php'; ?>
<br>

<span style="font-family: Verdana;">
<a href="http://darknromhacking.com/">http://darknromhacking.com</a></small></td>
<td align="center">
<img src="imagenes/sdlnet.gif">
</table>

<hr style="width: 100%; height: 2px;"><br>

<span style="font-family: Verdana;"><small>
<b>Objetivo:</b> Al hacer clic en pantalla se pintan sprites, al presionar Espacio se reproduce un sonido. Se debe escuchar en todo momento una m�sica de fondo. <br><br>
Para este cap�tulo se crea un clase nueva <b>cap3.cs</b> y se copia todo el c�digo de cap2.cs (esta mejor estructurado), cambiando todo lo que diga <b>cap2</b> por <b>cap3</b>, como se hizo en el cap�tulo anterior.
<br>Se copia la siguiente imagen en la carpeta <b>Imagenes</b>:

<br><img src="imagenes/bola.BMP" width="15" height="15" border="1"><br><br>

Luego se crea un carpeta <b>Sonidos</b> y tomaremos una midi de Super Mario 64 a modo de ejemplo, se trata de <a href="sonidos/m64bomb.mid">esta midi</a> y la copiamos dentro de Sonidos, lo mismo con esta otra 
<a href="sonidos/clic.WAV">WAV</a>:

<br><br><img src="imagenes/add_sonido.PNG" border="1"><br><br>

Ahora a programar. <br>

<br> Se crean las variables <b>resx</b> y <b>resy</b> desde un principio para tener la resoluci�n X,Y de la pantalla almacenadas all�:
<br><table><tr><td bgcolor="#CCCCCC"><pre>
int resx=400;
int resy=300;
</pre></td></tr></table><br>

Luego se usar�n en el constructor para seteo de la pantalla:
<br><table><tr><td bgcolor="#CCCCCC"><pre>
Video.SetVideoModeWindow(resx, resy);
</pre></td></tr></table><br>

Se crean las variables de Sonido y M�sica de SdlDotNet:

<br><table><tr><td bgcolor="#CCCCCC"><pre>
Sound sonido;
Music musica;
</pre></td></tr></table><br>

Dentro del constructor seteamos las variables de sonido y m�sica a los archivos respectivos:
<br><table><tr><td bgcolor="#CCCCCC"><pre>
musica = new Music(@"..\..\Sonidos\m64bomb.mid");
sonido = new Sound(@"..\..\Sonidos\clic.WAV");
</pre></td></tr></table><br>

Luego le damos "Play" a la m�sica de fondo:
<br><table><tr><td bgcolor="#CCCCCC"><pre>
musica.Play(true);
</pre></td></tr></table><br>

Agregamos el texto "ESC: Salir" abajo-derecha de la pantalla utilizando variables resx y resy.
<br><table><tr><td bgcolor="#CCCCCC"><pre>
texto = new TextSprite("ESC: salir", new SdlDotNet.Font("../../fuentes/ARIAL.TTF", 11), Color.White, new Point(resx-58,resy-15));
texto.Render(Video.Screen);
</pre></td></tr></table><br>

Hacemos transparente el color negro del imagen bola.bmp, el negro es el borde de la esfera. Usamos el m�todo <b>TransparentColor</b> de la clase Sprite:
<br><table><tr><td bgcolor="#CCCCCC"><pre>
sur_bola.TransparentColor = Color.Black;
</pre></td></tr></table><br>
Esto es muy �til para pintar objetos redondos de forma de hacer transparente los bordes. <br><br>

Para el manejo del Mouse, SDL.NET nos entrega una simple funci�n de la Clase <b>Mouse</b> y un m�todo llamado 
<b>IsButtonPressed (primer/segundo bot�n)</b>:

<br><table><tr><td bgcolor="#CCCCCC"><pre>
if (Mouse.IsButtonPressed(MouseButton.PrimaryButton))
{
  // el primer bot�n del mouse fue presionado
}
</pre></td></tr></table><br>

Como queremos que la bola se pinte en pantalla cuando se haga clic entonces ponemos:
<br><table><tr><td bgcolor="#CCCCCC"><pre>
if (Mouse.IsButtonPressed(MouseButton.PrimaryButton))
{						
    spr_bola.X = Mouse.MousePosition.X;
    spr_bola.Y = Mouse.MousePosition.Y;
    spr_bola.Render(Video.Screen);
}
</pre></td></tr></table><br>

Luego si queremos que al presionar <b>Espacio</b> se escuche un sonido, programamos en el manejador de teclado
<br><table><tr><td bgcolor="#CCCCCC"><pre>
private void Keyboard(object sender, KeyboardEventArgs e)
{
...
  if(e.Key == Key.Space)
  {	
      sonido.Play();				
  }		
}
</pre></td></tr></table><br>

Igualmente dentro del manejador de Teclado, no se te debe olvidar que al presionar <b>Escape</b> se sale de la aplicaci�n, entonces cerramos los sonidos que se reproducen:
<br><table><tr><td bgcolor="#CCCCCC"><pre>
if(e.Key == Key.Escape)
{				
    musica.Close();
    sonido.Stop();
    Events.QuitApplication();
}
</pre></td></tr></table><br>

Finalmente agregaremos un <b>�cono</b> al ejecutable, para esto tomamos un icono de mario bros: <br>
<img src="imagenes/mario3.ico"><br>
y lo pegamos en la carpeta <b>Imagenes</b>:<br><br>
<img src="imagenes/mario_ico.PNG"><br>
Luego dentro de las propiedades del proyecto escoge el icono:<br><br>

<img src="imagenes/escoge_icono.PNG"><br><br>
Esto har� que se tenga un archivo de salida "tutorial.exe" con un �cono encrustado: <br><br>
<img src="imagenes/icono1.PNG"><br><br> 

Ahora solo Recompila el proyecto y no deber�a arrojar errores. Si quieres presionas F5 para Testearlo y ver�s algo as�:<br><br>

<img src="imagenes/cap3_sin_icono.PNG"><br><br>
El programa que hiciste funciona ok, pero f�jate en la parte superior lo siguiente: 
<li>El <b>�cono se perdi�</b>
<li>El Caption de la Ventana dice <b>"SDL_app"</b>. 
<li>No se puede cerrar con la 'X'. 

<br><br>Cambiar Caption de la Ventana<br><br>

Usar <b>Video.WindowCaption = "texto"</b>. Ejemplo:<br>

<br><table><tr><td bgcolor="#CCCCCC"><pre>
Video.SetVideoModeWindow(400, 300);
Video.WindowCaption = "Tutorial SDL";</pre></td></tr></table>

<br>Cambiar �cono<br><br>
Usar <b>Video.WindowIcon(System.Drawing.Icon icono)</b> o <b>Video.WindowIcon(Surface.icon icono)</b>. Ejempo usando System.Drawing.Icon, se debe tener dentro de la carpeta "Imagenes" un �cono llamado "mario3.ico":<br>
<br><table><tr><td bgcolor="#CCCCCC"><pre>
Video.SetVideoModeWindow(400, 300);
Video.WindowIcon(new System.Drawing.Icon("../../Imagenes/mario3.ico"));
</pre></td></tr></table>

<br>Cerrar con la 'X'<br><br>
Ya que codificamos sobre una clase, al abrirse la ventana con el juego, no pod�amos salir de la aplicaci�n usando la t�pica 'X' de cada ventana, por lo que programamos que se salga con la Tecla ESC. 
<br>Para cerrar con 'X' debemos crear un Manejador de Evento de Salida, dentro del metodo <b>Run()</b> llamado
<b>Events.Quit</b> de esta forma:

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
public void Run()
{	
  ...
  Events.KeyboardDown += new KeyboardEventHandler(Events_KeyboardDown);  
  <b>Events.Quit += new QuitEventHandler(Events_Salir);</b>
  Events.Run();
}

private void Events_Salir(object sender, SdlDotNet.QuitEventArgs e)
{
  Events.QuitApplication();
}</pre></td></tr></table><br>

Ahora si tenemos algo de mejor nivel, y si puedes cerrar con la 'X':<br>
<img src="imagenes/cap3_final.PNG"><br><br>


Finalmente recuerda con <b>Explorar de Windows</b> ir a la carpeta <b>Debug/Bin</b> del proyecto y renombrar <b>tutorial.exe</b> a <b>cap3_mouse_sonido.exe</b> para que en lo siguientes cap�tulos no lo sobreescribas:<br>

<img src="imagenes/cap3_file.PNG"><br>

<br><hr style="width: 100%; height: 2px;"><br>
C�digo fuente de este cap�tulo:
<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
//Tutorial de SDL.NET
//Archivo: cap3.cs
//Autor: Dark-N (naldo_go@hotmail.com)
//Fecha: 22-11-2006
//Capitulo 3: Mouse y Sonido

using System;
using System.Drawing;
using SdlDotNet;
using SdlDotNet.Sprites;

using System.Threading;

namespace tutorial
{
    
    public class cap3
    {
        //variables que almacenan resoluci�n de pantalla
        int resx=400;
        int resy=300;
        
        Surface sur_bola;
        Sprite  spr_bola;
        TextSprite texto;
        
        Sound sonido;
        Music musica;     
                
        public cap3()
        {
            Video.SetVideoModeWindow(resx, resy);
            Video.WindowCaption = "Tutorial SDL: Capitulo 3";
            Video.WindowIcon(new System.Drawing.Icon("../../Imagenes/mario3.ico"));

            sur_bola = new Surface(@"..\..\Imagenes\bola.bmp");            
            spr_bola = new Sprite(sur_bola);

            musica = new Music(@"..\..\Sonidos\m64bomb.mid");
            sonido = new Sound(@"..\..\Sonidos\clic.WAV");

            //creamos un texto informativo
            SdlDotNet.Font fuente = new SdlDotNet.Font("../../fuentes/ARIAL.TTF", 14);        
            texto = new TextSprite("Haz clic en le Pantalla y escucha la m�sica.", fuente, Color.Yellow, new Point(5,5));
            texto.Render(Video.Screen);    
            texto = new TextSprite("Presiona Espacio para reproducir un sonido.", fuente, Color.Yellow, new Point(5,20));
            texto.Render(Video.Screen);    

            //texto ESC: salir
            texto = new TextSprite("ESC: salir", new SdlDotNet.Font("../../fuentes/ARIAL.TTF", 11), Color.White, new Point(resx-58,resy-15));
            texto.Render(Video.Screen);    
            
            //el color negro ser� transparente
            sur_bola.TransparentColor = Color.Black;

            //tocamos musica de fondo, repeat:on
            musica.Play(true);            

            Events.Tick += new TickEventHandler(Events_Tick);
        }

        private void Events_Tick(object sender, TickEventArgs e)
        {                
            juego();            
            Video.Screen.Update();
        }

        // l�gica de nuestro juego
        private void juego() 
        {                
            if (Mouse.IsButtonPressed(MouseButton.PrimaryButton))
            {                        
                spr_bola.X = Mouse.MousePosition.X;
                spr_bola.Y = Mouse.MousePosition.Y;
                spr_bola.Render(Video.Screen);
            }
        }
    
        public void Run()
        {    
            Events.KeyboardDown +=new KeyboardEventHandler(this.Keyboard);
            Events.Quit += new QuitEventHandler(Events_Salir);
            Events.Run();
        }

        private void Events_Salir(object sender, SdlDotNet.QuitEventArgs e)
        {
            musica.Close();
            sonido.Stop();
            Events.QuitApplication();
        }

        private void Keyboard(object sender, KeyboardEventArgs e)
        {    
            //Salimos del juego con tecla Escape
            if(e.Key == Key.Escape)
            {                
                musica.Close();
                sonido.Stop();
                Events.QuitApplication();
            }

            //Espacio para escuchar sonido
            if(e.Key == Key.Space)
            {    
                sonido.Play();                
            }        
        }

        [STAThread]
        public static void Main()
        {
            cap3 juego = new cap3();
            juego.Run();
        }
    }    
}
</pre></td></tr></table>

<span style="font-family: Verdana;"><small>

<font size="2"><a href="proyectos/VS2003_SDL4.0_cap1-4.rar">Fuentes: Bajar 
Proyecto VS2003 + SDL.NET 4.0 Cap 1 al 4.</a></font> <br>
</small></span>
<br><hr style="width: 100%; height: 2px;"><br>
<a href="cap2.php">Anterior</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap4.php">Siguiente</a>

<?php
include '../../piecdisq.php';
?>

