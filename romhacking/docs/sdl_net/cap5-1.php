<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>Tutorial de Creaci�n de Video Juegos con SDL .NET</title>
</head>


<body>
<small>
<span style="font-family: Verdana;">
<a href="cap4.php">Anterior</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap6.php">Siguiente</a>
</small>

<hr style="width: 100%; height: 2px;">

<table width="100%">
<tr>
<td>
<big>
<span style="font-weight: bold;">Cap�tulo 5: Parte 1. Sencillo Clon de Pong</span></big>
<small>
<br style="font-weight: bold;"> 
Lenguaje: C#<br>
Para:  VS2005<br>
Por Dark-N: <?php include '../../mail.php'; ?>
<br>

<span style="font-family: Verdana;">
<a href="http://darknromhacking.com/">http://darknromhacking.com</a>
<br><br>
<a href="cap5-2.php">Ir a Parte 2</a>
</td>

<td align="center">
<img src="imagenes/sdlnet.gif">
</table>

<hr style="width: 100%; height: 2px;"><br>

<span style="font-family: Verdana;"><small>
<b>Objetivo:</b> hacer una versi�n sencilla del juego PONG. Solo se puede jugar 
de a 2 player, el player 1 con las teclas Fecha Arriba y Fecha Abajo, y el 
player 2 con las teclas Num Pad 8 y Num Pad 2. Usaremos VS 2005 (C#) con 
SDL .NET 6.0.<br>
<br>
Es hora de aplicar todo lo aprendido antes. Primero debemos tener una pelota y 2 
paletas, estos 3 elementos los sacamos del tutorial de la p�gina de SDL .NET ya 
me gustaron. <br>
<br>
<img border="0" src="imagenes/Ball.PNG" width="6" height="6">&nbsp;
<img border="0" src="imagenes/Paddle1.PNG" width="11" height="43">&nbsp;
<img border="0" src="imagenes/Paddle2.PNG" width="11" height="43"><p>Ahora, como 
variables globales o de Clase, creamos 3 
Sprites, una llamada
<b>bola</b>, otra <b>paleta1</b> 
y otra <b>paleta2. </b>Tambi�n creamos e inicializamos las variables de la velocidad, 
pero como la bola se mueve en un plano X,Y (es un juego 2D) existe la variable 
velocidadX y velocidadY. Por ultimo hacemos lo mismo con las variables que 
guardar�n el puntaje de cada jugador:<b><br>
</b><br>
<table><tr><td bgcolor="#CCCCCC"><pre>
Sprite bola;
Sprite paleta1;
Sprite paleta2;
int velocidadBolaX = 1;
int velocidadBolaY = 1;
int puntajeJ1 = 0;
int puntajeJ2 = 0; 
</pre></td></tr></table>

<br>

Luego en el constructor definimos la resoluci�n y cargamos en las im�genes 
en los Sprites:<br>
<br><table><tr><td bgcolor="#CCCCCC"><pre>
Video.WindowIcon();
Video.WindowCaption = "SDL.NET - Pong";
Video.SetVideoMode(400, 300);

bola = new Sprite(new Surface("../../imagenes/Ball.PNG"));
bola.TransparentColor = Color.Magenta;
bola.Transparent = true;
...
</pre></td></tr></table>

	<br>

En el game-loop pintamos las variables del marcador con la clase <b>TextSprite("texto",[opciones])</b> y definimos los movimientos 
si se presiona una tecla con la clase <b>Keyboard.IsKeyPressed(&lt;tecla&gt;)</b>, 
nota que si presionamos hacia abajo es como ir hacia el Eje Y negativo (-Y) seg�n el eje 
cartesiano. Nosotros definimos 5 p�xeles el largo del desplazamiento de la barra:<br>

<br><table><tr><td bgcolor="#CCCCCC"><pre>
//marcador
SdlDotNet.Graphics.Font fuente = new SdlDotNet.Graphics.Font("../../fuentes/comic.TTF", 20);
TextSprite texto = new TextSprite(puntajeJ1.ToString()+" - "+puntajeJ2.ToString(), fuente, Color.Yellow);
texto.Center = new Point(Video.Screen.Width / 2, 10);

//movimiento paleta izq
if (Keyboard.IsKeyPressed(Key.UpArrow) && paleta1.Top >= 5)
    paleta1.Y -= 5; 

if (Keyboard.IsKeyPressed(Key.DownArrow) && paleta1.Bottom <= Video.Screen.Height - 5)
    paleta1.Y += 5;
</pre></td></tr></table>

	<br>

Ahora se define el movimiento de la bola con su velocidad inicial:<br>

<br><table><tr><td bgcolor="#CCCCCC"><pre>
//movimiento bola
bola.X += velocidadBolaX;
bola.Y += velocidadBolaY;
</pre></td></tr></table>

            
<br>Si la bola choca con la Paleta, hay que ver que si choca por el frente, arriba o abajo. Para simplificar las cosas, veremos si choca cualquier lado 
con la<br>clase <b>Sprite.IntersectsWith(otroSprite)</b>, haremos <b>velicidadX = velocidadX * -1</b>, es decir, cambiar de sentido o rebotar y se 
le agrega que a cada rebote la bola vaya mas r�pido, asi el juego no es tan monotono.<br>

<br><table><tr><td bgcolor="#CCCCCC"><pre>
//rebote bola con paleta, se acelera la velocidad de la bola
if (paleta1.IntersectsWith(bola))
{
    velocidadBolaX *= -1;
    velocidadBolaX += 1;
}
if (paleta2.IntersectsWith(bola))
{
    velocidadBolaX *= -1;
    velocidadBolaX -= 1;
}
</pre></td></tr></table>

<br>

Programamos cuando se haga un punto.<br>
<br><table><tr><td bgcolor="#CCCCCC"><pre>
//hacer un punto Jugador 1 a Jugador 2
if (bola.Right > Video.Screen.Width)
    {            
        puntajeJ1++;
        bola.X = 0;
        bola.Y = 0;
        velocidadBolaX = 1;
        velocidadBolaY = 1;
    }
</pre></td></tr></table>

<br>

Se agrega la condici�n de que la bola rebote arriba y abajo de la pantalla:<br>
<br><table><tr><td bgcolor="#CCCCCC"><pre>
//si bola choca arriba o abajo rebota
if (bola.Top < 0)            
    velocidadBolaY *= -1;            

if (bola.Bottom > Video.Screen.Height)            
    velocidadBolaY *= -1;
</pre></td></tr></table>


<br>
Finalmente se hace <b>render</b> se los objetos involucrados:<br>

<br><table><tr><td bgcolor="#CCCCCC"><pre>
// fondo negro
Video.Screen.Fill(Color.Black);

//texto en pantalla
Video.Screen.Blit(texto);

// pintamos la bola
Video.Screen.Blit(bola);

// pintamos las paletas
Video.Screen.Blit(paleta1);
Video.Screen.Blit(paleta2);

// se actualiza la pantalla
Video.Screen.Update();
</pre></td></tr></table>


</b></p>
<hr style="width: 100%; height: 2px;"><br>
C�digo fuente de este cap�tulo:<br>
<br><table><tr><td bgcolor="#CCCCCC"><pre>
//Tutorial de SDL .NET
//Capitulo 5: Sencillo Clon de Pong
//Por Dark-N


using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Drawing; //colores

using SdlDotNet;
using SdlDotNet.Graphics; //para sprites
using SdlDotNet.Core; //Eventos
using SdlDotNet.Graphics.Sprites;  //textos
using SdlDotNet.Input; //keyborad


namespace Pong
{
    class Pong: IDisposable
    {
        Sprite bola;
        Sprite paleta1;
        Sprite paleta2;
        int velocidadBolaX = 1; //valocidad inicial
        int velocidadBolaY = 1;
        int puntajeJ1 = 0;
        int puntajeJ2 = 0;        
        
        public Pong() 
        {
            Video.WindowIcon();
            Video.WindowCaption = "SDL.NET - Pong";
            Video.SetVideoMode(400, 300);

            bola = new Sprite(new Surface("../../imagenes/Ball.PNG"));
            bola.TransparentColor = Color.Magenta;
            bola.Transparent = true;

            paleta1 = new Sprite(new Surface("../../imagenes/Paddle1.PNG"));
            paleta1.Transparent = true;
            paleta1.Center = new Point(10, Video.Screen.Height / 2);
            paleta2 = new Sprite(new Surface("../../imagenes/Paddle2.PNG"));
            paleta2.Transparent = true;
            paleta2.Center = new Point(Video.Screen.Width-10, Video.Screen.Height / 2);  
       
        }


        private void Quit(object sender, QuitEventArgs e)
        {
            Events.QuitApplication();
        }

        private void Teclado(object sender, KeyboardEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape:                
                    Events.QuitApplication();
                    break;                
            }
        }


        //ciclo de juego (game-loop)
        private void Tick(object sender, TickEventArgs args)
        {

            //marcador
            SdlDotNet.Graphics.Font fuente = new SdlDotNet.Graphics.Font("../../fuentes/comic.TTF", 20);
            TextSprite texto = new TextSprite(puntajeJ1.ToString()+" - "+puntajeJ2.ToString(), fuente, Color.Yellow);
            texto.Center = new Point(Video.Screen.Width / 2, 10);


            //movimiento bola
            bola.X += velocidadBolaX;
            bola.Y += velocidadBolaY;

            //movimiento paleta izq
            if (Keyboard.IsKeyPressed(Key.UpArrow) && paleta1.Top >= 5)
                paleta1.Y -= 5;

            if (Keyboard.IsKeyPressed(Key.DownArrow) && paleta1.Bottom <= Video.Screen.Height - 5)
                paleta1.Y += 5;
            
            //movimiento paleta der            
            if (Keyboard.IsKeyPressed(Key.Keypad8) && paleta2.Top >= 5)
                paleta2.Y -= 5;

            if (Keyboard.IsKeyPressed(Key.Keypad2) && paleta2.Bottom <= Video.Screen.Height-5)
                paleta2.Y += 5;

            //rebote bola con paleta
            if (paleta1.IntersectsWith(bola))
            {
                velocidadBolaX *= -1;
                velocidadBolaX += 1;
            }
            if (paleta2.IntersectsWith(bola))
            {
                velocidadBolaX *= -1;
                velocidadBolaX -= 1;
            }


            //hacer un punto Jugador 1 a Jugador 2
            if (bola.X > Video.Screen.Width)
            {            
                puntajeJ1++;
                bola.X = 0;
                bola.Y = 0;
                velocidadBolaX = 1;
                velocidadBolaY = 1;
            }
            
            if (bola.X < 0)
            {             
                puntajeJ2++;
                bola.X = 0;
                bola.Y = 0;
                velocidadBolaX = 1; 
                velocidadBolaY = 1;
            }

            //si bola choca arriba o abajo rebota
            if (bola.Top < 0)            
                velocidadBolaY *= -1;            
            
            if (bola.Bottom > Video.Screen.Height)            
                velocidadBolaY *= -1;            

            // fondo negro
            Video.Screen.Fill(Color.Black);

            //texto en pantalla
            Video.Screen.Blit(texto);

            // pintamos la bola
            Video.Screen.Blit(bola);

            // pintamos las paletas
            Video.Screen.Blit(paleta1);
            Video.Screen.Blit(paleta2);
            
            // se actualiza la pantalla
            Video.Screen.Update();
        }


        public void Run()
        {
            Events.KeyboardDown += new EventHandler < KeyboardEventArgs >(this.Teclado);
            Events.Tick += new EventHandler < TickEventArgs >(this.Tick);
            Events.Quit += new EventHandler < QuitEventArgs >(this.Quit);
            Events.Run();
        }

        
        static void Main(string[] args)
        {
            Pong game = new Pong();
            game.Run();
        }

        #region IDisposable Members

        private bool disposed;


        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (this.bola != null)
                    {
                        this.bola.Dispose();
                        this.bola = null;
                    }
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }


        public void Close()
        {
            Dispose();
        }

        ~Pong()
        {
            Dispose(false);
        }

        #endregion
        
    }
}</pre></td></tr></table>
El proyecto se puede bajar para VS2005 con SDL.Net 6.0 desde <a href="proyectos/VS2005_SDL6.0_cap5_pong.rar">aqu�</a>.
<br>
<br>
Recuerda ir a la <a href="cap5-2.php">Parte 2</a> para ver una versi�n del juego m�s 
completa que la reci�n desarrollada.<br><hr style="width: 100%; height: 2px;"><br>
<a href="cap4.php">Anterior</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap6.php">Siguiente</a>

<?php
include '../../piecdisq.php';
?>

