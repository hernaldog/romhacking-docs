<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>Tutorial de Creaci�n de Video Juegos con SDL .NET</title>
</head>


<body>
<small>
<span style="font-family: Verdana;">
<a href="cap4.php">Anterior</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap6.php">Siguiente</a>
</small>

<hr style="width: 100%; height: 2px;">

<table width="100%">
<tr>
<td>
<big>
<span style="font-weight: bold;">Cap�tulo 5: Parte 2. Un Pong m�s Completo</span>
</big>
<small>
<br style="font-weight: bold;"> 
Lenguaje: C#<br>
Para:  VS2005<br>
Por Dark-N: <?php include '../../mail.php'; ?>
<br>

<span style="font-family: Verdana;">
<a href="http://darknromhacking.com/">http://darknromhacking.com</a><br>
<br>
<a href="cap5-1.php">Ir a Parte 1</a></td>
<td align="center">
<img src="imagenes/sdlnet.gif">
</table>

<hr style="width: 100%; height: 2px;"><br><span style="font-family: Verdana;">
<small><b>Objetivo:</b> hacer una versi�n mas completa de Pong, es decir que use 
mouse y sonido, men�, fondo en movimiento, etc.<br>
<br>
<img border="0" src="imagenes/cap52.PNG" width="301" height="248">
<img border="0" src="imagenes/cap521.PNG" width="301" height="248"><br>
<br>
No explicar� el c�digo ya que a esta altura deber�as dominarlo. Lo que si, a 
esta versi�n le colocamos:<br>
<br>
<li>Un Men� con una imagen de fondo. El men� solo tiene una opci�n que responde 
al mouse. Es �til para as� aprender a usar Men�s con el Mouse.</li><br>
<li>Un Sonido que se aplica cuando la bola golpea la paleta.</li><br>
<li>Un Sonido especial cuando se hace un punto.</li><br>
<li>El fondo del juego muestra imagen con un movimiento infinito.</li><br>
<li>Se incrementa la velocidad de la pelota cuando se le golpea, as� se le da m�s 
emoci�n al juego a medida que avanza.</li><br>
&nbsp;<hr style="width: 100%; height: 2px;">C�digo fuente de esta segunda parte:<br>
<br><table><tr><td bgcolor="#CCCCCC"><pre>
/* Tutorial de SDL.NET por Dark-N
 * Cap�tulo 5: Pong
 * Fecha: 10-08-2007
 * Descripci�n: Un sencillo clon del juego PONG para 2 PLAYERS.
 * http://darknromhacking.com
 * */

using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Drawing; //colores

using SdlDotNet.Graphics; //para el manejo de Sprites
using SdlDotNet.Core; //Eventos(clic, teclado)
using SdlDotNet.Graphics.Sprites;  //Textos
using SdlDotNet.Input; //Teclado
using SdlDotNet.Audio; //Sonidos


namespace Pong
{
    class Pong: IDisposable
    {
        Sprite bola;
        Sprite paleta1;
        Sprite paleta2;
        Sprite spr_fondo;  //2 Sprites de fondos para manejar
        Sprite spr_fondo2; //improvisadamente que el fondo se mueva constantemente
        Surface sur_fondo;        
        int velocidadBolaX = 1; //velocidad inicial
        int velocidadBolaY = 1;
        int puntajeJ1 = 0;
        int puntajeJ2 = 0;        
        SdlDotNet.Graphics.Font fuente;
        Sound son_rebote;
        Sound son_punto;
        Random r = new Random();
        int opcion = 0;
        bool stop = false;
        
        
        public Pong() 
        {            
            Video.WindowIcon();
            Video.WindowCaption = "SDL.NET - Pong";
            Video.SetVideoMode(400, 300);

            son_rebote = new Sound("../../sonidos/rebote.wav");
            son_punto = new Sound("../../sonidos/SWOOZ.WAV");
            
            bola = new Sprite(new Surface("../../imagenes/Ball.PNG"));
            bola.TransparentColor = Color.Magenta;
            bola.Transparent = true;

            bola.Y = r.Next(Video.Screen.Height);
            bola.X = 20;

            paleta1 = new Sprite(new Surface("../../imagenes/Paddle1.PNG"));
            paleta1.Transparent = true;
            
            paleta2 = new Sprite(new Surface("../../imagenes/Paddle2.PNG"));
            paleta2.Transparent = true;

            sur_fondo = new Surface("../../imagenes/Fondo.gif");               
            spr_fondo = new Sprite(sur_fondo);
            spr_fondo2 = new Sprite(sur_fondo);
            spr_fondo2.X = Video.Screen.Width;
        }
        
        private void Quit(object sender, QuitEventArgs e)
        {
            Events.QuitApplication();
        }

        private void Teclado(object sender, KeyboardEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape:
                    {
                        if (opcion == 0) 
                            Events.QuitApplication();
                        else 
                            menu();
                        break;
                    }
            }
        }

        //ciclo de juego (game-loop)
        private void Tick(object sender, TickEventArgs args)
        {
            //si opcion = 1 1p vs 2p
            //si opcion = 2 1p vs CPU (no implementado)
            if (opcion == 0)
                opcion = menu();            

            //entramos al juego en si
            if (opcion != 0)
            {
                //fondo de pantalla en movimiento
                mueveFondo();

                //marcador
                fuente = new SdlDotNet.Graphics.Font("../../fuentes/comic.TTF", 20);
                TextSprite texto = new TextSprite(puntajeJ1.ToString() + " - " + puntajeJ2.ToString(), fuente, Color.Yellow);
                texto.Center = new Point(Video.Screen.Width / 2, 10);

                //texto abajo de marcador
                fuente = new SdlDotNet.Graphics.Font("../../fuentes/comic.TTF", 11);
                TextSprite textop1p2 = new TextSprite("P1      P2", fuente, Color.Yellow);
                textop1p2.Center = new Point(Video.Screen.Width / 2+1, 25);

                //mensaje salir
                fuente = new SdlDotNet.Graphics.Font("../../fuentes/comic.TTF", 10);
                TextSprite tex_salir = new TextSprite("[ESC]=Men�", fuente, Color.White);
                tex_salir.Center = new Point(Video.Screen.Width - 50, Video.Screen.Height - 20); 

                //movimiento paleta izq
                if (Keyboard.IsKeyPressed(Key.UpArrow) && paleta1.Top >= 5)
                    paleta1.Y -= 5;

                if (Keyboard.IsKeyPressed(Key.DownArrow) && paleta1.Bottom <= Video.Screen.Height - 5)
                    paleta1.Y += 5;

                if (opcion == 1)
                {
                    //movimiento paleta der
                    if (Keyboard.IsKeyPressed(Key.Keypad8) && paleta2.Top >= 5)
                        paleta2.Y -= 5;

                    if (Keyboard.IsKeyPressed(Key.Keypad2) && paleta2.Bottom <= Video.Screen.Height - 5)
                        paleta2.Y += 5;
                }

                if (paleta1.IntersectsWith(bola))
                {
                    son_rebote.Play();

                    if (velocidadBolaX > 0) velocidadBolaX++;
                    else velocidadBolaX--;
                    if (velocidadBolaY > 0) velocidadBolaY++;
                    else velocidadBolaY--;

                    if (bola.Left <= paleta1.Right && bola.Bottom < paleta1.Bottom && bola.Top > paleta1.Top)
                        velocidadBolaX *= -1;
                    else
                        if (bola.Bottom > paleta1.Bottom || bola.Top < paleta1.Top)
                            velocidadBolaY *= -1;
                }

                if (paleta2.IntersectsWith(bola))
                {
                    son_rebote.Play();

                    if (velocidadBolaX > 0) velocidadBolaX++;
                    else velocidadBolaX--;
                    if (velocidadBolaY > 0) velocidadBolaY++;
                    else velocidadBolaY--;

                    if (bola.Right >= paleta2.Left && bola.Bottom < paleta2.Bottom && bola.Top > paleta2.Top)
                        velocidadBolaX *= -1;
                    else
                        if (bola.Bottom > paleta2.Bottom || bola.Top < paleta2.Top)
                            velocidadBolaY *= -1;
                }

                //movimiento bola
                if (stop == false)
                {
                    bola.X += velocidadBolaX;
                    bola.Y += velocidadBolaY;
                }

                //hacer un punto
                if (bola.Right > Video.Screen.Width)
                {
                    puntajeJ1++;
                    resetBola();
                    son_punto.Play();
                }

                if (bola.Left < 0)
                {
                    puntajeJ2++;
                    resetBola();
                    son_punto.Play();
                }

                //si bola choca arriba o abajo rebota
                if (bola.Top < 0)
                {
                    velocidadBolaY *= -1;
                }

                if (bola.Bottom > Video.Screen.Height)
                {
                    velocidadBolaY *= -1;
                }

                //render de textos en pantalla
                Video.Screen.Blit(texto);
                Video.Screen.Blit(textop1p2);
                Video.Screen.Blit(tex_salir); 
                
                // pintamos la bola
                Video.Screen.Blit(bola);

                // pintamos las paletas
                Video.Screen.Blit(paleta1);
                Video.Screen.Blit(paleta2);               

            }
            // se actualiza la pantalla
            Video.Screen.Update();
        }


        private int menu()
        {
            Video.Screen.Blit(spr_fondo);
            resetJuego();

            fuente = new SdlDotNet.Graphics.Font("../../fuentes/comic.TTF", 35);
            TextSprite titulo = new TextSprite("PONG", fuente, Color.GreenYellow);
            titulo.Center = new Point(Video.Screen.Width / 2, 50);
            Video.Screen.Blit(titulo);  

            fuente = new SdlDotNet.Graphics.Font("../../fuentes/comic.TTF", 15);
            TextSprite opcion1 = new TextSprite("Jugador 1 vs Jugador 2", fuente, Color.Wheat);
            opcion1.Center = new Point(Video.Screen.Width / 2, Video.Screen.Height / 2 - 10);
            Video.Screen.Blit(opcion1);          

            if (opcion1.IntersectsWith(Mouse.MousePosition))
            {
                opcion1.Color = Color.Aqua;
                Video.Screen.Blit(opcion1);
                if (Mouse.IsButtonPressed(MouseButton.PrimaryButton))
                    return 1;
            }

            //mensaje teclas
            fuente = new SdlDotNet.Graphics.Font("../../fuentes/comic.TTF", 12);
            Point punto = new Point(Video.Screen.Width / 2, Video.Screen.Height / 2 + 60);

            TextSprite tex_keys = new TextSprite("CONTROLES", fuente, Color.White);
            tex_keys.Center = punto;
            Video.Screen.Blit(tex_keys);
            punto.Y += 20;

            tex_keys = new TextSprite("JUGADOR 1: FlechaArriba/FlechaAbajo", fuente, Color.White);
            tex_keys.Center = punto;
            Video.Screen.Blit(tex_keys);
            punto.Y += 20;

            tex_keys = new TextSprite("JUGADOR 2: NumPad 8/NumPad 2", fuente, Color.White);
            tex_keys.Center = punto;
            Video.Screen.Blit(tex_keys);
            return 0;
        }
        
        private void resetBola()
        {
            bola.X = 20;
            bola.Y = r.Next(Video.Screen.Height-2);
            velocidadBolaX = 1;
            velocidadBolaY = 1;
        }

        private void resetJuego()
        {
            opcion = 0;
            puntajeJ1 = 0;
            puntajeJ2 = 0;
            paleta1.Center = new Point(10, Video.Screen.Height / 2);
            paleta2.Center = new Point(Video.Screen.Width - 10, Video.Screen.Height / 2);
            spr_fondo.X = 0;
            spr_fondo.Y = 0;
            resetBola();
        }

        private void mueveFondo()
        {
            if (spr_fondo.X <= -Video.Screen.Width+2)
                spr_fondo.X = 0;
            
            if (spr_fondo2.X <= 0)
                spr_fondo2.X = Video.Screen.Width-2;
            
            Video.Screen.Blit(spr_fondo);  
            Video.Screen.Blit(spr_fondo2);
            spr_fondo.X--;
            spr_fondo2.X--;            
        }

        public void Run()
        {
            Events.KeyboardDown += new EventHandler < KeyboardEventArgs >(this.Teclado);
            Events.Tick += new EventHandler < TickEventArgs >(this.Tick);
            Events.Quit += new EventHandler < QuitEventArgs >(this.Quit);
            Events.Run();
        }


        static void Main(string[] args)
        {
            Pong game = new Pong();
            game.Run();
        }


        #region IDisposable Members

        private bool disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (this.bola != null)
                    {
                        this.bola.Dispose();
                        this.bola = null;
                    }
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Close()
        {
            Dispose();
        }


        ~Pong()
        {
            Dispose(false);
        }

        #endregion
    }
}</pre></td></tr></table>
El proyecto de esta parte 2 b�jalo desde
<a href="proyectos/VS2005_SDL6.0_cap5_pong_parte2.rar">aqu�</a>.
<br><br>Volver a la <a href="cap5-1.php">Parte 1</a>
<br><hr style="width: 100%; height: 2px;"><br>
<a href="cap4.php">Anterior</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap6.php">Siguiente</a>

<?php
include '../../piecdisq.php';
?>
