<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>Tutorial de Creaci�n de Video Juegos con SDL .NET</title>
</head>


<body>
<small>
<span style="font-family: Verdana;">
<a href="cap0-1.php">Anterior</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap2.php">Siguiente</a>
</small>

<hr style="width: 100%; height: 2px;">

<table width="100%">
<tr>
<td>
<big>
<span style="font-weight: bold;">Cap�tulo 1: Hola Mundo</span>
</big>
<small>
<br style="font-weight: bold;"> 
Lenguaje: C#<br>
Para:  VS2003 / VS2005 / VS2008<br>
Por Dark-N: <?php include '../../mail.php'; ?>
<br>
<span style="font-family: Verdana;">
<a href="http://darknromhacking.com/">http://darknromhacking.com</a></small></td>
<td align="center">
<img src="imagenes/sdlnet.gif">
</table>

<hr style="width: 100%; height: 2px;"><br>

<span style="font-family: Verdana;"><small>

<b>Objetivo:</b> Mostrar en pantalla el cl�sico "hola mundo". Primero usaremos Visual 
Studio 2003 con C# y con SDL .NET 4.0. Para ver un ejemplo del &quot;Hola Mundo&quot; en Visual Basic .NET ver este <a href="http://cs-sdl.sourceforge.net/index.php/HelloWorld_VB.NET">tutorial</a> de la P&aacute;gina Oficial de Sdl.Net. 
<br>
<br> 

Lo primero es eliminar la clase que viene por defecto llamada "form1.cs" ya que nos sirve de mucho:

<br><br><img src="imagenes/delete.PNG"><br><br>

Agregar una nueva clase, que ser� nuestra ventana. Le llamaremos "cap1.cs":

<br><br><img src="imagenes/new_item.PNG"><br><br>

<img src="imagenes/new_item2.PNG"><br><br>

Abrimos la clase que creamos y contendr� el siguiente c�digo C#: 

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
using System;

namespace cap1
{
    /// <summary>
    /// Summary description for cap1.
    /// </summary>
    public class cap1
    {
        public cap1()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }
}
</pre></td></tr></table><br>

Para empezar a programar, lo primero es hacer el "puente" para usar la referencia que hicimos a <b>SdlDotNet</b>, para esto se usa la instrucci�n <b>using</b>, tambi�n se debe hacer referencia a <b>SdlDotNet.Sprites</b> que nos permite manejar Sprites que son objetos que podemos mover en pantalla, palabras que se rendear�n, etc, y a <b>System.Drawing</b> que nos permite definir puntos X,Y en pantalla para posicionar objetos:

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
using System;
using System.Drawing;
using SdlDotNet;
using SdlDotNet.Sprites;
</pre></td></tr></table><br>

Se debe cambiar el <b>namespace</b> a <b>tutorial</b>:
<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
...
namespace tutorial
{
...
</pre></td></tr></table><br>

Ahora en el <b>constructor</b> se inicializa la ventana que mostrar� el juego, que en nuestro caso solo mostrar� un mensaje. El c�digo que se debe colocar es el que configura la <b>resoluci�n</b> y el que inicia el <b>game-loop</b>. El game-loop es un ciclo infinito que realiza todo juego, donde solo se preocupa de pintar en pantalla mientras la aplicaci�n est� andando. Desde ahora no se hablar� de pintar en pantalla, sino de hacer <b>render</b> en pantalla.

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
public class cap1
{
    public cap1()
    {
        //modo ventana, resoluci�n 400x300
        Video.SetVideoModeWindow(400, 300);
        Events.Tick += new TickEventHandler(Events_Tick);
    }

    //game-loop
    private void Events_Tick(object sender, TickEventArgs e)
    {
    }
    ...
</pre></td></tr></table><br>

Lo que sigue es poner el c�digo que se rendear� en el game-loop. Lo que se hace es esto:

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
...
private void Events_Tick(object sender, TickEventArgs e)
{

    // Limpar la pantalla (dejarla negra)
    Video.Screen.Fill(Color.Black);

    // aqu� LOGICA del nuestro juego
    juego();

    // Se actualiza la pantalla siempre
    Video.Screen.Update();
}
...
</pre></td></tr></table><br>

Finalmente la l�gica del juego, que solo es mostrar un texto (un objeto <b>Sprite</b>) en la pantalla (la pantalla es en verdad es un <b>Surface</b>). En SDL. NET los <b>Sprites</b> son simples objetos 2D que se pueden mover en pantalla, como un texto o una imagen.<br>
Los <b>Surfaces</b> representan informaci�n gr�fica. Puedes crear nuevas Surfaces o cargarlas de diferentes partes como de una imagen. En general un Surface es el "fondo" en el que actua un Sprite. La pantalla, lo que se ve negro en el fondo es el surface principal llamado <b>Video.Screen.</b> 
<br>Los Sprites igual pueden cargar otros Surfaces, como imagenes, asi los puedes mover ya que los Sprites tienen la propiedad X,Y.

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
...
// l�gica de nuestro juego
private void juego() 
{
    //creamos una variable fuente, tama�o 18
    SdlDotNet.Font fuente = new SdlDotNet.Font("../../fuentes/ARIAL.TTF",18);
    
    //cremos un texto de color amarillo y le damos una posici�n 20,20
    TextSprite texto = new TextSprite("Hola Mundo", fuente, Color.Yellow, new Point(20,20));
    
    //se pinta en el Surface principal llamado Video.Screen
    texto.Render(Video.Screen);
}
...
</pre></td></tr></table><br>

Ya podemos mostrar en pantalla algo, lo �nico que falta es que podamos salir de la aplicaci�n, para esto devemos manejar  <b>Evento del Teclado</b>. Usaremos <b>Events.KeyboardDown</b> para registrar cuando una tecla se ejecute y hacer una funci�n asociada a ese Evento que permita salir del juego:

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
...
public void Run()
{    
    //Agregamos Evento de presionar una tecla
    Events.KeyboardDown +=new KeyboardEventHandler(this.Keyboard);
    Events.Run();
}

private void Keyboard(object sender, KeyboardEventArgs e)
{    
    //Salimos del juego con tecla Escape o la tecla 'Q'
    if(e.Key == Key.Escape || e.Key == Key.Q)
    {
        Events.QuitApplication();
    }        
}
...
</pre></td></tr></table><br>

Finalmente debemos probar lo que hemos hecho, para esto primero se debe <b>compilar</b> la aplicaci�n. En palabras sencillas, la compilaci�n es la etapa en que construimos una salida a partir de los archivos fuentes, la salida puede ser un EXE, un DLL, etc. En nuestro caso, como es un juego ejecutable la salida es un EXE, por eso escogimos en un principio un proyecto tipo Windows Application.
<br>El proceso es sencillo, se selecciona el proyecto con el 2do. bot�n del mouse y se escoge <b>Rebuild</b>:

<br><br><img src="imagenes/rebuild.PNG"><br><br>

Se compilar� y en la ventana <b>output</b> que est� abajo muestra la salida, si hubieron errores o si sali� todo ok.

<br><br><img src="imagenes/output.PNG"><br><br>

Como se ve en la imagen superior, deber�a decir que 1 elemento sali� bien y que 0 elemento fall�: <b>1 succeeded, 0 failed</b>.
<br>Con esto se gener� la salida en la carpeta de Salida de cada proyecto llamada <b>bin</b>. La ruta completa es <b>C:\SDL\cap1\bin\Debug</b>, mira all� y ver�s el ejecutable:

<br><br><img src="imagenes/carpeta_debug.PNG"><br><br>


Ahora es tiempo de <b>Ejecutar</b> lo que hemos hecho, para esto hay 2 maneras, simplemente ejecutar el <b>tutorial.exe</b> mostrado arriba, o en el mismo Visual Studio .NET usar el <b>Modo Depuraci�n/Debug</b> al presionar F5 o el signo Play azul:
<br><br><img src="imagenes/modo_debug.PNG"><br><br>

En ambos casos nos ejecutar� nuestra aplicaci�n:

<br><br><img src="imagenes/cap1.PNG"><br><br>

Con escape sales de la aplicaci�n.
<br>
La ventaja de usar el m�todo de Debug (con F5) es que mientras se ejecuta el juego, puedes poner <b>puntos de breakpoint</b> en el c�digo haciendo clic a la izquierda de la linea que deseas que se detenga la ejecuci�n o con F9 sobre la l�nea, asi puedes detener la ejecuci�n e ir paso a paso con F10. Esto sirve mucho para seguir la ejecuci�n del juego y ver posibles errores:

<br><br><img src="imagenes/breakpoint.PNG" width="759" height="429"><br><br>

<br><hr style="width: 100%; height: 2px;"><br>

C�digo fuente de este cap�tulo:
<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
//Archivo: cap1.cs
//Autor: Dark-N
using System;
using System.Drawing;
using SdlDotNet;
using SdlDotNet.Sprites;


namespace tutorial
{
    public class cap1
    {
        public cap1()
        {
            //modo ventana, resoluci�n 400x300
            Video.SetVideoModeWindow(400, 300);
            Events.Tick += new TickEventHandler(Events_Tick);
        }

        private void Events_Tick(object sender, TickEventArgs e)
        {

            // Limpar la pantalla (dejarla negra)
            Video.Screen.Fill(Color.Black);

            //aqui LOGICA del nuestro juego>
            juego();

            // Se actualiza la pantalla
            Video.Screen.Update();
        }

        // l�gica de nuestro juego
        private void juego() 
        {
            //creamos una variable fuente, tama�o 18
            SdlDotNet.Font fuente = new SdlDotNet.Font("../../fuentes/ARIAL.TTF",18);
            
            //cremos un texto de color amarillo y le damos una posici�n 20,20
            TextSprite texto = new TextSprite("Hola Mundo", fuente, Color.Yellow, new Point(20,20));
            
            //se pinta en el Surface principal llamado Video.Screen
            texto.Render(Video.Screen);
            
        }        

        public void Run()
        {    
            Events.KeyboardDown +=new KeyboardEventHandler(this.Keyboard);
            Events.Run();
        }

        private void Keyboard(object sender, KeyboardEventArgs e)
        {    
            //Salimos del juego con tecla Escape
            if(e.Key == Key.Escape)
            {
                Events.QuitApplication();
            }        
        }

        [STAThread]
        public static void Main()
        {
            cap1 juego = new cap1();
            juego.Run();
        }
    }
    
}
</pre></td></tr></table><br>

Recuerda cambiarle el nombre al archivo <b>tutorial.exe</b> por otro mas representativo como <b>cap1_hola_mundo.exe</b>.


<br><br>Estamos listos para pasar a aprender a usar Sprites con im�genes.<br>
<br>
<span style="font-family: Verdana;">
<font size="2"><a href="proyectos/VS2003_SDL4.0_cap1-4.rar">Fuentes: Bajar 
Proyecto VS2003 + SDL.NET 4.0 Cap 1 al 4.</a></font></span>

<br><br><hr style="width: 100%; height: 2px;">


<span style="font-family: Verdana;">
Agregado: Usamos <b>Visual Studio 2005</b> con SDL .NET 6.0<br><br>As� se ve el proyecto:<br>

<br>
<img border="0" src="imagenes/2005_holamundo.PNG" width="614" height="586"><br>
Gu�rdala en el disco para verla en su tama�o original.<br>
<br>
El c�digo fuente como se ve es distinto al anterior, esto es porque ahora usamos 
SDL .NET 6.0 y usamos tambi�n generics para el uso de Eventos en C#:
<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Drawing; //colores
using System.Text;

using SdlDotNet;
using SdlDotNet.Graphics; //para sprites
using SdlDotNet.Core; //Eventos
using SdlDotNet.Graphics.Sprites;  //textos
using SdlDotNet.Input; //keyborad


namespace SDLNet_2005
{
    class HolaMundo
    {
        public HolaMundo()
        {}

        private Surface screen;

        private void Quit(object sender, QuitEventArgs e)
        {
            Events.QuitApplication();
        }

        private void Teclado(object sender, KeyboardEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape:
                case Key.Q:
                    Events.QuitApplication();
                    break;
            }
        }


        //ciclo de juego (game-loop)
        private void Tick(object sender, TickEventArgs args)
        {
            //fondo negro
            screen.Fill(Color.Black);
            
            //texto con fuente
            SdlDotNet.Graphics.Font fuente = new SdlDotNet.Graphics.Font("../../fuentes/comic.TTF", 18);
            TextSprite texto = new TextSprite("hola mundo", fuente, Color.Yellow);
            texto.Center = new Point(screen.Width / 2, screen.Height / 2);
            screen.Blit(texto);
            
            screen.Update();
        }

        public void Run()
        {

            Video.WindowIcon(new Icon("../../imagenes/mario3.ico"));
            Video.WindowCaption = "SDL.NET - Hola Mundo";
            screen = Video.SetVideoMode(300, 400);

            Events.KeyboardDown += new EventHandler < KeyboardEventArgs >(this.Teclado);
            Events.Tick += new EventHandler < TickEventArgs >(this.Tick);
            Events.Quit += new EventHandler < QuitEventArgs >(this.Quit);
            Events.Run();
        }

        /// </summary>
        static void Main(string[] args)
        {
            HolaMundo game = new HolaMundo();            
            game.Run();
            
        }
    }
}
</pre></td></tr></table>
Si le das F5 
	
la salida es:<br>
<br>
<img border="0" src="imagenes/2005_salidaCap1.PNG"><br>

<br>El proyecto completo usando VS 2005 b�jalo desde <a href="proyectos/VS2005_SDL6.0_cap1.rar">aqu�</a>.

<hr style="width: 100%; height: 2px;">

<br>

<span style="font-family: Verdana;">
Agregado: Ahora con C# + <b>Visual Studio 2008</b> usando SDL.NET SDK 6.0 con el Framework 2.0<br>
<br>Crea el proyecto de esta forma:<br>
<img border="0" src="imagenes/vs2008-1.PNG">

<br><br>Cambia las propiedades del proyecto para que la salida sea de tipo Windows Application:<br>
<img border="0" src="imagenes/vs2008-2.PNG">

<br><br>Asi se debe ver tu proyecto. Nota que las referencias que est�n en rojo las debes agregar:<br>
<img border="0" src="imagenes/vs2008-3.PNG">

<br><br>Este es el C�digo fuente que es muy parecido al de VS 2005 por no decir igual:
<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
//Archivo: HolaMundo.cs
//Autor: Dark-N
//fecha: 20-05-2009

using System;
using System.Collections.Generic; //para eventos y otras cosas
using System.Drawing; //colores

using SdlDotNet.Graphics; //para sprites
using SdlDotNet.Core; //Eventos
using SdlDotNet.Graphics.Sprites;  //textos
using SdlDotNet.Input; //keyborad

namespace SDLNet_VS2008
{
    class HolaMundo
    {       
        private Surface screen;

        private void Evento_Salir(object sender, QuitEventArgs e)
        {
            Events.QuitApplication();
        }

        private void Evento_Teclado(object sender, KeyboardEventArgs e)
        {            
            if (e.Key == Key.Escape)
                Events.QuitApplication();          
        }

        //ciclo de juego (game-loop)
        private void Evento_Tick(object sender, TickEventArgs args)
        {
            //fondo negro
            screen.Fill(Color.Black);

            //texto con fuente
            SdlDotNet.Graphics.Font fuente = new SdlDotNet.Graphics.Font("arial.TTF", 16);
            TextSprite texto = new TextSprite("�Hola Mundo! con Framework 2.0 + C# + VS2008", fuente, Color.Yellow);
            texto.Center = new Point(screen.Width / 2, screen.Height / 2);
            screen.Blit(texto);

            screen.Update();
        }

        public void Run()
        {         
            Video.WindowCaption = "SDL.NET - Hola Mundo";
            screen = Video.SetVideoMode(400, 300);
            SdlDotNet.Core.Events.Fps = 60;

            SdlDotNet.Core.Events.KeyboardDown += new EventHandler < KeyboardEventArgs >(this.Evento_Teclado);
            SdlDotNet.Core.Events.Tick += new EventHandler < TickEventArgs >(this.Evento_Tick);
            SdlDotNet.Core.Events.Quit += new EventHandler < QuitEventArgs >(this.Evento_Salir);
            Events.Run();
        }

        static void Main()
        {
            HolaMundo game = new HolaMundo();
            game.Run();
        }
    }
}
</pre></td></tr></table>


<br>Antes de compilar recuerda copiar la fuente <a href="fuentes/ARIAL.TTF"><b>ARIAL.TTF</b></a> en el directorio <b>bin/debug</b> de tu aplicaci�n. Si no la haces, se caer� la aplicaci�n.<br>


<br>Compila el proyecto con <b>F5</b> y deber�a ejecutarse la aplicaci�n:<br>
<img border="0" src="imagenes/vs2008-4.PNG">

<br><br>El proyecto completo usando VS 2008 b�jalo desde <a href="proyectos/VS2008_SDL6.0_cap1.zip">aqu�</a>.

<br><br><hr style="width: 100%; height: 2px;">

<a href="cap0-1.php">Anterior</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap2.php">Siguiente</a>

<?php
include '../../piecdisq.php';
?>
