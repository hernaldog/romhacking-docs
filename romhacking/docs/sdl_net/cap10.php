<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>Tutorial de Creaci�n de Video Juegos con SDL .NET</title>
</head>


<body>
<small>
<span style="font-family: Verdana;">
<a href="cap9.php">Anterior</a> | <a href="SDL_NET_menu.php">�ndice</a>
</small>

<hr style="width: 100%; height: 2px;">

<table width="100%">
<tr>
<td>
<big>
<span style="font-weight: bold;">Cap�tulo 10: Velocidad de Animaci�n</span>
</big>
<small>
<br style="font-weight: bold;"> 
Lenguaje: C#<br>
Para:  VS2005 / VS2008<br>
Por Dark-N: <?php include '../../mail.php'; ?>
<br>

<span style="font-family: Verdana;">
<a href="http://darknromhacking.com/">http://darknromhacking.com</a></small></td>
<td align="center">
<img src="imagenes/sdlnet.gif">
</table>

<hr style="width: 100%; height: 2px;"><br>

<span style="font-family: Verdana;"><small>
<b>Objetivo:</b> Animaremos un sprite de 5 formas distintas con tal de mostrar ejemplos pr�cticos de como manejar la velocidad de la animaci�n.

<br><br>

Cuando se trabajan con sprites, por defecto cuando no se realiza un manejo de velocidades de animaci�n, en cada "tick" que se ejecuta del c�digo, se dibujan los frames a una velocidad de <b>60 frames por segundo</b>, esto quiere decir, 1 frame cada 0,0166666 segundos, o lo que es lo mismo, <b>1 frame cada 16 milisegundos</b> aproximadamente.
<br><br>
Si manejamos la velocidad de animaci�n, podemos hacer que cierta animaci�n ande m�s r�pido de lo normal o m�s lento, de esta forma un puede simular un sin fin de situaciones. Un ejemplo cl�sico es el de 2 personajes, uno que corre de un lado a otro y otro que solo camina. Ahora, para simular correctamente las acciones de caminar y correr, para el personaje que camina, la velocidad de animaci�n debe ser menor a la del personaje que corre, para esto lo que hacemos es leer cada frame y no pintando inmediatamente, sino que esperando cierto tiempo antes de pintar el siguiente frame.
<br><br>
Por ejemplo, si en nuestro juego, para la animaci�n de caminar/correr, se tiene un sprite con 3 frames como el sprite de abajo:<br><br>
<img src="imagenes/sprite_derecha.png">
<br>
Si vamos a mostrar un personaje caminando, lo normal es leer al sprite, y mostrar en pantalla de a un frame a la vez, y dejando X tiempo de espera, antes de empezar con el otro frame.
<br>En cambio si lo mostramos al personaje corriendo, debemos hacer lo mismo anterior pero debemos esperar menos tiempo (X - algo) antes de seguir con el siguiente frame.

<br><br>
Ahora vamos a la implementaci�n, como vimos en el cap�tulo anterior, para el manejo de animaciones, SDL.Net nos provee de la clase <b>AnimationCollection</b>. Esta clase tiene el m�todo <b>Add</b> con varias sobrecargas, y una de ellas es:
<br><br>
 <b>AnimationCollection.Add (SurfaceCollection miColeccionSurfaces, Double tiempoEsperaMiliseg)</b>
<br><br>
Donde los par�metros son:<br><br>
-SurfaceCollection: es la colecci�n de Surfaces cargada previamente, por ejemplo:<br><br>
  SurfaceCollection() miColecionSurfaces = new SurfaceCollection();<br>
  miColecionSurfaces.Add(new Surface("sprite_derecha.png"), new Size(24, 32), 0);<br><br>

-Double: tiempo de espera en milisegundos. SDL.Net por defecto lo setea en 30 milisegundos si no se indica este campo. En nuestro caso es fundamental ya que es el que cambiaremos para m�s de 30 (en caso que queramos que el tiempo de espera sea mayor) o menor a 30 en caso que queramos que sea menor.

<br><br>

Basados en este m�todo, generaremos 5 animaciones de ejemplo:<br><br>
&nbsp&nbsp&nbsp1. Leeremos los 3 frames del sprite y mostraremos la animaci�n sin desplazarse por la pantalla, con el Delay por defecto 30 milisegundos.
Por lo tanto usaremos algo as�:
<br>
&nbsp&nbsp<b>miAnimacioncoeccion.Add(miSurfaceColeccion, 30);</b>
<br><br>
&nbsp&nbsp&nbsp2. Leeremos los 3 frames del sprite y mostraremos la animaci�n sin desplazarse por la pantalla, con el Delay de 90 milisegundos. Esto lo har� verse m�s lento que el caso anterior, dando el efecto de que el personaje camina. Por lo tanto usaremos algo as�
<br>
&nbsp&nbsp<b>miAnimacioncoeccion.Add(miSurfaceColeccion, 90);</b>
<br><br>
&nbsp&nbsp&nbsp3. Leeremos los 3 frames del sprite y mostraremos la animaci�n desplaz�ndose por la pantalla de a 1 pixels por cada Tick. Este desplazamiento no lo estamos controlando, por lo tanto, lo primero que hacemos es utilizar el m�todo <b>Fps</b> de la clase de SDL.Net <b>Events</b> que a la vez es del Namespace <b>SdlDotNet.Core</b>, que nos permite setear la cantidad frames o ticks por segundos que correr� la aplicaci�n:
<br>
&nbsp&nbsp
<b>public void Run() 
<br>&nbsp&nbsp&nbsp{<br>
&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Events.Fps = 60;
<br>&nbsp&nbsp&nbsp}</b>

<br>con esto seteamos que cada tick se ejecutar� a 60 Ticks por segundo o 60 FPS por lo tanto en cada tick se mover� 1 pixels a la derecha. Por otra parte, usaremos un Delay de animaci�n de 30 milisegundos, por lo que usaremos algo as�:
<br>
&nbsp&nbsp<b>miAnimacioncoeccion.Add(miSurfaceColeccion, 30);</b><br>
y en game loop tenemos la l�gica:
<br>
&nbsp&nbsp<b>++miAnimatesprite.X;</b>
<br><br>
&nbsp&nbsp&nbsp4. Leeremos los 3 frames del sprite y mostraremos la animaci�n desplaz�ndose por la pantalla de a 1 pixels por cada Tick. Este desplazamiento si lo estamos controlando, por lo tanto a cada tick que suceda (60 Ticks por segundo), NO se mover� 1 pixels a la derecha inmediatamente como en el caso anterior, sin� que definiremos una variable que tendr� el tiempo que quiero que espere antes de moverse a la derecha, en este caso 48 milisegundos:
<br>
&nbsp&nbsp<b>int milisegndosPorFrame = 48</b>.
<br>Adem�s usaremos un Delay de animaci�n de 90 milisegundos por lo que ser� que se mueve a la derecha pero caminando y no corriendo. Por lo tanto usaremos algo as�
<br>
&nbsp&nbsp<b>miAnimacioncoeccion.Add(miSurfaceColeccion, 90);</b>
<br>
y en Loop
<br>
&nbsp&nbsp<b> if (Timer.TicksElapsed - ultimoCalculoFrame >= milisegndosPorFrame)<br>
&nbsp&nbsp&nbsp&nbsp {<br>
&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ++personaje4.X;<br>
&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ultimoCalculoFrame = Timer.TicksElapsed;
&nbsp&nbsp&nbsp&nbsp <br>
&nbsp&nbsp&nbsp&nbsp }</b>
<br><br>
&nbsp&nbsp&nbsp5. Leeremos los 3 frames del sprite y mostraremos la animaci�n desplaz�ndose por la pantalla de a 2 pixels por cada Tick (m�s r�pido que en el caso 3). Este desplazamiento no lo estamos controlando, por lo tanto a cada tick que suceda (60 Ticks por segundo igual que en el caso 3), se mover� 2 pixels a la derecha. Adem�s usaremos un Delay de animaci�n de 10 milisegundos, por lo que ver�n que las piernas las mueve realmente r�pido. Por lo tanto usaremos algo as�
<br>
&nbsp&nbsp<b>miAnimacioncoeccion.Add(miSurfaceColeccion, 10);</b> 
<br>y en Loop
<br>
&nbsp&nbsp<b>miAnimatesprite.X +=2;</b>

<br><br><br>
Bueno, esto es todo por hoy, quiz� quedaron cosas en el aire, pero mirando el c�digo fuente, seguro entender�n.
<br>

<br><hr style="width: 100%; height: 2px;"><br>
C�digo fuente de este cap�tulo para VS 2008, C# 2.0 y SDL.Net 6.1:

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
//Tutorial de SDL.NET
//Archivo: cap10.cs
//Autor: Dark-N
//Fecha: 13-06-2011
//Capitulo4: Animaci�n b�sica de Sprites. Ahora usando VS2008 con SDL.NET 6.1

using System;
using System.Drawing;
using SdlDotNet;
using SdlDotNet.Graphics; //para Sprites y surface Video
using SdlDotNet.Core; //Eventos Tick y Quit
using SdlDotNet.Input; //eventos teclado
using SdlDotNet.Graphics.Sprites;  //textos y graficos

namespace tutorial
{	
	public class cap10
	{
		int resx = 640;
		int resy = 480;
		
		Surface hojaSprite;
		AnimatedSprite personaje1 = new AnimatedSprite();
		AnimatedSprite personaje2 = new AnimatedSprite();
		AnimatedSprite personaje3 = new AnimatedSprite();
		AnimatedSprite personaje4 = new AnimatedSprite();
		AnimatedSprite personaje5 = new AnimatedSprite();
		AnimationCollection an1 = new AnimationCollection();
		AnimationCollection an2 = new AnimationCollection();
		AnimationCollection an3 = new AnimationCollection();
		SurfaceCollection caminaDerechaSrfColl;
		Size tamanoFrame = new Size(24, 32); // tama�o de frame en el sprite
		SdlDotNet.Graphics.Font fuente = new SdlDotNet.Graphics.Font("../../fuentes/ARIAL.TTF", 12);
		TextSprite texto1, texto2, texto3, texto4, texto4cont, texto5, texto5cont, texto6, texto6cont;
		
		//tiempos para manejar velocidad de animaci�n
		int ultimoCalculoFrame;	
		int milisegndosPorFrame = 48; // para animaci�n lenta a 20 FPS o 1 frame cada 48 milisegundos
		int cantDelayFramesAnimacion1 = 30; //30 por defecto. En los 3 frames que tiene la hoja de sprites, cada vez que recorre 1, espera 30 milisegundos antes de pasar al siguiente
		int cantDelayFramesAnimacion2 = 90; //90 de delay milisegundos entre cada frame
		int cantDelayFramesAnimacion3 = 10; //10 de delay milisegundos entre cada frame. Se ve caminando muy r�pido


		public cap10()
		{
			ultimoCalculoFrame = Timer.TicksElapsed;
			Video.SetVideoMode(resx, resy);			
			Video.WindowCaption = "Tutorial SDL: Capitulo 10 - VS2008 - SDL 6.1";
			
			hojaSprite = new Surface("../../Imagenes/sprite_derecha.png");
    
			// Se crean los Frames de Animaci�n
			caminaDerechaSrfColl = new SurfaceCollection();
			caminaDerechaSrfColl.Add(hojaSprite, tamanoFrame, 0);
			an1.Add(caminaDerechaSrfColl, cantDelayFramesAnimacion1);
			an2.Add(caminaDerechaSrfColl, cantDelayFramesAnimacion2); //animacion para mostrar personaje que anda mas lento, caminando
			an3.Add(caminaDerechaSrfColl, cantDelayFramesAnimacion3); //animacion para mostrar personaje que anda mas r�pido
			
			// Agregamos las animaciones al personaje. Se ver� corriendo
			personaje1.Animations.Add("CaminaDerecha", an1);
			// Pintamos transparente el fondo del sprite
			personaje1.TransparentColor = Color.Magenta;
			personaje1.Transparent = true;
			personaje1.CurrentAnimation = "CaminaDerecha";
			personaje1.Animate = true;
			
			//personaje 2 se mueve m�s lento, simulando que camina
			personaje2.Animations.Add("CaminaDerecha", an2);
			personaje2.TransparentColor = Color.Magenta;
			personaje2.Transparent = true;
			personaje2.CurrentAnimation = "CaminaDerecha";
			personaje2.Animate = true;
			
			// personaje 3 se mueve a la derecha m�s r�pido, se ve corriendo
			personaje3.Animations.Add("CaminaDerecha", an1);
			personaje3.TransparentColor = Color.Magenta;
			personaje3.Transparent = true;
			personaje3.CurrentAnimation = "CaminaDerecha";
			personaje3.Animate = true;
			
			// personaje 4 se mueve a la derecha, pero caminando y no corriendo
			personaje4.Animations.Add("CaminaDerecha", an2);
			personaje4.TransparentColor = Color.Magenta;
			personaje4.Transparent = true;
			personaje4.CurrentAnimation = "CaminaDerecha";
			personaje4.Animate = true;
			
			// personaje 5 se mueve a la derecha, pero caminando muy r�pido
			personaje5.Animations.Add("CaminaDerecha", an3);
			personaje5.TransparentColor = Color.Magenta;
			personaje5.Transparent = true;
			personaje5.CurrentAnimation = "CaminaDerecha";
			personaje5.Animate = true;
			
			//pocisiones
			personaje1.X = 10; personaje1.Y = 60;
			personaje2.X = 10; personaje2.Y = 140;
			personaje3.X = 0;  personaje3.Y = 210;
			personaje4.X = 0;  personaje4.Y = 290;
			personaje5.X = 0;  personaje5.Y = 370;
		}

		private void Events_Tick(object sender, TickEventArgs e)
		{
			Video.Screen.Fill(Color.Black);
			texto1 = new TextSprite("Ejemplo Animaci�n de un Sprite con distintas velocidades de animaci�n", fuente, Color.Yellow, new Point(0, 5));
			texto2 = new TextSprite("Animaci�n corriendo con un delay de 30 milisegundos antes de pasar al otro frame. Velocidad por defecto.", fuente, Color.White, new Point(0, 40));
			texto3 = new TextSprite("Animaci�n corriendo con un delay de 90 milisegundos antes de pasar al otro frame. Se ve caminado.", fuente, Color.White, new Point(0, 120));
			texto4 = new TextSprite("Movemos Personaje 60 veces por segundo (60 FPS - frames por segundo) o 1 frame cada 16 milisegundos", fuente, Color.White, new Point(0, 180));
			texto4cont = new TextSprite("Adem�s le damos el efecto de que se ve corriendo ya que el delay entre cada frame es de 30 miliseg.", fuente, Color.White, new Point(0, 180 + 12));
			texto5 = new TextSprite("Movemos Personaje 20 veces por segundo (20 FPS - frames por segundo) o 1 frame cada 48 milisegundos", fuente, Color.White, new Point(0, 260));
			texto5cont = new TextSprite("Adem�s le damos el efecto de que se ve camindo ya que el delay entre cada frame es de 90 miliseg.", fuente, Color.White, new Point(0, 260 + 12));
			texto6 = new TextSprite("Movemos Personaje a 60 FPS, con 2 pixels de movimiento y adem�s con un delay de animaci�n de 10 miliseg,", fuente, Color.White, new Point(0, 340));
			texto6cont = new TextSprite("lo que hace que se mueva muy r�pido.", fuente, Color.White, new Point(0, 340 + 10));
			
			Video.Screen.Blit(personaje1);
			Video.Screen.Blit(personaje2);
			Video.Screen.Blit(personaje3);
			Video.Screen.Blit(personaje4);
			Video.Screen.Blit(personaje5);
			
			LogicaMovimiento();
			
			Video.Screen.Blit(texto1);
			Video.Screen.Blit(texto2);
			Video.Screen.Blit(texto3);
			Video.Screen.Blit(texto4);
			Video.Screen.Blit(texto4cont);
			Video.Screen.Blit(texto5);
			Video.Screen.Blit(texto5cont);
			Video.Screen.Blit(texto6);
			Video.Screen.Blit(texto6cont);
			
			Video.Screen.Update();
		}


		private void LogicaMovimiento() 
		{
			if (personaje1.Animate)
			{
			    ++personaje3.X;
			}
			
			if (personaje5.Animate)
			{
			    personaje5.X += 2;
			}
			
			if (personaje2.Animate)
			{
			    //solo movemos el personaje si han pasado los frames que establecimos
			    if (Timer.TicksElapsed - ultimoCalculoFrame >= milisegndosPorFrame)
			    {
			        ++personaje4.X;
			        ultimoCalculoFrame = Timer.TicksElapsed;
			    }
			}
		}		

		public void Run()
		{	
			Events.Fps = 60;
			Events.Tick += new EventHandler<TickEventArgs>(this.Events_Tick);
			Events.KeyboardDown += new EventHandler<KeyboardEventArgs>(Events_KeyboardDown);
			Events.Quit += new EventHandler<QuitEventArgs>(this.Events_Salir);
			Events.Run();
		}

		private void Events_Salir(object sender, QuitEventArgs e)
		{
			Events.QuitApplication();
		}

		private void Events_KeyboardDown(object sender, KeyboardEventArgs e)
		{
			switch(e.Key)
			{				
				case Key.Escape:
					Events.QuitApplication();
					break;
			}							
		}

		[STAThread]
		public static void Main()
		{
			cap10 juego = new cap10();
			juego.Run();
		}
	}
	
}
</pre></td></tr></table>


<br><br>
Una vez codificado, si al compilar no arroja errores, debe aparecerte algo como esto:<br><br>
<img src="imagenes/cap10.PNG">

<br><br>Bajar proyecto para VS 2008 usando SDL.Net 6.1 <a href="proyectos/VS2008_SDL6.1_cap10.zip">aqu�</a>.
<br>

<br><hr style="width: 100%; height: 2px;"><br>
<a href="cap9.php">Anterior</a> | <a href="SDL_NET_menu.php">�ndice</a>

<?php
include '../../piecdisq.php';
?>

