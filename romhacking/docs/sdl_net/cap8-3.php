<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
<title>Tutorial de Creaci�n de Video Juegos con SDL .NET</title>
</head>


<body>
<small>
<span style="font-family: Verdana;">
<a href="cap7.php">Anterior</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap9.php">Siguiente</a>
</small>

<hr style="width: 100%; height: 2px;">

<table width="100%">
<tr>
<td>
<big>
<span style="font-weight: bold;">Cap�tulo 8: Part�culas - Gravedad y Seguimiento del Mouse</span>
</big>
<small>
<br style="font-weight: bold;"> 
Lenguaje: C#<br>
Para: VS2005 / VS2008 / SharpDevelop 2.2<br>
Por Dark-N: <?php include '../../mail.php'; ?>
<br>

<span style="font-family: Verdana;">
<a href="http://darknromhacking.com/">http://darknromhacking.com</a>
<br><br><a href="cap8-1.php">Ir a Parte 1</a> | <a href="cap8-2.php">Ir a Parte 2</a></td>

</td>

<td align="center">
<img src="imagenes/sdlnet.gif">
</table>

<hr style="width: 100%; height: 2px;"><br>

<span style="font-family: Verdana;"><small>
<b>Objetivo:</b> En este cap�tulo, se har�n 2 part�culas con gravedad que siguen al mouse, si este se les acerca. 
<br><br>Nota: Para todo el t�pico de part�culas use el c�digo libre hecho por uno de los programadores de SDL.Net <a href="http://cs-sdl.sourceforge.net/index.php/User:Rob_Loach">Rob Loach</a>.

<br><br>

La part�cula m�s peque�a que podemos tener, es un simple pixel, que se trabaja como una part�cula. Para esto SDL tiene la clase <b>ParticlePixel</b>. Esta se defime as�:

<br><table><tr><td bgcolor="#CCCCCC"><pre>
ParticlePixel miParticula = new ParticlePixel(color de la particula, pocicion X, pocicion Y, velocidad, duracion (si es -1 es infinita));
Ejemplo:
ParticlePixel pixelParticula = new ParticlePixel(Color.White, 100, 200, new Vector(0, 0, 0), -1);
</pre></td></tr></table>

La part�cula m�s grande que se puede tener es la de cualquier Surface que se trabaja como una part�cula. Para esto se tiene la clase <b>ParticleSprite</b>. Se define as�:

<br><table><tr><td bgcolor="#CCCCCC"><pre>
ParticleSprite miParticula = new ParticleSprite(Objeto Sprite,  pocicion X, pocicion Y, velocidad, duracion (si es -1 es infinita));
Ejemplo:
Sprite spr = new Sprite();
ParticleSprite second = new ParticleSprite(spr, 200, 200, new Vector(-7, -9, 0), 500);
</pre></td></tr></table>

La clase que nos sirve para darle "peso" o gravedad a los objetos es <b>ParticleGravity</b> y para manejar las fricciones entre part�culas tenemos <b>ParticleFriction</b>, que se sefine as�:
<br><table><tr><td bgcolor="#CCCCCC"><pre>
ParticleGravity miGravedad = new ParticleGravity(cuanta gravedad);
Ejemplo:
ParticleGravity grav = new ParticleGravity(0.5f);

ParticleFriction miFriccion = new ParticleFriction(cuanta friccion aplicar a la particula);
Ejemplo:
ParticleFriction fric = new ParticleFriction(0.1f);
</pre></td></tr></table>

Si se quiere que las part�culas sigan al mouse solo si se les aserca, se usa la clase <b>ParticleVortex</b>, que se define as�:

<br><table><tr><td bgcolor="#CCCCCC"><pre>
ParticleVortex miVortice = new ParticleVortex(fuerza, radio afectado);
Ejemplo:
ParticleVortex vort = new ParticleVortex(1f, 200f);
</pre></td></tr></table>

A m�s Fuerza, a penas muevas el mouse, las part�culas lo seguiran con rapidez y con la trayectoria que tiene el movimiento del mouse, sobre todo si haces movimientos bruscos. El Radio permite regular cuan cerca tienes que acercar el mouse para que las part�culas detecten el mouse para seguirlo.

<br><hr style="width: 100%; height: 2px;"><br>
C�digo fuente de est� secci�n (VS2005/V2008):

<br><br><table><tr><td bgcolor="#CCCCCC"><pre>
#region LICENSE
/*
 * $RCSfile: ParticlesExample.cs,v $
 * Copyright (C) 2005 Rob Loach (http://www.robloach.net)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#endregion LICENSE

using System;
using System.IO;
using System.Drawing;

using SdlDotNet.Particles;
using SdlDotNet.Particles.Emitters;
using SdlDotNet.Particles.Manipulators;
using SdlDotNet.Graphics.Sprites;
using SdlDotNet.Core;
using SdlDotNet.Input;
using SdlDotNet.Graphics;

namespace EjemploParticulas
{
	
	public class Ejemplo3 : IDisposable
	{
		ParticleSystem particles = new ParticleSystem();
		ParticleVortex vort = new ParticleVortex(1f, 200f);

		public Ejemplo3()
		{
			Video.WindowIcon();
			Video.WindowCaption = "SDL.NET - Particula Pixel y Particula Sprite";
			Video.SetVideoMode(400, 300);
			Events.KeyboardDown += new EventHandler<KeyboardEventArgs>(this.KeyboardDown);			
			Events.MouseMotion += new EventHandler<MouseMotionEventArgs>(this.MouseMotion);
			Events.Fps = 30;
			Events.Tick += new EventHandler<TickEventArgs>(this.Tick);
			Events.Quit += new EventHandler<QuitEventArgs>(this.Quit);
		}
		
		public void Go()
		{	

			//creamos una particula que es un pixel
			ParticlePixel first = new ParticlePixel(Color.White, 100, 200, new Vector(0, 0, 0), -1);
			particles.Add(first); // la agregamos al sistema
			
			//hacemos otra particula que es sprite animado
			AnimationCollection anim = new AnimationCollection();
			SurfaceCollection surfaces = new SurfaceCollection();
			surfaces.Add("BALL2.png", new Size(50, 50));
			anim.Add(surfaces, 1);
			AnimatedSprite marble = new AnimatedSprite(anim);
			marble.Animate = true;
			ParticleSprite second = new ParticleSprite(marble, 200, 200, new Vector(-7, -9, 0), 500);
			second.Life = -1;
			particles.Add(second); // la agregamos al sistema

			// agregamos manipuladores 
			ParticleGravity grav = new ParticleGravity(0.5f);
			particles.Manipulators.Add(grav); // Gravedad de 0.5f
			particles.Manipulators.Add(new ParticleFriction(0.3f)); // Slow down particles
			particles.Manipulators.Add(vort); // se agrega el efecto de v�rtice si el mouse se acerca a la part�cula
			particles.Manipulators.Add(new ParticleBoundary(SdlDotNet.Graphics.Video.Screen.Size)); // fix particles on screen.

			Events.Run();
		}


		[STAThread]
		public static void Main()
		{
			Ejemplo3 particula = new Ejemplo3();
			particula.Go();
		}

		
		private void Tick(object sender, TickEventArgs e)
		{
			particles.Update();
			Video.Screen.Fill(Color.Black);
			particles.Render(Video.Screen);
			Video.Screen.Update();
		}

		private void KeyboardDown(object sender, KeyboardEventArgs e)
		{
			if (e.Key == Key.Escape)
			{
				Events.QuitApplication();
			}			
		}

		private void Quit(object sender, QuitEventArgs e)
		{
			Events.QuitApplication();
		}		
		
		private void MouseMotion(object sender, MouseMotionEventArgs e)
		{
			vort.X = e.X;
			vort.Y = e.Y;
		}		

		#region IDisposable Members

		private bool disposed;

		/// <summary>
		/// Destroy object
		/// </summary>
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Destroy object
		/// </summary>
		public void Close()
		{
			Dispose();
		}

		/// <summary>
		/// Destroy object
		/// </summary>
		~Ejemplo3()
		{
			Dispose(false);
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="disposing"></param>
		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				this.disposed = true;
			}
		}

		#endregion
	}
}
</pre></td></tr></table>

<br>
Una vez codificado, si al compilar no arroja errores, debe aparecerte algo como esto al hacer clic en la pantalla:<br><br>
<img src="imagenes/cap83.PNG">

<br><br>Bajar proyecto para SharpDevelop 2.2 (lo siento, esta vez no lo hice en VS.NET ;) <a href="proyectos/VS2008_SDL6.0_cap8.zip">aqu�</a>.
<br>(En el Proyecto ZIP est�n los fuentes de las parte 1, 2 y 3. Adem�s hay un 4to c�digo de ejemplo, <i>ejemplo4.cs</i>, que ser�a como un "todo en uno" de las partes 1, 2 y 3.
.php
<br><br>
<a href="cap8-1.php">Ir a Parte 1</a> | <a href="cap8-2.php">Ir a Parte 2</a>

<br><hr style="width: 100%; height: 2px;"><br>
<a href="cap7.php">Anterior</a> | <a href="SDL_NET_menu.php">�ndice</a> | <a href="cap9.php">Siguiente</a>

<?php
include '../../piecdisq.php';
?>

